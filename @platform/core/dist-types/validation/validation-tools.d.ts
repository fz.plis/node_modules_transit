import type { IRuleInfo, IValidators, IValidationResult } from './interfaces';
export declare const EMPTY_RESULT: {
    warnings: {};
    errors: {};
};
export declare const getExpectedTwoStringArgsErr: (a1?: any, a2?: any) => string;
export declare const fillIndexes: (mainPath: string, secondPath: string) => string;
export declare const validateField: (subPath: string, rules: IRuleInfo[], allValues: any, validators: IValidators, currentValues?: any, pathPrefix?: string) => IValidationResult;
