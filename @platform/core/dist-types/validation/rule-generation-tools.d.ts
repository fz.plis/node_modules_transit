import type { IRuleInfo, IConditionInfo, IFiledValidationRules, IConditionGetter } from './interfaces';
export declare type Composer = {
    compose(): IFiledValidationRules;
} | {
    compose(): IFiledValidationRules[];
};
export interface IRuleGetter {
    compose(): IRuleInfo;
    but(...conditions: IConditionGetter[]): {
        compose(): IRuleInfo;
    };
}
export declare const conditionGenerator: <T extends Record<string, (...args: any[]) => IConditionGetter>>(conditionsGetters: T) => (path: string) => T;
export declare const rulesGenerator: <T extends Record<string, (...args: any[]) => IRuleGetter>>(rulesGetters: T, severity: string) => T;
export declare const asRule: <T extends Omit<IRuleInfo, "severity">>(ruleBaseGetter: T) => IRuleGetter;
export declare const asCondition: <T extends Omit<IConditionInfo, "path">>(conditionBaseGetter: T) => IConditionGetter;
export declare const check: (path: string) => {
    on: (...rulesGetters: Array<{
        compose(): IRuleInfo;
    }>) => {
        readonly compose: () => IFiledValidationRules;
        readonly but: (...conds: IConditionGetter[]) => {
            readonly compose: () => IFiledValidationRules;
            readonly withDependent: (...paths: string[]) => {
                compose: () => IFiledValidationRules;
            };
        };
        readonly withDependent: (...paths: string[]) => {
            readonly compose: () => IFiledValidationRules;
            readonly but: (...conds: IConditionGetter[]) => {
                compose: () => IFiledValidationRules;
            };
        };
    };
};
export declare const groupOf: (...checks: Array<Composer | undefined>) => {
    compose: () => IFiledValidationRules[];
    but: (...conds: IConditionGetter[]) => {
        compose: () => IFiledValidationRules[];
    };
};
export declare const composeValidation: (...composers: Array<Composer | undefined>) => IFiledValidationRules[];
