export interface IValidatorParams {
    params?: any;
    message: string;
}
/** Дополнительная информация по валидируемому полю. */
export interface ValidatingFieldInfo {
    /** Полный путь до валидируемого поля, с индексами в массивах. */
    fullPath: string;
}
export declare type FieldValidator = (value: any, allValues?: any, fieldInfo?: ValidatingFieldInfo) => string | undefined;
export declare type Validator = (params: IValidatorParams) => FieldValidator;
export declare type IValidators = Record<string, Validator>;
export declare enum RULE_SEVERITIES {
    ERROR = "ERROR",
    WARNING = "WARNING"
}
export interface IConditionInfo {
    id: string;
    path: string;
    params?: Record<string, any>;
}
export interface IRuleInfo {
    id: string;
    conditions?: IConditionInfo[];
    message: string;
    messageParams?: string;
    params?: Record<string, any>;
    severity: RULE_SEVERITIES;
}
export interface IFiledValidationRules {
    path: string;
    rules: IRuleInfo[];
    dependentPaths?: string[];
}
export interface IValidationResult {
    errors: Record<string, string[]>;
    warnings: Record<string, string[]>;
}
export interface IConditionGetter {
    compose(): IConditionInfo;
}
export interface IValidationMaster {
    validate(allValues: any, rules?: IFiledValidationRules[]): IValidationResult;
    validateByName(allValues: any, name: string): IValidationResult;
    validateByPrefix(allValues: any, preifx: string): IValidationResult;
}
