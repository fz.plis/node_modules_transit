import type { IValidators } from './interfaces';
export declare const NO_ERROR: undefined;
export declare const validators: IValidators;
