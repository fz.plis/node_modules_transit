import type { IFiledValidationRules, IValidators, IValidationMaster } from './interfaces';
export declare const createValidator: (allRules: IFiledValidationRules[], validators: IValidators) => IValidationMaster;
