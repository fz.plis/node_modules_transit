import type { IExecuter, IActionResult } from './interfaces';
/**
 * Создает мидлварь для экшн-экзекутора, которая будет вызвана после завершения экшна.
 *
 * @param onUpdate - Колбэк, который будет вызван после завершения экшна и получит результаты исполнения экшна.
 *
 * @example
 * applyMiddlewares(
 *  afterMiddleware((result) => console.log(result))
   )(executor)
 *
 */
export declare const afterMiddleware: <T>(onUpdate: (r: IActionResult<T>) => void) => (executer: IExecuter<T>) => IExecuter<T>;
/**
 * Создает мидлварь для экшн-экзекутора, которая обычно используется для автоматического управления состоянием загрузки `isLoading`.
 *
 * @param setLoading - Функция, которая перед исполнением экшна будет вызвана с параметром `true`, а после с параметром `false`.
 *
 * @example
 * const [isLoading, setIsLoading] = useState(false);
 *
 * applyMiddlewares(
 *  loadingMiddleware(setIsLoading)
   )(executor)
 *
 */
export declare const loadingMiddleware: (setLoading: (v: boolean) => void) => <T>(executer: IExecuter<T>) => IExecuter<T>;
/**
 * Функция которая возвращает функцию-middleware. Первым аргументом является функция которая будет вызвана
 * если в результате выполнения действия есть хотя бы одни успешный результат.
 * Первым аргументом в callback будет переданы все результаты выполнения действия (ActionResult).
 *
 * @param onSuccess - Колбэк, который будет вызван, если массив `succeeded` не пустой.
 * Колбэк получит в качестве аргумента результаты исполнения экшна.
 *
 * @example
 * const executerWithOnSuccessCallback = onSuccessMiddleware((result) => {
      console.log(result.succeeded.length > 0); // true
    })(executer);
 */
export declare const onSuccessMiddleware: <T>(onSuccess: (r: IActionResult<T>) => void) => (executer: IExecuter<T>) => IExecuter<T>;
/**
 * Функция которая возвращает функцию-middleware. Первым аргументом является функция которая должна вернуть массив с аргументами,
 * которые будут переданы в функцию execute и validate в качестве аргументов действия если функция execute
 * или validate обернутого executer'а будет вызвана без параметров. В случае если функция execute или validate обернутого executer'а
 * будет вызвана с параметрами, то они и будут использоваться.
 *
 * @param argsGetter - Эта функция будет вызвана и ее результаты будут переданы в качестве аргументов при исполнении экшна экзекутором.
 *
 * @example
 * const executerWithArgs = argsMiddleware(() => [1, 2, 3])(executer);
 
    executerWithArgs.execute(someAction).then((result) => {
      console.log(result.args); // [1, 2, 3]
    });
 
    executerWithArgs.execute(someAction, 'test').then((result) => {
      console.log(result.args); // ['test']
    });
 */
export declare const argsMiddleware: (argsGetter: () => any[]) => <T>(executer: IExecuter<T>) => IExecuter<T>;
/**
 * Функция для внедрения мидлварь в процесс экзекутора.
 *
 * @description Миддлвари направлены на расширение функциональности экшн экзекутора,
 * путем внедрения разных обработчиков в процесс исполнения действия.
 *
 * @param middlewares Список мидлварь через запятую.
 * @returns  Функцию, которая принимает экзекутор в качестве единственного аргумента, которая вернет модифицированный экзекутор,
 * с внедренными мидлварями.
 *
 * @example
 * let loading = false;
 
  const nextExecuter = applyMiddlewares(
    argsMiddleware(() => [1,2,3]),
    onSuccessMiddleware((result) => {
      console.log(result.succeeded.length > 0); // true
    },
    loadingMiddleware((val) => {
      loading = val;
    }
  )(executer);
 */
export declare const applyMiddlewares: <T>(...middlewares: ((ex: IExecuter<T>) => IExecuter<T>)[]) => (executer: IExecuter<T>) => IExecuter<T>;
