import type { Guardian } from './interfaces';
export declare const throwError: (error: string) => never;
export declare const ERRORS: {
    ONLY_ONE_DOCUMENT: string;
    DOCUMENT_SHOULD_BE_AN_OBJECT: string;
    DOCUMENTS_SHOULD_EXISTS: string;
    ONE_OR_MORE_DOCUMENTS_IN_WRONG_STATUSES: string;
    ACCEPT_ONLY_ONE_DEFINED_ARGUMENT: string;
    DEFAULT_ERROR_FOR_CUSTOM_GUARDIAN: string;
};
/**
 * Гардиан для проверки того, что экшну передан ровно 1 аргумент.
 * Не путать с singleAction, который проверяет, что первый  аргумент экшна - это массив из 1го элемента.
 */
export declare const oneDefinedArg: Guardian;
/**
 * Гардиан для проверки, что экшну передан ровно 1 документ.
 */
export declare const singleAction: Guardian;
/**
 * Гардиан для проверки того, что экшну передан не пустой массив документов.
 */
export declare const groupAction: Guardian;
/**
 * Гардиан для проверки того, что текущий статус документа находится в списке дозволенных.
 */
export declare const inStatuses: (statuses?: string[], statusGetter?: (d: any) => string) => Guardian;
/**
 * Создаёт гардиан.
 *
 * @param predicate - Функция предикат, которую созданный гардиан будет использовать для валидации.
 * @param errorMessage - Сообщение, которое будет выброшено в исключении, если в гардиан передано невалидное значение.
 *
 * @example
 * import { executer } from '@platform/core'
 * import { send } from '@platform/services'
 *
 * const isString = s => typeof s === string;
 * const isNotStringGuardian = not(isString)
 *
 * const sendAction = {
 *   ...send,
 *   guardians: [...send.guardians, isNotStringGuardian]
 * }
 *
 * executer.validate(sendAction, "string") // невалидно, вернёт ошибку
 *
 * executer.validate(sendAction, [5, "string"]) // невалидно, вернёт ошибку
 *
 * executer.validate(sendAction, [5]) // валидно, вернёт undefined
 */
export declare const not: <T>(predicate: (item: T) => boolean, errorMessage?: string) => (param: T | T[]) => void;
