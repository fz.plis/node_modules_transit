import type { ACTION_ADDONS } from '../interfaces';
/**
 * @ignore
 */
export declare type ExecuteFunc<T> = <TResult>(action: IActionConfig<T, TResult>, ...args: any[]) => Promise<IExecuteResult<T, TResult>>;
/** Методы, предоставляемые executor-ом экшну. */
export interface IExecuterMethods<T, TResult> {
    /**
     * Метод для добавления в массив успешно обработанных элементов.
     *
     * @description Так как в большинстве случаев действия работают с заявками, то элементом является обрабатываемая заявка(и).
     * Если по окончанию действия, массив успешно обработанных элементов не пустой, то запустится функция `succeededHandler` из экшн-конфига.
     */
    addSucceeded(...args: TResult[]): void;
    /**
     * Метод для добавления в массив элементов, обработка которых была провалена.
     *
     * @description Если по окончанию действия, массив проваленных элементов не пустой, то запустится функция `failedHandler` из экшн-конфига.
     */
    addFailed(...args: TResult[]): void;
    /**
     * Метод для досрочного завершения действия.
     *
     * @description Обычно вызывается, если в действии произошла критическая ошибка и дальнейшная корректная работа невозможна.
     * Например мы не смогли запросить (или корректно обработать) данные с сервера.
     * Вызов этой функции немедленно завершит исполнении функции `action` и вызовет метод `fatalHandler` из экшн конфига.
     */
    fatal(err?: any): void;
    /**
     * Метод для успешного завершения действия.
     *
     * @description Обычно вызывается в конце экшна, чтобы логично завершить работу экшна и
     * передать управление хендлерам succeededHandler, failedHandler.
     * Но его так-же как и `fatal` можно вызвать в любом месте экшна, что досрочно завершит исполнение действия и вернет результаты действия.
     */
    done(): void;
    /**
     * Метод для исполнения других экшнов внутри экшна.
     *
     * @description Этим методом можно экзекутить другие экшны при необходимости.
     */
    execute: ExecuteFunc<T>;
}
/**
 * Гардиан - это функция для проверки того, что аргументы экшна сответствуют ожидаемым.
 *
 * @description Если эта проверка провалена, то основное действие не будет выполнено и будет завершено критической ошибкой `fatal`.
 * Гардиан не возвращает true или false, а выкидывает ошибку при провале.
 * Например: у нас есть экшн, который ожидает, что у документа, с которым он работает должен быть определенный статус (или статусы).
 * Тогда мы можем воспользоваться гардианом `inStatuses` следующим образом.
 * @example
 * ```typescript
 *  export const actionConfig = {
 *    action: ...,
 *    guardians: [inStatuses(...ALLOWED_STATUSES_ARRAY)],
 *  };
 * ```
 * @description И если статус документа, который получил экшн не соответствует тем, что в гардиане,
 * то будет выкинута ошибка и действие не будет исполнено.
 */
export declare type Guardian = (...args: any[]) => void;
/**
 * @ignore
 */
export interface IActionResult<TContext> {
    context: TContext;
    actionConfig: any;
    args: any[];
}
/**
 * @ignore
 */
export declare type ActionSuccessResult<T> = IActionResult<T> & {
    result: any;
};
/**
 * @ignore
 */
export declare type ActionFailResult<T> = IActionResult<T> & {
    error: any;
};
/**
 * @ignore
 */
export declare type IContext = Record<string, any>;
/**
 * @ignore
 */
export declare type ActionGetter<TContext, TResult> = (result: IExecuterMethods<TContext, TResult>, context: TContext) => (...args: any[]) => Promise<void>;
/**
 * Экшн конфиг.
 *
 * @description Это объект, который полностью описывает какое-либо действие,
 * включая логику, которая должна произойти до или после исполнения этого действия.
 *
 * @template TContext Контекст exectutor-а.
 * @template TResult Возвращаемый тип, при успешном исполнении действия.
 *
 * @example
 *   ```typescript
 *   // Экшн для сохранения документа
 *   const update: IActionConfig<ContextType, ISaveResponse<DocumentType>> = {
 *     action: ({ addFailed, fatal, addSucceeded, done }, { showLoader, hideLoader, service }) => async ([doc]: DocumentType[]) => {
 *       showLoader();
 *
 *       const [result, err] = await to(service.update);
 *
 *       hideLoader();
 *       fatal(err);
 *
 *       if (result?.validation?.errors && result.validation.errors.length > 0) {
 *         addFailed(result);
 *       }
 *
 *       addSucceeded(result!);
 *       done();
 *     },
 *     failedHandler: ({ showError }) => {
 *       showError('Ошибка', 'Ошибка при сохранении документа');
 *     },
 *     succeededHandler: ({ showSuccess }, [{ data: doc }]) => {
 *       showSuccess('', 'Документ успешно сохранен');
 *     },
 *     fatalHandler: ({ showError }) => {
 *       showError('Критическая ошибка');
 *     },
 *     guardians: [singleAction, inStatuses(EDITABLE_STATUSES)],
 *   };
 *   ```
 */
export interface IActionConfig<TContext, TResult> {
    /**
     * Основное действие.
     *
     * @description Это функция, которая содержит всю логику действия.
     */
    action: ActionGetter<TContext, TResult>;
    /** Массив гардианов. */
    guardians?: Guardian[];
    /**
     * Функция вызовется, если есть успешно обработанные элементы.
     *
     * @description Обычно в этой функции показываются разные модалки, которые оповещают, что действие выполнено успешно.
     *
     * @param context Контекст executor-а.
     * @param sucessedResults Массив успешно обработанных элементов.
     *
     */
    succeededHandler?(context: TContext, sucessedResults: any[]): void;
    /**
     * Функция вызовется, если есть неудачно обработанные элементы.
     *
     * @description Обычно в этой функции показываются разные модалки, которые оповещают, что действие провалено.
     *
     * @param context Контекст executor-а.
     * @param failedResults Массив элементов, обработка которых была провалена.
     *
     */
    failedHandler?(context: TContext, failedResults: any[]): void;
    /**
     * Функция вызовется, если произошла критическая ошибка в действии.
     *
     * @description Функция вызовется автоматически, если действие не прошло проверку гардианами.
     * Так-же это функция вызовется, если в действии был вызван метод `fatal`, который досрочно завершает исполнение действия.
     * Обычно это происходит, если мы не смогли запросить (или корректно обработать) данные с сервера.
     *
     * @param context Контекст executor-а.
     * @param result Результат выполнения действия.
     *
     */
    fatalHandler?(context: TContext, result: IExecuteResult<TContext, TResult>): void;
    /** Флаги, при которых выполняются дополнительные проверки. */
    addons?: ACTION_ADDONS[];
}
/**
 * Результаты выполнения экшна.
 *
 * @template TContext Контекст exectutor-а.
 * @template TResult Возвращаемый тип, при успешном исполнении действия.
 */
export interface IExecuteResult<TContext, TResult> {
    /**
     * Если при выполнении действия приозошла критическая ошибка, то она будет лежать в этом поле.
     */
    error?: Error;
    /**
     * Массив успешно обработанных элементов.
     */
    succeeded: any[];
    /**
     * Массив элементов, которые не удалось обработать.
     */
    failed: any[];
    /**
     * Экшн конфиг, который был обработан.
     */
    actionConfig: IActionConfig<TContext, TResult>;
    /**
     * Контекст экзекутора.
     */
    context: TContext;
    /**
     * Аргументы, которые экшн получил при исполнении.
     */
    args: any[];
}
/**
 * Экшн экзекутер.
 *
 * @template T Контекст executor-а.
 */
export interface IExecuter<T> {
    /**
     * Метод для проверки экшн конфига и его аргументов гардианами.
     * Вызывается автоматически перед исполнением действия executor-ом.
     * Если проверка будет провалена, то действие не будет исполнено, а просто досрочно завершится `fatal`-ом.
     */
    validate(action: IActionConfig<T, any>, ...args: any[]): any | undefined;
    /**
     * Исполненет действие с указанными аргументами.
     * Первым аргументом принимает сам экшн, который требуется исполнить `IActionConfig`,
     * а все остальные аргументы будут переданы экшну и его гардианам. Результатом исполнения экшна будет объект типа
     * `IExecuteResult`, который будет содержать результирующие данные, ошибки и прочая информация.
     *
     * @example
     * executor.execute(update, [documents], additionalParam1, additionalParam2, ...)
     */
    execute: ExecuteFunc<T>;
    /**
     * Контекст экузетора.
     *
     * @description Содержит все необходимые данные и методы необходимые экшнам.
     *
     * Cервис методы, разные модалки, информацию о пользователе и другие всопомгательные функции.
     * В большинстве случаев наследуется от `IBaseContext`.
     */
    context: T;
}
