import type { IExecuter, IContext } from './interfaces';
/**
 * Создает экшн-экзекутор с заданным контекстом.
 *
 * @returns Экшн-экзекутор.
 */
export declare const createExecuter: <TContext extends IContext>(context: TContext) => IExecuter<TContext>;
