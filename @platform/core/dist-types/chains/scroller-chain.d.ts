import type { IDomain, PropsPorivder, IWrapChainConfig } from '@platform/tools/istore-react';
import type { ICategory, SORT_DIRECTION, ISortField, IGroupedRow, IScrollerMethodsBase } from '../interfaces';
import { BaseChain } from './base-chain';
export interface IScrollerState {
    rows: any[];
    groups?: Array<IGroupedRow<any>>;
    groupBy?: string;
    isLoading: boolean;
    focusedRow?: any;
    selectedRows?: any[];
    closedGroups?: any[];
    expandedRows?: any[];
    sort?: Record<string, SORT_DIRECTION>;
    category?: string;
    counters?: {
        response: any;
    };
    filterModal?: {
        opened: boolean;
    };
    filters?: Record<string, any>;
    applyedFilters?: Record<string, any>;
}
export interface IScrollerMethods extends IScrollerMethodsBase<any> {
    category: {
        set(category: string): void;
    };
    groupBy: {
        set(groupBy: string): void;
    };
    selectedRows: {
        set(rows: any[]): void;
        toggleAll(): void;
    };
    closedGroups?: {
        set(rows: any[]): void;
        closedAll(): void;
    };
    expandedRows?: {
        set(rows: any[]): void;
    };
    sort: {
        set(sort: Record<string, SORT_DIRECTION>): void;
    };
    filters: {
        [key: string]: (() => boolean) | (() => void) | {
            clear(): void;
        };
        getCanBeApplied(): boolean;
        apply(): void;
        clear(): void;
    };
    filterModal: {
        open(): void;
        close(): void;
    };
    getCountForCategory(category: string): number | undefined;
    getApplyedFilterTags(textLabels: Record<string, string>, filterIsEmpty?: (v: any) => boolean | undefined): Array<{
        value: string;
        label: string;
    }>;
    hasApplyedFilters(): boolean;
}
export interface ITableWrapOptions {
    /**
     * Если передать false то скроллер не будет делать запрос за данными при помещении на форму автоматически, иначе будет.
     */
    autoload?: boolean;
}
export declare class ScrollerChain<TVal, TMCProps, TDomain extends IDomain<any, any>, TScope = {}> extends BaseChain<TMCProps, TDomain, TScope> {
    protected selector: (s: TDomain['state']) => IScrollerState;
    protected methodsSelector: (m: TDomain['methods']) => IScrollerMethods;
    constructor(config: IWrapChainConfig<TDomain>, selector: (s: TDomain['state']) => IScrollerState);
    manager: (tabsOrGetter: ICategory[] | ((s: TDomain['state'], m: TDomain['methods']) => ICategory[])) => ScrollerChain<TVal, {
        onCategoryChange: (v: string) => void;
    } & {
        categories: ICategory[];
    } & {
        category: string | undefined;
    } & TMCProps, TDomain, TScope>;
    table: (options?: ITableWrapOptions) => ScrollerChain<TVal, {
        onIntersecting: () => void;
    } & {
        selectedRows: any[] | undefined;
        expandedRows: any[] | undefined;
        onChangeSelectedRows: (rows: any[]) => void;
        rows: any[];
        onChangeExpandedRows: ((rows: any[]) => void) | undefined;
        isLoading: boolean;
    } & TMCProps, TDomain, TScope>;
    groupedTable: (options?: ITableWrapOptions) => ScrollerChain<TVal, {
        onIntersecting: () => void;
    } & {
        selectedRows: any[] | undefined;
        closedGroups: any[] | undefined;
        expandedRows: any[] | undefined;
        groups: IGroupedRow<any>[] | undefined;
        isLoading: boolean;
        onChangeSelectedRows: (rows: any[]) => void;
        onChangeClosedGroups: ((rows: any[]) => void) | undefined;
        onChangeExpandedRows: ((rows: any[]) => void) | undefined;
    } & TMCProps, TDomain, TScope>;
    withGroupBy: (groupFieldsInput: ISortField[] | ((s: TDomain['state'], m: TDomain['methods']) => ISortField[])) => ScrollerChain<TVal, {
        onChangeGroup: (field: string) => void;
    } & {
        groupFields: ISortField[];
        groupBy: string | undefined;
    } & TMCProps, TDomain, TScope>;
    toolbar: (sortFieldsInput: ISortField[] | ((s: TDomain['state'], m: TDomain['methods']) => ISortField[]), actionsVisibleCount?: number) => ScrollerChain<TVal, {
        actionsVisibleCount: number;
    } & {
        sortBy: string;
        sortFields: ISortField[];
        sortDirection: SORT_DIRECTION;
        onChangeSort: (field: string, direction: SORT_DIRECTION) => void;
    } & {
        checkboxValue: boolean;
        checkboxIndeterminate: boolean;
        onChangeCheckbox: (...args: any[]) => void;
    } & TMCProps, TDomain, TScope>;
    filtersPanel: () => any;
    applyedFiltersPanel: (labels: Record<string, string> | ((s: TDomain['state'], m: TDomain['methods']) => Record<string, string>), filterIsEmpty?: ((v: any) => boolean | undefined) | undefined) => ScrollerChain<TVal, {
        tags: {
            value: string;
            label: string;
        }[];
    } & {
        onClick: () => void;
        onRemoveAllTags: () => void;
        onRemoveTag: (filterTagKey: string) => void;
    } & TMCProps, TDomain, TScope>;
    placeholder: () => ScrollerChain<TVal, {
        useFilter: boolean | undefined;
        onClick: (_?: any) => void;
    } & TMCProps, TDomain, TScope>;
    sortbar: (sortFieldsInput: ISortField[] | ((s: TDomain['state'], m: TDomain['methods']) => ISortField[])) => ScrollerChain<TVal, {
        sortBy: string;
        sortFields: ISortField[];
        sortDirection: SORT_DIRECTION;
        onChangeSort: (field: string, direction: SORT_DIRECTION) => void;
    } & TMCProps, TDomain, TScope>;
    selectbar: () => ScrollerChain<TVal, {
        checkboxValue: boolean;
        checkboxIndeterminate: boolean;
        onChangeCheckbox: (...args: any[]) => void;
    } & TMCProps, TDomain, TScope>;
    withProps<T>(propsGetter: PropsPorivder<TDomain, T, TScope>): ScrollerChain<TVal, T & TMCProps, TDomain, TScope>;
    join<T, TJoinScope>(chain: BaseChain<T, any, TJoinScope>): ScrollerChain<TVal, T & TMCProps, TDomain, TJoinScope & TScope>;
    protected createInstance(config: IWrapChainConfig<TDomain>): ScrollerChain<unknown, unknown, TDomain, {}>;
}
