import type { IDomain, PropsPorivder } from '@platform/tools/istore-react';
import { BaseChain as IStoreBaseChain } from '@platform/tools/istore-react';
import type { IActionInfo } from '../interfaces';
import type { TransfomedAction } from '../utils';
export interface IModalState {
    opened: boolean;
}
export declare abstract class BaseChain<TMCProps, TDomain extends IDomain<any, any>, TScope = {}> extends IStoreBaseChain<TMCProps, TDomain, TScope> {
    modal: (selector: (s: TDomain['state']) => IModalState) => any;
    withActions: (actions: Array<TransfomedAction<IActionInfo<any, any>>> | PropsPorivder<TDomain, Array<TransfomedAction<IActionInfo<any, any>>>, TScope>) => any;
}
