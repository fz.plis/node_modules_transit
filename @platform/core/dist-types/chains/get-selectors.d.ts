/// <reference types="react" />
import type { IDomain, DomainGetter, IExtraLockDomain } from '@platform/tools/istore-react';
import type { ICategory, SORT_DIRECTION, ISortField } from '../interfaces';
import type { IModalState } from './base-chain';
import type { INotificationsContainerState } from './lock-domain';
import type { IScrollerState, ITableWrapOptions, IScrollerMethods } from './scroller-chain';
export interface IScrollerSelector<T extends IDomain<any, any>> {
    (s: T['state']): IScrollerState;
    (m: T['methods'], flag: true): IScrollerMethods;
    load(): Promise<void>;
}
export declare const getSelectors: <T extends IDomain<any, any>, P = {}>(nameOrGetter: string | ((props: P) => string), domainOrGetter: T | DomainGetter<T, P>, extra?: IExtraLockDomain<T, P> | undefined) => {
    call: import("@platform/tools/istore-react").ICallFuncSelector<T["methods"]>;
    bind: <TRet>(getter: (s: T["state"], m: T["methods"]) => TRet) => {
        selector: (context: any) => TRet;
    };
    modal: (selector: (s: T["state"]) => IModalState) => {
        opened: {
            selector: (context: any) => boolean;
        };
        onClose: import("@platform/tools/istore-react").Selector<(...args: any[]) => unknown>;
    };
    forForm: import("./form").IFormSelector<T>;
    notificationsContainer: (selector: (s: T["state"]) => INotificationsContainerState, pushSelector: (m: T["methods"]) => (url: string) => void) => {
        notifications: {
            selector: (context: any) => any[];
        };
        onClose: import("@platform/tools/istore-react").Selector<(...args: any[]) => unknown>;
        onPush: import("@platform/tools/istore-react").Selector<(url: string) => void>;
    };
    forScroller: (sel: (s: T["state"]) => IScrollerState) => {
        manager: (tabsOrGetter: ICategory[] | ((s: T["state"], m: T["methods"]) => ICategory[])) => {
            category: {
                selector: (context: any) => string;
            };
            onCategoryChange: import("@platform/tools/istore-react").Selector<(category: string) => void>;
            categories: {
                selector: (context: any) => ICategory[];
            };
        };
        table: (options?: ITableWrapOptions) => {
            selectedRows: {
                selector: (context: any) => any[] | undefined;
            };
            expandedRows: {
                selector: (context: any) => any[] | undefined;
            };
            onChangeSelectedRows: import("@platform/tools/istore-react").Selector<(rows: any[]) => void>;
            onChangeExpandedRows: import("@platform/tools/istore-react").Selector<(rows: any[]) => void>;
            rows: {
                selector: (context: any) => any[];
            };
            isLoading: {
                selector: (context: any) => boolean;
            };
            onIntersecting: import("@platform/tools/istore-react").Selector<() => void>;
            onInitHook: import("@platform/tools/istore-react").Selector<() => void> | undefined;
        };
        groupedTable: (options?: ITableWrapOptions) => {
            selectedRows: {
                selector: (context: any) => any[] | undefined;
            };
            closedGroups: {
                selector: (context: any) => any[] | undefined;
            };
            expandedRows: {
                selector: (context: any) => any[] | undefined;
            };
            groups: {
                selector: (context: any) => import("../interfaces").IGroupedRow<any>[] | undefined;
            };
            isLoading: {
                selector: (context: any) => boolean;
            };
            onChangeSelectedRows: import("@platform/tools/istore-react").Selector<(rows: any[]) => void>;
            onChangeClosedGroups: import("@platform/tools/istore-react").Selector<(rows: any[]) => any>;
            onChangeExpandedRows: import("@platform/tools/istore-react").Selector<(rows: any[]) => void>;
            onIntersecting: import("@platform/tools/istore-react").Selector<() => void>;
            onInitHook: import("@platform/tools/istore-react").Selector<() => void> | undefined;
        };
        withGroupBy: (groupFieldsInput: ISortField[] | ((s: T["state"], m: T["methods"]) => ISortField[])) => {
            groupFields: {
                selector: (context: any) => ISortField[];
            };
            groupBy: {
                selector: (context: any) => string | undefined;
            };
            onChangeGroup: import("@platform/tools/istore-react").Selector<(groupBy: string) => void>;
        };
        sortBar: (sortFieldsInput: ISortField[] | ((s: T["state"], m: T["methods"]) => ISortField[])) => {
            sortBy: {
                selector: (context: any) => string;
            };
            sortFields: {
                selector: (context: any) => ISortField[];
            };
            sortDirection: {
                selector: (context: any) => any;
            };
            onChangeSort: import("@platform/tools/istore-react").Selector<(field: string, direction: SORT_DIRECTION) => void>;
        };
        selectBar: () => {
            checkboxValue: {
                selector: (context: any) => boolean;
            };
            checkboxIndeterminate: {
                selector: (context: any) => boolean;
            };
            onChangeCheckbox: import("@platform/tools/istore-react").Selector<() => void>;
        };
        toolbar: (sortFieldsInput: ISortField[] | ((s: T["state"], m: T["methods"]) => ISortField[])) => {
            sortBy: {
                selector: (context: any) => string;
            };
            sortFields: {
                selector: (context: any) => ISortField[];
            };
            sortDirection: {
                selector: (context: any) => any;
            };
            onChangeSort: import("@platform/tools/istore-react").Selector<(field: string, direction: SORT_DIRECTION) => void>;
            checkboxValue: {
                selector: (context: any) => boolean;
            };
            checkboxIndeterminate: {
                selector: (context: any) => boolean;
            };
            onChangeCheckbox: import("@platform/tools/istore-react").Selector<() => void>;
        };
        filtersPanel: () => {
            opened: {
                selector: (context: any) => boolean;
            };
            onClose: import("@platform/tools/istore-react").Selector<(...args: any[]) => unknown>;
            okDisabled: {
                selector: (context: any) => boolean;
            };
            onOk: import("@platform/tools/istore-react").Selector<() => void>;
            onClear: import("@platform/tools/istore-react").Selector<() => void>;
        };
        applyedFiltersPanel: (labels: Record<string, string> | ((s: T["state"], m: T["methods"]) => Record<string, string>), filterIsEmpty?: ((v: any) => boolean | undefined) | undefined) => {
            onClick: import("@platform/tools/istore-react").Selector<() => void>;
            onRemoveAllTags: import("@platform/tools/istore-react").Selector<(...args: any[]) => unknown>;
            onRemoveTag: import("@platform/tools/istore-react").Selector<(filterTagKey: string) => void>;
            tags: {
                selector: (context: any) => {
                    value: string;
                    label: string;
                }[];
            };
        };
        placeholder: () => {
            onClick: import("@platform/tools/istore-react").Selector<() => void>;
            useFilter: {
                selector: (context: any) => boolean;
            };
        };
    };
    asRoot: <T_1>(Component: import("react").ComponentType<T_1>) => import("react").ForwardRefExoticComponent<import("react").PropsWithoutRef<import("@platform/tools/istore-react").IRootWrappedComponentProps & T_1> & import("react").RefAttributes<any>>;
    join: import("@platform/tools/istore-react").IJoin;
    useDomain: <TRet_1>(selector: (s: T["state"], m: T["methods"]) => TRet_1) => TRet_1;
};
