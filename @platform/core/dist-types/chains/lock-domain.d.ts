import type { ComponentType } from 'react';
import type { DomainGetter, IDomain, IExtraLockDomain, PropsPorivder, IWrapChainConfig } from '@platform/tools/istore-react';
import type { IRangeButtonsOption } from '../interfaces';
import { BaseChain } from './base-chain';
import { InputChain } from './input-chain';
import type { IScrollerState } from './scroller-chain';
import { ScrollerChain } from './scroller-chain';
export interface IModalState {
    opened: boolean;
}
export interface INotificationsContainerState {
    visible: any[];
}
export declare class MainChain<TMCProps, TDomain extends IDomain<any, any>, TScope = {}> extends BaseChain<TMCProps, TDomain, TScope> {
    input: <TVal>(selector: (s: TDomain['state'], scope: TScope) => TVal, setterSelector?: ((m: TDomain['methods'], scope: TScope) => (v: TVal, p?: any) => void) | undefined) => InputChain<TVal, unknown, TDomain, TScope>;
    scroller: (selector: (s: TDomain['state']) => IScrollerState) => ScrollerChain<unknown, unknown, TDomain, {}>;
    dataRange: (from: (s: TDomain['state']) => string, to: (s: TDomain['state']) => string, rangeButtons?: IRangeButtonsOption[] | null) => MainChain<{
        rangeButtons: IRangeButtonsOption[] | null;
    } & {
        onFromChange: (value: string) => void;
        onToChange: (value: string) => void;
    } & {
        from: string;
        to: string;
    } & TMCProps, TDomain, TScope>;
    modal: (selector: (s: TDomain['state']) => IModalState) => MainChain<{
        opened: boolean;
        onClose: any;
    } & TMCProps, TDomain, TScope>;
    notificationsContainer: (selector: (s: TDomain['state']) => INotificationsContainerState, pushSelector: (m: TDomain['methods']) => (url: string) => void) => MainChain<{
        notifications: any[];
        onClose: any;
        onPush: (url: string) => void;
    } & TMCProps, TDomain, TScope>;
    withProps<T>(propsGetter: PropsPorivder<TDomain, T, TScope>): MainChain<T & TMCProps, TDomain, TScope>;
    join<T, TJoinScope>(chain: MainChain<T, any, TJoinScope>): MainChain<T & TMCProps, TDomain, TJoinScope & TScope>;
    withScope<T>(s: ComponentType<T>): MainChain<TMCProps, TDomain, T & TScope>;
    protected createInstance(config: IWrapChainConfig<TDomain>): MainChain<unknown, TDomain, {}>;
}
export declare const lockDomain: <T extends IDomain<any, any>, P = {}>(nameOrGetter: string | ((props: P) => string), domainOrGetter: T | DomainGetter<T, P>, extra?: IExtraLockDomain<T, P> | undefined) => MainChain<unknown, T, {}>;
