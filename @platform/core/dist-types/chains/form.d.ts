import type { Selector, IDomain } from '@platform/tools/istore-react';
export interface IFieldSelector<TVal> {
    name: string;
    value: Selector<TVal>;
    onChange: Selector<(val: TVal) => void>;
}
export interface IFieldWithValidationSelector {
    errorText: Selector<string>;
    warningText: Selector<string>;
    touched: Selector<boolean>;
    onTouch: Selector<() => void>;
    onValidate: Selector<() => void>;
}
export interface IFieldSelectorGenerator<TDomain extends IDomain<any, any>, T, TAddons = {}> {
    field: ISelectorGenerator<T, TAddons>;
    withAddon<TEtraAddon>(addon: TEtraAddon | ((name: string) => TEtraAddon)): IFieldSelectorGenerator<TDomain, T, TAddons & TEtraAddon>;
    withValidation(validationSelector: (m: TDomain['methods'], s: TDomain['state']) => import('../domains/validation').ValidationMethods): IFieldSelectorGenerator<TDomain, T, IFieldWithValidationSelector & TAddons>;
}
export declare type IFormSelector<TDomain extends IDomain<any, any>> = <T>(selector: (s: TDomain['state']) => T) => IFieldSelectorGenerator<TDomain, T>;
export declare type P<T, TKey> = T extends any[] ? number : TKey;
export declare type V<T, TKey extends keyof T> = T extends any[] ? T[0] : T[TKey];
export interface ICarrySelectorGenerator<T, TAddon> {
    <K1 extends keyof T>(p1: P<T, K1>): ISelectorGenerator<V<T, K1>, TAddon>;
    <K1 extends keyof T, K2 extends keyof V<T, K1>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>): ISelectorGenerator<V<V<T, K1>, K2>, TAddon>;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>): ISelectorGenerator<V<V<V<T, K1>, K2>, K3>, TAddon>;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>, K4 extends keyof V<V<V<T, K1>, K2>, K3>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>, p4: P<V<V<V<T, K1>, K2>, K3>, K4>): ISelectorGenerator<V<V<V<V<T, K1>, K2>, K3>, K4>, TAddon>;
}
export interface ISelectorGenerator<T, TAddon> {
    carry: ICarrySelectorGenerator<T, TAddon>;
    <K1 extends keyof T>(p1: P<T, K1>): IFieldSelector<V<T, K1>> & TAddon;
    <K1 extends keyof T, K2 extends keyof V<T, K1>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>): IFieldSelector<V<V<T, K1>, K2>> & TAddon;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>): IFieldSelector<V<V<V<T, K1>, K2>, K3>> & TAddon;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>, K4 extends keyof V<V<V<T, K1>, K2>, K3>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>, p4: P<V<V<V<T, K1>, K2>, K3>, K4>): IFieldSelector<V<V<V<V<T, K1>, K2>, K3>, K4>> & TAddon;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>, K4 extends keyof V<V<V<T, K1>, K2>, K3>, K5 extends keyof V<V<V<V<T, K1>, K2>, K3>, K4>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>, p4: P<V<V<V<T, K1>, K2>, K3>, K4>, p5?: P<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>): IFieldSelector<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>> & TAddon;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>, K4 extends keyof V<V<V<T, K1>, K2>, K3>, K5 extends keyof V<V<V<V<T, K1>, K2>, K3>, K4>, K6 extends keyof V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>, p4: P<V<V<V<T, K1>, K2>, K3>, K4>, p5?: P<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, p6?: P<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>): IFieldSelector<V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>> & TAddon;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>, K4 extends keyof V<V<V<T, K1>, K2>, K3>, K5 extends keyof V<V<V<V<T, K1>, K2>, K3>, K4>, K6 extends keyof V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K7 extends keyof V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>, p4: P<V<V<V<T, K1>, K2>, K3>, K4>, p5?: P<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, p6?: P<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, p7?: P<V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, K7>): IFieldSelector<V<V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, K7>> & TAddon;
    <K1 extends keyof T, K2 extends keyof V<T, K1>, K3 extends keyof V<V<T, K1>, K2>, K4 extends keyof V<V<V<T, K1>, K2>, K3>, K5 extends keyof V<V<V<V<T, K1>, K2>, K3>, K4>, K6 extends keyof V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K7 extends keyof V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, K8 extends keyof V<V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, K7>>(p1: P<T, K1>, p2: P<V<T, K1>, K2>, p3: P<V<V<T, K1>, K2>, K3>, p4: P<V<V<V<T, K1>, K2>, K3>, K4>, p5?: P<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, p6?: P<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, p7?: P<V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, K7>, p8?: P<V<V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, K7>, K8>): IFieldSelector<V<V<V<V<V<V<V<V<T, K1>, K2>, K3>, K4>, K5>, K6>, K7>, K8>> & TAddon;
}
export declare const getFormSelector: <T extends IDomain<any, any>>(bind: (sel: (s: T["state"], m: T["methods"]) => any) => Selector<any>, call: (sel: (m: any) => (...args: any[]) => any) => Selector<(...args: any[]) => any>) => IFormSelector<T>;
