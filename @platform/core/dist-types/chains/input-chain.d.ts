import type { ComponentType } from 'react';
import type { IDomain, PropsPorivder, IWrapChainConfig } from '@platform/tools/istore-react';
import { BaseChain } from '@platform/tools/istore-react';
import type { IListRequestMetaData, ICollectionResponse } from '../interfaces';
export interface IValidationState {
    errors: Record<string, string[]>;
    warnings: Record<string, string[]>;
    touchedFields: string[];
    touchedItems: string[];
    allTouched: boolean;
}
export declare type OptionsProvider<T> = (options: IListRequestMetaData) => Promise<ICollectionResponse<T>>;
export declare class InputChain<TVal, TMCProps, TDomain extends IDomain<any, any>, TScope = {}> extends BaseChain<TMCProps, TDomain, TScope> {
    protected selector: (s: TDomain['state'], scope: TScope) => TVal;
    protected setterSelector?: ((m: TDomain['methods'], scope: TScope) => (v: TVal) => void) | undefined;
    constructor(config: IWrapChainConfig<TDomain>, selector: (s: TDomain['state'], scope: TScope) => TVal, setterSelector?: ((m: TDomain['methods'], scope: TScope) => (v: TVal) => void) | undefined);
    withOptions: (options: any[] | ((s: TDomain['state'], m: TDomain['methods']) => any[])) => InputChain<TVal, {
        options: any[];
    } & TMCProps, TDomain, TScope>;
    withLookup: <T extends {
        label: string;
        value: any;
    }>(getOptionsProvider: (m: TDomain['methods']) => OptionsProvider<T>, charCount?: number) => InputChain<TVal, {
        remoteData: OptionsProvider<any>;
        charCount: number;
    } & TMCProps, TDomain, TScope>;
    withValidation: (documentSelector: (s: TDomain['state']) => any, validationSelector: (s: TDomain['state'], scope: TScope) => IValidationState) => InputChain<TVal, {
        onTouch: () => void;
        onValidate: () => void;
    } & TMCProps, TDomain, TScope>;
    withProps<T>(propsGetter: PropsPorivder<TDomain, T, TScope>): InputChain<TVal, T & TMCProps, TDomain, TScope>;
    withScope<T>(s: ComponentType<T>): InputChain<TVal, TMCProps, TDomain, T & TScope>;
    join<T>(chain: BaseChain<T, any>): InputChain<TVal, T & TMCProps, TDomain, TScope>;
    protected createInstance(config: IWrapChainConfig<TDomain>): InputChain<TVal, unknown, TDomain, TScope>;
}
