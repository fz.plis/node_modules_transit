import type { Unit } from '@platform/tools/istore';
import type { FilterInfo, OperatorConditions, LogicConditions } from '../interfaces';
export declare const conditions: {
    eq: "eq";
    contains: "contains";
    in: "in";
    ge: "ge";
    le: "le";
    notEq: "notEq";
    and: "and";
    or: "or";
};
export declare type OperatorMetaDataGetter = (name: string, value: any) => FilterInfo;
export declare type LogicMetaDataGetter = (...args: FilterInfo[]) => FilterInfo;
export declare type MetaGetterHelpers = {
    [T in OperatorConditions]: OperatorMetaDataGetter;
} & {
    [T in LogicConditions]: LogicMetaDataGetter;
};
export declare const filterHelpers: MetaGetterHelpers;
export interface IFilterMetadata {
    condition: string;
    fieldName?: string;
}
export interface WithSetter<T> {
    set(v: T): void;
}
export declare const filter: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, metaDataGetter: (value: TVal, externalName: string) => FilterInfo) => Unit<TVal, {
    set: (v: TVal) => void;
    getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
    clear: () => void;
}>;
export declare const customFilter: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, metaGetter: (value: TVal, metaDataGetterHelpers: MetaGetterHelpers) => FilterInfo) => Unit<TVal, {
    set: (v: TVal) => void;
    getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
    clear: () => void;
}>;
export declare const getSimpleFilter: (operator: OperatorConditions) => <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, fieldName?: string | undefined) => Unit<TVal, {
    set: (v: TVal) => void;
    getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
    clear: () => void;
}>;
export declare const filters: {
    eq: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, fieldName?: string | undefined) => Unit<TVal, {
        set: (v: TVal) => void;
        getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
    contains: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, fieldName?: string | undefined) => Unit<TVal, {
        set: (v: TVal) => void;
        getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
    in: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, fieldName?: string | undefined) => Unit<TVal, {
        set: (v: TVal) => void;
        getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
    ge: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, fieldName?: string | undefined) => Unit<TVal, {
        set: (v: TVal) => void;
        getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
    le: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, fieldName?: string | undefined) => Unit<TVal, {
        set: (v: TVal) => void;
        getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
    notEq: <TVal, TMethods extends WithSetter<TVal>>(unit: Unit<TVal, TMethods>, defaultValue: TVal, fieldName?: string | undefined) => Unit<TVal, {
        set: (v: TVal) => void;
        getFilterMetadata: (value: TVal, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
    someContains: <TMethods_1 extends WithSetter<string>>(unit: Unit<string, TMethods_1>, defaultValue: string, firstFieldName: string, secondFieldName: string, ...otherNames: string[]) => Unit<string, {
        set: (v: string) => void;
        getFilterMetadata: (value: string, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
    custom: <TVal_1, TMethods_2 extends WithSetter<TVal_1>>(unit: Unit<TVal_1, TMethods_2>, defaultValue: TVal_1, metaGetter: (value: TVal_1, metaDataGetterHelpers: MetaGetterHelpers) => FilterInfo) => Unit<TVal_1, {
        set: (v: TVal_1) => void;
        getFilterMetadata: (value: TVal_1, externalName: string) => FilterInfo;
        clear: () => void;
    }>;
};
