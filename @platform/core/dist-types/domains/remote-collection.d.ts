export interface IDataLoader<TRow> {
    data: TRow[];
    total: number;
}
export declare const OUTDATED = "OUTDATE";
export declare const remoteCollection: <TRow, T extends (...args: any[]) => Promise<IDataLoader<TRow>>>(loader: T) => import("@platform/tools/istore").Domain<{
    rows: import("@platform/tools/istore").Unit<TRow[], {
        toggleItem: (item: TRow) => void;
        set: (val: TRow[]) => void;
        setItem: (index: number, item: TRow) => void;
        add: (item: TRow | TRow[]) => void;
        remove: (item: TRow | TRow[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<TRow>) => void;
    }>;
    isLoading: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
    error: import("@platform/tools/istore").Unit<any, {
        set: (val: any) => void;
    }>;
    total: import("@platform/tools/istore").Unit<number | null, {
        set: (val: number | null) => void;
    }>;
    count: import("@platform/tools/istore").Unit<number, {
        set: (val: number) => void;
    }>;
}, {
    loadMore: (...args: any[]) => Promise<"OUTDATE" | IDataLoader<TRow> | undefined> | undefined;
    load: (...args: any[]) => Promise<"OUTDATE" | IDataLoader<TRow> | undefined>;
    rows: Readonly<{
        toggleItem: (item: TRow) => void;
        set: (val: TRow[]) => void;
        setItem: (index: number, item: TRow) => void;
        add: (item: TRow | TRow[]) => void;
        remove: (item: TRow | TRow[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<TRow>) => void;
    }>;
    count: Readonly<{
        set: (val: number) => void;
    }>;
}>;
