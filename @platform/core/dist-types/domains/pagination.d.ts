export declare const calcNumberOfPages: (totalElements: number, pageSize: number) => number;
export declare const pagination: () => import("@platform/tools/istore").Domain<{
    page: import("@platform/tools/istore").Unit<number, {
        set: (val: number) => void;
    }>;
    pageSize: import("@platform/tools/istore").Unit<number, {
        set: (val: number) => void;
    }>;
    totalPages: import("@platform/tools/istore").Unit<number, {
        set: (val: number) => void;
    }>;
    totalElements: import("@platform/tools/istore").Unit<number, {
        set: (val: number) => void;
    }>;
}, {
    setPage: (nextPage: number) => void;
    setPageSize: (nextPageSize: number) => void;
    setTotalElements: (nextTotalElements: number) => void;
}>;
