import type { Domain, IBlock, IOnlyMethods } from '@platform/tools/istore';
export declare const entity: <T extends Record<string, IBlock<any, {
    set(v: any): void;
    merge?(v: any): void;
}>>>(item: T) => Domain<T, IOnlyMethods<T> & {
    set(v: any): void;
    merge(v: any): void;
}>;
