import type { ILocalSearchParams } from '../utils/local-search';
export declare const localDictionary: <TRow extends {
    id: string;
}>(data?: TRow[]) => import("@platform/tools/istore").Unit<{
    rows: TRow[];
}, {
    set: (rows: TRow[]) => void;
    search: (term: string, path: string[], options?: ILocalSearchParams | undefined) => TRow[];
    getById: (id: string) => TRow | undefined;
}>;
