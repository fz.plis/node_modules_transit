import type { History, Location } from 'history';
export declare const router: import("@platform/tools/istore").Unit<{
    location: Location;
}, {
    push: {
        (path: string, state?: unknown): void;
        (location: import("history").LocationDescriptor<unknown>): void;
    };
    goBack: () => void;
    goForward: () => void;
    replace: {
        (path: string, state?: unknown): void;
        (location: import("history").LocationDescriptor<unknown>): void;
    };
    go: (n: number) => void;
    getHistoryInstance: () => History<unknown>;
    connectToHistoryInstance: (historyInput: History) => void;
}>;
