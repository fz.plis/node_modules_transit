import type { IValidators, IFiledValidationRules } from '../validation';
export declare const validation: (rulesGetter: () => Promise<IFiledValidationRules[]>, moreValidators?: IValidators) => import("@platform/tools/istore").Domain<{
    errors: import("@platform/tools/istore").Unit<Record<string, string[]>, {
        merge: (updates: Partial<Record<string, string[]>>) => void;
        set: (val: Record<string, string[]>) => void;
    }>;
    warnings: import("@platform/tools/istore").Unit<Record<string, string[]>, {
        merge: (updates: Partial<Record<string, string[]>>) => void;
        set: (val: Record<string, string[]>) => void;
    }>;
    touchedFields: import("@platform/tools/istore").Unit<string[], {
        toggleItem: (item: string) => void;
        set: (val: string[]) => void;
        setItem: (index: number, item: string) => void;
        add: (item: string | string[]) => void;
        remove: (item: string | string[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: string) => void;
    }>;
    touchedItems: import("@platform/tools/istore").Unit<string[], {
        toggleItem: (item: string) => void;
        set: (val: string[]) => void;
        setItem: (index: number, item: string) => void;
        add: (item: string | string[]) => void;
        remove: (item: string | string[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: string) => void;
    }>;
    isLoading: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
    allTouched: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
}, {
    validateAndFocusFirst: (document: any) => boolean;
    validateItemAndFocusFirst: (document: any, prefix: string) => boolean;
    setErrorAndFocus: (name: string, error: string[] | string) => void;
    validate: (document: any, name?: string | undefined) => void;
    validateItem: (document: any, prefix: string) => void;
    isValid: () => boolean;
    itemIsValid: (prefix: string) => boolean;
    touch: (name: string) => void;
    clearErrors: () => void;
    clearErrorsByName: (name: string) => void;
    getFirstNameWithErrors: () => string | undefined;
    getItemFirstNameWithErrors: (path: string) => string | undefined;
    setErrors: (name: string, errs: string[]) => void;
    touchAll: () => void;
    touchItem: (item: string | string[]) => void;
    isTouched: (name: string) => boolean;
    getFirstFieldError: (name: string) => string;
    getFirstFieldWarning: (name: string) => string;
    loadValidationRules: () => Promise<void>;
    focus: (name: string) => void;
}>;
export declare type ValidationMethods = ReturnType<typeof validation>['methods'];
