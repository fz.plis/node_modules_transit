export declare const modal: (opened?: boolean) => import("@platform/tools/istore").Unit<{
    opened: boolean;
}, {
    open: () => void;
    close: () => void;
}>;
