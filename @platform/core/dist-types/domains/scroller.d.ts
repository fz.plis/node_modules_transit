import type { Domain, Unit, IOnlyMethods, IOnlyStates } from '@platform/tools/istore';
import type { SORT_DIRECTION, IGroupedRow, IScrollerMethodsBase, IFilters, FilterInfo, IFilterHandler } from '../interfaces';
export interface IInitialDomain<TRow> {
    rows: Unit<TRow[], {
        set(v: TRow[]): void;
    }>;
    isLoading: Unit<boolean, {
        set(v: boolean): void;
    }>;
    error: Unit<string | undefined, {
        set(v: string | undefined): void;
    }>;
    total: Unit<number | undefined, {}>;
}
export interface IScrollerConfig {
    extenders: any[];
    methodsGetters: any[];
    metadataGetters: any[];
    onLoadCallbacks: any[];
    filterHandler?: IFilterHandler;
}
export interface IFilterMethods<TRow> {
    clear(): Promise<{
        data: TRow[];
    }>;
    apply(): Promise<{
        data: TRow[];
    }>;
    getCanBeApplied(): boolean;
}
export declare const defaultFilterIsEmpty: (val?: any) => boolean | undefined;
export declare type IFiltersStructure = Record<string, Unit<any, {
    clear(): void;
    getFilterMetadata(value: any, externalName: string): FilterInfo;
}>>;
export declare class Scroller<TRow, TDomain extends IInitialDomain<TRow>, TMethods, TMetadata> {
    private config;
    constructor(config: IScrollerConfig);
    withAddon: <ExtraT extends Record<string, any>, TExtraMethods = IOnlyMethods<ExtraT>, TExtraMetadata = {}>(extender: ExtraT, metadataGetter?: ((s: IOnlyStates<ExtraT>, m: IOnlyMethods<ExtraT> & IScrollerMethodsBase<TRow, TMetadata>) => TExtraMetadata) | undefined, methodsGetter?: ((m: IOnlyMethods<ExtraT> & IScrollerMethodsBase<TRow, TMetadata>, s: IOnlyStates<ExtraT & TDomain>) => TExtraMethods) | undefined, onLoad?: ((resp: any, m: IOnlyMethods<ExtraT> & IScrollerMethodsBase<TRow, TMetadata>, s: IOnlyStates<ExtraT & TDomain>) => void) | undefined) => Scroller<TRow, ExtraT & TDomain, TExtraMethods & TMethods, TExtraMetadata & TMetadata>;
    withMetaData<T>(metaDataProvider: () => T): Scroller<TRow, (() => {}) & TDomain, IOnlyMethods<() => {}> & TMethods, T & TMetadata>;
    private getStorageFilterSection;
    private setStorageFilterSection;
    withSort: (initialSort: Record<string, SORT_DIRECTION>) => Scroller<TRow, {
        sort: Unit<Record<string, SORT_DIRECTION>, {
            set: (val: Record<string, SORT_DIRECTION>) => void;
        }>;
    } & TDomain, IOnlyMethods<{
        sort: Unit<Record<string, SORT_DIRECTION>, {
            set: (val: Record<string, SORT_DIRECTION>) => void;
        }>;
    }> & TMethods, {
        sort: Record<string, SORT_DIRECTION>;
    } & TMetadata>;
    withFocusedRow: () => Scroller<TRow, {
        focusedRow: Unit<TRow | null, {
            set: (val: TRow | null) => void;
        }>;
    } & TDomain, IOnlyMethods<{
        focusedRow: Unit<TRow | null, {
            set: (val: TRow | null) => void;
        }>;
    }> & TMethods, {} & TMetadata>;
    withSelectedRows: () => Scroller<TRow, {
        selectedRows: Unit<TRow[], {
            toggleItem: (item: TRow) => void;
            set: (val: TRow[]) => void;
            setItem: (index: number, item: TRow) => void;
            add: (item: TRow | TRow[]) => void;
            remove: (item: TRow | TRow[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<TRow>) => void;
        }>;
    } & TDomain, {
        selectedRows: {
            selectAll: () => void;
            deselectAll: () => void;
            getAllSelected: () => boolean;
            toggleAll: () => void;
            toggleItem: (item: TRow) => void;
            set: (val: TRow[]) => void;
            setItem: (index: number, item: TRow) => void;
            add: (item: TRow | TRow[]) => void;
            remove: (item: TRow | TRow[]) => void;
            removeByIndex: (index: number) => void;
        };
    } & TMethods, {} & TMetadata>;
    withGroups: (initialGroup: string) => Scroller<TRow, {
        groupBy: Unit<string, {
            set: (val: string) => void;
        }>;
        closedGroups: Unit<(string | number)[], {
            toggleItem: (item: string | number) => void;
            set: (val: (string | number)[]) => void;
            setItem: (index: number, item: string | number) => void;
            add: (item: string | number | (string | number)[]) => void;
            remove: (item: string | number | (string | number)[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<string | number>) => void;
        }>;
        groups: Unit<IGroupedRow<TRow>[], {
            toggleItem: (item: IGroupedRow<TRow>) => void;
            set: (val: IGroupedRow<TRow>[]) => void;
            setItem: (index: number, item: IGroupedRow<TRow>) => void;
            add: (item: IGroupedRow<TRow> | IGroupedRow<TRow>[]) => void;
            remove: (item: IGroupedRow<TRow> | IGroupedRow<TRow>[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<IGroupedRow<TRow>>) => void;
        }>;
    } & TDomain, {
        closedGroups: {
            closeAll: () => void;
            openAll: () => void;
            getAllClosed: () => boolean;
            closedAll: () => void;
            toggleItem: (item: string | number) => void;
            set: (val: (string | number)[]) => void;
            setItem: (index: number, item: string | number) => void;
            add: (item: string | number | (string | number)[]) => void;
            remove: (item: string | number | (string | number)[]) => void;
            removeByIndex: (index: number) => void;
        };
        groupBy: Readonly<{
            set: (val: string) => void;
        }>;
    } & TMethods, {
        groupBy: string;
    } & TMetadata>;
    withExpandedRows: () => Scroller<TRow, {
        expandedRows: Unit<TRow[], {
            toggleItem: (item: TRow) => void;
            set: (val: TRow[]) => void;
            setItem: (index: number, item: TRow) => void;
            add: (item: TRow | TRow[]) => void;
            remove: (item: TRow | TRow[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<TRow>) => void;
        }>;
    } & TDomain, {
        expandedRows: Readonly<{
            toggleItem: (item: TRow) => void;
            set: (val: TRow[]) => void;
            setItem: (index: number, item: TRow) => void;
            add: (item: TRow | TRow[]) => void;
            remove: (item: TRow | TRow[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<TRow>) => void;
        }>;
    } & TMethods, {} & TMetadata>;
    withCategory: (initialCategory: string, counterLoader: any) => Scroller<TRow, {
        category: Unit<string, {
            set: (val: string) => void;
        }>;
        counters: Unit<{
            response: never[] | undefined;
            sending: boolean;
            error: undefined;
            count: number;
        }, {
            send: (...loadDataParams: any[]) => Promise<never[]>;
        }>;
    } & TDomain, {
        getCountForCategory: (category: string) => number | undefined;
        category: Readonly<{
            set: (val: string) => void;
        }>;
        counters: Readonly<{
            send: (...loadDataParams: any[]) => Promise<never[]>;
        }>;
        load(): Promise<{
            data: TRow[];
            total: number;
        }>;
        loadMore(): void;
        getMetaData(): TMetadata;
        rows: {
            toggleItem(item: TRow): void;
            set(val: TRow[]): void;
            setItem(index: number, item: TRow): void;
            add(item: TRow | TRow[]): void;
            remove(item: TRow | TRow[]): void;
            removeByIndex(index: number): void;
            mergeToItem(index: number, obj: Partial<TRow>): void;
        };
    } & TMethods, {
        category: string;
    } & TMetadata>;
    withConfigurableColumns: (defaultHiddenColumns: string[]) => Scroller<TRow, {
        hiddenColumnsIds: Unit<string[], {
            toggleItem: (item: string) => void;
            set: (val: string[]) => void;
            setItem: (index: number, item: string) => void;
            add: (item: string | string[]) => void;
            remove: (item: string | string[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: string) => void;
        }>;
    } & TDomain, IOnlyMethods<{
        hiddenColumnsIds: Unit<string[], {
            toggleItem: (item: string) => void;
            set: (val: string[]) => void;
            setItem: (index: number, item: string) => void;
            add: (item: string | string[]) => void;
            remove: (item: string | string[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: string) => void;
        }>;
    }> & IScrollerMethodsBase<TRow, TMetadata> & TMethods, {} & TMetadata>;
    withFilters: <T extends IFiltersStructure>(initialFilters: T) => Scroller<TRow, {
        filters: T;
        filterModal: Unit<{
            opened: boolean;
        }, {
            open: () => void;
            close: () => void;
        }>;
        applyedFilters: Unit<Record<string, any>, {
            set: (val: Record<string, any>) => void;
        }>;
    } & TDomain, {
        filters: import("@platform/tools/istore").MethodsDecorator<IFilterMethods<TRow> & IOnlyMethods<T>, import("../utils").IGetNameDecorator>;
        getApplyedFilterTags: (textLabels: Record<string, string>) => any[];
        hasApplyedFilters: () => boolean;
        filterModal: Readonly<{
            open: () => void;
            close: () => void;
        }>;
    } & TMethods, {
        filters: IFilters;
    } & TMetadata>;
    withQuickFilters: <T extends IFiltersStructure>(quickFilters: T) => Scroller<TRow, {
        quickFilters: T;
    } & TDomain, IOnlyMethods<{
        quickFilters: T;
    }> & TMethods, {
        quickFilter: {};
    } & TMetadata>;
    compose: (loader: (metadata: TMetadata) => Promise<{
        data: TRow[];
        total: number;
    }>, pageSize?: number) => Domain<IInitialDomain<TRow> & TDomain, IScrollerMethodsBase<TRow, TMetadata> & TMethods>;
    private next;
}
export declare const scroller: <TRow>(filterHandler?: IFilterHandler | undefined) => Scroller<TRow, IInitialDomain<TRow>, IScrollerMethodsBase<TRow, {}>, {
    pageSize: number;
    offset: number;
}>;
