export * from './chains';
export * from './utils';
export * from './domains';
export * from './actions';
export * from './validation';
export * from './interfaces';
export declare const scrollerSettings: (storageKey: string) => import("./interfaces").IFilterHandler, clearScrollerSettings: () => void;
export declare const alias = "@platform/core";
