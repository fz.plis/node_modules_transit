export declare const values: (obj: Record<string, any>) => any[];
export declare const getByPath: (obj: any, path: string[] | string) => any;
export { NOT_FOUND } from '@platform/tools/istore';
