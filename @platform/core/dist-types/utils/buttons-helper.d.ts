import type { IExecuter } from '../actions';
import type { IActionInfo, ButtonsConfig } from '../interfaces';
/**
 * Трансформированный экшн, пригодный для использования в качестве пропов для кнопки.
 *
 * @description Этот объект получается после обработки экшн конфига.
 */
export declare type TransfomedAction<T extends IActionInfo<any, any>> = Omit<T, keyof IActionInfo<any, any>> & {
    /** Название кнопки. */
    name: string;
    /**
     * Функция, вызов которой исполнит экшн, который был привязан к изначальному конфигу.
     * Экшн будет исполнен тем экзекутором и с теми параметрами, которые были использованы при трансформации
     * через функцию `getActionButtons`.
     */
    onClick(): Promise<any> | void;
    /** Индикатор того, что валидация экшна гардианами провалена. */
    disabled?: boolean;
};
/**
 * Функция трансформирует переданные экшн конфиги, в конфиги пригодные для кнопок.
 *
 * @param buttonsInfo Массив экшн конфигов.
 * @param executer Экзекутор, которым будут исполнены экшны.
 * @param params Аргументы, которые получит экшн.
 * @returns Массив трансформированных экшнов.
 */
export declare const getActionButtons: <T extends IActionInfo<any, any>>(buttonsInfo: ButtonsConfig<T>, executer: IExecuter<any>, params: any[]) => TransfomedAction<T>[];
/**
 * Функция трансформирует переданный объект с экшн конфигами, в конфиги пригодные для кнопок тулбара скроллера.
 *
 * @param config Объект с экшн конфигами. Ключ объекта - категория скроллера, а значение - массив экшн конфигов для этой категории.
 * @param category Текущая активная категория скроллера.
 * @param rows Выделенные строки.
 * @param executer Экзекутор, которым будут исполнены экшны.
 * @returns Массив трансформированных экшнов.
 */
export declare const getScrollerActionButtons: <T extends IActionInfo<any, any>>(config: Record<string, ButtonsConfig<T>>, category: string, rows: any[], executer: IExecuter<any>) => TransfomedAction<T>[];
/**
 * Функция трансформирует переданный объект с экшн конфигами, в конфиги пригодные для кнопок карточки (или строки) скроллера.
 *
 * @param config Объект с экшн конфигами. Ключ объекта - категория скроллера, а значение - массив экшн конфигов для этой категории.
 * @param category Текущая активная категория скроллера.
 * @param row Данные текущей строки.
 * @param executer Экзекутор, которым будут исполнены экшны.
 * @returns Массив трансформированных экшнов.
 */
export declare const getScrollerRowActionButtons: <T extends IActionInfo<any, any>>(config: Record<string, ButtonsConfig<T>>, category: string, row: Record<string, any>, executer: IExecuter<any>) => TransfomedAction<T>[];
export declare const getFormActionButtons: <T extends IActionInfo<any, any>>(config: ButtonsConfig<T>, executer: IExecuter<any>) => TransfomedAction<T>[];
