export declare const WRONG_TO_ARG_ERROR: string;
export declare const to: <TResult>(promise: Promise<TResult>) => Promise<[TResult | null, any]>;
