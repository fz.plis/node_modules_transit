export declare enum SEARCH_MODE {
    START = "START",
    EVERYWHERE = "EVERYWHERE"
}
export interface ILocalSearchParams {
    mode?: SEARCH_MODE;
    casesensetive?: boolean;
}
export declare const localSearch: <T>(array: T[], expression: string, path: string[], { mode, casesensetive }?: ILocalSearchParams) => T[];
