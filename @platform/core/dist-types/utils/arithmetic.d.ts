import { BigNumber } from '@platform/tools/big-number';
export declare class Arithmetic {
    private value;
    constructor(n: BigNumber | number | string, base?: number);
    toString: () => string;
    toNumber: () => number;
    /**
     * Возвращает строку с n количетвом десятичных знаков.
     * Если в числе знаков < n - дополняте нулями.
     * N: количество знаков после запятой.
     */
    toFixed: (n: number) => string;
    isNaN: () => boolean;
    isEqualTo: (n: Arithmetic | number | string) => boolean;
    plus: (n: Arithmetic | number | string) => Arithmetic;
    minus: (n: Arithmetic | number | string) => Arithmetic;
    dividedBy: (n: Arithmetic | number | string) => Arithmetic;
    multipliedBy: (n: Arithmetic | number | string) => Arithmetic;
    /**
     * Возвращает объект типа Arithmetic с n количетвом знаков десятичных знаков.
     * N: количество знаков после запятой.
     */
    decimalPlaces: (n: number) => Arithmetic;
}
