import type { IStorage, IStorageMethods } from '../interfaces';
export declare const createBaseStorage: (storage: IStorage) => IStorageMethods;
