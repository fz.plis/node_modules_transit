import type { MethodsDecorator } from '@platform/tools/istore';
export interface IGetNameDecorator {
    getName(): string;
}
export declare const decorateWithNameGetter: <T extends Record<string, any>>(methods: T, name?: string) => MethodsDecorator<T, IGetNameDecorator>;
