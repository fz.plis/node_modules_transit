/** Возвращает функцию-обертку над `translate`,которая будет замкнута на определенную область локализации (`area`).
 *
 * @param area Именнованая область локализации, на которую будет замкнута возвращаемая функция.
 *
 * @example
 * ```
 * // Состояние хранилища
 * {
 *   areas: {
 *     test: { 'example': 'тестовые данные' },
 *     test1: { 'example1': 'тестовые данные 1' }
 *   }
 * }
 * ```
 * @example
 * const LOCALIZATION_RESOURCE = 'test1'
 * const translate = getTranslator(LOCALIZATION_RESOURCE);
 * translate('example1') // 'тестовые данные 1'
 */
export declare const getTranslator: (area: string) => (key: string, params?: Record<string, string | number> | undefined) => string;
