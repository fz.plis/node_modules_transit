export interface ISortField {
    name: string;
    label: string;
}
export interface ICategory {
    label: string;
    value: string;
    disabled?: boolean;
    count?: number;
}
export declare enum SORT_DIRECTION {
    ASC = "asc",
    DESC = "desc"
}
export interface ICollectionResponse<TRow> {
    data: TRow[];
    total: number;
}
export interface IGroupedRow<TRow> {
    value: number | string;
    data: TRow;
}
export declare type OperatorConditions = 'contains' | 'eq' | 'ge' | 'in' | 'le' | 'notEq';
export declare type LogicConditions = 'and' | 'or';
export interface OperatorFilterInfo {
    value: any;
    condition: OperatorConditions;
    fieldName: string;
}
export interface LogicFilterInfo {
    value: FilterInfo[];
    condition: LogicConditions;
}
export declare type FilterInfo = LogicFilterInfo | OperatorFilterInfo;
export declare type IFilters = Record<string, FilterInfo>;
