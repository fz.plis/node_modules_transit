export interface IListRequestMetaData {
    offset: number;
    pageSize: number;
    search?: any;
    sort?: any;
}
