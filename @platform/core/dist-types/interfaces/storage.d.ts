import type { SORT_DIRECTION } from '../interfaces';
export interface IFiltersStorageObject {
    sort: Record<string, SORT_DIRECTION>;
    category: string;
    filters: Record<string, any>;
}
export interface IStorage {
    getItem(key: string): IFiltersStorageObject | null;
    removeItem(key: string): void;
    setItem(key: string, value: IFiltersStorageObject): void;
}
export interface IFilterHandler {
    setItem(val: IFiltersStorageObject): void;
    getSection(sectionKey: keyof IFiltersStorageObject): IFiltersStorageObject[keyof IFiltersStorageObject];
    getItem(): IFiltersStorageObject;
}
export declare type IStorageMethods = [(storageKey: string) => IFilterHandler, () => void];
