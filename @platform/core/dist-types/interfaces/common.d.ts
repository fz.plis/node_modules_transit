export interface IDateRangeResult {
    from: string;
    to: string;
}
export interface IRangeButtonsOption {
    title: string;
    getRangeData(): IDateRangeResult;
}
export declare enum NOTIFICATION_TYPE {
    ERROR = "ERROR",
    INFO = "INFO",
    SUCCESS = "SUCCESS",
    WARNING = "WARNING"
}
export interface IAction {
    disabled?: boolean;
    name: string;
    action(name: string): void;
}
export interface IScrollerMethodsBase<TRow, TMetadata = Record<string, any>> {
    load(): Promise<{
        data: TRow[];
        total: number;
    }>;
    loadMore(): void;
    getMetaData(): TMetadata;
    rows: {
        toggleItem(item: TRow): void;
        set(val: TRow[]): void;
        setItem(index: number, item: TRow): void;
        add(item: TRow | TRow[]): void;
        remove(item: TRow | TRow[]): void;
        removeByIndex(index: number): void;
        mergeToItem(index: number, obj: Partial<TRow>): void;
    };
}
/** Аддоны для экшона, которые являются флагом для выполнения или отсутствия определенного функционала. */
export declare enum ACTION_ADDONS {
    /** При данном флаге предотвращается выполнение перезагрузки скроллера после выполнения экшона. */
    DONT_RELOAD = "DONT_RELOAD",
    /** Флаг для индикации групповых действий, необходим при использовании скроллеров по нескольким заявкам. */
    GROUP_ACTION = "GROUP_ACTION"
}
