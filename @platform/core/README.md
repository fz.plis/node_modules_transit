# core
Библиотека с общими инструментами/доменами/цепочками для функциональных стримов.

<!-- toc -->

- [API](#api)
  * [Инструменты по работе с действиями](#%D0%B8%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B5-%D1%81-%D0%B4%D0%B5%D0%B9%D1%81%D1%82%D0%B2%D0%B8%D1%8F%D0%BC%D0%B8)
    + [ActionConfig](#actionconfig)
    + [executer](#executer)
    + [createExecuter(context)](#createexecutercontext)
    + [guardians](#guardians)
      - [oneDefinedArg](#onedefinedarg)
      - [singleAction](#singleaction)
      - [groupAction](#groupaction)
      - [inStatuses(statuses, statusGetter)](#instatusesstatuses-statusgetter)
      - [not(predicate, errorMessage)](#notpredicate-errormessage))
    + [middlewares](#middlewares)
      - [loadingMiddleware(loaderSetter)(executer)](#loadingmiddlewareloadersetterexecuter)
      - [onSuccessMiddleware(callback)(executer)](#onsuccessmiddlewarecallbackexecuter)
      - [argsMiddleware(argsProvider)(executer)](#argsmiddlewareargsproviderexecuter)
    + [функция applyMiddlewares(...middlewares)(executer)](#%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D1%8F-applymiddlewaresmiddlewaresexecuter)
  * [функция scroller](#%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D1%8F-scroller)
    + [.withAddon(modelExtender, metaDataGetter?, methodsGetter?, onLoadGetter?)](#withaddonmodelextender-metadatagetter-methodsgetter-onloadgetter)
    + [.withMetaData(metaDataGetter)](#withmetadatametadatagetter)
    + [.withCategory(initialCategory, countsLoader)](#withcategoryinitialcategory-countsloader)
    + [.withPagination()](#withpagination)
    + [.withSort(initialSort)](#withsortinitialsort)
    + [.withFilters(filters)](#withfiltersfilters)
    + [.withFocusedRow()](#withfocusedrow)
    + [.withSelectedRows()](#withselectedrows)
    + [.withConfigurableColumns(defaultHiddenColumnsIds)](#withconfigurablecolumnsdefaulthiddencolumnsids)
    + [.compose(loader)](#composeloader)
    + [функция filter(type)](#%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D1%8F-filtertype)
    + [объект с предзаданными фильтрами filters](#%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82-%D1%81-%D0%BF%D1%80%D0%B5%D0%B4%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%BC%D0%B8-%D1%84%D0%B8%D0%BB%D1%8C%D1%82%D1%80%D0%B0%D0%BC%D0%B8-filters)
  * [Цепочки](#%D1%86%D0%B5%D0%BF%D0%BE%D1%87%D0%BA%D0%B8)
    + [lockDomain(prefixProvider, domainProvider, extra)](#lockdomainprefixprovider-domainprovider-extra)
    + [MainChain](#mainchain)
    + [InputChain](#inputchain)
    + [FormChain](#formchain)
  * [Домены](#%D0%B4%D0%BE%D0%BC%D0%B5%D0%BD%D1%8B)
    + [remoteCollection](#remotecollection)
    + [pagination](#pagination)
    + [validation](#validation)
  * [хелперы/инструменты](#%D1%85%D0%B5%D0%BB%D0%BF%D0%B5%D1%80%D1%8B%D0%B8%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B)
    + [formatDate(strWithDate) **removed**](#formatdatestrwithdate-removed)
    + [formatAccountCode(codeStr) **removed**](#formataccountcodecodestr-removed)
    + [formatMoney(amount) **removed**](#formatmoneyamount-removed)
    + [to(promise)](#topromise)
  * [Декораторы](#%D0%B4%D0%B5%D0%BA%D0%BE%D1%80%D0%B0%D1%82%D0%BE%D1%80%D1%8B)
    + [decorateWithNameGetter(objToDecorate)](#decoratewithnamegetterobjtodecorate)

<!-- tocstop -->

# API

## Инструменты по работе с действиями
Для работы со сложными асинхронными действиями (например действия над документами) был разработан гибкий и обособленный набор инструментов. Данные инструменты позволяют быстро и не задумываюясь об ui состоялвяющей описать любые асинхронные действия состоящие из набора более мелких асинхронных действий. Инструмены по работые с действиями ни как не связаны ни с одним из используемых фреймворков (istore, redux, react) и полностью независимы и не имеют внешних зависимостей.
Основные термины:
* **ActionConfig** - объект определенного вида, описывающий какое-либо действие (состоит из функции с самим действием, guardians и др),
* **guardian** - функция, проверяющая может ли быть вызыван какое-либо действие,
* **executer** - объект с функциями для валидации .validate(action, ...args) и исполнения действия .execute(action, ...args)
* **middleware** - функция для расширения функциональности executer.

Далее рассмотрим каждый термин отдельно и дадим ему подробное описание с примерами.

### ActionConfig
Объект вида:
```javascript
{
  action: ({ addFailed, addSucceeded, done, fatal, execute }: Object, context: Object) => (...args: any[]) => Promise<any>;
  succeededHandler?: (context: Object, succeededResults: any[]) => void;
  failedHandler?: (context: Object, failedResults: any[]) => void;
  fatalHandler?: (context: Object, result: Object) => void;
  guardians?: Guardian[],
}
```

Рассмотрим каждое свойство объекта подробно:
* **action** - функция, принимающая служебные функции для сохранения результатов выполнения действия (будут описаны ниже) и возвращающая фукнция, в которой описано тело действия и принимающую некие параметры. Функция описывающая тело действия должна возвращать промис.
* **succeededHandler** - функция для отображения каких-либо нотификаций пользовател для успешных результатов выполнения действия. Принимает первым параметром контекс выполнения действия, а вторым массив с успешными результатами выполнения действия,
* **failedHandler** - функция для отображения каких-либо нотификаций пользовател для неуспешных результатов выполнения действия. Принимает первым параметром контекс выполнения действия, а вторым массив с неуспешными результатами выполнения действия,
* **fatalHandler** - функция отображения каких-либо нотификаций пользователю при фатальной ошибки при выолнении действия. Принимает первым параметром контекс выполнения действия, а вторым массив с неуспешными результатами выполнения действия,
* **guardians** - массив guardians для данного действия.

Теперь подробно рассмотрим то как нужно писать действия и как они работают.
При написании действий нужно принимать во внимание следующее:
* Действие должно быть асинхронным, т.е. возвращать промис!
* Результатом действия могут быть одновременно один или несколько успешных результатов, один или несколько не успешных результатов и фатальная ошибка выполнения. Так же могут быть только успешные или неуспешные результаты или только фатальная ошибка, или результатов может не быть вообще. Т.е. комбинация результатов может быть любая и к этому надо быть готовым при обработке результатов действия. Это нужно для выполнения групповых действий из скроллера документов (например **отправить**), в то же время групповые операции можно использовать для выполнения действия с одним документоа (из формы редактирования документа), а вот наоборот сделать уже не получится.
* Для получения внешних данных, необходимых для выолнения действия или для отправки данных на сервер (выполмнения запросов на сервер, запрос данных у пользователя через ui и т.д.), используется context, который приходит вторым параметром в функция **action** из конфига действия. Context предоставляется executer'ом и тип контекта действия и тип контекста executer'a должны совпадать. В контексте должны содержаться все внешние взаимодействия с системой
* Для фиксирования результатов выолнения действия нужно использовать служебные функции приходящие в функиця *action* из конфига действия:
  *  **addSucceeded [(...results: any[]) => void]** - функция для добавления успешных результатов. Ее можно вызывать сколько угодно раз по мере выполнения действия и передавать ей сколько угодно аргументов (каждый аргумент будет добавлен как успешный результат). Если в результате выполнения действия был хотя бы 1 успешный результат, то после завершения выполнения дейстия (даже в случае фатальной ошибки) будет вызван **succeededHandler** из конфига действия.
  *  **addFailed [(...results: any[]) => void]** - функция для добавления не успешных результатов. Ее можно вызывать сколько угодно раз по мере выполнения действия и передавать ей сколько угодно аргументов (каждый аргумент будет добавлен как не успешный результат). Если в результате выполнения действия был хотя бы 1 не успешный результат, то после завершения выполнения дейстия (даже в случае фатальной ошибки) будет вызван **failedHandler** из конфига действия.
  * **done [() => void]** - функция для завершения действия, обязательно должна быть вызвана внутри действия когда действие завершено и не произошло фатальной ошибки. Можно вызвать только один раз, а код расположенный за вызовом данной функции не будет выполняться.
  * **fatal [(error?: any) => void]** - функция для завершения действия с фатальной ошибкой. Если вызвать эту функция без параметров либо с одним параметром в одном из значений - **null, undefined, '', false, 0** то выполнения действия продолжится и фактически такие случаи игнорируются и не приводят к фатальной ошибке (это нужно для уменьшения кол-ва кода). Если вызвать эту функция с параметром отличным от вышеперечисленных то выполнение действия будет остановлено а в релутьтат выполнения действия в поле **error** будет помещена переданная ошибка. Все релультаты накопленные до фатальной ошибки останутся не тронутыми и будут доступны в релутьтатах выполнения действия. Если выполнение действия закончилось фатальной ошибкой то будет вызван **fatalHandler** из конфига действия.
  * **execute [(action: ActionConfig, ...args: any[]) => Promise<ActionResult>]** - функция для выполнения действия внутри действия. Может быть использована для выполнения комбинированных действия, напрмиер **подписать и отправить**.

Рассмотрим простое действие "отпарвить" для группы документов на примере:
```javascript
import { to } from 'utils';

const send = {
 // описываем действие, вынимаем функции для фиксирования результата выполнения действия из первого параметра функции, из второго (это context) вынимаем сервис по работе с бэкендом
 action: ({ addSucceeded, addFailed, done, fatal }, { service }) =>
 // описываем тело действия в виде функции, в данную функцию буду приходить параметры которые буду переданы при выполнении действи, в данном случае это массив документов для отправки
 async (documents) => {
    const [result, err] = await to(service.send(documents)); // для отправки документов используем действие send из сервиса. Для более удобной обработки результатов выполнения асинхронной операции используем функцию to

    fatal(err); // если в err будет ошика то действие упадет с фатальной ошибкой, а если в err будет null то выполнение действия продолжится

    addSucceeded(...result.succeeded); // сохраняем все успешно отправленные документы в успешные результаты выполнения действия
    addFailed(...result.failed);  // сохраняем все не отправленные документы в не успешные результаты выполнения действия
    done(); // завершаем действие
  },
  succeededHandler: ({ alert }, succeeded) => alert('Успешные:' + succeeded.map(x => x.id).join(',')), // отображаем пользователю информация об успешно отправленных документах, используя функцию alert из контекста (это просто пример, в реальности все будет по другому)
  failedHandler: ({ alert }, failed) => alert('Не успешные:' + failed.map(x => x.id).join(',')), // отображаем пользователю информация об не отправленных документах, используя функцию alert из контекста (это просто пример, в реальности все будет по другому)
  fatalHandler: ({ alert }, { error }) => alert('Произошла фатальная ошибка:' + error), // отображаем пользователю информация о фатальной ошбике используя функцию alert из контекста (это просто пример, в реальности все будет по другому)
  guardians: [ // описываем при каким условиям может быть выполнено действие
    groupAction, // для группы документов (1 или больше)
    inStatuses([ STATUSES.SIGNED ]), // только если все документы в определенных статусах
    // это реально существующие guardians, про которые можно пичитать в разделе guardians
  ],
}

```

Как можно увидеть из примера, действие лишь описывает над какими данными оно может быть выолнено, куда эти данные нужно оправить, и как отобразить результаты действия пользователю. При этом логика отправки данных на бэкенд описывается в сервисе, а дополнительная обработка результата действия выполняется тем кто это действие вызвал.

### executer
Объект имеющие следующие свойства:
* **validate [(action: ActionConfig, ...args: any[]) => undefined | Error]** - функция для валидации аргументов действия, принимает первым параметром конфигурацию действия, а далее аргументы с которыми планируется вызывать действие. Данная функция вызвает все guardians из конфига действия передавая им аргумента действия, если хотя не один guardian не вернет ошибок то функция вернет **undefined**, иначе вернется первая ошибка.
* **execute [(action: ActionConfig, ...args: any[]) => Promise<ActionResult>]** - функция для выполнения действия. Принимает конфиг действия первым параметром и аргументы для выполнения действия в виде остальных параметров. Возвращает промис, которые при любом результате выполнения действия будет разрешен. Т.е. в обработчике catch для возращенного промиса нет ни какой необходимости, он ни когда будет будет вызван. Результатом разрения промиса будет объект с результатами выполнения действия:
  * **error [any]** - содержит фатальную ошибку выполнения действия, может быть undefined если фатальной ошибки не произошло,
  * **succeeded [any[]]** - массив успешных результатов выполнения действия.
  * **failed [any[]]** - массив не успешных результатов выполнения действия,
  * **actionConfig [ActionConfig]** - конфиг выполненого действия
  * **context [Object]** - контекс executer'а
  * **args: [any[]]** - массив с аргументами с которыми было вызвано действие.
  * **context [Object]** - контекст executer'а. 

**Пример**
```javascript
import { ruPaymentExecuter } from 'executers';
import { send } from 'actions';


ruPaymentExecuter.execute(send, [<some documents>]) // выполняем действие
  .then((result) => { // после выполнения действия получаем результаты его выполнения
    if (result.succeeded.length) { // если есть успешные резульататы можно сделать что-либо, например перезагрузить скроллер документов
      // do something
    }
  });

```


### createExecuter(context)

Функция для создания executer, принимает объект, который будет использоваться как контекст у возращаемого executer. 

**Пример**
```javascript
import { ruPaymentService } from 'services';

const ruPaymentExecuterContext = {
  service: ruPaymentService,
  doSometging: (data) => new Promise((resolve, reject) => {
    // some implementation
  })
}

export const ruPaymentExecuter = createExecuter(ruPaymentExecuterContext);
```

### guardians

#### oneDefinedArg
Не возвращает ошибок если их кол-во аргментов = 1 и первый аргумент не undefined.

**Пример**
```javascript
const actionConfig = {
  action: (result, context) => ...,
  guardians: [oneDefinedArg],
};

executer.validate(actionConfig, 1, 2, 3); // вернет ошибку так как аргументов больше чем 1
executer.validate(actionConfig); // вернет ошибку так как нет аргументов
executer.validate(actionConfig, 1); // валидно, вернет undefined так как есть один аргумент не равный undefined
```

#### singleAction
Guardian который контролирует что в первый аргумент является либо экземпляром какого-либо объекта (но не null) либо массивом с одним элементом который является экземпляром какого-либо объекта (но не null).

**Пример**
```javascript
const actionConfig = {
  action: (result, context) => ...,
  guardians: [singleAction],
};

executer.validate(actionConfig, 1); // вернет ошибку так аргумент не объект
executer.validate(actionConfig); // вернет ошибку так как нет аргументов
executer.validate(actionConfig, null); // вернет ошибку так как аргумент null
executer.validate(actionConfig, { id: 10 }); // валидно, вернет undefined так как первый аргумент является объектом

executer.validate(actionConfig, [{ id: 10 }]); // валидно, вернет undefined так как первый аргумент является массивом с одним объектом

executer.validate(actionConfig, [{ id: 10 }, { id: 2}]); // вернет ошибку так как в массиве больше 1 элемента
```

#### groupAction
Guardian который контролирует что в первый аргумент является массивом с одним или более элементами.

**Пример**
```javascript
const actionConfig = {
  action: (result, context) => ...,
  guardians: [groupAction],
};

executer.validate(actionConfig, [{ id: 10 }]); // валидно, вернет undefined так как первый аргумент является массивом c элементом

executer.validate(actionConfig, [{ id: 10 }, { id: 2}]); // валидно, вернет undefined так как первый аргумент является массивом с объектами внутри

executer.validate(actionConfig, []); // вернет ошибку так как массив пуст
```

#### inStatuses(statuses, statusGetter)
Функция которая принимает массив с допустимыми статусами и возвращает Guardian который контролирует что первый аргумент является массивом с документами или документом статусы которых включаются в массив с допустимым статусами. Второй аргумент фунуции inStatuses является опциональным и является функцией которая возращает статус для экзепляра документа. По дефолту статус берётся из поля ***status***


**Пример**
```javascript
const statusGetter = d => d.status;

const actionConfig = {
  action: (result, context) => ...,
  guardians: [inStatuses(['NEW', 'DRAFT'], statusGetter)],
};

const doc = { status: 'NEW' };
const doc2 = { status: 'SIGNED' };

executer.validate(actionConfig, doc); // валидно, вернет undefined так как первый аргумент является документом в допустимом статусе

executer.validate(actionConfig, [doc]); // валидно, вернет undefined так как первый аргумент является массивом с документом в допустимом статусе

executer.validate(actionConfig, doc2); // вернет ошибку так документ находится в недопустимом статусе
```

#### not(predicate, errorMessage)
Функция, которая принимает предикат и сообщение об ошибке, и возвращает Guardian. Если в гардиан передан массив, то predicate будет применяться к каждому элементу массива. ***Если в каком-то из вызовов предикат вернёт true, то гардиан выбросит исключение, иначе вернёт undefined.***


**Пример**
```javascript
const isString = s => typeof s === string;

const isNotStringGuardian = not(isString)

const sendAction = {
  ...send,
  guardians: [...send.guardians, isNotStringGuardian]
}

executer.validate(sendAction, "string") // невалидно, вернёт ошибку

executer.validate(sendAction, [5, "string"]) // невалидно, вернёт ошибку

executer.validate(sendAction, [5]) // валидно, вернёт undefined
```

### middlewares

#### loadingMiddleware(loaderSetter)(executer)
Функция которая возвращает функцию-middleware. Первым аргументом является функция которая будет вызвана с аргументом = true когда executer который будет обернут в функцию-middleware начнет выполнять какое-либо действие. loaderSetter будет вызван повторно с аргументом = false когда выполняемое действие завершится. Можно использовать для отображения глобального лоадера на страницах при выполнении действий.

**Пример**
```javascript

let loading = false;
const executerWithLoading = loadingMiddleware((val) => {
  loading = val;
})(executer);

executerWithLoading.execute(someAction).then(() => {
  console.log(loading); // false
});

console.log(loading); // true
```

#### onSuccessMiddleware(callback)(executer)
Функция которая возвращает функцию-middleware. Первым аргументом является функция которая будет вызвана если в результате выполнения действия есть хотя бы одни успешный результат. Первым аргументом в callback будет переданы все результаты выполнения действия (ActionResult)

**Пример**
```javascript

const executerWithOnSuccessCallback = onSuccessMiddleware((result) => {
  console.log(result.succeeded.length > 0); // true
})(executer);
```

#### argsMiddleware(argsProvider)(executer)
Функция которая возвращает функцию-middleware. Первым аргументом является функция которая должна вернуть массив с аргументами которые будут переданы в функцию execute и validate в качестве аргументов действия если функция execute или validate обернутого executer'а будет вызвана без параметров. В случае если функция execute или validate обернутого executer'а будет вызвана с параметрами, то они и будут использоваться.

**Пример**
```javascript

const executerWithArgs = argsMiddleware(() => [1, 2, 3])(executer);

executerWithArgs.execute(someAction).then((result) => {
  console.log(result.args); // [1, 2, 3]
});

executerWithArgs.execute(someAction, 'test').then((result) => {
  console.log(result.args); // ['test']
});
```


### функция applyMiddlewares(...middlewares)(executer)

Функиця для наложения нескольких middleware для executer'а. В качестве аргументов принимает любое кол-во middleware. Возвращает функцию которя принимает executer и возвращает executer к которому применены все middleware'ы.

**Пример**
```javascript

let loading = false;

const nextExecuter = applyMiddlewares(
  argsMiddleware(() => [1,2,3]),
  onSuccessMiddleware((result) => {
    console.log(result.succeeded.length > 0); // true
  },
  loadingMiddleware((val) => {
    loading = val;
  }
)(executer);
```

## функция scroller

В пакете есть функция scroller, которая позволяет формировать домен для работы со скроллерами.

По сути данная функция является началом цепопочки, каждый метод их которой возвращает новую цепочку.
Простейший вид цепочки выглядит так: scroller().compose(loader), где compose является замыкающей функцией которая возвращает домен сформированый цепчокой.
В простешем случае **compose** вернет домен типа **remoteCollection**. Так-же функция **compose** принимает **loader**, это функция, которая возвращает
промис, который должен вернуть объект вида **{ data: Row[] }**, в котором **data** это массив строк скроллера. В **loader** будут переданы метаданные скроллера, которых в простейшем случае нет. Если коротко то тут надо понять 2 вещи, **compose** возвращает **домен** и принимает функцию, которая ожидает **метаданные** скроллера.
Предлагаю использовать вот такую нотация для описания состояния, метаданных и методов скролллера:
```javascript
state: {
    [name]: any,
}

methods: {
    [name]: any
}

metadata: {
   [name]: any
}
```
Т.е. для простейшего скроллера мы получим:

```javascript
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
}

methods: {
    load: (...args: anyp[]) => Promise<any>, 
}

metadata: {
}
```


А теперь рассмотрим методы цепочки, которые добавляют юниты или другие домены в модель а так-же метаданные для запроса:


### .withAddon(modelExtender, metaDataGetter?, methodsGetter?, onLoadGetter?)

Метод для расширения модели домена и добавления метаданных, используется всеми последующими методами цепочки, и позволяет расширить функциональность скроллера.

**Параметры**
* **modelExtender [optional, Object]** - объект с моделью для расширения базовой модели домена скроллера,
* **metaDataGetter [optional, (state: TState, methods: TMethods) => object]** - функция, которая принимает первым параметром состояние домена, вторым методы домена и возвращающая дополнительный набор метаданных в виде объекта. Вызывается данная функция каждый раз перед вызовом **loader**.
* **methodsGetter [optional, (methods: TMethods, state: TState) => TNextMethods] - функция описывающая как аддон влияет на методы домена скроллера. Принимает текущие методы домена скроллера и его состояние, возвращает новый набор методов, которые будут доступны для домена скроллера.
* **onLoadGetter [optional, (response: object, methods: TMethods, state: TState) => void]** - функция принимающая очередной ответ с сервера, методы домена скроллера, состояние домена скроллера. Можно использовать для совершения каких либо действий после получения новых данных с сервера.


**Пример:**
```javascript
const dataScroller = scroller()
    .withAddon(
      { pagination: pagination() },
      (s) => ({page: s.pagination.page, pageSize: s.pagination.pageSize }),
      (m) => m,
      (resp, m) => m.pagination.setTotalElements(resp.total),
    .compose(loader)


// Состояние скроллера после применения вышеописанного аддона:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    pagination: {
      page: number,
      totalPages: number,
      pageSize: number,
      totalElements: number,
    }
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    pagination: {
      setPage: (page: number) => void,
      setPageSize: (pageSize: number) => void,
      setTotalElements: (totalElements: number) => void
    }
}

metadata: {
    page: number,
    pageSize: number
}
```

**Замечание:** все дальнейшие методы цепочки являются обертками над методом .withAddon(), скроме метода .compose(), при этом каждый из методов, кроме .withAddon() можно вызывать только один раз.

### .withMetaData(metaDataGetter)
Метод для добавления метаданных к скроллеру.

**Параметры**
* **metaDataGetter [() => object]** - функция, которая возвращающая дополнительный набор метаданных в виде объекта.

**Пример:**
```javascript
const dataScroller = scroller()
    .withMetaData(() => ({
        fields: 'id,name,date' 
    }))
    .compose(loader);

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
}

methods: {
    load: (...args: anyp[]) => Promise<any>, 
}

metadata: {
    fields: string
}
```


### .withCategory(initialCategory, countsLoader)

Метод для добавленения в скроллер информации о выбранной категории и кол-ва документов в той или иной категории. Если в скроллере есть пагинация, то при изменении значения категории пагинации будет назначена 1 страница.

**Параметры**
* **initialCategory [required, string]** - категория выбранная по умолчанию,
* **countsLoader [required, () => Promise<object[]>]** - функция фозвращающая промис, который возвращает данные описывающие кол-во документов в той или иной категории. Формат ответа ни как не регламенитирован, но ожидается что это будет массив объектов.


**Пример:**
```javascript
const categories = [
  {
    id: "ALL",
    label: "Все"
  },
  {
    id: 'SIGNED',
    label: "Подписанные"
  }
];

const countsLoader = () => fetch('api/counts'); // возвращает массив объектов

const dataScroller = scroller()
    .withCategory(categories[0].id, countsLoader)
    .compose(loader)

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    category: string,
    counters: {
      rows: any[],
      isLoading: boolean,
      error: any,
    }
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    category: {
      set: (category: string) => void
    },
    counters: {
      load: (...args: anyp[]) => Promise<any>,
    }
}

metadata: {
    category: string
}
```

### .withPagination()

Метод для добавленения в скроллер пагинации и метаданных пагинации. Ожидается что при использовании этого метода loader будет возвращать по мимо массива data еще и поле **total**, в котором будет хранится общее кол-во элементов в скроллере. Т.е.: 
```javascript
{
  data: [...],
  total: 100
}
```

**Пример:**
```javascript
const dataScroller = scroller()
    .withPagination()
    .compose(loader)

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    pagination: {
      page: number,
      totalPages: number,
      pageSize: number,
      totalElements: number,
    }
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    pagination: {
      setPage: (page: number) => void,
      setPageSize: (pageSize: number) => void,
      setTotalElements: (totalElements: number) => void
    }
}

metadata: {
    page: number,
    pageSize: number
}

```

### .withSort(initialSort)

Метод для добавленения в скроллер информации о сортировке.

**Параметры**
* **initialSort [required, { [columnName]: string }]** - изначальное состояние сортировки

**Пример:**
```javascript
const dataScroller = scroller()
    .withSort({ date: 'asc' })
    .compose(loader)

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    sort: { [columnName]: string },
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    sort: {
      set: (sort: { [columnName]: string }) => void,
    }
}

metadata: {
  sort: { [columnName]: string }
}
```

### .withFilters(filters)

Метод для добавленения в скроллер информации о фильтрах и методы для их применения. При этом при применении или очистке фильтров (методы apply и clear) автоматичеси будет вызван loader, т.е. уйдет запрос за новыми данными. Если в скроллере есть пагинация, то при применении или очистке фильров пагинации будет назначена 1 страница.

**Параметры**
* **filters [required, { [filterName]: FilterUnit } ]** - объект с фильтрами скроллера.

**Пример:**
```javascript
const dataScroller = scroller()
    .withFilters({
      dataFrom: filters.ge(value()),
      dateTo: filters.le(value()),
    })
    .compose(loader)

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    applyedFilters: Object,
    filters: {
      dataFrom: string,
      dateTo: string,
    },
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    applyedFilters: {
      set: (filters: Object) => void,
    },
    filters: {
      apply: () => void,
      clear: () => void,
      getCanBeApplied: () => boolean,
      dataFrom: {
        set: (value: string) => void,
        getFilterMetadata: () => Object,
        clear: () => void
      },
      dataTo: {
        set: (value: string) => void,
        getFilterMetadata: () => Object,
        clear: () => void
      }
    }
}

metaData: {
  filters: { [filterName]: FilterInfo } // в данном объекте будут находиться все примененные фильтры, FilterInfo будет содержать value, fieldName, condition
}

```

### .withFocusedRow()

Метод для добавленения в скроллер информации об активной строке. На данный момент активная строка автоматически очищается при получнеии новых данных скроллером, т.е. после вызова loader.

**Пример:**
```javascript
const dataScroller = scroller()
    .withFocusedRow()
    .compose(loader)

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    focusedRow: undefined | Object,
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    focusedRow: {
      set: (row: Object) => void,
    }
}

metaData: {}
```


### .withSelectedRows()

Метод для добавленения в скроллер информации об выбранных строках (с помощью чекбоксов). На данный момент все выбранные строки автоматически очищаются при получнеии новых данных скроллером, т.е. после вызова loader.

**Пример:**
```javascript
const dataScroller = scroller()
    .withSelectedRows()
    .compose(loader)

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    selectedRows: Object[],
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    selectedRows: {
      set: (val: Object[]) => void,
      setItem: (index: number, item: Object) => void,
      add: (item: Object | Object[]) => void,
      remove: (item: Object | Object[]) => void,
      removeByIndex: (index: number) => void,
      selectAll: () => void,
      deselectAll: () => void,
      getAllSelected: () => boolean,
      toggleAll: () => void,
    }
}

metaData: {}
```

### .withConfigurableColumns(defaultHiddenColumnsIds)

Метод для добавленения в скроллер информации о скрытых колонках скроллера.

**Параметры**
* **defaultHiddenColumnsIds [required, string[]]** - изначальный набор id колонок, которые не видимы в скроллере.

**Пример:**
```javascript
const dataScroller = scroller()
    .withConfigurableColumns(['name', 'description'])
    .compose(loader)

// Состояние скроллера:
state: {
    rows: any[],
    isLoading: boolean,
    error: any,
    hiddenColumnsIds: string[],
}

methods: {
    load: (...args: anyp[]) => Promise<any>,
    hiddenColumnsIds: {
      set: (string[]: Object) => void,
    }
}

metaData: {}
```

### .compose(loader)

Замыкающий метод цепочки, принимает лоадер данных и возвращает итоговый домен скроллера.

**Параметры**
* **loader [required, (metaData: object) => Promise]** - функция возвращающая промис, который возвращает данные для скроллера в зависимости от входного объект с метаданными (параметр metaData) скроллера.

**Пример:** 
```javascript
const loader = (metadata) => fetch('/api/data' + parseMetaData(metadata)) // возвращает объект { data: Row[], total: number};
const counterLoader = () => fetch('api/counts');

const dataScroller = scroller()
    .withFocusedRow()
    .withSort({date: SORTING.ORDER_ASC})
    .withPagination()
    .withSelectedRows()
    .withConfigurableColumns()
    .withFilters({
      dataFrom: filters.ge(value()),
      dateTo: filters.le(value()),
    })
    .withCategory("ALL", counterLoader)
    .compose(loader);

// Итоговое состояние скроллера для цепочки выше
state: {
  rows: any[],
  isLoading: boolean,
  error: any,
  focusedRow: undefined | Object,
  sort: { [columnName]: string },
  pagination: {
    page: number,
    totalPages: number,
    pageSize: number,
    totalElements: number,
  },
  selectedRows: Object[],
  hiddenColumnsIds: string[],
  applyedFilters: Object,
  filters: {
    ...
  },
  category: string,
  counters: {
    rows: any[],
    isLoading: boolean,
    error: any,
  }
}

methods: {
  load: (...args: anyp[]) => Promise<any>,
  focusedRow: {
      set: (row: Object) => void,
  },
  sort: {
    set: (sort: { [columnName]: string }) => void,
  },
  pagination: {
    setPage: (page: number) => void,
    setPageSize: (pageSize: number) => void,
    setTotalElements: (totalElements: number) => void
  },
  selectedRows: {
    set: (val: Object[]) => void,
    setItem: (index: number, item: Object) => void,
    add: (item: Object | Object[]) => void,
    remove: (item: Object | Object[]) => void,
    removeByIndex: (index: number) => void,
    selectAll: () => void,
    deselectAll: () => void,
    getAllSelected: () => boolean,
    toggleAll: () => void,
  }
  hiddenColumnsIds: {
    set: (string[]: Object) => void,
  },
  applyedFilters: {
    set: (filters: Object) => void,
  },
  filters: {
    apply: () => void,
    clear: () => void,
    getCanBeApplied: () => boolean,
    ...
  }
  category: {
    set: (category: string) => void
  },
  counters: {
    load: (...args: anyp[]) => Promise<any>,
  }
}

metaData: {
  sort: { [columnName]: string },
  page: number,
  pageSize: number,
  category: string,
  filters: {
     ...
  }
}

```

### функция filter(type)
Функция, которая принимает тип фильтра (строка) и возвращает функцию которая принимает юнит, метаданные для него, и возвращающая расширенный юнит, который может использовться как фильтр.

**Параметры:**
* **type [required, string]** - тип фильтра, (например 'eq', 'le' и т.д. )

Функция filter возвращает функцию у которой следующие параметры:

* **unit [required, Unit]** - юнит с методом set
* **defaultValue [optional, any]** - дефолтное значение юнита, т.е. которое будет ему назанчаться после очистки фильтров.
* **fieldName** - название фильтра, используется для формирования метаданных для фильтрации

Данная функция вернет новый расширенный экземпляр переданного в нее юнита. У расширенного юнита будет 3 метода:
* **set(val: T) => void** - метод для изменения состояния юнита, берется из базового юнита,
* **getFilterMetadata() => { fieldName: string | undefined, condition: string}** - метод для получения метаданных фильтра, таких как название фильтра (может быть пустой строкой если не передали), и тип фильтрации.


**Пример:**
```javascript
const dateFromFilter = filter('le')(value(), '12-12-2018', 'dateFrom')

```
### объект с предзаданными фильтрами filters
Объект вида: 
```javascript
{
    eq: filter(conditions.eq),
    like: filter(conditions.like),
    in: filter(conditions.in),
    ge: filter(conditions.ge),
    le: filter(conditions.le),
}
```

содержит заготовки для будущих фильтров

**Пример:**
```javascript
const documentsScroller = scroller()
    .withFilters({
        fromDate: filters.le(value()),
        toDate: filters.ge(value()),
        number: filters.eq(value(), 1),
        code: filters(value(), undefined, 'currency.code')
    })
    .compose(...)

```

## Цепочки
Перед чтением этого раздела необходимо ознакомиться с общим миханизмом оборачивания с использованием библиотеки istore-react (https://github.com/Curnull/istore-react).

Для оборачивания компонентом необходимо получить экзмепляр класса WrapChain, который связан с определенным доменом. Для этих целей в istore-react служит функция lockDomain, которая в параметрах принимает назание домена и сам домен, а возвразает экземпляр класса WrapChain.
Так как в приложении есть набор шаблонных (часто повторяющихся) оберток, например для оборачивания какого-либо компонента ввода данных или скроллера, возникает необходимость сделать быстрый способ оборачивания таких шаблонных компонентов. Для этого в библиотеке core был сделан расширенный метод lockDomain, который в целом работает так же как и lockDomain из istore-react, но возвращает не экзмепляр WrapChain а экземпляр класса MainChain, который является декоратором над WrapChain. MainChain предоставляет все те-же методы что и WrapChain и добавляет набор методов для шаблонных оберток. Таким образом все шаблонные обертки уже содержатся в MainChain и позволяют удобно и быстро обернуть любой уникальный или шаблонный компонент.

Рассмотрим lockDomain и MainChain подробно:

### lockDomain(prefixProvider, domainProvider, extra)

Функция для создания экземпляра класса MainChain, который будет связан с определенным доменом. 

**Параметры**
* **prefixProvider [required, string | (props: any) => string]** - строка либо функция возвращающая строку и принимающая проперти "главного" компонента. Возвращенная строка будет использоваться как префикс при помещении состояния домена в хранилище (domain.mount(prefix)). Префикс должен быть уникальным в рамках всего приложения.

* **domainProvider [required, Domain | (props: any) => Domain]** - экземпляр домена, либо функция возвращающая экземпляр домена на основе пропертей главного компонента.

* **extra [optional, Object]** - объект вида (все свойства опциональны):
  * **onMount: (domain, props) => void** - функция которая будет вызвана при помещении главного компонента на страницу, в нее будет передан экземпляр домена и проперти главного компонента. Может быть использована для первичной инициализации состояния домена.
  * **onChangeProps: (domain, nextProps, prevProps) => void** - функция которая будет вызываться каждый раз при изменении "внешних" (т.е. пропертей приходящих из родительского компонента, но не из оберток) пропертей. Первый аргумент функции это экземпляр домена, второй объект с новыми пропертями, третий объект со старыми пропертями. Может использоваться для обновления состояния домена при изменении пропертей главного компонента.

### MainChain
Класс для оборачивания react компонентов. Раотает по принципу цепочки с 2мя методами для замыкания цепочки (.component, .rootComponent) и получения обернутого компонента. Предоставляет удобный набор методов для быстрого оборачивания любого компонента приложения, будь то уникальный компонетнт с уникальным набором пропертей или шаблонный компонент. MainChain предоставляет интерфейс идентичный интерфейсу класс WrapChain но расширяет его набором методов для оборачивания шаблонных компонентов. Рассмотрим методы кдасса MainChain взятые из WrapChain:
* **withProps(propsProvider: (s: State, m: Methods, extra: Object) => Object): MainChain** - метод для предоствления пропертей оборачиваемому компоненту,
* **onChangeProps(callback: (domain: Domain, nextProps: any, prevProps: any, isInit: boolean) => void):  MainChain** - метод для добавления коллбэков срабатывающих при изменении пропертей оборнутого компонента,
* **withHOC(hoc: (component: Component) => Component): MainChain** - метод для расширения компонента каким-либо сторонним HOC,
* **join(chain: WrapChain): MainChain** - метод для объединения 2х цепочек в одну,
* **lockSubDomain(stateProvider, methodsProveider: ): MainChain** - метод для создания нового экземпляра WrapChain, при этом домен этого экземпляра будет сформирован из текущего домена,
* **сomponent(component: Component): Component** - метод для применения цепочки к какому-либо компоннту,
* **rootComponent(component: Component): Component** - метод для применения цепчоки к "главному" компоненту страницы.

Рассмотрим дополнительные методы класса MainChain:
* **scroller(scrollerSelector: (s: TState) => Object): WrapChain** - метод для оборачивания скроллера, принимает на входе функцию которой будет передано состояние домена и которая должна вернуть состояние оборачиваемого скроллера. Метод возвращает экземпляр WrapChain но в будущем будет возвращать обернутый компонент скроллера,
* **filtersPanel(methodsSelectr: (m: TMethods) => Object): WrapChain** - метод для оборачивания панели фильтрации, принимает на входе функцию которой будет переданы методы домена и которая должна вернуть методы применения, очистки фильтров и метод для проверки изменились ли фильтры (apply, clear, getCanBeApplied). Метод возвращает экземпляр WrapChain но в будущем будет возвращать обернутый компонент панели фильтрации,
* **pagination(scrollerSelector: (s: TState) => Object): WrapChain** - метод для оборачивания пагинации скроллера, принимает на входе функцию которой будет передано состояние домена и которая должна вернуть состояние скроллера с пагинацией. Метод возвращает экземпляр WrapChain но в будущем будет возвращать обернутый компонент пагинации,
* **input(selector: (s: TState) => any, setterSelector?: (m: TMethods) => (v: any) => void): InputChain** - метод для оборачивания компонентов ввода, принимает на входе функцию которой будет передано состояние домена и которая должна вернуть значение компонента ввода, для получения метода для изменения значения компонента ввода вызывается selector только передается ему не состояние домена, а методы, в вернувшемся значении берется метод set. Если нужна альтернативная логика для получения сеттера, используйте второй аргумент setterSelector, который является функцией которой будут переданы методы домена и которая должна вернуть нужный сеттер. Метод возвращает объект который будет описан в разделе InputChain,
* **form(documentSelector: (s: TState) => any, validationSelector: (s: TState) => IValidationState): FormChain** - метод для получения цепочки для оборачивания компонентов ввода формы. Принимает 2 аргумета: documentSelector функция которой будет передано состояние домена и которая должна вернуть объект представляющий собой все поля формы, validationSelector функция которой будет передано состояние домена и которая должна вернуть состояние валидации формы. Метод возвращает объект который будет описан в разделе FormChain. **Важно:** для правильной работы валидации необходимо задекарировать методы документа декоратором decorateWithNameGetter для получения имен полей.

### InputChain
Объект содержащий следующие свойства:
* **сomponent(component: Component): Component** - функция для замыкания цепочки каким-либо компонентом,
* **select(options: any[] | (s: State) => any[]): { component: (component: Component) => Component }** - метод для оборачивания компонента select. Принимает на входе либо массив с опциями компонента либо функцию которой будет передано состояние домена и которая должна вернуть массив с опциями компонента. Возвращает объект с одним свойсвом: component, случащим для замыкания цепочки каким либо компонентом и получения обернутого компонента. В будущем будет возвращать обернутый компонент селекта.

### FormChain
Объект содержащий следующие свойства:
* **withProps(propsProvider: (s: State, m: Methods, extra: Object) => Object): FormChain** - метод для предоствления пропертей оборачиваемому компоненту,
* **withHOC(hoc: (component: Component) => Component): FormChain** - метод для расширения компонента каким-либо сторонним HOC,
* **join(chain: WrapChain): FormChain** -  метод для объединения 2х цепочек в одну,
* **input(selector: (s: TState) => any, setterSelector?: (m: TMethods) => (v: any) => void): InputChain** - метод для оборачивания компонентов ввода, принимает на входе функцию которой будет передано состояние домена и которая должна вернуть значение компонента ввода. По умолчанию для получения метода для изменения значения компонента ввода вызывается selector, но передается ему не состояние домена, а методы домена, в вернувшемся значении берется метод set. Если нужна альтернативная логика для получения сеттера, используйте второй аргумент setterSelector, который является функцией которой будут переданы методы домена и которая должна вернуть нужный сеттер. Метод возвращает объект который будет описан в разделе InputChain,

**Пример**

```javascript
const myDomain = domain({
  document: {
    name: value(''),
    age: value(0),
    gender: value('Male'),
    email: value(''),
  },
  validation: validation(rulesProvider),
  scroller: scroller()
    .withFilters({
      dateFrom: filters.ge(value('')),
      dateTo: filters.le(value('')),
    })
    .withPagination()
    .compose(dataLoader),
}).withMethods(m => ({
  ...m,
  document: decorateWithNameGetter(m.document), // декорируем методы документа декоратором decorateWithNameGetter
}));

const wrap = lockDomain('example', myDomain); // получем эземпляр класса MainChain для обертки компонентов

const DateFromFilterInput = wrap.input(s => s.scroller.filters.dateFrom).component(DateInput); // создаем компонент фильтра для фильтра dateFrom
const DateToFilterInput = wrap.input(s => s.scroller.filters.dateTo).component(DateInput); // создаем компонент фильтра для фильтра dateTo

const MyFilterPanel = wrap.filtersPanel(s => s.scroller.filters).component(FilterPanel); // создаем компонент для панели фильтрации
const MyScroller = wrap.scroller(s => s.scroller).component(Scroller); // создаем компонент скроллера
const MyPagination = wrap.pagination(s => s.scroller).component(Pagination);  // создаем компонент пагинации

const MyCustomComponent = wrap // пример не шаблонного компонента
  .withProps(s => ({ // предоставляем неоюходимые для компонента проперти из домена страницы
    name: s.document.name,
    focusedRow: s.scroller.focusedRow,
  })
  .component(CustomComponent); // замыкаем цепчоку каким-либо компонетом


// обертка компонентов ввода формы
const wrapForm = wrap.form((s => s.document, s => s.validation); // получаем экземпляр объекта FormChain для обертки компонентов ввода формы

const NameInput = wrapForm.input(s => s.document.name).component(TextInput); // создаем компонент ввода формы для поля name
const AgeInput = wrapForm.withProps(() => ({ maxValue: 150 })).input(s => s.document.age).component(NumericInput); // создаем компонент ввода формы для поля age, предоставляем дополнительные проперти с помощью метода withProps
const options = [{ label: 'Муж.', value: 'Male'}, { label: 'Жен.', value: 'Female'}]; // описываем опции для компонента выбора пола
const GenderInput = wrapForm.input(s => s.document.gender).select(options).component(Select); //создаем компонент выбора пола для формы
const EmailInput = wrapForm.input(s => s.document.email).component(TextInput); // создаем компонент ввода формы для поля email
```


## Домены

### remoteCollection

**remoteCollection(loader)** - домен для работы с коллекциями данных находящимися на сервере.

**Параметры:**
loader [required, (...args: any[]) => Promise<Row[]>] - функция, которая возвращает экземпляр Promise, который должен при успешном выполнении передать в then массив объектов.

**state:**
 * isLoading [boolean] - true если loader запущен, false если нет
 * rows [Array<any>] - массив с данными пришедшими после запуска loader'а
 * errors [any] - хранит ошибку полученную в результате выполнения loader

**methods:**
 * load(...args: any[]) => Promise<Row[]> - запускает loader и передает ему все параметры полученные при вызове.

**Пример:**
```javascript
const getAccounts = (count) => Promise.resolve([1,2,3,4]);

const accountsDictionary = remoteCollection(getAccounts);

accountsDictionary.mount('accountsDictionary');

console.log(accountsDictionary.state); // { isLoading: false, rows: [], error: undefined }

accountsDictionary.methods.load(100).then(() => {
  console.log(accountsDictionary.state); // { isLoading: false, rows: [1,2,3,4], error: undefined }
})

console.log(accountsDictionary.state); // { isLoading: true, rows: [], error: undefined }

```

---
### pagination

**pagination()** - домен для работы с пагинацией, содержит информацию о текущей выбранной странице, их кол-ве, кол-ве элементов на странице и так далее.

**state:**
 * page [number] - номер текущей страницы
 * pageSize [number] - текущий размер страницы
 * totalElements [number] - общее кол-во элементов в коллекции
 * totalPages [number] - общее кол-во страниц размером pageSize в коллеции. Высчитывается автоматически

**methods:**
 * setPage: (page: number) => void - метод для изменения текущей страницы
 * setPageSize: (pageSize: number) => void - метод для изменения размеры страницы
 * setTotalElements: (totalElements: number) => void; - метод для изменения общего кол-ва элеметов в коллекции

**Пример:**
```javascript
  const paginationDomain = pagination();
  paginationDomain.mount('pagination');
  console.log(paginationDomain.state) // { page: 1, pageSize: 20, totalPages: 1, totalElements: 0 });

  paginationDomain.methods.setTotalElements(100);
  console.log(paginationDomain.state) // { page: 1, pageSize: 20, totalPages: 5, totalElements: 100 });

   paginationDomain.unmount();
```


### validation

**validation(rulesLoader)** - домен для валидирования документа и хранения метаданных валидации

**параметры**
* rulesLoader [required, () => Promise<object>] - функция которая возвращает Promise, который загружает правила валдидации для данного документа.

**state:**
* errors [{[prop: string]: string }] - объект с текущими ошибками валидации, где ключи это названия полей а значения строки с текстами ошибок
* touchedFields [string[]] - массив названий полей, к которым "прикоснулся" пользователь
* activeField [string] - название текщего активного поля (может быть пустым)
* isLoading [boolean] - если true значит загружаются правила валидации (rulesLoader), иначе false
* allTouched - true если была вызвана валидация всего документа перед отправкой на сервер.

**methods:**
* validate(document) => void - производит валидацию документа (входной параметр document)
* isValid() => boolean - true если нет ошибок в errors, иначе false
* focus(fieldName: string) => void - помечает поле связанное с переданным враппером как активное (в фокусе)
* unfocus(fieldName: string) => void - помечает поле как не активное (не в фокусе)
* touch(name: string) => void - помечает поле как touched
* clearError(name: string) => void - очищает ошибки для поля
* clearErrors() => void - очищает все ошибки
* setError(field: string, err: string) => void - добавляет ошибку err для поля field в errors
* touchAll() => void - переводит значение поля allTouched состояния домена в true
* loadValidationRules() => void - запускает rulesLoader для загрузки валидационных правил, пока эта фунеция не вызвана валидация работать не будет.
 
**Пример:**
```javascript
const loadValidationRules = () => fetch('api/validationRules');

const validation = validation(loadValidationRules)
}

validation.mount('validation', validation);

console.log(validation.state); // { errors: {}, touchedFields: [], activeField: '', isLoading: true, allTouched: false }
validation.methods.loadValudationRules();

// когда правила загрузились
const document = { name: '', age: 432 }
validation.methods.validate(document);

console.log(validation.state); // { errors: { name: 'name is required' }, touchedFields: [], activeField: '', isLoading: true, allTouched: false }

validation.methods.touch('name');

console.log(validation.state); // { errors: { name: 'name is required' }, touchedFields: ['name'], activeField: '', isLoading: true, allTouched: false }

validation.unmount()
```

## хелперы/инструменты

### formatDate(strWithDate) **removed**
Функиця переехала в @platform/localization

### formatAccountCode(codeStr) **removed**
Функиця переехала в @platform/localization

### formatMoney(amount) **removed**
Функиця переехала в @platform/localization

### to(promise)
Функция для упрощения работы с асинхронными действиями. Функция принимает на входе промис и возвращает промис, который при разрешении будет получать массив, в случае успеха в котором первым элементом будет результат разрешения промиса а вторым null, в случае если произошла ошибка первым элементом массива будет null а вторым объект ошибки.

**Пример**
```javascript
import { to } from 'core';

const successPromise = Promise.resolve('success');
const failedPromise = Promise.reject('error');

const someAsyncFunc = async () => {
  const [result, error] = await to(successPromise);

  console.log(result); // 'success'
  console.log(error); // null

  const [result2, error2] = await to(failedPromise);
  console.log(result2); // null
  console.log(error2); // error
}

someAsyncFunc();
```

## Декораторы

### decorateWithNameGetter(objToDecorate)

Декоратор для добавления метода getName для входного объекта и его содержимого. Данный декоратор предлагается использовать для документов, что бы получать имена полей на основе их положения в документе.

**Пример**
```javascript

//допустим имеется объект с методами
const documentMethods = {
  id: {
    set: (v) => { ... },
  },
  amount: {
    set: (v) => { ... },
  },
  budget: {
    budgetType: {
      set: (v) => { ... },
    } ,
    customsCode: {
      set: () => { ... },
    },
  }
};

const nextMethods = decorateWithNameGetter(documentMethods);

//Тип объекта nextMethods будет иметь вид
const documentMethods = {
  id: {
    set: (v) => void,
    getName: () => string, // вернет 'id'
  },
  amount: {
    set: (v) => void,
    getName: () => string, // вернет 'amount'
  },
  budget: {
    budgetType: {
      set: (v) => void,
      getName: () => string, // вернет 'budget.budgetType'
    },
    customsCode: {
      set: (v) => void,
      getName: () => string, // вернет 'budget.customsCode'
    },
    getName: () => string, // вернет 'budget'
  },
  getName: () => string, // вернет ''
};

// как это использовать
const documentDomain = domain({
  documentNumber: value(''),
  documentDate: value(''),
  amount: value(0),
  payer: {
    clientId: value(),
    name: value(''),
    account: value('', ''),
    bank: {
      name: value(''),
      bic: value('', ''),
      corrAccount: value('', ''),
    },
  },
}).withMethods((methods) => decorateWithNameGetter(methods));

documentDomain.mount('doc');

documentDomain.methods.payer.bank.name.getName(); // "payer.bank.name"

```
