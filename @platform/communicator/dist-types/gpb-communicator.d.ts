import type { IUserInfo, IBankClient, IDialogOptions, IConfirmationOptions, INotificationOption } from './interfaces';
export declare class GPBCommunicator {
    version: string;
    private communicator;
    constructor(name: any, version: string);
    startHandshake: () => Promise<boolean>;
    finishHandshake: () => void;
    requestUserInfo: () => Promise<IUserInfo>;
    requestSelectedOrgId: () => Promise<string>;
    requestUserOrganizations: () => Promise<IBankClient[]>;
    showLoaderOverlay: () => void;
    hideLoaderOverlay: () => void;
    showAlert: (params: IDialogOptions) => Promise<Record<string, unknown>>;
    showConfirmation: (params: IConfirmationOptions) => Promise<boolean>;
    goToUrl: (url: string) => void;
    showSuccessPushNotification: (params: INotificationOption) => void;
    showErrorPushNotification: (params: INotificationOption) => void;
    showWarningPushNotification: (params: INotificationOption) => void;
    showInfoPushNotification: (params: INotificationOption) => void;
    onChangeSelectedOrganization: (handler: (orgId: string) => void) => () => void;
}
export declare const getGPBCommunicator: (name: string, version: string) => GPBCommunicator;
