import type { IParentCommunicatorHandlers, IParentCommunicator } from './interfaces';
export declare const getParentCommunicator: (childrenName: string, handlers: IParentCommunicatorHandlers) => IParentCommunicator;
