export declare enum COMMUNICATOR_MESSAGE_TYPE {
    MESSAGE = "COMMUNICATOR_MESSAGE",
    REQUIEST = "COMMUNICATOR_REQUIEST",
    RESPONSE = "COMMUNICATOR_RESPONSE"
}
export interface ICommunicatorBaseMessage<T> {
    sender: string;
    receiver: string;
    data?: T;
    name: string;
}
export interface ICommunicatorMessage<T> extends ICommunicatorBaseMessage<T> {
    type: COMMUNICATOR_MESSAGE_TYPE;
}
export interface ICommunicatorRequest<T> extends ICommunicatorMessage<T> {
    id: string;
    type: COMMUNICATOR_MESSAGE_TYPE.REQUIEST;
}
export interface ICommunicatorResponse<T> extends ICommunicatorMessage<T> {
    id: string;
    type: COMMUNICATOR_MESSAGE_TYPE.RESPONSE;
}
export declare class Communicator {
    name: string;
    parthenName: string;
    private postMessage;
    private responseHandlers;
    private requestHandlers;
    private handlers;
    private counter;
    constructor(name: string, parthenName: string, postMessage: (message: ICommunicatorMessage<any> | ICommunicatorRequest<any>) => void, messageHandler: (handler: (m: ICommunicatorMessage<any> | ICommunicatorResponse<any>) => void) => void);
    on: <T>(name: string, handler: (message: ICommunicatorMessage<T>) => void) => () => void;
    onRequest: <T>(name: any, handler: (message: ICommunicatorRequest<T>) => any) => void;
    send: (name: string, data?: any) => void;
    request: <T>(name: string, data?: any) => Promise<T>;
    private getId;
    private addResponseHandler;
    private handleIncomingMessage;
}
