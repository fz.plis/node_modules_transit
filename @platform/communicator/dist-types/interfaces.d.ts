export declare enum BLOCK_STATE {
    UNBLOCKED = "UNBLOCKED",
    BLOCKED = "BLOCKED"
}
export declare enum BANK_CLIENT_STATUS {
    ACTIVE = "ACTIVE",
    POTENTIAL = "POTENTIAL"
}
export declare enum BANK_CLIENT_TYPE {
    UL = "UL",
    IP = "IP",
    FL = "FL"
}
export declare enum ACTIVITY_TYPE {
    FEDERAL = 1,
    GOS = 2,
    NON_GOS = 3,
    IP = 4
}
export interface ICountry {
    code: string;
    id: string;
    name: string;
}
export declare enum SYSTEM_SOURCES {
    BSS_DBO_320 = "BSS_DBO_320",
    ECO = "ECO",
    RKO_F1 = "RKO_F1",
    ESK = "ESK",
    TESSA = "TESSA"
}
export interface BankClientCustomerId {
    customerId?: string;
    source?: SYSTEM_SOURCES;
}
export interface IBankClient {
    absId: string;
    bankClientCustomerIds?: BankClientCustomerId[];
    createdAt?: string;
    createdBySource?: SYSTEM_SOURCES;
    emails?: string[];
    externalClientGuid?: string;
    id: string;
    isConnectedToDbo?: boolean;
    isDebitOperationsForbidden?: boolean;
    legalAddress?: string;
    phone?: string;
    postAddress?: string;
    clientId: number;
    isResident: boolean;
    clientType: BANK_CLIENT_TYPE;
    fullName: string;
    shortName: string;
    innKio: string;
    status: BANK_CLIENT_STATUS;
    additionalInfo: string;
    blockState: BLOCK_STATE;
    okopf: string;
    okfs: string;
    activityType: ACTIVITY_TYPE;
    ogrnOgrip: string;
    kpp: string;
    regNumber: string;
    internationalName: string;
    tin: string;
    country: ICountry;
    hasConstitution: boolean;
}
export interface IParsedToken {
    jti: string;
    exp: number;
    firstName: string;
    userId: string;
    login: string;
    patronymic: string;
    phoneNumber: string;
    secondName: string;
}
export interface IUserData {
    firstName: string;
    secondName: string;
    patronymic: string;
    userId: string;
    login: string;
    phoneNumber: string;
    email: string;
}
export interface IUserInfo {
    token: string;
    profile: IUserData;
    roles: string[];
}
export interface IDialogOptions {
    text: string;
    header?: string;
    okButtonText?: string;
}
export interface IConfirmationOptions extends IDialogOptions {
    cancelButtonText?: string;
}
export interface INotificationOption {
    message: string;
    header: string;
}
export declare enum MESSAGE_NAMES {
    START_HANDSHAKE = "START_HANDSHAKE",
    FINISH_HANDSHAKE = "FINISH_HANDSHAKE",
    GET_USER_INFO = "USER_INFO",
    GET_USER_ORGANIZATIONS = "USER_ORGANIZATIONS",
    GET_SELECTED_ORG = "GET_SELECTED_ORG",
    SHOW_LOADER_OVERLAY = "SHOW_LOADER_OVERLAY",
    HIDE_LOADER_OVERLAY = "HIDE_LOADER_OVERLAY",
    SHOW_ALERT = "SHOW_ALERT",
    SHOW_CONFIRMATION = "SHOW_CONFIRMATION",
    GO_TO_URL = "GO_TO_URL",
    SHOW_SUCCESS_PUSH_NOTIFICATION = "SHOW_SUCCESS_PUSH_NOTIFICATION",
    SHOW_ERROR_PUSH_NOTIFICATION = "SHOW_ERROR_PUSH_NOTIFICATION",
    SHOW_WARNING_PUSH_NOTIFICATION = "SHOW_WARNING_PUSH_NOTIFICATION",
    SHOW_INFO_PUSH_NOTIFICATION = "SHOW_INFO_PUSH_NOTIFICATION"
}
export declare enum EVENTS {
    ACTION_BUTTON_CLICK = "ACTION_BUTTON_CLICK",
    MENU_ITEM_CLICK = "MENU_ITEM_CLICK",
    SELECTED_ORGANIZATION_CHANGED = "SELECTED_ORGANIZATION_CHANGED"
}
export declare const PARENT_NAME = "parent";
export interface IModuleMetaData {
    version: string;
    name: string;
}
export interface IParentCommunicatorHandlers {
    validateMetadata(meta: IModuleMetaData): boolean;
    handleHandshakeTimeout(): void;
    handleFinishHandshake(): void;
    handleGetInfoRequest(): IUserInfo;
    handleGetOrganizationsRequest(): IBankClient[];
    handleGetSelectedOrg(): string;
    handleShowOveraly(): void;
    handleHideOverlay(): void;
    handleGoToUrl(url: string): void;
    handleShowAlert(options: IDialogOptions): Promise<void>;
    handleShowConfirmation(options: IConfirmationOptions): Promise<boolean>;
    handleShowSuccessPushNotification(params: INotificationOption): void;
    handleShowErrorPushNotification(params: INotificationOption): void;
    handleShowWarningPushNotification(params: INotificationOption): void;
    handleShowInfoPushNotification(params: INotificationOption): void;
}
export interface IParentCommunicator {
    sendChangedOrgId(orgId: string): void;
    destroy(): void;
}
