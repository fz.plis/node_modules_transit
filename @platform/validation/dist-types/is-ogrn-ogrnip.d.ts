import type * as yup from 'yup';
declare module 'yup' {
    interface StringSchema {
        isOgrnOgrnip(msg?: string): this;
    }
}
export declare const isOgrnOgrnip: (this: yup.StringSchema, message?: string) => yup.default<string | undefined, import("yup/lib/types").AnyObject, string | undefined>;
