import type * as yup from 'yup';
declare module 'yup' {
    interface StringSchema {
        isInn(msg?: string): this;
    }
}
export declare const isInn: (this: yup.StringSchema, message?: string) => yup.default<string | undefined, import("yup/lib/types").AnyObject, string | undefined>;
