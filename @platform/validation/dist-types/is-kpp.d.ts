import type * as yup from 'yup';
declare module 'yup' {
    interface StringSchema {
        isKpp(msg?: string): this;
    }
}
export declare const isKpp: (this: yup.StringSchema, message?: string) => yup.default<string | undefined, import("yup/lib/types").AnyObject, string | undefined>;
