import type { Config, ValidationErrors } from 'final-form';
import type { AnySchema } from 'yup';
export declare const validate: <T>(errorSchema?: AnySchema<any, any, any> | undefined, warningSchema?: AnySchema<any, any, any> | undefined) => (values: T) => ValidationErrors | Promise<ValidationErrors>;
