export declare const t: (key: string, params?: any) => string;
declare const _default: {
    /**
     * @ru
     * квартал
     * @en-us
     * quarter
     */
    readonly quart: string;
    /**
     * @ru
     * Ошибки
     * @en-us
     * Errors
     */
    readonly errors: string;
    /**
     * @ru
     * Предупреждения
     * @en-us
     * Warnings
     */
    readonly warnings: string;
    dateRange: {
        /**
         * @ru
         * За текущий день
         * @en-us
         * In current day
         */
        readonly inCurrentDay: string;
        /**
         * @ru
         * За текущую неделю
         * @en-us
         * In current week
         */
        readonly inCurrentWeek: string;
        /**
         * @ru
         * За текущий месяц
         * @en-us
         * In current month
         */
        readonly inCurrentMonth: string;
    };
    /**
     * @ru
     * полугодие
     * @en-us
     * half-year
     */
    readonly half: string;
    calendar: {
        /**
         * @ru
         * Cегодня {d, date}
         * @en-us
         * Today {d, date}
         */
        today: (p: {
            d: string;
        }) => string;
        /**
         * @ru
         * Дата
         * @en-us
         * Date
         */
        readonly dateText: string;
        monthLow: {
            /**
             * @ru
             * МС
             * @en-us
             * MT
             */
            readonly text: string;
        };
        quartLow: {
            /**
             * @ru
             * КВ
             * @en-us
             * QR
             */
            readonly text: string;
        };
        halflow: {
            /**
             * @ru
             * ПЛ
             * @en-us
             * HL
             */
            readonly text: string;
        };
        yeaslow: {
            /**
             * @ru
             * ГД
             * @en-us
             * YR
             */
            readonly text: string;
        };
        yearChoice: {
            /**
             * @ru
             * Выбор года
             * @en-us
             * Select year
             */
            readonly text: string;
        };
        halfChoice: {
            /**
             * @ru
             * Выбор полугодия
             * @en-us
             * Select half-year
             */
            readonly text: string;
        };
        quarterChoice: {
            /**
             * @ru
             * Выбор квартала
             * @en-us
             * Select quarter
             */
            readonly text: string;
        };
        monthChoice: {
            /**
             * @ru
             * Выбор месяца
             * @en-us
             * Select month
             */
            readonly text: string;
        };
        dayChoice: {
            /**
             * @ru
             * Выбор дня
             * @en-us
             * Select day
             */
            readonly text: string;
        };
        /**
         * @ru
         * Январь
         * @en-us
         * January
         */
        readonly month1: string;
        /**
         * @ru
         * Февраль
         * @en-us
         * February
         */
        readonly month2: string;
        /**
         * @ru
         * Март
         * @en-us
         * March
         */
        readonly month3: string;
        /**
         * @ru
         * Апрель
         * @en-us
         * April
         */
        readonly month4: string;
        /**
         * @ru
         * Май
         * @en-us
         * May
         */
        readonly month5: string;
        /**
         * @ru
         * Июнь
         * @en-us
         * June
         */
        readonly month6: string;
        /**
         * @ru
         * Июль
         * @en-us
         * July
         */
        readonly month7: string;
        /**
         * @ru
         * Август
         * @en-us
         * August
         */
        readonly month8: string;
        /**
         * @ru
         * Сентябрь
         * @en-us
         * September
         */
        readonly month9: string;
        /**
         * @ru
         * Октябрь
         * @en-us
         * October
         */
        readonly month10: string;
        /**
         * @ru
         * Ноябрь
         * @en-us
         * November
         */
        readonly month11: string;
        /**
         * @ru
         * Декабрь
         * @en-us
         * December
         */
        readonly month12: string;
        /**
         * @ru
         * ПН
         * @en-us
         * MO
         */
        readonly daysWeekLow1: string;
        /**
         * @ru
         * ВТ
         * @en-us
         * TU
         */
        readonly daysWeekLow2: string;
        /**
         * @ru
         * СР
         * @en-us
         * WE
         */
        readonly daysWeekLow3: string;
        /**
         * @ru
         * ЧТ
         * @en-us
         * TH
         */
        readonly daysWeekLow4: string;
        /**
         * @ru
         * ПТ
         * @en-us
         * FR
         */
        readonly daysWeekLow5: string;
        /**
         * @ru
         * СБ
         * @en-us
         * SA
         */
        readonly daysWeekLow6: string;
        /**
         * @ru
         * ВС
         * @en-us
         * SU
         */
        readonly daysWeekLow7: string;
    };
    dialog: {
        /**
         * @ru
         * OK
         * @en-us
         * OK
         */
        readonly ok: string;
        /**
         * @ru
         * Применить
         * @en-us
         * Apply
         */
        readonly apply: string;
        /**
         * @ru
         * Отмена
         * @en-us
         * Cancel
         */
        readonly cancel: string;
        /**
         * @ru
         * Сбросить
         * @en-us
         * Clear
         */
        readonly clear: string;
        /**
         * @ru
         * Подтверждение
         * @en-us
         * Confirmation
         */
        readonly confirmationHeader: string;
        /**
         * @ru
         * Предупреждение
         * @en-us
         * Warning
         */
        readonly alertHeader: string;
        /**
         * @ru
         * Исправить
         * @en-us
         * Fix
         */
        readonly errorOk: string;
        /**
         * @ru
         * Продолжить с ошибками
         * @en-us
         * Continue with errors
         */
        readonly errorCancel: string;
    };
    topline: {
        /**
         * @ru
         * Выход
         * @en-us
         * Exit
         */
        readonly exit: string;
    };
    filter: {
        /**
         * @ru
         * Параметры поиска
         * @en-us
         * Search option
         */
        readonly header: string;
    };
    tagsPanel: {
        more: {
            /**
             * @ru
             * Еще
             * @en-us
             * More
             */
            readonly text: string;
        };
        /**
         * @ru
         * Фильтр
         * @en-us
         * Filter
         */
        readonly filter: string;
    };
    multiSelect: {
        /**
         * @ru
         * Выбрать все
         * @en-us
         * Select all
         */
        readonly selectAll: string;
        /**
         * @ru
         * Очистить
         * @en-us
         * Clear
         */
        readonly clearAll: string;
    };
    placeholder: {
        /**
         * @ru
         * Поиск
         * @en-us
         * Search
         */
        readonly search: string;
    };
    notify: {
        /**
         * @ru
         * Свернуть
         * @en-us
         * Collapse
         */
        readonly collapse: string;
        /**
         * @ru
         * Раскрыть
         * @en-us
         * Expand
         */
        readonly expand: string;
        /**
         * @ru
         * Перейти
         * @en-us
         * Open
         */
        readonly link: string;
    };
    scroller: {
        /**
         * @ru
         * Нет данных для отображения
         * @en-us
         * No data to display
         */
        readonly emptyPlaceholter: string;
        /**
         * @ru
         * По Вашему запросу ничего не найдено. Попробуйте
         * @en-us
         * At Your request, nothing was found. Give it a try
         */
        readonly emptyPlaceholterWithFilter: string;
        /**
         * @ru
         * изменить параметры фильтрации
         * @en-us
         * change filtering options
         */
        readonly changeFilterOptions: string;
        toolbar: {
            /**
             * @ru
             * Сортировка
             * @en-us
             * Sort
             */
            readonly sort: string;
        };
    };
    captcha: {
        /**
         * @ru
         * Капча
         * @en-us
         * Captcha
         */
        readonly captcha: string;
        /**
         * @ru
         * Идет загрузка...
         * @en-us
         * Loading...
         */
        readonly loading: string;
        /**
         * @ru
         * Обновить
         * @en-us
         * Reload
         */
        readonly reload: string;
    };
    pagination: {
        /**
         * @ru
         * {page} из {total}
         * @en-us
         * {page} out of {total}
         */
        hint: (p: {
            page: number;
            total: number;
        }) => string;
    };
    action: {
        /**
         * @ru
         * Редактировать
         * @en-us
         * Edit
         */
        readonly edit: string;
    };
    /**
     * @ru
     * Статус
     * @en-us
     * Status
     */
    readonly status: string;
    fileInput: {
        /**
         * @ru
         * Размер — до {size,size}.
         * @en-us
         * Max size — {size,size}
         */
        maxSize: (p: {
            size: number;
        }) => string;
        /**
         * @ru
         * Перетаскивайте сюда файлы с компьютера или загружайте их по кнопке ниже.
         * @en-us
         * Drug and drop files here or press button below.
         */
        readonly defaultText: string;
        /**
         * @ru
         * Форматы:
         * @en-us
         * Accept
         */
        readonly formats: string;
        /**
         * @ru
         * Добавить файл
         * @en-us
         * Add file
         */
        readonly add: string;
        /**
         * @ru
         * Добавить еще
         * @en-us
         * Add more
         */
        readonly more: string;
        /**
         * @ru
         * Выполняется загрузка файла
         * @en-us
         * Uploading file
         */
        readonly loading: string;
        /**
         * @ru
         * Выполняется проверка на вирусы
         * @en-us
         * Malware checking
         */
        readonly malwareCheck: string;
    };
    resultView: {
        /**
         * @ru
         * Комментарий
         * @en-us
         * Comment
         */
        readonly comment: string;
    };
    businessOnline: {
        /**
         * @ru
         * бизнес онлайн
         * @en-us
         * business online
         */
        readonly text: string;
    };
    sidebar: {
        feedback: {
            /**
             * @ru
             * Cвязаться с банком
             */
            readonly label: string;
            /**
             * @ru
             * 8 800 100-11-89
             */
            readonly number: string;
            /**
             * @ru
             * Бесплатно для регионов России
             */
            readonly numberInfo: string;
            /**
             * @ru
             * *0701
             */
            readonly mobile: string;
            /**
             * @ru
             * Для абонентов ГПБ Мобайл, Билайн, Мегафон, МОТИВ, МТС, TELE2 и других сотовых операторов
             */
            readonly mobileInfo: string;
        };
        footer: {
            /**
             * @ru
             * © Банк ГПБ (АО), 1990-{year}
             */
            partOne: (p: {
                year: string;
            }) => string;
            /**
             * @ru
             * Генеральная лицензия Банка России № 354
             */
            readonly partTwo: string;
        };
    };
    /**
     * @ru
     * Не задано
     */
    readonly emptySortLabel: string;
};
export default _default;
