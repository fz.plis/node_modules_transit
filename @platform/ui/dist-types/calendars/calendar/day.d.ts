import React from 'react';
import type { IWebBoxProps } from '../../atoms';
export interface IDayProps extends Omit<IWebBoxProps, 'onClick' | 'onClick'> {
    day?: number;
    selected?: boolean;
    disabled?: boolean;
    isToday?: boolean;
    onClick?(day: number): void;
    onDbClick?(day: number): void;
}
export declare const Day: React.FC<IDayProps>;
