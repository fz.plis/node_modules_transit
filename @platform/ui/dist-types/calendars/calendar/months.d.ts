import React from 'react';
export interface IMonthsProps extends Omit<React.HTMLAttributes<HTMLDivElement>, 'onChange'> {
    month: number;
    onChange(month: number): void;
}
export declare const Months: ({ month, onChange }: IMonthsProps) => JSX.Element;
