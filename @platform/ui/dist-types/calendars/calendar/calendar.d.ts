import React from 'react';
import type { IWebBoxProps } from '../../atoms';
import type { ICalendarDateProps } from '../../interfaces';
export interface ICalendarProps extends ICalendarDateProps, Omit<IWebBoxProps, 'onChange' | 'onClick'> {
    date?: Date;
    onChange?(date: Date): void;
    onClick?(date: Date): void;
    isPeriod?: boolean;
    today?: Date;
}
export interface ICalendarState {
    day: number;
    month: number;
    year: number;
}
export declare class Calendar extends React.Component<ICalendarProps, ICalendarState> {
    static defaultProps: {
        onChange: (_?: any) => void;
        onClick: (_?: any) => void;
    };
    constructor(props: ICalendarProps);
    state: {
        day: number;
        month: number;
        year: number;
    };
    private handlePrevMonthClick;
    private handleNextMonthClick;
    private handleDayDbClick;
    private handleDayClick;
    private handleMonthClick;
    private handleYearClick;
    private handleChangeDate;
    render(): JSX.Element;
}
