import React from 'react';
export interface ICalendarHeaderProps {
    month: number;
    year: number;
    onPrevMonthClick(): void;
    onNextMonthClick(): void;
    onChangeMonth(month: number): void;
    onChangeYear(year: number): void;
}
export declare const CalendarHeader: React.FC<ICalendarHeaderProps>;
