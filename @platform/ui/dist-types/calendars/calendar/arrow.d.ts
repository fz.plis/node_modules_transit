import React from 'react';
import type { IWebBoxProps } from '../../atoms';
export interface IArrow extends IWebBoxProps {
    reverse?: boolean;
}
export declare const Arrow: React.FC<IArrow>;
