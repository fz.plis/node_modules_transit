import React from 'react';
import type { ISelectProps } from '../../../inputs/select';
export interface ISelectCalendar extends ISelectProps {
    yearInput?: boolean;
}
export declare const SelectCalendar: React.FC<ISelectCalendar>;
