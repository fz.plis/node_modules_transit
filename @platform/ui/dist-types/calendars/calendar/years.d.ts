import React from 'react';
export interface IYearsProps extends Omit<React.HTMLAttributes<HTMLDivElement>, 'onChange'> {
    year: number;
    onChange(year: number): void;
}
export interface IYearsState {
    min: number;
    max: number;
}
export declare class Years extends React.Component<IYearsProps, IYearsState> {
    state: {
        min: number;
        max: number;
    };
    private handleScroll;
    render(): JSX.Element;
}
