/// <reference types="react" />
import type { ICalendarDateProps } from '../../interfaces';
export interface IDaysPageProps extends ICalendarDateProps {
    currDay: number;
    currMonth: number;
    currYear: number;
    onDayClick(newDay: number): void;
    onDayDbClick(newDay: number): void;
}
export declare const DaysPage: ({ minDate, maxDate, currYear, currDay, currMonth, onDayClick, onDayDbClick }: IDaysPageProps) => JSX.Element;
