import { Calendar as Date } from './calendar';
export declare const Calendar: {
    Date: typeof Date;
};
export * from './calendar';
