export declare const ROMAN: string[];
export declare const DAY_WEEK: string[];
export declare enum PERIOD_OPTIONS {
    DAY = "DAY",
    MONTH = "MONTH",
    QUART = "QUART",
    HALF = "HALF",
    YEAR = "YEAR"
}
export declare const PERIOD_TYPES: string[];
export declare enum PERIOD_PAGES {
    MAIN = "MAIN",
    YEAR = "YEAR"
}
export declare enum CALENDARPAGE {
    DAY = "DAY",
    MONTH = "MONTH",
    YEAR = "YEAR"
}
export declare const ACTIONS: {
    NEXT_MONTH: string;
    PREV_MONTH: string;
    NEXT_YEAR: string;
    PREV_YEAR: string;
    OPEN_MONTHS: string;
    OPEN_YEARS: string;
    OPEN_MAIN: string;
};
