import React from 'react';
import type { IBadgeProps } from '../badge/interface';
/** Пропы для  FilterButton. */
declare type IFilterButtonProps = Omit<IBadgeProps, 'fill' | 'Icon'>;
export declare const FilterButton: React.FC<IFilterButtonProps>;
export {};
