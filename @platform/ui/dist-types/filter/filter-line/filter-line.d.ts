import React from 'react';
export interface IFilterLineProps {
    text: React.ReactNode;
    children: React.ReactNode;
    labelWidth?: number;
}
export declare const FilterLine: React.FC<IFilterLineProps>;
