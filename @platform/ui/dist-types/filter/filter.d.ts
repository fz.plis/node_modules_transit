import React from 'react';
export interface IFilterProps {
    className?: string;
    offsetTopline?: boolean;
    header?: string;
    children?: React.ReactChildren | React.ReactNode;
    okDisabled?: boolean;
    cancelDisabled?: boolean;
    onClose?(): void;
    onOk?(): void;
    onClear?(): void;
}
export declare const FilterBase: ({ header, children, okDisabled, cancelDisabled, onOk, onClear, onClose, className, }: IFilterProps) => JSX.Element;
export declare const Filter: React.ComponentClass<import("../hoc/as-modal").IModalProps & IFilterProps, any>;
