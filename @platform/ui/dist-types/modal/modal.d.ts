import React from 'react';
export declare const Modal: ({ className, children, extraSmall, ...restProps }: React.HTMLProps<HTMLDivElement> & {
    extraSmall?: boolean | undefined;
}) => JSX.Element;
