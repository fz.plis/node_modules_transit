import React from 'react';
export interface ITagProps extends Omit<React.AllHTMLAttributes<HTMLDivElement>, 'onChange'> {
    children: React.ReactNode;
    isLast?: boolean;
    onRemoveClick?(e: React.MouseEvent<HTMLElement>): void;
    closeRef?: React.RefObject<HTMLDivElement>;
}
export declare class Tag extends React.Component<ITagProps> {
    private handleIconsClick;
    render(): JSX.Element;
}
