import React from 'react';
import type { IOption } from '../interfaces';
export interface IFilterTagsProps extends React.AllHTMLAttributes<HTMLDivElement> {
    tags?: IOption[];
    onRemoveAllTags?(): void;
    onRemoveTag?(value: string): void;
}
export interface IFilterTagsState {
    tags: IOption[];
}
export declare class FilterTags extends React.Component<IFilterTagsProps, IFilterTagsState> {
    static defaultProps: any;
    constructor(props: IFilterTagsProps);
    private handleClick;
    render(): JSX.Element;
}
