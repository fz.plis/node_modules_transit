import React from 'react';
import type { IModalProps } from '../hoc/as-modal';
export interface ILoaderOverlayProps {
    onClose?(): void;
    text?: string;
}
export interface ILoaderDelayProps {
    delay?: number;
}
export interface ILoaderDelayState {
    visible: boolean;
}
export declare const LoaderOverlayComponent: React.ComponentClass<IModalProps & ILoaderOverlayProps, any>;
export declare class LoaderOverlay extends React.Component<ILoaderDelayProps & ILoaderOverlayProps & IModalProps, ILoaderDelayState> {
    static getDerivedStateFromProps(props: ILoaderDelayProps, { visible }: ILoaderDelayState): {
        visible: boolean;
    };
    private static needImmediateRender;
    state: {
        visible: boolean;
    };
    componentDidMount(): void;
    componentDidUpdate(prevProps: ILoaderDelayProps): void;
    private timerId;
    private debounceShow;
    render(): JSX.Element | null;
}
