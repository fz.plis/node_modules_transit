import React from 'react';
import type { IIntersectionOptions } from '../pick';
/**
 * `Props` for `Sentinel` component.
 */
export interface ISentinelProps {
    /**
     * Callback for intersection changes.
     */
    onTrigger?(options: IIntersectionOptions): void;
}
/**
 * Watcher that notify when children appears or dissapears in view port.
 *
 * @example
 * <Sentinel onTrigger={handleTrigger} />
 */
export declare class Sentinel extends React.Component<ISentinelProps> {
    /**
     * @inheritDoc
     */
    static defaultProps: Partial<ISentinelProps>;
    /**
     * @inheritDoc
     */
    componentDidMount(): void;
    /**
     * @inheritDoc
     */
    componentWillUnmount(): void;
    private element?;
    private scrollTarget?;
    private addListeners;
    private removeListeneres;
    private handleScroll;
    render(): JSX.Element;
}
