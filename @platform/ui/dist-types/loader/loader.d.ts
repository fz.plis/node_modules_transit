import React from 'react';
import type { IHorizonProps } from '../atoms';
import { Horizon } from '../atoms';
export declare type ILoaderProps = IHorizonProps;
export declare const Loader: React.MemoExoticComponent<React.ForwardRefExoticComponent<import("../atoms").IHorizonStyleProps & React.AllHTMLAttributes<HTMLDivElement> & React.RefAttributes<Horizon>>>;
