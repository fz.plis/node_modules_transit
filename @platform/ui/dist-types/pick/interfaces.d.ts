/// <reference types="react" />
import type { IOption, IGroup } from '../core';
/**
 * Provides intersection info for triggered element.
 */
export interface IIntersectionOptions {
    /**
     * When `true` indicates that children are in view port.
     */
    readonly isIntersecting: boolean;
    /**
     * Area of intersection with viewport or `null` when children aren't visible.
     */
    readonly intersectionRect?: ClientRect | null;
}
export interface IOptionTemplateProps<T extends IOption = IOption> {
    className?: string;
    active?: boolean;
    selected?: boolean;
    option: T;
    onMouseDown?(e: any): void;
    onClick?(e: any): void;
    withCheckbox?: boolean;
    customLabel?: React.ReactNode;
    ref?: React.Ref<any>;
    indeterminate?: boolean;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export interface ILookupResponse<T = IOption> {
    data: T[];
    total: number;
    groups?: Array<IGroup<any>>;
}
export interface ILookupRequestMetadata {
    offset: number;
    pageSize: number;
    search?: any;
    sort?: any;
}
