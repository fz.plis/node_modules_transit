import React from 'react';
import { Key } from 'ts-keycode-enum';
import type { IOption } from '../core/interfaces';
/**
 * Keys used in navigation.
 */
export declare const handledKeys: Key[];
/**
 * Direction of focus moving for horizontal lists.
 */
export declare enum LOOK_DIRECTION {
    /**
     * Up.
     */
    BACK = -1,
    /**
     * Down.
     */
    NEXT = 1
}
/**
 * Returns next or previous non-disabled option index.
 *
 * @param items Array of available options.
 * @param direction Direction of focus moving.
 * @param current Current focused option index.
 * @returns Index of next focused option.
 */
export declare const nextItemIndex: (items: IOption[], direction: LOOK_DIRECTION, current?: number) => number;
/**
 * Returns option index by value.
 *
 * @param options Array of available options.
 * @param value Current value.
 */
export declare const getActiveIndex: (options: IOption[], value: any, multiSelect?: boolean | undefined) => number;
/**
 * State of ListNavigated HOC.
 *
 * @see ListNavigated
 */
export interface IListNavigatedState {
    activeIndex: number;
}
/**
 * Injected props of ListNavigated HOC.
 *
 * @see ListNavigated
 */
export interface IListNavigatedProps {
    options: IOption[];
    value?: any;
    onChange?(value: any, arg?: any): void;
    onSelectAllOptions?(options?: IOption[]): any;
    onClearAllOptions?(options?: IOption[]): any;
    multiSelect?: boolean;
    dropdownIsOpened?: boolean;
    /**
     * Index of current focused option.
     */
    activeIndex?: number;
    onChangeActiveIndex?(activeIndex: number): void;
    /**
    /**
     * Keyboard handler.
     */
    onKeyDown?: React.KeyboardEventHandler<any>;
    /**
     * Callback for option clicking.
     */
    onSelectOption?(option: IOption, arg?: any): void;
    /**
     * Ref to option node for scroll support.
     */
    optionRef?: any;
    onChangeSearch?(v: string): void;
}
/**
 * ListNavigated HOC provides keyboard navigation behavior through the options in single-select lists.
 */
export declare const withKeyboardNavigation: <TComponentProps extends IListNavigatedProps>(Component: React.ComponentType<TComponentProps>) => React.ComponentType<TComponentProps>;
