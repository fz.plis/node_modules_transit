import React from 'react';
import type { IOption } from '../core';
import type { IIntersectionOptions } from './interfaces';
import type { IListNavigatedProps } from './with-keyboard-navigation';
export interface IWithOptionsProps extends IListNavigatedProps {
    disabled?: boolean;
    withCheckbox?: boolean;
    /**
     * Width of dropdown in pixels.
     */
    dropDownWidth?: number | string;
    /**
     * Maximum dropdown width in pixels
     * defined by design.
     */
    maxWidth?: number | string;
    /**
     * Height of dropdown in pixels.
     */
    dropDownHeight?: number | string;
    /**
     * When `true`, indicates that new page of options is requesting.
     */
    isLoading?: boolean;
    /**
     * When `true`, the dropdownIsOpened property works.
     */
    isOutsideControl?: boolean;
    /**
     * Controls the dropdownIsOpened property.
     */
    onOpen?(): void;
    /**
     * Controls the dropdownIsOpened property.
     */
    onClose?(): void;
    /**
     * When `true`, dropdown with options is visible.
     */
    dropdownIsOpened?: boolean;
    /**
     * Render function for individual option.
     *
     * @param props Props for render.
     */
    optionTemplate?: React.ComponentType<any>;
    /**
     * Callback for watcher. That notify when children appears or dissapears in view port.
     */
    onIntersecting?(options: IIntersectionOptions): void;
    /**
     * Callback for handling selecting of option.
     *
     * @param option Selected item from the list of options.
     */
    onSelectOption?(option: IOption, arg?: any): void;
    searchValue?: string;
    withSearch?: boolean;
    onChangeSearch?(v: string): void;
    spaceSensetive?: boolean;
}
export interface IWithOptionsState {
    /**
     * When `true`, dropdown with options is visible.
     */
    opened: boolean;
}
export declare const openKeys: number[];
export declare abstract class WithOptions<TProps extends IWithOptionsProps, TState extends IWithOptionsState = IWithOptionsState> extends React.Component<TProps, TState> {
    static defaultProps: {
        dropDownHeight: number;
        dropDownWidth: string;
        maxWidth: string;
        onChange: (_?: any) => void;
        onChangeSearch: (_?: any) => void;
        onClose: (_?: any) => void;
        onOpen: (_?: any) => void;
        onSelectAllOptions: (_?: any) => void;
        onSelectOption: (_?: any) => void;
        options: never[];
        searchValue: string;
    };
    anchorRef?: HTMLElement;
    onOpen: () => void;
    onClose: () => void;
    protected abstract getHighlightedPeaceOfLabel(peace: string): React.ReactNode;
    protected handleSelectOption: (option: IOption, e: React.MouseEvent<any>) => void;
    protected handleKeyDown: (e: React.KeyboardEvent<any>) => void;
    protected getLabel: ({ label }: IOption, searchValue?: string) => React.ReactNode;
    protected handleClick: (e: React.MouseEvent<any, MouseEvent>) => void;
    protected handleSelectAllOptions: (e: React.MouseEvent<any>) => void;
    protected handleClearAllOptions: (e: React.MouseEvent<any>) => void;
    protected allOptionsSelected: () => boolean;
    abstract render(): React.ReactNode;
}
