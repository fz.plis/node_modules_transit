import React from 'react';
import type { IOption, ITypingInputProps } from '../core';
import type { IWithOptionsProps } from './with-options';
import type { IWithRemoteOptionsProps } from './with-remote-options';
/**
 * Provides props for lookup extender.
 *
 * @see withLookup
 */
export interface IWithLookupProps extends Omit<IWithRemoteOptionsProps, 'children' | 'filterValue' | 'opened'> {
    /**
     * Callback for handling selecting of option.
     *
     * @param option Selected item from the list of options.
     */
    onSelectOption?(option: IOption, arg?: any): void;
}
/**
 * State of lookup extender.
 *
 * @see lookup
 */
export interface IWithLookupState {
    /**
     * When `true`, dropdown with options is visible.
     */
    opened: boolean;
    focused: boolean;
}
/**
 * Add dropdown with options to input component.
 *
 * When `onChange` is called from input and the length of the value in it exceeds `charCount`
 * it calls `remoteData` and renders `options` from response.
 *
 * Supports lazy load for options on scrolling.
 *
 * Supports options grouping.
 *
 * If another `onChange` will be called in `changeDelay` ms, previous `remoteData` will be omited.
 *
 * If `remoteData` returns no options, dropdown will not shown.
 *
 * @summary Add lookup behavior to inputs.
 */
export declare const withLookup: <TComponentProps extends ITypingInputProps<any, any> & IWithOptionsProps>(WrappedComponent: React.ComponentType<TComponentProps & {
    ref?: any;
}>) => React.ComponentType<Omit<IWithLookupProps & TComponentProps, "options" | "isLoading" | "onIntersecting">>;
