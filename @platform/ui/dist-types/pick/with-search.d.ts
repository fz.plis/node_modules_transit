import React from 'react';
import type { IOption } from '../core';
export interface IWithSearchProps {
    options: IOption[];
    initialValue?: string;
    children(options: IOption[], searchVal: string, onChangeSearch: (value: string) => any): React.ReactElement;
    filterFn?(searchValue: string): IOption[];
}
/**
 * WithSearch HOC provides search options in lists.
 */
export declare const WithSearch: ({ initialValue, options, children, filterFn }: IWithSearchProps) => React.ReactElement<any, string | React.JSXElementConstructor<any>>;
