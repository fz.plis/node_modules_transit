import React from 'react';
import type { IOption, IDebouncer } from '../core';
import type { ILookupRequestMetadata, ILookupResponse, IIntersectionOptions } from './interfaces';
export interface IWithRemoteOptionsParams {
    options: IOption[];
    onIntersecting(options: IIntersectionOptions): void;
    isLoading: boolean;
}
export interface IWithRemoteOptionsProps {
    opened: boolean;
    changeDelay?: number;
    /**
     * Minimal value length for lookup trigger.
     */
    charCount?: number;
    /**
     * Number of items per page.
     */
    pageSize?: number;
    /**
     * Function for request options in lookup.
     *
     * @param options Request options.
     */
    remoteData(options: ILookupRequestMetadata): Promise<ILookupResponse>;
    filterValue: string;
    children(params: IWithRemoteOptionsParams): React.ReactElement;
    onLoad?(resp: ILookupResponse): void;
    remoteParams?: Record<string, unknown>;
}
/**
 * State of withRemoteOptions extender.
 *
 * @see lookup
 */
export interface IWithRemoteOptionsState {
    /**
     * Array of avalaible options.
     */
    options: IOption[];
    /**
     * Options count.
     */
    total: number;
    /**
     * When `true`, indicates that new page of options is requesting.
     */
    loading: boolean;
}
export declare class WithRemoteOptions extends React.Component<IWithRemoteOptionsProps, IWithRemoteOptionsState> {
    static defaultProps: any;
    constructor(props: IWithRemoteOptionsProps);
    state: IWithRemoteOptionsState;
    componentWillReceiveProps(nextProps: IWithRemoteOptionsProps): void;
    componentDidUpdate(prevProps: IWithRemoteOptionsProps): void;
    componentWillUnmount(): void;
    loadData?: IDebouncer;
    clear: () => void;
    handleIntersecting: (options: IIntersectionOptions) => void;
    updateDebouncer: () => void;
    render(): JSX.Element;
}
