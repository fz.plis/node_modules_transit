/**
 * `computeRectIntersection` calcs intersection of two rects.
 *
 * @param rect1 First rect.
 * @param rect2 Second rect.
 * @returns Intersection rect if it exists.
 */
export declare const computeRectIntersection: (rect1: ClientRect, rect2: ClientRect) => ClientRect | null;
/**
 * Returns intersection rect when target is in visible area.
 *
 * @param target Element that checks for intersection in visible area.
 * @param root Element used as container for target.
 * @returns Intersection if it exists.
 */
export declare const getIntersectionRect: (target: HTMLElement, root?: HTMLElement | null | undefined) => ClientRect | null | undefined;
export declare const isTextBox: (element?: Element | undefined) => boolean;
