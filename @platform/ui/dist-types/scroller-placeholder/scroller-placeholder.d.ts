import React from 'react';
export interface IScrollerPlaceholderClick {
    useFilter?: boolean;
    onClick?(): void;
}
export declare const ScrollerPlaceholder: React.FC<IScrollerPlaceholderClick>;
