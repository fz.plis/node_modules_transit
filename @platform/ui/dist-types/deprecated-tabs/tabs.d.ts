import React from 'react';
import type { ISelectGeneralProps } from '../interfaces';
export interface IDepricatedTabsProps<T> extends ISelectGeneralProps<T>, Omit<React.AllHTMLAttributes<HTMLDivElement>, 'onChange' | 'value'> {
}
export declare class DepricatedTabs<T = number | string> extends React.Component<IDepricatedTabsProps<T>> {
    render(): JSX.Element;
}
