import React from 'react';
export declare type ITabItemProps = React.AllHTMLAttributes<HTMLDivElement>;
export declare class TabItem extends React.Component<ITabItemProps> {
    render(): JSX.Element;
}
