export declare const emptyArr: any[];
export interface IVisibleAreaData {
    startIndex: number;
    endIndex: number;
    needLoadMore: boolean;
}
export interface IVisibleAreaProps {
    scrollTop: number;
    rowHeight?: number;
    overscanRowCount: number;
    totalCount: number;
    gridHeight?: number;
    screenHeight: number;
    offsetTop: number;
}
export declare const emptyVisibleAreaData: {
    startIndex: number;
    endIndex: number;
    needLoadMore: boolean;
};
export declare const getVisibleAreaData: ({ scrollTop, rowHeight, overscanRowCount, totalCount, gridHeight, offsetTop, screenHeight, }: IVisibleAreaProps) => IVisibleAreaData;
export declare const getScreenHeight: () => number;
