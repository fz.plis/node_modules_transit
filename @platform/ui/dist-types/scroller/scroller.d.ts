import type { AllHTMLAttributes } from 'react';
import React from 'react';
import type { IRowTemplateProps } from '../interfaces';
import { ScrollYSubContext } from '../sticky-layout';
export interface IScroller extends Omit<AllHTMLAttributes<HTMLDivElement>, 'placeholder' | 'rows'> {
    rows: any[];
    rowTemplate: React.ComponentType<IRowTemplateProps>;
    selectedRows?: any[];
    onChangeSelectedRows?(selectedRows: any[]): void;
    onIntersecting?(): void;
    isLoading?: boolean;
    overscanRowCount?: number;
    subscribeToScrollY?(sub: (scrollY: number) => void): () => void;
    placeholder?: React.ReactNode | string;
}
export declare class Scroller extends React.PureComponent<IScroller> {
    static defaultProps: {
        onChangeSelectedRows: (_?: any) => void;
        overscanRowCount: number;
        rows: never[];
        selectedRows: any[];
    };
    componentDidMount(): void;
    componentWillReceiveProps(props: IScroller): void;
    componentWillUnmount(): void;
    static contextType: React.Context<(sub: (scrollY: number) => void) => () => void>;
    context: React.ContextType<typeof ScrollYSubContext>;
    rowResizeSensor: any;
    gridResizeSensor: any;
    rowRef: any;
    containerRef: any;
    scrollY: number;
    unsubFromScrollY?: () => void;
    startIndex: number;
    endIndex: number;
    rowHeight: number;
    gridHeight: number;
    calculateVisibleAreaData: (props: IScroller, rowHeight: number, gridHeight: number, offsetTop: number) => void;
    rowRenderer: (index: number, isHiddenRow?: boolean) => JSX.Element;
    private handleChangeOfRowHeight;
    private handleChangeOfContainerHeight;
    private handleRowRef;
    private handleContainerRef;
    private selectRow;
    private deselectRow;
    private getOffsetTop;
    render(): JSX.Element;
}
