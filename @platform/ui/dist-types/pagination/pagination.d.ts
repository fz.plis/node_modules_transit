import React from 'react';
export interface IPaginationProps {
    totalPages: number;
    page: number;
    onPageChange(page: number): void;
}
export declare const Pagination: React.FC<IPaginationProps>;
