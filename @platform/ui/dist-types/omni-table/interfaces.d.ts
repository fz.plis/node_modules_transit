import type React from 'react';
import type { VariableSizeList } from 'react-window';
import type { IButtonAction } from '../interfaces';
import type { IHeaderColumn } from './header';
import type { IOmniTableRowColumn, IRowDetailsTemplateProps } from './row';
export declare type IOmniTableColumn<T> = IHeaderColumn & IOmniTableRowColumn<T>;
/** Пропы для выделения строк. */
export interface SelectableRowsProps<T> {
    /** Массив выделенных строк. */
    selectedRows: T[];
    /**
     * Колбек, который исполняется при select/deselect строки,
     * получает новый массив выделенных строк.
     */
    onChangeSelectedRows(rows: T[]): void;
}
/** Пропы для раскрытия строк. */
export interface ExpandableRowsProps<T> {
    /** Компонент для рендера содержимого строки в раскрытом состоянии. */
    RowDetailsTemplate: React.ComponentType<IRowDetailsTemplateProps<T>>;
    /** Массив раскрытых строк. */
    expandedRows: T[];
    /** Функция по данным строки возвращает признак возможности развернуть строку. */
    canRowExpand?(row: T): boolean;
    /**
     * Колбек, который исполняется при сворачивании/разворачивании строки,
     * получает новый массив раскрытых строк.
     */
    onChangeExpandedRows(rows: T[]): void;
}
/** Пропы для ленивой загрузки данных. */
export interface ILazyLoadProps {
    /**
     * Колбэк, вызывающийся, когда доскраливаем до конца таблицы
     * Обычно используется для догрузки данных.
     */
    onIntersecting(): Promise<void>;
    /** Количество строк в одном запросе догрузки.
     *
     * @default 50
     */
    pageSize?: number;
    /** Количестство элементов, которое мы рендерим вне вьюпорта.
     *
     * @default 2
     */
    overscanCount?: number;
}
export interface IOmniTableProps<T> {
    /** Параметры колонок. */
    columns: Array<IOmniTableColumn<T>>;
    /** Массив данных для строк. */
    rows: T[];
    /**
     * Высота таблицы, можно передавать число или css строку.
     *
     * @default 200px.
     */
    height?: React.CSSProperties['height'];
    /** Пропы для выделения строк, если undefined, то строки невыделяемые. */
    selectableProps?: SelectableRowsProps<T>;
    /** Пропы для раскрытия строк, если undefined, то строки нераскрываемые. */
    expandableProps?: ExpandableRowsProps<T>;
    /** Функция для получения действий для строк, принимает текущую строку. */
    getRowActions?(row: T): IButtonAction[];
    /**
     * Текущий статус готовности данных, используется для управления показом спиннера.
     */
    isLoading?: boolean;
    /**
     * Проп для управления ленивой загрузкой данных, если не передать, то ленивая загрузка будет отключена.
     */
    lazyLoadProps?: ILazyLoadProps;
    /** Двойной клик по строке. */
    onRowDoubleClick?(row: T): void;
    /** Функция для получения проп для выделения строчки. */
    onRowClick?(row: T): void;
    /** Элемент, который будет показываться, если в таблице нет записей. */
    placeholder?: React.ReactElement;
    /** Будет ли виртуализирована таблица. */
    virtualized?: boolean;
}
/**
 * Технический интерфейс для данных, которые мы пересылаем в шаблон строки react-window.
 * Все эти переменные находятся внутри OmniTable и там-же описаны, не вижу смысла описать заново каждый проп.
 */
export interface IItemData {
    rowsHeights: React.MutableRefObject<Record<string, number>>;
    listRef: React.RefObject<VariableSizeList>;
    isItemLoaded(index: number): boolean;
}
