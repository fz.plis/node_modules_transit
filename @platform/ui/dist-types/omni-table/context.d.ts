import React from 'react';
import type { positionValues } from 'react-custom-scrollbars';
import type { IOmniTableProps } from './interfaces';
import type { IOmniTableRowProps } from './row';
export interface IOmniTableContext<T = unknown> extends IOmniTableProps<T> {
    /** Колбэк на обновление позиции скролла. */
    onScrollPositionUpdate(values: positionValues): void;
    /** Функция для получения проп для раскрытия строчки. */
    getRowExpandableProps?(row: T): IOmniTableRowProps<T>['expandableProps'];
    /** Функция для получения проп для выделения строчки. */
    getRowSelectableProps?(row: T): IOmniTableRowProps<T>['selectableProps'];
}
export declare const OmniTableContext: React.Context<IOmniTableContext<unknown>>;
