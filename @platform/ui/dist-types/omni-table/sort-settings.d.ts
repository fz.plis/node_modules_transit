import React from 'react';
import type { IOption } from '../core';
import { SORT_DIRECTION } from '../core';
/** Пропы для  SortSettings. */
export interface ISortSettingsProps {
    /** Поля, по которым возможна сортировка. */
    options: IOption[];
    /** Текущее поле сортировки. */
    sortBy?: string;
    /** Направление сортировки. */
    direction?: SORT_DIRECTION;
    /** Колбэк, который исполняется при изменении текущих настроек сортировки.
     *
     * @param fieldName Новое поле сортировки.
     * @param order Новое направление сортировки.
     */
    onChange?(fieldName: string, order: SORT_DIRECTION): void;
    /** Выводимый текст, если поле сортировки не задана, по умолчанию "Не задано". */
    emptySortLabel?: string;
}
/**
 * Компонент, который отображает настройки сортировки
 * если options пустой, то вернет null.
 */
export declare const SortSettings: React.FC<ISortSettingsProps>;
