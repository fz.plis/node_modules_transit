import React from 'react';
import type { ListChildComponentProps } from 'react-window';
import type { IItemData } from './interfaces';
/** Шаблон строки для react-window, мемоизация необходима, чтобы не рендерить всю видимую часть заново, а только ту часть,
 * которая еще не была отрендерена. Должен являться единственным дочерним элементом VariableSizeList-а react-window.
 */
export declare const Row: React.MemoExoticComponent<({ index, style, data: { rowsHeights, listRef, isItemLoaded } }: Omit<ListChildComponentProps, 'data'> & {
    data: IItemData;
}) => JSX.Element | null>;
