import { FILL } from '../atoms';
/** Конфиг для функции getRowBoxFill. */
export interface IRowBoxFillGetterConfig {
    /** Статус выделения строки. */
    isSelected?: boolean;
    /** Статус наведения курсора на строку. */
    isHovered?: boolean;
}
/** Функция для определения цвета заливки на основании статуса строки. */
export declare const getRowBoxFill: ({ isSelected, isHovered }: IRowBoxFillGetterConfig) => FILL;
/** Хук для ререндера компонента. */
export declare const useForceUpdate: () => () => void;
