import React from 'react';
import type { Font } from '../atoms';
/** Параметры колонки. */
export interface IHeaderColumn {
    /** Заголовок.. */
    title?: string;
    /** Горизонтальное выравнивание текста. */
    align?: Font['props']['align'];
    /** Ширина колонки. */
    width?: React.CSSProperties['width'];
}
/** Параметры шапки. */
export interface IOmniTableHeaderProps {
    /** Колонки. */
    columns: IHeaderColumn[];
    /** Функция, которая выполняется при клике на кнопку для сворачивания всех строк. */
    onCollapseAll?(): void;
    /** Параметры чекбокса, если undefined, то чекбокс не рендерится. */
    checkboxProps?: {
        /** Состояние чекбокса. */
        checked?: boolean;
        /** Неопределенное состояние чекбокса. */
        indeterminate?: boolean;
        /** Функция, которая выполняется при клике на чекбокс, получает новое состояние чекбокса. */
        onChange(newValue: boolean): void;
    };
    /** Есть ли у строк действия. */
    hasActions?: boolean;
    /** Массив рефов для колонок. */
    columnsRefs: HTMLDivElement[];
}
/**
 * Шапка таблицы OmniTable.
 *
 * @see https://app.zeplin.io/project/5b6c23b90422817783ccc482/screen/5dd7969ca26e49997b72a6fd
. */
export declare const OmniTableHeader: React.ForwardRefExoticComponent<IOmniTableHeaderProps & React.RefAttributes<HTMLDivElement>>;
