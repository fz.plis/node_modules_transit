import React from 'react';
import type { ICategory } from '../../core';
export interface ICategoryTabProps extends ICategory {
    /** Колбэк на нажатие. */
    onClick?(): void;
    /** Состояние активности категории. */
    active?: boolean;
    /** Показывать ли count после названия категории. */
    showCounts?: boolean;
    /** Экспериментальный дизайн. */
    extraSmall?: boolean;
}
export declare const CategoryTab: React.ForwardRefExoticComponent<ICategoryTabProps & React.RefAttributes<HTMLDivElement>>;
