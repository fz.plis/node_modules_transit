import React from 'react';
import type { ICategory } from '../../core';
export interface ICategoryTabsProps {
    /** Активная категория. */
    category: string;
    /** Данные для категорий. */
    categories: ICategory[];
    /** Колбэк при смене категории. */
    onCategoryChange(value: string, option: ICategory): void;
    /** Показывать ли count после названий категорий. */
    showCounts?: boolean;
    /** Экспериментальный дизайн. */
    extraSmall?: boolean;
}
/** Размер статуса в пикселях. */
export declare const CATEGORY_STATUS_SIZE = 10;
export declare const CategoryTabs: React.FC<ICategoryTabsProps>;
