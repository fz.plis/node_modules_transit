import type { PropsWithChildren } from 'react';
import type { IOmniTableProps } from './interfaces';
/**
 * Таблица Omni-Table.
 *
 * @see http://storybook.gboteam.ru/?path=/story/%D1%82%D0%B0%D0%B1%D0%BB%D0%B8%D1%86%D1%8B-omnitable--omni-table-story
 */
export declare const OmniTable: <T>(props: PropsWithChildren<IOmniTableProps<T>>) => JSX.Element;
