import type { PropsWithChildren } from 'react';
import React from 'react';
import type { Font } from '../atoms';
import type { IButtonAction } from '../interfaces';
/** Параметры колонки. */
export interface IOmniTableRowColumn<T> {
    /** Горизонтальное выравнивание текста. */
    align?: Font['props']['align'];
    /** Ширина колонки. */
    width?: React.CSSProperties['width'];
    /** Метод для селекции данных из строки. */
    selector(row: T): React.ReactNode;
}
/** Параметры компонента row-details. */
export interface IRowDetailsTemplateProps<T> {
    /** Данные строки. */
    row: T;
}
/** Параметры базовой строки. */
export interface IOmniTableRowProps<T> {
    /** Параметры выделяемой строки. */
    selectableProps?: {
        /** Состояние выделения строки. */
        isSelected: boolean;
        /** Колбек, который исполняется при select/deselect строки. */
        onChangeSelected?(row: T): void;
    };
    /** Пропы для сворачивания\разворачивания строки. */
    expandableProps?: {
        /** Развернута ли строка. */
        isExpanded: boolean;
        /** Колбек, который исполняется при смене состояния раскрытия строки. */
        toggleExpanded(row: T): void;
        /** Компонент для рендера содержимого строки в раскрытом состоянии. */
        RowDetailsTemplate: React.ComponentType<IRowDetailsTemplateProps<T>>;
        /** Функция по данным строки возвращает признак возможности развернуть строку. */
        canRowExpand?(row: T): boolean;
    };
    /** Колонки. */
    columns: Array<IOmniTableRowColumn<T>>;
    /** Объект с данными для строки. */
    row: T;
    /** Геттер для массива действий. */
    getActions?(row: T): IButtonAction[];
    /** Клик по строке. */
    onRowClick?(row: T): void;
    /** Двойной клик по строке. */
    onDoubleClick?(row: T): void;
}
/** Строка таблицы OmniTable. */
export declare const OmniTableRowT: <T>(props: PropsWithChildren<IOmniTableRowProps<T>>, ref: React.MutableRefObject<HTMLDivElement | null> | ((instance: HTMLDivElement | null) => void) | null) => JSX.Element;
/** Тип компонента Row. */
declare type OmniTableRowType = <T>(props: PropsWithChildren<IOmniTableRowProps<T>> & {
    ref?: React.MutableRefObject<HTMLDivElement | null> | ((instance: HTMLDivElement | null) => void) | null;
}) => React.ReactElement;
/**
 * При прокидывании рефа теряется генерик интерфейс,
 * поэтому приходиться кастовать явно, чтобы сохранить преимущества генерика.
 * */
export declare const OmniTableRow: OmniTableRowType;
export {};
