import React from 'react';
import type { IActionBarAction } from '../buttons/actions-bar';
export interface IOmniTableToolbarProps extends React.HTMLAttributes<HTMLDivElement> {
    actions?: IActionBarAction[];
    actionsVisibleCount?: number;
}
/**
 * Панель групповых действий для OmniTable.
 *
 * @see https://app.zeplin.io/project/5b6c23b90422817783ccc482/screen/5dd7969ca26e49997b72a6fd
 */
export declare const OmniTableToolbar: React.FC<IOmniTableToolbarProps>;
