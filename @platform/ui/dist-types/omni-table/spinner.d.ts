import React from 'react';
import type { IHorizonProps } from '../atoms';
export declare const Spinner: React.FC<IHorizonProps>;
