import React from 'react';
import type { IHorizonProps } from '../../atoms';
export interface IConfirmationRowProps extends Omit<IHorizonProps, 'label' | 'value'> {
    label: React.ReactNode;
    value: React.ReactNode;
    multiline?: boolean;
}
export declare const ConfirmationRow: React.FC<IConfirmationRowProps>;
