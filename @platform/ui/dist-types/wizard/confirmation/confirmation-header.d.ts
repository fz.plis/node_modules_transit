import React from 'react';
import type { IHorizonProps } from '../../atoms';
import type { ITypographyProps } from '../../typography';
export interface IConfirmationHeaderProps extends IHorizonProps {
    onEditClick?(): void;
}
export declare const ConfirmationHeader: React.FC<IConfirmationHeaderProps>;
export declare const ConfirmationSubHeader: React.FC<ITypographyProps>;
