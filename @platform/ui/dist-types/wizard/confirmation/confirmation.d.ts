import React from 'react';
import type { IWebBoxProps } from '../../atoms';
export interface IConfirmationOption {
    name?: string;
    label: React.ReactNode;
    value: React.ReactNode;
}
export interface IConfirmationProps extends IWebBoxProps {
    options: IConfirmationOption[];
    multiline?: boolean;
}
export declare class Confirmation extends React.Component<IConfirmationProps> {
    static Header: React.FC<import("./confirmation-header").IConfirmationHeaderProps>;
    static SubHeader: React.FC<import("../..").ITypographyProps>;
    render(): JSX.Element;
}
