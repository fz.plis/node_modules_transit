import React from 'react';
import type { IWebBoxProps } from '../../atoms';
import { Horizon } from '../../atoms';
export interface IWizardTabProps extends IWebBoxProps {
    selected: boolean;
    index: number;
}
export declare const WizardTab: React.ForwardRefExoticComponent<IWizardTabProps & React.RefAttributes<Horizon>>;
