import React from 'react';
import type { IHorizonProps } from '../../atoms';
import type { IOption } from '../../interfaces';
export interface IWizardTabsProps<T extends IOption = IOption> extends IHorizonProps {
    value: any;
    options: T[];
    onChange?(value: any, option?: T): void;
}
export interface IWizardTabsState {
    width: number;
    left: number;
}
export declare class WizardTabs extends React.PureComponent<IWizardTabsProps, IWizardTabsState> {
    static defaultProps: any;
    state: {
        width: number;
        left: number;
    };
    componentDidMount(): void;
    componentDidUpdate(prevProps: IWizardTabsProps): void;
    private activeTabElement;
    private isOptionDisabled;
    private getRef;
    private handleRef;
    private calcBarPosition;
    private calcBarStyles;
    render(): JSX.Element;
}
