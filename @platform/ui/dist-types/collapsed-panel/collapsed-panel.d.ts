import React from 'react';
import type { IWebSVGIconProps } from '../icons';
export declare enum TOGGLE_POSITION {
    LEFT = "left",
    RIGHT = "right"
}
declare type ChildrenRenderProp = (args: {
    opened: boolean;
}) => React.ReactNode;
declare type HeaderRenderProp = (opened: boolean) => React.ReactNode;
/** Интерфейс компонента аккордиона. */
export interface ICollapsedPanelProps extends React.AllHTMLAttributes<HTMLDivElement> {
    /** Заголовок аккордиона. Можно получить стейт аккордиона, вызывая как функцию. */
    header?: HeaderRenderProp | React.ReactNode;
    /** Первоначальный стейт раскрытия аккордиона. По дефолту true. */
    togglePosition?: TOGGLE_POSITION;
    /** Контент, который врапит аккордион. */
    children?: ChildrenRenderProp | React.ReactNode;
    /** Инверсия компонента. */
    inversed?: boolean;
    /** Кастомный класснейм. */
    summaryClassName?: string;
    /** Первоначальный стейт раскрытия аккордиона. По дефолту true. */
    expanded?: boolean;
    /** Отменяет полупрозрачность хедера. */
    solid?: boolean;
    /** Раскрывает контент вверх, а не вниз. */
    collapseToTop?: boolean;
    /** Иконка с поворотом на 180 градусов. */
    circularArrow?: boolean;
    /** Задает размер иконки стрелки. По дефолту MD. */
    arrowScale?: IWebSVGIconProps['scale'];
    /** Задает цвет иконки стрелки. По дефолту BASE. */
    arrowFill?: IWebSVGIconProps['fill'];
}
/**
 * Компонент аккордиона.
 *
 * @example
    <CollapsedPanel
      solid
      collapseToTop
      expanded={false}
      header={isVisible => isVisible ? locale.debt.card.accordeon.closed : locale.debt.card.accordeon.open}
      togglePosition={TOGGLE_POSITION.RIGHT}
    >
      <Content />
    </CollapsedPanel>
 *
 * @deprecated - Следует использовать Details компонент.
 * @see {@link Details}
 */
export declare const CollapsedPanel: React.FC<ICollapsedPanelProps>;
export {};
