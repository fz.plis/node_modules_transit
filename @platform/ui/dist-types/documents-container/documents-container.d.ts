import React from 'react';
import type { IBoxWebStyleProps } from '../atoms';
export interface IContentDocumentsContainer extends IBoxWebStyleProps {
    details: React.ReactNode;
    deleted?: boolean;
    onDeleteClick?(): void;
    header?: string;
    error?: string;
    expanded?: boolean;
    onChangeExpanded?(): void;
    disabled?: boolean;
}
export declare const DocumentsContainer: React.FC<IContentDocumentsContainer>;
