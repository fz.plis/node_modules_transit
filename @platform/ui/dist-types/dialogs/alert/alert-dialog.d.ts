import React from 'react';
import { BUTTON, DIALOG_TYPE } from '../../interfaces';
export interface IAlertDialogProps {
    onClose(): void;
    content: React.ReactNode;
    onOk(): void;
    additionalContent?: React.ReactNode;
    header?: React.ReactNode;
    okButtonText?: string;
    dialogType?: DIALOG_TYPE;
}
export declare class AlertDialog extends React.Component<IAlertDialogProps> {
    static defaultProps: any;
    buttons: {
        label: string;
        name: string;
        buttonType: BUTTON;
        onClick: () => void;
    }[];
    render(): JSX.Element;
}
