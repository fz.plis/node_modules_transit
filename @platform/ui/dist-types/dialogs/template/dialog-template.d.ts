import React from 'react';
import type { IWebSpecialIconProps } from '../../icons';
import type { IButtonAction } from '../../interfaces';
export interface IDialogTemplateProps {
    icon?: React.ComponentType<IWebSpecialIconProps>;
    header: React.ReactNode;
    content: React.ReactNode;
    actions?: IButtonAction[];
    footerAddon?: React.ReactNode;
    onClose?(): void;
    additionalContent?: React.ReactNode;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare class DialogTemplate extends React.Component<IDialogTemplateProps> {
    render(): JSX.Element;
}
