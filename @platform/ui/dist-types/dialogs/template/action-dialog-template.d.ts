import React from 'react';
import type { IDialogTemplateProps } from './dialog-template';
export declare class ActionDialogTemplate extends React.Component<IDialogTemplateProps> {
    render(): JSX.Element;
}
