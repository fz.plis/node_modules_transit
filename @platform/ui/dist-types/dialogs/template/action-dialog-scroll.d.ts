import React from 'react';
export interface IDialogTemplateState {
    contentHeight: number;
}
export declare class ActionDialogScroll extends React.Component<Record<string, unknown>, IDialogTemplateState> {
    state: {
        contentHeight: number;
    };
    componentDidMount(): void;
    private refEl;
    render(): JSX.Element;
}
