import React from 'react';
import { BUTTON } from '../../interfaces';
import type { IAlertDialogProps } from '../alert';
export declare type ISuccessDialogProps = IAlertDialogProps;
export declare class SuccessDialog extends React.Component<ISuccessDialogProps> {
    static defaultProps: any;
    buttons: {
        label: string;
        name: string;
        buttonType: BUTTON;
        onClick: () => void;
    }[];
    render(): JSX.Element;
}
