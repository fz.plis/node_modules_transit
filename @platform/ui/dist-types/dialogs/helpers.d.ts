import { DIALOG_TYPE } from '../interfaces';
export declare const getIconByDialogType: (dialogType: DIALOG_TYPE) => import("../icons").WebSpecialIcon;
