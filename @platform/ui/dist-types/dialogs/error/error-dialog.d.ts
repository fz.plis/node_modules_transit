import React from 'react';
import type { IErrorParams } from '../../interfaces';
import { BUTTON } from '../../interfaces';
export interface IErrorDialogProps extends IErrorParams {
    content: React.ReactNode;
}
export declare class ErrorDialog extends React.Component<IErrorDialogProps> {
    static defaultProps: any;
    buttons: {
        label: string;
        name: string;
        buttonType: BUTTON;
        onClick: () => void;
    }[];
    render(): JSX.Element;
}
