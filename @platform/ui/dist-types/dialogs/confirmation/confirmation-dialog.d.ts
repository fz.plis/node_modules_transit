import React from 'react';
import { BUTTON } from '../../interfaces';
import type { IAlertDialogProps } from '../alert';
export interface IConfirmationDialogProps extends IAlertDialogProps {
    onCancel(): void;
    cancelButtonText?: string;
}
export declare class ConfirmationDialog extends React.Component<IConfirmationDialogProps> {
    static defaultProps: any;
    buttons: {
        label: string;
        name: string;
        buttonType: BUTTON;
        onClick: () => void;
    }[];
    render(): JSX.Element;
}
