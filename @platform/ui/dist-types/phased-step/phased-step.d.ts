import React from 'react';
import type { IWebBoxProps } from '../atoms';
export interface IPhasedStepIconProps {
    selected: boolean;
    passed: boolean;
    hasError?: boolean;
    hasWarning?: boolean;
}
export declare type IPhasedStepProps = IPhasedStepIconProps & IWebBoxProps;
export declare const PhasedStepIcon: React.FC<IPhasedStepIconProps>;
export declare const PhasedStep: React.FC<IPhasedStepProps>;
