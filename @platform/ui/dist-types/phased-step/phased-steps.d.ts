import React from 'react';
import type { IHorizonProps } from '../atoms';
import type { IOption } from '../interfaces';
export interface IPhasedStepsOption extends IOption {
    hasError?: boolean;
    hasWarning?: boolean;
}
export interface IPhasedStepsProps extends IHorizonProps {
    /**
     * Current step value.
     */
    value: any;
    /**
     * Last value of passedStep.
     */
    lastPassed?: any;
    options: IPhasedStepsOption[];
    onChange?(value: any, option?: IPhasedStepsOption): void;
}
export declare const PhasedSteps: React.FC<IPhasedStepsProps>;
