import React from 'react';
/**
 * Creates new component which consume props from `Context`.
 */
export declare const withContext: <TContext, TProps extends TContext>(Context: React.Context<TContext>) => (WrappedComponent: React.ComponentType<TProps>) => React.ComponentType<Omit<TProps, keyof TContext>>;
