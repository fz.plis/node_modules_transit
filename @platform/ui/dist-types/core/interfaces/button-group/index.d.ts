import type { IButtonProps, BUTTON_SIZE, BUTTON_VARIANT } from '../button';
export interface IButtonGroupProps<TIconProps extends Record<string, unknown> = Record<string, unknown>> {
    visibleCount?: number;
    buttons?: Array<IButtonProps<TIconProps>>;
    buttonType?: keyof typeof BUTTON_VARIANT;
    weight?: keyof typeof BUTTON_SIZE;
    onClickButton(name: string, eventArg: any): any;
}
