import type React from 'react';
export interface IOption<T = any> {
    label: string;
    value: T;
    disabled?: boolean;
    className?: string;
}
export interface IActionButton {
    disabled?: boolean;
    onClick(): any;
    label?: string;
    name: string;
}
export declare type IIndexable = Record<string, any>;
export interface IHOC<TProps> extends React.ComponentClass<TProps> {
    /**
     * Компонент переданный в HOC.
     */
    WrappedComponent: React.ComponentType<TProps>;
}
