/// <reference types="react" />
import type { FILL } from '../../../atoms';
import type { IOption, IActionButton } from '../common';
export declare enum SORT_DIRECTION {
    ASC = "asc",
    DESC = "desc"
}
export interface ISortField {
    name: string;
    label: string;
}
export interface IGroupRow {
    value: number | string;
    data: any[];
}
export interface IGroupHeaderTemplateProps {
    group: IGroupRow;
    closed?: boolean;
    value: number | string;
    onClick?(closed: boolean): void;
}
export interface ICategory extends IOption<string> {
    count?: number;
    /** Цвет заливки для статуса категории.  */
    status?: FILL;
}
export interface IScrollerDate {
    date: Date | string;
    showYear?: boolean;
}
export interface IScrollerTabs {
    options: ICategory[];
    value: string;
    onChange(value: string, option: ICategory): void;
}
export interface ISummaryInfo {
    options: IOption[];
}
export interface IRowTemplateProps {
    row: any;
    checked?: boolean;
    onChangeChecked?(value: boolean): void;
    expanded?: boolean;
    onChangeExpanded?(expanded: boolean): void;
}
export interface IScrollerBaseProps<TRow = any> {
    rowTemplate: React.ComponentType<TRow>;
    selectedRows?: any[];
    isLoading?: boolean;
    placeholder?: React.ReactNode;
    onIntersecting?(): void;
    onChangeSelectedRows?(selectedRows: any[]): void;
}
export interface IScroller<TTEmplateProps extends IRowTemplateProps = IRowTemplateProps> extends IScrollerBaseProps<TTEmplateProps> {
    rows: any[];
}
export interface IScrollerSort {
    sortFields?: ISortField[];
    sortBy?: string;
    sortDirection?: SORT_DIRECTION;
    onChangeSort?(fielName: string, order: SORT_DIRECTION): void;
}
export interface IScrollerToolbar extends IScrollerSort {
    actions?: IActionButton[];
    actionsVisibleCount?: number;
    checkboxValue?: boolean;
    checkboxIndeterminate?: boolean;
    onChangeCheckbox?(val: boolean): void;
}
export interface IGroupedScrollerProps<TTEmplateProps extends IRowTemplateProps = IRowTemplateProps> extends IScrollerBaseProps<TTEmplateProps> {
    groups?: IGroupRow[];
    expandedRows?: any[];
    closedGroups?: Array<number | string>;
    onChangeClosedGroups?(closedGroups: Array<number | string>): void;
    onChangeExpandedRows?(expandedRows: any[]): void;
    groupHeaderTemplate: React.ComponentType<IGroupHeaderTemplateProps>;
}
export interface IScrollerSelectedItemStatus {
    /**
     * Количество выбранных объектов.
     */
    count: number;
    /**
     * Если `true`, значит выбраны все элементы.
     */
    checkboxValue: boolean;
    /**
     * Callback на действие в статусбаре.
     */
    onChangeCheckbox(val: boolean): void;
}
export interface IGroup<T> {
    name: string;
    values: T[];
}
