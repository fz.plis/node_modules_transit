import type React from 'react';
import type { IActionButton } from '../common';
export declare enum BUTTON_VARIANT {
    PRIMARY = "PRIMARY",
    REGULAR = "REGULAR"
}
export declare enum BUTTON_SIZE {
    BASE = "BASE",
    SMALL = "SMALL"
}
export interface IButtonProps<TIconProps extends Record<string, unknown>> extends Omit<IActionButton, 'label' | 'name' | 'onClick'> {
    variant?: keyof typeof BUTTON_VARIANT;
    weight?: keyof typeof BUTTON_SIZE;
    label?: React.ReactNode;
    icon?: React.ComponentType<TIconProps>;
    onClick?(event: any): void;
    name?: string;
}
