/// <reference types="react" />
import type { IOption } from '../common';
/**
 * Базовый интерфейс пропертей для всех компонентов ввода информации (селекты, тексты, чекбоксы и т.д.).
 * Имеет 2 дженерик параметра
 * `T` - тип значения с которым работает компонент (по умолчанию `string`)
 * `TArgs` - тип второго аргумента в коллбэке для изменения значения элемента (`onChange`, по умолчанию `never`).
 */
export interface IInputBaseProps<T = string, TArg = never> {
    /**
     * Текущее значение компонента.
     */
    value?: T;
    /**
     * Метод для изменения значения компонента.
     *
     * @param value Новое значение компонента.
     * @param arg Второй параметр коллбэка типа `TArg`.
     */
    onChange?(value: T, arg?: TArg): void;
    /**
     * Флаг позволяющий указывать активен компонент или нет (позволяет ли он менять свое значение).
     */
    disabled?: boolean;
    /**
     * Флаг указывающий наличие ошибки связанной с данным компонентом ввода. Если `true` значит ошибка есть, иначе нет.
     */
    hasError?: boolean;
    /**
     * Флаг указывающий наличие предупреждения связанного с данным компонентом ввода.
     * Если `true` значит предупреждение есть, иначе нет.
     */
    hasWarning?: boolean;
    /**
     * Имя элемента ввода на форме.
     */
    name?: string;
}
export interface IInputWithValidation {
    errorText?: string;
    warningText?: string;
    touched?: boolean;
}
/**
 * Базовый интерфейс пропертей для комопнентов формы которые позволяют вводить значение с клавиатуры
 * Имеет 2 дженерик параметра
 * `T` - тип значения с которым работает компонент
 * `TEvent` - тип элемента на котором будут срабатывать события (`onChange`, `onFocus`, `onBlur`)
 * (по умолчанию `HTMLInputElement`).
 */
export interface ITypingInputProps<T, TEvent = HTMLInputElement> extends IInputBaseProps<T, React.SyntheticEvent<TEvent>> {
    placeholder?: string;
    label?: string;
    onFocus?(e: any): void;
    onBlur?(e: any): void;
}
/**
 * Базовый интерфейс пропертей для комопнентов выбора из списка
 * Имеет 2 дженерик параметра
 * `T` - тип значения с которым работает компонент
 * `TEvent` - тип элемента на котором будут срабатывать события (`onChange`, `onFocus`, `onBlur`)
 * (по умолчанию `HTMLDivElement`).
 */
export interface ISelectBaseProps<T, TEvent = HTMLDivElement> {
    options: Array<IOption<T>>;
    onFocus?(e: React.FocusEvent<TEvent>): void;
    onBlur?(e: React.FocusEvent<TEvent>): void;
}
/**
 * Базовый интерфейс пропертей для комопнентов выбора из списка одного значения
 * `T` - тип значения с которым работает компонент.
 */
export interface ISelectGeneralProps<T = string> extends ISelectBaseProps<T>, IInputBaseProps<T, IOption<T>> {
}
/**
 * Базовый интерфейс пропертей для комопнентов выбора из списка множества значений
 * `T` - тип значения с которым работает компонент.
 */
export interface IMultiSelectGeneralProps<T = string> extends ISelectBaseProps<T>, IInputBaseProps<T[], Array<IOption<T>>> {
}
