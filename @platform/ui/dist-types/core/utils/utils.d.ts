import React from 'react';
export declare const fake: (..._: any[]) => any;
export declare const makeFake: <TProps>() => React.ComponentType<TProps>;
export interface ICheckChildrendParams {
    types: any[];
    single?: boolean;
    notAllowEmpty?: boolean;
}
export declare const childrenIterator: (childrenInput: React.ReactNode) => Iterable<React.ReactNode>;
export declare const getChildrenChecker: (params: ICheckChildrendParams) => (children: React.ReactNode) => boolean;
export declare const noop: (_?: any) => void;
export declare const mergeRefs: <T>(...refs: React.Ref<T>[]) => (ref: T | null) => void;
