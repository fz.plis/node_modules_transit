/**
 * Type of any function that return `void`.
 */
export declare type Procedure = (...args: any[]) => void;
/**
 * Interface that has posibility to cancel task.
 */
export interface IDebouncer extends Procedure {
    /**
     * Function that cancel current task.
     */
    cancel(): void;
    /**
     * Calls function immediatly.
     */
    flush(): void;
}
/**
 * A wrapper that defer `func` call to `timeout` milliseconds.
 * "Superfluous" calls overwrite previous pending jobs.
 * All arguments and context are transferred.
 *
 * @param func Wrapped function.
 * @param timeout Defers time.
 * @returns Debouncer of `func`.
 *
 * @summary Wraps `func` and return debouncer.
 *
 * @example
 * const log = debounce(console.log)
 * log('first')
 * log('second') // prints only 'second'
 * @example
 * const log = debounce(console.log)
 * log('first')
 * log.cancel() // log not called
 * @example
 * const log = debounce(console.log)
 * log('first')
 * log('second')
 * log.flush() // immediatly calls log and prints 'second'
 */
export declare const debounce: <F extends Procedure>(func: F, timeout?: number) => IDebouncer;
/**
 * A wrapper that defer call `fn` no more than once in `wait` milliseconds.
 * "Superfluous" calls overwrite previous pending jobs.
 * All arguments and context are transferred.
 *
 * @param fn Wrapped function.
 * @param wait Defers time.
 * @returns Debouncer of `func`.
 *
 * @summary Wraps `fn` and return throttled func.
 *
 * @example
 * const log = throttle(console.log)
 * log('first') // prints 'first'
 * log('second') // console.log not called
 * log('third') // prints only 'second' after timeout
 * @example
 * const log = throttle(console.log)
 * log('first') // prints 'first'
 * log('second')
 * log.cancel() // log not called
 * @example
 * const log = debounce(console.log)
 * log('first') // prints 'first'
 * log('second')
 * log.flush() // immediatly calls log and prints 'second'
 */
export declare const throttle: <F extends Procedure>(fn: F, wait?: number) => IDebouncer;
