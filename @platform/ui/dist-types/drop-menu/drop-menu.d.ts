import React from 'react';
import type { IBoxWebStyleProps } from '../atoms';
import type { IButtonAction } from '../interfaces';
import { CONTAINER_POSITION } from '../interfaces';
export interface IDropMenuProps extends IBoxWebStyleProps {
    /** Массив действий. */
    actions?: IButtonAction[];
    /** Отображение серой подложки. */
    withBackground?: boolean;
    /** Заблокировано. */
    disabled?: boolean;
    /** Расположение выпадающего списка относительно кнопки.
     *
     * @default
     * ['BOTTOM', 'TOP']
     * */
    positioningOrder?: Array<keyof typeof CONTAINER_POSITION>;
    /** Кол-во отображаемых без скролла элементов.
     *
     * @default
     * 5
     * */
    visibleCount?: number;
}
/**
 * Компонент для отображения меню быстрых действий.
 *
 * {@link https://app.zeplin.io/project/5b6c23b90422817783ccc482/screen/5db7055ae9d98c2c63cf213c}.
 * {@link https://holism.gpbdev.ru/uikit/?component=Drop%20Menu}.
 *
 * @param props Свойства.
 * @param props.actions Массив действий.
 * @param props.withBackground Отображение серой подложки.
 * @param props.disabled Заблокировано.
 * @param props.positioningOrder Расположение выпадающего списка относительно кнопки.
 * @param props.visibleCount Кол-во отображаемых без скролла элементов.
 */
export declare const DropMenu: React.FC<IDropMenuProps>;
