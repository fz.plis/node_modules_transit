import React from 'react';
import type { IButtonAction } from '../interfaces';
interface IDropMenuItemProps {
    action: IButtonAction;
    toggleOpen(toggleStatus: boolean): void;
}
export declare const DropMenuItem: React.FC<IDropMenuItemProps>;
export {};
