import React from 'react';
import type { IIndexable } from '../interfaces';
export interface IMapState {
    center: number[];
    zoom: number;
}
export interface IPlacemark {
    address: string;
    coordinates: number[];
    hintContent: string;
    balloon: IIndexable;
    value: string;
}
export interface IYaMapsProps {
    locality?: string;
    mapState?: IMapState;
    width?: number | string;
    height?: number | string;
    placemarks?: IPlacemark[];
    placemarksIcon?: any;
    value?: string;
    placemarkClick?(value: string): void;
    balloonContentHeaderRender?(header: string): string;
    balloonContentBodyRender?(body: string): string;
}
export interface IYaMapsState {
    center: [number, number];
}
export declare class YaMaps extends React.Component<IYaMapsProps, IYaMapsState> {
    static defaultProps: any;
    constructor(props: any);
    componentDidUpdate({ locality, value }: {
        locality: string;
        value: string;
    }): void;
    private ymaps;
    private ymapInstance;
    instanceRef: (ymaps: {
        behaviors: {
            disable(param: string): void;
        };
    }) => void;
    center: () => void;
    onInit: (ymaps: any) => void;
    render(): JSX.Element;
}
