import React from 'react';
export declare type IListProps = React.HTMLProps<HTMLUListElement>;
export declare const List: React.FC<IListProps>;
