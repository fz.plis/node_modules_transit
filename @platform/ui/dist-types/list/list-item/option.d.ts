import React from 'react';
import type { IOptionTemplateProps } from '../../pick';
export declare const Option: React.FC<IOptionTemplateProps>;
