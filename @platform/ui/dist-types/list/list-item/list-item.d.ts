import React from 'react';
export interface IListItemProps extends React.AllHTMLAttributes<HTMLDivElement> {
    active?: boolean;
    disabled?: boolean;
    selected?: boolean;
    isLast?: boolean;
    dimension?: 'LG' | 'MD' | 'SM';
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const ListItem: React.ForwardRefExoticComponent<IListItemProps & React.RefAttributes<HTMLDivElement>>;
