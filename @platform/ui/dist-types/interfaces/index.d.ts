/// <reference types="react" />
import type { IInputBaseProps, IOption } from '../core';
import type { WebIcon } from '../icons/icon';
import type { IWithValidationCallbacks } from '../inputs';
export declare enum DOCUMENT_STATUS_TYPE {
    ERROR = "ERROR",
    SUCCESS = "SUCCESS",
    INPROGRESS = "INPROGRESS",
    NEW = "NEW"
}
export declare enum DIALOG_TYPE {
    ERROR = "ERROR",
    WARNING = "WARNING",
    SUCCESS = "SUCCESS"
}
export declare enum BUTTON {
    PRIMARY = "PRIMARY",
    REGULAR = "REGULAR"
}
export { CLOSE_REASON, CONTAINER_POSITION } from '../floating';
export { IOption, IInputBaseProps, IIndexable, IHOC, IRowTemplateProps } from '../core';
/**
 * Базовый интерфейс пропертей для комопнентов формы которые позволяют вводить значение с клавиатуры
 * Имеет 2 дженерик параметра
 * `T` - тип значения с которым работает компонент
 * `TEvent` - тип элемента на котором будут срабатывать события (`onChange`, `onFocus`, `onBlur`)
 * (по умолчанию `HTMLInputElement`).
 */
export interface ITypingInputProps<T, TEvent = HTMLInputElement> extends IInputBaseProps<T, React.SyntheticEvent<TEvent>> {
    placeholder?: string;
    /**
     * Max char count in input.
     */
    maxLength?: number;
    /**
     * Disables trim spaces when blur.
     */
    disableTrim?: boolean;
    onFocus?(e: React.FocusEvent<TEvent>): void;
    onBlur?(e: React.FocusEvent<TEvent>): void;
}
/**
 * Базовый интерфейс пропертей для комопнентов выбора из списка
 * Имеет 2 дженерик параметра
 * `T` - тип значения с которым работает компонент
 * `TEvent` - тип элемента на котором будут срабатывать события (`onChange`, `onFocus`, `onBlur`)
 * (по умолчанию `HTMLDivElement`).
 */
export interface ISelectBaseProps<T, TOption = IOption<T>, TEvent = HTMLDivElement> {
    options: TOption[];
    onFocus?(e: React.FocusEvent<TEvent>): void;
    onBlur?(e: React.FocusEvent<TEvent>): void;
}
/**
 * Базовый интерфейс пропертей для комопнентов выбора из списка одного значения
 * `T` - тип значения с которым работает компонент.
 */
export interface ISelectGeneralProps<T = string, TOption = IOption<T>> extends ISelectBaseProps<T, TOption>, IInputBaseProps<T, TOption> {
}
/**
 * Базовый интерфейс пропертей для комопнентов выбора из списка множества значений
 * `T` - тип значения с которым работает компонент.
 */
export interface IMultiSelectGeneralProps<T = string, TOption = IOption<T>> extends ISelectBaseProps<T, TOption>, IInputBaseProps<T[], TOption[]> {
}
/**
 * Provides ref function for wrapped dom node.
 *
 * Passing ref to wrapped component will give a ref to the wrapper, not to DOM node.
 * To get a ref to wrapped DOM node, pass innerRef prop.
 */
export interface IInnerRef<T = any> {
    /**
     * Ref object.
     */
    innerRef?: React.Ref<T>;
}
/**
 * Принимает в себя опциональные Date строки в формате ISO
 * (new Date(2020, 3, 20)).toISOString().
 */
export interface ICalendarDateProps {
    /** Максимально возможная дата. */
    maxDate?: string;
    /** Минимально возможная дата. */
    minDate?: string;
}
export declare enum NOTIFICATION_TYPE {
    ERROR = "ERROR",
    INFO = "INFO",
    SUCCESS = "SUCCESS",
    WARNING = "WARNING"
}
export declare type Action = (name: any) => Promise<any> | void;
export interface IButtonAction {
    icon?: WebIcon;
    disabled?: boolean;
    name: string;
    label: string;
    onClick: Action;
    buttonType?: BUTTON;
}
export interface IActionListAction extends IButtonAction {
    children?: IButtonAction[];
}
export interface IBreadcrumb {
    disabled?: boolean;
    onClick(): void;
    label: string;
    tooltip?: React.ReactNode;
}
export interface IInputWithValidation extends IWithValidationCallbacks {
    errorText?: string;
    warningText?: string;
    touched?: boolean;
}
export interface IAlertExtraParams {
    /** Заголовок модального окна. */
    header?: React.ReactNode;
    /** Хендлер закрытия модального окна. */
    onClose?(): void;
    /** Хендлер нажатия на 'Ок'. */
    onOk?(): void;
    /** Текст кнопки ознакомления с контентом (по умолчанию 'Ок'). */
    okButtonText?: string;
    /** Дополнительный контент (отображается под основным). */
    additionalContent?: React.ReactNode;
    /** Тип диалога (ошибка, предупреждение или успех). */
    dialogType?: DIALOG_TYPE;
}
export interface IConfirmationExtraParams extends IAlertExtraParams {
    /** Хендлер нажатия 'Отмены'. */
    onCancel?(): void;
    /** Тект кнопки 'Отмены'. */
    cancelButtonText?: string;
}
export declare type IErrorParams = IConfirmationExtraParams;
export declare type OpenModalFunc = <TProps extends {
    onClose(...args: any[]): void;
}>(
/**
 * Наименования модального  окна.
 */
name: string, 
/**
 * Отображаемый компонент.
 */
component: React.ComponentType<TProps>, 
/**
 * Дополнительные свойста модального окна.
 */
props?: Omit<TProps, 'onClose'>, 
/**
 * Функция закрытия модального окна.
 */
onClose?: (...args: any[]) => void) => void;
export interface IDialogMaster {
    show: OpenModalFunc;
    showAlert(content: React.ReactNode, params?: IAlertExtraParams): void;
    showConfirmation(content: React.ReactNode, onOk: () => void, params?: IConfirmationExtraParams): void;
    showSuccessAlert(content: React.ReactNode, params?: IAlertExtraParams): void;
    showError(content: React.ReactNode, params?: IErrorParams): void;
}
