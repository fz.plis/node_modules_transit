import React from 'react';
import type { ISortField } from './constants';
import { SORT_DIRECTION } from './constants';
export interface ISortGroupProps {
    sortFields?: ISortField[];
    sortBy?: string;
    direction?: SORT_DIRECTION;
    onChange?(fielName: string, order: SORT_DIRECTION): void;
}
export declare class SortGroup extends React.Component<ISortGroupProps> {
    static defaultProps: {
        direction: SORT_DIRECTION;
        onChange: (_?: any) => void;
        sortBy: string;
        sortFields: never[];
    };
    private handleSelectChange;
    private handleSortIconClick;
    render(): JSX.Element | null;
}
