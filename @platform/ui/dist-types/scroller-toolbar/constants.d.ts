/// <reference types="react" />
export interface ISelectedRowsInfoProps extends React.AllHTMLAttributes<HTMLSpanElement> {
    text?: string;
}
export interface ISortField {
    name: string;
    label: string;
}
export declare enum SORT_DIRECTION {
    ASC = "asc",
    DESC = "desc"
}
