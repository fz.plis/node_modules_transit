import React from 'react';
import type { ISelectedRowsInfoProps } from './constants';
export declare const SelectedRowsInfo: React.FC<ISelectedRowsInfoProps>;
