import React from 'react';
import type { IActionListAction } from '../interfaces';
import type { SORT_DIRECTION, ISortField } from './constants';
export interface IScrollerToolbarProps extends React.HTMLAttributes<HTMLDivElement> {
    info?: React.ReactNode;
    actionsVisibleCount?: number;
    actions?: IActionListAction[];
    sortFields?: ISortField[];
    sortBy?: string;
    sortDirection?: SORT_DIRECTION;
    onChangeSort?(fielName: string, order: SORT_DIRECTION): void;
    checkboxValue?: boolean;
    checkboxIndeterminate?: boolean;
    onChangeCheckbox?(val: boolean): void;
    showCheckbox?: boolean;
}
export declare class ScrollerToolbar extends React.Component<IScrollerToolbarProps> {
    render(): JSX.Element;
}
