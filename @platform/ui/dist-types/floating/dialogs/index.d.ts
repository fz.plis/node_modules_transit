import React from 'react';
export declare type Diff<T extends string, U extends string> = (Record<string, never> & {
    [P in T]: P;
} & {
    [P in U]: never;
})[T];
export declare type Omit<T, K extends keyof T> = Pick<T, Diff<keyof T, K>>;
export interface IAlertExtraParams {
    header?: string;
    onClose?(): void;
    onOk?(): void;
    okButtonText?: string;
}
export interface IConfirmationExtraParams extends IAlertExtraParams {
    onCancel?(): void;
    cancelButtonText?: string;
}
export declare type OpenModalFunc = <TProps extends {
    onClose(): void;
}>(name: string, component: React.ComponentType<TProps>, props?: Omit<TProps, 'onClose'>, onClose?: () => void) => void;
export interface IDialogMaster {
    show: OpenModalFunc;
    showAlert(content: string, params?: IAlertExtraParams): void;
    showConfirmation(content: string, onOk: () => void, params?: Omit<IConfirmationExtraParams, 'onOk'>): void;
}
export interface IAlertDialogProps extends IAlertExtraParams {
    content: React.ReactNode;
}
export interface IConfirmationDialogProps extends IConfirmationExtraParams {
    content: React.ReactNode;
}
export declare const getDialogMaster: (AlertDialog: React.ComponentType<IAlertDialogProps>, ConfirmationDialog: React.ComponentType<IConfirmationDialogProps>) => IDialogMaster;
