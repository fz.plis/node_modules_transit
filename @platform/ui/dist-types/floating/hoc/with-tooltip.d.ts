import React from 'react';
import { CONTAINER_POSITION } from '../container';
export interface ICalcPos {
    position: CONTAINER_POSITION;
}
export interface IWithTooltipProps {
    visible?: boolean;
    text?: string;
    ref?: React.RefObject<HTMLElement>;
    tooltipMargin?: number;
    onClickOutside?(): void;
    positioningOrder?: Array<keyof typeof CONTAINER_POSITION>;
    tooltip(pos: ICalcPos): React.ReactNode;
    children(ref: React.RefObject<any>, show: () => void, hide: () => void): React.ReactNode;
}
export declare const WithTooltip: React.SFC<IWithTooltipProps>;
