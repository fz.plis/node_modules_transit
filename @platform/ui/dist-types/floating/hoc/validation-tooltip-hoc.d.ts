import React from 'react';
import type { IInputWithValidation } from '../../core';
import type { CONTAINER_POSITION } from '../container';
export interface IWithValidationTooltipState {
    hovered: boolean;
    editing: boolean;
    focused: boolean;
}
export interface ITooltipProps {
    position?: CONTAINER_POSITION;
    onMouseEnter?(...args: any[]): void;
    onMouseLeave?(...args: any[]): void;
    children?: React.ReactNode;
}
export declare const getWithValidationTooltip: <TTooltipProps extends ITooltipProps>(TooltipInput: React.ComponentType<TTooltipProps>, withTooltipParams?: {
    tooltipMargin?: number | undefined;
    positioningOrder?: CONTAINER_POSITION[] | undefined;
} | undefined) => <TProps>(ComponentInput: React.ComponentType<TProps>) => React.ComponentType<IInputWithValidation & TProps>;
