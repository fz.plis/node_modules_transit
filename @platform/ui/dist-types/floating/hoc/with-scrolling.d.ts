import React from 'react';
/**
 * @see withScrolling
 */
/**
 * Данным HOC оборачиваются компоненты использующие всплывающие элементы (container).
 *
 * При скролинте layout-scroll`a в дочерние компоненты перелается React Context со значением true.
 * В HOC значение вычитывается и передается в компонент как параметр isScrolling=true.
 */
export declare const withScrolling: <TProps extends {
    isScrolling?: boolean | undefined;
}>(Component: React.ComponentType<TProps>) => React.ComponentType<TProps>;
export interface IScrollableProps {
    onScroll?(e: React.UIEvent<any>): void;
}
export declare const withScrollingProvider: <TProps extends IScrollableProps>(WrappedComponent: React.ComponentType<TProps>) => React.ComponentType<TProps>;
