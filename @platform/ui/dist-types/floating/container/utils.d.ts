import { CONTAINER_POSITION } from './interfaces';
export interface ICalcPos {
    top: number;
    left: number;
    position: CONTAINER_POSITION;
}
export interface IRect {
    height: number;
    width: number;
    top: number;
    left: number;
}
export interface ICalculatePositionParams extends IRect {
    tooltipHeight: number;
    tooltipWidth: number;
    margin: number;
    windowHeight: number;
    windowWidth: number;
    positioningOrder: Array<keyof typeof CONTAINER_POSITION>;
}
export declare const calculatePosition: (params: ICalculatePositionParams) => ICalcPos;
