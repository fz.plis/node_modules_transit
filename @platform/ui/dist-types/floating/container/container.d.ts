import React from 'react';
import type { CONTAINER_POSITION } from './interfaces';
import { CLOSE_REASON } from './interfaces';
export interface IContainerProps extends React.HTMLProps<HTMLDivElement> {
    anchorEl: HTMLElement;
    opened: boolean;
    offset?: number;
    disableOutsideClick?: boolean;
    disableEscapeKeyDown?: boolean;
    disableCloseOnScrolling?: boolean;
    positioningOrder?: Array<keyof typeof CONTAINER_POSITION>;
    onClose?(reason: CLOSE_REASON, e?: Event): void;
    onSetContainerPos?(pos: CONTAINER_POSITION): void;
    disableDetectingOutsideClickByMouseCoords?: boolean;
    isScrolling?: boolean;
}
export interface IContaineState {
    left: string;
    top: string;
}
export declare const Container: React.ComponentType<IContainerProps>;
