import React from 'react';
export interface IPortalProps {
    nodeGetter?(): HTMLElement;
    onRendered?(): void;
    notModal?: boolean;
}
export declare class Portal extends React.Component<IPortalProps> {
    componentDidMount(): void;
    componentWillUnmount(): void;
    private portalContainer?;
    setBodyOwerflow: (hidden: boolean) => void;
    render(): React.ReactPortal | null;
}
