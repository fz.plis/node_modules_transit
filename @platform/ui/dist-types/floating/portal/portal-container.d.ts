import React from 'react';
export declare type IPortalContainerProps = React.HTMLAttributes<HTMLDivElement>;
export declare class PortalContainer extends React.Component<IPortalContainerProps, {
    fake: boolean;
}> {
    componentWillMount(): void;
    componentWillUnmount(): void;
    modals: Record<string, React.ReactElement>;
    add: (name: string, Component: React.ComponentType<any>) => void;
    delete: (name: string) => void;
    isOpened: (name: string) => boolean;
    getPortalId: () => string;
    render(): JSX.Element;
}
