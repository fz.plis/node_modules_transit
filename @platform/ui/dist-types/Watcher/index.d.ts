import React from 'react';
import type { IIndexable } from '../interfaces';
export interface IWatcherProps<T> {
    entity: T;
    onChange(value: T): void;
    comparator?(next: T, current: T): boolean;
}
export declare class Watcher<TEntity = IIndexable> extends React.Component<IWatcherProps<TEntity>> {
    static defaultProps: any;
    shouldComponentUpdate(nextProps: IWatcherProps<TEntity>): boolean;
    componentDidUpdate(): void;
    render(): null;
}
