import React from 'react';
import type { IFontStyleWebProps } from '../atoms';
import type { WebIcon } from '../icons';
/**
 * Свойства компонента.
 */
export interface ILinkProps extends Omit<IFontStyleWebProps, 'align' | 'background' | 'clickable' | 'fill' | 'inline' | 'lineHeight'>, React.AllHTMLAttributes<HTMLAnchorElement> {
    /**
     * Если свойство определено, то ссылка во всех состояниях (hover, active и т.д) отображается с подчёркиванием,
     * кроме случаев когда ссылка отображается с иконкой.
     * Значение свойства определяет тип подчёркивания.
     */
    underlined?: 'dashed' | 'solid';
    /**
     * Определяет срабатывают ли на элементе события мыши.
     */
    disabled?: boolean;
    /**
     * Иконка. Если иконка указана, то ссылка во всех состояниях (hover, active и т.д) отображается без подчёркивания.
     */
    icon?: WebIcon;
    /**
     * Позиция иконки по горизонтали, относительно текстового содержимого ссылки.
     */
    iconPosition?: 'left' | 'right';
    /**
     * Определяет будет ли иконка обведена круглой рамкой.
     */
    iconBorder?: boolean;
    /**
     * Определяет можно ли выделить текст ссылки с помощью мыши.
     */
    noSelect?: boolean;
}
export declare const Link: React.FC<ILinkProps>;
