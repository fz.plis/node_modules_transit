import React from 'react';
import type { IWebBoxProps } from '../atoms';
/**
 * Свойства компонента Comment.
 */
export interface ICommentProps extends IWebBoxProps {
    /**
     * Заголовок комментария.
     */
    header: string;
    /**
     * Дата атрибут для автотестов.
     */
    dataName?: string;
}
/**
 * Комментарий к заявке, в сайдбаре визарда.
 */
export declare const Comment: React.FC<ICommentProps>;
