import React from 'react';
export interface ILimitWidthProps {
    size: {
        className: string;
    };
    /** Флаг, при проставлении которого убирается дефолтный паддниг скроллера. */
    removeDefaultPadding?: boolean;
}
export declare class LimitWidth extends React.PureComponent<ILimitWidthProps> {
    static defaultProps: {
        size: {
            className: string;
        };
    };
    static SIZE: {
        MD: {
            className: string;
        };
        AUTO: {
            className: string;
        };
    };
    render(): JSX.Element;
}
