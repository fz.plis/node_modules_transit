import React from 'react';
declare type TClick = (stepName: string) => void;
export interface IStep {
    label?: string;
    name: string;
}
export interface IStepsProps {
    width?: number | string;
    height?: number | string;
    steps?: IStep[];
    activeStepName?: string;
    footerLine?: React.ReactNode;
    onClick?: TClick;
}
export declare const Steps: React.FC<IStepsProps>;
export {};
