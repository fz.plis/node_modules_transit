/// <reference types="react" />
export interface IStepFooterLine {
    elementCount?: number;
    fillElementCount?: number;
}
export declare const StepFooterLine: ({ elementCount, fillElementCount }: IStepFooterLine) => JSX.Element;
