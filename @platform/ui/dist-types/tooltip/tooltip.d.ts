import React from 'react';
import type { ITooltipProps as IBaseTooltipProps } from '../floating';
export interface ITooltipCompProps extends IBaseTooltipProps {
    variant?: 'ERROR' | 'INFO';
    byContent?: boolean;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare class Tooltip extends React.Component<ITooltipCompProps> {
    render(): JSX.Element;
}
