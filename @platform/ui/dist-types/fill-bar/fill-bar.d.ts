import React from 'react';
import { Box } from '../atoms';
export interface IFillBar {
    progress: number;
    className?: string;
    fill?: Box['props']['fill'];
}
export declare class FillBar extends React.PureComponent<IFillBar> {
    render(): JSX.Element;
}
