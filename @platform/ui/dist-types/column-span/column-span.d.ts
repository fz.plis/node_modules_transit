import React from 'react';
import type { IAdjustProps } from '../atoms';
export interface IColumnSpanProps extends IAdjustProps {
    topDataField?: string;
    topText?: string;
    bottomDataField?: string;
    bottomText?: string;
    boldTop?: boolean;
    className?: string;
    topElement?: JSX.Element;
    bottomElement?: JSX.Element;
    leftNode?: React.ReactNode;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare class ColumnSpan extends React.Component<IColumnSpanProps> {
    render(): JSX.Element;
}
