import React from 'react';
import type { IHorizonProps } from '../atoms';
export interface IFileInfoProps extends IHorizonProps {
    text?: string;
}
export declare const FileInfo: React.FC<IFileInfoProps>;
