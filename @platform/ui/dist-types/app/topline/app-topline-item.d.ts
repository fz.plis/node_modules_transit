import React from 'react';
import type { IOption } from '../../interfaces';
export interface IAppToplineItemProps extends React.AllHTMLAttributes<HTMLDivElement> {
    label?: string;
    onClick?(): void;
    options?: IOption[];
    onChangeOption?(value: string): void;
    optionValue?: any;
    icon?: any;
    badge?: React.ReactNode;
}
export interface IToplineItemState {
    opened: boolean;
}
export declare class AppToplineItem extends React.PureComponent<IAppToplineItemProps, IToplineItemState> {
    state: {
        opened: boolean;
    };
    private refButton;
    handleClick: () => void;
    handleClose: () => void;
    handleChange: (value: string) => void;
    render(): JSX.Element;
}
