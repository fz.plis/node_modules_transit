import React from 'react';
import type { WebIcon } from '../../icons';
import type { IOption, IButtonAction } from '../../interfaces';
export interface IAppCounter {
    icon: WebIcon;
    count?: number;
    onClick?(): void;
}
export interface IAppToplineProps extends React.AllHTMLAttributes<HTMLDivElement> {
    actionLabel?: string;
    useName?: string;
    organizationsOption?: IOption[];
    organizationValue?: string;
    actions?: IButtonAction[];
    onClickUser?(): void;
    onClickExit?(): void;
    onChangeOrganization?(value: string): void;
    breadcrumbs?: IOption[];
    onClickBreadcrumb?(value: string, breadcrumb: IOption): void;
    hasNotification?: boolean;
    notificationsCount?: number;
    onClickNotification?(): void;
    importantNotification?: React.ReactNode;
}
export declare class AppTopline extends React.Component<IAppToplineProps> {
    static defaultProps: {
        actionLabel: string;
        actions: never[];
        onClickBreadcrumb: (_?: any) => void;
        organizationValue: string;
        organizationsOption: never[];
        useName: string;
    };
    render(): JSX.Element;
}
