import React from 'react';
export interface IAppLayoutProps {
    sidebar: React.ReactNode;
    topline: React.ReactNode;
    showImportantNotification?: boolean;
}
export declare class AppLayout extends React.PureComponent<IAppLayoutProps> {
    private rs?;
    private sidebar;
    private content;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element;
    private update;
}
