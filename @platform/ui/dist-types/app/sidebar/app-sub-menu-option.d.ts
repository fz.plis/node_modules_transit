import React from 'react';
import type { IAppSidebarOptionProps } from './app-sidebar-option';
export declare const AppSubMenuOption: React.FC<IAppSidebarOptionProps>;
