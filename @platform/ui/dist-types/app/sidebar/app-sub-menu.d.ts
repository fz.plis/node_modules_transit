import React from 'react';
import type { IWebBoxProps } from '../../atoms';
import type { WebIcon } from '../../icons';
import type { IAppSidebarOptionProps } from './app-sidebar-option';
export interface ISubMenuProps extends IWebBoxProps {
    options?: IAppSidebarOptionProps[];
    toggle?: IAppSidebarOptionProps;
    value?: string;
    onClickOption?(value: any[], option: IAppSidebarOptionProps): void;
    feedback?: {
        label: string;
        icon: WebIcon;
    };
    currentMenuValue?: string;
    footer?: React.ComponentType;
}
export declare const SubMenu: React.FC<ISubMenuProps>;
