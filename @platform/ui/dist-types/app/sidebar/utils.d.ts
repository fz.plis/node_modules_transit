import type { IAppSidebarOptionProps } from './app-sidebar-option';
export declare const getActiveMenuItem: (currentMenuValue?: string | undefined) => ({ value: itemValue, options: itemOptions }: IAppSidebarOptionProps) => boolean | undefined;
