import React from 'react';
import type { IWebBoxProps } from '../../atoms';
import type { IAppSidebarOptionProps } from './app-sidebar-option';
export interface IAppSidebarProps extends IWebBoxProps {
    options: IAppSidebarOptionProps[];
    toggle?: IAppSidebarOptionProps;
    value: any[];
    onClickOption?(value: any[], option: IAppSidebarOptionProps): void;
}
export declare const AppSidebar: React.FC<IAppSidebarProps>;
