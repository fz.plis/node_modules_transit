import React from 'react';
import type { WebIcon } from '../../icons';
/**
 * Тип элемента меню.
 */
export declare enum MENU_ITEM_TYPES {
    /**
     * Разделитель.
     */
    SEPARATOR = "separator",
    /**
     * Стрим.
     */
    STREAM = "stream",
    /**
     * Ссылка.
     */
    LINK = "link"
}
export interface IAppSidebarOptionProps {
    label?: string;
    className?: string;
    type?: MENU_ITEM_TYPES;
    value?: string;
    icon?: WebIcon;
    disabled?: boolean;
    active?: boolean;
    options?: IAppSidebarOptionProps[];
    onClick?(value: any[], option: IAppSidebarOptionProps): void;
    collapsedLabel?: string;
    badge?: string;
    collapsed?: boolean;
    tabIndex?: number;
    anchorRef?: React.RefObject<HTMLDivElement>;
    currentMenuValue?: string;
    subFooter?: React.ComponentType;
    onHover?(option: IAppSidebarOptionProps, hovered: boolean): void;
    id?: string;
    checked?: boolean;
}
export declare const AppSidebarOption: React.NamedExoticComponent<IAppSidebarOptionProps>;
