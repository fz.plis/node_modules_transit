export declare const ACTIONS: {
    CLEAR: string;
    LOOKUP: string;
    MORE: string;
    OPEN: string;
    CANCEL: string;
    LOGOUT: string;
    PROFILE: string;
    SUBMIT: string;
    SORT_DIRECTION: string;
    FOCUS: string;
    CLOSE: string;
    NOTIFICATION: string;
};
export declare const VALIDATION_CONTROL_DATA_TYPE: {
    ERROR: string;
    WARNING: string;
};
export declare const IFRAME_CONTAINER_NAME = "iframeContainer";
export { KEY_CODES, ROLE, DATA_TYPE } from '../core';
