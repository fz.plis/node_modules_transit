import { ACTION_TYPES } from './actions';
import type { INotificationProps } from './interface';
interface Action {
    type: ACTION_TYPES;
    payload?: INotificationProps | number;
}
/**
 * Редьюсер уведомелний.
 *
 * @param state Список уведомлений.
 * @param action Действие.
 * @param action.type Тип действия.
 * @param action.payload Значение.
 */
export declare const notificationReducer: (state: INotificationProps[], { type, payload }: Action) => INotificationProps[];
export {};
