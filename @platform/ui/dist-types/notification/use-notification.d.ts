import type { INotification } from './interface';
/**
 * Хук для работы с уведомлениями.
 *
 * @returns Функции для работы с нотификацией.
 */
export declare const useNotification: () => {
    notifications: import("./interface").INotificationProps[];
    addNotification: (notification: INotification) => void;
    removeAllNotifications: () => void;
};
