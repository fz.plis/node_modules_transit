import type React from 'react';
/**
 * Тип уведомления.
 */
export declare enum SYSTEM_NOTIFICATION_TYPE {
    SUCCESS = "SUCCESS",
    INFO = "ACCENT",
    WARNING = "WARNING",
    ERROR = "CRITIC"
}
/**
 * Интерфейс уведомления.
 */
export interface INotification {
    /**
     * Тип уведомления.
     */
    type: SYSTEM_NOTIFICATION_TYPE;
    /**
     * Флаг отображения иконки.
     */
    showIcon?: boolean;
    /**
     * Флаг отображения иконки закрыть.
     */
    showCloseIcon?: boolean;
    /**
     * Событие закрытия уведомления.
     */
    onClose?(): void;
    /**
     * Контент уведомления.
     */
    content: React.ReactNode;
    /**
     * Иконка для отображения уведомления.
     */
    Icon?: React.ComponentType;
}
/**
 * Интерфейс компонета уведомление.
 */
export interface INotificationProps extends INotification {
    /**
     * Идентификатор уведоиления.
     */
    id?: number;
}
/**
 * Интерфейс компонета списка уведомлений.
 */
export interface INotificationContainerProps {
    /**
     * Уведомления.
     */
    notifications: INotificationProps[];
}
