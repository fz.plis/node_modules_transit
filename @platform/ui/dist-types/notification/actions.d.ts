import type { INotificationProps } from './interface';
export declare enum ACTION_TYPES {
    REMOVE = "REMOVE",
    ADD = "ADD",
    REMOVE_ALL = "REMOVE_ALL"
}
export declare const removeAction: (id: number) => {
    type: ACTION_TYPES;
    payload: number;
};
export declare const removeAllAction: () => {
    type: ACTION_TYPES;
};
export declare const addAction: (notification: INotificationProps) => {
    type: ACTION_TYPES;
    payload: INotificationProps;
};
