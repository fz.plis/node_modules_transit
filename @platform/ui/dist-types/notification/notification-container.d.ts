import React from 'react';
import type { INotificationContainerProps } from './interface';
/**
 * Компонент списка уведомлений.
 *
 * @param props Свойства.
 * @param props.notifications Список уведомлений.
 * @class
 */
export declare const NotificationsContainer: React.FC<INotificationContainerProps>;
