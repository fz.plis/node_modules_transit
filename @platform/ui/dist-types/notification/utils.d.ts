import { SYSTEM_NOTIFICATION_TYPE } from './interface';
/**
 * Получить иконки уведомления.
 *
 * @param type Тип иконки.
 */
export declare const getIcon: (type: SYSTEM_NOTIFICATION_TYPE) => import("../icons").WebIcon;
/**
 * Получить uuid.
 */
export declare const uuid: () => number;
