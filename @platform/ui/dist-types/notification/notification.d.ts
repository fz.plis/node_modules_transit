import React from 'react';
import type { INotificationProps } from './interface';
/**
 * Компонент уведомление.
 *
 * @param props Свойства.
 * @param props.Icon Иконка уведомления.
 * @param props.showCloseIcon Флаг отображения иконки закрыть.
 * @param props.showIcon Флаг отображения иконки.
 * @param props.type Тип уведомления.
 * @param props.onClose Событие закрытия уведомления.
 * @param props.content Контент уведомления.
 * @class
 */
export declare const Notification: React.FC<INotificationProps>;
