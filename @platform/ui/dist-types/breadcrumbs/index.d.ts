import React from 'react';
import type { IBreadcrumb } from '../interfaces';
/** Свойства компонента. */
export interface IBreadcrumbsProps {
    /** Заголовок. */
    header?: string;
    /** Функция-обработчик клика на иконку "Домой". */
    onHomeClick?(): void;
    /** Хлебные крошки. */
    items?: IBreadcrumb[];
}
/**
 * Компонент "Хлебные крошки".
 *
 * {@link https://jira.gboteam.ru/browse/GBO-8296}.
 *
 * @param props Свойства компонента.
 * @param props.header Заголовок.
 * @param props.onHomeClick Функция-обработчик клика на иконку "Домой".
 * @param props.items Хлебные крошки.
 */
export declare const Breadcrumbs: React.FC<IBreadcrumbsProps>;
