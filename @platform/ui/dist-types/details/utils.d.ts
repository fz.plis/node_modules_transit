import type { FC, ReactElement } from 'react';
import type { SummaryChildren } from './types';
/** Дефолтный вид текстового содержимого компонента Summary. */
export declare const DefaultSummaryContent: FC<{
    children: string;
}>;
/**
 * Возвращает элемент для отображения в Summary.
 *
 * @param summaryContent - Если строка, то будет обёрнута в DefaultSummary,
 * если реакт элемент, то будет возвращён без изменений.
 */
export declare const getSummaryElement: (summaryContent: ReactElement | string) => ReactElement;
/**
 * Возвращает массив из двух реакт элементов,
 * первый элемент для открытого состояния компонента Details,
 * второй для закрытого.
 *
 * @param summaryChildren - Содержимое свойства children, компонента summary.
 */
export declare const getSummaryElements: (summaryChildren: SummaryChildren) => [openedContent: ReactElement, closedContent: ReactElement];
