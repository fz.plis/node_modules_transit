import React from 'react';
/**
 * Контекст компонента Details.
 *
 * Если isOpen === true, то содержимое Details отображается.
 *
 * ToggleIsOpen переключает флаг isOpen.
 */
export declare type DetailsContextType = [isOpen: boolean, toggleIsOpen: () => void];
/** Контекст компонента Details. */
export declare const DetailsContext: React.Context<DetailsContextType>;
