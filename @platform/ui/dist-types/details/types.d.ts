import type { ReactElement } from 'react';
/** Тип свойства children компонента Summary. */
export declare type SummaryChildren = ReactElement | string | [openedContent: ReactElement, closedConent: ReactElement] | [openedContent: string, closedConent: string];
