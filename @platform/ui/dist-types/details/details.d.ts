import React from 'react';
/** Свойства компонента Details. */
export interface IDetailsProps {
    /**
     * Если true - то компонент открыт.
     *
     * Если Details используется как Controlled Component, то определяет видимость контента.
     * Если как Uncontrolled Component то определяет начальное состояние видимости компонента.
     *
     * @default false.
     */
    opened?: boolean;
    /**
     * Дочерние элементы передаваемые в Details.
     *
     * Первый элемент массива - это элемент кликом по которому меняется видимости контента.
     * Может быть элементом типа Summary, или любым другим кастомным элементом,
     * который использует DetailsContext, для управления видимостью контента.
     *
     * Остальные элементы массива - это содержимое которое будет скрываться ли показываться,
     * кликом по компоненту summary.
     */
    children: [summary: React.ReactNode, ...content: React.ReactNode[]];
    /**
     * Колбек клика по Summary. Если передаётся,
     * то компонент будет работать в режиме Controlled Component.
     */
    onSummaryClick?(): void;
    /**
     * Атрибут для автотестов.
     */
    dataName?: string;
    /**
     * Атрибут для автотестов, когда надо сделать элементы уникальными.
     */
    dataId?: string;
}
/**
 * Используется для отображения контента,
 * который может быть скрыт или раскрыт кликом по элементу Summary.
 */
export declare const Details: React.FC<IDetailsProps>;
