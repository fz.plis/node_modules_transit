import React from 'react';
import type { SummaryChildren } from './types';
/** Свойства компонента Summary. */
export interface ISummaryProps {
    /**
     * Cодержимое компонента Summary.
     * Если в children передан один элемент, то он будет отображаться,
     * и в закрытом и в открытом состоянии элемента Details.
     * Если передан массив, то первый элемент будет использоваться для открытого состояния,
     * а второй для закрытого.
     */
    children: SummaryChildren;
}
/** Элемент кликом по которому меняется видимость контента элемента Details. */
export declare const Summary: React.FC<ISummaryProps>;
