import React from 'react';
export interface ISpinnerProps {
    small?: boolean;
    className?: string;
}
export declare class Spinner extends React.Component<ISpinnerProps> {
    render(): JSX.Element;
}
