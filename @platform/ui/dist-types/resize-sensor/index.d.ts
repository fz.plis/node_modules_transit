/**
 * Copyright Marc J. Schmidt. See the LICENSE file at the top-level
 * directory of this distribution and at
 * https://github.com/marcj/css-element-queries/blob/master/LICENSE.
 */
export declare class ResizeSensorClass {
    constructor(element: Element | Element[], callback: Function);
    detach(callback: Function): void;
    reset(): void;
}
declare const _default: typeof ResizeSensorClass;
/**
 * @deprecated
 */
export default _default;
