import React from 'react';
import type { THEME_COLOR } from '../atoms';
import { FILL } from '../atoms';
export declare enum ICON_SCALE {
    XS = 12,
    SM = 16,
    MD = 20,
    LG = 24,
    XL = 32,
    X2L = 48,
    X3L = 56,
    X4L = 72
}
interface ICommonWebIconProps {
    scale?: number | keyof typeof ICON_SCALE;
}
interface IWebIconProps extends ICommonWebIconProps {
    fill?: keyof typeof FILL;
    background?: THEME_COLOR;
    focusable?: boolean;
}
interface IIconClickableProps {
    clickable?: boolean;
}
export declare type IWebSpecialIconProps = ICommonWebIconProps & IIconClickableProps & React.AllHTMLAttributes<HTMLElement>;
export declare type IWebSVGIconProps = IWebIconProps & IWebSpecialIconProps;
export declare type WebIcon = React.ComponentType<IWebSVGIconProps>;
export declare type WebSpecialIcon = React.ComponentType<IWebSpecialIconProps>;
export declare const createIcon: (IconComp: any) => {
    new (props: IWebSVGIconProps | Readonly<IWebSVGIconProps>): {
        render(): JSX.Element;
        context: any;
        setState<K extends never>(state: {} | ((prevState: Readonly<{}>, props: Readonly<IWebSVGIconProps>) => {} | Pick<{}, K> | null) | Pick<{}, K> | null, callback?: (() => void) | undefined): void;
        forceUpdate(callback?: (() => void) | undefined): void;
        readonly props: Readonly<IWebSVGIconProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<{}>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<IWebSVGIconProps>, nextState: Readonly<{}>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<IWebSVGIconProps>, prevState: Readonly<{}>): any;
        componentDidUpdate?(prevProps: Readonly<IWebSVGIconProps>, prevState: Readonly<{}>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<IWebSVGIconProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<IWebSVGIconProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<IWebSVGIconProps>, nextState: Readonly<{}>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<IWebSVGIconProps>, nextState: Readonly<{}>, nextContext: any): void;
    };
    new (props: IWebSVGIconProps, context: any): {
        render(): JSX.Element;
        context: any;
        setState<K extends never>(state: {} | ((prevState: Readonly<{}>, props: Readonly<IWebSVGIconProps>) => {} | Pick<{}, K> | null) | Pick<{}, K> | null, callback?: (() => void) | undefined): void;
        forceUpdate(callback?: (() => void) | undefined): void;
        readonly props: Readonly<IWebSVGIconProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<{}>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<IWebSVGIconProps>, nextState: Readonly<{}>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<IWebSVGIconProps>, prevState: Readonly<{}>): any;
        componentDidUpdate?(prevProps: Readonly<IWebSVGIconProps>, prevState: Readonly<{}>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<IWebSVGIconProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<IWebSVGIconProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<IWebSVGIconProps>, nextState: Readonly<{}>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<IWebSVGIconProps>, nextState: Readonly<{}>, nextContext: any): void;
    };
    defaultProps: any;
    contextType?: React.Context<any> | undefined;
};
export declare const createSpecialIcon: (IconComp: any) => {
    new (props: IWebSpecialIconProps | Readonly<IWebSpecialIconProps>): {
        render(): JSX.Element;
        context: any;
        setState<K extends never>(state: {} | ((prevState: Readonly<{}>, props: Readonly<IWebSpecialIconProps>) => {} | Pick<{}, K> | null) | Pick<{}, K> | null, callback?: (() => void) | undefined): void;
        forceUpdate(callback?: (() => void) | undefined): void;
        readonly props: Readonly<IWebSpecialIconProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<{}>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<IWebSpecialIconProps>, nextState: Readonly<{}>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<IWebSpecialIconProps>, prevState: Readonly<{}>): any;
        componentDidUpdate?(prevProps: Readonly<IWebSpecialIconProps>, prevState: Readonly<{}>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<IWebSpecialIconProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<IWebSpecialIconProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<IWebSpecialIconProps>, nextState: Readonly<{}>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<IWebSpecialIconProps>, nextState: Readonly<{}>, nextContext: any): void;
    };
    new (props: IWebSpecialIconProps, context: any): {
        render(): JSX.Element;
        context: any;
        setState<K extends never>(state: {} | ((prevState: Readonly<{}>, props: Readonly<IWebSpecialIconProps>) => {} | Pick<{}, K> | null) | Pick<{}, K> | null, callback?: (() => void) | undefined): void;
        forceUpdate(callback?: (() => void) | undefined): void;
        readonly props: Readonly<IWebSpecialIconProps> & Readonly<{
            children?: React.ReactNode;
        }>;
        state: Readonly<{}>;
        refs: {
            [key: string]: React.ReactInstance;
        };
        componentDidMount?(): void;
        shouldComponentUpdate?(nextProps: Readonly<IWebSpecialIconProps>, nextState: Readonly<{}>, nextContext: any): boolean;
        componentWillUnmount?(): void;
        componentDidCatch?(error: Error, errorInfo: React.ErrorInfo): void;
        getSnapshotBeforeUpdate?(prevProps: Readonly<IWebSpecialIconProps>, prevState: Readonly<{}>): any;
        componentDidUpdate?(prevProps: Readonly<IWebSpecialIconProps>, prevState: Readonly<{}>, snapshot?: any): void;
        componentWillMount?(): void;
        UNSAFE_componentWillMount?(): void;
        componentWillReceiveProps?(nextProps: Readonly<IWebSpecialIconProps>, nextContext: any): void;
        UNSAFE_componentWillReceiveProps?(nextProps: Readonly<IWebSpecialIconProps>, nextContext: any): void;
        componentWillUpdate?(nextProps: Readonly<IWebSpecialIconProps>, nextState: Readonly<{}>, nextContext: any): void;
        UNSAFE_componentWillUpdate?(nextProps: Readonly<IWebSpecialIconProps>, nextState: Readonly<{}>, nextContext: any): void;
    };
    defaultProps: any;
    contextType?: React.Context<any> | undefined;
};
export {};
