import React from 'react';
export interface IIfProps {
    children: React.ReactNode;
    condition: boolean;
    elseNode?: React.ReactNode;
}
/**
 * Компонент для условного вывода.
 *
 * @deprecated Используйте стандартные конструкции JSX: `{condition && <JsxMarkup />}`.
 */
export declare const If: React.FC<IIfProps>;
