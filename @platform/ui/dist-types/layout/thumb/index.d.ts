import React from 'react';
/**
 * Type of scrollbar: `vertical` or `horizontal`.
 */
export declare enum ORIENTATION {
    VERTICAL = "vertical",
    HORIZONTAL = "horizontal"
}
/**
 * Provides props for scrollbar `Thumb`.
 *
 * @see Thumb
 */
export declare type ThumbProps = React.AllHTMLAttributes<HTMLDivElement>;
/**
 * Provides props for scrollbar `Track`.
 *
 * @see Track
 */
export interface ITrackProps extends Omit<React.AllHTMLAttributes<HTMLDivElement>, 'size'> {
    /**
     * Define vertical or horizontal scrollbar.
     */
    variant?: ORIENTATION;
    /**
     * Define track size.
     */
    size?: number | string;
    marginTop?: number | string;
    marginBottom?: number | string;
    marginLeft?: number | string;
    marginRight?: number | string;
}
/**
 * Thumb in scrollbar.
 *
 * @example
 * <Thumb style={{ width: 40 }} />
 */
export declare const Thumb: React.FC;
/**
 * Scrollbar track.
 *
 * @example
 *  <Track style={{width: '100px'}} />
 */
export declare const Track: React.ForwardRefExoticComponent<ITrackProps & React.RefAttributes<HTMLDivElement>>;
