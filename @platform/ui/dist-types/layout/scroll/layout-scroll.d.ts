import React from 'react';
import type { ScrollbarProps } from 'react-custom-scrollbars';
import Scrollbars from 'react-custom-scrollbars';
import type { IInnerRef } from '../../interfaces';
declare type ScrollbarOmited = 'renderThumbHorizontal' | 'renderThumbVertical' | 'renderTrackHorizontal' | 'renderTrackVertical' | 'renderView';
/**
 * Provides props for `LayoutScroll`, that omits renderProps from `ScrollbarProps`.
 *
 * For this component `innerRef` prop must be callback ref.
 *
 * @augments ScrollbarProps
 * @see https://github.com/malte-wessel/react-custom-scrollbars
 */
export interface ILayoutScrollProps extends IInnerRef<Scrollbars>, Omit<ScrollbarProps, ScrollbarOmited> {
    /**
     * Size of scrollbar.
     */
    scrollTop?: number;
    /**
     * Size of scrollbar track and thumb.
     */
    scrollSize?: number | string;
    scrollMarginHorizontalBottom?: number | string;
    scrollMarginHorizontalLeft?: number | string;
    scrollMarginHorizontalRight?: number | string;
    scrollMarginVerticalTop?: number | string;
    scrollMarginVerticalBottom?: number | string;
    scrollMarginVerticalRight?: number | string;
}
export declare class LayoutScrollComponent extends React.Component<ILayoutScrollProps> {
    static defaultProps: {
        autoHide: boolean;
        hideTracksWhenNotNeeded: boolean;
        innerRef: (_?: any) => void;
        onScroll: (_?: any) => void;
        scrollSize: number;
    };
    componentDidMount(): void;
    componentWillReceiveProps(newProps: ILayoutScrollProps): void;
    private scrollbar;
    scrollTop: (top: number) => void;
    scrollLeft: (left: number) => void;
    getScrollTop: () => (top: number) => void;
    private handleRef;
    private handleScrollTop;
    private renderTrackHorizontal;
    private renderTrackVertical;
    private renderView;
    render(): JSX.Element;
}
/**
 * Renders layout with scrollbars.
 * For correct render pass `style` prop to `LayoutScroll` or place it into `flex` container.
 *
 * If you want to access `Scrollbar` with `innerRef` prop use callback ref.
 *
 * @example
 * const style = { height: '500px' }
 * <LayoutScroll style={style}>{children}</LayoutScroll>
 * @example
 * const style = { display: 'flex', flexFlow: 'column' }
 * <div style={style}><LayoutScroll>{children}</LayoutScroll></div>
 */
export declare const LayoutScroll: React.ComponentType<ILayoutScrollProps>;
export {};
