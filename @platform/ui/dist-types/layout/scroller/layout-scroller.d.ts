import React from 'react';
export interface ILayoutScrollerProps {
    manager: JSX.Element;
    toolbar: JSX.Element;
    children: React.ReactNode;
}
export declare const LayoutScroller: React.FC<ILayoutScrollerProps>;
