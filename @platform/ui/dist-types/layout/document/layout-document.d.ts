import React from 'react';
import type { IButtonAction, IBreadcrumb } from '../../interfaces';
export interface ILayoutDocumentProps extends React.HTMLProps<HTMLDivElement> {
    className?: string;
    scrollHeight?: number;
    header: string;
    actionsVisibleCount?: number;
    actions?: IButtonAction[];
    fields?: React.ReactNode;
    breadcrumbs?: IBreadcrumb[];
    onHomeClick?(): void;
}
export declare class LayoutDocument extends React.PureComponent<ILayoutDocumentProps> {
    static defaultProps: any;
    render(): JSX.Element;
}
