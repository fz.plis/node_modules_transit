import React from 'react';
import type { IWebBoxProps } from '../atoms';
import { Box } from '../atoms';
export interface ICommentContainerProps extends IWebBoxProps {
    header?: React.ReactNode;
}
export declare const CommentContainer: React.ForwardRefExoticComponent<ICommentContainerProps & React.RefAttributes<Box>>;
