import React from 'react';
import type { IWebFontProps } from '../atoms';
import { Font } from '../atoms';
import type { IWebSVGIconProps } from '../icons';
export interface IInformerCommonProps {
    text: React.ReactNode;
}
export interface IInformerProps extends IWebSVGIconProps, IInformerCommonProps {
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export interface IInformerLinkProps extends IWebFontProps, IInformerCommonProps {
}
export declare class Informer extends React.Component<IInformerProps> {
    static Link: React.ForwardRefExoticComponent<IInformerLinkProps & React.RefAttributes<Font>>;
    render(): JSX.Element;
}
