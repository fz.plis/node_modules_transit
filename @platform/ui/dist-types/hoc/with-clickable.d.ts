import React from 'react';
export interface IClickableEvent {
    hovered: boolean;
    active: boolean;
    focused: boolean;
}
export interface IWithClickableProps {
    children(ref: (element: any) => void, event: IClickableEvent): JSX.Element;
}
export declare type IWithClickableState = IClickableEvent;
export declare class WithClickable extends React.Component<IWithClickableProps, IWithClickableState> {
    state: {
        hovered: boolean;
        active: boolean;
        focused: boolean;
    };
    componentWillUnmount: () => void;
    private listeningElement;
    unsub: () => void;
    private anchorRef;
    private handleMouseEnter;
    private handleMouseLeave;
    private handleFocus;
    private handleBlur;
    private handleMouseDown;
    private MouseMouseUp;
    private handleKeyup;
    private handleKeydown;
    render(): JSX.Element;
}
