import type React from 'react';
import type { IInputWithValidation } from '../interfaces';
export declare const asField: <T>(component: React.ComponentType<T>, validateOnChange?: boolean) => React.ComponentType<IInputWithValidation & T>;
