/// <reference types="react" />
export { withContext } from '../core';
export { withKeyboardNavigation, WithSearch, IListNavigatedProps, WithOptions, ILookupRequestMetadata, ILookupResponse } from '../pick';
export { withScrolling, WithTooltip, withScrollingProvider, ICalcPos } from '../floating';
export declare const withValidationTooltip: <TProps>(ComponentInput: import("react").ComponentType<TProps>) => import("react").ComponentType<import("../core").IInputWithValidation & TProps>;
