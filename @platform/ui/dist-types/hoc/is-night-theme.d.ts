/// <reference types="react" />
export declare const IsNightTheme: ({ children }: {
    children(isNightTheme: boolean): JSX.Element;
}) => JSX.Element;
