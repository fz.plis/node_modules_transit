export declare const getWidthStyleOptions: (target: HTMLElement, maxWidth?: string | number | undefined) => {
    minWidth: string;
    maxWidth: string | number;
} | {
    maxWidth: string | number | undefined;
    minWidth?: undefined;
};
