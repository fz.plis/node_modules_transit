import React from 'react';
import type { IHorizonProps } from '../atoms';
import { CONTAINER_POSITION } from '../interfaces';
export interface IInformerContentProps extends IHorizonProps {
    onCloseClick(e: React.MouseEvent<HTMLDivElement>): void;
    text: React.ReactNode;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const InformerContent: React.FC<IInformerContentProps>;
export interface IWithInformerProps extends React.AllHTMLAttributes<HTMLDivElement> {
    text: React.ReactNode;
    children(ref: React.RefObject<any>, toggle: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void): React.ReactNode;
    positioningOrder?: CONTAINER_POSITION[];
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const WithInformer: React.FC<IWithInformerProps>;
