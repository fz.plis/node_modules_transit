import React from 'react';
export declare enum MODAL_POSITION {
    CENTER = "CENTER",
    RIGHT = "RIGHT"
}
/**
 * Интерфейс модального  окна.
 */
export interface IModalProps {
    /**
     *
     * @param args
     */
    onClose?(...args: any[]): void;
    /**
     * Позиция модального окна.
     */
    modalPosition?: MODAL_POSITION;
    /**
     * Прокидывание кастомного класса для модального окна.
     */
    modalClassName?: string;
    /**
     * Не использовать в Portal.
     */
    doNotUsePortal?: boolean;
    /**
     * Статус открытия модального окна.
     */
    opened: boolean;
    /**
     * Возможность закрытия по esc.
     */
    closeByEcs?: boolean;
}
export declare const asModal: <TProps>(component: React.ComponentType<TProps>) => React.ComponentClass<IModalProps & TProps, any>;
