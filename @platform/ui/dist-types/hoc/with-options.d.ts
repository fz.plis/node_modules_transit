import React from 'react';
import { Horizon } from '../atoms';
import { TextInput } from '../inputs/input';
import { CONTAINER_POSITION } from '../interfaces';
import type { IWithOptionsProps as IBaseProps, IWithOptionsState as IBaseState, IOptionTemplateProps as IBaseOptionTemplate } from '../pick';
export interface IWithOptionsProps extends IBaseProps {
    positioningOrder?: Array<keyof typeof CONTAINER_POSITION>;
    joint?: boolean;
    onChangeSearch?(v: string): void;
    dimension?: TextInput['props']['dimension'];
    searchValueTransformer?(val: string | undefined): string;
    footer?: React.ReactNode;
}
export interface IWithOptionsState extends IBaseState {
    position: CONTAINER_POSITION;
}
export interface IWithOptionTemplateProps extends IBaseOptionTemplate {
    dimension?: TextInput['props']['dimension'];
}
export declare const withOptions: <T extends React.AllHTMLAttributes<HTMLDivElement> & React.RefAttributes<Horizon> & {
    opened?: boolean | undefined;
    dimension?: TextInput['props']['dimension'];
    extraSmall?: boolean | undefined;
}>(WrappedComponent: React.ComponentType<T>) => React.ForwardRefExoticComponent<React.PropsWithoutRef<IWithOptionsProps & T> & React.RefAttributes<React.Component<IWithOptionsProps & T, {}, any>>>;
