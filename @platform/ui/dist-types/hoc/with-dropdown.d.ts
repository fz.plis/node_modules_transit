import React from 'react';
import type { IBoxStyleProps } from '../atoms';
import type { IButtonAction } from '../interfaces';
import { CONTAINER_POSITION } from '../interfaces';
export interface IWithDropDownProps extends IBoxStyleProps {
    children(ref: React.RefObject<any>, opened: boolean, toggleOpen: () => void): React.ReactNode;
    actions?: IButtonAction[];
    positioningOrder?: Array<keyof typeof CONTAINER_POSITION>;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
    /**
     * Класс для элемента.
     */
    className?: string;
    /**
     * Размер отступа контейнера.
     */
    offset?: number;
}
export declare const WithDropDown: React.FC<IWithDropDownProps>;
