import React from 'react';
import type { ITypingInputProps } from '../interfaces';
export declare const withLookup: <TComponentProps extends ITypingInputProps<any, any>>(WrappedComponent: React.ComponentType<TComponentProps>) => React.ComponentType<Omit<import("../pick").IWithLookupProps & React.PropsWithoutRef<import("./with-options").IWithOptionsProps & React.RefAttributes<any> & TComponentProps> & React.RefAttributes<React.Component<import("./with-options").IWithOptionsProps & React.RefAttributes<any> & TComponentProps, {}, any>>, "options" | "isLoading" | "onIntersecting">>;
