import type React from 'react';
import type { FILL } from '../atoms';
/**
 * Интерфейс компонента бадж.
 */
export interface IBadgeProps {
    /**
     * Значение, отображаемое в бадже.
     */
    value: number;
    /**
     * Иконка, на которой отображается бадж.
     */
    Icon: React.ComponentType;
    /**
     * Действие выполняемое при нажатии на badge.
     */
    onClick?(): void;
    /**
     * Цвет заливки.
     */
    fill?: FILL;
}
