import React from 'react';
import type { IBadgeProps } from './interface';
/**
 * Компонент бадж.
 *
 * @param props Свойства.
 * @param props.value Значение.
 * @param props.Icon Иконка баджа.
 * @param props.onClick Событие нажатия на бадж.
 * @param props.fill Цвет заливки контейнера.
 * @param props.rest Опциональные св-ва, например data-отрибуты и т.д.
 * @class
 */
export declare const Badge: React.FC<IBadgeProps>;
