import React from 'react';
import type { IInputProps } from '../input';
import { TextInput } from '../input';
export interface ICaptchaProps extends IInputProps {
    dimension?: 'LG' | 'MD';
    inputTop?: boolean;
    url: string;
    onChangeCaptcha(): void;
}
export declare const Captcha: React.ForwardRefExoticComponent<ICaptchaProps & React.RefAttributes<TextInput>>;
