import React from 'react';
import type { IInputBaseProps } from '../../interfaces';
/**
 * Позиционирование лейбла.
 */
export declare enum POSITION {
    RIGHT = "RIGHT",
    LEFT = "LEFT"
}
export interface IToggleProps extends IInputBaseProps<boolean, React.SyntheticEvent<HTMLInputElement>>, Omit<React.AllHTMLAttributes<HTMLLabelElement>, 'onChange' | 'value'> {
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
    /**
     * Позиционирование лейбла.
     */
    labelPosition?: keyof typeof POSITION;
}
export declare const Toggle: React.ForwardRefExoticComponent<IToggleProps & React.RefAttributes<HTMLLabelElement>>;
