import React from 'react';
export declare enum FILE_UPLOAD_STATE {
    UPLOADING = "UPLOADING",
    MALWARE_CHECK = "MALWARE_CHECK",
    UPLOADED = "UPLOADED"
}
export interface IFileObject {
    /** Id. */
    id: any;
    /** Имя файла. */
    name: string;
    /** Размер файла. */
    size: number;
    /** Статус загрузки. */
    status: FILE_UPLOAD_STATE;
    /**
     * @deprecated Не используется внутри компоента.
     */
    disabled?: boolean;
    /** Текст ошибки. */
    error?: string;
    /** Прогресс загрузки.(параметр нормализован, т.е. 1 соответсвует 100%). */
    progress?: number;
}
export interface IFileInputProps {
    /** Разрешает одновременный выбор нескольких файлов. */
    multi?: boolean;
    /** Доступность компонента. */
    disabled?: boolean;
    /** Текст ошибки. Отображается под рамкой компонента. */
    error?: string;
    /** Массив объектов, содержащий данные о состоянии загружаемых файлов. */
    files?: IFileObject[];
    /**
     * Число в байтах, максимально возможный размер файла.
     * Отображается после hint и acceptText. Имеет вид 'Размер - до {maxSixe} {ед. измерения (проставляются сами)}'.
     */
    maxSize?: number;
    /**
     * @summary Функция вызывается после того, как браузер получит данные о загружаемых файлах.
     *
     * @param fileList Массив объектов, содержащий данные о загружаемых файлах.
     */
    onFileUpload?(fileList: File[]): void;
    /**
     * @summary Функция удаления файла. Вызывается при клике на иконку 'x' в строке ифнормации о загружаемом файле.
     *
     * @param file Удаляемый файл.
     */
    onDeleteFile?(file: IFileObject): void;
    /** Разрешенные форматы. Соответствует свойству accept html тега input. */
    accept?: string;
    /** Отображается после текста hint, имеет вид 'Форматы: {acceptText}'. */
    acceptText?: string;
    /** Подсказка, которая выводится в drag'n'drop области компонента. */
    hint?: string;
    /** Текст кнопки добавления файла. */
    uploadButtonText?: string;
}
export interface IFileInputState {
    dragEnter: boolean;
    more: boolean;
}
export interface IFileRowProps {
    isLoading?: boolean;
    fileName: string;
    hint?: string;
    onDeleteClick?(): void;
    hasError?: boolean;
    progress?: number;
}
export interface IFileUploadProgressProps {
    value: number;
}
export declare class FileInput extends React.Component<IFileInputProps, IFileInputState> {
    static defaultProps: any;
    state: {
        dragEnter: boolean;
        more: boolean;
    };
    /**
     * Реф для нативного input, используется только для вызова file picker и получения массива выбранных файлов.
     */
    private inputRef;
    /**
     * Обработка события установки файлов в input, вызывается при выборе файлов в
     * системном file picker-е. Вызывает onFileUpload только при наличии выбранных
     * файлов в input.
     */
    private handleChange;
    private handleButtonMoreClick;
    /** Хендлер события click в нативном input, вызывает file picker системы. */
    private handleButtonClick;
    /**
     * Обработка события input.onclick, сбрасывает текущий value для возможности
     * обработки одного и того же файла повторно. Нужно учитывать в
     * input.onchange, что на IE11 сброс target.value вызывает onChange повторно —
     * необходимо проверять FileList на пустоту.
     */
    private handleInputClick;
    private handleDragLeave;
    private handleDragEnter;
    private handleDrop;
    render(): JSX.Element;
}
