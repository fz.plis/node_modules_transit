/// <reference types="react" />
import { MaskedInput } from './masked-input';
export declare const SNILSInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const PassportInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const PassportSerialInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const PassportBranchCodeInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const OKVEDInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const LicenseInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const MoneyInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const PercentInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const AccountInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const PhoneInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const TimeInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
export declare const UUIDInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
/**
 * Для организаций в дополнение к ИНН в связи с постановкой на учет в налоговых органах по основаниям,
 * предусмотренным Налоговым кодексом Российской Федерации, присваивается код причины постановки на учет (КПП).
 * Структура КПП представляет собой **девятизначный код**: NNNNPPXXX.
 * КПП состоит из следующей последовательности символов слева направо:
 * NNNN (4 знака) — код налогового органа, который осуществил постановку на учет организации по месту её нахождения,
 * месту нахождения обособленного подразделения организации, расположенного на территории Российской Федерации,
 * месту нахождения принадлежащих ей недвижимого имущества и транспортных средств, а также по иным основаниям,
 * предусмотренным Налоговым кодексом Российской Федерации, или осуществил учет сведений в отношении организации в случаях,
 * предусмотренных Порядком постановки на учет,
 * снятия с учета в налоговых органах российских организаций по месту нахождения их обособленных подразделений,
 * принадлежащих им недвижимого имущества и (или) транспортных средств, физических лиц — граждан Российской Федерации,
 * а также индивидуальных предпринимателей, применяющих упрощенную систему налогообложения на основе патента.
 * PP (2 знака) — причина постановки на учет (учёта сведений).
 * Числовое значение символов PP может принимать значение в соответствии с ведомственным (ФНС)
 * «Справочником причин постановки на учёт налогоплательщиков-организаций в налоговых органах (СППУНО)»[2]:
 * для российской организации от 01 до 50 (01 — по месту её нахождения);
 * для иностранной организации от 51 до 99;
 * В частности могут применяться следующие значения:
 * «43» — постановка на учет российской организации по месту нахождения её филиала;
 * «44» — постановка на учет российской организации по месту нахождения её представительства;
 * «45» — постановка на учет российской организации по месту нахождения её обособленного подразделения.
 * XXX (3 знака) — порядковый номер постановки на учет (учета сведений) в налоговом органе по соответствующему основанию.
 */
export declare const KPPInput: import("react").ForwardRefExoticComponent<Omit<import("./masked-input").IMaskedInputProps, "maskType"> & import("react").RefAttributes<MaskedInput>>;
