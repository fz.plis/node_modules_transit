import React from 'react';
import type { ITypingInputProps } from '../../interfaces';
import type { TextAlignType } from '../constants';
import type { IInputTemplateProps } from '../input-template';
import type { IMaskedInputPropsUIBase as IBaseMaskedInputProps } from '../ui-base-masked-input';
import { MaskedInputUIBase as BaseMaskedInput } from '../ui-base-masked-input';
export interface IMaskedInputProps extends ITypingInputProps<string, HTMLInputElement | HTMLSpanElement>, Omit<IInputTemplateProps, 'onBlur' | 'onChange' | 'onFocus'> {
    width?: number | string;
    maskType: IBaseMaskedInputProps['maskType'];
    decimalCount?: IBaseMaskedInputProps['decimalCount'];
    integerCount?: IBaseMaskedInputProps['integerCount'];
    signAllowed?: IBaseMaskedInputProps['signAllowed'];
    mask?: IBaseMaskedInputProps['mask'];
    guide?: IBaseMaskedInputProps['guide'];
    placeholderChar?: IBaseMaskedInputProps['placeholderChar'];
    keepCharPositions?: IBaseMaskedInputProps['keepCharPositions'];
    pipe?: IBaseMaskedInputProps['pipe'];
    showMask?: IBaseMaskedInputProps['showMask'];
    clearingSet?: IBaseMaskedInputProps['clearingSet'];
    moneyPrefix?: IBaseMaskedInputProps['moneyPrefix'];
    moneySuffix?: IBaseMaskedInputProps['moneySuffix'];
    textAlign?: TextAlignType | string;
}
export interface IStateType {
    focused: boolean;
}
/** Input.Masked. */
export declare class MaskedInput extends React.Component<IMaskedInputProps, IStateType> {
    static WithProps: (fixedProps: Partial<IMaskedInputProps>) => React.ForwardRefExoticComponent<Omit<IMaskedInputProps, "maskType"> & React.RefAttributes<MaskedInput>>;
    static defaultProps: any;
    state: {
        focused: boolean;
    };
    input: HTMLInputElement | null;
    setFocused: (focused: boolean) => void;
    focus: () => void;
    handleBlur: (e: React.FocusEvent<HTMLInputElement>) => void;
    handleChange: (value: string, e?: React.SyntheticEvent<HTMLInputElement, Event> | undefined) => void;
    handleClearClick: () => void;
    handleFocus: (e: React.FocusEvent<HTMLInputElement>) => void;
    handleRef: (ref: BaseMaskedInput) => HTMLInputElement | null;
    private handleClick;
    render(): JSX.Element;
}
