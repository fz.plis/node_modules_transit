import React from 'react';
import type { Horizon } from '../../atoms';
import type { IAutoSizeInputProps } from '../auto-size-input';
export declare const TextArea: React.ForwardRefExoticComponent<IAutoSizeInputProps & React.RefAttributes<Horizon>>;
