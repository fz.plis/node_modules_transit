import React from 'react';
interface IStepValueProps {
    /**
     * Обработчик при клике по значению диапазона.
     */
    onChange(value: number): void;
    /**
     * Значение диапазона.
     */
    stepValue: number;
    /**
     * Отступ для правильного позиционирования на шкале диапазонов.
     */
    offset: number;
    /**
     * Символ разделения тысяч если необходимо.
     */
    thousandsSeparatorSymbol?: string;
    /**
     * После value например postfix=" ₽".
     */
    postfix?: string | ((value?: number) => string);
}
/**
 * Компонент для отображения значения диапазона.
 *
 * @param props Свойстваю.
 * @param props.onChange Обработчик при клике по значению диапазона.
 * @param props.stepValue Значение диапазона.
 * @param props.offset Отступ для правильного позиционирования на шкале диапазонов.
 * @param props.thousandsSeparatorSymbol Символ разделения тысяч если необходимо.
 * @param props.postfix Приставка после value например postfix=" ₽".
 */
export declare const StepValue: React.FC<IStepValueProps>;
export {};
