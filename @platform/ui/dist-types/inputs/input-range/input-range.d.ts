import React from 'react';
import { MaskedInput } from '../masked-input';
import type { INumberInputProps } from '../number/number-input';
export interface IInputRangeProps extends INumberInputProps {
    /**
     * Нижняя граница диапазона.
     */
    minValue: number;
    /**
     * Верхняя граница диапазона.
     */
    maxValue: number;
    /**
     * Диапазонов.
     */
    ranges?: number[];
    /**
     * True - можно выбирать value только из массива ranges.
     */
    isOnlyRangesValues?: boolean;
    /**
     * Перед value например prefix="$ ".
     */
    prefix?: string;
    /**
     * После value например postfix=" ₽".
     */
    postfix?: string | ((value?: number) => string);
    /**
     * Символ разделения тысяч если необходимо.
     */
    thousandsSeparatorSymbol?: string;
}
/**
 * Поле ввода со слайдером позволяет выбирать значения диапазона.
 * Как правило используется в калькуляторах для полей суммы или срока.
 *
 * {@link https://app.zeplin.io/project/5b6c23b90422817783ccc482/screen/5cdc24c254e6df1dff2ec82b}.
 * {@link https://holism.gpbdev.ru/uikit/?component=Input%20Range}.
 *
 * @param minValue Нижняя граница диапазона.
 * @param maxValue Верхняя граница диапазона.
 * @param ranges Массив диапазонов.
 * @param isOnlyRangesValues True - можно выбирать value только из массива ranges.
 * @param prefix Приставка перед value например prefix="$ ".
 * @param postfix Приставка после value например postfix=" ₽".
 * @param thousandsSeparatorSymbol Символ разделения тысяч если необходимо.
 */
export declare const InputRange: React.ForwardRefExoticComponent<IInputRangeProps & React.RefAttributes<MaskedInput>>;
