import React from 'react';
interface IStepCircleProps {
    /**
     * Обработчик при клике по компоненту.
     */
    onChange(value: number): void;
    /**
     * Текущее значение родительского поля ввода.
     */
    value?: number;
    /**
     * Значение диапазона.
     */
    stepValue: number;
    /**
     * Отступ для правильного позиционирования на шкале диапазонов.
     */
    offset: number;
}
/**
 * Компонент для отображения кружочка для диапазона.
 *
 * @param props Свойства.
 * @param props.onChange Обработчик при клике по компоненту.
 * @param props.value Текущее значение родительского поля ввода.
 * @param props.stepValue Значение диапазона.
 * @param props.offset Отступ для правильного позиционирования на шкале диапазонов.
 */
export declare const StepCircle: React.FC<IStepCircleProps>;
export {};
