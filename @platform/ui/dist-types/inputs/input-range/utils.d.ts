export declare const formatThousands: (value: number | string, separator?: string) => string;
export declare const formatWithPostfix: (val: number, postfix?: string | ((value?: number | undefined) => string) | undefined) => string;
