import React from 'react';
import type { IWebBoxProps } from '../../atoms';
export interface ISwitchBarOptionProps extends IWebBoxProps {
    focused?: boolean;
    name?: string;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const SwitchBarOption: React.FC<ISwitchBarOptionProps>;
