import React from 'react';
import type { ISelectGeneralProps } from '../../interfaces';
export interface ISwitchBarProps<T> extends ISelectGeneralProps<T>, Omit<React.AllHTMLAttributes<HTMLDivElement>, 'onChange' | 'value'> {
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const SwitchBar: React.ForwardRefExoticComponent<ISwitchBarProps<any> & React.RefAttributes<HTMLDivElement>>;
