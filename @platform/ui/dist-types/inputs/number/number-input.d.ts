import React from 'react';
import type { IInputProps } from '../input';
import { TextInput } from '../input';
export interface INumberInputProps extends Omit<IInputProps, 'onChange' | 'value'> {
    value?: number;
    onChange?(value: number | undefined, e?: any): void;
}
export declare const NumberInput: React.ForwardRefExoticComponent<INumberInputProps & React.RefAttributes<TextInput>>;
