import React from 'react';
import type { IInputProps } from './input';
import { TextInput } from './input';
/**
 * СМС код - 6 цифр.
 */
export declare const ConfirmationCodeInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * Номер паспорта - 6 цифр.
 */
export declare const PassportNumberInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * БИК - 9 цифр.
 */
export declare const BICInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * Идентификатор ГК - 25 цифр.
 */
export declare const IdentityInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * ОГРН (основной государственный регистрационный номер) - 13 цифр.
 */
export declare const OGRNInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * ОГРНИП (Основной государственный регистрационный номер индивидуального предпринимателя) - 15 цифр.
 */
export declare const OGRNIPInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * ИНН с самым большим кол-вом символов - 12
 * 12 - ИНН ИП, 10 - ИНН ЮЛ, 5 - КИО (Код иностранной организации)
 * Этот инпут нужно использовать в 2х случая:
 * 1. Нужен ввод ИНН для ИП (+валидация на длинну равной 12 символов), лейбл должен быть "ИНН"
 * 2. Что именно вводим не известно, т.е. Это можент ИНН ИП или ЮЛ или КИО (валидация на длинну 5, 10, 12), лейбл "ИНН/КИО".
 */
export declare const INNInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * ИНН для ЮЛ или КИО - до 10 цифр
 * Этот инпут нужно использовать в 2х случая:
 * 1. Нужен ввод ИНН для ЮЛ (+валидация на длинну равной 10 символов), лейбл должен быть "ИНН"
 * 2. Что именно вводим не известно, т.е. Это можент ИНН ЮЛ или КИО (валидация на длинну 5, 10), лейбл "ИНН/КИО".
 */
export declare const INNULInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * Для ввода имён.
 */
export declare const NameInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * ОКАТО - 11 цифр (для ЮЛ).
 */
export declare const OKATOInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * ОКПО - 10 цифр (для ЮЛ).
 */
export declare const OKPOInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
/**
 * Код налогового органа - 4 цифры.
 */
export declare const TaxAgentCodeInput: React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
