import React from 'react';
import type { ITypingInputProps } from '../../interfaces';
import type { TextAlignType } from '../constants';
import { AUTO_COMPLETE } from '../constants';
import type { IInputTemplateProps } from '../input-template';
export interface IInputProps extends ITypingInputProps<string, HTMLInputElement | HTMLSpanElement>, Omit<IInputTemplateProps, 'onBlur' | 'onChange' | 'onFocus'> {
    inputClassName?: string;
    password?: boolean;
    autoComplete?: AUTO_COMPLETE;
    textAlign?: TextAlignType | string;
    allowedSymbols?: RegExp;
}
export interface IInputState {
    focused: boolean;
}
export declare class TextInput extends React.Component<IInputProps, IInputState> {
    static defaultProps: any;
    state: {
        focused: boolean;
    };
    inputElement: React.RefObject<HTMLInputElement>;
    private setFocused;
    private handleFocus;
    private handleBlur;
    private handleChange;
    private handleClick;
    private handleClearClick;
    render(): JSX.Element;
}
export declare const onlyNumbers: (maxLenght: number) => React.ForwardRefExoticComponent<IInputProps & React.RefAttributes<TextInput>>;
