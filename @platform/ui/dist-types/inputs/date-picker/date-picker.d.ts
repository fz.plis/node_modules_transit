import React from 'react';
import type { Dayjs } from 'dayjs';
import type { ITypingInputProps, ICalendarDateProps } from '../../interfaces';
import type { IInputTemplateProps } from '../input-template';
import { MaskedInput } from '../masked-input/masked-input';
export interface IDataPickerPropsTypes extends ICalendarDateProps, ITypingInputProps<string, HTMLInputElement | HTMLSpanElement>, Pick<IInputTemplateProps, 'useClearBtn'> {
    /** Формат значения даты  (по умолчанию YYYY-MM-DD). */
    format?: string;
    /** Имя класса стилей, который применится на поле ввода. */
    className?: string;
    /**
     * Ширина поля ввода, задается строкой с указанием единиц измерения.
     * Например: 100%, 200px, 10rem и т.д.
     */
    width?: number | string;
    /** Хэндлер клика на иконку календаря. */
    onIconClick?(e: React.MouseEvent<any>): void;
    /** Хэндлер события мыши при попадании курсора внутрь поля. */
    onMouseEnter?(e: React.MouseEvent<HTMLDivElement>): void;
    /** Хэндлер события мыши при выходе курсора за пределы поля. */
    onMouseLeave?(e: React.MouseEvent<HTMLDivElement>): void;
    /** Отступ от края компонента. */
    dimension?: MaskedInput['props']['dimension'];
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export interface IDataPickerStateTypes {
    value: string;
    date: Dayjs;
    opened: boolean;
    showMask: boolean;
}
export declare class DatePicker extends React.Component<IDataPickerPropsTypes, IDataPickerStateTypes> {
    static defaultProps: {
        format: string;
        onBlur: (_?: any) => void;
        onChange: (_?: any) => void;
        onFocus: (_?: any) => void;
        onIconClick: (_?: any) => void;
        onMouseEnter: (_?: any) => void;
        onMouseLeave: (_?: any) => void;
    };
    constructor(props: IDataPickerPropsTypes);
    state: {
        date: Dayjs;
        value: string;
        opened: boolean;
        showMask: boolean;
    };
    componentWillReceiveProps(nextProps: IDataPickerPropsTypes): void;
    private input?;
    private handleRef;
    private handleChange;
    private handleIconClick;
    private handleClose;
    private handleFocus;
    private handleBlur;
    private handleCalendarClick;
    render(): JSX.Element;
}
