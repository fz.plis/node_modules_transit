import React from 'react';
export interface IWithValidationCallbacks {
    onTouch?(): void;
    onValidate?(): void;
}
export declare const withValidationOnChange: <T>(Component: React.ComponentType<T>) => React.ForwardRefExoticComponent<React.PropsWithoutRef<IWithValidationCallbacks & T> & React.RefAttributes<any>>;
export declare const withValidationOnBlur: <T>(Component: React.ComponentType<T>) => React.ForwardRefExoticComponent<React.PropsWithoutRef<IWithValidationCallbacks & T> & React.RefAttributes<any>>;
