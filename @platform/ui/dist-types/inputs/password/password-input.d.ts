import React from 'react';
import type { IInputProps } from '../input';
export interface IPasswordInputProps extends Omit<IInputProps, 'icon' | 'password'> {
    showPassword?: boolean;
}
export interface IPasswordInputState {
    showPassword: boolean;
}
export declare class PasswordInput extends React.Component<IPasswordInputProps, IPasswordInputState> {
    static defaultProps: {
        onIconClick: (_?: any) => void;
    };
    state: {
        showPassword: boolean;
    };
    private isControlled;
    private handleIconClick;
    render(): JSX.Element;
}
