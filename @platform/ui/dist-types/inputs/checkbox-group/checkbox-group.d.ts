import React from 'react';
import type { IGapStyleProps, IPatternSpanProps, IPatternProps } from '../../atoms';
import type { IOption, IMultiSelectGeneralProps, IInputWithValidation } from '../../interfaces';
export interface ICheckboxItemAddonProps {
    option: IOption;
    checked: boolean;
}
export interface ICheckboxOption<T = string> extends IOption<T> {
    informer?: React.ReactNode;
}
export interface ICheckboxGroupProps<T> extends Omit<IPatternProps, 'onChange' | 'value'>, IMultiSelectGeneralProps<T, ICheckboxOption<T>>, IInputWithValidation {
    columns?: IPatternSpanProps['size'];
    indent?: IGapStyleProps['size'];
    addon?: React.ComponentType<ICheckboxItemAddonProps>;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare class CheckboxGroup extends React.Component<ICheckboxGroupProps<string>> {
    static defaultProps: any;
    private handlePropagation;
    private handleChange;
    render(): JSX.Element;
}
