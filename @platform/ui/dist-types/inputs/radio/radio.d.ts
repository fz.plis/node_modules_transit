import React from 'react';
import { Box } from '../../atoms';
import type { IInputBaseProps } from '../../interfaces';
export interface IRadioProps extends IInputBaseProps<any>, Omit<React.AllHTMLAttributes<HTMLDivElement>, 'label' | 'onChange' | 'value'> {
    informer?: React.ReactNode;
    /** Контент самой кнопки, может быть строкой или другим подходящим типом в `ReactNode`. */
    label?: React.ReactNode;
}
export declare const RadioButton: React.ForwardRefExoticComponent<IRadioProps & React.RefAttributes<Box>>;
