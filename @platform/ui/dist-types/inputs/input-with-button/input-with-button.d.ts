import React from 'react';
import type { IInputProps } from '../../inputs/input';
export interface IInputWithButtonProps extends IInputProps {
    onClickButton?(): void;
    disabled?: boolean;
    disabledButton?: boolean;
    buttonText: string;
    style?: React.CSSProperties;
}
export declare class InputWithButton extends React.Component<IInputWithButtonProps> {
    render(): JSX.Element;
}
