import React from 'react';
import type { Horizon } from '../../atoms';
import type { ITypingInputProps } from '../../interfaces';
import type { IInputTemplateProps } from '../input-template';
export interface IAutoSizeInputProps extends ITypingInputProps<string, HTMLSpanElement | HTMLTextAreaElement>, Omit<IInputTemplateProps, 'onBlur' | 'onChange' | 'onFocus'> {
    height?: number | string;
    allowedSymbols?: RegExp;
}
export declare const AutoSizeInput: React.ForwardRefExoticComponent<IAutoSizeInputProps & React.RefAttributes<Horizon>>;
