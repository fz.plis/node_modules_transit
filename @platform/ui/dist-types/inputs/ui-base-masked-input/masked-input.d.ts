import React from 'react';
import type { MaskedInputProps } from 'react-text-mask';
import ImportedMaskedInput from 'react-text-mask';
import type { ITypingInputProps } from '../../core/interfaces';
export declare const CYRILLIC: RegExp;
export declare const CYRILLIC_UPPER_CASE: RegExp;
export declare const CYRILLIC_LOWER_CASE: RegExp;
export declare const autoCorrectedDatePipeTime: (conformedValue: any) => {
    value: any;
    indexesOfPipedChars: any[];
};
export declare const autoCorrectedDatePipe: (conformedValue: any) => {
    value: any;
    indexesOfPipedChars: any[];
};
export declare type PartialMaskedInputProps = Partial<MaskedInputProps>;
export declare enum MASK_INPUT_TYPE {
    LICENSE = "LICENSE",
    OKVED = "OKVED",
    PASSPORT = "PASSPORT",
    CODE = "CODE",
    SNILS = "SNILS",
    ACCOUNT = "ACCOUNT",
    MONEY = "MONEY",
    DATE = "DATE",
    DATETIME = "DATETIME",
    TAX_PERIOD = "TAX_PERIOD",
    CUSTOM = "CUSTOM"
}
export interface IMaskedInputPropsUIBase extends Omit<MaskedInputProps, 'onBlur' | 'onChange' | 'onFocus' | 'value'>, ITypingInputProps<string, HTMLInputElement> {
    maskType: keyof typeof MASK_INPUT_TYPE;
    /**
     * Регекс для очистки от спецсимволов.
     * При значении AA 122-111 c clearingSet = /[\-_\s]/g вернётся AA122111,
     * а при clearingSet = /[\s]/g вернётся AA122-111.
     */
    clearingSet?: RegExp;
    decimalCount?: number;
    integerCount?: number;
    signAllowed?: boolean;
    moneyPrefix?: string;
    moneySuffix?: string;
    moneyThousandsSeparator?: string;
    /** При true, будет пытаться приводить в большие или маленькие для совпадения с маской. */
    modifyCase?: boolean;
}
/**
 * Маска для инпута.
 * Всегда возвращает обычный HTML-input, поэтому нужно
 * или передать стили, или отрендерить внутри контейнера с классами.
 * Можно передать или отдельную маску или использовать один из
 * предподготовленных типов.
 * Для своей маски использовать - https://github.com/text-mask/text-mask/tree/master/react#readme как примеры.
 * Список пропсов и как делать pipe - https://github.com/text-mask/text-mask/blob/master/componentDocumentation.md#readme.
 */
export declare class MaskedInputUIBase extends React.Component<IMaskedInputPropsUIBase> {
    static readonly defaultProps: Readonly<{
        showMask: boolean;
        guide: boolean;
        decimalCount: number;
        integerCount: number;
        modifyCase: boolean;
        signAllowed: boolean;
    }>;
    inputContainer?: ImportedMaskedInput | null;
    inputElement?: HTMLElement | null;
    private lastCursorPosition;
    private lastValue;
    handleRef: (ref: ImportedMaskedInput) => void;
    /**
     * Waits for end of event queue and then sets the caret position.
     */
    setCaret: (e: React.BaseSyntheticEvent, position: number) => void;
    /**
     * Sanitizes raw value before passing it higher.
     */
    onChangeHandler: (removePattern: RegExp, maskType: keyof typeof MASK_INPUT_TYPE) => (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleFocus: (e: React.FocusEvent<HTMLInputElement>) => void;
    /**
     * Маска не всегда правильно обрабатывает.
     * Эта функция предотвращает неправильное поведение и подправляет результат до скармливания его маске.
     * При backspace, если встречается статичный символ, то удалять 2 символа назад.
     * В деньгах, при backspace перед проблом, удалять 2 символа.
     * При delete, если встречается статичный символ, то удалять 2 символа вперёд.
     */
    onKeyDown: (e: React.KeyboardEvent<HTMLInputElement>, onChange: any, mask: any) => void;
    predefinedProps(maskType: keyof typeof MASK_INPUT_TYPE): PartialMaskedInputProps;
    render(): JSX.Element;
}
