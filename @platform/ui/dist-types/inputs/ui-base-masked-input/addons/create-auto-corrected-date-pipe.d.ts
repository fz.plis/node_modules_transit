declare const _default: (dateFormat?: string, { minYear, maxYear }?: {
    minYear?: number | undefined;
    maxYear?: number | undefined;
}) => (conformedValue: any) => {
    value: any;
    indexesOfPipedChars: any[];
};
export default _default;
