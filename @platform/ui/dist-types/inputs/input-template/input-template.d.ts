import React from 'react';
import { Horizon } from '../../atoms';
import type { WebIcon, IWebSVGIconProps } from '../../icons';
import type { IInputBaseProps } from '../../interfaces';
export interface IInputTemplateProps extends IInputBaseProps, Omit<React.AllHTMLAttributes<HTMLDivElement>, 'onChange' | 'size' | 'value'> {
    width?: number | string;
    useClearBtn?: boolean;
    iconAction?: string;
    icon?: WebIcon;
    iconSize?: IWebSVGIconProps['scale'];
    onIconClick?(e: React.MouseEvent<HTMLSpanElement>): void;
    dimension?: 'LG' | 'MD' | 'SM';
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export interface IInputTemplateInternalProps extends IInputTemplateProps {
    children(className: string): React.ReactElement;
    isTextInput: boolean;
    focused: boolean;
    hovered?: boolean;
    onClearClick?(e: React.KeyboardEvent<HTMLSpanElement> | React.MouseEvent<HTMLSpanElement>): void;
    valueIsPlaceholder?: boolean;
    appendix?: React.ReactNode;
}
export interface IInputIconProps {
    onClick?(e: React.MouseEvent<HTMLSpanElement>): void;
    icon: WebIcon;
    name?: string;
    iconAction?: string;
    iconSize?: IWebSVGIconProps['scale'];
    disabled?: boolean;
}
export declare const InputIcon: ({ disabled, onClick, name, icon: Icon, iconAction, iconSize }: IInputIconProps) => JSX.Element;
export declare const InputTemplate: React.ForwardRefExoticComponent<IInputTemplateInternalProps & React.RefAttributes<Horizon>>;
