import React from 'react';
export interface IClearBtnProps extends React.HTMLProps<HTMLSpanElement> {
    className?: string;
    visible?: boolean;
    hasError?: boolean;
    hasWarning?: boolean;
    onClear?(e: React.KeyboardEvent<HTMLSpanElement> | React.MouseEvent<HTMLSpanElement>): void;
}
export declare const ClearBtn: React.FC<IClearBtnProps>;
