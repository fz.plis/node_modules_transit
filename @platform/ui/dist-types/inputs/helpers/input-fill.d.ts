export declare const getInputFill: (hovered?: boolean, focused?: boolean, hasWarning?: boolean, hasError?: boolean, disabled?: boolean, extraSmall?: boolean) => {
    borderFill: "FAINT" | "ACCENT" | "CRITIC" | "WARNING" | "TRANSPARENT";
    fontFill: "BASE" | "FAINT" | "CRITIC" | "WARNING";
    iconFill: string;
};
