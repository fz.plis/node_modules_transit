import React from 'react';
export interface IBlurProviderProps<T> {
    onBlur?(e: React.FocusEvent<T>): void;
}
export interface IWithTrimProps<TArg = never, TEvent = any> extends IBlurProviderProps<TEvent> {
    value?: string;
    onChange?(value: string, arg?: TArg): void;
    disabled?: boolean;
    children(props: IBlurProviderProps<TEvent>): React.ReactElement | null;
}
export declare class WithTrim extends React.Component<IWithTrimProps> {
    static defaultProps: {
        disabled: boolean;
        onBlur: (_?: any) => void;
        onChange: (_?: any) => void;
        value: string;
    };
    private handleBlur;
    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null;
}
