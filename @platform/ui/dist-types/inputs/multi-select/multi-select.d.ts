import React from 'react';
import type { Horizon } from '../../atoms';
import type { IWithOptionsProps } from '../../hoc/with-options';
import type { IOption } from '../../interfaces';
import type { IInputTemplateProps } from '../input-template';
export interface IMultiSelectBaseClassProps extends Omit<IInputTemplateProps, 'onChange'> {
    /**
     * If `true` the input's content is considered
     * as if it has failed validation. Considered
     * error-free otherwise (if `false`, `undefined` or unset).
     */
    selectedOptions?: IOption[];
    onClearOption?(option: IOption): any;
    onClearAllOptions?(): void;
    opened?: boolean;
}
export declare const MultiSelectBaseClass: React.ForwardRefExoticComponent<IMultiSelectBaseClassProps & React.RefAttributes<Horizon>>;
export interface IMultiSelectProps extends IWithOptionsProps, Omit<IMultiSelectBaseClassProps, 'onClearAllOptions' | 'onClick' | 'onKeyDown' | 'value'> {
    /**
     * Функция для кастомной фильтрации списка.
     */
    filterFn?(searchValue: string): IOption[];
    /**
     * Флаг отменяющий сортировку опций.
     */
    withOutSortOptions?: boolean;
}
export interface IMultiSelectState {
    opened?: boolean;
    sortOptions: IOption[];
}
export declare class MultiSelect extends React.Component<IMultiSelectProps, IMultiSelectState> {
    static defaultProps: any;
    constructor(props: IMultiSelectProps);
    state: IMultiSelectState;
    componentDidUpdate(_: IMultiSelectProps, prevState: IMultiSelectState): void;
    sortOptions: () => void;
    handleSelectOption: (option: IOption) => void;
    handleSelectAllOptions: (options?: IOption<any>[] | undefined) => void;
    handleClearOption: (option: IOption) => void;
    handleClearAllOptions: (e: React.MouseEvent<any>) => void;
    getSortOptions: () => IOption[];
    getSelectedOptions: () => IOption[];
    getAvailableOptions: (options?: IOption[]) => IOption[];
    render(): JSX.Element;
}
export declare const MultySelectWithSearch: React.FC<IMultiSelectProps>;
