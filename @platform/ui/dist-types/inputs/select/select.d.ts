import React from 'react';
import type { Horizon } from '../../atoms';
import type { IWithOptionsProps } from '../../hoc/with-options';
import type { IWebSVGIconProps } from '../../icons';
import type { IOption } from '../../interfaces';
import type { IInputTemplateProps } from '../input-template';
export interface ISelectBaseClassProps extends Omit<IInputTemplateProps, 'onChange'> {
    opened?: boolean;
    /** Признак использования компонента `Skeleton` пока список опций пуст, заменяет спиннер при установленном `isLoading`. */
    useSkeleton?: boolean;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const SelectIcon: React.FC<IWebSVGIconProps & {
    opened?: boolean;
}>;
export declare const SelectBaseClass: React.ForwardRefExoticComponent<ISelectBaseClassProps & React.RefAttributes<Horizon>>;
export interface ISelectProps extends IWithOptionsProps, Omit<ISelectBaseClassProps, 'onClick' | 'onKeyDown' | 'value'> {
    /**
     * Функция для кастомной фильтрации списка.
     */
    filterFn?(searchValue: string): IOption[];
}
export declare class Select extends React.Component<ISelectProps> {
    selectRef: React.RefObject<any>;
    shouldSkipSelectHandler: () => boolean | undefined;
    handleSelectOption: (option: IOption) => void;
    render(): JSX.Element;
}
