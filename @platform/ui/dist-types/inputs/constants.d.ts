export declare const blackAttributesList: string[];
export declare const DEFAULT_INPUT_WIDTH = "100%";
export declare const EMPTY_VALUE = "";
export declare const TEXT_ALIGN: {
    LEFT: string;
    RIGHT: string;
    INHERIT: string;
};
export declare type TextAlignType = 'inherit' | 'left' | 'right';
export declare enum DATE_BLOCKS {
    DAY = "DAY",
    MONTH = "MONTH",
    YEAR = "YEAR"
}
export declare enum PERIOD_BLOCKS {
    FIRST = "FIRST",
    SECOND = "SECOND",
    PERIOD = "PERIOD",
    YEAR = "YEAR"
}
export declare const DATE_FORMAT = "DD.MM.YYYY";
export declare const DATE_ISO_FORMAT = "YYYY-MM-DD";
export declare const DATE_ALMOST_ISO_FORMAT = "YYYY-M-D";
export declare const DATE_ENTER_DIGIT = 10;
export declare enum AUTO_COMPLETE {
    ON = "on",
    OFF = "off"
}
