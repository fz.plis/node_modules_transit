import React from 'react';
import type { IHorizonProps } from '../atoms';
import { Gap } from '../atoms';
import type { IOption } from '../interfaces';
export interface ITabsProps<T extends IOption = IOption> extends IHorizonProps {
    value: any;
    options: T[];
    onChange?(value: any, option?: T): void;
    disabled?: boolean;
    optionsOffset?: Gap['props']['size'] | number;
}
export interface ITabsState {
    width: number;
    left: number;
}
export declare class Tabs extends React.PureComponent<ITabsProps, ITabsState> {
    static defaultProps: any;
    state: {
        width: number;
        left: number;
    };
    componentDidMount(): void;
    componentDidUpdate(prevProps: ITabsProps): void;
    private activeTabElement;
    private getRef;
    private handleRef;
    private calcBarPosition;
    private calcBarStyles;
    render(): JSX.Element;
}
