import React from 'react';
import type { IWebFontProps } from '../atoms';
export interface ITabProps extends Omit<IWebFontProps, 'clickable' | 'fill'> {
    selected: boolean;
    isFirst: boolean;
    onSelect?(): void;
}
export declare const Tab: React.ForwardRefExoticComponent<ITabProps & React.RefAttributes<HTMLDivElement>>;
