import React from 'react';
import type { IBoxWebStyleProps } from '../atoms';
import { DOCUMENT_STATUS_TYPE } from '../interfaces';
export interface IDocumentStatusProps extends Omit<IBoxWebStyleProps, 'fill'> {
    variant: DOCUMENT_STATUS_TYPE;
    text: string;
    header?: string;
    name?: string;
}
export declare const DocumentStatus: React.FC<IDocumentStatusProps>;
