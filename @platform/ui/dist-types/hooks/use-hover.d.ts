export declare const useHover: (onMouseEnter?: React.MouseEventHandler, onMouseLeave?: React.MouseEventHandler) => {
    hovered: boolean;
    handleMouseEnter: (e: any) => void;
    handleMouseLeave: (e: any) => void;
};
