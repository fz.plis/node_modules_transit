/**
 * Позволяет применить debounce к быстроменяющемуся значению.
 * Возвращаемое значение `value` будет получено, только по истечению времени,
 * указанному в параметре `delay`.
 *
 * @see Реализация взята из {@link https://usehooks.com/useDebounce/|useHooks.com}
 *
 * @param value Изменяющееся значение.
 * @param delay Задержка изменения значения.
 *
 * @example
 * const [searchTerm, setSearchTerm] = useState('');
 * const [results, setResults] = useState([]);
 * const [isSearching, setIsSearching] = useState(false);
 * const debouncedSearchTerm = useDebounce(searchTerm, 500);
 *
 * useEffect(
 *   () => {
 *     if (debouncedSearchTerm) {
 *       setIsSearching(true);
 *       searchCharacters(debouncedSearchTerm).then(results => {
 *         setIsSearching(false);
 *         setResults(results);
 *       });
 *     } else {
 *       setResults([]);
 *     }
 *   },
 *   [debouncedSearchTerm]
 */
export declare const useDebounce: <T>(value: T, delay: number) => T;
