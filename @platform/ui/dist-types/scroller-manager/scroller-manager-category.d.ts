import React from 'react';
import type { ICategory } from './interfaces';
export interface ICategoryProps extends ICategory {
    key?: any;
    onClick?(): void;
    active: boolean;
    showCounts: boolean;
}
export declare const ScrollerManagerCategory: React.FC<ICategoryProps>;
