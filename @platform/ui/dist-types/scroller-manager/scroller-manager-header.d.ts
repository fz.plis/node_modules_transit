import React from 'react';
import type { IBreadcrumb } from '../interfaces';
export interface IScrollerManagerHeaderProps {
    header?: string;
    filterNode?: React.ReactChildren | React.ReactNode;
    onHomeClick?(): void;
    breadcrumbs?: IBreadcrumb[];
}
export declare const ScrollerManagerHeader: React.FC<IScrollerManagerHeaderProps>;
