import React from 'react';
import type { ICategory } from './interfaces';
export interface IScrollerManagerProps {
    header?: string;
    filterNode?: React.ReactChildren | React.ReactNode;
    showCounts?: boolean;
    categories?: ICategory[];
    category: string;
    onCategoryChange(value: any, option: ICategory): void;
    view?: string;
    onViewChange?(view: string): void;
    onHomeClick?(): void;
}
export declare class ScrollerManager extends React.Component<IScrollerManagerProps> {
    render(): JSX.Element;
}
