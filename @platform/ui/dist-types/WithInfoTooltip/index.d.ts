import React from 'react';
export interface IWithTooltipInfoProps extends React.AllHTMLAttributes<HTMLDivElement> {
    text: React.ReactNode;
    children(ref: (ref: any) => void): React.ReactNode;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const WithInfoTooltip: React.FC<IWithTooltipInfoProps>;
