import React from 'react';
import { Font } from '../atoms';
import { DOCUMENT_STATUS_TYPE } from '../interfaces';
export interface IStatusProps {
    line?: Font['props']['line'];
    type?: DOCUMENT_STATUS_TYPE;
    className?: string;
}
export declare const styles: {
    height: string;
    minHeight: string;
    width: string;
    minWidth: string;
};
export declare const Status: React.FC<IStatusProps>;
