import React from 'react';
import type { WebIcon } from '../icons';
import type { IActionListAction } from '../interfaces';
/**
 * Свойства дополнительного действия.
 */
export interface IExtraActionListAction extends IActionListAction {
    /**
     * Иконка.
     */
    icon: WebIcon;
}
/**
 * Блок с дополнительной информацией.
 */
export interface IInformation {
    /**
     * Наименование блока.
     */
    label: string;
    /**
     * Значение блока.
     */
    value: React.ReactNode | undefined;
}
/**
 * Свойства карточки скроллера.
 */
export interface IScrollerCardProps {
    /**
     * Заголовок.
     */
    title: string;
    /**
     * Иконка.
     */
    icon?: WebIcon;
    /**
     * Флаг отображения чекбокса.
     */
    showCheckbox?: boolean;
    /**
     * Флаг чекбокса.
     */
    checked?: boolean;
    /**
     * Функция обработчик события выбора и снятия выбора чекбокса.
     */
    onChangeChecked?(value: boolean): void;
    /**
     * Цвет кружка статуса.
     */
    statusColor?: COLOR;
    /**
     * Текст статуса.
     */
    statusText?: string;
    /**
     * Текст прогресс-бара.
     */
    progressText?: string;
    /**
     * Значение прогресс-бара.
     */
    progress?: number;
    /**
     * Главное действие.
     */
    mainAction?: IActionListAction;
    /**
     * Массив дополнительных действий.
     */
    extraActions?: IExtraActionListAction[];
    /**
     * Массив действий.
     */
    actions?: IActionListAction[];
    /**
     * Массив строк дополнительной информации.
     */
    information?: IInformation[];
    /**
     * Функция обработчик двойного нажатия.
     */
    onDoubleClick?(): void;
    /**
     * Флаг, фильтрующий пустые блоки с информацией.
     */
    filterEmptyInfoBlocks?: boolean;
}
/**
 * Максимальное количество дополнительных действий.
 */
export declare const MAX_EXTRA_ACTION_COUNT = 2;
/**
 * Максимальное количество строк дополнительной информации.
 */
export declare const MAX_INFO_ROWS_COUNT = 6;
declare type COLOR = 'ACCENT' | 'BASE' | 'CRITIC' | 'FAINT' | 'SUCCESS' | 'WARNING';
/**
 * Универсальная карточка скроллера.
 */
export declare const ScrollerCard: React.FC<IScrollerCardProps>;
export {};
