import React from 'react';
import type { IWebBoxProps } from '../atoms';
import { Box } from '../atoms';
export declare enum RESULT_TYPE {
    SUCCESS = "SUCCESS",
    ERROR = "ERROR",
    WARNING = "WARNING"
}
/**
 * Интерфейс результирующего представления.
 */
export interface IResultViewProps extends IWebBoxProps {
    /**
     * Тип.
     */
    type: keyof typeof RESULT_TYPE;
    /**
     * Заголовок.
     */
    header?: React.ReactNode;
    /** Контент для представления. Будет показан сразу после заголовка. */
    children?: React.ReactNode;
    /**
     * Комментарий.
     */
    comment?: React.ReactNode;
    /**
     * Заголовок.
     */
    commentHeader?: React.ReactNode;
    /**
     * Текст главной кнопки.
     */
    buttonText?: string;
    /**
     * Действие главной кнопки.
     */
    onClickButton?(): void;
    /**
     * Текст дополнительной кнопки.
     */
    secondButtonText?: string;
    /**
     * Действие дополнительной кнопки.
     */
    onClickSecondButton?(): void;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const ResultView: React.ForwardRefExoticComponent<IResultViewProps & React.RefAttributes<Box>>;
