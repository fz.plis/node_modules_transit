import React from 'react';
import type { IBoxWebStyleProps } from '../atoms';
import { VALIDATION_CONTROL_TYPE } from './validation-control';
export interface IValidationControlItem {
    name: string;
    messages: string[];
    label: string;
    isLink?: boolean;
}
export interface IValidationControlsProps extends IBoxWebStyleProps {
    type: VALIDATION_CONTROL_TYPE;
    items?: IValidationControlItem[];
    onItemLinkClick?(name: string): void;
}
export interface IValidationControlsState {
    opened: boolean;
}
export declare class ValidationControls extends React.Component<IValidationControlsProps, IValidationControlsState> {
    static defaultProps: any;
    state: {
        opened: boolean;
    };
    private handleClick;
    render(): JSX.Element | null;
}
