import React from 'react';
/**
 * Свойства компонента FormValidation.
 */
interface FormValidationProps {
    /**
     * Колбек клика на заголовке элемента, отображающего ошибку.
     */
    onItemLinkClick?(name: string): void;
    /**
     * Объект отображающий имена полей формы на заголовки элементов показывающих ошибки.
     * Обычно в качестве заголовков указываются лейблы полей.
     */
    fieldLabels: Record<string, string>;
}
/**
 * Показывает ошибки валидации.
 */
export declare const FormValidation: React.FC<FormValidationProps>;
export {};
