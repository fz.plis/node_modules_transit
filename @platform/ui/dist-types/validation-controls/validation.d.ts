import React from 'react';
import type { IValidationControlsProps, IValidationControlItem } from './validation-controls';
export interface IValidationProps extends Omit<IValidationControlsProps, 'items' | 'type'> {
    errors?: IValidationControlItem[];
    warnings?: IValidationControlItem[];
}
export declare const Validation: React.FC<IValidationProps>;
