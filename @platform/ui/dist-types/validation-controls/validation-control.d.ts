import React from 'react';
import type { IWebBoxProps } from '../atoms';
export declare enum VALIDATION_CONTROL_TYPE {
    ERROR = "ERROR",
    WARNING = "WARNING"
}
export interface IValidationControlProps extends IWebBoxProps {
    type?: keyof typeof VALIDATION_CONTROL_TYPE;
    isLink?: boolean;
    label?: string;
    messages?: string[];
    onLinkClick(): void;
}
export declare class ValidationControl extends React.Component<IValidationControlProps> {
    static defaultProps: any;
    renderLabel(): JSX.Element;
    renderLink: (children: React.ReactNode) => JSX.Element;
    renderTextHeader: (children: React.ReactNode) => JSX.Element;
    render(): JSX.Element;
}
