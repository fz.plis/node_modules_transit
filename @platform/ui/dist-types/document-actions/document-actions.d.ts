import React from 'react';
import type { IBoxWebStyleProps } from '../atoms';
import type { IActionButton } from '../core';
import type { WebIcon } from '../icons';
export interface IDocumentAction extends IActionButton {
    icon: WebIcon;
}
export interface IDocumentActionsProps extends IBoxWebStyleProps {
    actions: IDocumentAction[];
}
export declare const DocumentActions: React.FC<IDocumentActionsProps>;
