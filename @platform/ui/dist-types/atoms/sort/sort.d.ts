import React from 'react';
export interface ISortProps {
    options: Array<{
        label: string;
        value: string;
    }>;
    onChange(value: string): void;
    value?: string;
    className?: string;
}
export declare class Sort extends React.PureComponent<ISortProps> {
    state: {
        opened: boolean;
    };
    private refParent;
    handleClose: () => void;
    handleOpen: () => void;
    handleChange: (value: string) => void;
    render(): JSX.Element;
}
