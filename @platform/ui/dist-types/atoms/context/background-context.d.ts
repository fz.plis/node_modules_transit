import React from 'react';
import type { THEME_COLOR } from '../interfaces';
export declare const BackgroundContext: React.Context<THEME_COLOR | undefined>;
