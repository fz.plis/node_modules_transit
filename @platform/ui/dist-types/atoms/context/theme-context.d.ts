import React from 'react';
import type { ITheme, FILL } from '../interfaces';
import { THEME_COLOR } from '../interfaces';
export declare const ThemeContext: React.Context<ITheme | undefined>;
export declare const shouldInverse: (color: THEME_COLOR, theme?: ITheme | undefined, background?: THEME_COLOR | undefined) => boolean;
export declare const getThemeColor: (color: keyof typeof FILL, inverse?: boolean) => THEME_COLOR;
