import React from 'react';
import type { IAdjustStyleProps, PadSize } from '../../interfaces';
export interface IAdjustProps extends IAdjustStyleProps, React.AllHTMLAttributes<HTMLDivElement> {
}
export declare class Adjust extends React.Component<IAdjustProps> {
    static getPadClass: (pad?: PadSize) => string;
    render(): JSX.Element;
}
