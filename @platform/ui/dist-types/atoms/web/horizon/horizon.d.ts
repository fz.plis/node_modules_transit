import React from 'react';
import type { IHorizonStyleProps } from '../../interfaces';
import { ALIGN } from '../../interfaces';
import { HorizonSpacer } from './horizon-spacer';
export declare type IHorizonProps = IHorizonStyleProps & React.AllHTMLAttributes<HTMLDivElement>;
export declare class Horizon extends React.PureComponent<IHorizonProps> {
    static defaultProps: {
        align: ALIGN;
        allHeight: boolean;
    };
    static Spacer: typeof HorizonSpacer;
    render(): JSX.Element;
}
