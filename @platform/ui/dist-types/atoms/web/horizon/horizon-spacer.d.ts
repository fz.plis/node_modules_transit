import React from 'react';
export declare type IHorizonSpacerProps = React.AllHTMLAttributes<HTMLDivElement>;
export declare class HorizonSpacer extends React.PureComponent<IHorizonSpacerProps> {
    render(): JSX.Element;
}
