import React from 'react';
import type { IFontStyleProps } from '../../interfaces';
export declare enum FONT_LINE {
    /** Скроет остатки под три точки. */
    COLLAPSE = "COLLAPSE",
    /** Принудительные перенос длинных строк. */
    BREAK = "BREAK",
    NOWRAP = "NOWRAP"
}
export interface IFontStyleWebProps extends IFontStyleProps {
    className?: string;
    line?: keyof typeof FONT_LINE;
    clickable?: boolean;
}
export interface IWebFontProps extends React.AllHTMLAttributes<HTMLDivElement>, IFontStyleWebProps {
}
export declare type IShortWebFontProps = Omit<IWebFontProps, 'fill'>;
export interface IWebFontPropsForGetClass extends IFontStyleWebProps {
    children(boxClass: string): JSX.Element;
}
export declare class Font extends React.PureComponent<IWebFontProps> {
    static Base: React.ForwardRefExoticComponent<IShortWebFontProps & React.RefAttributes<Font>>;
    static Faint: React.ForwardRefExoticComponent<IShortWebFontProps & React.RefAttributes<Font>>;
    static Accent: React.ForwardRefExoticComponent<IShortWebFontProps & React.RefAttributes<Font>>;
    static Critic: React.ForwardRefExoticComponent<IShortWebFontProps & React.RefAttributes<Font>>;
    static Warning: React.ForwardRefExoticComponent<IShortWebFontProps & React.RefAttributes<Font>>;
    static Success: React.ForwardRefExoticComponent<IShortWebFontProps & React.RefAttributes<Font>>;
    static GetClass: React.FC<IWebFontPropsForGetClass>;
    render(): JSX.Element;
}
