import React from 'react';
import type { IBoxStyleProps } from '../../interfaces';
export interface IBoxWebStyleProps extends IBoxStyleProps {
    clickable?: boolean;
    className?: string;
}
export interface IWebBoxProps extends React.AllHTMLAttributes<HTMLDivElement>, IBoxWebStyleProps {
}
export declare type IShortWebBoxProps = Omit<IWebBoxProps, 'fill'>;
export interface IWebBoxPropsForGetClass extends IBoxWebStyleProps {
    children(boxClass: string): JSX.Element;
}
export declare class Box extends React.Component<IWebBoxProps> {
    static Base: React.ForwardRefExoticComponent<IShortWebBoxProps & React.RefAttributes<Box>>;
    static Faint: React.ForwardRefExoticComponent<IShortWebBoxProps & React.RefAttributes<Box>>;
    static Accent: React.ForwardRefExoticComponent<IShortWebBoxProps & React.RefAttributes<Box>>;
    static Critic: React.ForwardRefExoticComponent<IShortWebBoxProps & React.RefAttributes<Box>>;
    static Warning: React.ForwardRefExoticComponent<IShortWebBoxProps & React.RefAttributes<Box>>;
    static Success: React.ForwardRefExoticComponent<IShortWebBoxProps & React.RefAttributes<Box>>;
    static GetClass: React.FC<IWebBoxPropsForGetClass>;
    protected static getClassString: (props: IWebBoxProps, inverseBorder: boolean) => string;
    protected static getContentStatic: (props: IWebBoxProps, inverseBorder: boolean) => JSX.Element;
    protected static getContentWithContexts: (props: IWebBoxProps, getContent: (inverseBorder: boolean) => JSX.Element) => JSX.Element;
    render(): JSX.Element;
    private getContent;
}
