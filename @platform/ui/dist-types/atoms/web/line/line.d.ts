import React from 'react';
import type { ILineStyleProps } from '../../interfaces';
import { LINE_WEIGHT, LINE_FILL } from '../../interfaces';
export interface IWebLineProps extends ILineStyleProps, Omit<React.AllHTMLAttributes<HTMLDivElement>, 'children'> {
}
export declare type IShortWebLineProps = Omit<IWebLineProps, 'fill'>;
export declare class Line extends React.Component<IWebLineProps> {
    static defaultProps: {
        fill: LINE_FILL;
        weight: LINE_WEIGHT;
    };
    static Base: React.ForwardRefExoticComponent<IShortWebLineProps & React.RefAttributes<Line>>;
    static Faint: React.ForwardRefExoticComponent<IShortWebLineProps & React.RefAttributes<Line>>;
    static Accent: React.ForwardRefExoticComponent<IShortWebLineProps & React.RefAttributes<Line>>;
    static Critic: React.ForwardRefExoticComponent<IShortWebLineProps & React.RefAttributes<Line>>;
    static Warning: React.ForwardRefExoticComponent<IShortWebLineProps & React.RefAttributes<Line>>;
    static Success: React.ForwardRefExoticComponent<IShortWebLineProps & React.RefAttributes<Line>>;
    render(): JSX.Element;
}
