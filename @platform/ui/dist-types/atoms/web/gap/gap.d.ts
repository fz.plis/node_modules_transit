import React from 'react';
import type { IGapStyleProps } from '../../interfaces';
import { SIZE } from '../../interfaces';
export interface IGapProps extends IGapStyleProps {
    className?: string;
}
export declare class Gap extends React.PureComponent<IGapProps> {
    static defaultProps: {
        size: SIZE;
    };
    static SM: (props: IGapProps) => JSX.Element;
    static XS: (props: IGapProps) => JSX.Element;
    static X2S: (props: IGapProps) => JSX.Element;
    static LG: (props: IGapProps) => JSX.Element;
    static XL: (props: IGapProps) => JSX.Element;
    static X2L: (props: IGapProps) => JSX.Element;
    static X3L: (props: IGapProps) => JSX.Element;
    render(): JSX.Element;
}
