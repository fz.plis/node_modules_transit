import React from 'react';
import { SIZE } from '../../interfaces';
export interface IPatternProps extends React.AllHTMLAttributes<HTMLDivElement> {
    gap?: keyof typeof SIZE;
}
export interface IPatternSpanProps extends React.AllHTMLAttributes<HTMLDivElement> {
    size: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
}
export declare class PatternSpan extends React.Component<IPatternSpanProps> {
    render(): JSX.Element;
}
export declare class Pattern extends React.Component<IPatternProps> {
    static Span: typeof PatternSpan;
    render(): JSX.Element;
}
