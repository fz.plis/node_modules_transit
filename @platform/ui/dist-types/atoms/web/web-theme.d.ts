export declare const camelCaseToKebabCase: (val: string) => string;
export declare const toCssVars: (obj: any, prefix?: string) => any;
/** Отписка от событий смены темы. */
export declare const unsubFromThemeChange: () => void;
