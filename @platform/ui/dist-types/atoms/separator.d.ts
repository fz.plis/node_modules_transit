import React from 'react';
import type { IWebLineProps } from '../atoms/web/line';
export declare const Separator: React.FC<IWebLineProps>;
