import type { ITheme } from '../interfaces';
import type { IBlankOfTheme } from './theme-filler';
export declare type Sub = (theme: ITheme) => void;
export declare const setTheme: (nextTheme: IBlankOfTheme) => void;
export declare const getTheme: () => ITheme;
export declare const subscribeToTheme: (sub: Sub) => () => void;
