import type { ITheme } from '../interfaces';
export interface IBlankOfTheme {
    main: ITheme['main'];
    line?: {
        hover?: Partial<ITheme['line']['hover']>;
        fill?: Partial<ITheme['line']['fill']>;
        weight?: Partial<ITheme['line']['weight']>;
    };
    box?: {
        fill?: Partial<ITheme['box']['fill']>;
        hover?: Partial<ITheme['box']['hover']>;
        fancy?: {
            fill?: Partial<ITheme['box']['fancy']['fill']>;
            hover?: Partial<ITheme['box']['fancy']['hover']>;
        };
        radius?: Partial<ITheme['box']['radius']>;
        shadow?: Partial<ITheme['box']['shadow']>;
        shadowInner?: Partial<ITheme['box']['shadowInner']>;
    };
    font?: {
        fill?: Partial<ITheme['font']['fill']>;
        hover?: Partial<ITheme['font']['hover']>;
        family?: Partial<ITheme['font']['family']>;
        volume?: Partial<ITheme['font']['volume']>;
        lineHeight?: Partial<ITheme['font']['lineHeight']>;
        weight?: Partial<ITheme['font']['weight']>;
    };
    gap?: {
        size?: Partial<ITheme['gap']['size']>;
    };
    link?: Partial<ITheme['link']>;
}
export declare const fillTheme: (theme: IBlankOfTheme) => ITheme;
export declare const getDefaultsFromMainColors: (theme: IBlankOfTheme) => ITheme;
