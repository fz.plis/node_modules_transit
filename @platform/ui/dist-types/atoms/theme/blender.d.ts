export declare const blend: (color: string, secondColor: string) => string;
export declare const modify: (color: string, modifyer: number) => string;
