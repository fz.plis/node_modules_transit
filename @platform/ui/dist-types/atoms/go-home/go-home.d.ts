import React from 'react';
export interface IGoHomeProps {
    className?: string;
    onClick?(): void;
}
export declare class GoHome extends React.PureComponent<IGoHomeProps> {
    render(): JSX.Element;
}
