import React from 'react';
import type { ILogo, THEME_COLOR } from '../../atoms/interfaces';
export interface IWebLogoProps extends ILogo, React.AllHTMLAttributes<HTMLElement> {
    background?: THEME_COLOR;
}
export declare const Logo: React.ForwardRefExoticComponent<IWebLogoProps & React.RefAttributes<HTMLDivElement>>;
