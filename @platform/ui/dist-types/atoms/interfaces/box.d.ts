import type { FILL, LINE_FILL, LINE_WEIGHT } from './common';
import type { THEME_COLOR } from './theme';
/**
 * Тени.
 */
export declare enum SHADOW {
    SM = "SM",
    MD = "MD",
    LG = "LG"
}
/**
 * Тени внутриние.
 */
export declare enum SHADOW_INNER {
    SM = "SM",
    MD = "MD",
    LG = "LG"
}
/**
 * Радиус скругления.
 */
export declare enum RADIUS {
    /**
     * Экспериментальный дизайн.
     */
    XS = "XS",
    X2S = "X2S",
    SM = "SM",
    MD = "MD",
    LG = "LG",
    XL = "XL",
    X2L = "X2L",
    MAX = "MAX"
}
/**
 * Расположение скругленныъ углов.
 */
export declare enum RADIUS_APPLY {
    ALL = "ALL",
    TOP = "TOP",
    BOTTOM = "BOTTOM",
    RIGHT = "RIGHT",
    LEFT = "LEFT"
}
export interface IBoxStyleProps {
    fill?: keyof typeof FILL;
    shadow?: keyof typeof SHADOW;
    shadowInner?: keyof typeof SHADOW_INNER;
    radius?: keyof typeof RADIUS | [keyof typeof RADIUS, keyof typeof RADIUS_APPLY];
    border?: keyof typeof LINE_FILL | [keyof typeof LINE_FILL, keyof typeof LINE_WEIGHT];
    background?: THEME_COLOR;
    inverse?: boolean;
    fancy?: boolean;
    lightup?: boolean;
    lightdown?: boolean;
}
