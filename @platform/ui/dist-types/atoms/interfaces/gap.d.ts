export declare enum SIZE {
    X2S = "X2S",
    XS = "XS",
    SM = "SM",
    MD = "MD",
    LG = "LG",
    XL = "XL",
    X2L = "X2L",
    X3L = "X3L"
}
export interface IGapStyleProps {
    size?: keyof typeof SIZE;
    inline?: boolean;
}
