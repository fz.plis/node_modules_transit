import type { LINE_FILL, LINE_WEIGHT } from './common';
import type { THEME_COLOR } from './theme';
export interface ILineStyleProps {
    fill?: keyof typeof LINE_FILL;
    weight?: keyof typeof LINE_WEIGHT;
    vertical?: boolean;
    background?: THEME_COLOR;
}
