/**
 * Цвет заливки.
 */
export declare enum FILL {
    BASE = "BASE",
    FAINT = "FAINT",
    ACCENT = "ACCENT",
    CRITIC = "CRITIC",
    WARNING = "WARNING",
    SUCCESS = "SUCCESS"
}
/**
 * Цвет границ.
 */
export declare enum LINE_FILL {
    BASE = "BASE",
    FAINT = "FAINT",
    ACCENT = "ACCENT",
    CRITIC = "CRITIC",
    WARNING = "WARNING",
    SUCCESS = "SUCCESS",
    TRANSPARENT = "TRANSPARENT"
}
export declare enum LINE_WEIGHT {
    /**
     * Экспериментальный дизайн.
     */
    XS = "XS",
    SM = "SM",
    MD = "MD",
    LG = "LG"
}
