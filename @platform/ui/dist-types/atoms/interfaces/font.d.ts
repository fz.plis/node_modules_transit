import type { FILL } from './common';
import type { THEME_COLOR } from './theme';
/**
 * Размер шрифта.
 */
export declare enum VOLUME {
    XS = "XS",
    SM = "SM",
    MD = "MD",
    LG = "LG",
    XL = "XL",
    X2L = "X2L",
    X3L = "X3L",
    X4L = "X4L"
}
/**
 * Начертание шрифта.
 */
export declare enum WEIGHT {
    LIGHT = "LIGHT",
    MEDIUM = "MEDIUM",
    REGULAR = "REGULAR",
    BOLD = "BOLD"
}
/**
 * Стиль шрифта.
 */
export declare enum MODE {
    NORMAL = "NORMAL",
    ITALIC = "ITALIC"
}
/**
 * Выравнивание по горизонтали.
 */
export declare enum TEXT_ALIGN {
    LEFT = "LEFT",
    CENTER = "CENTER",
    RIGHT = "RIGHT"
}
/**
 * Высота строки.
 */
export declare enum FONT_LINE_HEIGHT {
    SM = "SM",
    XS = "XS",
    MD = "MD",
    LG = "LG"
}
/**
 * Общий интерфейс компонента `Font`.
 */
export interface IFontStyleProps {
    volume?: keyof typeof VOLUME;
    weight?: keyof typeof WEIGHT;
    mode?: keyof typeof MODE;
    fill?: keyof typeof FILL;
    align?: keyof typeof TEXT_ALIGN;
    lineHeight?: keyof typeof FONT_LINE_HEIGHT;
    background?: THEME_COLOR;
    noSelect?: boolean;
    inline?: boolean;
}
