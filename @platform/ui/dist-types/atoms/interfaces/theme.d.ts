export interface IFontSize {
    xs: string;
    sm: string;
    md: string;
    lg: string;
    xl: string;
    x2l: string;
    x3l: string;
    x4l: string;
}
export declare enum THEME_COLOR {
    BASE_STRONG = "BASE_STRONG",
    FAINT_STRONG = "FAINT_STRONG",
    ACCENT_STRONG = "ACCENT_STRONG",
    WARNING_STRONG = "WARNING_STRONG",
    SUCCESS_STRONG = "SUCCESS_STRONG",
    CRITIC_STRONG = "CRITIC_STRONG",
    BASE_WEAK = "BASE_WEAK",
    FAINT_WEAK = "FAINT_WEAK",
    ACCENT_WEAK = "ACCENT_WEAK",
    WARNING_WEAK = "WARNING_WEAK",
    SUCCESS_WEAK = "SUCCESS_WEAK",
    CRITIC_WEAK = "CRITIC_WEAK"
}
export interface IThemeFontWeightValues {
    light: string;
    regular: string;
    medium: string;
    bold: string;
}
export interface IThemeFontVolumeValues {
    xs: string;
    sm: string;
    md: string;
    lg: string;
    xl: string;
    x2l: string;
    x3l: string;
    x4l: string;
}
export interface IThemeFontLineHeightValues {
    sm: string;
    xs: string;
    md: string;
    lg: string;
}
export interface IThemeFontFamilyValues {
    primary: string;
    secondary: string;
}
export interface IThemeColorValues {
    baseStrong: string;
    faintStrong: string;
    accentStrong: string;
    warningStrong: string;
    successStrong: string;
    criticStrong: string;
    baseWeak: string;
    faintWeak: string;
    accentWeak: string;
    warningWeak: string;
    successWeak: string;
    criticWeak: string;
}
export interface IThemeGapSizeValues {
    x2s: string;
    xs: string;
    sm: string;
    md: string;
    lg: string;
    xl: string;
    x2l: string;
    x3l: string;
}
export interface IThemeBoxShadowValues {
    sm: string;
    md: string;
    lg: string;
}
export interface IThemeLineWeightValues {
    sm: string;
    md: string;
    lg: string;
}
export interface IThemeBoxRadiusValues {
    x2s: string;
    xs: string;
    sm: string;
    md: string;
    lg: string;
    xl: string;
    x2l: string;
}
export interface IThemeModesValues {
    lightUp: number;
    lightDown: number;
}
export interface ILinkTheme {
    underline: {
        fill: {
            accentStrong: string;
        };
        hover: {
            accentStrong: string;
        };
    };
}
export interface ITheme {
    main: {
        name: string;
        color: IThemeColorValues;
        modes: IThemeModesValues;
        inverseContentColorOn: THEME_COLOR[];
    };
    line: {
        hover: IThemeColorValues;
        fill: IThemeColorValues;
        weight: IThemeLineWeightValues;
    };
    box: {
        fill: IThemeColorValues;
        hover: IThemeColorValues;
        fancy: {
            fill: IThemeColorValues;
            hover: IThemeColorValues;
        };
        lightup: {
            fill: IThemeColorValues;
        };
        lightdown: {
            fill: IThemeColorValues;
        };
        radius: IThemeBoxRadiusValues;
        shadow: IThemeBoxShadowValues;
        shadowInner: IThemeBoxShadowValues;
    };
    font: {
        fill: IThemeColorValues;
        hover: IThemeColorValues;
        family: IThemeFontFamilyValues;
        volume: IThemeFontVolumeValues;
        lineHeight: IThemeFontLineHeightValues;
        weight: IThemeFontWeightValues;
    };
    gap: {
        size: IThemeGapSizeValues;
    };
    link: ILinkTheme;
}
export declare enum COLOR_TYPE {
    NONE = "NONE",
    COLOR = "COLOR",
    LINEAR_GRADIENT = "LINEAR_GRADIENT"
}
