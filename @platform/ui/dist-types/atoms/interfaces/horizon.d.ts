export declare enum ALIGN {
    STRETCH = "STRETCH",
    TOP = "TOP",
    CENTER = "CENTER",
    BOTTOM = "BOTTOM"
}
export interface IHorizonStyleProps {
    align?: keyof typeof ALIGN;
    allHeight?: boolean;
}
