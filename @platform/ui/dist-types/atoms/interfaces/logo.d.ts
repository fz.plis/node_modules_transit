export declare enum SCALE {
    SM = "SM",
    MD = "MD"
}
/**
 * Заливка лого.
 */
export declare enum LOGO_FILL {
    /**
     * Цветное.
     */
    COLOR = "COLOR",
    /**
     * Монохромное.
     */
    BW = "BW"
}
export interface ILogo {
    scale?: keyof typeof SCALE;
    fill?: keyof typeof LOGO_FILL;
    /**
     * Изменяет заливку для контрастного фона при `fill=LOGO_FILL.BW`.
     */
    inverse?: boolean;
}
