import type { SIZE } from './gap';
export declare enum HOR {
    LEFT = "LEFT",
    RIGHT = "RIGHT",
    CENTER = "CENTER"
}
export declare enum VERT {
    TOP = "TOP",
    CENTER = "CENTER",
    BOTTOM = "BOTTOM"
}
declare type Key = keyof typeof SIZE | null | undefined;
export declare type PadSize = Key | [Key, Key, Key, Key] | [Key, Key] | [Key];
export interface IAdjustStyleProps {
    hor?: keyof typeof HOR;
    vert?: keyof typeof VERT;
    pad?: PadSize;
}
export {};
