import React from 'react';
export declare enum TRANSPARENCY {
    XS = "XS",
    SM = "SM",
    MD = "MD",
    LG = "LG",
    XL = "XL"
}
export interface ITransparencyStyleProps {
    volume?: keyof typeof TRANSPARENCY;
}
export interface IWebTransparencyProps extends ITransparencyStyleProps, React.AllHTMLAttributes<HTMLDivElement> {
}
export interface IWebTransparencyPropsForGetClass extends ITransparencyStyleProps {
    children(transparencyClass: string): React.ReactNode;
}
export declare class Transparency extends React.PureComponent<IWebTransparencyProps> {
    static defaultProps: {
        volume: "MD";
    };
    static SM: (props: IWebTransparencyProps) => JSX.Element;
    static XS: (props: IWebTransparencyProps) => JSX.Element;
    static LG: (props: IWebTransparencyProps) => JSX.Element;
    static XL: (props: IWebTransparencyProps) => JSX.Element;
    static GetClass: React.FC<IWebTransparencyPropsForGetClass>;
    render(): JSX.Element;
}
