import type React from 'react';
import type { THEME_COLOR, FILL } from '../interfaces';
import { LINE_FILL } from '../interfaces';
export interface IShouldInverseContentProps {
    background?: THEME_COLOR;
    color: keyof typeof FILL | keyof typeof LINE_FILL;
    children(inverse: boolean, background: THEME_COLOR): React.ReactElement;
}
export declare const ShouldInverseContent: React.SFC<IShouldInverseContentProps>;
