import React from 'react';
export declare const ThemeProvider: React.SFC<{
    children: React.ReactNode;
}>;
