import React from 'react';
import type { Font } from '../atoms';
import { Box } from '../atoms';
export interface IColumn<T = any> {
    title: string;
    align?: Font['props']['align'];
    width?: number | string;
    selector(row: T): React.ReactNode;
}
export interface IMiniTableProps {
    columns: IColumn[];
    rows: any[];
    selectedRows?: any[];
    onChangeSelectedRows?(rows: any[]): void;
    selectable: boolean;
}
export declare const MiniTable: React.ForwardRefExoticComponent<IMiniTableProps & React.RefAttributes<Box>>;
