export declare const fillString: (char: string, len: number, str?: string, fromStart?: boolean) => string;
export declare const replaceChar: (str: string, char: string, position: number) => string;
export declare const replaceChars: (str: string, char: string, from: number, to: number) => string;
export declare const insertChar: (str: string, char: string, position: number) => string;
export declare const format: (template: string, ...args: any[]) => string;
