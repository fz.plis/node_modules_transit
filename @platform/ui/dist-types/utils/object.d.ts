export declare const deepEqual: (x: any, y: any) => boolean;
export declare const setObjectValue: <T extends Record<string, unknown>>(obj: Record<string, any>, path: string, value: any) => T;
export declare const getObjectValue: <T extends Record<string, any>>(obj: T, path: string, defaultValue?: any) => unknown;
