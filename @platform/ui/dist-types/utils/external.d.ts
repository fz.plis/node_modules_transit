import scrollIntoViewIfNeeded from 'scroll-into-view-if-needed';
export * from 'scroll-into-view-if-needed';
export declare const scrollIntoView: typeof scrollIntoViewIfNeeded;
