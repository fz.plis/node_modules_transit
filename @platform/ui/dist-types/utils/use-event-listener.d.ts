declare type Listener<K extends keyof WindowEventMap> = (this: Window, ev: WindowEventMap[K]) => any;
/**
 * Хук для навешивания обработчика на событие.
 *
 * @example useEventListener('mousemove', () => { ... });
 *
 * @param eventName Событие.
 * @param handler Обработчик события.
 */
export declare const useEventListener: <K extends keyof WindowEventMap>(eventName: K, handler: Listener<K>) => void;
export {};
