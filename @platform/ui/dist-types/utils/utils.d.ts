export { IDebouncer, debounce, throttle, getChildrenChecker, ICheckChildrendParams, noop, mergeRefs } from '../core';
export declare const identity: (s: any) => any;
/**
 * Проверяет что переданый элемент сожержит тэги, т.е. Не является обычной строкой, числом или другим примитивом.
 *
 * @param node Элемент для проверки.
 */
export declare const isNodeWithTags: (node: any) => boolean;
export declare const getCssVarName: (name: string) => string;
