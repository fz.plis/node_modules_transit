/**
 * Переносит фокус на HTML элемент.
 *
 * @param name - Значение атрибутов name или data-name, элемента на который будет перенесён фокус.
 */
export declare const focusByName: (name: string) => void;
