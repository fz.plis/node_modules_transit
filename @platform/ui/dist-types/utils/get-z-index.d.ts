/**
 * Z-индексы для осносновных типов компонентов.
 */
export declare enum Z_LAYERS {
    SPECIFIC_ELEMENT = "SPECIFIC_ELEMENT",
    SPECIFIC_ELEMENT_ACCENT = "SPECIFIC_ELEMENT_ACCENT",
    CONTROL = "CONTROL",
    FLUID = "FLUID",
    FIX = "FIX",
    TOPLINE = "TOPLINE",
    SIDEBAR = "SIDEBAR",
    MODAL = "MODAL",
    CONTAINER = "CONTAINER",
    MESSAGE = "MESSAGE"
}
/**
 * Функция для создания класса с определенным z-index
 * Результат функции необходимо добавить к остальным стилям компонента через библиотеку classnames
 * Функция возвращает имя соответствующего css-класса.
 *
 * @param layer Слой для элемента.
 * @example
 * ```
 * <Box className={cn(css.container, getIndex(Z_INDEXES.MODAL))}>
 * ```
 */
export declare const getIndex: (layer: Z_LAYERS) => string;
/** Функция вернет z-index для переданного слоя. */
export declare const getLayerIndex: (layer: Z_LAYERS) => number;
