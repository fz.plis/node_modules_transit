import React from 'react';
import type { PadSize } from '../atoms';
export declare type Dimension = 'LG' | 'MD' | 'SM';
interface ISkeletonProps {
    /** Ширина рядов в скелетоне, задаётся числом пикселей или строкой для CSS свойства `width`. */
    width?: number | string;
    /** Количество рядов, по-умолчанию 1. */
    rows?: number;
    /** Компонент для отступ между рядами, по-умолчанию `Gap.MD`. */
    gap?: React.ReactNode;
    /** Проп для задания отступов, по-умолчанию не установлены. */
    padding?: PadSize;
}
/** Компонент для отображения процесса загрузки контента, по-умолчанию отображает один анимированный ряд. */
export declare const Skeleton: React.FC<ISkeletonProps>;
export {};
