import React from 'react';
export declare enum LABEL_POSITION {
    TOP = "top",
    LEFT = "left",
    RIGHT = "right",
    BOTTOM = "bottom"
}
export declare enum LABEL_TEXT_ALIGN {
    TOP = "top",
    CENTER = "center",
    BOTTOM = "bottom",
    LEFT = "left",
    RIGHT = "right"
}
export interface ILabelProps extends React.HTMLProps<HTMLDivElement> {
    className?: string;
    width?: number | string;
    position?: LABEL_POSITION;
    textAlign?: LABEL_TEXT_ALIGN;
    margin?: string;
    forId?: string;
    text?: string;
    children?: React.ReactChildren | React.ReactNode;
    onClick?(e: React.MouseEvent<HTMLSpanElement>): void;
    onMouseEnter?(e: React.MouseEvent<HTMLSpanElement>): void;
    onMouseLeave?(e: React.MouseEvent<HTMLSpanElement>): void;
    disableFullWidth?: boolean;
    /**
     * Надстрочный текст.
     */
    superscript?: string;
    /**
     * Текст для информера.
     */
    informerText?: string;
    /**
     * Полупрозрачность.
     *
     * @description Необходимо добавлять полупрозрачность для читаемости текста в блоках с цветовой заливкой.
     */
    withOpacity?: boolean;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const Label: React.FC<ILabelProps>;
