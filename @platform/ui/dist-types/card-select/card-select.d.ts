import React from 'react';
import type { IInputWithValidation } from '../core/interfaces';
import type { ISelectGeneralProps } from '../interfaces';
import type { ICardSelectOption } from './card-select-option';
/**
 * Интерфейс пропертей компонента CardSelect (карточки выбора).
 */
export interface ICardSelectProps<T> extends ISelectGeneralProps<T, ICardSelectOption>, Omit<React.HTMLProps<HTMLDivElement>, 'onChange' | 'value'>, IInputWithValidation {
    /**
     * Максимальное кол-во колонок в ряду.
     *
     * @default 5.
     */
    columns?: number;
}
export declare const CardSelect: {
    <T>({ value, name, options, columns: colsCount, onChange, touched, errorText, warningText, className, }: ICardSelectProps<T>): JSX.Element;
    displayName: string;
};
