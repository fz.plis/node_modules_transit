import React from 'react';
import type { IWebBoxProps } from '../atoms/web/box';
import type { IOption } from '../core/interfaces';
import type { WebIcon } from '../icons';
/**
 * Интерфейс, описывающий карточку.
 */
export interface ICardSelectOption extends Omit<IOption, 'label'> {
    /**
     * Иконка карточки.
     */
    icon: WebIcon;
    /**
     * Дополнительная иконка карточки, которая отображается тогда, когда у нас опция не активна.
     */
    disabledIcon?: WebIcon;
    /**
     * Компонент с лэйблом карточки.
     */
    label: React.ReactNode;
    /**
     * Флаг, при котором опция не активна.
     */
    disabled?: boolean;
}
/**
 * Интерфейс пропертей компонента CardSelectOption.
 */
export interface ICardSelectOptionProps extends ICardSelectOption, Omit<IWebBoxProps, 'label' | 'value'> {
    /**
     * Признак того, что карточка выбрана.
     */
    selected: boolean;
}
export declare const CardSelectOption: React.FC<ICardSelectOptionProps>;
export declare const InvisibleCard: React.FC;
