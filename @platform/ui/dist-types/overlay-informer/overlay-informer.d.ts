import React from 'react';
import type { IBoxWebStyleProps } from '../atoms';
export interface IOverlayInformerProps extends IBoxWebStyleProps {
    actionText?: string;
    onActionClick?(): void;
    onClose?(): void;
}
export declare const OverlayInformer: React.FC<IOverlayInformerProps>;
