import type React from 'react';
import type { FormApi } from 'final-form';
import type { FieldInputProps, FieldMetaState } from 'react-final-form';
export interface IServerValidationResult {
    fieldName: string;
    messages: string[];
}
export interface IServerValidation {
    errors: IServerValidationResult[];
    warnings: IServerValidationResult[];
}
export interface IValidationResult {
    errors: Record<string, string[]>;
    warnings: Record<string, string[]>;
}
export declare enum RULE_SEVERITIES {
    ERROR = "ERROR",
    WARNING = "WARNING"
}
export interface IConditionInfo {
    id: string;
    path: string;
    params?: Record<string, any>;
}
export interface IRuleInfo {
    id: string;
    conditions?: IConditionInfo[];
    message: string;
    messageParams?: string;
    params?: Record<string, any>;
    severity: RULE_SEVERITIES;
}
export interface IFieldValidationRules {
    path: string;
    rules: IRuleInfo[];
    dependentPaths?: string[];
}
export declare type OnChangeType<TValue = any, T extends HTMLElement = HTMLElement> = ({ value, event, input, meta, formApi, }: {
    value: TValue;
    event: React.FocusEvent<HTMLElement>;
    input: FieldInputProps<TValue, T>;
    meta: FieldMetaState<TValue>;
    formApi: FormApi;
}) => void;
export declare type OnBlurType<TValue = any, T extends HTMLElement = HTMLElement> = ({ event, input, meta, formApi, }: {
    event: React.FocusEvent<HTMLElement>;
    input: FieldInputProps<TValue, T>;
    meta: FieldMetaState<TValue>;
    formApi: FormApi;
}) => void;
export declare type FieldedComponentType<T> = React.ComponentType<Omit<T, 'onBlur' | 'onChange'> & {
    name: string;
    onChange?: OnChangeType;
    onBlur?: OnBlurType;
    fieldFormat?: string;
}>;
export interface IAdapterProps<TValue = any> {
    touched?: boolean;
    errorText?: string;
    hasError?: boolean;
    hasWarning?: boolean;
    value?: TValue;
    format?: string;
    warningText?: string;
    onBlur?(e: React.FocusEvent<HTMLElement>): void;
    onChange?(value: TValue, e: React.FocusEvent<HTMLElement>): void;
}
export declare type IFieldValidationResult = Record<string, {
    error: string;
    warning: string;
}>;
