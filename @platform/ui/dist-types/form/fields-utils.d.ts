import React from 'react';
import type { AnyObject } from 'react-final-form';
import type { FieldedComponentType, IAdapterProps } from './interfaces';
/**
 * HOC для создания компонента формы.
 *
 * @param InputComponent Компонент ввода.
 */
export declare const createField: <T = IAdapterProps<any>>(InputComponent: React.ComponentType<T>, params?: AnyObject) => FieldedComponentType<T>;
