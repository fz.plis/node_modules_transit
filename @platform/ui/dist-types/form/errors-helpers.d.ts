import type { AnyObject, FormState } from 'final-form';
import type { IValidationControlItem } from '../validation-controls';
/**
 * Метод форматирования ошибки.
 *
 * @param state State формы.
 * @param fieldLabels Метки полей формы.
 *
 * @example
 * const { getState, mutators } = useForm();
 * const formState = getState();
 *
 * const [errors, warnings] = useMemo(() => formatErrors(formState, FORM_FIELD_LABELS), [formState]);
 *
 * <Validation errors={errors} warnings={warnings} />
 */
export declare const formatErrors: <T>(state: FormState<Partial<T>, Partial<Partial<T>>>, fieldLabels: AnyObject) => IValidationControlItem[][];
