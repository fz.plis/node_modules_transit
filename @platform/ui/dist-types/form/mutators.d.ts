import type { Tools, MutableState, AnyObject } from 'final-form';
import type { IServerValidation } from './interfaces';
export declare const fieldsMutators: {
    setValueMutator: <T>([path, value]: [string, any], state: MutableState<T, Partial<T>>, tools: Tools<T, Partial<T>>) => void;
    updateMutator: <T_1 extends AnyObject>([data]: [T_1], state: MutableState<T_1, Partial<T_1>>) => void;
    updateDocumentValuesMutator: <T_2 extends AnyObject>([data]: [T_2], state: MutableState<T_2, Partial<T_2>>) => void;
    setServerError: <T_3>([validationResults]: [IServerValidation], state: MutableState<T_3, Partial<T_3>>) => void;
};
