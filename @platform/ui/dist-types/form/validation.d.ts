import type { IServerValidation, IValidationResult, IFieldValidationResult } from './interfaces';
export declare const transformServerValidation: (validateResult: IServerValidation) => IFieldValidationResult;
export declare const transformFormValidation: <T>(values: T, validate: (allValues: T) => IValidationResult) => IFieldValidationResult;
