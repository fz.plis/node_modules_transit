import React from 'react';
import type { AnyObject } from 'final-form';
import type { FormProps } from 'react-final-form';
/**
 * Компонент формы.
 *
 * @default
 * Встроенные мутаторы: setValueMutator, updateMutator,
 * updateDocumentValuesMutator, setServerError
 *
 * @example
 * <Form handleSubmit={values => ...} validateOnBlur initialValues={{ test: '' }}
 *   render={({ handleSubmit }) =>
 *     <form onSubmit={handleSubmit}>
 *      <Fields.Text name="test" />
 *      <PrimaryButton type="submit">Save</PrimaryButton>
 *     </form>
 *   }
 * />
 *
 * @see {@link https://final-form.org/react}
 */
export declare const Form: <FormValues = AnyObject>({ mutators, ...rest }: FormProps<FormValues, Partial<FormValues>>) => React.ReactElement;
