import React from 'react';
export declare const ScrollYSubContext: React.Context<(sub: (scrollY: number) => void) => () => void>;
export default ScrollYSubContext;
