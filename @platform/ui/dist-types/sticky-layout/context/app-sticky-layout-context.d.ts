/// <reference types="react" />
export interface IPositionProps {
    top?: number;
    left?: number;
    width?: number;
    height?: number;
}
/**
 * Props provided for sticky elements.
 */
export interface IStickyUpdateProps {
    /**
     * When `true` element is sticked.
     */
    sticky: boolean;
    /**
     * Style props for render sticky element.
     */
    position: IPositionProps;
}
export declare type StickyUpdater = (props: IStickyUpdateProps) => void;
/**
 * Function that register element as sticky.
 *
 * @param comp Instance of React Component.
 * @param updater Callback for update state of `comp`.
 * @param fluid When `true` element disappears on scroll drown and appears on scroll up.
 * @returns Unregister func.
 */
export declare type RegisterFunc = (domref: HTMLElement, updater: StickyUpdater, fluid?: boolean, top?: boolean) => () => void;
/**
 * Props for sticky layout context.
 */
export interface IAppStickyLayoutContext {
    /**
     * Function for register new sticky element.
     */
    register?: RegisterFunc;
}
/**
 * Context for AppStickyLayout.
 *
 * @see AppStickyLayout
 * @deprecated Функционал оказался забагованым.
 * На данный момент только reg-doc и eco-irao используют.
 * Регдок обещают переписать к осени. В irao нужно отдельную задачу на правку.
 */
export declare const AppStickyLayoutContext: import("react").Context<IAppStickyLayoutContext>;
export declare const ScrollToContext: import("react").Context<(left: number, top: number) => void>;
