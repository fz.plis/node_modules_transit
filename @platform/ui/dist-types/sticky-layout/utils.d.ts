import type { IStickyUpdateProps } from './context/app-sticky-layout-context';
import type { IStickyStorage, IStickyStorageItem } from './interfaces';
export declare type IStickyUpdateResult = Record<string, IStickyUpdateProps>;
export interface IStickyUpdateOptions {
    root: HTMLElement;
    items: IStickyStorage;
    scroll: number;
    previousScroll: number;
}
export interface ICalcStickyStepResult {
    sticky: boolean;
    position: any;
    newStackHeight: number;
    lastSticky?: number;
}
export declare const uuid: () => string;
export declare const next: (_: number, el: HTMLElement, stackHeight: number) => ICalcStickyStepResult;
export declare const nextFluid: (item: IStickyStorageItem, delta: number, _: number, stackHeight: number, lastStickyTop: number) => ICalcStickyStepResult;
export declare const nextItem: (updates: any, items: IStickyStorage, id: string, delta: number, scroll: number) => ICalcStickyStepResult;
export declare const getStackUpdates: (options: IStickyUpdateOptions) => IStickyUpdateResult;
