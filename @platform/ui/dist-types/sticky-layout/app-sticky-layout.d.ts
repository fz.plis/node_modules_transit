import React from 'react';
export interface ILayoutScrollPropsUIBase {
    style?: React.CSSProperties;
    onScroll?(e: any): void;
    ref: React.Ref<ILayoutScrollComponent>;
}
export declare type ILayoutScrollComponent = React.ComponentType<ILayoutScrollPropsUIBase> & {
    scrollTop(top: number): void;
    scrollLeft(left: number): void;
    getScrollTop(): number;
};
/**
 * Props for `AppStickyLayout`.
 *
 * @see AppStickyLayout
 */
export interface IAppStickyLayoutProps extends React.AllHTMLAttributes<HTMLDivElement> {
    onScroll?(e: any): void;
    CustomLayoutScrollComponent?: ILayoutScrollComponent;
}
/**
 * Component create stacked sticky layout and watch for the children that wraps in `Sticky` component.
 *
 * When layout is scrolled down it's sticky children hold on top of the layout.
 *
 * When several children are sticked they are placed one under the other, like a stack.
 *
 * @summary Creates layout with children that can stick to the top of layout.
 * @deprecated Функционал оказался забагованым.
 * На данный момент только reg-doc и eco-irao используют.
 * Регдок обещают переписать к осени. В irao нужно отдельную задачу на правку.
 */
export declare class AppStickyLayout extends React.Component<IAppStickyLayoutProps> {
    constructor(props: IAppStickyLayoutProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    private storage;
    private rs?;
    private scrollY;
    private root?;
    private scrollBar?;
    private scrollSubs;
    private windowSize;
    private contexValue;
    scrollTo: (left: number, top: number) => void;
    private renderInner;
    private renderProvider;
    private getScrollTop;
    private handleRef;
    private handleResize;
    private register;
    private unregister;
    private update;
    private calcSticky;
    private resetLayout;
    private subToScollY;
    private handleScroll;
    render(): JSX.Element;
}
