import type { IStickyUpdateProps, StickyUpdater } from './context/app-sticky-layout-context';
export interface IStickyStorageItem extends IStickyUpdateProps {
    domRef: HTMLElement;
    updater: StickyUpdater;
    fluid?: boolean;
}
export declare type IStickyStorage = Record<string, IStickyStorageItem>;
