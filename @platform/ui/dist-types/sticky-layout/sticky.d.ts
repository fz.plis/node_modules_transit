import React from 'react';
/**
 * Props for `StickyComponent` component.
 */
export interface IStickyProps {
    /**
     * When `true` component registered as `fluid`.
     *
     * It hides on scroll down and appears on scroll up.
     */
    fluid?: boolean;
    /**
     * Called after component became sticked.
     */
    onPin?(): void;
    /**
     * Called after component became usual.
     */
    onUnpin?(): void;
    children?: React.ReactNode;
    top?: boolean;
}
/**
 * State props of `StickyComponent`.
 */
interface IStickyState {
    /**
     * When `true` element is sticked in contaner.
     */
    sticky: boolean;
    /**
     * Style of container.
     *
     * Empty when sticky is `false`.
     */
    style: React.CSSProperties;
}
/**
 * @deprecated Функционал оказался забагованым.
 * На данный момент только reg-doc и eco-irao используют.
 * Регдок обещают переписать к осени. В irao нужно отдельную задачу на правку.
 */
export declare class Sticky extends React.Component<IStickyProps, IStickyState> {
    static defaultProps: {
        fluid: boolean;
        onPin: () => void;
        onUnpin: () => void;
    };
    state: IStickyState;
    componentDidMount(): void;
    componentDidUpdate(_: any, prevState: IStickyState): void;
    componentWillUnmount(): void;
    static contextType: React.Context<import("./context/app-sticky-layout-context").IAppStickyLayoutContext>;
    private unregister;
    private onUpdate;
    render(): JSX.Element;
}
export {};
