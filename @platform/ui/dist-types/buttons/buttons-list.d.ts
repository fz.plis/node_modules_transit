import React from 'react';
import type { IRegularButtonProps } from '../buttons/regular-button';
import type { IButtonAction, BUTTON } from '../interfaces';
export interface IButtonsListProps extends React.AllHTMLAttributes<HTMLDivElement> {
    actions?: IButtonAction[];
    variant?: keyof typeof BUTTON;
    dimension?: IRegularButtonProps['dimension'];
}
export declare const ButtonsList: React.ForwardRefExoticComponent<IButtonsListProps & React.RefAttributes<HTMLDivElement>>;
