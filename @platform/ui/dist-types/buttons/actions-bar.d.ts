import React from 'react';
import type { WebIcon } from '../icons';
import type { IButtonAction } from '../interfaces';
export interface IActionBarAction extends IButtonAction {
    children?: IButtonAction[];
    icon: WebIcon;
}
export interface IActionsBarProps extends React.AllHTMLAttributes<HTMLDivElement> {
    visibleCount?: number;
    actions?: IActionBarAction[];
}
/**
 * Панель групповых действий.
 */
export declare const ActionsBar: React.FC<IActionsBarProps>;
