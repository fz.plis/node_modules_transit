import { RADIUS } from '../atoms';
import { VOLUME } from '../atoms/interfaces';
import { ICON_SCALE } from '../icons';
export declare type Dimension = 'LG' | 'MC' | 'MD' | 'SM';
export declare type Rounding = 'ROUND' | 'STANDART';
/** Согласно дизайн системе может быть только 2 вида скругления. */
export declare const RADIUS_BUTTON: {
    STANDART: RADIUS;
    ROUND: RADIUS;
};
export declare const ICON_SIZE_FROM_DIMENSION: Record<string, ICON_SCALE>;
export declare const FONT_SIZE_FROM_DIMENSION: Record<string, VOLUME>;
export interface IGetButtonClsName {
    dimension: Dimension;
    hasIcon?: boolean;
    className?: string;
}
/** В размерах gap не хватает ряда размеров, поэтому они добавлены через css классы отдельно для кнопок. */
export declare const getButtonClsName: ({ hasIcon, dimension, className }: IGetButtonClsName) => string;
