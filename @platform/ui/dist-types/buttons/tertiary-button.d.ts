import React from 'react';
import type { WebIcon } from '../icons';
import type { Dimension, Rounding } from './utils';
/**
 * Свойства серой кнопки.
 */
export interface ITertiaryButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    /**
     * Размер.
     */
    dimension?: Dimension;
    /**
     * Иконка.
     */
    icon?: WebIcon;
    /**
     * Флаг загрузки.
     */
    loading?: boolean;
    /**
     * Степень скругление краёв.
     */
    rounding?: Rounding;
}
/**
 * Серая кнопка.
 *
 * Кнопка для действия в карточке скроллера.
 *
 * {@link https://app.zeplin.io/project/5b6c23b90422817783ccc482/screen/5cd9631f38bfb567f0057bf2}.
 */
export declare const TertiaryButton: React.ForwardRefExoticComponent<ITertiaryButtonProps & React.RefAttributes<HTMLButtonElement>>;
