import React from 'react';
import type { WebIcon } from '../icons';
export interface IActionButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    icon?: WebIcon;
}
export declare const ActionButton: React.ForwardRefExoticComponent<IActionButtonProps & React.RefAttributes<HTMLButtonElement>>;
