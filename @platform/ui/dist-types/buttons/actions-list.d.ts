import React from 'react';
import type { IWebSVGIconProps } from '../icons';
import type { IActionListAction } from '../interfaces';
export interface IActionsListProps extends React.AllHTMLAttributes<HTMLDivElement> {
    visibleCount?: number;
    actions?: IActionListAction[];
    menuIcon?: React.ComponentType<IWebSVGIconProps>;
}
export declare const ActionsList: React.FC<IActionsListProps>;
