import React from 'react';
import type { WebIcon } from '../icons';
import type { Rounding, Dimension } from './utils';
export interface IRegularButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    dimension?: Dimension;
    icon?: WebIcon;
    loading?: boolean;
    rounding?: Rounding;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const RegularButton: React.ForwardRefExoticComponent<IRegularButtonProps & React.RefAttributes<HTMLButtonElement>>;
