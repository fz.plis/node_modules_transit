import React from 'react';
import type { WebIcon } from '../icons';
import type { Dimension, Rounding } from './utils';
export interface IPrimatyButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
    dimension?: Dimension;
    icon?: WebIcon;
    loading?: boolean;
    rounding?: Rounding;
    /**
     * Экспериментальный дизайн.
     */
    extraSmall?: boolean;
}
export declare const PrimaryButton: React.ForwardRefExoticComponent<IPrimatyButtonProps & React.RefAttributes<HTMLButtonElement>>;
