import React from 'react';
import type { IWebBoxProps } from '../atoms';
import { Box } from '../atoms';
import type { WebIcon } from '../icons';
export interface ILinkButtonProps extends Omit<IWebBoxProps, 'clickable'> {
    icon: WebIcon;
    hideBorder?: boolean;
}
export declare const LinkButton: React.ForwardRefExoticComponent<ILinkButtonProps & React.RefAttributes<Box>>;
