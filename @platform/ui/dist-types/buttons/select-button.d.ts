import React from 'react';
import type { LINE_FILL, LINE_WEIGHT } from '../atoms';
import { FILL } from '../atoms';
import type { IButtonAction } from '../interfaces';
export interface ISelectButtonBaseProps extends React.AllHTMLAttributes<HTMLButtonElement> {
    actions?: IButtonAction[];
    fill?: keyof typeof FILL;
    inverse?: boolean;
    border?: keyof typeof LINE_FILL | [keyof typeof LINE_FILL, keyof typeof LINE_WEIGHT];
}
export declare const SelectButton: React.ForwardRefExoticComponent<ISelectButtonBaseProps & React.RefAttributes<HTMLButtonElement>>;
