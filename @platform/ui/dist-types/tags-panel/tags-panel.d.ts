import React from 'react';
import type { IOption } from '../interfaces';
export interface ITagsPanelProps extends React.AllHTMLAttributes<HTMLDivElement> {
    tags?: IOption[];
    onRemoveTag?(value: string): void;
    onClickTag?(value: string): void;
}
export interface ITagsPanelState {
    tags: IOption[];
}
export declare class TagsPanel extends React.Component<ITagsPanelProps, ITagsPanelState> {
    static defaultProps: any;
    constructor(props: ITagsPanelProps);
    componentDidMount(): void;
    componentDidUpdate: (prevProps: ITagsPanelProps, _: ITagsPanelState) => void;
    componentWillUnmount(): void;
    private tagsRef;
    private arrRef;
    private getTags;
    private resize;
    private unsubscribeEvents;
    private subscribeEvents;
    render(): JSX.Element;
}
