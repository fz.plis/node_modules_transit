/// <reference types="react" />
export interface ITimerProps {
    /**
     * Время в секундах.
     */
    time: number;
    /**
     * Процент заполнения.
     */
    percent: number;
}
export declare const Timer: ({ time, percent }: ITimerProps) => JSX.Element;
