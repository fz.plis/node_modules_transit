/**
 * Достигнуто максимальное число попыток.
 */
export declare const MAX_NUMBER_OF_ATTEMPS_REACHED = "MAX_NUMBER_OF_ATTEMPS_REACHED";
export declare const polling: <TJobResult, TArgs extends any[]>(job: (...args: TArgs) => Promise<TJobResult>, checker: (result: TJobResult) => boolean, interval: number, maxCount?: number) => (...args: TArgs) => Promise<TJobResult>;
