import type { IMetaData, ICollectionResponse, ISearchByName } from '../interfaces';
export declare const getDictionaryService: <TRow extends {
    id: string;
}, TCreate = TRow>(url: string) => {
    getList: (metaData: IMetaData) => Promise<ICollectionResponse<TRow>>;
    getCounter: ({ category, ...metaData }: IMetaData) => Promise<any>;
    get: (id: string) => Promise<TRow>;
    create: (payload: TCreate) => Promise<any>;
    searchByName: <T extends keyof TRow>(params: ISearchByName, fieldName: T) => Promise<ICollectionResponse<TRow>>;
};
