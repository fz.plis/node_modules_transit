export declare const setToken: (newToken: string) => void;
export declare const getToken: () => string | null;
export declare const clearToken: () => void;
