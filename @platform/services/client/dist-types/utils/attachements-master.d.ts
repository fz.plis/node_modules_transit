import type { IFileObject } from '../interfaces';
export interface IBufferAttachement extends IFileObject {
    accessToken?: string;
    file?: File;
}
declare const unit: import("@platform/tools/istore").Unit<any[], {
    toggleItem: (item: any) => void;
    set: (val: any[]) => void;
    setItem: (index: number, item: any) => void;
    add: (item: any) => void;
    remove: (item: any) => void;
    removeByIndex: (index: number) => void;
    mergeToItem: (index: number, obj: Partial<any>) => void;
}>;
/**
 * Retur.
 */
declare type ListValueUnit = typeof unit;
export interface IAttachementsMasterParams<T> {
    attachmentsMethods: ListValueUnit['methods'];
    bufferMethods: ListValueUnit['methods'];
    getAttachments(): T[];
    getBuffer(): IBufferAttachement[];
    uploadFile(file: File): Promise<T>;
    downloadFile(accessToken: string, fileId: string, fileName: string): Promise<any>;
    transformer(input: T): IBufferAttachement;
    maxSize?: number;
}
export declare const attachementsMaster: <T>(params: IAttachementsMasterParams<T>) => {
    uploadFile: (files: File[]) => void;
    downloadFile: (files: IBufferAttachement[]) => void;
    deleteFile: (file: IBufferAttachement) => void;
    getMaxSize: () => number | undefined;
    getFilesInfo: () => IBufferAttachement[];
};
export {};
