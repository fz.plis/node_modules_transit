/**
 * Возвращает массив всех результатов при сопоставлении строки с регулярным выражением.
 *
 * @param val Строка для поиска.
 * @param regexp Регулярное выражение.
 */
export declare const matchAll: (val: string, regexp: RegExp) => RegExpMatchArray[] | null;
/**
 * Усекает строку, если её длина больше параметра maxlength,
 * усечённая строка будет содержать maxlength-1 первых символов исходной строки плюс "...".
 *
 * @param str - Строка которую необходимо усечь.
 * @param maxlength - Все строки длина которых выше этого параметра, будут усекаться.
 *
 * @example
 * // returns 'abc...'
 * truncate('abcdef', 4);.
 */
export declare const truncateWithEllipsis: (str: string, maxlength: number) => string;
