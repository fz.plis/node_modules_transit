import type { IPollingResponse } from '../interfaces';
export declare const pollingRequest: <T extends IPollingResponse>(fetcher: () => Promise<T>, setter: (val: any) => void) => Promise<void>;
