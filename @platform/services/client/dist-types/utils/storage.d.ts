/**
 * Метод очищения localStorage.
 *
 * @param completelyClear Флаг принудительного очищения всего хранилища.
 */
export declare const clearLocalStorage: (completelyClear?: boolean) => void;
