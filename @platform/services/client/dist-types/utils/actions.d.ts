import type { IActionConfig, IExecuteResult } from '@platform/core';
export declare const chainFatalHandler: (...configs: Array<IActionConfig<any, any>['fatalHandler']>) => (context: any, result: IExecuteResult<any, any>) => void;
export declare const notAvailableInDemo: () => void;
