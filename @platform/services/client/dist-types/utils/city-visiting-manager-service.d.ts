import type { CityVisitingManagerDto, IServerResp } from '../interfaces';
/**
 * Справочник городов выездных менеджеров.
 */
export declare const getCityVisitingManagerService: (url: string) => {
    /**
     * Получить данные из справочника городов выездных менеджеров для продукта ОНБ для переданного подразделения.
     *
     * @description Привилегии: hasAuthority('BRANCH_GET_PAGE').
     */
    findDocPackZone(branchId: string): Promise<IServerResp<CityVisitingManagerDto>>;
};
