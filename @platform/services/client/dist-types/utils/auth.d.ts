import type { ILoginPasswordRequest, ILoginApiRequest, ILoginCertificateRequest, ILoginOtpTotpRequest, IActionWithAuth, IActionWebInfo, IAuthoritiesObject } from '../interfaces';
export declare const createTransformObject: (keyTransformer: (key: string) => string) => <T extends Record<string, any>, TResult extends Record<string, any>>(obj: T) => TResult;
export declare const fromLowerCamelCase: (val: string) => string;
export declare const transformToApi: <T extends Record<string, any>, TResult extends Record<string, any>>(obj: T) => TResult;
export declare const transformFromApi: <T extends Record<string, any>, TResult extends Record<string, any>>(obj: T) => TResult;
export declare const transformError: (err: any) => Promise<never>;
export declare const createAuthRequestFunc: <TRequest>(url: string) => <TData, TResult>(transform: (data: TData) => TRequest) => (data: TData, redirectUri?: string | undefined) => Promise<TResult>;
export declare const transformPassword: ({ password, username, userEnvironment }: ILoginPasswordRequest) => ILoginApiRequest;
export declare const transformCertificate: ({ certData, signData, token, cryptoPlugin, userEnvironment, cryptoPluginVersion, }: ILoginCertificateRequest) => ILoginApiRequest;
export declare const transformOtpTotp: ({ token, code, mfaType, userEnvironment }: ILoginOtpTotpRequest) => ILoginApiRequest;
/**
 * Получить список доступных текущему пользователю действий.
 *
 * @deprecated Используйте const { getAvailableActions } = useAuth() или const { getAvailableActions } = getAuthValue().
 */
export declare const getAvailableActions: (actions: IActionWithAuth[]) => Array<IActionWebInfo<any, any>>;
/**
 * Получить список доступных текущему пользователю элементов.
 *
 * @deprecated Используйте const { getAvailableItems } = useAuth() или const { getAvailableItems } = getAuthValue().
 */
export declare const getAvailableItems: <T>(items: (IAuthoritiesObject & T)[]) => T[];
