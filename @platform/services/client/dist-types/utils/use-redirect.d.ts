import type { History } from 'history';
/**
 * Хук для создания функции редиректа через react-router.
 *
 * @param url - URL, на котоый нужно перевести пользователя.
 * @param state - Дополнительные данные, передаваемые со страницы перехода.
 * @returns Колбэк, при вызове которого произойдёт редирект.
 * @example
 * ```
 * useRedirect('/path/to/scroller', { referer: '/path/to/current/form' })
 * ```
 * Затем на странице скроллера можно получить данные state:
 * ```
 * const { state: { referer } } = useLocation();
 * ```
 */
export declare const useRedirect: (url: string, state?: History.LocationState) => () => void;
