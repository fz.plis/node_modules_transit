import type { IParsedToken } from '../interfaces';
export declare const parseToken: (token: string) => IParsedToken;
export declare const tokenIsExpired: (token: IParsedToken) => boolean;
