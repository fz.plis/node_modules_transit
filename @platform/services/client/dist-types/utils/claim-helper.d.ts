import type { IIndexable } from '@platform/ui';
import type { IAccountClaims, IOpenAccount, IAccount } from '../interfaces';
export declare const getPercentFill: (obj: IIndexable, onlyPositiveBoolean?: boolean) => number;
export declare const getClaimPercentFill: (claim: IAccountClaims) => number;
export declare const getOpenAccountPercentFill: (claim: IOpenAccount) => number;
/**
 * Возвращает относительный url для перехода к заявке, по которой был открыт счет.
 * Может вернуть урл на заявку на резервирование, открытие первого счета или открытие 2+ счета.
 *
 * @param doc ДТО счета.
 * @param isAdmin Признак перехода на страницу "Управление заявкой" для админа.
 * @returns Относительный урл до заявки.
 * @throws Not Found.
 */
export declare const getPageUrlOfRequest: (doc: IAccount, isAdmin?: boolean) => string;
