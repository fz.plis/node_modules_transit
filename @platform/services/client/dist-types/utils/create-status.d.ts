import { DOCUMENT_STATUS_TYPE } from '@platform/ui';
interface ICreateStatusOptions<T> {
    executed: T[];
    inProcess: T[];
    rejected: T[];
}
export declare const createStatus: <T>(config: ICreateStatusOptions<T>) => (status: T) => DOCUMENT_STATUS_TYPE;
export {};
