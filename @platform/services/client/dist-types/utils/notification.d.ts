import type { IRenderNotification, IUserNotification } from '../interfaces';
export declare const preparedNotification: (rawNotifications?: IUserNotification[]) => IRenderNotification;
