import React from 'react';
export interface IErrorHandlerInjectedProps {
    error?: any;
}
export interface IErrorBoundaryInjectedProps {
    take?(error: any): boolean;
    fallback: React.ComponentType<IErrorHandlerInjectedProps>;
}
export declare type IErrorHandlerProps = IErrorBoundaryInjectedProps & IErrorHandlerInjectedProps;
export declare const ErrorHandler: React.FC<IErrorHandlerProps>;
export declare const notFoundErr: (err: any) => boolean;
export declare const PageErrorBoundary: React.FC<IErrorHandlerInjectedProps>;
export declare class FatalErrorBoundary extends React.Component {
    state: {
        hasError: boolean;
    };
    componentDidCatch(): void;
    render(): React.ReactNode;
}
