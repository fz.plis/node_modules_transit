import type { DOCUMENT_TYPE_CODE, IBankClient, IUserCertificate } from '../../interfaces';
import { AUTHORITY_VALIDATORS } from '../../interfaces';
/**
 * Получить сертификаты пользователя по его организации.
 *
 * @param bankClient Организация пользователя.
 */
export declare const getCertificates: (bankClient: IBankClient) => Promise<IUserCertificate[]>;
/**
 * Получить доступные привилегии.
 *
 * @param props Свойства.
 * @param props.bankClient Организация.
 * @param props.certificates Сертификаты организации.
 * @param props.validators Опции проверки полномочий.
 * @param props.documentType Тип документа(проверка прав ЕИО).
 * @param props.productCodes Коды продуктов банка.
 */
export declare const getUserAuthorities: ({ bankClient, certificates, validators, documentType, productCodes, }: {
    bankClient: IBankClient;
    certificates: IUserCertificate[];
    validators?: AUTHORITY_VALIDATORS[] | undefined;
    documentType?: DOCUMENT_TYPE_CODE | undefined;
    productCodes?: string[] | undefined;
}) => Promise<[import("../../interfaces").IAuthorityResponse[] | null, any]>;
/**
 * Проверка на пустоту.
 *
 * @param obj Объект.
 */
export declare const isEmpty: <T extends Record<string, any>>(obj: T | null | undefined) => boolean;
