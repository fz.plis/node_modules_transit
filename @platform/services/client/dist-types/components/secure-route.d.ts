import React from 'react';
import type { RouteProps } from 'react-router';
export interface IFallbackRouteProps extends RouteProps {
    /**
     * Ошибка, возникашая при проверке доступа.
     */
    error?: Error;
}
export declare type FallBackComponentType = React.ComponentType<IFallbackRouteProps>;
export declare type ISecureRouteProps = RouteProps & {
    /**
     * Признак, определяющий должен рисоваться контекст или заглушка.
     */
    isAuthenticated: boolean;
    /**
     * Компонент-заглушка, используемый если роут недоступен.
     */
    fallback: FallBackComponentType;
    /**
     * Ошибка, передаваемая в заглушку.
     */
    authErr?: Error;
};
/**
 * Расширенный `Route`, умеющий выводит заглушку, если не передан признак возможности рендера.
 *
 * Если пропс `isAuthenticated` - false, то рендерит заглушку, в которую передает ошибку из пропса `authErr`.
 * Иначе ведет себя как обычный `Route` из `react-router-dom`.
 */
export declare const SecureRoute: React.FC<ISecureRouteProps>;
