/// <reference types="react" />
export declare const asModal: <TProps>(component: import("react").ComponentType<TProps>) => import("react").ComponentType<import("@platform/tools/istore-react").ILifecicleProps<import("@platform/ui").IModalProps & TProps> & import("@platform/tools/istore-react").WithSelectors<import("@platform/ui").IModalProps & TProps>>;
