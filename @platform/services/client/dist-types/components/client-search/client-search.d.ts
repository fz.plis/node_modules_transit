/// <reference types="react" />
import type { IBankClient } from '@platform/communicator';
import type { IFilters } from '@platform/core';
import type { ITypingInputProps } from '@platform/ui';
import type { IClientSearch, ICollectionResponse } from '../../interfaces';
export interface IBankClientSearch extends ITypingInputProps<string, HTMLElement> {
    searchFn(params: IClientSearch): Promise<ICollectionResponse<IBankClient>>;
    filters?: IFilters;
}
export declare const ClientSearch: ({ searchFn, filters, ...props }: IBankClientSearch) => JSX.Element;
