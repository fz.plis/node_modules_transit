export declare const parseConsentDoc: (documentString: string) => {
    header: string;
    content: string;
};
