export declare const FORM_FIELDS: {
    NEW_STATUS: string;
    COMMENT_FOR_CLIENT: string;
    COMMENT_FOR_BANK: string;
};
export declare const FORM_FIELD_LABELS: Record<string, string>;
