import type { ISetting } from '../interfaces';
export declare const healthCheck: {
    getStream: (streamName: string) => Promise<boolean>;
    refreshStreamHealth: (refreshInterval: number) => void;
};
export declare const setHealthCheck: (method: () => Promise<ISetting>) => () => Promise<ISetting>;
