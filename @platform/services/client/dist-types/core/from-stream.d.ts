import React from 'react';
export interface IFromStreamProps {
    name: string;
    streamInstance?: any;
    loader: React.ReactNode;
    children(stream: any): JSX.Element;
}
export declare const FromStream: React.FC<IFromStreamProps>;
