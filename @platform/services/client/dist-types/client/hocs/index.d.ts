import React from 'react';
import type { IParentCommunicator, IParentCommunicatorHandlers } from '@platform/communicator';
export interface IExternalModuleProps {
    name: string;
    src: string;
    children(children: React.ReactNode, onChangeOrgId: (org: string) => void): React.ReactNode;
}
export interface IExternalModuleState {
    isLoading: boolean;
    showLoaderByDemand: boolean;
    timeout: boolean;
}
export declare class ExternalModule extends React.Component<IExternalModuleProps, IExternalModuleState> {
    constructor(props: IExternalModuleProps);
    state: {
        isLoading: boolean;
        showLoaderByDemand: boolean;
        timeout: boolean;
    };
    componentWillUnmount(): void;
    communicator: IParentCommunicator;
    getHandlers: () => IParentCommunicatorHandlers;
    handleChangeOfOrgId: (orgId: string) => void;
    render(): React.ReactNode;
}
