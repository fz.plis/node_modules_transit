import React from 'react';
export interface ILayoutProps {
    onChangeSelectedOrgId?(val: string): void;
    disableReserveButton?: boolean;
    disableOpenAccountButton?: boolean;
    disableRegDocButton?: boolean;
    disableOfferButton?: boolean;
    hideImportantNotifications?: boolean;
    beforeLogout?(): Promise<boolean>;
    beforeOrgChange?(): Promise<boolean>;
    onClickExit?(): void;
}
export declare const MainLayout: React.FC<ILayoutProps>;
