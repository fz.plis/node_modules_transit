import type { FallBackComponentType } from '../../components';
import type { ErrorGuardHanlder } from './fallbacks';
/**
 * Абстрактная класс для создания на его основе классов ошибок в функциях `guard`.
 *
 * Дополнительно хранит затребованные настройки и фактически полученные параметры
 * для вывода их каким-либо образом, если потребуется.
 */
export declare abstract class RouteGuardError<T> extends Error {
    /**
     * Внутренний код ошибки, должен быть уникален для каждого наследника.
     */
    abstract code: string;
    /**
     * Затребованные `guard'ом` параметры.
     */
    readonly requested: T;
    /**
     * Фактические параметры из токена.
     */
    readonly received: T;
    constructor({ message, requested, received }: {
        message?: string;
        requested: T;
        received: T;
    });
}
/**
 * Ошибка доступа из-за несоответствия сегментов.
 */
export declare class SegmentGuardError extends RouteGuardError<string[]> {
    code: string;
}
/**
 * Ошибка доступа из-за несоответствия контекста юзера.
 */
export declare class Dbo3GuardError extends RouteGuardError<string[]> {
    code: string;
}
export declare const guardHanldler: (checker: (err: Error) => boolean, comp: FallBackComponentType) => ErrorGuardHanlder;
/**
 * Функция, возвращающая type guard для ошибки, унаследованной от RouteGuardError.
 *
 * @param code Внутренний код ошибки.
 */
export declare const creareRouteTypeGuard: <T extends RouteGuardError<unknown>>(code: string) => (err: Error) => err is T;
/**
 * Type guard, проверяющий ошибку на соответствие Dbo3GuardError.
 */
export declare const isDbo3GuardError: (err: Error) => err is Dbo3GuardError;
/**
 * Type guard, проверяющий ошибку на соответствие SegmentGuardError.
 */
export declare const isSegmentGuardError: (err: Error) => err is SegmentGuardError;
