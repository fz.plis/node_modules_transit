import { REQUEST_FUNCTION } from '../../../../constants';
import type { IBankClient } from '../../../../interfaces';
import { USER_CONTEXT_TYPE } from '../../../../interfaces';
import { ERROR_TYPES } from './constants';
/**
 * Проверка доступности роута для пользователя.
 *
 * @param authority Привилегии роута.
 * @param userAuthorities Пользовательские привилегии.
 */
export declare const checkRouteAvailability: (authority: string[] | string, userAuthorities: string[]) => boolean;
/**
 * Получить тип ошибки.
 *
 * @param userAuthorities Привилегии пользователя.
 * @param userOrganizations Организации пользователя.
 * @param contextTypes Контекст пользователя.
 * @param type Тип функции.
 */
export declare const getErrorViewType: (userAuthorities: string[], userOrganizations: IBankClient[], contextTypes: USER_CONTEXT_TYPE[], type?: REQUEST_FUNCTION | undefined) => ERROR_TYPES;
