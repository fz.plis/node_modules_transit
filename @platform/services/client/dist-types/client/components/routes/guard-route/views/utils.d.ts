/// <reference types="react" />
import { ERROR_TYPES } from '../constants';
export declare const getMessage: (type: ERROR_TYPES) => {
    message: string;
    code: string;
    title?: undefined;
} | {
    title: string;
    message: string;
    code: string;
} | {
    code: string;
    message?: undefined;
    title?: undefined;
};
export declare const getActions: (type: ERROR_TYPES, toLogin: () => void, toConstitution: () => void) => JSX.Element | null;
