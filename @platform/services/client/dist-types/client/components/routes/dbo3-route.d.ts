import React from 'react';
import type { RouteProps } from 'react-router-dom';
/**
 * Секьюрный роут, ограничивающий доступ для юзеров ЭКО, и организаций,
 * не имеющим доступ к сегментам переданным в пропс `availableSegments`.
 */
export declare const Dbo3Route: React.FC<RouteProps & {
    availableSegments: string[];
}>;
