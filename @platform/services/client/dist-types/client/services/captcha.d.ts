import type { IGenerateCaptchaResponse } from '../../interfaces';
export declare const captchaService: {
    getCaptchaUrl: (id: string) => string;
    generate: () => Promise<IGenerateCaptchaResponse>;
};
