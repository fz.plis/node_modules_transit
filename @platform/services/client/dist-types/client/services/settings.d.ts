export declare const appSettingsService: {
    getAll: () => Promise<import("..").ISetting[]>;
    getByCode: (code: string) => Promise<import("..").ISetting>;
};
export declare const appPublicSettingsService: {
    getAll: () => Promise<import("..").ISetting[]>;
    getByCode: (code: string) => Promise<import("..").ISetting>;
};
