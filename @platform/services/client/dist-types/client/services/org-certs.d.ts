import type { IValidateResult, ICertValidateResult, IDigest, ICertSignature, IOrgCertFileInfo } from '../../interfaces';
import type { ICert } from '../../interfaces/org-cert';
import type { IOrganizationCertificatesService } from '../../services/helpers/org-certs';
export interface IOrgCertsService extends IOrganizationCertificatesService {
    getAccredited(): Promise<string[]>;
    parse(rawCertificate: string): Promise<IOrgCertFileInfo>;
    uploadFile(attachmentId: string, orgOfficialCertId: string, requestDate: string, signature?: ICertSignature, orgKpp?: string): Promise<ICertValidateResult>;
    validate(attachmentId: string, orgOfficialCertId: string): Promise<IValidateResult>;
    getDigest(certificateValue: string, date: string, orgKpp?: string): Promise<IDigest>;
    getCertificate(id: string): Promise<ICert>;
    downloadCaAccredited(): Promise<{
        fileName: string;
        type: string;
        data: string;
    }>;
}
/**
 * Сертификаты представителя клиента Банка.
 */
export declare const orgCertsService: IOrgCertsService;
