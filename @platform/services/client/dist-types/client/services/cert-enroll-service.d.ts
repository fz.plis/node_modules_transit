import type { IBaseEntity } from '../../interfaces';
/** Сервис по проверке доступности создания заявок НЭП. */
export declare const certEnrollService: {
    /**
     * Проверить возможность создания новой заявки.
     *
     * @param spoId Идентификатор СПО для которого делаем проверку.
     */
    checkCreateNew: (spoId: string) => Promise<boolean>;
    /**
     * Получить последнюю активную заявку.
     *
     * @param spoId Идентификатор СПО в разрезе которого получаем заявки.
     */
    getLastActiveRequest: (spoId: string) => Promise<IBaseEntity | undefined>;
};
