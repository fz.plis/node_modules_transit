import type { ICertificateRequest, ICertificateRequestHistory } from '../../interfaces';
/**
 * Сервис по работе с заявками на регистрацию клиента.
 */
export declare const certificateRequestService: {
    getBySpoId: (spoId: string) => Promise<ICertificateRequest>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICertificateRequest>>;
    get: (id: string) => Promise<ICertificateRequest>;
    getCounter: (metaData: import("../../interfaces").IMetaData) => Promise<any>;
    getHistory: (id: string) => Promise<ICertificateRequestHistory[]>;
};
