import type { CERTIFICATE_TYPE } from '../constants';
import type { IContainerDboBss } from '../crypto/olk/crypto-module-interface';
import type { IClientOfficial, IEskOrganization, IHistoryResponse } from './entities';
export declare enum ORG_CERT_STATUS {
    NEW = "NEW",
    PROCESSING = "PROCESSING",
    ACTIVE = "ACTIVE",
    REJECTED = "REJECTED"
}
export declare enum REMOTE_SOURCE {
    BSS_DBO_320 = "BSS_DBO_320"
}
export interface ICertFilter {
    commonName?: string;
    inn?: string;
    ogrn?: string;
    ogrnip?: string;
}
export interface ICertResponse<T> {
    code: number;
    message: string;
    data: T & {
        errorMessage?: string;
    };
}
export interface IOrgCert {
    certOwnerName: string;
    certificateId: string;
    certificateName: string;
    clientOfficial: IClientOfficial;
    id: string;
    noteForClient: string;
    remoteId: string;
    remoteSource: REMOTE_SOURCE;
    status: ORG_CERT_STATUS;
    footprint: string;
    issuerName: string;
    certRequestId?: string;
}
export interface IOrgCertCheckResult {
    title: string;
    description: string;
    data: any;
    code: string;
}
/** Информация о результате загрузки сертификата. */
export interface ICertValidateResult {
    /** Результаты контролей. */
    checkResults: IOrgCertCheckResult[];
    /** Сообщение об ошибке. */
    errorMessage: string;
    /** ID заявки на регистрацию сертификата. */
    certRequestId: string;
    /** Статус заявки на регистрацию сертификата. */
    certRequestStatus: CERTIFICATES_REQUEST_STATUSES;
    /** Тип сертификата. */
    certType: CERTIFICATE_TYPE;
    /** Признак незавершенной заявки. */
    needProcess: boolean;
}
export interface IOrgCertFileInfo {
    authorityName?: string;
    checkResults?: IOrgCertCheckResult[];
    issuer: string;
    organizationName: string;
    validFrom?: string;
    validTo?: string;
    whomIssued: string;
    body?: string;
    serialNumber?: string;
    thumbprint: string;
    footprint?: string;
    subjectCommonName?: string;
    subjectOrganization?: string;
    subjectOrganizationalUnit?: string;
    subjectLocality?: string;
    subjectCountry?: string;
    subjectEmail?: string;
    subjectGivenName?: string;
    subjectSurname?: string;
    subjectTitle?: string;
    subjectInn?: string;
    subjectOgrn?: string;
}
export interface ICertParseResponse {
    authorityCertificateSerial: string;
    authorityKeyId: string;
    authorityName: string;
    footprint: string;
    keyUsage: boolean[];
    serialNumber: string;
    signTool: string;
    subject: string;
    subjectGivenName: string;
    subjectInn: string;
    subjectOrganization: string;
    subjectSurname: string;
    subjectTitle: string;
    subjectOrganizationalUnit: string;
    subjectLocality: string;
    subjectCountry: string;
    subjectEmail: string;
    validFrom: string;
    validTo: string;
    subjectOgrn: string;
}
/** Информация о результате валидации сертификата. */
export interface IValidateResult {
    /** Результаты контролей. */
    checkResults?: IOrgCertCheckResult[];
    /** Тип сертификата. */
    certType?: CERTIFICATE_TYPE;
    /** Сообщение об ошибке. */
    errorMessage?: string;
}
export interface ICertSignature {
    signedAt: string;
    certificateValue: string;
    signatureValue: string;
}
/**
 * Статусы заявки на регистрацию КЭП.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=359922
 */
export declare enum CERTIFICATES_REQUEST_STATUSES {
    /** Заявка заполнена, контроли пройдены. */
    COMPLETED = "COMPLETED",
    /** Документ подписан и отправлен в Банк пользователем клиента. */
    DELIVERED = "DELIVERED",
    /** Документ поступил в Банк, проверка контролей пройдена успешно. */
    DETAILS_VALID = "DETAILS_VALID",
    /** Документ поступил в Банк, проверка подписи пройдена успешно. */
    SIGN_VALID = "SIGN_VALID",
    /** Документ принят Банком и готов к обработке. */
    RECEIVED = "RECEIVED",
    /** Осуществляется запрос информации о договорах ДБО в АБС Ф1. */
    GET_DBO_CONTRACTS = "GET_DBO_CONTRACTS",
    /** Выполнен запрос информации о договорах ДБО в АБС Ф1. */
    GET_DBO_CONTRACTS_EXECUTED = "GET_DBO_CONTRACTS_EXECUTED",
    /** Ошибка при запросе информации о договорах ДБО в АБС Ф1. */
    GET_DBO_CONTRACTS_ERROR = "GET_DBO_CONTRACTS_ERROR",
    /** Осуществляется автоматическое определение клиента для регистрации сертификата КЭП. */
    IDENTIFY_ORG_AUTO = "IDENTIFY_ORG_AUTO",
    /** Осуществляется ручное определение клиента для регистрации сертификата КЭП. */
    IDENTIFY_ORG_MANUAL = "IDENTIFY_ORG_MANUAL",
    /** Клиент определен. */
    ORG_DEFINED = "ORG_DEFINED",
    /** Осуществляется регистрация сертификата для определенного клиента. */
    REGISTER_CERT = "REGISTER_CERT",
    /** Заявка исполнена. */
    EXECUTED = "EXECUTED",
    /** Заявка отказана Банком. */
    DENIED = "DENIED"
}
export interface ICertificateRequest {
    certData: string;
    certParsedData: IOrgCertFileInfo;
    certType: CERTIFICATE_TYPE;
    createdAt: string;
    fio: string;
    id: string;
    innKio: string;
    kpp: string;
    ogrn: string;
    orgName: string;
    orgOfficialCertId: string;
    requestDate: string;
    requestNumber: string;
    signature: ICertSignature;
    ssdVersion: number;
    userId: string;
    commentForBank?: string;
    /**
     * Комментарий для клиента.
     */
    commentForClient?: string;
    status: CERTIFICATES_REQUEST_STATUSES;
    certificateId?: string;
    containerInfo?: IContainerDboBss;
    signingCertificate?: string;
}
export interface ICertificateRequestHistory extends IHistoryResponse {
    commentForClient: string;
    commentForBank: string;
}
/**
 * Интерфейс ошибки проверки сертификата.
 */
export interface ICertificateCheck {
    /**
     * Код.
     */
    code: string;
    /**
     * Описание.
     */
    description: string;
    /**
     * Наименование ошибки.
     */
    title: string;
}
/**
 * Интерфейс ответа одобрения запроса на заявку.
 */
export interface ICertificateRequestAccept {
    /**
     * Организации найденные на основании ответа ЕСК.
     */
    eskOrganizations: IEskOrganization[];
    /**
     * Результат операции.
     */
    result: boolean;
    /**
     * Результаты контролей регистрации сертификата.
     */
    checkResults: ICertificateCheck[];
}
/**
 * Данные сертификата.
 */
export interface ICert {
    /**
     * Идентификатор в АБС.
     **/
    absId: string;
    /**
     * Издатель.
     **/
    authorityCertificate: string;
    /**
     * Имя и расположение контейнера. Всегда вычисляется на основе signparams.
     **/
    containerNameLocation: string;
    /**
     * Данные сертификата base64.
     **/
    data: string;
    /**
     * Отпечаток.
     **/
    footprint: string;
    /**
     * Идентификатор.
     **/
    id: string;
    /**
     * Идентификатор ключа УЦ.
     **/
    keyId: string;
    /**
     * Наименование организации.
     **/
    organizationName: string;
    /**
     * Источник сертификата.
     **/
    origin: string;
    /**
     * Серийный номер.
     **/
    serialNumber: string;
    /**
     * Параметры подписи.
     **/
    signParams: string;
    /**
     * Идентификатор пользователя в UAA.
     **/
    userId: string;
    /**
     * Инициалы пользователя.
     **/
    userName: string;
    /**
     * Период действия с.
     **/
    validFrom: string;
    /**
     * Период действия по.
     **/
    validTo: string;
    /**
     * Кому выдан.
     **/
    whomIssued: string;
}
/**
 * Интерфейс ответа добавления сертификата пользователя.
 */
export interface IAddCertificateResponse {
    /**
     * Данные.
     */
    data?: IOrgCert;
    /**
     * Ошибка.
     */
    error?: {
        /**
         * Код ошибки.
         */
        code: number;
        /**
         * Сообщения.
         */
        message: string;
        /**
         * Данные об ошибке.
         */
        data: string[];
    };
}
