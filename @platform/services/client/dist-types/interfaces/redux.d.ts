import type { IReduxStore } from './store';
export interface IAction {
    type: string;
    payload: Record<string, unknown> | undefined;
}
export declare type GetStateType = () => IReduxStore;
export declare type ThunkActionType = (dispatch: DispatchType, getState: GetStateType) => any;
export declare type DispatchType = (action: IAction | ThunkActionType) => any;
