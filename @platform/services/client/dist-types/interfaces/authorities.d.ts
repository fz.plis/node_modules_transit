/**
 * Объект содержащий привилегии.
 */
export interface IAuthoritiesObject {
    /**
     * Привилегии.
     */
    authorities: string[];
}
