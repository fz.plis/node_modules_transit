import type { IUserData } from './common';
import type { IBankClient } from './entities';
export interface IStore {
    main: {
        user: {
            /** Организации пользователя. */
            organizations: IBankClient[];
            /** Выбранная в топлайне организация. */
            selectedOrganization?: string;
            /** Токен пользовател под которым осуществлен вход в систему. */
            token: string;
            /** Роли. */
            roles: string[];
            /** Привилегии. */
            authorities: string[];
            userBranches: string[];
            selectedBranchId?: string;
            /** Тип клиента (BANK, CLIENT). */
            type: string;
            /** Персональные данные пользователя. */
            data: IUserData;
        };
    };
}
export interface IReduxStore {
    istore: IStore;
    [storeName: string]: any;
}
