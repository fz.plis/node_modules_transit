import type { IPollingResponse } from './common';
/**
 * Статус выполнения.
 * */
export declare enum USER_NOTIFICATION_EXECUTION_STATUS {
    /**
     * Не выполнено.
     */
    NOT_EXECUTED = "NOT_EXECUTED",
    /**
     * Выполнено.
     */
    EXECUTED = "EXECUTED",
    /**
     * Отменено.
     */
    CANCELED = "CANCELED",
    /**
     * Не выполнено отложено на всегда.
     * */
    POSTPONED = "POSTPONED",
    /**
     * Информационное.
     * */
    INFORMATIONAL = "INFORMATIONAL"
}
export declare enum USER_NOTIFICATION_STATUS {
    NEW = "NEW",
    READ = "READ",
    ARCHIVE = "ARCHIVE"
}
export declare enum USER_NOTIFICATION_IMPORTANCE {
    HIGH = "HIGH",
    NORMAL = "NORMAL"
}
export declare enum USER_NOTIFICATION_POSTPONEMENT_MODE {
    FORBIDDEN = "FORBIDDEN",
    TEMPORARY = "TEMPORARY",
    PERMANENT = "PERMANENT"
}
/**
 * Типы уведомлений.
 * */
export declare enum USER_NOTIFICATION_TYPE {
    /**
     * Уведомление об истечении срока действия сертификата.
     */
    NOTIF_CERT_EXPIRED = "NOTIF_CERT_EXPIRED",
    /**
     * Уведомление об изменений полномочий сотрудника.
     */
    NOTIF_RIGHTS_CHANGE = "NOTIF_RIGHTS_CHANGE",
    /**
     * Уведомление о необходимости привязать сертификат.
     */
    NOTIF_CERT_ADD = "NOTIF_CERT_ADD",
    /**
     * Уведомление о предстоящем аннулировании номера счета.
     */
    NOTIF_ACC_CANCEL_SOON = "NOTIF_ACC_CANCEL_SOON",
    /**
     * Уведомление об аннулировании счета.
     */
    NOTIF_ACC_CANCEL = "NOTIF_ACC_CANCEL"
}
/**
 * Интерфейс типа уведомления.
 */
export interface IUserNotificationType {
    /**
     * Текст кнопки исполнения.
     */
    actionName: string;
    /**
     * Идентификатор.
     */
    code: string;
    /**
     * Важность.
     */
    importance: USER_NOTIFICATION_IMPORTANCE;
    /**
     * Наименование.
     */
    name: string;
    /**
     * Возможность отложить уведомление.
     */
    postponementMode: USER_NOTIFICATION_POSTPONEMENT_MODE;
    /**
     * Текст в панели уведомлений.
     */
    preview: string;
    /**
     * Актуально дней.
     */
    daysActual?: string;
}
/**
 * Интерфейс пользовательских уведомления.
 */
export interface IUserNotification {
    /**
     * Адрес формы исполнения.
     */
    actionUrl: string;
    /**
     * Текст уведомления.
     */
    body: string;
    /**
     * Краткий текст уведомления.
     */
    brief?: string;
    /**
     * Идентификатор подразделения.
     */
    branchId: string;
    /**
     * Дата уведомления.
     */
    createdAt: string;
    /**
     * Дата исполнения.
     */
    executionDate: string;
    /**
     * Выполнить до.
     */
    executionDeadline: string;
    /**
     * Статус выполнения.
     */
    executionStatus: USER_NOTIFICATION_EXECUTION_STATUS;
    /**
     * Идентификатор связанного объекта.
     */
    extId: string;
    /**
     * Идентификатор уведомления.
     */
    id: string;
    /**
     * Отложено до.
     */
    postponedUntil: string;
    /**
     * Дата прочтения.
     */
    readDate: string;
    /**
     * Дата окончания актуальности.
     */
    relevanceEndDate: string;
    /**
     * Сервис-источник уведомления.
     */
    resourceName: string;
    /**
     * Статус прочтения.
     */
    status: USER_NOTIFICATION_STATUS;
    /**
     * Тип уведомления.
     */
    type: IUserNotificationType;
    /**
     * Идентификатор пользователя.
     */
    userId: string;
}
/**
 * Интерфейс количества непрочитанных удедомлений пользователя.
 */
export interface IUserNotificationCount extends IPollingResponse {
    /**
     * Счетчик уведомлений.
     */
    count: number;
    /**
     * Количество уведомлений для показа на главном экране.
     */
    importantCount: number;
}
/**
 * Подготовленное уведомление к показу на главном экране.
 */
export interface INotificationMessage {
    /**
     * Текст сообщения.
     */
    message: string;
    /**
     * Дата сообщения.
     */
    date: string;
    /**
     * Id сообщения.
     */
    notificationId: string;
    /**
     * Короткий текст сообщения.
     */
    brief?: string;
}
/**
 * Объект с подготовленными для дальнейшей обработки сообщениями.
 */
export declare type IPreparedNotification = Record<string, [INotificationMessage]>;
/**
 * Объект с подготовленными к показу сообщениями.
 */
export interface IRenderNotification {
    /**
     * Key - Сообщение.
     */
    message: string;
    /**
     * Id сообщения.
     */
    notificationId: string;
    /**
     * Дата сообщения.
     */
    date: string;
}
/**
 * Объект с данными хранимыми в домене нотификации.
 */
export interface INotificationDomain {
    /**
     * Счетчик нотификаций.
     */
    notificationCount: number;
    /**
     * Количество показываемых нотификаций.
     */
    importantCount: number;
    /**
     * Объект с отображаемой нотификацией.
     */
    importantNotificationMessage?: IRenderNotification;
    /**
     * URL запроса для нотификаций.
     */
    importantNotificationUrl: string;
    /**
     * Флаг разрешающий показ уведомлений.
     */
    showImportantNotification: boolean;
    /**
     * Массив пользовательских уведомлений уведомлений.
     */
    importantNotifications: IUserNotification[];
}
