export { SORT_DIRECTION, ICollectionResponse } from '@platform/core';
export declare type IActionConfig<TContext, TResult> = import('@platform/core').IActionConfig<TContext, TResult>;
export { IValidators, IValidatorParams } from '@platform/core';
