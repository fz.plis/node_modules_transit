import type { ICheckClientMeta } from '../interfaces';
export declare const stopListService: {
    checkClient(data: ICheckClientMeta): Promise<boolean>;
};
