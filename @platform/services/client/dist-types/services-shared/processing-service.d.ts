import type { IAbsMeta, IBalancePositionResponse } from '../interfaces';
export declare const processingService: {
    balancePosition(data: IAbsMeta): Promise<string>;
    checkPosition(id: string): Promise<IBalancePositionResponse>;
};
