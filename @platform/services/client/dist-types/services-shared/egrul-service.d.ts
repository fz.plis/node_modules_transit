import type { IEgrulInfo } from '../interfaces';
import { EGRUL_SEARCH_TYPE } from '../interfaces';
export declare const egrulService: {
    inn: (id: string) => Promise<IEgrulInfo>;
    ogrn: (id: string) => Promise<IEgrulInfo>;
    search: (mode: EGRUL_SEARCH_TYPE, id: string) => Promise<IEgrulInfo>;
};
