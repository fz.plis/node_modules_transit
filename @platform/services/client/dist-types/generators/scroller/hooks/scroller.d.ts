import type { ICollectionResponse } from '@platform/core';
import type { IScroller } from '@platform/ui';
import type { IMetaData } from '../../../interfaces';
interface IConfig<T> {
    fetcher(metaData: IMetaData): Promise<ICollectionResponse<T>>;
    requestParams?: Partial<IMetaData>;
    /**
     * Отключает обработку сетевой ошибки.
     * По умолчанию `false`. При ошибке будет показывать модалку с сообщением ошибки.
     *
     * @default false
     */
    disableErrorHandling?: boolean;
}
interface IUseScroller {
    methods: {
        reload(): Promise<void>;
    };
    props: Pick<IScroller, 'onIntersecting' | 'rows'>;
    /** Состояния скроллера. */
    state: {
        /** Состояние загрузки. */
        isLoading: boolean;
        /** Состояние, проверяющее наличие ошибок. Переходит в true, когда запрос вернул ошибку. */
        hasError: boolean;
        /** Содержит информацию об ошибке. */
        error: any;
    };
}
/**
 * Хук, возвращающий props скроллера.
 */
export declare const useScroller: <T = unknown>({ fetcher, requestParams, disableErrorHandling }: IConfig<T>) => IUseScroller;
export {};
