import React from 'react';
import type { AnyObject } from 'final-form';
import type { IModalProps } from '@platform/ui';
/**
 * Интерфейс компонента фильтры.
 */
export interface IFilterProps extends IModalProps {
    /**
     * Значение фильтра.
     */
    values: AnyObject;
    /**
     * Применить фильтры.
     *
     * @param values Значения фильтра.
     */
    onOk(values: AnyObject): void;
    /**
     * Очистить фильтр.
     */
    onClear(): void;
    /**
     * Функция определяющая доступность кнопки "Применить".
     *
     * @param values Значения фильтра.
     */
    okDisabled?(values: AnyObject): boolean;
    /**
     * Заголовок модального окна фильтра.
     */
    header?: string;
    /**
     * Функция, проверяющая форму на валидность.
     *
     * @param values Значения фильтра.
     */
    validate?(values: AnyObject): Promise<Record<string, unknown>> | Record<string, unknown>;
    /**
     * Поле для передачи стилей модального окна фильтра.
     */
    className?: string;
}
export declare const Filter: React.FC<IFilterProps>;
