/// <reference types="react" />
/**
 * Интерфейс данных скроллера.
 */
interface IScrollerData {
    /**
     * Текущая категория.
     */
    category?: string;
    /**
     * Текущее значение сортировки.
     */
    sort?: string;
    /**
     * Текущее значение фильтров.
     */
    filters?: Record<string, unknown>;
}
/**
 * Контекст текущей информации о скроллере.
 */
export declare const ScrollerDataContext: import("react").Context<IScrollerData>;
/**
 * Хук, который позволяет получить текущую информацию о скроллере.
 */
export declare const useScrollerData: () => IScrollerData;
export {};
