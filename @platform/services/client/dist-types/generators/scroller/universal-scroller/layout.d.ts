import React from 'react';
import type { ICategoryTabsProps, ISortSettingsProps, IActionsBarProps, ICheckboxProps, IBreadcrumb } from '@platform/ui';
import type { ISelectedDocumentsInfo } from '../../../components';
import type { IFilterProps } from '../hooks/filter';
import type { IFilterButtonProps } from './interface';
interface IScrollerPageLayout {
    /** Мэйнлэйаут, в который все будет обернуто, можно передать клиентский или админский. */
    mainLayout: React.ComponentType;
    /** Заголовок страницы. */
    pageTitle: string;
    /** Пропы для категорий. */
    categoryTabsProps?: ICategoryTabsProps;
    /** Пропы для чекбокса для управления выделением всех строк. */
    selectRowsCheckboxProps?: ICheckboxProps;
    /** Пропы для экшнбара. */
    actionsBarProps?: IActionsBarProps;
    /** Пропы для настроек сортировки. */
    sortSettingsProps?: ISortSettingsProps;
    /** Пропы для панели с фильтрами. */
    filtersPanelProps?: IFilterProps;
    /** Пропы для кнопки для открытия панели фильтров. */
    filterNodeProps?: IFilterButtonProps;
    /** Состояние загрузки данных. Если true, то будет показываться Loader. */
    isLoading: boolean;
    /** Информация о выбранных элементах. */
    selectedDocumentsInfo?: ISelectedDocumentsInfo;
    /** Хлебные крошки. */
    breadcrumbs?: IBreadcrumb[];
    /** Состояние заголовка скроллера. Если true, то заголовок будет скрыт. */
    hideHeader?: boolean;
    /** Высота скроллера. Если false, то высота скроллера будет во весь экран, даже если записей меньше. */
    compactHeight?: boolean;
    hasError?: boolean;
}
export declare const ScrollerPageLayout: React.FC<IScrollerPageLayout>;
export {};
