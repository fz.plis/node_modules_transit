import type { IExecuter } from '@platform/core';
import type { IActionBarAction } from '@platform/ui';
import type { IActionWithAuth } from '../../../interfaces';
import type { IGeneratorCategory, IGeneratorFilter } from '../interface';
import type { IActionWithChildren } from './interface';
declare type Parts = 'categories' | 'header' | 'toolbar' | 'topline';
declare type LayoutHeaderGetterConfig = Partial<Record<Parts, boolean>>;
/** Функция, которая высчитывает высоту шапки скроллера, чтобы понять какая высота будет непосредственно у самого скроллера. */
export declare const getLayoutHeaderHeight: (config: LayoutHeaderGetterConfig) => number;
/**
 * Функция, создаст кнопки тулбара из конфига (умеет обрабатывать вложенные экшны с children-ами).
 *
 * @throws Выбрасывает ошибку, если в конфиге встречаются дочерние экшены кроме как у первого элемента.
 * // FIXME: Исправить с помощью Typescript Spread/Rest Types.
 * */
export declare const getTransformedToolbarActions: <TRow, TExecuterContext>({ actions, executer, selectedRows, actionsParams, }: {
    actions: Array<IActionWithAuth | IActionWithChildren>;
    executer: IExecuter<TExecuterContext>;
    selectedRows: TRow[];
    actionsParams: unknown;
}) => IActionBarAction[];
/**
 * Если передан defaultCategory, то он вернет config, заменив defaultCategory и добавит поле `ignoreStorageOnInit: true`,
 * иначе просто вернет config.
 * */
export declare const mergeCategoriesConfig: (config: IGeneratorCategory | undefined, defaultCategory?: string | undefined) => IGeneratorCategory | undefined;
/**
 * Если передан initialFilters, то он вернет config,  заменив значения fields.value значениями из initialFilters
 * и добавит поле `ignoreStorageOnInit: true`, иначе просто вернет config.
 * */
export declare const mergeFiltersConfig: (config?: IGeneratorFilter | undefined, initialFilters?: Record<string, unknown> | undefined) => IGeneratorFilter | undefined;
export {};
