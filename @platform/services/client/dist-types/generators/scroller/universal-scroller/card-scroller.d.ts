import React from 'react';
import type { IRowTemplateProps } from '@platform/ui';
import type { ICommonScrollerGeneratorProps, ScrollerPageProps } from './interface';
import type { IScrollerFeatures } from './scroller-features';
/** Пропы для скроллера с карточками. */
export interface ICardScrollerGeneratorProps<TRow, TExecuterContext> extends ICommonScrollerGeneratorProps, IScrollerFeatures<TRow, TExecuterContext> {
    card: React.ComponentType<Omit<IRowTemplateProps, 'row'> & {
        row: TRow;
    }>;
}
/**
 * Получить скроллер карточек.
 *
 * @param props Свойства.
 * @param props.fetcher Метод получения данных скроллера.
 * @param props.categories Категории.
 * @param props.filters Фильтр.
 * @param props.sorting Сортировка.
 * @param props.selectable Можно выделять строки.
 * @param props.executer Экшн экзекутор, через который будут исполняться действия скроллера и строк.
 * @param props.header Заголовок скроллера.
 * @param props.actions Действия для скроллера в тулбаре (поддерживаются вложенные экшны с полем children).
 * @param props.actionsVisibleCount Количество отображаемых действий.
 * @param props.card Компонент карточки скроллера.
 * @param props.mainLayout Мэйнлэйаут, в который все будет обернуто, можно передать клиентский или админский.
 * @param props.placeholder Элемент, который будет показываться, если в таблице нет записей.
 * @param props.hideHeader Видимость заголовка скроллера.
 * @param props.removeDefaultPadding Флаг, при проставлении которого убирается дефолтный паддниг скроллера.
 * @param props.handleCategoryChange Метод смены категории.
 *
 * @example
  const rules = composeValidation(check('name').on(err.notEmpty()));
  const validateFunc = values => transformFormValidation(values, createValidator(rules, validators).validate)
  export const ScrollerPage = getCardScrollerPage({
    fetcher: periodicMoneyTransferService.getList,
    categories: {
      fetcher: periodicMoneyTransferService.getCounter,
      locale: getTranslator(LOCALIZATION_RESOURCE),
      defaultCategory: CATEGORY.ALL,
    },
    filters: {
      fields: FIELDS,
      labels: FILTER_FIELD_LABELS,
      component: Filter,
      validate: validateFunc,
    },
    sorting: {
      sortFields: SORT_FIELD_LABELS,
    },
    selectable: true,
    header: locale.scroller.bank.title,
    executer: getMoneyTransferExecuter(),
    actions: SCROLLER_ACTIONS,
    card: Card,
    handleCategoryChange: (params) => params.filters.onClear()
  })
 */
export declare const getCardScrollerPage: <TRow, TExecuterContext>({ card, mainLayout, hideHeader, header, categories, filters, placeholder, handleCategoryChange, ...scrollerFeaturesConfig }: ICardScrollerGeneratorProps<TRow, TExecuterContext>) => React.FC<ScrollerPageProps>;
