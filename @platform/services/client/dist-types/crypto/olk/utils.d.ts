/**
 * Получить версию бсс плагина, установленного на компьютере пользователя.
 *
 * @throws Axios error.
 */
export declare const getBssPluginVersion: () => Promise<any>;
/**
 * Получить версию сфт плагина, установленного на компьютере пользователя.
 *
 * @throws Axios error.
 */
export declare const getSftPluginVersion: () => Promise<any>;
