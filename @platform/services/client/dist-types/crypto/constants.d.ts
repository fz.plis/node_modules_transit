export declare const CRYPTO_ERROR_LOCALE: {
    readonly fatalHeader: string;
    readonly fatalText: string;
    readonly internalServerError: string;
    readonly fatalCollapsedHeader: string;
    readonly accessText1: string;
    readonly accessText2: string;
    readonly accessHeader: string;
    readonly moduleNotInstalledHeader: string;
    readonly cryptomodulNotInstalledText1: string;
    readonly cryptomodulNotInstalledText2: string;
    readonly certsNotFound: string;
    readonly install: string;
    readonly errorCode: string;
    readonly reTry: string;
    readonly cancel: string;
    readonly invalidSign: string;
};
