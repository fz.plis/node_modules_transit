import type { Canceler } from 'axios';
import type { IAttachment, ICheckUploadOptions, IDownloadedAttachment } from '../interfaces';
export declare const attachmentService: {
    getStatus: (attachmentId: string, onCancel?: ((cancel: Canceler) => void) | undefined) => any;
    upload: (file: File, options?: ICheckUploadOptions | undefined) => Promise<IAttachment>;
    checkUploadProgress: (attachmentId: string, options?: ICheckUploadOptions) => Promise<IAttachment>;
    download: (attachmentId: string, accessToken: string) => Promise<IDownloadedAttachment>;
    downloadBase64: (attachmentId: string, accessToken?: string | undefined) => Promise<IDownloadedAttachment>;
    downloadOneTimeData: (attachmentId: string, accessToken: string) => Promise<IDownloadedAttachment>;
};
