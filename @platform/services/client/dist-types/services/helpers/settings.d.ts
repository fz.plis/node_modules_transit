import type { AxiosResponse } from 'axios';
import type { IDataResponse, IServerResp, ISetting } from '../../interfaces';
export declare const appSettingsResponse: <T>({ data }: AxiosResponse<IDataResponse<T>>) => T;
export declare const saveAppSettingsResponse: <T>({ data }: AxiosResponse<IServerResp<T, import("../../interfaces").ERROR>>) => T;
export declare const getNewAppSettings: (url: string) => {
    getAll: () => Promise<ISetting[]>;
    getByCode: (code: string) => Promise<ISetting>;
};
