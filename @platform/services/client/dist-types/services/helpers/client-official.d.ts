import type { IClientOfficial } from '../../interfaces';
export declare const createClientOffical: (baseUrl: string) => {
    getUserOfficials: (userId: string) => Promise<IClientOfficial[]>;
};
