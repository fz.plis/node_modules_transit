import type { IBankClient, ISearchByName, IClientSearch } from '../../interfaces';
export declare const createBankClientService: (baseUrl: string) => {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
    get: (id: string) => Promise<IBankClient>;
    searchByName: <T extends keyof IBankClient>(params: ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
    searchByTextQuery: (params: IClientSearch) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
    searchOgrn: (params: ISearchByName) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
    searchRegNumber: (params: ISearchByName) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
};
