import type { ICertificateRequest } from '../../interfaces';
export declare const createCertificateRequestService: (baseUrl: string) => {
    getBySpoId: (spoId: string) => Promise<ICertificateRequest>;
};
