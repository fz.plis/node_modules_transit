import type { ICertificateRequest, ICertificateRequestHistory } from '../interfaces';
export declare const certificateRequestService: {
    get: (id: string) => Promise<ICertificateRequest>;
    getList: (metaData: import("../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICertificateRequest>>;
    getCounter: (metaData: import("../interfaces").IMetaData) => any;
    getBySpoId: (_spoId: string) => Promise<ICertificateRequest>;
    getHistory: (_id: string) => Promise<ICertificateRequestHistory[]>;
};
