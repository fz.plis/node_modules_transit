import type { IBranchV2 } from '../../interfaces';
export declare const branchV2: {
    get: (id: string) => Promise<IBranchV2>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranchV2>>;
    getCounter: (metaData: import("../../interfaces").IMetaData) => any;
    searchByName: <T extends keyof IBranchV2>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranchV2>>;
    localityList: () => Promise<string[]>;
};
