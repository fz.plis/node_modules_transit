import type { IClientOfficial } from '../../interfaces';
export declare const clientOfficial: {
    getUserOfficials: (userId: string) => Promise<IClientOfficial[]>;
};
