import type { IBankClient, ISearchByName, ICollectionResponse } from '../../interfaces';
export declare const bankClient: {
    get: (id: string) => Promise<IBankClient>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<ICollectionResponse<IBankClient>>;
    searchByName: <T extends keyof IBankClient>(params: ISearchByName, fieldName: T) => Promise<ICollectionResponse<IBankClient>>;
    searchOgrn: (params: ISearchByName) => Promise<ICollectionResponse<IBankClient>>;
    searchRegNumber: (params: ISearchByName) => Promise<ICollectionResponse<IBankClient>>;
};
