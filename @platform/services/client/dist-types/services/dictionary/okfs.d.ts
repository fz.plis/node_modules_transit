import type { IOkfs } from '../../interfaces';
export declare const okfs: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkfs>>;
    searchByName: <T extends keyof IOkfs>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkfs>>;
};
