import type { ICountry } from '../../interfaces';
export declare const country: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICountry>>;
    get: (id: string) => Promise<ICountry>;
    searchByName: <T extends keyof ICountry>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICountry>>;
};
