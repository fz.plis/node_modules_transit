import type { IAccount, IAccountV2 } from '../interfaces';
export declare const accountService: {
    get: (id: string) => Promise<IAccount>;
    getList: (metaData: import("../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccount>>;
    getCounter: (metaData: import("../interfaces").IMetaData) => any;
    list: (ids: string[]) => Promise<IAccount[]>;
    listWithBalance: (ids: string[]) => Promise<IAccountV2[]>;
};
