/**
 * Локализованные ошибки проверки подписи.
 */
export declare const VALIDATE_SIGN_ERROR_LABELS: {
    /**
     * Не найдена информация о подписанте или сертификате.
     */
    readonly CANT_RETRIEVE_INFO_ABOUT_SIGNER_OR_CERTIFICATE: string;
};
