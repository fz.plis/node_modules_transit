import { ERROR } from '../interfaces';
/**
 * Используется для реста authorize, уникальный ключ нашего приложения.
 */
export declare const CLIENT_ID: string;
export declare const AUTH_REQUEST_CONFIG: {
    disableTokenHeader: boolean;
    method: "POST";
};
/**
 * Маппинг кодов ошибок авторизации на общие коды.
 */
export declare const AUTH_ERROR_MAP: Record<string, ERROR>;
export declare const SEGMENTS_LABELS: {
    readonly CAS_ACC_OPEN_SECOND_REQ: string;
    readonly CAS_OFFER_RKO_ACCEPTANCE_REQ: string;
    readonly CAS_OFFER_JOIN_GPB_BO1_REQ: string;
    readonly CAS_OFFER_JOIN_GPB_BO2_REQ: string;
    readonly CAS_FREE_FORMAT_DOC: string;
    readonly CAS_DBO_BSS_FRAME: string;
    readonly CAS_SWITCHING_TO_DBO_BSS: string;
    readonly CAS_OFFER_JOIN_PO_UVED: string;
    readonly CAS_SWITCHING_TO_UVED_PROCESS: string;
    readonly CAS_SWITCHING_TO_UVED_VIEW: string;
};
/**
 * Локализованные типы пользователя.
 */
export declare const USER_TYPE_LABELS: {
    readonly BANK: string;
    readonly CLIENT: string;
    readonly TECHNICAL: string;
};
