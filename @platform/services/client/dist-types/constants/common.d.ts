import type { IOption } from '@platform/ui';
export declare const SMS_CODE_LIFETIME = 120;
export declare const DATE_FORMAT = "DD.MM.YYYY";
export declare const DATE_TIME_FORMAT = "DD.MM.YYYY HH:mm:ss";
export declare const DATE_TIME_FORMAT_WITHOUT_SEC = "DD.MM.YYYY HH:mm";
export declare const FILE_EXTENSIONS: {
    HTML: string;
    PDF: string;
    ZIP: string;
    XLS: string;
    CSV: string;
    RTF: string;
};
/**
 * Типы внешних систем.
 */
export declare enum SYSTEM_TYPE {
    /**
     * ЭТП.
     */
    ETP = "ETP_GPB"
}
/**
 * Типы согласий.
 */
export declare enum CONSENT_TYPE {
    /**
     * Согласие о предоставлении персональных данных.
     */
    CONSENT_OF_PERSONAL_DATA = "CONSENT_OF_PERSONAL_DATA"
}
/** Тип сертификата, КЭП/НЭП. */
export declare enum CERTIFICATE_TYPE {
    /** КЭП — Квалифицированная электронная подпись. */
    QUALIFIED_SIGN = "QUALIFIED_SIGN",
    /** НЭП — Неквалифицированная электронная подпись. */
    UNQUALIFIED_SIGN = "UNQUALIFIED_SIGN"
}
/**
 * Продукты ГПБ БО.
 */
export declare enum GPB_PRODUCTS {
    /**
     * Соглашение о присоединении к ГПБ БО.
     */
    CONSTITUTION = "CONSTITUTION",
    /**
     * Соглашение о подключении сервиса внешнеэкономической деятельности.
     */
    LK_UVED = "LK_UVED",
    /**
     * Соглашение о подключении сервиса материального пулинга.
     */
    LK_MP = "LK_MP"
}
/**
 * Наименование продуктов ГПБ.
 */
export declare const GPB_PRODUCTS_LABELS: Record<GPB_PRODUCTS, string>;
/**
 * Список продуктов ГПБ.
 */
export declare const GPB_PRODUCTS_OPTS: IOption[];
/**
 * Локализованные типы пользователя.
 */
export declare const HISTORY_DIALOG_USER_TYPE: {
    BANK: string;
    TECHNICAL: string;
    CLIENT: string;
};
/**
 * Локализованные подсказки для типов пользователя.
 */
export declare const HISTORY_DIALOG_USER_TYPE_TOOLTIPS: {
    readonly BANK: string;
    readonly CLIENT: string;
    readonly TECHNICAL: string;
};
/**
 * Доступные заявки системы.
 */
export declare enum REQUEST_FUNCTION {
    /**
     * [О2+] Клиент: Функция просмотра списка заявок.
     */
    O2_VIEW = "O2_VIEW",
    /**
     * [РКО] Клиент: Функция просмотра списка заявок.
     */
    RKO_VIEW = "RKO_VIEW",
    /**
     * [АО_РКО] Клиент: Функция просмотра списка заявок.
     */
    AO_RKO_VIEW = "AO_RKO_VIEW",
    /**
     * [Конст] Клиент: Функция просмотра списка заявок.
     */
    CONSTITUTION_VIEW = "CONSTITUTION_VIEW",
    /**
     * [Конст_З2] Клиент: Функция просмотра списка заявок.
     */
    CONSTITUTION_32_VIEW = "CONSTITUTION_VIEW",
    /**
     * [АО_УВЭД_З1] Клиент: Функция просмотра списка заявок.
     */
    AO_UVED_1_VIEW = "AO_UVED_1_VIEW",
    /**
     * [АО_УВЭД_З2] Клиент: Функция просмотра списка заявок.
     */
    AO_UVED_2_VIEW = "AO_UVED_2_VIEW",
    /**
     * [АО_МП_З1] Клиент: Функция просмотра списка заявок.
     */
    AO_MP_1_VIEW = "AO_MP_1_VIEW",
    /**
     * [АО_МП_З2] Клиент: Функция просмотра списка заявок.
     */
    AO_MP_2_VIEW = "AO_MP_2_VIEW",
    /**
     * [ДСФ_ЭКО] Клиент: Функция просмотра списка документов в банк.
     */
    DSF_BANK_VIEW = "DSF_BANK_VIEW",
    /**
     * [ДСФ_ЭКО] Клиент: Функция просмотра списка документов из Банка.
     */
    DSF_CLIENT_VIEW = "DSF_CLIENT_VIEW",
    /**
     * [ФНС] Клиент: Функция просмотра списка налоговых задолженностей Клиента.
     */
    FNS_VIEW = "FNS_VIEW",
    /**
     * [Справки] Клиент: Функция просмотра Журнала "Справки".
     */
    REFERENCE_VIEW = "REFERENCE_VIEW"
}
