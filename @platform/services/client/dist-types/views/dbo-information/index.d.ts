import React from 'react';
/**
 * ЭФ "Вывод информационного уведомления при входе в АС Экосистема для пользователей ДБО БСС".
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005123
 */
export declare const DboInformationDialog: React.FC<{
    onClose(): void;
}>;
