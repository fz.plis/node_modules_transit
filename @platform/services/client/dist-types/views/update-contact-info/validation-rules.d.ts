export declare const confirmValidationRules: import("@platform/core").IFiledValidationRules[];
export declare const phoneNumberValidationRules: () => import("@platform/core").IFiledValidationRules[];
export declare const emailValidationRules: import("@platform/core").IFiledValidationRules[];
export declare const phoneValidation: () => import("@platform/core").IValidationMaster;
export declare const emailValidation: () => import("@platform/core").IValidationMaster;
export declare const codeValidation: () => import("@platform/core").IValidationMaster;
