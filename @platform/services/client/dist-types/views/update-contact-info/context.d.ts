import React from 'react';
import { RESULT_TYPE, STEPS } from './constants';
import type { IUpdateContactProviderProps } from './interfaces';
/**
 * Результирующая форма.
 */
interface IResultForm {
    /**
     * Контент формы.
     */
    text: string;
    /**
     * Тип формы.
     */
    type: RESULT_TYPE;
}
/**
 * Свойства контекста обновления данных пользователя.
 */
interface IUpdateContextContext {
    /**
     * Идентификатор подтверждения.
     */
    challengeId: string;
    /**
     * Результирующая форма.
     */
    resultForm: IResultForm;
    /**
     * Значение обратного счета.
     */
    timerValue: number;
    /**
     * Время жизни кода.
     */
    codeLifeTime: number;
    /**
     * Текущий шаг.
     */
    step: STEPS;
    /**
     * Отменить операцию обновления данных.
     */
    cancel(): void;
    /**
     * Переход на следующий шаг.
     */
    next(): void;
    /**
     * Закрытие диалога.
     */
    onClose(): void;
}
export declare const UpdateContactProvider: React.FC<IUpdateContactProviderProps>;
export declare const useUpdateContactContext: () => IUpdateContextContext;
export {};
