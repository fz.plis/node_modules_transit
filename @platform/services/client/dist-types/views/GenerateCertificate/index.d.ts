import React from 'react';
interface IGenerateCertificateProps {
    /**
     * Id Сертификата.
     */
    orgCertId: string;
    /**
     * Метод закрытия окна.
     */
    onClose(): void;
    /**
     * Дополнительный метод для выполнения перед переходом на следующий этап.
     */
    onContinue?(): void;
}
export declare const GenerateCertificate: React.FC<IGenerateCertificateProps>;
export {};
