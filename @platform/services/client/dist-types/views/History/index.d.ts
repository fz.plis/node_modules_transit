import React from 'react';
import type { IHistoryDialogData, IHistoryResponse } from '../../interfaces';
export interface IHistoryRowProps<T extends IHistoryResponse> {
    data: T;
    hasAdmin: boolean;
    onHistoryRowLinkClick?(): void;
}
export interface IHistoryDialog {
    data: IHistoryDialogData<any>;
    num: string;
    date: string;
    clientName: string;
    row: React.ComponentType<IHistoryRowProps<any>>;
    header: React.ReactNode;
    hasAdmin: boolean;
    onHistoryRowLinkClick(userId: string): void;
    onClose(): void;
}
export declare const HistoryDialog: React.FC<IHistoryDialog>;
