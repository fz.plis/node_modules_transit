import React from 'react';
import type { IGPBCryptoModuleIssuer } from '../../crypto';
import type { ISignVerify, IVerifySignError } from '../../interfaces';
import type { IWithDetailProps } from './shared';
export interface IVersifySignResult<T> extends ISignVerify {
    valid: boolean;
    document: T;
    fio?: string;
    issuer?: IGPBCryptoModuleIssuer;
    serialNumber?: string;
    validFrom?: string;
    validTo?: string;
    error?: IVerifySignError;
}
export interface IDocNameRenderProps<T> {
    docNameGetter(doc: T): string;
}
export interface IVerifySignProps<T> extends IDocNameRenderProps<T> {
    data: Array<IVersifySignResult<T>>;
    onClose(): void;
}
export interface IVerifySignSingleProps<T> extends IDocNameRenderProps<T>, IWithDetailProps {
    data: IVersifySignResult<T>;
}
export interface IVerifySignMultiProps<T> extends IDocNameRenderProps<T> {
    data: Array<IVersifySignResult<T>>;
}
export interface IVerifySignSingleState {
    showDetails: boolean;
}
export declare const validLocale: (valid: boolean) => string;
export interface IResultBlockProps<T> extends IDocNameRenderProps<T>, IWithDetailProps {
    valid?: boolean;
    data: Array<IVersifySignResult<T>>;
}
export declare const ResultBlock: any;
export declare const VerifySignSingle: any;
export declare class VerifySignMulti extends React.Component<IVerifySignMultiProps<any>> {
    render(): JSX.Element;
}
export declare class VerifySign<T> extends React.Component<IVerifySignProps<T>> {
    constructor(props: IVerifySignProps<T>);
    private dialogActions;
    renderIcon(): import("@platform/ui").WebSpecialIcon | undefined;
    renderContent(): JSX.Element;
    private handleCancel;
    private isSingle;
    render(): JSX.Element;
}
