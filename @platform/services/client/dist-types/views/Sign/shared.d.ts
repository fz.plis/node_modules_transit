import React from 'react';
import type { ILinkProps } from '@platform/ui';
export interface IWithDetailProps {
    showDetails: boolean;
    onShowDetailsChange(val: boolean): void;
}
export declare type IDetailsLinkProps = IWithDetailProps & Omit<ILinkProps, 'onClick'>;
export interface IDetailRowProps {
    label: string;
    value?: number | string;
    dataField?: string;
}
export declare class DetailsLink extends React.Component<IDetailsLinkProps> {
    private toggle;
    render(): JSX.Element;
}
export declare const SubHeader: React.FC;
export declare const DetailRow: React.FC<IDetailRowProps>;
export declare const DetailMultiRow: React.FC<IDetailRowProps>;
export declare const withDetails: <T extends IWithDetailProps>(showDetails?: boolean) => (WrappedComponent: React.ComponentType<T>) => any;
