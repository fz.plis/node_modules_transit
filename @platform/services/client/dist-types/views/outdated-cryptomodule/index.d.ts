import React from 'react';
/**
 * ЭФ установка ПО для работы с ЭП по результатам успешной авторизации/аутентификации.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005211
 */
export declare const OutdatedCryptomoduleDialog: React.FC<{
    onClose(): void;
}>;
