import React from 'react';
export interface ICollapsedPanelProps extends React.AllHTMLAttributes<HTMLDivElement> {
    header?: React.ReactNode;
    expand?: boolean;
}
export declare const CollapsedPanel: React.FC<ICollapsedPanelProps>;
