import React from 'react';
import type { IUniversalSignDetails, IUniversalSignEntity } from '../../interfaces';
export declare const SignDetailsBlock: React.FC<IUniversalSignDetails>;
export interface ISignDetailsProps {
    documents: IUniversalSignEntity[];
}
export declare const SignDetails: React.FC<ISignDetailsProps>;
