import type { IConfirmSpoForm } from './interface';
export declare const dsfTypeValidation: import("@platform/core").IValidationMaster;
export declare const validateConfirmSpoForm: (values: IConfirmSpoForm) => import("@platform/ui/dist-types/form").IFieldValidationResult;
