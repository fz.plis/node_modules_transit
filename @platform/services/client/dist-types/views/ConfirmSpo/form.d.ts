import React from 'react';
import type { IConfirmProps } from './interface';
/** Форма "Изменение статуса сертификата". */
export declare const ConfirmSpoModal: React.FC<IConfirmProps>;
