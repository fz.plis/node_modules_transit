import React from 'react';
import type { IOption } from '@platform/ui';
import type { IExportActionSettings } from '../../interfaces';
import { FORM_TYPE } from '../../interfaces';
export interface IExportProps {
    numberOfDocuments: number;
    formType: FORM_TYPE;
    options: IOption[];
    defaultValue: string;
    onClose(): void;
    onExportClick(settings: IExportActionSettings): void;
    userlocale: {
        header: string;
        text: string;
    };
}
export declare const ExportDialog: React.FC<IExportProps>;
