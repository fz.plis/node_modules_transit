import type { IActionContext, IActionConfig, ISignService, ICertificateUserContext, IUserDataContext, IUniversalSignResultSummary } from '../interfaces';
export declare const universalSign: IActionConfig<IActionContext<ISignService> & ICertificateUserContext & IUserDataContext, IUniversalSignResultSummary>;
