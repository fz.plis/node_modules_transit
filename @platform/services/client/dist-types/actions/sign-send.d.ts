import type { IActionContext, IActionConfig, ISignService, ICertificateUserContext, IBaseEntity, IUserDataContext } from '../interfaces';
import type { send as sendAction, ISendService } from './send';
export declare type SignAction = IActionConfig<IActionContext<ISignService> & ICertificateUserContext & IUserDataContext, IBaseEntity>;
export interface ISignSendConfig {
    sign: SignAction;
    send: typeof sendAction;
}
export declare type SignSendContext = IActionContext<ISendService & ISignService> & ICertificateUserContext;
export declare const signSend: ({ sign, send }: ISignSendConfig) => IActionConfig<SignSendContext, any>;
