import type { IActionConfig } from '@platform/core';
import type { IBaseContext, IBaseEntity, ICrudService, IActionContext } from '../interfaces';
export declare const create: <TContext extends IBaseContext, TDoc>(baseUrl: string) => IActionConfig<TContext, TDoc>;
export declare const open: <TContext extends IBaseContext, TDoc extends IBaseEntity>(urlGetter: string | ((doc: TDoc) => string)) => IActionConfig<TContext, TDoc>;
export interface IRemoveDialogOptions {
    confirmText?(count: number): string;
    dialogTitle?: string;
}
export declare const remove: <TContext extends IActionContext<ICrudService<IBaseEntity>>, TDoc extends IBaseEntity>({ confirmText, dialogTitle, }?: IRemoveDialogOptions) => IActionConfig<TContext, TDoc>;
