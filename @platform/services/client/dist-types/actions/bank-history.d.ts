import type { IBaseEntity, IActionContext, IHistoryResponse, BankHistoryDialogData, IActionConfig, BankClientName } from '../interfaces';
/**
 * Сервис получения истории изменений.
 */
export interface IHistoryService {
    /**
     * Метод получения записей истории изменений.
     *
     * @param id - Id документа.
     */
    getHistory(id: string): Promise<IHistoryResponse[]>;
}
export interface BankHistoryActionParams<TDoc extends IBaseEntity> {
    /**
     * Функция, возвращающая заголовок модального окна.
     *
     * @param arg - Базовая сущность.
     */
    getHeader?(arg: TDoc): string;
    /**
     * Функция, возвращающая локализованный статус.
     *
     * @param arg Сущность истории.
     */
    getStatusLabel(arg: string): string;
    /**
     * Функция, возвращающая краткое или полное наименование организации.
     *
     * @param arg - Базовая сущность.
     */
    getBankClientName?(arg: TDoc): BankClientName;
    /**
     * Обработчик при нажатии на имя юзера.
     *
     * @param userId Id пользователя.
     */
    onUserNameClick?(userId: string): void;
}
export declare const getBankHistoryAction: <TDoc extends IBaseEntity>({ getHeader, getStatusLabel, getBankClientName, onUserNameClick, }: BankHistoryActionParams<TDoc>) => IActionConfig<IActionContext<IHistoryService>, BankHistoryDialogData>;
