import type { IActionContext, IActionConfig, IBaseSignedDocument, IUniversalSignVerifyActionResult, IBaseSignedDocumentService, AttachmentService } from '../interfaces';
export declare type IUniversalSignVerifyActionContext<TService> = IActionContext<TService> & {
    getDigestVersion?(document: IBaseSignedDocument, signId: string): number;
    downloadBase64: AttachmentService['downloadBase64'];
};
export declare const universalSignVerify: IActionConfig<IUniversalSignVerifyActionContext<IBaseSignedDocumentService>, IUniversalSignVerifyActionResult[]>;
