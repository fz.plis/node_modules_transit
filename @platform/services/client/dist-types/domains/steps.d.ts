export declare const step: <T>(steps: T[], initialStep?: T | undefined) => import("@platform/tools/istore").Unit<T, {
    nextStep: () => T;
    prevStep: () => T;
    isLastStep: () => boolean;
    isFirstStep: () => boolean;
    jump(nextStep: T): void;
    next: () => void;
    prev: () => void;
    getSteps: () => T[];
}>;
