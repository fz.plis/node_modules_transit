import type { IAuthConfiguration } from '../interfaces';
/**
 * Используется для `getNotificationsDomain`.
 *
 * @deprecated Используется только для `istore` и будет удален.
 */
export interface INotificationsUnitSettings {
    autocloseTimeout?: number;
    permanentName?: string;
}
/**
 * Используется для `INotification`.
 *
 * @deprecated Используется только для `istore` и будет удален.
 */
export declare enum NOTIFICATION_TYPE {
    ERROR = "ERROR",
    INFO = "INFO",
    SUCCESS = "SUCCESS",
    WARNING = "WARNING"
}
/**
 * Используется для `getNotificationsDomain`.
 *
 * @deprecated Используется только для `istore` и будет удален.
 */
export interface INotification {
    date: string;
    header: string;
    link?: string;
    linkText?: string;
    message: string;
    onPush?(url: string): void;
    type: NOTIFICATION_TYPE;
}
/**
 * Возвращает домен нотификации.
 *
 * @param params Настройки домена нотификации.
 * @returns Домен нотификации.
 * @deprecated Конструкции с использованием istore не должны использоваться.
 */
export declare const getNotificationsDomain: (params: INotificationsUnitSettings) => import("@platform/tools/istore").Domain<{
    visible: import("@platform/tools/istore").Unit<INotification[], {
        toggleItem: (item: INotification) => void;
        set: (val: INotification[]) => void;
        setItem: (index: number, item: INotification) => void;
        add: (item: INotification | INotification[]) => void;
        remove: (item: INotification | INotification[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<INotification>) => void;
    }>;
    archive: import("@platform/tools/istore").Unit<INotification[], {
        toggleItem: (item: INotification) => void;
        set: (val: INotification[]) => void;
        setItem: (index: number, item: INotification) => void;
        add: (item: INotification | INotification[]) => void;
        remove: (item: INotification | INotification[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<INotification>) => void;
    }>;
}, {
    add: (data: Omit<INotification, 'date'>) => void;
    close: (item: INotification | INotification[]) => void;
}>;
export declare const notifications: import("@platform/tools/istore").Domain<{
    visible: import("@platform/tools/istore").Unit<INotification[], {
        toggleItem: (item: INotification) => void;
        set: (val: INotification[]) => void;
        setItem: (index: number, item: INotification) => void;
        add: (item: INotification | INotification[]) => void;
        remove: (item: INotification | INotification[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<INotification>) => void;
    }>;
    archive: import("@platform/tools/istore").Unit<INotification[], {
        toggleItem: (item: INotification) => void;
        set: (val: INotification[]) => void;
        setItem: (index: number, item: INotification) => void;
        add: (item: INotification | INotification[]) => void;
        remove: (item: INotification | INotification[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<INotification>) => void;
    }>;
}, {
    add: (data: Omit<INotification, 'date'>) => void;
    close: (item: INotification | INotification[]) => void;
}>;
export declare const authConfig: import("@platform/tools/istore").Unit<IAuthConfiguration | undefined, {
    set: (val: IAuthConfiguration | undefined) => void;
}>;
export declare const hasFatalError: import("@platform/tools/istore").Unit<boolean, {
    set: (val: boolean) => void;
}>;
