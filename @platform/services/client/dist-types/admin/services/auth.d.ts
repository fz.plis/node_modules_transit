import type { ILoginPasswordRequest, ILoginResponse, IAuthConfiguration } from '../../interfaces';
export declare const authService: {
    getAuthConfiguration: () => Promise<IAuthConfiguration>;
    login: {
        byPassword: (data: ILoginPasswordRequest, redirectUri?: string | undefined) => Promise<ILoginResponse>;
    };
    logout: () => Promise<any>;
};
