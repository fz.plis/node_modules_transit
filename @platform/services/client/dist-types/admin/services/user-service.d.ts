import type { IUser, IServerResp, IUaaUser, IDbo320Context, IRoleConflictResponse } from '../../interfaces';
export interface IUserServiceData {
    data: string;
}
export declare const userService: {
    block: (userId: string) => Promise<IServerResp<IUserServiceData>>;
    blockList: (ids: string[]) => Promise<IServerResp<IUserServiceData>>;
    unblock: (userId: string) => Promise<IServerResp<IUser>>;
    unblockList: (ids: string[]) => Promise<IServerResp<IUserServiceData>>;
    update: (data: IUaaUser) => Promise<IServerResp<IUaaUser>>;
    dbo320Context: (id: string) => Promise<IServerResp<IDbo320Context>>;
    /**
     * Проверка конфликта ролей.
     *
     * @param roleId ID добавляемой роли.
     * @param roleIds  Список ID ролей пользователя.
     */
    isRoleConflict: (roleId: string, roleIds?: string[] | undefined) => Promise<IRoleConflictResponse>;
};
