import type { IServerResp, IMfaChains, IMfaAvailableChain } from '../../interfaces';
export declare const mfaChainsService: {
    update(data: IMfaChains): Promise<IServerResp<IMfaChains>>;
    available(id: string): Promise<IServerResp<IMfaAvailableChain[]>>;
    default(id: string): Promise<IServerResp<IMfaAvailableChain[]>>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IMfaChains>>;
    getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<IMfaChains>;
    create: (payload: IMfaChains) => Promise<any>;
    searchByName: <T extends keyof IMfaChains>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IMfaChains>>;
};
