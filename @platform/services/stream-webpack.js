const CopyWebpackPlugin = require('copy-webpack-plugin')
const configs = require('@platform/tools/configs')
const externals = require('./externals')
const path = require('path')
const fs = require('fs')

require('custom-env').env(process.env.DEV_ENV, path.resolve(__dirname))

const mode = process.env.MODE

const isSPA = (mode || '').toLowerCase() === 'spa'
const isProduction = configs.getEnv() === 'production'

const rootFolder = isProduction ? './node_modules/@platform/root/dist' : './node_modules/@platform/root/dist-dev'

const getMainConfig = (area, params = {}) => {
  const proxyTarget = area === 'admin' ? process.env.ADMIN_PROXY : process.env.CLIENT_PROXY

  return {
    ...configs.getDefaultConfig({
      sourceMap: false,
    }),
    entry: `${rootFolder}/index.js`,
    devServer: {
      https: true,
      key: fs.readFileSync('./node_modules/@platform/services/cert/localhost.key'),
      cert: fs.readFileSync('./node_modules/@platform/services/cert/localhost.crt'),
      proxy: [
        {
          context: '/gbo-ws/**/websocket',
          target: proxyTarget,
          changeOrigin: true,
          ws: true,
        },
        {
          context: ['/api'],
          target: proxyTarget,
          changeOrigin: true,
        },
      ],
      disableHostCheck: true,
      watchOptions: {
        aggregateTimeout: 500,
        poll: 1000,
      },
      historyApiFallback: true,
      stats: 'errors-only',
      host: '0.0.0.0',
      port: 8000,
    },
    plugins: [
      new CopyWebpackPlugin([
        {
          from: './node_modules/@platform/core/dist',
          to: './streams/_core',
          ignore: ['*.d.ts'],
        },
        {
          from: './node_modules/@platform/ui/dist',
          to: './streams/ui',
          ignore: ['*.d.ts'],
        },
        {
          from: `./node_modules/@platform/services/${area}`,
          to: './streams/_services',
          ignore: ['*.d.ts'],
        },
        {
          from: rootFolder,
          to: './',
        },
        ...(params.extraStreams
          ? params.extraStreams.map(s => ({
              from: `./node_modules/${s.name}`,
              to: './streams/',
              ignore: ['*.d.ts'],
            }))
          : []),
      ]),
    ],
  }
}

const getExtenralStream = name => (_, request, callback) => {
  if (new RegExp(`^${name}/.*`).test(request)) {
    return callback(null, name)
  }
  callback()
}

const getStreamConfig = (
  name, // string
  area, // string
  params = {} // { distFolder: string, entry: string, extraStreams: Array<{ name: string }>, plugins: Array<plugin>}
) => {
  const streamConfig = configs.getDefaultConfig({
    module: name,
    outputFolder: isSPA ? path.resolve(`./dist/streams/${name}`) : path.resolve(`./${params.distFolder || area}/${name}`),
  })

  const entry = params.entry || path.resolve(__dirname, !area ? '../../../src/index.tsx' : `../../../src/${area}.tsx`)
  const plugins = params.plugins || []

  return {
    ...streamConfig,

    // в качестве входной точки используем либо кастомный файл либо если есть area то файл по названию area иначе index.tsx
    entry,
    devtool: isProduction ? undefined : 'cheap-module-eval-source-map',

    plugins: [
      ...streamConfig.plugins,
      ...plugins,
      // добавляем файл для TS который связывает сбилженый index.js файл с сгенерированными файлами d.ts для модуля
      {
        apply: compiler => {
          compiler.hooks.emit.tap('emitPlugin', compilation => {
            compilation.assets[`./index.d.ts`] = {
              size: function () {
                return 0
              },
              source: function () {
                return `export * from './src/${area}'`
              },
            }
          })
        },
      },
    ],
    externals: [
      ...externals,
      // добавляем в игнор все импорты из дополнительных стримов
      ...(params.extraStreams ? params.extraStreams.map(s => getExtenralStream(s.name)) : []),
    ],
  }
}

module.exports = (name, area, params) =>
  isSPA ? [getMainConfig(area, params), getStreamConfig(name, area, params)] : getStreamConfig(name, area, params)
