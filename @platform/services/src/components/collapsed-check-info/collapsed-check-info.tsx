import React, { useState, useCallback, useMemo } from 'react';
import {
  Adjust,
  Box,
  Gap,
  Horizon,
  Line,
  SpecialIcons,
  Typography,
  Link,
  WithInformer,
  formatDate,
  CONTAINER_POSITION,
  CollapsedPanel,
  TOGGLE_POSITION,
} from '@platform/ui';
import { locale } from '../../localization';
import { formatDateTime } from '../../utils';
import css from './style.scss';

/**
 * Свойства компонента валидности подписи.
 */
interface ISignValid {
  /**
   * Флаг валидности подписи.
   */
  valid: boolean;
}

/**
 * Компонент валидности подписи.
 *
 * @param props Свойства компонента.
 * @param props.valid Флаг валидности подписи.
 */
const SignValid: React.FC<ISignValid> = ({ valid }) => {
  const Icon = valid ? SpecialIcons.Success : SpecialIcons.Error;

  return (
    <>
      <Icon scale="SM" />
      <Gap.XS />
      <Typography.SmallText line={'NOWRAP'}>
        {valid ? locale.components.collapsedCheckInfo.signIsValid : locale.components.collapsedCheckInfo.signIsNotValid}
      </Typography.SmallText>
    </>
  );
};

SignValid.displayName = 'SignValid';

/**
 * Свойства компонента информации о сертификате.
 */
interface ICertificateInfo {
  /**
   * Владелец.
   */
  owner: string;
  /**
   * Организация.
   */
  organization: string;
  /**
   * Номер.
   */
  number: string;
  /**
   * Издатель.
   */
  creator: string;
  /**
   * Начало срока действия.
   */
  validFrom: string;
  /**
   * Окончание срока действия.
   */
  validTo: string;
}

/**
 * Компонент информации о сертификате.
 *
 * @param props Свойства компонента.
 * @param props.owner Владелец.
 * @param props.organization Организация.
 * @param props.number Номер.
 * @param props.creator Издатель.
 * @param props.validFrom Начало срока действия.
 * @param props.validTo Окончание срока действия.
 */
const CertificateInfo: React.FC<ICertificateInfo> = ({ owner, organization, number, creator, validFrom, validTo }) => (
  <div className={css.informerContent}>
    <Typography.TextBold>{locale.components.collapsedCheckInfo.certInfo.title}</Typography.TextBold>
    <Typography.TextBold inline>{locale.components.collapsedCheckInfo.certInfo.owner}</Typography.TextBold>
    &nbsp;{owner}
    <br />
    <Typography.TextBold inline>{locale.components.collapsedCheckInfo.certInfo.org}</Typography.TextBold>
    &nbsp;{organization}
    <br />
    <Typography.TextBold inline>{locale.components.collapsedCheckInfo.certInfo.num}</Typography.TextBold>
    &nbsp;{number}
    <br />
    <Typography.TextBold inline>{locale.components.collapsedCheckInfo.certInfo.creator}</Typography.TextBold>
    &nbsp;{creator}
    <br />
    <Typography.TextBold inline>{locale.components.collapsedCheckInfo.certInfo.valid}</Typography.TextBold>
    &nbsp;{`${formatDate(validFrom)} - ${formatDate(validTo)}`}
  </div>
);

CertificateInfo.displayName = 'CertificateInfo';

/**
 * Свойства компонента расширенной информации о документе, подписантах и информации о подписях и сертификатах.
 */
export interface ISignInfo {
  /**
   * ФИО.
   */
  fio: string;
  /**
   * Дата.
   */
  date: string;
  /**
   * Роль.
   */
  role: string;
  /**
   * Флаг валидности.
   */
  valid: boolean;
  /**
   * Информация о сертификате.
   */
  certificate: ICertificateInfo;
  /**
   * Код ошибки.
   */
  errorCode?: string;
  /**
   * Текст ошибки.
   */
  errorText?: string;
}

/**
 * Компонент расширенной информации о документе, подписантах и информации о подписях и сертификатах.
 *
 * @param props Свойства компонента.
 * @param props.fio ФИО.
 * @param props.date Дата.
 * @param props.role Роль.
 * @param props.valid Флаг валидности.
 * @param props.certificate Информация о сертификате.
 * @param props.errorCode Код ошибки.
 * @param props.errorText Текст ошибки.
 */
const SignExtInfo: React.FC<ISignInfo> = ({ fio, date, role, valid, certificate, errorCode = '', errorText }) => {
  const [expandErrorText, setExpandErrorText] = useState(false);
  const handleExpandErrorText = useCallback(() => {
    setExpandErrorText(!expandErrorText);
  }, [expandErrorText, setExpandErrorText]);

  return (
    <>
      <Horizon align="CENTER" className={Adjust.getPadClass(['XS', null])}>
        <Gap />
        <Typography.Text fill="FAINT">{locale.components.collapsedCheckInfo.signExtInfo.signed}</Typography.Text>
        <Gap.X2S />
        <Typography.Text>{fio}</Typography.Text>
        <Gap.X2S />
        <Typography.Text fill="FAINT">{locale.components.collapsedCheckInfo.signExtInfo.date}</Typography.Text>
        <Gap.X2S />
        <Typography.Text>{formatDateTime(date, true)}</Typography.Text>
        <Gap.X2S />
        <Typography.Text fill="FAINT">{locale.components.collapsedCheckInfo.signExtInfo.role}</Typography.Text>
        <Gap.X2S />
        <Typography.Text>{role}</Typography.Text>
        <Gap.X2S />
        <Horizon.Spacer />
        <SignValid valid={valid} />
        <Gap.XS />
        <WithInformer positioningOrder={[CONTAINER_POSITION.BOTTOM_CENTER]} text={<CertificateInfo {...certificate} />}>
          {(ref, onClick) => (
            <Box ref={ref} className={css.certificateInformer}>
              <SpecialIcons.Certificate scale="SM" onClick={onClick} />
            </Box>
          )}
        </WithInformer>
        <Gap />
      </Horizon>
      {!valid && (
        <Horizon className={Adjust.getPadClass(['X2S', null])}>
          <Horizon.Spacer />
          {!expandErrorText && (
            <Link onClick={handleExpandErrorText}>
              <Typography.SmallText fill="ACCENT">
                {`${locale.components.collapsedCheckInfo.signExtInfo.errorCode} ${errorCode}...`}
              </Typography.SmallText>
            </Link>
          )}
          {expandErrorText && (
            <div className={Adjust.getPadClass(['X2S', null])}>
              <Typography.SmallText align="RIGHT">
                {`${locale.components.collapsedCheckInfo.signExtInfo.errorCode} ${errorCode}`}
              </Typography.SmallText>
              <Typography.SmallText align="RIGHT">{errorText}</Typography.SmallText>
            </div>
          )}
          <Gap />
          <Gap />
          <Gap.XS />
        </Horizon>
      )}
    </>
  );
};

SignExtInfo.displayName = 'SignExtInfo';

/**
 * Свойства компонента информации о документе, подписантах и информации о подписях и сертификатах.
 */
export interface ICollapsedCheckInfo {
  /**
   * Заголовок документа.
   */
  documentTitle: string;
  /**
   * Информация о подписантах, подписях и сертификатах.
   */
  signInfos: ISignInfo[];
  /**
   * Признак раскрытия.
   */
  expanded?: boolean;
}

/**
 * Компонент информации о документе, подписантах и информации о подписях и сертификатах.
 *
 * @param props Свойства компонента.
 * @param props.documentTitle Заголовок документа.
 * @param props.signInfos Информация о подписантах, подписях и сертификатах.
 * @param props.expanded Признак раскрытия.
 */
export const CollapsedCheckInfo: React.FC<ICollapsedCheckInfo> = ({ documentTitle, signInfos, expanded = false }) => {
  const valid = useMemo(() => () => !signInfos.some(info => !info.valid), [signInfos]);

  return (
    <Box className={Adjust.getPadClass(['XS', null])} shadow="MD">
      <CollapsedPanel
        arrowScale="SM"
        expanded={expanded}
        header={
          <>
            <Typography.TextBold>{documentTitle}</Typography.TextBold>
            <Horizon.Spacer />
            <SignValid valid={valid()} />
            <Gap />
          </>
        }
        summaryClassName={css.summary}
        togglePosition={TOGGLE_POSITION.LEFT}
      >
        <Line />
        {signInfos.map((signInfo, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <SignExtInfo key={index} {...signInfo} />
        ))}
      </CollapsedPanel>
    </Box>
  );
};

CollapsedCheckInfo.displayName = 'CollapsedCheckInfo';
