import React from 'react';
import type { RouteProps } from 'react-router';
import { Route } from 'react-router-dom';

export interface IFallbackRouteProps extends RouteProps {
  /**
   * Ошибка, возникашая при проверке доступа.
   */
  error?: Error;
}

export type FallBackComponentType = React.ComponentType<IFallbackRouteProps>;

export type ISecureRouteProps = RouteProps & {
  /**
   * Признак, определяющий должен рисоваться контекст или заглушка.
   */
  isAuthenticated: boolean;
  /**
   * Компонент-заглушка, используемый если роут недоступен.
   */
  fallback: FallBackComponentType;
  /**
   * Ошибка, передаваемая в заглушку.
   */
  authErr?: Error;
};

/**
 * Расширенный `Route`, умеющий выводит заглушку, если не передан признак возможности рендера.
 *
 * Если пропс `isAuthenticated` - false, то рендерит заглушку, в которую передает ошибку из пропса `authErr`.
 * Иначе ведет себя как обычный `Route` из `react-router-dom`.
 */
export const SecureRoute: React.FC<ISecureRouteProps> = ({
  authErr,
  isAuthenticated,
  component: Component,
  fallback: Fallback,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => {
      if (!isAuthenticated) {
        return <Fallback {...props} error={authErr} />;
      }

      if (rest.render) {
        return rest.render(props);
      }

      if (Component) {
        return <Component {...props} />;
      }

      return null;
    }}
  />
);

SecureRoute.displayName = 'SecureRoute';
