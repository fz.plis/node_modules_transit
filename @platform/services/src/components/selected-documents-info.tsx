import React from 'react';
import { Typography } from '@platform/ui';
import { locale } from '../localization';

export interface ISelectedDocumentsInfo {
  /** Функция для получения надписи о количестве выбранных элементов. */
  getSelectedElementsInfo?(p: { selected: number; total: number }): string;

  /** Количество выбранных элементов. */
  selected: number;

  /** Всего элементов. */
  total: number;
}

/** Информация о выбранных элементах. */
export const SelectedDocumentsInfo: React.FC<ISelectedDocumentsInfo> = ({
  selected,
  total,
  getSelectedElementsInfo = locale.selectedDocumentsInfo,
}) => <Typography.SmallText fill="FAINT">{getSelectedElementsInfo({ selected, total })}</Typography.SmallText>;

SelectedDocumentsInfo.displayName = 'SelectedDocumentsInfo';
