import React from 'react';
import { ACTIONS, Box, Font, Gap, Link, Modal, PrimaryButton, ServiceIcons, Typography, useToggle, Horizon } from '@platform/ui';
import { locale } from '../../localization';
import type { ICollapsedCheckInfo } from '../collapsed-check-info';
import { CollapsedCheckInfo } from '../collapsed-check-info';
import css from './styles.scss';

/**
 * Свойства элемента списка результата проверки.
 */
export type ISignCheckDialogData = Omit<ICollapsedCheckInfo, 'expanded'>;

/**
 * Свойства диалога просмотра результатов проверки подписи.
 */
export interface ISignCheckDialog {
  /**
   * Функция закрывания диалога.
   */
  onClose(): void;
  /**
   * Список результата проверки.
   */
  data: ISignCheckDialogData[];
  /**
   * Кнопка выгрузки реквизитов ЭП.
   */
  exportSignatureButton?: JSX.Element;
  /**
   * Кнопка сформировать протокол проверки подписей.
   */
  signatureVerificationButton?: JSX.Element;
}

/**
 * Диалог просмотра результатов проверки подписи.
 *
 * @param props Свойства компонента.
 * @param props.data Список результата проверки.
 * @param props.onClose Функция закрывания диалога.
 * @param props.exportSignatureButton Кнопка выгрузки рекизитов ЭП.
 * @param props.signatureVerificationButton Кнопка сформировать протокол проверки подписей.
 */
export const SignCheckDialog: React.FC<ISignCheckDialog> = ({ onClose, data, exportSignatureButton, signatureVerificationButton }) => {
  const [expandedAll, toggleExpandedAll] = useToggle();

  return (
    <Modal className={css.modal}>
      <Box clickable className={css.closeMark} data-action={ACTIONS.CLOSE} onClick={onClose}>
        <ServiceIcons.Close fill={'FAINT'} />
      </Box>
      <Typography.H3>{locale.modals.signCheckDialog.title}</Typography.H3>
      <Gap />
      <Horizon>
        <Font.GetClass volume="MD">
          {fontClass => (
            <Link className={fontClass} underlined="dashed" onClick={toggleExpandedAll}>
              {expandedAll ? locale.modals.signCheckDialog.hideAll : locale.modals.signCheckDialog.showAll}
            </Link>
          )}
        </Font.GetClass>
        <Gap.XL />
        {exportSignatureButton}
        <Horizon.Spacer />
        {signatureVerificationButton}
      </Horizon>
      <Gap.XL />
      <div className={css.content}>
        {data.map((document, index: number) => (
          // eslint-disable-next-line react/no-array-index-key
          <div key={index}>
            <CollapsedCheckInfo {...document} expanded={expandedAll} />
            <Gap />
          </div>
        ))}
      </div>
      <Gap />
      <Horizon>
        <PrimaryButton onClick={onClose}>{locale.action.close}</PrimaryButton>
      </Horizon>
    </Modal>
  );
};
