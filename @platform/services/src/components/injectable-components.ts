import { injectable } from '@platform/tools/istore-react';
import { asModal as asModalHOC } from '@platform/ui';

// Данное решение по оборачиванию hoc asModal является временным, поскольку на данный момент не решен вопрос,
// связанный с проблемами в типизации при использовании withInjectableProps.
// Также решили вынести его отдельно, чтобы каждый раз не приходилось оборачивать компонент при необходимости использования asModal

export const asModal = <TProps>(component: React.ComponentType<TProps>) => injectable({ Comp: asModalHOC(component) }).Comp;
