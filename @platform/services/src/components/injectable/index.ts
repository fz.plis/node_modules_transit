import { injectable } from '@platform/tools/istore-react';
import {
  Input as UiInput,
  PrimaryButton as UiPrimaryButton,
  RegularButton as UiRegularButton,
  AppSidebar as UiAppSidebar,
  AppTopline as UiAppTopline,
  Timer as UiTimer,
  Link as UiLink,
  AppLayout,
} from '@platform/ui';

export const { Sidebar, Topline, CommonLayout, Input, Timer, RegularButton, PrimaryButton, Link } = injectable({
  Input: injectable({
    ...UiInput,
  }),
  Timer: UiTimer,
  PrimaryButton: UiPrimaryButton,
  RegularButton: UiRegularButton,
  Link: UiLink,
  Sidebar: UiAppSidebar,
  Topline: UiAppTopline,
  CommonLayout: AppLayout,
});
