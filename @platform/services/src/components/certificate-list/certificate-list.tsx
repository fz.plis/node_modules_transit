import React from 'react';
import { Box, LayoutScroll, Gap, Horizon, Font, Icons, WithClickable, ServiceIcons } from '@platform/ui';
import type { IUserCertificate } from '../../interfaces';
import css from './certificate-list.scss';

export interface IUserListProps {
  options?: IUserCertificate[];
  selectedOption: IUserCertificate | null;
  onSelectOption?(option: IUserCertificate): void;
}

export const CertificateList: React.FC<IUserListProps> = props => {
  const { options = [], selectedOption, onSelectOption } = props;

  const handleOptionClick = React.useCallback(
    (opt: IUserCertificate) => {
      if (onSelectOption) {
        onSelectOption(opt);
      }
    },
    [onSelectOption]
  );

  const handleKeyDown = React.useCallback(
    (e: React.KeyboardEvent<any>, opt: IUserCertificate): void => {
      if (e.keyCode === 32 && onSelectOption) {
        onSelectOption(opt);
      }
    },
    [onSelectOption]
  );

  return (
    <WithClickable>
      {(ref, { focused, hovered }) => (
        <Box className={css.certificateList}>
          <LayoutScroll autoHide={false} hideTracksWhenNotNeeded={false}>
            {options.map((option, key) => {
              const selected = selectedOption === option;

              return (
                <>
                  <Box
                    key={option.id}
                    ref={ref}
                    clickable
                    border="TRANSPARENT"
                    className={css.box}
                    fill={selected || focused || hovered ? 'ACCENT' : 'FAINT'}
                    tabIndex={1}
                    onClick={() => handleOptionClick(option)}
                    onKeyDown={e => handleKeyDown(e, option)}
                  >
                    <Gap.XS />
                    <Horizon>
                      <Gap.SM />
                      <Icons.User fill={'ACCENT'} />
                      <Gap.XS />
                      <Box className={css.userFio}>
                        <Font line="COLLAPSE" title={option.userName || option.ownerName} volume="SM">
                          {option.userName || option.ownerName}
                        </Font>
                        <Font.Faint line="COLLAPSE" title={option.organizationName} volume="XS">
                          {option.organizationName}
                        </Font.Faint>
                      </Box>
                      <Horizon.Spacer />
                      {option === selectedOption && (
                        <>
                          <Gap.XS />
                          <ServiceIcons.Tick fill={'ACCENT'} />
                          <Gap.SM />
                        </>
                      )}
                    </Horizon>
                    <Gap.XS />
                  </Box>
                  {key < options.length - 1 && <Gap.SM />}
                </>
              );
            })}
          </LayoutScroll>
        </Box>
      )}
    </WithClickable>
  );
};

CertificateList.displayName = 'CertificateList';
