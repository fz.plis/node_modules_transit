import React from 'react';
import { Box, Typography, Gap, Horizon, Adjust } from '@platform/ui';
import { locale } from '../../localization';
import rootCss from '../styles.scss';
import css from './styles.scss';
import { Unavailable } from './unavailable';

export const ServiceUnavailable: React.FC = () => (
  <Adjust className={rootCss.rootErrorContainer}>
    <Box className={css.container}>
      <Horizon>
        <Horizon.Spacer />
        <Box className={css.serviceUnavailable}>
          <Unavailable.Service />
        </Box>
        <Horizon.Spacer />
      </Horizon>
      <Gap.LG />
      <Typography.H2 align="CENTER">{locale.serviceUnavailable.header}</Typography.H2>
      <Gap.LG />
      <Typography.P align="CENTER">{locale.serviceUnavailable.techWork}</Typography.P>
      <Gap.XL />
      <Typography.P align="CENTER">{locale.serviceUnavailable.sorry}</Typography.P>
    </Box>
  </Adjust>
);

ServiceUnavailable.displayName = 'ServiceUnavailable';
