import React, { useState } from 'react';
import type { IBankClient } from '@platform/communicator';
import type { IFilters } from '@platform/core';
import type { ITypingInputProps, IOption, IOptionTemplateProps, IBoxWebStyleProps } from '@platform/ui';
import { Lookup, Option, Box, Font, Horizon, Gap, Toggle, Adjust } from '@platform/ui';
import { conditions } from '../../constants/external';
import type { IClientSearch, ICollectionResponse } from '../../interfaces';
import { BANK_CLIENT_STATUS, BLOCK_STATE } from '../../interfaces';
import { locale } from '../../localization';
import { LabelWithHighlight } from './label-with-highlight';

export interface IBankClientSearch extends ITypingInputProps<string, HTMLElement> {
  searchFn(params: IClientSearch): Promise<ICollectionResponse<IBankClient>>;
  filters?: IFilters;
}

interface IOptionBanlClient extends IOption<IBankClient> {
  search?: string;
  isPotentional?: boolean;
}

const statusColor: Record<string, IBoxWebStyleProps['fill']> = {
  [BANK_CLIENT_STATUS.ACTIVE]: 'SUCCESS',
  [BANK_CLIENT_STATUS.POTENTIAL]: 'WARNING',
};

// кружки для отметки потенциальных и активных клиентов
const styles = {
  height: '10px',
  minHeight: '10px',
  width: '10px',
  minWidth: '10px',
};

const { bankClient } = locale;

const OptionTemplate = React.forwardRef<typeof Option, IOptionTemplateProps<IOptionBanlClient>>(({ option, ...rest }, ref) => {
  const { value, search, isPotentional } = option;
  const { innKio, ogrnOgrip, absId, shortName, fullName } = value;
  const inn = innKio ? (
    <LabelWithHighlight matchFromStartOnly fieldName={bankClient.labels.innKio} label={innKio} searchValue={search} />
  ) : (
    ''
  );
  const ogrn = ogrnOgrip ? (
    <LabelWithHighlight matchFromStartOnly fieldName={bankClient.labels.ogrn} label={ogrnOgrip} searchValue={search} />
  ) : (
    ''
  );
  const abs = absId ? <LabelWithHighlight matchFromStartOnly fieldName={bankClient.labels.abs} label={absId} searchValue={search} /> : '';
  const details = [inn, ogrn, abs].filter(Boolean).map((detail, idx) => (
    // eslint-disable-next-line react/no-array-index-key
    <React.Fragment key={idx}>
      <span>{idx !== 0 && ', '}</span>
      {detail}
    </React.Fragment>
  ));

  /*
   * shortName является не обязательным полем,
   * но показывать при выборе оргнанизации мы должны именно его, либо
   * заменять его на fullName, в случае его отсутствия
   */
  const label = shortName || fullName;

  return (
    <Option
      {...rest}
      ref={ref}
      customLabel={
        <Box>
          <Horizon>
            {isPotentional && (
              <>
                <Box inverse fill={statusColor[value.status]} radius="MAX" style={styles} />
                <Gap.XS />
              </>
            )}
            <Font volume="MD">
              <LabelWithHighlight label={label} searchValue={search} />
            </Font>
          </Horizon>

          <Horizon>
            {isPotentional && <Gap />}
            <Font fill={'FAINT'} volume="XS">
              {details}
            </Font>
          </Horizon>
        </Box>
      }
      option={option}
    />
  );
});

OptionTemplate.displayName = 'OptionTemplate';

interface IBankClientFooter {
  setShowPotetial(show: boolean): void;
  showPotentional?: boolean;
}

const Footer: React.FC<IBankClientFooter> = ({ setShowPotetial, showPotentional }) => (
  <Horizon className={Adjust.getPadClass(['XS', 'SM'])}>
    <Toggle label={bankClient.labels.showPotential} value={showPotentional} onChange={setShowPotetial} />
  </Horizon>
);

export const ClientSearch = ({ searchFn, filters = {}, ...props }: IBankClientSearch) => {
  const [showPotentional, setShowPotetial] = useState(false);

  const bankClientLookup = {
    charCount: 0,
    remoteParams: {
      filters: {
        status: {
          fieldName: 'status',
          value: showPotentional ? '' : BANK_CLIENT_STATUS.ACTIVE,
          condition: conditions.eq,
        },
        blockState: {
          fieldName: 'blockState',
          value: BLOCK_STATE.UNBLOCKED,
          condition: conditions.eq,
        },
        ...filters,
      },
    },
    remoteData: async (meta: IClientSearch) => {
      const resp = await searchFn(meta);

      return {
        ...resp,
        data: resp.data.map((row: IBankClient) => ({
          value: row,
          label: row.shortName || row.fullName,
          isPotentional: showPotentional,
          search: meta.search,
        })),
      };
    },
  };

  return (
    <Lookup.Text
      footer={<Footer setShowPotetial={setShowPotetial} showPotentional={showPotentional} />}
      optionTemplate={OptionTemplate}
      {...bankClientLookup}
      {...props}
    />
  );
};
