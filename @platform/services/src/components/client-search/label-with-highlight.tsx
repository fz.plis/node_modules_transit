import React from 'react';
import { Box, Font } from '@platform/ui';

const getHighlightedPeaceOfLabel = (label: string) => (
  <Box.Accent inverse style={{ display: 'inline' }}>
    <Font inline>{label}</Font>
  </Box.Accent>
);

export interface ILabelWithHighlight {
  label: string;
  searchValue?: string;
  fieldName?: string;
  matchFromStartOnly?: boolean;
}

/**
 * Компонент лейбла элемента в результатах поиска с подсветкой совпадающих символов.
 *
 * @param props Свойства.
 * @param props.label Текущее значение поля.
 * @param props.searchValue Последовательность символов для поиска.
 * @param props.fieldName Название поля.
 * @param props.matchFromStartOnly Флаг проверки вхождения поисковой строки только с начала последовательности.
 */
export const LabelWithHighlight: React.FC<ILabelWithHighlight> = ({ label, searchValue = '', fieldName, matchFromStartOnly }) => {
  const field = fieldName ? `${fieldName}: ` : '';
  const defaultResult = (
    <span>
      {field}
      {label}
    </span>
  );

  if (!searchValue) {
    return defaultResult;
  }

  const result: React.ReactNode[] = [];
  const startIndex = 0;

  const index = label.toLowerCase().indexOf(searchValue.toLowerCase(), startIndex);

  // Для режима проверки "только по вхождению с начала последовательности" не отображаем все остальные найденные варианты
  if (index === -1 || (matchFromStartOnly && index !== 0)) {
    return defaultResult;
  }

  // выделяем первый(требование аналитики) совпавший кусок
  result.push(
    label.substring(startIndex, index),
    getHighlightedPeaceOfLabel(label.substring(index, index + searchValue.length)),
    label.substr(index + searchValue.length)
  );

  return <span>{[field, ...result]}</span>;
};
