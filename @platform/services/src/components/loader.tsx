import React from 'react';
import { appendToPortal, deleteFromPortal, LoaderOverlay, isComponentAddedToPortal } from '@platform/ui';

export const Loader: React.FC = () => <LoaderOverlay opened />;

const LOADER = 'LOADER';

let loaderCount = 0;

const isAdded = isComponentAddedToPortal as (name: string) => boolean;

/**
 * Показать лоадер на странице.
 */
export const showLoader = () => {
  loaderCount += 1;

  if (!isAdded(LOADER)) {
    appendToPortal(LOADER, Loader);
  }
};

/**
 * Скрыть лоадер со страницы.
 *
 * @param force Флаг скрытия лоадера.
 */
export const hideLoader = (force?: boolean) => {
  if (loaderCount > 0) {
    loaderCount -= 1;
  }

  if (loaderCount === 0 || force) {
    deleteFromPortal(LOADER);
  }

  if (force) {
    loaderCount = 0;
  }
};
