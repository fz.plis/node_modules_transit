import React from 'react';
import { DialogTemplate, Box, dialog } from '@platform/ui';
import { locale } from '../../localization';
import { CryptoInstallerContent } from './content';
import type { ICryptoInstallerProps } from './interfaces';
import styles from './styles.scss';

export const CryptoInstaller: React.FC<ICryptoInstallerProps> = ({ onClose, header = locale.cryptoInstaller.title }) => {
  const closeDialogHandler = () => {
    onClose(false);
  };

  return (
    <Box className={styles.container}>
      <DialogTemplate content={<CryptoInstallerContent onClose={onClose} />} header={header} onClose={closeDialogHandler} />
    </Box>
  );
};

CryptoInstaller.displayName = 'CryptoInstaller';

export const openCryptoInstallerDialog = (header: string): Promise<boolean> =>
  new Promise(resolve =>
    dialog.show(
      'cryptoInstaller',
      CryptoInstaller,
      {
        header,
      },
      (isInstalled: boolean) => resolve(isInstalled)
    )
  );
