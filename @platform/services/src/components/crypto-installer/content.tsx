import React, { useState } from 'react';
import { to } from '@platform/core';
import { Adjust, Gap, Horizon, PrimaryButton, RegularButton, SpecialIcons, Typography } from '@platform/ui';
import { getCryptoModule, getCryptoPluginApp } from '../../crypto';
import { locale } from '../../localization';
import type { ICryptoInstallerProps } from './interfaces';
import styles from './styles.scss';

const INTERVAL = 3000;
const AWAIT_TIME_RESPONSE = 5 * 60 * 1000;

export const CryptoInstallerContent: React.FC<ICryptoInstallerProps> = ({ onClose }) => {
  const cryptoModule = getCryptoModule();
  const [disabled, setDisabled] = useState(false);
  const [isCheck, setIsCheck] = useState(false);
  const [isInstall, setIsInstall] = useState(false);

  const cancelHandler = () => {
    onClose(false);
  };

  const continueHandler = async () => {
    setDisabled(true);
    setIsCheck(true);

    const [installed] = await to(cryptoModule.installationCheck());

    setDisabled(false);
    setIsCheck(false);

    if (installed) {
      onClose(true);
    }
  };

  const installHandler = () => {
    window.open(getCryptoPluginApp());
    setDisabled(true);
    setIsInstall(true);

    const jobId = setInterval(async () => {
      const [installed] = await to(cryptoModule.installationCheck());

      if (installed) {
        clearInterval(jobId);
        setDisabled(false);
        setIsInstall(false);
        onClose(true);
      }
    }, INTERVAL);

    setTimeout(() => {
      clearInterval(jobId);
      setDisabled(false);
      setIsInstall(false);
    }, AWAIT_TIME_RESPONSE);
  };

  return (
    <>
      <Horizon align={'TOP'}>
        <SpecialIcons.Warning />
        <Gap />
        <Adjust pad={[undefined, 'XL', undefined, undefined]}>
          <Typography.P>{locale.cryptoInstaller.header}</Typography.P>
          <Gap.XL />
          <Typography.P>{locale.cryptoInstaller.content}</Typography.P>
        </Adjust>
      </Horizon>
      <Gap.XL />
      <Horizon>
        <PrimaryButton className={styles.install} dimension="SM" disabled={disabled} loading={isInstall} onClick={installHandler}>
          {locale.cryptoInstaller.actions.installApp}
        </PrimaryButton>
        <Gap.XS />
        <RegularButton className={styles.continue} dimension="SM" disabled={disabled} loading={isCheck} onClick={continueHandler}>
          {locale.continue}
        </RegularButton>
        <Gap.XS />
        <RegularButton dimension="SM" onClick={cancelHandler}>
          {locale.cryptoInstaller.actions.cancel}
        </RegularButton>
      </Horizon>
    </>
  );
};

CryptoInstallerContent.displayName = 'CryptoInstallerContent';
