import React from 'react';
import { Adjust } from '@platform/ui';
import { ERROR } from '../interfaces';
import { FatalErrorContent } from './fatal-error';
import { NotFoundContent } from './not-found';

export interface IErrorHandlerInjectedProps {
  error?: any;
}

export interface IErrorBoundaryInjectedProps {
  take?(error: any): boolean;
  fallback: React.ComponentType<IErrorHandlerInjectedProps>;
}

export type IErrorHandlerProps = IErrorBoundaryInjectedProps & IErrorHandlerInjectedProps;

export const ErrorHandler: React.FC<IErrorHandlerProps> = ({ error, take = () => true, fallback: Fallback, children }) => {
  if (!!error && take(error)) {
    return <Fallback error={error} />;
  }

  // eslint-disable-next-line react/jsx-no-useless-fragment
  return <>{children}</>;
};

ErrorHandler.displayName = 'ErrorHandler';

export const notFoundErr = (err: any) => {
  if (!err || !err.response) {
    return false;
  }

  return err.response.data.errorInfo.code === ERROR.NOT_FOUND;
};

export const PageErrorBoundary: React.FC<IErrorHandlerInjectedProps> = props => (
  <ErrorHandler
    {...props}
    fallback={() => (
      <Adjust pad={'XL'}>
        <NotFoundContent />
      </Adjust>
    )}
    take={notFoundErr}
  />
);

PageErrorBoundary.displayName = 'PageErrorBoundary';

export class FatalErrorBoundary extends React.Component {
  state = { hasError: false };

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  render() {
    const { children } = this.props;
    const { hasError } = this.state;

    if (hasError) {
      return <FatalErrorContent />;
    }

    return children;
  }
}
