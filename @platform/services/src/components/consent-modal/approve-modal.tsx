import React, { useCallback, useMemo, useState } from 'react';
import {
  Typography,
  Gap,
  DialogTemplate,
  useToggle,
  LayoutScroll,
  Input,
  BUTTON,
  Link,
  dialog,
  Box,
  Horizon,
  ServiceIcons,
} from '@platform/ui';
import type { CONSENT_TYPE } from '../../constants';
import { locale } from '../../localization';
import { ConsentError } from './consent-error';
import styles from './styles.scss';
import { useConsent } from './use-consent';

/**
 * Модальное окно согласия.
 */
interface IApproveModalProps {
  /**
   * Тип согласия.
   */
  type: CONSENT_TYPE;

  /**
   * Метод закрытия диалога.
   *
   * @param value Значение принятия согласия.
   */
  onClose(value?: boolean): void;

  /**
   * Текущее значение принятия согласия.
   */
  value?: boolean;
}

export const ApproveConsentModal: React.FC<IApproveModalProps> = ({ type, value, onClose }) => {
  const [toggleState, setToggle] = useToggle();
  const [isAccept, setIsAccept] = useState(value);
  const { isError, consentHeader, reload, consentContent, consentLoaded } = useConsent(type);

  const handleAccept = useCallback(() => {
    onClose(isAccept);
  }, [isAccept, onClose]);

  const handleClose = useCallback(() => {
    onClose();
  }, [onClose]);

  const actions = useMemo(
    () => [
      {
        name: 'CLOSE',
        label: locale.continue,
        buttonType: BUTTON.PRIMARY,
        onClick: handleAccept,
        disabled: !isAccept,
      },
    ],
    [handleAccept, isAccept]
  );

  const content = useMemo(
    () => (
      <>
        <Typography.H2>{locale.consent.approve.header}</Typography.H2>
        {toggleState && (
          <>
            <Gap.LG />
            <Typography.PBold>{consentHeader}</Typography.PBold>
            <Gap.X2S />
            <LayoutScroll style={{ height: 280, width: 630 }}>
              <div dangerouslySetInnerHTML={{ __html: consentContent }} />
            </LayoutScroll>
          </>
        )}
      </>
    ),
    [consentHeader, toggleState, consentContent]
  );

  const acceptLabel = useMemo(() => {
    if (!toggleState) {
      return locale.consent.labels.iAgree;
    }

    return locale.consent.labels.iAgreeWithPersonalData;
  }, [toggleState]);

  const handleSetAccept = useCallback(
    (accept: boolean) => {
      setIsAccept(accept);
    },
    [setIsAccept]
  );

  const additionalContent = useMemo(
    () => (
      <>
        {toggleState && (
          <>
            <Link onClick={setToggle}>
              <Horizon>
                <Typography.P fill="ACCENT">{locale.consent.toggle.collapse}</Typography.P>
                <Gap.XS />
                <ServiceIcons.ChevronUp fill="ACCENT" />
              </Horizon>
            </Link>
            <Gap.XS />
          </>
        )}
        <Horizon>
          <Input.Checkbox label={acceptLabel} value={isAccept} onChange={handleSetAccept} />
          {!toggleState && (
            <>
              &nbsp;
              <Link className={styles.consentLink} onClick={setToggle}>
                <Typography.P fill="ACCENT">{locale.consent.labels.withPersonalData}</Typography.P>
              </Link>
            </>
          )}
        </Horizon>
      </>
    ),
    [acceptLabel, handleSetAccept, isAccept, setToggle, toggleState]
  );

  if (!consentLoaded) {
    return null;
  }

  if (isError) {
    return <ConsentError onClose={onClose} onReload={reload} />;
  }

  return (
    <Box className={styles.modalContainer}>
      <DialogTemplate actions={actions} additionalContent={additionalContent} content={content} header="" onClose={handleClose} />
    </Box>
  );
};

export const openApproveConsentModal = ({ onClose, ...rest }: IApproveModalProps) => {
  dialog.show('approveConsentModal', ApproveConsentModal, rest, onClose);
};

ApproveConsentModal.displayName = 'ApproveConsentModal';
