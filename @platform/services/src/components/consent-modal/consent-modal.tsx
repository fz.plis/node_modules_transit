import React, { useCallback } from 'react';
import { dialog } from '@platform/ui';
import type { CONSENT_TYPE } from '../../constants';
import { locale } from '../../localization';
import { ConsentDialog } from './consent';
import { ConsentError } from './consent-error';
import { useConsent } from './use-consent';

/**
 * Свойства модального окна согласия.
 */
interface IConsentModalProps {
  /**
   * Лейбл принятия согласия.
   */
  acceptLabel?: string;
  /**
   * Тип согласия.
   */
  type: CONSENT_TYPE;
  /**
   * Закрыть модальное окно.
   */
  onClose(accept?: boolean): void;
  /**
   * Текущее значение принятия согласия.
   */
  value?: boolean;
  /**
   * Флаг только для чтения(дизейблим чекбокс).
   */
  readOnly?: boolean;
}

export const ConsentModal: React.FC<IConsentModalProps> = ({ acceptLabel, type, onClose, readOnly, value = false }) => {
  const { isError, consentHeader, reload, consentContent, consentLoaded } = useConsent(type);

  const handleClose = useCallback(() => {
    onClose();
  }, [onClose]);

  const handleAccept = useCallback(
    isAccept => {
      onClose(isAccept);
    },
    [onClose]
  );

  if (!consentLoaded) {
    return null;
  }

  return isError ? (
    <ConsentError onClose={handleClose} onReload={reload} />
  ) : (
    <ConsentDialog
      acceptLabel={acceptLabel || locale.consent.labels.personalData}
      consentContent={consentContent}
      consentHeader={consentHeader}
      readOnly={readOnly}
      value={value}
      onClose={handleAccept}
    />
  );
};

ConsentModal.displayName = 'ConsentModal';

export const openConsentModal = ({ onClose, ...rest }: IConsentModalProps) => dialog.show('ConsentModal', ConsentModal, rest, onClose);
