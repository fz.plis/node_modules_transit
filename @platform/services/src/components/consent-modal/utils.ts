export const parseConsentDoc = (documentString: string) => {
  let headerText = '';
  const documentElement = document.createElement('div');

  documentElement.innerHTML = documentString;

  const header = documentElement.querySelector('h3') || documentElement.querySelector('.header');

  if (header) {
    headerText = header.innerHTML;

    header.parentNode?.removeChild(header);
  }

  return {
    header: headerText,
    content: documentElement.innerHTML,
  };
};
