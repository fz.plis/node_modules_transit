import React, { useMemo } from 'react';
import { Box, BUTTON, DialogTemplate, SpecialIcons } from '@platform/ui';
import { locale } from '../../localization';
import styles from './styles.scss';

/**
 * Свойства модального окна ошибки загрузки согласия.
 */
interface IConsentErrorProps {
  /**
   * Закрыть окно.
   */
  onClose(): void;

  /**
   * Перезагрузить согласие.
   */
  onReload(): void;
}

export const ConsentError: React.FC<IConsentErrorProps> = ({ onClose, onReload }) => {
  const errorActions = useMemo(
    () => [
      {
        name: 'REPEAT',
        label: locale.action.repeat,
        buttonType: BUTTON.PRIMARY,
        onClick: onReload,
      },
      {
        name: 'CLOSE',
        label: locale.action.close,
        onClick: onClose,
      },
    ],
    [onReload, onClose]
  );

  return (
    <Box className={styles.errorContainer}>
      <DialogTemplate
        actions={errorActions}
        content={locale.consent.labels.documentNotAvailable}
        header={locale.error.text}
        icon={SpecialIcons.Error}
        onClose={onClose}
      />
    </Box>
  );
};

ConsentError.displayName = 'ConsentError';
