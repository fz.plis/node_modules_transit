import React, { useCallback, useMemo, useState } from 'react';
import { Box, BUTTON, DialogTemplate, Input, LayoutScroll } from '@platform/ui';
import { locale } from '../../localization';
import styles from './styles.scss';

/**
 * Диалог согласия.
 */
interface IConsentDialogProps {
  /**
   * Текст согласия.
   */
  acceptLabel: string;

  /**
   * Метод закрытия диалога.
   *
   * @param value Значение принятия согласия.
   */
  onClose(value?: boolean): void;

  /**
   * Контент согалсия.
   */
  consentContent: string;

  /**
   * Заголовок согласия.
   */
  consentHeader: string;

  /**
   * Текущее значение принятия согласия.
   */
  value?: boolean;

  /**
   * Флаг только для чтения(дизейблим чекбокс).
   */
  readOnly?: boolean;
}

export const ConsentDialog: React.FC<IConsentDialogProps> = ({ acceptLabel, consentContent, consentHeader, onClose, value, readOnly }) => {
  const [isAccept, setIsAccept] = useState(value);

  const handleClose = useCallback(() => {
    onClose();
  }, [onClose]);

  const handleAccept = useCallback(() => {
    onClose(isAccept);
  }, [isAccept, onClose]);

  const actions = useMemo(
    () => [
      {
        name: 'CLOSE',
        label: locale.action.close,
        buttonType: BUTTON.PRIMARY,
        onClick: handleAccept,
      },
    ],
    [handleAccept]
  );

  const handleSetAccept = useCallback(
    (accept: boolean) => {
      setIsAccept(accept);
    },
    [setIsAccept]
  );

  return (
    <Box className={styles.modalContainer}>
      <DialogTemplate
        actions={actions}
        additionalContent={<Input.Checkbox disabled={readOnly} label={acceptLabel} value={isAccept} onChange={handleSetAccept} />}
        content={
          <LayoutScroll style={{ height: 400, width: 630 }}>
            <div dangerouslySetInnerHTML={{ __html: consentContent }} />
          </LayoutScroll>
        }
        header={consentHeader}
        onClose={handleClose}
      />
    </Box>
  );
};

ConsentDialog.displayName = 'ConsentDialog';
