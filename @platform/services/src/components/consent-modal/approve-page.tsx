import React, { useCallback, useState } from 'react';
import { Box, Gap, Horizon, Input, Link, PrimaryButton, Typography } from '@platform/ui';
import { CONSENT_TYPE } from '../../constants';
import { locale } from '../../localization';
import { openConsentModal } from './consent-modal';
import styles from './styles.scss';

/**
 * Страница подтверждения согласия.
 */
interface IApproveConsentPageProps {
  /**
   * Принять согласие.
   *
   * @param isAccept Значение согласия.
   */
  onAccept(isAccept?: boolean): void;
  /**
   * Название кнопки.
   *
   * @param buttonLabel Название кнопки отправки согласия.
   * @default locale.continue
   */
  buttonLabel?: string;
  /**
   * Скрывать заголовок компонента.
   *
   * @param hideHeader Скрыть заголовок.
   */
  hideHeader?: boolean;
  /**
   * Текст возле чекбокса.
   *
   * @param acceptText Текст возле чекбокса.
   * @default locale.consent.labels.iAccept
   */
  acceptText?: string;
  /**
   * Текст возле чекбокса, представленный в виде ссылки.
   *
   * @param acceptWithPersonalDataText Текст возле чекбокса, представленный в виде ссылки.
   * @default locale.consent.labels.acceptWithPersonalData
   */
  acceptWithPersonalDataText?: string;
  /**
   * Лейбл принятия согласия в модальном окне.
   *
   * @param acceptLabel Лейбл принятия согласия в модальном окне.
   */
  acceptLabel?: string;
}

export const ApproveConsentPage: React.FC<IApproveConsentPageProps> = ({
  onAccept,
  buttonLabel = locale.continue,
  hideHeader,
  acceptText = locale.consent.labels.iAccept,
  acceptWithPersonalDataText = locale.consent.labels.acceptWithPersonalData,
  acceptLabel,
}) => {
  const [isAccept, setIsAccept] = useState(false);

  const handleSetAccept = useCallback(
    (value: boolean) => {
      setIsAccept(value);
    },
    [setIsAccept]
  );

  const handleAccept = useCallback(() => {
    onAccept(isAccept);
  }, [isAccept, onAccept]);

  const handleOpenConsentDialog = useCallback(() => {
    openConsentModal({
      type: CONSENT_TYPE.CONSENT_OF_PERSONAL_DATA,
      value: isAccept,
      acceptLabel,
      onClose: accept => {
        if (accept !== undefined) {
          setIsAccept(accept);
        }
      },
    });
  }, [isAccept, setIsAccept, acceptLabel]);

  return (
    <Box>
      {!hideHeader && (
        <>
          <Typography.H3>{locale.consent.approve.header}</Typography.H3>
          <Gap.XL />
          <Typography.P>{locale.consent.approve.message}</Typography.P>
          <Gap.LG />
        </>
      )}
      <Horizon>
        <Input.Checkbox label={acceptText} value={isAccept} onChange={handleSetAccept} />
        <Gap.XS />
        <Link className={styles.consentLink} onClick={handleOpenConsentDialog}>
          <Typography.P fill="ACCENT">{acceptWithPersonalDataText}</Typography.P>
        </Link>
      </Horizon>
      <Gap.XL />
      <PrimaryButton disabled={!isAccept} onClick={handleAccept}>
        {buttonLabel}
      </PrimaryButton>
    </Box>
  );
};

ApproveConsentPage.displayName = 'ApproveConsentPage';
