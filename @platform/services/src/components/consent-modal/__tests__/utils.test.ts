import { parseConsentDoc } from '../utils';

describe('utils', () => {
  it('header должен быть Тест, content <div>Content</div>', () => {
    const consentDocHtml = '<h3>Тест</h3><div>Content</div>';

    expect(parseConsentDoc(consentDocHtml)).toStrictEqual({
      header: 'Тест',
      content: '<div>Content</div>',
    });
  });

  it('header должен быть Тест 2, content <div>Content</div>', () => {
    const consentDocHtml = '<div class="header">Тест2</div><div>Content</div>';

    expect(parseConsentDoc(consentDocHtml)).toStrictEqual({
      header: 'Тест2',
      content: '<div>Content</div>',
    });
  });

  it('content и header пустые', () => {
    const consentDocHtml = '';

    expect(parseConsentDoc(consentDocHtml)).toStrictEqual({
      header: '',
      content: '',
    });
  });
});
