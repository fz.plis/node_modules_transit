import { useCallback, useEffect, useState } from 'react';
import { to } from '@platform/core';
import { dictionaryService } from '../../client/services/dictionary';
import type { CONSENT_TYPE } from '../../constants';
import { showLoader, hideLoader } from '../loader';
import { parseConsentDoc } from './utils';

export const useConsent = (type: CONSENT_TYPE) => {
  const [isError, setIsError] = useState(false);
  const [consentLoaded, setConsentLoaded] = useState(false);
  const [consentContent, setConsentContent] = useState('');
  const [consentHeader, setConsentHeader] = useState('');

  const load = useCallback(async () => {
    showLoader();
    setConsentLoaded(false);
    setIsError(false);
    setConsentHeader('');
    setConsentContent('');

    const [response, error] = await to(dictionaryService.consent.getContent(type));

    if (error) {
      setIsError(true);
    } else {
      const { header, content } = parseConsentDoc(response);

      setConsentHeader(header);

      setConsentContent(content);
    }

    setConsentLoaded(true);
    hideLoader();
  }, [type]);

  useEffect(() => {
    void load();
  }, [load, type]);

  return {
    isError,
    consentContent,
    consentHeader,
    consentLoaded,
    reload: load,
  };
};
