import { checkAllSelected, getOptions } from '..';
import type { ITreeOption } from '../../../components';
import { BRANCH_TYPE } from '../../../interfaces';

const list: any[] = [
  {
    id: '1',
    branchType: BRANCH_TYPE.BRANCH,
    filialName: 'f1',
  },
  {
    id: '2',
    parentBranch: '1',
    branchType: BRANCH_TYPE.SUBDIVISION,
    name: 'n2',
    number: 'n2',
  },
  {
    id: '3',
    parentBranch: '1',
    branchType: BRANCH_TYPE.SUBDIVISION,
    name: 'n3',
    number: 'n3',
  },
];

const options: ITreeOption[] = [
  {
    value: '1',
    label: 'f1',
    children: [
      {
        value: '2',
        label: 'n2 n2',
        parent: '1',
      },
      {
        value: '3',
        label: 'n3 n3',
        parent: '1',
      },
    ],
  },
  {
    value: '2',
    label: 'n2 n2',
    parent: '1',
  },
  {
    value: '3',
    label: 'n3 n3',
    parent: '1',
  },
];

describe('branch-multi-select', () => {
  it('getOptions', () => {
    const result = getOptions(list);

    expect(result).toStrictEqual(options);
  });

  it('checkAllSelected should be true', () => {
    expect(checkAllSelected(options, ['1', '2'], 'n2')).toBeTruthy();
  });

  it('checkAllSelected should be false', () => {
    expect(checkAllSelected(options, [], 'n2')).toBeFalsy();
  });
});
