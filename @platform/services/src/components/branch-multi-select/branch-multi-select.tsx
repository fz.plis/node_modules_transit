import React, { useCallback, useEffect, useState } from 'react';
import type { IMultiSelectProps, IOption } from '@platform/ui';
import { asField, createField, MultySelectWithSearch, Option } from '@platform/ui';
import { dictionaryService as adminDictionary } from '../../admin/services/dictionary';
import { dictionaryService as clientDictionary } from '../../client/services/dictionary';
import type { IBranchV2, IFilters } from '../../interfaces';
import { BRANCH_TYPE } from '../../interfaces';

/**
 * Расширенная опция для древовидного отображения.
 */
export interface ITreeOption extends IOption {
  /**
   * Id родителя.
   */
  parent?: string;
  /**
   * Массив дочерних опций.
   */
  children?: ITreeOption[];
  /**
   * Неопределенный значение опции для чекбокса.
   */
  indeterminate?: boolean;
}

type Param = keyof IBranchV2;

const sortByParam = (param: Param) => (a: IBranchV2, b: IBranchV2) => {
  const paramA = (a[param] as string).toLowerCase();
  const paramB = (b[param] as string).toLowerCase();

  if (paramA < paramB) {
    return -1;
  }

  if (paramA > paramB) {
    return 1;
  }

  return 0;
};

export const getOptions = (result: IBranchV2[]) => {
  const branches = result.filter(item => item.branchType === BRANCH_TYPE.BRANCH).sort(sortByParam('filialName'));
  const subdivisions = result.filter(item => item.branchType === BRANCH_TYPE.SUBDIVISION).sort(sortByParam('number'));

  const options = branches.reduce((optionsResult: ITreeOption[], branch) => {
    const children = subdivisions.reduce((childrenResult: ITreeOption[], item) => {
      if (item.parentBranch === branch.id) {
        childrenResult.push({
          value: item.id,
          label: `${item.name} ${item.number}`,
          parent: item.parentBranch,
        });
      }

      return childrenResult;
    }, []);

    const option = {
      value: branch.id,
      label: branch.filialName,
      children,
    };

    return optionsResult.concat([option, ...children]);
  }, []);

  return options;
};

const searchFilterFn = (item: ITreeOption | undefined, searchValue: string) => {
  if (!item) {
    return null;
  }

  return item.label.toLowerCase().includes(searchValue.toLowerCase());
};

export const checkAllSelected = (options: ITreeOption[], values: string[], searchValue: string) => {
  let resultLength = 0;
  const filteredValues = values.reduce<string[]>((acc, val) => {
    const opt = options.find(item => item.value === val);

    if (searchFilterFn(opt, searchValue)) {
      return [...acc, ...(opt!.parent && !acc.includes(opt!.parent) ? [opt!.parent] : []), val];
    }

    return acc;
  }, []);

  options.forEach(item => {
    if (searchFilterFn(item, searchValue)) {
      resultLength += 1;

      if (item?.parent) {
        const parent = options.find(x => item.parent === x.value);

        if (!searchFilterFn(parent, searchValue)) {
          resultLength += 1;
        }
      }
    }
  });

  return resultLength === filteredValues.length;
};

export interface IBranchMultiSelect extends Omit<IMultiSelectProps, 'options'> {
  /**
   * Дополнительные фильтры для запроса списка бранчей.
   */
  filters?: IFilters;
  /** Признак клиентской стороны. */
  isClient?: boolean;
}

export const Component: React.FC<IBranchMultiSelect> = ({ filters = {}, isClient = false, ...props }) => {
  const [options, setOptions] = useState<ITreeOption[]>([]);
  const [search, setSearch] = useState<string>();

  useEffect(() => {
    const service = isClient ? clientDictionary.branchV2 : adminDictionary.branchV2;

    void service
      .getList({
        offset: 0,
        pageSize: 10_000,
        filters: {
          isDeleted: {
            condition: 'eq',
            fieldName: 'isDeleted',
            value: false,
          },
          ...filters,
        },
      })
      .then(result => {
        setOptions(getOptions(result.data));
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const filterFn = useCallback(
    (searchValue: string) => {
      setSearch(searchValue);

      return options.filter(opt => {
        const optResult = searchFilterFn(opt, searchValue);

        if (opt.children) {
          const childrenResult = opt.children.filter(childOpt => searchFilterFn(childOpt, searchValue));

          return childrenResult.length > 0 || optResult;
        }

        return optResult;
      });
    },
    [options]
  );

  const handleChange = useCallback(
    (value: string[], callbackOptions?) => {
      // возможны варианты:
      // 1. нажали на кнопку "Выбрать всё"
      // фильтруем список выбрав только подразделения и отдаем его в onChange
      // 2. нажали на кнопку "Очистить"
      // отдаем value === [] в onChange
      // 3. кликнули на элемент списка
      // 3.1 если он филиал то удаляем его из списка и в зависимости от того есть ли
      // еще выбранные подразделения этого филиала,
      // либо добавляем в список все подразделения (с учетом поиска,
      // пользователь может отфильтровать список
      // и в этом случае выберутся только отфильтрованные),
      // либо удаляем выбранные подразделения из списка
      // 3.2 если он подразделение то просто передаем value в onChange

      let result = value;

      if (value.length === options.length && !callbackOptions) {
        // нажали выбрать все
        // callbackOptions - помогает определить нажата ли кнопка "выбрать все", если только один филиал в списке
        result = options.reduce((res: string[], opt) => {
          if (opt.parent) {
            res.push(opt.value);
          }

          return res;
        }, []);
      } else if (search && checkAllSelected(options, value, search)) {
        const selectedValues = value.reduce<string[]>((acc, val) => {
          const selectedOption = options.find(opt => opt.value === val);

          if (selectedOption?.children?.length) {
            const children = selectedOption.children.map(item => item.value);

            return [...acc, ...children];
          }

          return [...acc, val];
        }, []);

        result = Array.from(new Set(selectedValues));
      } else {
        const selected = value.find(id => !(props as { value: string[] }).value.includes(id));

        if (selected) {
          const selectedOption = options.find(opt => opt.value === selected && !opt.parent);

          if (selectedOption?.children && selectedOption.children.length > 0) {
            // кликнули по филиалу
            // удалим его из массива
            result = result.filter(id => id !== selectedOption.value);

            // ищем выбранные подразделения данного филиала
            const selectedSubdivisions = selectedOption.children.reduce((res: string[], opt) => {
              if (value.includes(opt.value)) {
                res.push(opt.value);
              }

              return res;
            }, []);

            if (selectedSubdivisions.length > 0) {
              // есть выбранные подразделения данного филиала - снимаем с них выделение
              result = result.filter(id => !selectedSubdivisions.includes(id));
            } else {
              // нет выбранных подразделений данного филиала - выделяем все дочерние подразделения
              const subdivisions = selectedOption.children;

              result = [...result, ...subdivisions.map(childOpt => childOpt.value)];
            }
          }
        }
      }

      props.onChange?.(result);
    },
    [options, search, props]
  );

  const getIndeterminate = useCallback(
    (option: ITreeOption) => {
      if (!option.parent) {
        const selectedSubdivisions = option.children?.filter(opt => (props as { value: string[] }).value.includes(opt.value));

        if (selectedSubdivisions?.length && selectedSubdivisions?.length !== option.children?.length) {
          return true;
        }
      }

      return false;
    },
    [props]
  );

  const getSelected = useCallback(
    (selected: boolean, option: ITreeOption) => {
      if (!option.parent) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        const selectedSubdivisions = option.children?.filter(opt => props.value.includes(opt.value));

        if (selectedSubdivisions?.length && selectedSubdivisions?.length === option.children?.length) {
          return true;
        }
      }

      return selected;
    },
    // eslint-disable-next-line react/destructuring-assignment
    [props.value]
  );

  return (
    <MultySelectWithSearch
      withOutSortOptions
      filterFn={filterFn}
      optionTemplate={(optionTemplateProps: any) => (
        <Option
          {...optionTemplateProps}
          indeterminate={getIndeterminate(optionTemplateProps.option)}
          selected={getSelected(optionTemplateProps.selected, optionTemplateProps.option)}
          style={optionTemplateProps.option.parent && { paddingLeft: '24px' }}
        />
      )}
      {...props}
      options={options}
      onChange={handleChange}
    />
  );
};

export const BranchMultiSelect = asField(Component);

export const BranchMultiSelectField = createField(BranchMultiSelect, { multi: true });
