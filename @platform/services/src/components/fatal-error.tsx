import React from 'react';
import { Box, Gap, Horizon, Typography } from '@platform/ui';
import { locale } from '../localization';
import css from './styles.scss';

const errorLocale = locale.errors.fatalHandler;

export const FatalErrorContent: React.FC = () => (
  <Horizon className={css.rootErrorContainer}>
    <Horizon.Spacer />
    <Box className={css.errorContainer}>
      <Horizon>
        <Horizon.Spacer />
        <Box className={css.errorBot} />
        <Horizon.Spacer />
      </Horizon>
      <Gap.LG />
      <Typography.H1 align="CENTER" fill="BASE">
        {errorLocale.header}
      </Typography.H1>
      <Gap.LG />
      <Horizon>
        <Horizon.Spacer />
        <Typography.P align="CENTER" className={css.errorFirstBlock} fill="BASE">
          {errorLocale.firstBlock}
        </Typography.P>
        <Horizon.Spacer />
      </Horizon>
      <Gap.LG />
      <Horizon>
        <Horizon.Spacer />
        <Typography.P align="CENTER" className={css.errorSecondBlock} fill="BASE">
          {errorLocale.secondBlock}
        </Typography.P>
        <Horizon.Spacer />
      </Horizon>
      <Typography.P align="CENTER" fill="ACCENT">
        {errorLocale.phone}
      </Typography.P>
    </Box>
    <Horizon.Spacer />
  </Horizon>
);

FatalErrorContent.displayName = 'FatalErrorContent';
