import React from 'react';
import { Box, Gap, Typography } from '@platform/ui';
import { locale } from '../localization';

export const NotFoundContent: React.FC = () => (
  <Box>
    <Typography.H2>{locale.error.text} 404</Typography.H2>
    <Gap />
    <Typography.P>{locale.page.error404}</Typography.P>
  </Box>
);

NotFoundContent.displayName = 'NotFoundContent';
