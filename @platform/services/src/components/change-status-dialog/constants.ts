import { pathGenerator } from '@platform/core';
import { locale } from '../../localization';
import type { IChangeStatusFormState } from './interfaces';

const path = pathGenerator<IChangeStatusFormState>();

export const FORM_FIELDS = {
  NEW_STATUS: path('newStatus'),
  COMMENT_FOR_CLIENT: path('commentForClient'),
  COMMENT_FOR_BANK: path('commentForBank'),
};

export const FORM_FIELD_LABELS: Record<string, string> = {
  get [FORM_FIELDS.NEW_STATUS]() {
    return locale.changeStatusDialog.newStatusLabel;
  },
  get [FORM_FIELDS.COMMENT_FOR_CLIENT]() {
    return locale.changeStatusDialog.commentForClientLabel;
  },
  get [FORM_FIELDS.COMMENT_FOR_BANK]() {
    return locale.changeStatusDialog.commentForBankLabel;
  },
};
