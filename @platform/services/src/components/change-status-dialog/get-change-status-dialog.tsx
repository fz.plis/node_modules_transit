import React from 'react';
import type { IOptionTemplateProps } from '@platform/ui';
import {
  Form,
  DialogTemplate,
  Gap,
  Label,
  Typography,
  Fields,
  Horizon,
  PrimaryButton,
  RegularButton,
  Status,
  noop,
  Option,
} from '@platform/ui';
import { locale } from '../../localization';
import { FORM_FIELDS, FORM_FIELD_LABELS } from './constants';
import { getValidate } from './get-validate';
import type { IStatusOption, IChangeStatusFormState, ICurrentStatus } from './interfaces';
import css from './styles.scss';

/** Цветовой индикатором и название статуса. */
export const StatusOptionTemplate = React.forwardRef<typeof Option, IOptionTemplateProps<IStatusOption>>(({ option, ...rest }, ref) => (
  <Option
    {...rest}
    ref={ref}
    customLabel={
      <Horizon>
        <Status type={option.indicator} />
        <Typography.P inline>{option.label}</Typography.P>
      </Horizon>
    }
    option={option}
  />
));

StatusOptionTemplate.displayName = 'StatusOptionTemplate';

/** Свойства компонента ChangeStatusDialog. */
export interface IChangeStatusDialogProps {
  /** Текущий статус заявки. */
  currentStatus: ICurrentStatus;
  /** Определяет надо ли отображать поле ввода комментария для банка. */
  hiddenBankComment?: boolean;
  /** Колбек закрытия окна. */
  onClose(): void;
  /** Колбек сабмита формы. */
  onSelectStatus(result: IChangeStatusFormState): void;
}

/**
 * Возвращает компонент диалог смены статуса заявки.
 *
 * @param availableStatuses - Статусы на которые можно сменить текущий.
 */
export const getChangeStatusDialog = (availableStatuses: IStatusOption[]): React.FC<IChangeStatusDialogProps> => {
  const validate = getValidate(availableStatuses);

  /**
   * Диалог смены статуса заявки.
   *
   * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=328142
   * */
  const ChangeStatusDialog: React.FC<IChangeStatusDialogProps> = ({ currentStatus, onClose, onSelectStatus, hiddenBankComment }) => {
    const onSubmit = (values: IChangeStatusFormState) => {
      onSelectStatus({
        newStatus: values.newStatus,
        commentForBank: values.commentForBank,
        commentForClient: values.commentForClient,
      });
      onClose();
    };

    return (
      <DialogTemplate
        content={
          <Form<IChangeStatusFormState>
            render={({ handleSubmit }) => (
              <form className={css.form} onSubmit={handleSubmit}>
                <Gap.XS />
                <Horizon>
                  <Typography.P inline fill="FAINT">
                    {locale.changeStatusDialog.currentStatusLabel}
                  </Typography.P>
                  <Gap.XS inline />
                  <Status type={currentStatus.indicator} />
                  <Typography.P inline>{currentStatus.label}</Typography.P>
                </Horizon>
                <Gap.LG />
                <Label text={FORM_FIELD_LABELS[FORM_FIELDS.NEW_STATUS]}>
                  <Fields.Select
                    name={FORM_FIELDS.NEW_STATUS}
                    optionTemplate={StatusOptionTemplate}
                    options={availableStatuses}
                    placeholder={locale.changeStatusDialog.selectPlaceholder}
                  />
                </Label>
                <Gap.LG />
                <Label text={FORM_FIELD_LABELS[FORM_FIELDS.COMMENT_FOR_CLIENT]}>
                  <Fields.TextArea name={FORM_FIELDS.COMMENT_FOR_CLIENT} />
                </Label>
                <Gap.LG />
                {!hiddenBankComment && (
                  <>
                    <Label text={FORM_FIELD_LABELS[FORM_FIELDS.COMMENT_FOR_BANK]}>
                      <Fields.TextArea name={FORM_FIELDS.COMMENT_FOR_BANK} />
                    </Label>
                    <Gap.LG />
                  </>
                )}
                <Horizon>
                  <PrimaryButton dimension="SM" type="submit" onClick={noop}>
                    {locale.changeStatusDialog.buttons.set}
                  </PrimaryButton>
                  <Gap.SM />
                  <RegularButton dimension="SM" onClick={onClose}>
                    {locale.changeStatusDialog.buttons.cancel}
                  </RegularButton>
                </Horizon>
              </form>
            )}
            validate={validate}
            onSubmit={onSubmit}
          />
        }
        header={locale.changeStatusDialog.header}
        onClose={onClose}
      />
    );
  };

  ChangeStatusDialog.displayName = 'ChangeStatusDialog';

  return ChangeStatusDialog;
};
