import { composeValidation, check, createValidator } from '@platform/core';
import { transformFormValidation } from '@platform/ui';
import { locale } from '../../localization';
import { err, validators, when } from '../../utils';
import { FORM_FIELDS, FORM_FIELD_LABELS } from './constants';
import type { IStatusOption, IChangeStatusFormState } from './interfaces';

/**
 * Возвращает валидатор для формы смены статуса заявки.
 *
 * @param availableStatuses - Статусы на которые можно сменить текущий.
 */
export const getValidate = (availableStatuses: IStatusOption[]) => {
  // Для смены на некоторые статусы обязателен комментарий для клиента.
  const statusesWithRequireCommentForClient = availableStatuses
    .filter(status => status.requireCommentForClient)
    .map(status => status.value);

  const rules = composeValidation(
    check(FORM_FIELDS.NEW_STATUS).on(
      err.notEmpty(locale.changeStatusDialog.emptyField({ fieldName: FORM_FIELD_LABELS[FORM_FIELDS.NEW_STATUS] }))
    ),
    check(FORM_FIELDS.COMMENT_FOR_CLIENT)
      .on(err.notEmpty(locale.changeStatusDialog.emptyField({ fieldName: FORM_FIELD_LABELS[FORM_FIELDS.COMMENT_FOR_CLIENT] })))
      .but(when(FORM_FIELDS.NEW_STATUS).inArray(statusesWithRequireCommentForClient))
  );

  const validator = createValidator(rules, validators);

  return (values: IChangeStatusFormState) => transformFormValidation(values, validator.validate);
};
