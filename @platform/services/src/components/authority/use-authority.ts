import { useCallback, useEffect, useState } from 'react';
import { AUTHORITY_VALIDATORS } from '../../interfaces';
import { locale } from '../../localization';
import type { IAuthorityProps, IErrorResult } from './interfaces';
import { getCertificates, getUserAuthorities, isEmpty } from './utils';

/**
 * Хук привилегий.
 *
 * @param props Свойства.
 * @param props.bankClient Организация пользователя.
 * @param props.documentType Тип документа.
 * @param props.productCodes Коды продуктов банка.
 * @param props.validators Опции проверки полномочий.
 * @param props.certificateErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для сертификата.
 * @param props.constitutionErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для ЕИО.
 * @param props.eioErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для конституцию.
 * @param props.noCertificateErrorMessage Сообщение по-умолчанию об отсутствии сертификата.
 * @class
 */
export const useAuthority = ({
  bankClient,
  documentType,
  productCodes,
  validators,
  certificateErrorDefaultMessage = '',
  constitutionErrorDefaultMessage = '',
  eioErrorDefaultMessage = '',
  noCertificateErrorMessage = locale.components.authority.noCertificateErrorMessage,
}: IAuthorityProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const [errors, setErrors] = useState<IErrorResult[]>([]);
  const [hasAuthorityError, setAuthorityError] = useState(false);
  const [certificateError, setCertificateError] = useState(false);
  const handleInformerItemClose = useCallback(
    idx => {
      const newErrorMessages = errors.filter((_, itemIdx) => idx !== itemIdx);

      setErrors(newErrorMessages);
    },
    [setErrors, errors]
  );

  const handleCloseInformer = useCallback(() => {
    setErrors([]);
  }, [setErrors]);

  const getAuthorities = useCallback(async () => {
    if (isEmpty(bankClient)) {
      handleCloseInformer();
      setAuthorityError(false);

      return;
    }

    setIsLoading(true);

    // Получаем сертификаты пользователя
    const certificates = await getCertificates(bankClient!);

    if (certificates.length === 0) {
      setCertificateError(true);
      handleCloseInformer();
      setAuthorityError(true);
      setIsLoading(false);
      setErrors([
        {
          message: noCertificateErrorMessage,
          type: AUTHORITY_VALIDATORS.CERTIFICATE,
        },
      ]);

      return;
    }

    // Получаем привилегии пользователя
    const [authorities, authoritiesError] = await getUserAuthorities({
      bankClient: bankClient!,
      certificates,
      validators,
      documentType,
      productCodes,
    });

    setIsLoading(false);

    if (authoritiesError) {
      handleCloseInformer();
      setAuthorityError(true);

      return;
    }

    const invalidAuthority = authorities?.find(item => !item.result);

    if (!invalidAuthority) {
      setAuthorityError(false);
      handleCloseInformer();

      return;
    }

    const errorMessages = invalidAuthority.validatorResults
      .filter(item => !item.result)
      // eslint-disable-next-line array-callback-return
      .map(item => {
        // eslint-disable-next-line default-case
        switch (item.type) {
          case AUTHORITY_VALIDATORS.CERTIFICATE:
            return {
              type: AUTHORITY_VALIDATORS.CERTIFICATE,
              message: item.message || certificateErrorDefaultMessage,
            };

          case AUTHORITY_VALIDATORS.CONSTITUTION:
            return {
              type: AUTHORITY_VALIDATORS.CONSTITUTION,
              message: item.message || constitutionErrorDefaultMessage,
            };

          case AUTHORITY_VALIDATORS.EIO:
            return {
              type: AUTHORITY_VALIDATORS.EIO,
              message: item.message || eioErrorDefaultMessage,
            };
        }
      });

    setAuthorityError(true);
    setErrors(errorMessages);
  }, [
    documentType,
    bankClient,
    productCodes,
    validators,
    handleCloseInformer,
    setIsLoading,
    setAuthorityError,
    setErrors,
    certificateErrorDefaultMessage,
    constitutionErrorDefaultMessage,
    eioErrorDefaultMessage,
    noCertificateErrorMessage,
  ]);

  useEffect(() => {
    void getAuthorities();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [documentType, JSON.stringify(bankClient), JSON.stringify(productCodes), JSON.stringify(validators)]);

  return {
    errors,
    hasAuthorityError,
    certificateError,
    isLoading,
    removeMessage: handleInformerItemClose,
    clearMessages: handleCloseInformer,
    reload: getAuthorities,
  };
};
