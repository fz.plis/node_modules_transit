import { renderHook, act } from '@testing-library/react-hooks';
import { to } from '@platform/core';
import type { IBankClient } from '../../../interfaces';
import { useAuthority } from '../use-authority';

jest.mock('../utils', () => ({
  getCertificates: () => Promise.resolve([{ id: 3 }, { id: 4 }]),
  getUserAuthorities: () =>
    to(
      Promise.resolve([
        {
          certificateId: '3',
          result: false,
          validatorResults: [
            { type: 'CERTIFICATE', result: true, message: null },
            {
              type: 'EIO',
              result: false,
              message: 'error',
            },
          ],
        },
      ])
    ),
  isEmpty: () => false,
}));

describe('use-authority', () => {
  const bankClient = {
    absId: '20145022',
    innKio: '8391541666',
    kpp: '492801454',
    fullName: 'Публичное акционерное общество "Мантия"',
    ogrnOgrip: '5117132437857',
    externalClientGuid: '',
  };

  it('render', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useAuthority({ bankClient: (bankClient as unknown) as IBankClient }));

    await waitForNextUpdate();
    expect(result.current.errors).toHaveLength(1);
  });

  it('информер должен удалиться успешно', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useAuthority({ bankClient: (bankClient as unknown) as IBankClient }));

    await waitForNextUpdate();
    expect(result.current.errors).toHaveLength(1);
    act(() => {
      result.current.removeMessage(0);
    });
    expect(result.current.errors).toHaveLength(0);
  });

  it('информер должен полностью очистится', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useAuthority({ bankClient: (bankClient as unknown) as IBankClient }));

    await waitForNextUpdate();
    expect(result.current.errors).toHaveLength(1);
    act(() => {
      result.current.clearMessages();
    });
    expect(result.current.errors).toHaveLength(0);
  });
});
