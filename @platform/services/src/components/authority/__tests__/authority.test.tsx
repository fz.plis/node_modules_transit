import React from 'react';
import { shallow } from 'enzyme';
import type { IBankClient } from '../../../interfaces';
import { Authority } from '../authority';

describe('authority', () => {
  const bankClient = {
    absId: '20145022',
    innKio: '8391541666',
    kpp: '492801454',
    fullName: 'Публичное акционерное общество "Мантия"',
    ogrnOgrip: '5117132437857',
    externalClientGuid: '',
  };

  it('компонент должен отрисоваться', () => {
    const Wrapper = shallow(<Authority bankClient={(bankClient as unknown) as IBankClient}>{() => <div>test</div>}</Authority>);

    expect(Wrapper).toMatchSnapshot();
  });
});
