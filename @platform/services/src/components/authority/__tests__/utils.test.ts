import { isEmpty } from '../utils';

describe('isEmpty', () => {
  it('объект не определен', () => {
    expect(isEmpty(undefined)).toBe(true);
  });

  it('объект должен быть пустым', () => {
    expect(isEmpty({ a: undefined, b: '', c: null })).toBe(true);
  });

  it('объект должен быть определен', () => {
    expect(isEmpty({ a: undefined, b: false, c: 0 })).toBe(false);
  });
});
