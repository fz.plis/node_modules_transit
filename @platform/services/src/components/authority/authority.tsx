import React, { useCallback } from 'react';
import { Box, LoaderOverlay, OverlayInformer, Typography } from '@platform/ui';
import { locale } from '../../localization';
import type { IAuthorityInformerProps, IAuthorityProps } from './interfaces';
import css from './styles.scss';
import { useAuthority } from './use-authority';

/**
 * Элемент информера.
 *
 * @param props Свойства.
 * @param props.idx Индекс элемента.
 * @param props.message Сообщение.
 * @param props.onClose Закрыть сообщение.
 * @class
 */
export const AuthorityInformer: React.FC<IAuthorityInformerProps> = ({ idx, message, onClose }) => {
  const handleClose = useCallback(() => {
    onClose(idx);
  }, [idx, onClose]);

  return (
    <Box className={css.informerContainer}>
      <OverlayInformer key={idx} className={css.informerItem} onClose={handleClose}>
        <Typography.P>{message}</Typography.P>
      </OverlayInformer>
    </Box>
  );
};

/**
 * Информер.
 *
 * @param props Свойства.
 * @param props.bankClient Организация пользователя.
 * @param props.documentType Тип документа.
 * @param props.productCodes Коды продуктов банка.
 * @param props.validators Опции проверки полномочий.
 * @param props.certificateErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для сертификата.
 * @param props.constitutionErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для ЕИО.
 * @param props.eioErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для конституцию.
 * @param props.noCertificateErrorMessage Сообщение по-умолчанию об отсутствии сертификата.
 * @param props.children Дочерние элементы.
 * @class
 */
export const Authority: React.FC<IAuthorityProps & { children(hasAuthorityError: boolean): React.ReactNode }> = ({
  bankClient,
  documentType,
  productCodes = [],
  validators,
  certificateErrorDefaultMessage = '',
  constitutionErrorDefaultMessage = '',
  eioErrorDefaultMessage = '',
  noCertificateErrorMessage = locale.components.authority.noCertificateErrorMessage,
  children,
}) => {
  const { errors, hasAuthorityError, removeMessage, isLoading } = useAuthority({
    bankClient,
    documentType,
    productCodes,
    validators,
    certificateErrorDefaultMessage,
    constitutionErrorDefaultMessage,
    eioErrorDefaultMessage,
    noCertificateErrorMessage,
  });

  return (
    <>
      <LoaderOverlay opened={isLoading} />
      {errors.map((item, idx) => (
        // eslint-disable-next-line react/no-array-index-key
        <AuthorityInformer key={idx} idx={idx} message={item.message} onClose={removeMessage} />
      ))}
      {children(hasAuthorityError)}
    </>
  );
};

Authority.displayName = 'Authority';
