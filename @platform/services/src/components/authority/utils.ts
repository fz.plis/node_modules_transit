import type { FilterInfo } from '@platform/core';
import { to } from '@platform/core';
import type { DOCUMENT_TYPE_CODE, IBankClient, IUserCertificate } from '../../interfaces';
import { AUTHORITY_VALIDATORS } from '../../interfaces';
import { certificateUserService } from '../../services-shared';

const CLIENT_UUID_FIELD = 'clientUuid';

/**
 * Получить сертификаты пользователя по его организации.
 *
 * @param bankClient Организация пользователя.
 */
export const getCertificates = async (bankClient: IBankClient) => {
  const [resp, err] = await to(
    certificateUserService.getList({
      pageSize: 10,
      offset: 0,
      filters: {
        clientUuid: ({
          fieldName: CLIENT_UUID_FIELD,
          value: bankClient.clientId,
        } as unknown) as FilterInfo,
      },
    })
  );

  return err ? [] : resp!.data;
};

/**
 * Получить доступные привилегии.
 *
 * @param props Свойства.
 * @param props.bankClient Организация.
 * @param props.certificates Сертификаты организации.
 * @param props.validators Опции проверки полномочий.
 * @param props.documentType Тип документа(проверка прав ЕИО).
 * @param props.productCodes Коды продуктов банка.
 */
export const getUserAuthorities = async ({
  bankClient,
  certificates,
  validators = [AUTHORITY_VALIDATORS.EIO, AUTHORITY_VALIDATORS.CERTIFICATE, AUTHORITY_VALIDATORS.CONSTITUTION],
  documentType,
  productCodes = [],
}: {
  bankClient: IBankClient;
  certificates: IUserCertificate[];
  validators?: AUTHORITY_VALIDATORS[];
  documentType?: DOCUMENT_TYPE_CODE;
  productCodes?: string[];
}) =>
  to(
    certificateUserService.checkAuthority({
      documentType,
      validators,
      productCodes,
      certificateIds: certificates.map(cert => cert.id),
      clientAbsId: bankClient.absId,
      clientEskId: bankClient.externalClientGuid,
      clientInnKio: bankClient.innKio,
      clientKpp: bankClient.kpp,
      clientName: bankClient.fullName,
      clientOgrn: bankClient.ogrnOgrip,
    })
  );

/**
 * Проверка на пустоту.
 *
 * @param obj Объект.
 */
export const isEmpty = <T extends Record<string, any>>(obj: T | null | undefined) =>
  !obj || Object.keys(obj).every(key => obj[key] === undefined || obj[key] === '' || obj[key] === null);
