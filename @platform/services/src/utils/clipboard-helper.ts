const fallbackCopyTextToClipboard = (text: string) => {
  const textArea = document.createElement('textarea');

  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.style.position = 'fixed';
  textArea.style.top = '-200px';
  textArea.focus();
  textArea.select();

  try {
    if (!document.execCommand('copy')) {
      throw new Error('Error copying data to clipboard');
    }
  } catch (err) {
    console.error(err);
  }

  document.body.removeChild(textArea);
};

export const copyTextToClipboard = (text: string) => {
  const { clipboard } = navigator as { clipboard: { writeText(text: string): void } };

  if (!clipboard) {
    fallbackCopyTextToClipboard(text);

    return;
  }

  clipboard.writeText(text);
};
