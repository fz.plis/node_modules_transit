import { DOCUMENT_STATUS_TYPE } from '@platform/ui';

interface ICreateStatusOptions<T> {
  executed: T[];
  inProcess: T[];
  rejected: T[];
}

export const createStatus = <T>(config: ICreateStatusOptions<T>) => (status: T): DOCUMENT_STATUS_TYPE => {
  if (config.executed.includes(status)) {
    return DOCUMENT_STATUS_TYPE.SUCCESS;
  } else if (config.rejected.includes(status)) {
    return DOCUMENT_STATUS_TYPE.ERROR;
  }

  return DOCUMENT_STATUS_TYPE.INPROGRESS;
};
