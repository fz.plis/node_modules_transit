export const getParamFromString = (str: string, paramName: string) => {
  if (!str || !paramName) {
    return null;
  }

  const paramsArr = str.split('&').map(x => x.split('='));

  return decodeURIComponent((paramsArr.find(x => (x[0] || '').toLowerCase() === paramName.toLowerCase()) || [])[1]);
};

export const getParamFromUrl = (url: string, paramName: string) => {
  const params = url.split('?')[1];

  return getParamFromString(params, paramName);
};

export const createServiceUrl = (clientUrl: string, internalUrl: string) => (slug: string) => (isInternal: boolean = false) =>
  `${isInternal ? internalUrl : clientUrl}/${slug}`;

export const parseUrlSearch = (search: string): Record<string, string> => {
  const values = search.substr(1);
  const obj = values.split('&').reduce<any>((result, part) => {
    const [key, value] = part.split('=');

    result[key] = decodeURIComponent(value);

    return result;
  }, {});

  return obj;
};

export const queryString = (params: Record<string, any>): string =>
  Object.keys(params)
    .map(key => key + '=' + encodeURIComponent(params[key]))
    .join('&');

/**
 * Метод для выделения базового роута.
 *
 * @param path Роут из useRouteMatch.
 * @returns Урл с отброшенным суффиксом '/:id/:tab'.
 */
export const getBaseUrl = (path: string) => {
  const idPath = path.indexOf('/:');

  // Отбрасываем у роута часть с '/:id/:tab' и т.п.
  return idPath === -1 ? path : path.slice(0, idPath);
};
