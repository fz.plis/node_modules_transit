import type { getTranslator } from '@platform/core';
import type {
  IApiValidationResult,
  IGroupValidationResult,
  IValidationCheckResult,
  IServerValidationResult,
  IBaseEntity,
  ISaveResponse,
} from '../../interfaces';
import { extractFromGroup, pathTransformer, flatify } from './helpers';

type Translator = ReturnType<typeof getTranslator>;

type ICheckMap = Record<string, IValidationCheckResult[]>;

export interface IValidationMap {
  errors: ICheckMap;
  warnings: ICheckMap;
}

/**
 * Функция принимает функцию-переводчик и возвращает функцию для трансформирования серверных контролей к виду фронтовых.
 *
 * @param translator Функция-переводчик, которая принимает ключ ресурса локализации и возвращает локализованную строку.
 * Обычно это обертка над функцией `translate`, которая замкнута на определенную именованную область локализации - `area`.
 *
 * Возвращает `transformer` Функцию, которая принимает результаты запроса с сервера.
 */
export const createSaveResponseHandler = (translator: Translator) => <T extends IBaseEntity>({
  data,
  validationResult,
}: {
  validationResult?: IApiValidationResult;
  data: T;
}): ISaveResponse<T> => {
  if (!validationResult) {
    return {
      data,
    };
  }

  const fieldMap: IValidationMap = validationResult.groupResults.reduce(
    (allFields: IValidationMap, group: IGroupValidationResult) => {
      const newMap = extractFromGroup(allFields, group);

      return newMap;
    },
    { errors: {}, warnings: {} }
  );

  const transformer = (checkResult: IValidationCheckResult) =>
    translator(checkResult.message!.message, { fieldName: `[[${checkResult.fieldName}]]`, ...checkResult.message!.params });
  const normalizeValidation = pathTransformer(data);
  const mapper = flatify(transformer)(normalizeValidation);
  const errors: IServerValidationResult[] = mapper(fieldMap.errors);
  const warnings: IServerValidationResult[] = mapper(fieldMap.warnings);

  return {
    data,
    validation: {
      errors,
      warnings,
    },
  };
};
