import { pathTransformer } from '../helpers';

describe('helpers', () => {
  describe('pathTransform', () => {
    const someField = 4;
    const emptyObj = {};
    const anotherArr = [
      { emptyObj, id: 'id_another_1', descr: 45 },
      { emptyObj, id: 'id_another_2', descr: 2 },
      { emptyObj, id: 'id_another_3', descr: 432 },
    ];
    const objWithArr = {
      anotherArr,
    };
    const simpleArr = [
      {
        id: 'test_id_1',
        name: 'test1',
      },
      {
        id: 'test_id_2',
        name: 'test2',
      },
    ];
    const arrInArr = [
      {
        id: 'arr_inn_arr_id_1',
        test: true,
        arr: [
          {
            id: 'complex_arr_id_1',
            notes: 'some_notes',
          },
          {
            id: 'complex_arr_id_2',
            notes: 'some_notes',
          },
          {
            id: 'complex_arr_id_3',
            notes: 'some_notes',
          },
        ],
      },
      {
        id: 'arr_inn_arr_id_2',
        test: true,
        arr: [
          {
            id: 'complex_arr_id_4',
            notes: 'some_notes',
          },
          {
            id: 'complex_arr_id_5',
            notes: 'some_notes',
          },
          {
            id: 'complex_arr_id_6',
            notes: 'some_notes',
          },
        ],
      },
      {
        id: 'arr_inn_arr_id_4',
        test: true,
        arr: [
          {
            id: 'complex_arr_id_7',
            notes: 'some_notes',
          },
        ],
      },
    ];

    const data = {
      someField,
      simpleArr,
      objWithArr,
      arrInArr,
      field: {
        almost: {
          leaf: 'Groovy!',
        },
      },
    };

    const transformer = pathTransformer(data);

    // must not modify field path
    it.each([['someField'], ['field.long.lead'], ['objWithArr.anotherArr'], ['simpleArr']])(
      'simpleField => pathTransformer(%s) // %s',
      (path: string) => {
        expect(transformer(path)).toBe(path);
      }
    );

    it.each([
      ['simpleArr["test_id_2"]', 'simpleArr[1]'],
      ['simpleArr["test_id_1"].name', 'simpleArr[0].name'],
      ['objWithArr.anotherArr["id_another_3"]', 'objWithArr.anotherArr[2]'],
      ['objWithArr.anotherArr["id_another_3"].emptyObj', 'objWithArr.anotherArr[2].emptyObj'],
      ['arrInArr["arr_inn_arr_id_4"].arr["complex_arr_id_7"]', 'arrInArr[2].arr[0]'],
      ['arrInArr["arr_inn_arr_id_4"].arr["complex_arr_id_7"].notes', 'arrInArr[2].arr[0].notes'],
      ['arrInArr["arr_inn_arr_id_2"].test', 'arrInArr[1].test'],
      ['arrInArr["arr_inn_arr_id_1"].arr["complex_arr_id_2"]', 'arrInArr[0].arr[1]'],
    ])('arrField => pathTransformer(%s) // %s', (path: string, expected: string) => {
      expect(transformer(path)).toBe(expected);
    });
  });
});
