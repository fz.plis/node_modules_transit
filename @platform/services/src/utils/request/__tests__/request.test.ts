import { emptyfy } from '../request';

describe('emptyfy', () => {
  it('with formData', () => {
    const obj = new FormData();

    expect(emptyfy(obj)).toBe(obj);
  });

  it('with null', () => {
    expect(emptyfy(null)).toBeUndefined();
  });

  it('with string', () => {
    expect(emptyfy('')).toBeUndefined();
  });

  it('with array', () => {
    const arr = ['', 'hello', [1, undefined, '', 'text']];

    expect(emptyfy(arr)).toStrictEqual(['hello', [1, 'text']]);
  });

  it('with undefined', () => {
    expect(emptyfy(undefined)).toBeUndefined();
  });

  it('with object', () => {
    const objFiles = new FormData();
    const initialObject = {
      users: [{ name: '', surname: '' }],
      documentNumber: '',
      documentDate: '',
      birthDate: '',
      car: {},
      work: null,
      address: { homeAddress: 'Moscow, Vavilova st', workAddress: '' },
      files: objFiles,
      hasMobilePhone: undefined,
    };
    const expectedObject = {
      users: [{}],
      car: {},
      address: { homeAddress: 'Moscow, Vavilova st' },
      files: objFiles,
      hasMobilePhone: undefined,
    };

    // eslint-disable-next-line jest/prefer-strict-equal
    expect(emptyfy(initialObject)).toEqual(expectedObject);
  });
});
