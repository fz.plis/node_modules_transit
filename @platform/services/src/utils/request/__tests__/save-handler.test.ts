import type { IApiValidationResult, IServerValidation, IValidationCheckResult, IControlValidationResult } from '../../../interfaces';
import { API_VALIDATION_RESULT, VALIDATION_LEVEL } from '../../../interfaces';
import { createSaveResponseHandler } from '../save-handler';

const testField = 'test.field';
const testWarningField = 'test.warningField';

const invalidCheck: IValidationCheckResult = {
  result: API_VALIDATION_RESULT.INVALID,
  fieldName: testField,
  message: { message: 'validation error', params: { pcrit: 'pcrit' } },
};

const validCheck: IValidationCheckResult = {
  result: API_VALIDATION_RESULT.VALID,
  fieldName: 'test.validField',
};

const warningCheck: IValidationCheckResult = {
  result: API_VALIDATION_RESULT.VALID,
  fieldName: testWarningField,
  message: { message: 'validation warning', params: { pwar: 'pwar' } },
};

const invalidResult: IControlValidationResult = {
  id: 'crInvalid',
  name: 'crInvalid',
  result: API_VALIDATION_RESULT.INVALID,
  level: VALIDATION_LEVEL.CRITICAL,
  checkResults: [invalidCheck],
};

const validResult: IControlValidationResult = {
  id: 'crValid',
  name: 'crValid',
  result: API_VALIDATION_RESULT.VALID,
  level: VALIDATION_LEVEL.CRITICAL,
  checkResults: [validCheck],
};

const warningResult: IControlValidationResult = {
  id: 'crWarning',
  name: 'crWarning',
  result: API_VALIDATION_RESULT.VALID,
  level: VALIDATION_LEVEL.WARNING,
  checkResults: [warningCheck],
};

const createGroupResult = (result: API_VALIDATION_RESULT) => (
  controlResults: IControlValidationResult[]
): { data: any; validationResult: IApiValidationResult } => ({
  data: {},
  validationResult: {
    groupResults: [
      {
        result,
        controlResults,
        id: 'test',
        name: 'test',
      },
    ],
    result,
  },
});

const createValidGroup = createGroupResult(API_VALIDATION_RESULT.VALID);
const createInvalidGroup = createGroupResult(API_VALIDATION_RESULT.INVALID);

describe('createSaveResponseHandler', () => {
  const translator = jest.fn((message, params) => `${message} params: ${JSON.stringify(params)}`);
  const transformer = createSaveResponseHandler(translator);

  it('simple validation', () => {
    const testData = createInvalidGroup([invalidResult]);

    const extected: IServerValidation = {
      errors: [{ fieldName: testField, messages: expect.any(Array) }],
      warnings: [],
    };

    expect(transformer(testData).validation).toMatchObject(extected);
  });

  it('with valids', () => {
    const testData = createValidGroup([validResult]);

    const extected: IServerValidation = {
      errors: [],
      warnings: [],
    };

    expect(transformer(testData).validation).toMatchObject(extected);
  });

  it('with warnings', () => {
    const testData = createValidGroup([warningResult]);

    const extected: IServerValidation = {
      errors: [],
      warnings: [{ fieldName: testWarningField, messages: expect.any(Array) }],
    };

    expect(transformer(testData).validation).toMatchObject(extected);
  });

  it('with valids and invalids', () => {
    const testData = createInvalidGroup([validResult, invalidResult]);

    const extected: IServerValidation = {
      errors: [{ fieldName: testField, messages: expect.any(Array) }],
      warnings: [],
    };

    const result = transformer(testData).validation!;

    expect(result).toMatchObject(extected);
    expect(result.errors[0].messages).toHaveLength(1);
  });

  it('all', () => {
    const testData = createInvalidGroup([validResult, invalidResult, warningResult]);

    const extected: IServerValidation = {
      errors: [{ fieldName: testField, messages: expect.any(Array) }],
      warnings: [{ fieldName: testWarningField, messages: expect.any(Array) }],
    };

    const result = transformer(testData).validation!;

    expect(result).toMatchObject(extected);
    expect(result.errors[0].messages).toHaveLength(1);
    expect(result.warnings[0].messages).toHaveLength(1);
  });
});
