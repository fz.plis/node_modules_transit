import type { AxiosPromise, AxiosRequestConfig } from 'axios';
import axios from 'axios';
import { redirectToLogin } from '../oauth-provider';
import { getStoreValue } from '../store';
import { getToken, clearToken } from '../token-storage';

export interface IRequestParams extends AxiosRequestConfig {
  disableTokenHeader?: boolean;
}

export type RequestMiddleware = (config: AxiosRequestConfig) => Promise<AxiosRequestConfig>;

export const authRequestMiddleware: RequestMiddleware = params =>
  Promise.resolve(getToken()).then(token => ({
    ...params,
    headers: token
      ? {
          ...params.headers,
          Authorization: `Bearer ${token}`,
        }
      : params.headers,
  }));

const EMPTY_VALUE = undefined;

/**
 * Заменяет пустые строки на `undefined` перед отправкой на бекенд и убирает пустые объекты.
 */
export const emptyfy = (obj: any): any => {
  if (obj instanceof FormData) {
    return obj;
  }

  if (obj === null) {
    return EMPTY_VALUE;
  }

  const objType = typeof obj;

  if (objType === 'string' && obj === '') {
    return EMPTY_VALUE;
  }

  if (Array.isArray(obj)) {
    const newArr = obj.map(emptyfy);

    return newArr.filter(x => typeof x !== 'undefined');
  }

  if (typeof obj === 'object' && obj !== null) {
    const newObj = Object.keys(obj).reduce((result: any, key: string) => {
      const val = obj[key];
      const newVal = emptyfy(val);

      if (typeof newVal !== 'undefined') {
        result[key] = newVal;
      }

      return result;
    }, {});

    return newObj;
  }

  return obj;
};

export const emptyfyMiddleware: RequestMiddleware = params => {
  const newData = emptyfy(params.data);

  return Promise.resolve({
    ...params,
    data: newData,
  });
};

export const setOrganizationIdHeaderMiddleware: RequestMiddleware = params => {
  if (params.headers?.Authorization) {
    const OrganizationId = getStoreValue(['main', 'user', 'selectedOrganization']);

    if (OrganizationId) {
      params.headers.OrganizationId = OrganizationId;
    }
  }

  return Promise.resolve(params);
};

// Почему то краткая запись сбоит
// в регдоках при сохранении заявки теряется часть данных в массивах
// почему то в некоторых случаях теряются данные
/* export const emptyfyMiddleware: RequestMiddleware = params =>
  Promise.resolve({
    ...params,
    data: emptyfy(params.data),
  }) */
const middlewares: RequestMiddleware[] = [emptyfyMiddleware];

export const addSetOrganizationIdHeaderMiddleware = () => {
  middlewares.push(setOrganizationIdHeaderMiddleware);
};

export const addRequestMiddleware = (middleware: (config: AxiosRequestConfig) => Promise<AxiosRequestConfig>) => {
  if (typeof middleware === 'function') {
    middlewares.push(middleware);
  } else {
    throw new Error('middleware should be a function!');
  }
};

const getResultConfig = ({ disableTokenHeader, ...params }: IRequestParams): Promise<AxiosRequestConfig> =>
  new Promise((resolve, reject) =>
    middlewares
      .reduce((task, middleware) => task.then(middleware), disableTokenHeader ? Promise.resolve(params) : authRequestMiddleware(params))
      .then(resolve)
      .catch(reject)
  );

// Если указать unknown, то для всех вызовов через request
// нужно явно будет указать тип возвращаемого значения.
export const request = <T = any>(params: IRequestParams): AxiosPromise<T> =>
  getResultConfig(params).then(config =>
    axios(config).catch(err => {
      if (err.response && err.response.status === 401) {
        clearToken();
        redirectToLogin();
      }

      throw err;
    })
  );
