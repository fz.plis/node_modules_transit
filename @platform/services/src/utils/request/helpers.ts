import { getByPath } from '@platform/core';
import type {
  IBaseEntity,
  IGroupValidationResult,
  IValidationCheckResult,
  IServerValidationResult,
  IControlValidationResult,
} from '../../interfaces';
import { VALIDATION_LEVEL, API_VALIDATION_RESULT } from '../../interfaces';
import { matchAll } from '../string';

type ICheckMap = Record<string, IValidationCheckResult[]>;

interface IValidationMap {
  errors: ICheckMap;
  warnings: ICheckMap;
}

export type PathIterator = ReturnType<typeof pathTransformer>;

/**
 * Возвращает функцию, преобразующую путь до поля с учетом списочных сущностей в исходном объекте.
 *
 * @param entity Проверяемый объект.
 */
export const pathTransformer = <T extends Record<string, any>>(entity: T) => {
  interface IFieldItem {
    path: string;
    val: any;
  }

  interface IFieldBlock extends IFieldItem {
    key: string;
  }

  // хранилище уже преобразованных путей
  const keysMap: Record<string, IFieldItem> = {};

  return (fieldName: string) => {
    const carretIndex = fieldName.lastIndexOf(']');

    // если нет списочных сущностей, то возвращаем как есть
    if (carretIndex === -1) {
      return fieldName;
    }

    if (fieldName in keysMap) {
      return keysMap[fieldName].path;
    }

    const part = fieldName.substring(0, carretIndex + 1);
    const suffix = fieldName.substring(carretIndex + 1);
    // получаем все вложенные сущности из пути
    const matches = matchAll(part, /(.*?)\["(.*?)"]/g)!;

    const { path }: IFieldBlock = matches.reduce(
      (result: IFieldBlock, match: string[]) => {
        const [field, pathData, id] = match;
        const fieldpath = result.key.concat(field);

        // если путь уже был мемоизирован, то переходим к следующей части
        if (field in keysMap) {
          return {
            ...keysMap[field],
            key: fieldpath,
          };
        }

        // преобразуем часть пути с идентификатором в часть пути с индексом
        const normalized = pathData.startsWith('.') ? pathData.substring(1) : pathData;
        const val: IBaseEntity[] = getByPath(result.val, normalized);
        const index = val.findIndex(x => x.id === id);
        const indexPath = `${result.path.concat(pathData)}[${index}]`;

        const item: IFieldItem = {
          path: indexPath,
          val: val[index],
        };

        // запоминаем преобразованный путь
        keysMap[fieldpath] = item;

        return {
          ...item,
          key: fieldpath,
        };
      },
      { path: '', val: entity, key: '' }
    );

    return path.concat(suffix);
  };
};

export const flatify = (localizator: (checkResult: IValidationCheckResult) => string) => (pathIterator: PathIterator) => (
  valMap: ICheckMap
): IServerValidationResult[] =>
  Object.keys(valMap).map((fieldName: string) => ({
    fieldName: pathIterator(fieldName),
    messages: valMap[fieldName].map(localizator),
  }));

export const extractFromControlResult = (allFields: IValidationMap, result: IControlValidationResult): IValidationMap =>
  result.checkResults.reduce(
    (controls: IValidationMap, item) => {
      const part = result.level === VALIDATION_LEVEL.CRITICAL ? 'errors' : 'warnings';

      if (item.result === API_VALIDATION_RESULT.VALID && part === 'errors') {
        return controls;
      }

      const arr = controls[part][item.fieldName] || [];

      arr.push(item);

      controls[part][item.fieldName] = arr;

      return controls;
    },
    { ...allFields }
  );

export const extractFromGroup = (allFields: IValidationMap, group: IGroupValidationResult) =>
  group.controlResults.reduce(
    (result, item) => {
      const newMap = extractFromControlResult(result, item);

      return newMap;
    },
    { ...allFields }
  );
