import { useCallback } from 'react';
import type { History } from 'history';
import { useHistory } from 'react-router-dom';

/**
 * Хук для создания функции редиректа через react-router.
 *
 * @param url - URL, на котоый нужно перевести пользователя.
 * @param state - Дополнительные данные, передаваемые со страницы перехода.
 * @returns Колбэк, при вызове которого произойдёт редирект.
 * @example
 * ```
 * useRedirect('/path/to/scroller', { referer: '/path/to/current/form' })
 * ```
 * Затем на странице скроллера можно получить данные state:
 * ```
 * const { state: { referer } } = useLocation();
 * ```
 */
export const useRedirect = (url: string, state?: History.LocationState) => {
  const { push } = useHistory();

  return useCallback(() => push(url, state), [push, url, state]);
};
