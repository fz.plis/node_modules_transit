import type { IBranch } from '../interfaces';

export interface IUserName {
  /**
   * Фамилия.
   */
  familyName: string;
  /**
   * Имя.
   */
  firstName: string;
  /**
   * Отчество.
   */
  middleName: string | null | undefined; // может быть null, если не заполнено (присылает бек)
}

export const getFio = ({ familyName, firstName, middleName }: IUserName) => `${familyName || ''} ${firstName || ''} ${middleName || ''}`;

export const getBranchLabel = ({ name, number: branchNumber }: IBranch) => `${name} ${branchNumber}`;
