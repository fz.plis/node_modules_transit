export const MAP_ESCAPED_XML: Record<string, string> = {
  '&amp;': '&',
  '&quot;': '"',
  '&lt;': '<',
  '&gt;': '>',
};

export const decodeXml = (xml: string = '') => xml.replace(/(&quot;|&lt;|&gt;|&amp;)/g, (_, item) => MAP_ESCAPED_XML[item]);
