import { request } from './request';

export const getDefaultLocaleService = (url: string) => ({
  get: (): Promise<Record<string, string>> =>
    request({
      //  TODO: хардкод, так как нет полноценной поддержки локализации
      url: `${url}/ru_Ru`,
      method: 'GET',
    }).then(resp => resp.data),
});
