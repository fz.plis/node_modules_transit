import type { IFiasResult } from '../interfaces';
import { request } from './request';

export const getDefaultSearchService = (url: string) => ({
  search: (query: string, count = 10): Promise<IFiasResult[]> =>
    request({
      url: `${url}`,
      data: {
        query,
        count,
      },
      method: 'POST',
    }).then(resp => resp.data.suggestions),
});
