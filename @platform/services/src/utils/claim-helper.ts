import type { IIndexable } from '@platform/ui';
import { COMMON_STREAM_URL } from '../constants/stream-urls';
import type { IAccountClaims, IOpenAccount, IAccount } from '../interfaces';

export const getPercentFill = (obj: IIndexable, onlyPositiveBoolean = true): number => {
  if (!obj) {
    return 0;
  }

  let count = 0;
  let fillCount = 0;

  Object.keys(obj).forEach(key => {
    const value = obj[key];

    if (typeof value === 'boolean' && (value || !onlyPositiveBoolean)) {
      fillCount += 1;
    }

    if (typeof value === 'string' && value) {
      fillCount += 1;
    } else if (typeof value === 'number' && value) {
      fillCount += 1;
    } else if (typeof value === 'object' && value) {
      if (Array.isArray(value) && value[0]) {
        fillCount += 1;
      } else {
        const subPercent = getPercentFill(value, onlyPositiveBoolean);

        fillCount = fillCount + (subPercent ? getPercentFill(value) / 100 : 0);
      }
    }

    count += 1;
  });

  return (fillCount * 100) / count;
};

export const getClaimPercentFill = (claim: IAccountClaims) => {
  const { client, branch, accountsInfo } = claim;
  const sections = [client, branch, accountsInfo];

  return Math.round(sections.reduce((sum: number, section: IIndexable) => sum + getPercentFill(section) / sections.length, 0));
};

export const getOpenAccountPercentFill = (claim: IOpenAccount) => {
  const {
    bankClient = { clientId: '' },
    accountInfos,
    commissionAccount = { number: '' },
    branch,
    contract = { number: '' },
    opopAccount = { number: '' },
  } = claim;
  const sections = [
    { bankClient: bankClient.clientId },
    { contract: contract.number },
    {
      accountInfo:
        // eslint-disable-next-line no-nested-ternary
        !accountInfos || accountInfos.length === 0
          ? null
          : accountInfos[0].reservedAccounts.length > 0
          ? accountInfos[0].reservedAccounts[0].number
          : accountInfos[0].openingAccountsInfos,
    },
    { branch },
    { commissionAccount: commissionAccount.number },
    { opopAccount: opopAccount.number },
  ];

  const r = Math.round(sections.reduce((sum: number, section) => sum + getPercentFill(section) / sections.length, 0));

  return r;
};

/**
 * Возвращает относительный url для перехода к заявке, по которой был открыт счет.
 * Может вернуть урл на заявку на резервирование, открытие первого счета или открытие 2+ счета.
 *
 * @param doc ДТО счета.
 * @param isAdmin Признак перехода на страницу "Управление заявкой" для админа.
 * @returns Относительный урл до заявки.
 * @throws Not Found.
 */
export const getPageUrlOfRequest = (doc: IAccount, isAdmin = false) => {
  if (doc.openingRequestId) {
    return `${COMMON_STREAM_URL.OPEN_ACCOUNTS}/${doc.openingRequestId}`;
  } else if (doc.requestId) {
    return `${COMMON_STREAM_URL.RESERVE_ACCOUNT}/${doc.requestId}`;
  } else if (doc.documentPackageRequestId) {
    let baseUrl = COMMON_STREAM_URL.REGDOC;

    if (isAdmin) {
      baseUrl = baseUrl + '/manage';
    }

    return `${baseUrl}/${doc.documentPackageRequestId}`;
  } else if (doc.openingContractRequestId) {
    return `${COMMON_STREAM_URL.NEW_OPEN_ACCOUNT}/${doc.openingContractRequestId}`;
  }

  throw new Error('Not Found');
};
