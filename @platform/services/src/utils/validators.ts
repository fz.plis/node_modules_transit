/* eslint-disable sonarjs/no-duplicate-string */
/* eslint-disable @eco/no-missing-localization */
import dayjs from 'dayjs';
import {
  getByPath,
  validators as baseValidators,
  err as baseErr,
  warn as baseWarn,
  validationRules,
  when as baseWhen,
  rulesGenerator,
  asRule,
  RULE_SEVERITIES,
  conditionGenerator,
  asCondition,
} from '@platform/core';
import type { IValidators, IValidatorParams } from '../interfaces';
import { locale } from '../localization';

const noError = undefined;

const nillable = (validator: (val: any, ...args: any[]) => string | undefined) => (value: any, ...args: any[]) => {
  if (value !== null && value !== undefined) {
    return validator(value, ...args);
  }

  return noError;
};

/**
 * Телефон начинается с +7, поэтому берем с третьего символа.
 */
const meaningSymbolIndex = 2;

const phoneNumberLength = 12;

// eslint-disable-next-line prefer-regex-literals
const LOGIN_REGEXP = new RegExp('^[0-9a-zA-Z]{7,50}$');

const EMAIL_VALIDATION_REGEX =
  "^[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

// Проверка UUID v4, допустимы числовые значения и символы от 'a' до 'f'
// https://tools.ietf.org/html/rfc4122
// eslint-disable-next-line prefer-regex-literals
const regExpUuid = new RegExp('^[0-9a-fA-F]{8}-([0-9a-fA-F)]{4}-){3}[0-9a-fA-F]{12}$');

// eslint-disable-next-line prefer-regex-literals
const regExpPassword = new RegExp(
  '(?=.*[!@#$%^&*()_+|~\\-=`{}\\[\\]:\\";\'<>?,.\\/])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()_+|~\\-=`{}\\[\\]:\\";\'<>?,.\\/]{8,50}'
);

export const loginValidators: IValidators = {
  doesNotMatchPhone: ({ message, params = {} }: IValidatorParams) => (value: any[], allValues = {}) => {
    const phoneNumber = getByPath(allValues, params.path);

    if (!phoneNumber || typeof phoneNumber !== 'string') {
      return noError;
    }

    return phoneNumber.length < phoneNumberLength || !value.includes(phoneNumber.slice(meaningSymbolIndex)) ? noError : message;
  },
  email: ({ message }: IValidatorParams) => value => (new RegExp(EMAIL_VALIDATION_REGEX).test(value) ? noError : message),
  /**
   * Валидатор для поля "логин".
   * Не может включать номер телефона, равняться паролю.
   * Может включать только латинские символы и цифры.
   */
  login: ({ message, params = {} }: IValidatorParams) => (value, allValues) => {
    /**
     * TODO ждем уточнений по https://jira.gboteam.ru/browse/GBO-5671
     * let err = loginValidators.email({ message })(value)
     * if (!err) { return noError }
     * Пока принято решение сделать по постановке https://confluence.gboteam.ru/pages/viewpage.action?pageId=6364043.
     */

    const validators = [
      baseValidators.matchesPattern({ message: locale.validation.login.pattern, params: { regexp: LOGIN_REGEXP } }),
      loginValidators.doesNotMatchPhone({ message, params: { path: params.phoneField } }),
      baseValidators.notEqualToField({ message, params: { path: params.passwordField } }),
    ];

    for (const validator of validators) {
      const err = validator(value, allValues);

      if (err) {
        return err;
      }
    }

    return noError;
  },
};

export const documentsValidators: IValidators = {
  ogrnOgrnipCalcKey: ({ message }: IValidatorParams) => value => {
    if (!value) {
      return noError;
    }

    const { length } = value;

    if (length === 13 || length === 15) {
      const k = length === 13 ? 11 : 13;
      const lastKey = value[k + 1];
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      const n = (parseInt(value.slice(0, -1)) % k).toString().slice(-1);

      return n === lastKey ? noError : message;
    }

    return noError;
  },
};

export const dateValidators: IValidators = {
  dateAfter: ({ params = {}, message }) => nillable(value => (dayjs(value).isAfter(params.value) ? noError : message)),
  dateBefore: ({ params = {}, message }) => nillable(value => (dayjs(value).isBefore(params.value) ? noError : message)),
};

export const validators = {
  ...baseValidators,
  ...dateValidators,
  ...loginValidators,
  ...documentsValidators,
};

const rules = {
  notEmpty: (message: string = 'Поле обязательно для заполнения') => validationRules.notEmpty(message),
  notEmptyString: (message: string = 'Поле обязательно для заполнения') => validationRules.notEmptyString(message),
  notEmptyObject: (message: string = 'Поле обязательно для заполнения') => validationRules.notEmptyObject(message),
  shorterThan: (length: number, message: string = locale.validation.shorterThan({ length })) =>
    validationRules.shorterThan(length, message),
  longerThan: (length: number, message: string = locale.validation.longerThan({ length })) => validationRules.longerThan(length, message),
  exactLength: (length: number, message: string = locale.validation.exactLength({ length })) =>
    validationRules.exactLength(length, message),
  onlyNumbers: (message: string = locale.validation.onlyNumbers) => validationRules.onlyNumbers(message),
  notNil: (message: string = 'Поле обязательно для заполнения') => validationRules.notNil(message),
  decimalMin: (
    min: number,
    inclusive: boolean,
    message: string = inclusive ? `Значение должно быть больше или равно ${min}` : `Значение должно быть больше ${min}`
  ) => validationRules.decimalMin(min, inclusive, message),
  decimalMax: (
    max: number,
    inclusive: boolean,
    message: string = inclusive ? `Значение должно быть меньше или равно ${max}` : `Значение должно быть меньше ${max}`
  ) => validationRules.decimalMax(max, inclusive, message),
  email: (message: string = 'Указан неверный email') => asRule({ id: 'email', message }),
  login: ({
    message = locale.validation.login.phoneOrPassword,
    ...params
  }: {
    phoneField?: string;
    passwordField?: string;
    message?: string;
  }) => asRule({ id: 'login', message, params }),
  innCalcKey: (message: string = 'Реквизит не прошел проверку на ключевание') => asRule({ id: 'innCalcKey', message }),
  ogrnOgrnipCalcKey: (message: string = 'Реквизит не прошел проверку на ключевание') => asRule({ id: 'ogrnOgrnipCalcKey', message }),
  doesNotMatchPhone: (path: string, message: string) => asRule({ id: 'doesNotMatchPhone', message, params: { path } }),
  dateAfter: (value: string, message: string) => asRule({ id: 'dateAfter', message, params: { value } }),
  dateBefore: (value: string, message: string) => asRule({ id: 'dateBefore', message, params: { value } }),
  isEqual: (value: boolean | number | string, message: string) => asRule({ id: 'isEqual', message, params: { value } }),
  isUuid: (message: string = 'Некорректный идентификатор') => validationRules.matchPattern(regExpUuid, message),
  password: (message: string) => validationRules.matchPattern(regExpPassword, message),
};

const conditions = {
  longerThan: (length: number) => asCondition({ id: 'longerThan', params: { length: length + 1 } }),
};

export const err = {
  ...baseErr,
  ...rulesGenerator(rules, RULE_SEVERITIES.ERROR),
};

export const warn = {
  ...baseWarn,
  ...rulesGenerator(rules, RULE_SEVERITIES.WARNING),
};

export const when = (path: string) => ({
  ...baseWhen(path),
  ...conditionGenerator(conditions)(path),
});
