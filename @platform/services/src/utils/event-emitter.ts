type F<T> = (args: T) => void;

/**
 * Типизированный эвент эмиттер для сихронизации состояния между хуками useLocalStorage.
 *
 * @see https://gist.github.com/mudge/5830382#gistcomment-3398873
 */
export const eventEmitter = <T>() => {
  const events: Record<string, Array<F<T>>> = {};

  const on = (name: string, fn: F<T>) => {
    if (!events[name]) {
      events[name] = [];
    }

    events[name].push(fn);
  };

  const trigger = (name: string, args: T) => {
    if (!events[name]) {
      return false;
    }

    events[name].forEach(fn => fn(args));
  };

  const off = (name: string, fn: F<T>) => {
    if (events[name]) {
      const index = events[name].indexOf(fn);

      if (index >= 0) {
        events[name].splice(index, 1);
      }
    }
  };

  return {
    on,
    trigger,
    off,
  };
};
