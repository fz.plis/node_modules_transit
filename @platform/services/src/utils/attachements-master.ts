import { listValue } from '@platform/tools/istore';
import type { IFileObject } from '../interfaces';
import { showFile } from './export';
import { extToMimeType } from './files-helper';

export interface IBufferAttachement extends IFileObject {
  accessToken?: string;
  file?: File;
}

const unit = listValue<any>([]);

/**
 * Retur.
 */
type ListValueUnit = typeof unit;

export interface IAttachementsMasterParams<T> {
  attachmentsMethods: ListValueUnit['methods'];
  bufferMethods: ListValueUnit['methods'];
  getAttachments(): T[];
  getBuffer(): IBufferAttachement[];
  uploadFile(file: File): Promise<T>;
  downloadFile(accessToken: string, fileId: string, fileName: string): Promise<any>;
  transformer(input: T): IBufferAttachement;
  maxSize?: number;
}

export const attachementsMaster = <T>(params: IAttachementsMasterParams<T>) => {
  const getAttachmentById = (id: any) => params.getAttachments().find(x => params.transformer(x).id === id);

  const getFilesInfo = () => [
    ...params
      .getAttachments()
      .map(params.transformer)
      .filter(file => !params.getBuffer().some(buffer => buffer.id === file.id)),
    ...params.getBuffer(),
  ];

  return {
    uploadFile: (files: File[]) => {
      files.forEach((file, index) => {
        const item: IBufferAttachement = {
          id: `buffer_${index + params.getBuffer().length}`,
          name: file.name,
          size: file.size,
          isLoading: true,
          file,
        };
        const totalSize = getFilesInfo().reduce((sum, x) => sum + x.size, item.size);

        if (params.maxSize && totalSize > params.maxSize) {
          // eslint-disable-next-line @eco/no-missing-localization
          item.error = 'Превышен максимальный общий размер файлов!';
        }

        params.bufferMethods.add(item);

        if (item.error) {
          return;
        }

        params
          .uploadFile(file)
          .then(result => {
            if (!params.getBuffer().includes(item)) {
              return;
            }

            params.attachmentsMethods.add(result);
            params.bufferMethods.setItem(params.getBuffer().indexOf(item), {
              ...item,
              isLoading: false,
              id: params.transformer(result).id,
            });
          })
          .catch(e => {
            console.log(e);
            params.bufferMethods.setItem(params.getBuffer().indexOf(item), {
              ...item,
              isLoading: false,
              // eslint-disable-next-line @eco/no-missing-localization
              error: 'Произошла ошибка при загрузке!',
            });
          });
      });
    },
    downloadFile: (files: IBufferAttachement[]) => {
      files.forEach(file => {
        if (file.accessToken) {
          void params.downloadFile(file.accessToken, file.id, file.name).then(resp => {
            showFile(resp, file.name, extToMimeType(file.name));
          });
        } else if (file.file) {
          showFile(file.file, file.name, extToMimeType(file.name));
        }
      });
    },
    deleteFile: (file: IBufferAttachement) => {
      const bufferIndex = params.getBuffer().indexOf(file);

      if (bufferIndex !== -1) {
        params.bufferMethods.removeByIndex(bufferIndex);
      }

      const attachmentIndex = getAttachmentById(file.id);

      if (attachmentIndex) {
        params.attachmentsMethods.remove(attachmentIndex);
      }
    },
    getMaxSize: () => params.maxSize,
    getFilesInfo,
  };
};
