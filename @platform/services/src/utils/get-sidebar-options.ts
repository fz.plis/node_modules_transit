import type { IAppSidebarOptionProps } from '@platform/ui';
import { MenuIcons, Icons } from '@platform/ui';
import type { IMenuItem } from '../interfaces/menu';

/**
 * Сборник иконок из старого и нового дизайна.
 *
 * // TODO: Привести в порядок, когда дизайнер соберет все иконки.
 */
const AllIcons = {
  ...Icons,
  ...MenuIcons,
};

export const getSidebarOptions = (menuItems: IMenuItem[]): IAppSidebarOptionProps[] =>
  menuItems.map(({ label, icon, path, options, key, type, collapsed }) => {
    const sidebarOptionProps = ({} as unknown) as IAppSidebarOptionProps;
    /** При добавлении пунктов меню в app-configurations необходимо указывать руссифицированные значения. */

    sidebarOptionProps.label = label;

    sidebarOptionProps.collapsed = collapsed;

    if (icon && icon in AllIcons) {
      sidebarOptionProps.icon = AllIcons[icon];
    }

    if (path) {
      sidebarOptionProps.value = path;
    }

    if (options) {
      sidebarOptionProps.options = getSidebarOptions(options);
    }

    if (key) {
      sidebarOptionProps.id = key;
    }

    if (type) {
      sidebarOptionProps.type = type;
    }

    return sidebarOptionProps;
  });
