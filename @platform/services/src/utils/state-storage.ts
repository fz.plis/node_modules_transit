/* tslint:disable */
import { getDefaultStore } from '@platform/tools/istore'

declare const process: any

if (process.env.NODE_ENV === 'development') {
  const store = getDefaultStore()
  const storageKey = 'states'
  ;(window as any).saveState = (...stateNames: string[]) => {
    const statesToSave = stateNames.reduce((result, stateName) => {
      return { ...result, [stateName]: store.getState(stateName.split('.')) }
    }, {})

    localStorage.setItem(storageKey, JSON.stringify(statesToSave))
  }
  ;(window as any).clearSavedState = () => {
    localStorage.removeItem(storageKey)
  }

  const stateToRestore: any = JSON.parse(localStorage.getItem(storageKey) as any)

  if (stateToRestore && Object.keys(stateToRestore).length) {
    Object.keys(stateToRestore).forEach(path => {
      store.setState(path.split('.'), stateToRestore[path])
    })
  }
}
