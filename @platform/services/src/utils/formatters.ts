import { formatDateTime as dateTimeFormat } from '@platform/tools/date-time';

/**
 * Возвращает дату-время в фрмате ISO.
 *
 * @deprecated Следует использовать функцию `formatDateTime` из `@platform/tools/date-time`.
 */
export const formatDateTime = (dateStr: string, asLocal = false): string => dateTimeFormat(dateStr, { keepLocalTime: asLocal });

export const formatMobilePhone = (phone: string) => {
  if (!phone) {
    return '';
  }

  let result = '{0}{1} ({2}{3}{4}) {5}{6}{7}-{8}{9}-{10}{11}';

  for (let i = 0; i < 12; i++) {
    const char = phone[i] || '_';

    result = result.replace(`{${i}}`, char);
  }

  return result;
};

/**
 * Преобразование массива строк в одну строку с разделителями.
 *
 * @param items {Array<string>}. Массив строк.
 * @param separator {string}. Разделитель.
 */
export const prepareText = (items: Array<string | undefined>, separator = ', ') => {
  const result = items.reduce<string[]>((acc, val) => {
    if (val) {
      acc.push(val);
    }

    return acc;
  }, []);

  return result.join(separator);
};

export const formatToShortFio = (fio: string) =>
  fio
    .split(' ')
    .map((name, idx) => (idx === 0 ? name : `${name[0]}.`))
    .join(' ');
