import { extToMimeType } from './files-helper';

export const printBase64 = (base64: string, extension: string) => {
  const byteCharacters = atob(base64);
  const byteNumbers = new Array(byteCharacters.length);

  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }

  const byteArray = new Uint8Array(byteNumbers);
  const blob = new Blob([byteArray], {
    type: extToMimeType(extension),
  });

  if (window.navigator && 'msSaveBlob' in window.navigator) {
    const w = window.open() as Window;

    w.document.write(`${decodeURIComponent(escape(window.atob(base64)))}`);
    w.document.execCommand('print', false);
    w.print();
  } else {
    const fileURL = URL.createObjectURL(blob);
    const w = window.open(fileURL);

    w!.focus();
    w!.print();
  }
};
