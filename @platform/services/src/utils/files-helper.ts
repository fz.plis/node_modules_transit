export const extToMimeType = (extInput: string) => {
  if (!extInput || typeof extInput !== 'string') {
    throw new Error('ext is required and have to be a string');
  }

  const ext = extInput.toLowerCase();

  switch (ext) {
    case 'doc':
      return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    case 'docx':
      return 'application/msword';
    case 'text':
    case 'txt':
    case 'onec':
    case 'csv':
      return 'text/plain';
    case 'xls':
      return 'application/vnd.ms-excel';
    case 'xlsx':
      return 'vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    case 'html':
      return 'text/html';
    case 'pptx':
      return 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
    default:
      return `application/${ext}`;
  }
};

export const getExtenstion = (fileName: string) => {
  const splitted = fileName.split('.');

  if (splitted.length > 1) {
    return splitted[splitted.length - 1];
  }

  return '';
};

/** Функция проверки допустимости файлов, возвращает истину в случае недопустимости. */
export const checkIfForbiddenFiles = (files: File[], allowedMimes: string[], allowedExtentions: string[]) =>
  files.some(file => {
    const isAllowedMime = allowedMimes.includes(file.type);
    const isEmptyMime = file.type === '';
    const isNoMimesProvided = allowedMimes.length === 0;

    // Если MIME не определен или не был предоставлен список MIME-ов, то дальше проверять по расширению.
    if (isEmptyMime || isNoMimesProvided) {
      const extension = file.name.split('.').pop(); // формат файла

      return allowedExtentions.length > 0 && !allowedExtentions.includes(extension!); // если формат разрешён, то продолжаем
    }

    // Иначе полагаемся на проверку по MIME.
    return !isAllowedMime;
  });
