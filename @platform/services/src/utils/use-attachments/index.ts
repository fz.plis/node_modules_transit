import { useState, useRef, useCallback, useEffect } from 'react';
import { guid } from '@platform/tools/istore';
import { dialog } from '@platform/ui';
import type { IAttachment, IFileBuffer } from '../../interfaces';
import { FILE_UPLOAD_STATE } from '../../interfaces';
import { locale } from '../../localization';
import { errorHandler } from '../error';
import { checkIfForbiddenFiles } from '../files-helper';
import { FILE_UPLOAD_ERROR } from './constants';
import type { IAttachmentHookConfig } from './interfaces';

/**
 * Хук для загрузки файлов через `attachmentsService`, работает по аналогии с istore-доменом `attachments`.
 * В качестве обязательного параметра принимает attachmentService, куда передается файловый сервис для КЧ или БЧ.
 *
 * @example
 * ```
 * // пример использования с FileInput
 * const attachmentsProps = useAttachments({
 *   files,
 *   onEndLoad,
 *   onRemove,
 *   attachmentService
 * });
 *
 * return (
 *   <FileInput
 *     accept={attachmentsProps.accept}
 *     acceptText={'pdf'}
 *     files={attachmentsProps.buffer}
 *     maxSize={attachmentsProps.maxSize}
 *     multi={attachmentsProps.multi}
 *     onDeleteFile={attachmentsProps.remove}
 *     onFileUpload={attachmentsProps.onFileUpload}
 *   />
 * );
 * ```
 */
export const useAttachments = ({
  onEndLoad,
  onRemove,
  multi = false,
  accept = [],
  maxSize,
  interval = 500,
  maxTries = 600,
  files: initFiles = [],
  attachmentsErrors,
  setIsLoading,
  supportFormats = [],
  attachmentService,
}: IAttachmentHookConfig<IAttachment>) => {
  /** Набор обрабатываемых файлов. */
  const [buffer, setBuffer] = useState<IFileBuffer[]>(initFiles);
  /** Используется как способ передачи операции отмены из запроса в хендлеры (для примера см. `remove`). */
  const cancel = useRef(new Map<string, () => void>());

  const findIndexById = useCallback(
    (fileId: string, bufferToSearchIn: IFileBuffer[]) => bufferToSearchIn.findIndex(x => x.id === fileId),
    []
  );

  const removeAll = useCallback(() => {
    setBuffer([]);
  }, [setBuffer]);

  const updateBuffer = useCallback(
    (fileId: string, updateFile: Partial<IFileBuffer>) => {
      setBuffer(prevState => {
        const newBuffer = Array.from(prevState);
        const fileIndex = findIndexById(fileId, newBuffer);

        if (fileIndex !== -1) {
          newBuffer.splice(fileIndex, 1, {
            ...prevState[fileIndex],
            ...updateFile,
          });
        }

        return newBuffer;
      });
    },
    [findIndexById]
  );

  // Инициализация
  useEffect(() => {
    const newBuffer = Array.from(buffer);

    // при наличии attachmentsErrors с внешней валидации проставляем ошибки конкретным файлам.
    attachmentsErrors?.forEach(e => {
      const fileIndex = findIndexById(e.id, buffer);

      if (fileIndex !== -1) {
        newBuffer.splice(fileIndex, 1, {
          ...buffer[fileIndex],
          error: e.error,
        });
      }

      setBuffer(newBuffer);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [attachmentsErrors, buffer.length, findIndexById]); // не добавлять зависимости кроме этой

  const updateProgress = (fileId: string, progress: number) => {
    updateBuffer(fileId, { progress });
  };

  /** Хендлер сборса операции отмены действия (см. `cancel`). */
  const clearCancel = (id: string) => {
    cancel.current.delete(id);

    if (cancel.current.size === 0 && setIsLoading) {
      setIsLoading(false);
    }
  };

  /** Хендлер удаления файла, изменяет `buffer` и вызывает переданный `onRemove`. */
  const remove = (file: IFileBuffer) => {
    if (file.isLoading) {
      const cancelFunc = cancel.current.get(file.id);

      if (cancelFunc) {
        cancelFunc();
        clearCancel(file.id);
      }
    }

    setBuffer(prevState => {
      const newBuffer = Array.from(prevState);
      const fileIndex = findIndexById(file.id, newBuffer);

      if (fileIndex !== -1) {
        newBuffer.splice(fileIndex, 1);
      }

      return newBuffer;
    });

    onRemove(file);
  };

  const setError = (id: string, err: string) => {
    clearCancel(id);

    updateBuffer(id, {
      isLoading: false,
      status: FILE_UPLOAD_STATE.UPLOADED,
      progress: undefined,
      error: err,
    });
  };

  const endLoad = (id: string, attachmentId: string) => {
    clearCancel(id);

    updateBuffer(id, {
      status: FILE_UPLOAD_STATE.UPLOADED,
      isLoading: false,
      progress: undefined,
      id: attachmentId,
    });
  };

  const startMalwareCheck = (id: string) => {
    updateBuffer(id, {
      status: FILE_UPLOAD_STATE.MALWARE_CHECK,
      isLoading: true,
      progress: 0,
    });
  };

  const markAsMalware = (id: string) => {
    updateBuffer(id, {
      isLoading: false,
      isMalware: true,
      errorObj: locale.files.error.isMalware,
      error: locale.files.error.isMalware,
    });
  };

  const onFileUpload = (files: File[]) => {
    if (!multi && files.length > 1) {
      dialog.showAlert(locale.files.error.singleOnly);

      return;
    }

    const maxSizeReached = maxSize ? files.reduce((sum, x) => sum + x.size, 0) > maxSize : false;

    if (maxSizeReached) {
      const { multi: mul, single } = locale.files.error.dataSizeExceeded;
      const message = multi ? mul : single;

      dialog.showAlert(
        message({
          size: maxSize!, // попадём в отображение диалога только в случае объявленного maxSize, см. maxSizeReached
        })
      );

      return;
    }

    if (checkIfForbiddenFiles(files, accept, supportFormats)) {
      // TODO: подумать над маппингом mime-type в расширения
      dialog.showAlert(`${locale.files.error.invalidExt} ${supportFormats.join(', ')}`);

      return;
    }

    if (setIsLoading) {
      setIsLoading(true);
    }

    files.forEach(file => {
      const id = guid();

      setBuffer(prevBuffer => {
        prevBuffer.push({
          id,
          name: file.name,
          size: file.size,
          progress: 0,
          status: FILE_UPLOAD_STATE.UPLOADING,
          isLoading: true,
        });

        return prevBuffer;
      });

      attachmentService
        .upload(file, {
          onUploadProgress: (e: ProgressEvent) => updateProgress(id, e.loaded / e.total),
          onCancel: c => {
            cancel.current.set(id, c);
          },
        })
        .then(result => {
          startMalwareCheck(id);

          return attachmentService.checkUploadProgress(result.id, {
            interval,
            maxTries,
            onUploadProgress: (e: ProgressEvent) => {
              updateProgress(id, e.loaded / e.total);
            },
            onCancel: c => {
              cancel.current.set(id, c);
            },
          });
        })
        .then(result => {
          endLoad(id, result.id);
          onEndLoad(result);
        })
        .catch(err => {
          clearCancel(id);

          throw err;
        })
        .catch(
          errorHandler({
            [FILE_UPLOAD_ERROR.NOT_LOADED_ERROR]: () => {
              setError(id, locale.files.error.notLoaded);
            },
            [FILE_UPLOAD_ERROR.FORBIDDEN]: () => {
              setError(id, locale.files.error.notLoaded);
            },
            [FILE_UPLOAD_ERROR.UNSUPPORTED_MEDIA_TYPE]: () => {
              setError(id, locale.files.error.invalidExtenstions);
            },
            [FILE_UPLOAD_ERROR.FILE_MALWARE]: () => {
              markAsMalware(id);
            },
          })
        );
    });
  };

  return {
    accept: accept.join(','),
    buffer,
    maxSize,
    multi,
    remove,
    removeAll,
    onFileUpload,
  };
};

export * from './interfaces';
