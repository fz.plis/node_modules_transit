import type { MutableRefObject } from 'react';
import type { IBaseAttachment } from '../../domains/attachments';
import type { IAttachment, IPollingOptions, IFileBuffer, AttachmentService } from '../../interfaces';

/** Конфиг для передачи параметров по работе с загружаемыми файлами в хуке `useAttachments`. */
export interface IAttachmentHookConfig<T extends IBaseAttachment> extends IPollingOptions {
  /** Действий по успешному окончанию загрузки файла на сервер. */
  onEndLoad(entity: T): void;
  /** Действие при удалении файла. */
  onRemove(info: IFileBuffer): void;
  /** Файловый сервис. */
  attachmentService: { upload: AttachmentService['upload']; checkUploadProgress: AttachmentService['checkUploadProgress'] };
  setIsLoading?(isLoading: boolean): void;
  uploadButtonText?: string;
  multi?: boolean;
  maxSize?: number;
  /** Набор MIME для проверки файлов. */
  accept?: string[];
  /** Список расширений файлов, без точки. */
  supportFormats?: string[];
  files?: IFileBuffer[];
  /** Список ошибок валидации для установки на конкретные файлы. */
  attachmentsErrors?: ISubmitAttachmentsErrors[];
}

export type AlternativeUploadHandler = (
  /** Файл загруженный в браузер. */
  file: File,
  /**
   * Id загружаемого файла
   * (изначально генерируется посредством guid() после загрузки на сервер заменяется на attachmentId).
   */
  id: string,
  innerHandlers: {
    /** Обновляет информацию о ходе загрузки файла. */
    updateProgress(fileId: string, progress: number): void;
    /** Используется как способ передачи операции отмены из запроса в хендлеры (см. UseAttachments). */
    cancel: MutableRefObject<Map<string, () => void>>;
  }
) => Promise<IAttachment>;

export interface ISubmitAttachmentsErrors {
  id: string;
  error: string;
}
