import { ERROR } from '../../interfaces';

const { FORBIDDEN, UNSUPPORTED_MEDIA_TYPE, FILE_MALWARE } = ERROR;

/** Коды ошибок при загрузке файлов. */
export const FILE_UPLOAD_ERROR = {
  /** Отсутствие авторизации. */
  FORBIDDEN,
  /** Формат содержимого не поддерживается сервером. */
  UNSUPPORTED_MEDIA_TYPE,
  /** Файл не прошел проверку антивирусом. */
  FILE_MALWARE,
  /** Ошибка загрузки. */
  NOT_LOADED_ERROR: 504,
};
