import dayjs from 'dayjs';
import type { IRenderNotification, IUserNotification } from '../interfaces';

// сортируем по дате
const sortByDateNotification = (notificationA: IRenderNotification, notificationB: IRenderNotification) => {
  if (dayjs(notificationA.date).isBefore(dayjs(notificationB.date))) {
    return 1;
  }

  if (dayjs(notificationA.date).isAfter(dayjs(notificationB.date))) {
    return -1;
  }

  return 0;
};

export const preparedNotification = (rawNotifications: IUserNotification[] = []): IRenderNotification => {
  const [result] = rawNotifications
    .reduce<IRenderNotification[]>((acc, userNotification) => {
      const {
        id,
        brief,
        executionDeadline,
        type: { preview },
      } = userNotification;
      const message = brief || preview || '';

      acc.push({
        message,
        notificationId: id,
        date: executionDeadline,
      });

      return acc;
    }, [])
    .sort(sortByDateNotification);

  return result;
};
