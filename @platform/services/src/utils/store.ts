import type { Path } from '@platform/tools/istore';
import { getDefaultStore } from '@platform/tools/istore';

export const getStoreValue = (path: Path) => getDefaultStore().getState(path);
