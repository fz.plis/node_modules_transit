let token = '';

const isPersistent = true;

export const setToken = (newToken: string) => {
  if (isPersistent) {
    localStorage.setItem('token', newToken);
  } else {
    token = newToken;
  }
};

export const getToken = () => {
  if (isPersistent) {
    return localStorage.getItem('token');
  }

  return token;
};

export const clearToken = () => {
  if (isPersistent) {
    localStorage.removeItem('token');
  } else {
    token = '';
  }
};
