import type { CONSENT_TYPE } from '../constants';
import { request } from './request';

export const getDefaultConsentService = (url: string) => ({
  getLastVersion: (type: CONSENT_TYPE) =>
    request({
      method: 'GET',
      url: `${url}/${type}/version`,
    }).then(res => res.data.data),
  getContent: (type: CONSENT_TYPE) =>
    request({
      method: 'GET',
      url: `${url}/${type}/content`,
    }).then(res => res.data),
});
