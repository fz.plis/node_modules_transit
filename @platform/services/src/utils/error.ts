import { dialog } from '@platform/ui';
import { signErrorDialog } from '../crypto';
import { ERROR } from '../interfaces';
import { locale } from '../localization';

export type Handler = (data?: any, defaultErrorHandler?: Handler) => void;

export type IHandlerError = Record<string, (data?: any, defaultErrorHandler?: Handler) => void>;

const DEFAULT_MESSAGES = {
  get INVALID_CREDENTIALS_MESSAGE() {
    return locale.errors.incorrectData;
  },
  get TOKEN_ATTEMPS_EXHAUST_MESSAGE() {
    return locale.errors.numberAttemptsExceededBlock;
  },
  get USER_BLOCKED_MESSAGE() {
    return locale.errors.accountBlocked;
  },
};

export const CTYPTO_ERROR_HANDLERS: IHandlerError = {
  [ERROR.CRYPTO_INTERNAL_SERVER_ERROR]: data => signErrorDialog.internalError(data),
  [ERROR.CRYPTO_UNAUTHORIZE]: () => signErrorDialog.keyAccessDeined(),
  [ERROR.CRYPTO_NOT_FOUND]: () => signErrorDialog.moduleNotFound(),
  [ERROR.CERTIFICATES_NOT_FOUND]: () => signErrorDialog.certificatesNotFound(),
  [ERROR.CERTIFICATES_NOT_RIGHT_SIGN]: () => signErrorDialog.certificatesNotRightSign(),
  [ERROR.CRYPTO_CERTS_NOT_FOUND]: () => signErrorDialog.signCertificatesNotFound(),
  [ERROR.CRYPTO_INVALID_SIGN_CERTIFICATE]: () => signErrorDialog.signCertificateInvalid(),
};

export const DEFAULT_ERROR_HANDLERS: IHandlerError = {
  ...CTYPTO_ERROR_HANDLERS,
  [ERROR.NO_CONNECTION]: () => dialog.showAlert(locale.errors.networkError),
  [ERROR.ONETIME_PASSWORD_EXPIRED]: () => {},
  [ERROR.INVALID_VERIFICATION_CODE]: () => {},
  [ERROR.ACCOUNT_NOT_FOUND]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.CONFIRMATION_ATTEMPTS_EXHAUSTED]: () => dialog.showAlert(DEFAULT_MESSAGES.TOKEN_ATTEMPS_EXHAUST_MESSAGE),
  [ERROR.INCORRECT_LOGIN_PASSWORD]: () => {},
  [ERROR.MFA_TOKEN_ATTEMPS_EXHAUSTED]: () => dialog.showAlert(DEFAULT_MESSAGES.TOKEN_ATTEMPS_EXHAUST_MESSAGE),
  [ERROR.PASSWORD_ATTEMPTS_EXHAUSTED]: () => dialog.showAlert(DEFAULT_MESSAGES.USER_BLOCKED_MESSAGE),
  [ERROR.UNEXPECTED_SYSTEM_ERROR]: () => dialog.showAlert(locale.errors.serverError),
  [ERROR.LOGIN_OR_EMAIL_EXIST]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.EMAIL_EXIST]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.PREREGISTERED_USER_NOT_FOUND]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.LOGIN_NOT_FOUND]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.USER_BLOCKED]: () => dialog.showAlert(DEFAULT_MESSAGES.USER_BLOCKED_MESSAGE),
  [ERROR.USER_NOT_CLIENT]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.INCORRECT_PASSWORD]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.PASSWORD_UNSAFE]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.PASSWORD_USE_ANOTHER_USER]: () => dialog.showAlert(DEFAULT_MESSAGES.INVALID_CREDENTIALS_MESSAGE),
  [ERROR.EMAIL_DELIVERY_ERROR]: () => dialog.showAlert(locale.errors.email),
  [ERROR.CAPTCHA_ERROR]: () => {},
  [ERROR.SMS_TIMEOUT_NOT_EXPIRED]: () => dialog.showAlert(locale.error.codeRequest),
  [ERROR.EMAIL_DELIVERY_ERROR]: () => dialog.showAlert(locale.error.codeCheck),
  [ERROR.ONETIME_PASSWORD_EXPIRED_REG]: () => {},
  [ERROR.CONFIRMATION_ATTEMPTS_EXHAUSTED_REG]: () => dialog.showAlert(locale.errors.numberAttemptsExceeded),
  [ERROR.ONETIME_PASSWORD_SEND_TIMEOUT]: () => dialog.showAlert(locale.errors.numberAttemptsExceeded),
  [ERROR.NUMBER_OF_TRIES_EXCEEDED_SEND_SMS]: () => dialog.showAlert(locale.errors.numberOfTriesExceededSendSMS),
  [ERROR.FAILED_SEND_CODE]: () => dialog.showAlert(locale.errors.phoneNumber),
  [ERROR.MFA_INVALID_TOKEN_VALUE]: () => {},
  [ERROR.VALIDATION_SERVER_CONTROLS_ERROR]: () => {},
  [ERROR.MFA_TOTP_USER_NOT_FOUND]: () => dialog.showAlert(locale.errors.mfaTotpUserNotFound),
  [ERROR.OPTIMISTIC_LOCK]: () => dialog.showAlert(locale.dialog.optimisticLockError.text, { header: locale.error.text }),
};

export const errorHandler = (errorHandlers?: IHandlerError) => (resp: any = {}) => {
  const allError = ({
    ...DEFAULT_ERROR_HANDLERS,
    ...errorHandlers,
  } as unknown) as IHandlerError;

  const { response, message } = resp;
  let code = 0;

  if (response) {
    code = response.data.errorInfo.code;
  } else if (message === 'Network Error') {
    code = ERROR.NO_CONNECTION;
  }

  const handler = allError[code];

  if (handler) {
    handler(((response || {}).data || {}).data, DEFAULT_ERROR_HANDLERS[code]);
  } else {
    throw resp;
  }
};

export const showSignError = (err: any, handlers: IHandlerError = CTYPTO_ERROR_HANDLERS) => errorHandler(handlers)(err);
