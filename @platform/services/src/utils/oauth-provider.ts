import { guid } from '@platform/tools/istore';
import type { IOAuthError, IOAuthSuccess } from '../interfaces';
import { clearLocalStorage } from './storage';
import { parseUrlSearch, queryString } from './url-helper';

export interface IOAuthProvider {
  buildLoginUrl(redirectUri: string): string;
  buildLogoutUrl(redirectUri: string): string;
  parseCallbackUrl(url: string): void;
}

export interface IOAuthProviderProps {
  clientId: string;
  authUri: string;
  loginUri: string;
  logoutUri: string;
  responseType: 'code' | 'token';
  onReceiveToken(result: IOAuthSuccess): void;
  onError(error: IOAuthError): void;
}

export class OAuthProvider implements IOAuthProvider {
  static of(props: IOAuthProviderProps) {
    return new OAuthProvider(props);
  }

  private options: IOAuthProviderProps = {
    clientId: '',
    authUri: '',
    logoutUri: '',
    responseType: 'token',
    onReceiveToken: () => {},
    onError: () => {},
    loginUri: '',
  };

  private constructor(props: IOAuthProviderProps) {
    this.options = props;
  }

  buildLoginUrl(redirectUri: string) {
    const { authUri, clientId, responseType, loginUri } = this.options;

    const query = queryString({
      redirect_uri: redirectUri,
      response_type: responseType,
      client_id: clientId,
      nonce: guid(),
      from_uri: authUri,
      login_uri: loginUri,
    });

    const authUrl = `${authUri}?${query}`;

    return authUrl;
  }

  buildLogoutUrl(redirectUri: string) {
    return `${this.options.logoutUri}?redirect_uri=${encodeURIComponent(redirectUri)}`;
  }

  parseCallbackUrl(url: string) {
    const { onError, onReceiveToken } = this.options;

    const result = parseUrlSearch(url);

    if (result.error) {
      onError((result as unknown) as IOAuthError);

      return;
    }

    const { access_token: accessToken } = result;

    if (accessToken) {
      onReceiveToken((result as unknown) as IOAuthSuccess);
    }
  }
}

let oauthProvider: IOAuthProvider | undefined;

export const getOauthProvider = () => {
  if (!oauthProvider) {
    throw new Error('OAuthProvider isn`t found. Call `setAuthProvider` for auth flow');
  }

  return oauthProvider;
};

export const setOauthProvider = (provider: IOAuthProvider) => (oauthProvider = provider);

export const redirectToLogin = () => {
  // Очищаем все локальные настройки для всех сервисов, согласно постановке
  clearLocalStorage();

  const loginUrl = getOauthProvider().buildLoginUrl(window.location.href);

  window.location.replace(loginUrl);
};
