import dayjs from 'dayjs';
import type { IParsedToken } from '../interfaces';

const b64DecodeUnicode = (str: string) =>
  decodeURIComponent(
    atob(str).replace(/(.)/g, (_, p) => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      let code = p.charCodeAt(0).toString(16).toUpperCase();

      if (code.length < 2) {
        code = `0${code}`;
      }

      return `%${code}`;
    })
  );

const parseBase64 = (str: string) => {
  let output = str.replace(/-/g, '+').replace(/_/g, '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw new Error('Illegal base64url string!');
  }

  try {
    return b64DecodeUnicode(output);
  } catch (err) {
    return window.atob(output);
  }
};

export const parseToken = (token: string): IParsedToken => {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');

  return JSON.parse(parseBase64(base64));
};

export const tokenIsExpired = (token: IParsedToken) => !token || dayjs(token.exp * 1000).isBefore(dayjs());
