import { getTranslator } from '@platform/core';
import { OPEN_REQ_LOC_RES } from '../constants/validation';

export const openAccountTranslator = getTranslator(OPEN_REQ_LOC_RES);
