import { saveAs } from 'file-saver';

export const showFile = (data: ArrayBuffer | Blob | Uint8Array | string, name: string, mimeType: string) => {
  let blob = data;

  if (typeof blob === 'string') {
    const byteCharacters = atob(data as string);
    const byteNumbers = new Array(byteCharacters.length);

    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    blob = new Uint8Array(byteNumbers);
  }

  const blobObj = new Blob([blob], { type: mimeType });
  const fileName = `${name}`;

  saveAs(blobObj, fileName);
};

export const getFileName = (param: string) => {
  let filename = '';

  if (param.includes('attachment')) {
    const filenameRegex = /filename\*?=["']?(?:UTF-8["']*)?([^\n\r"';]*)["']?;?/;
    const matches = filenameRegex.exec(param);

    if (matches?.[1]) {
      filename = matches[1].replace(/["']/g, '');
    }
  }

  return decodeURI(filename);
};
