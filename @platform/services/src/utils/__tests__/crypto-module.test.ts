import type { IAppConfig, ICryptoModule } from '../../interfaces';
import { canUseCryptoPlugin, isOutdatedCryptoModule } from '../crypto-module';

describe('crypto-module', () => {
  const v = '32.11.18.222';

  it('можно использовать версию', () => {
    expect(canUseCryptoPlugin('1.12.10', '1.12.4')).toBe(true);
  });

  it('можно использовать версию. Мажорная версия выше минимальной версии плагина', () => {
    expect(canUseCryptoPlugin('33.11.18.221', '32.11.18.221')).toBe(true);
  });

  it('нельзя использовать версию', () => {
    expect(canUseCryptoPlugin(v, '32.11.18.223')).toBe(false);
  });

  it('можно использовать версию. Версии идентичны', () => {
    expect(canUseCryptoPlugin('1.12.10', '1.12.10')).toBe(true);
  });

  it('можно использовать версию. Мин. версия не определена', () => {
    expect(canUseCryptoPlugin(v)).toBe(true);
  });

  it('можно использовать версию. Мин. версия не имеет минорной версии', () => {
    expect(canUseCryptoPlugin(v, '32.11')).toBe(true);
  });
});

describe('isOutdatedCryptoModule', () => {
  const pluginsConfig: IAppConfig['cryptoPlugins'] = {
    SFT: {
      url: '',
      minVersion: '1.12.13',
    },
    BSS: {
      url: '',
      minVersion: '3.20.2.2150',
    },
  };
  const mockCryptoModule = (result: { bss?: string; sft?: string }) =>
    (({
      getAvailableCryptoModulesVersion: () => Promise.resolve(result),
    } as unknown) as ICryptoModule);

  it('считает модуль устаревшим, если отсутствуют все плагины', async () => {
    const cryptoModule = mockCryptoModule({});

    expect(await isOutdatedCryptoModule(cryptoModule, pluginsConfig)).toBe(true);
  });

  it('считает модуль устаревшим, если присутствует только один устаревший плагин', async () => {
    const cryptoModule = mockCryptoModule({
      sft: '0.0.13',
    });

    expect(await isOutdatedCryptoModule(cryptoModule, pluginsConfig)).toBe(true);
  });

  it('считает модуль актуальным, если все плагины актуальной версии', async () => {
    const cryptoModule = mockCryptoModule({
      sft: '2.12.13',
      bss: '4.20.2.2150',
    });

    expect(await isOutdatedCryptoModule(cryptoModule, pluginsConfig)).toBe(false);
  });

  it('считает модуль актуальным, если присутствует только один плагин актуальной версии', async () => {
    const cryptoModule = mockCryptoModule({
      sft: '2.12.13',
    });

    expect(await isOutdatedCryptoModule(cryptoModule, pluginsConfig)).toBe(false);
  });

  it('считает модуль устаревшим, если присутствует хотя бы один устаревший плагин', async () => {
    const cryptoModule = mockCryptoModule({
      sft: '2.12.13',
      bss: '0',
    });

    expect(await isOutdatedCryptoModule(cryptoModule, pluginsConfig)).toBe(true);
  });
});
