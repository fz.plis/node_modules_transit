import { DOCUMENT_STATUS_TYPE } from '@platform/ui';
import { createStatus } from '../create-status';

describe('createStatus', () => {
  const config = { executed: ['status'], inProcess: [], rejected: ['status'] };

  it('если config.executed и config.rejected не включают status', () => {
    expect(createStatus(config)('wrong status')).toBe(DOCUMENT_STATUS_TYPE.INPROGRESS);
  });

  it('если config.executed включает status', () => {
    expect(createStatus(config)('status')).toBe(DOCUMENT_STATUS_TYPE.SUCCESS);
  });

  it('если config.rejected включает status', () => {
    config.rejected[0] = 'statusRej';
    expect(createStatus(config)('statusRej')).toBe(DOCUMENT_STATUS_TYPE.ERROR);
  });
});
