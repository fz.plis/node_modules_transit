import { getFileName } from '../export';

describe('getFileName', () => {
  it('without attachment', () => {
    const fileName = ' filename*="UTF-8\'ru-ru\'%D0%A3%D0%B2%D0%B5%D0%B4%D0%BE%D0%BC%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5_460_29.10.2019.pdf"';

    expect(getFileName(fileName)).toBe('');
  });

  it('with empty string', () => {
    expect(getFileName('')).toBe('');
  });

  it('not matches', () => {
    const fileName = 'attachment; text"';

    expect(getFileName(fileName)).toBe('');
  });
  it('with value', () => {
    const fileName = 'attachment; filename*="UTF-8\'%D0%A3%D0%B2%D0%B5%D0%B4%D0%BE%D0%BC%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5_460_29.10.2019.pdf"';

    expect(getFileName(fileName)).toBe('Уведомление_460_29.10.2019.pdf');
  });
});
