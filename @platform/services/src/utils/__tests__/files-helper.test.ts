import { checkIfForbiddenFiles } from '../files-helper';

type TestFile = Partial<File>;

const randomElementOf = <T>(list: T[]) => list[Math.floor(Math.random() * list.length)];

describe('attachments', () => {
  const allowedMimes = ['application/pkix-cert', 'application/x-x509-ca-cert', 'application/x-x509-user-cert', 'application/x-pem-file'];
  const allowedFormats = ['crt', 'pem'];

  it('пропускает файлы с корректными MIME', () => {
    const files: TestFile[] = allowedMimes.map(mime => ({
      type: mime,
      name: `${mime}.test`,
    }));

    expect(checkIfForbiddenFiles(files as File[], allowedMimes, allowedFormats)).toBe(false);
  });

  it('не пропускает файлы с отличным MIME', () => {
    const files: TestFile[] = allowedMimes.map(mime => ({
      type: `invalid ${mime}`,
      name: `document.${randomElementOf(allowedFormats)}`,
    }));

    expect(checkIfForbiddenFiles(files as File[], allowedMimes, allowedFormats)).toBe(true);
  });

  it('пропускает файлы по расширениям, если в списке MIME присутствует пустая строка', () => {
    const allowedMimesWithEmptyString = [...allowedFormats, ''];
    const files: TestFile[] = allowedMimesWithEmptyString.map(mime => ({
      type: `${mime}`,
      name: `document.${randomElementOf(allowedFormats)}`,
    }));

    expect(checkIfForbiddenFiles(files as File[], allowedMimesWithEmptyString, allowedFormats)).toBe(false);
  });

  it('пропускает файлы с пустым MIME, если их расширения корректные', () => {
    const files: TestFile[] = allowedMimes.map(() => ({
      type: '',
      name: `document.${randomElementOf(allowedFormats)}`,
    }));
    const invalidFiles = [
      {
        type: '',
        name: 'file.test',
      },
      { type: '', name: '' },
    ];

    expect(checkIfForbiddenFiles(files as File[], allowedMimes, allowedFormats)).toBe(false);
    expect(checkIfForbiddenFiles(invalidFiles as File[], allowedMimes, allowedFormats)).toBe(true);
  });
});
