import { COMMON_STREAM_URL } from '../../../constants/stream-urls';
import type { IAccount } from '../../../interfaces';
import { getPageUrlOfRequest } from '../../claim-helper';

describe('getPageUrlOfRequest', () => {
  type RequestIdKey = keyof Pick<IAccount, 'documentPackageRequestId' | 'openingRequestId' | 'requestId'>;
  type RequestIdMap = { [key in RequestIdKey]: string };
  type TestCase = [RequestIdKey, boolean | undefined, string];

  const ids: { [key in RequestIdKey]: IAccount[key] } = {
    documentPackageRequestId: 'regdocId',
    openingRequestId: 'openSecondPlusAccountId',
    requestId: 'reserveRequestId',
  };

  const EXPECTED: RequestIdMap = {
    documentPackageRequestId: `${COMMON_STREAM_URL.REGDOC}/${ids.documentPackageRequestId ?? ''}`,
    openingRequestId: `${COMMON_STREAM_URL.OPEN_ACCOUNTS}/${ids.openingRequestId ?? ''}`,
    requestId: `${COMMON_STREAM_URL.RESERVE_ACCOUNT}/${ids.requestId}`,
  };
  const EXPECTED_ADMIN: RequestIdMap = {
    ...EXPECTED,
    documentPackageRequestId: `${COMMON_STREAM_URL.REGDOC}/manage/${ids.documentPackageRequestId ?? ''}`,
  };

  /**
   * Возварщает пустой счет с заполненным полем ссылки на заявку.
   *
   * @param key Поле идентификатора заявки.
   * @returns ДТО счета.
   */
  const createAccount = (key: RequestIdKey): IAccount =>
    ({
      [key]: ids[key],
    } as any); // берем только одно поле, потому что функция только на него смотрит

  const buildCases = (isAdmin: boolean = false): TestCase[] =>
    Object.keys(ids).map(key => {
      const expected = isAdmin ? EXPECTED_ADMIN : EXPECTED;

      return [key, isAdmin, expected[key as RequestIdKey]] as TestCase;
    });

  const cases: TestCase[] = buildCases().concat(buildCases(true));

  it.each(cases)('заявка %s; админ - s%; url - %s)', (key, isAdmin, expected) => {
    const account = createAccount(key);

    expect(getPageUrlOfRequest(account, isAdmin)).toBe(expected);
  });

  it('должен выбросить ошибку, если нет идентификаторов', () => {
    expect(() => getPageUrlOfRequest({} as any, false)).toThrow(Error);
  });
});
