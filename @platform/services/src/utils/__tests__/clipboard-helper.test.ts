import { copyTextToClipboard } from '../clipboard-helper';

describe('copyTextToClipboard', () => {
  it('если execCommand("copy") возвращает false и clipboard не определен', () => {
    const execCommandMock = jest.fn().mockImplementation(() => false);

    const errorMock = jest.fn();

    Object.defineProperty(global.document, 'execCommand', { value: execCommandMock, writable: true });

    Object.defineProperty(global.console, 'error', { value: errorMock });

    copyTextToClipboard('123');

    expect(errorMock).toHaveBeenCalledTimes(1);
    expect(execCommandMock).toHaveBeenCalledTimes(1);
  });

  it('если execCommand("copy") возвращает true и clipboard не определен', () => {
    const execCommandMock = jest.fn().mockImplementation(() => true);
    const errorMock = jest.fn();

    Object.defineProperty(global.document, 'execCommand', { value: execCommandMock });

    Object.defineProperty(global.console, 'error', { value: errorMock });

    jest.spyOn(document.body, 'removeChild').mockImplementation();

    copyTextToClipboard('123');

    expect(execCommandMock).toHaveBeenCalledTimes(1);
    expect(errorMock).toHaveBeenCalledTimes(0);
    expect(document.body.removeChild).toHaveBeenCalledTimes(1);
  });

  it('если clipboard определен', () => {
    const execCommandMock = jest.fn().mockImplementation(() => false);

    const errorMock = jest.fn();

    Object.assign(navigator, {
      clipboard: {
        writeText: jest.fn(),
      },
    });

    Object.defineProperty(global.document, 'execCommand', { value: execCommandMock });

    Object.defineProperty(global.console, 'error', { value: errorMock });

    copyTextToClipboard('123');

    expect(errorMock).toHaveBeenCalledTimes(0);
    expect(execCommandMock).toHaveBeenCalledTimes(0);
    expect(navigator.clipboard.writeText).toHaveBeenCalledTimes(1);
    expect(navigator.clipboard.writeText).toHaveBeenCalledWith('123');
  });
});
