import { ERROR } from '../../interfaces';
import { errorHandler } from '../error';

describe('errorHandler', () => {
  it('with response', () => {
    const mockCallback = jest.fn();
    const response = {
      response: {
        data: {
          data: {
            accounts: ['Выберите как минимум одно значение'],
          },
          errorInfo: {
            code: 999,
          },
        },
      },
    };

    errorHandler({
      [ERROR.VALIDATION_SERVER_CONTROLS_ERROR]: mockCallback,
    })(response);

    expect(mockCallback.mock.calls[0][0]).toStrictEqual(response.response.data.data);
    expect(mockCallback.mock.calls[0][1]).toStrictEqual(expect.any(Function));
  });

  it('without response', () => {
    const mockCallback = jest.fn();
    const resp = {
      message: 'Network Error',
      code: 1,
    };

    errorHandler({
      [ERROR.NO_CONNECTION]: mockCallback,
    })(resp);

    expect(mockCallback.mock.calls[0][0]).toBeUndefined();
    expect(mockCallback.mock.calls[0][1]).toStrictEqual(expect.any(Function));
  });

  it('code is not found', () => {
    const resp = {
      message: 'Error',
      code: 10,
    };

    expect(() => {
      errorHandler()(resp);
    }).toThrow();
  });
});
