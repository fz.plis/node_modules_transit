import { getParamFromString, queryString } from '../url-helper';

describe('getParamFromString', () => {
  it('without string', () => {
    expect(getParamFromString('', 'from_uri')).toBeNull();
  });

  it('without param', () => {
    expect(getParamFromString('http://sandbox.jtc.ooo:21140/', '')).toBeNull();
  });

  it('with all parameters', () => {
    expect(getParamFromString('from_uri=http%3A%2F%2Fsandbox.jtc.ooo%3A21120%2Fapi%2Fuaa%2Foauth%2Fauthorize', 'from_uri')).toBe(
      'http://sandbox.jtc.ooo:21120/api/uaa/oauth/authorize'
    );
  });
});

describe('queryString', () => {
  it('with one param', () => {
    expect(queryString({ body: 'exit' })).toBe('body=exit');
  });

  it('with two params', () => {
    expect(
      queryString({
        id: '1',
        uri: 'https://webSite.com',
      })
    ).toBe('id=1&uri=https%3A%2F%2FwebSite.com');
  });

  it('fullExample', () => {
    const params = {
      client_id: 'c1111-2222-333d-aaaa-qwerty',
      redirect_uri: 'http://sandbox.jtc.ooo:21100/',
      response_type: 'token',
    };
    const expected = 'client_id=c1111-2222-333d-aaaa-qwerty&redirect_uri=http%3A%2F%2Fsandbox.jtc.ooo%3A21100%2F&response_type=token';

    expect(queryString(params)).toBe(expected);
  });
});
