import { loginValidators, documentsValidators } from '../validators';

describe('validators', () => {
  describe('loginValidators', () => {
    describe('email', () => {
      const message = 'failed';
      const validator = loginValidators.email({ message });

      it.each([
        ['111@mail.ru', undefined],
        ['12sdf@bla.com', undefined],
        ['dsds@ya.ru', undefined],
        ['34324@bk.ru', undefined],
        ['PetrovII@YANDEX.com', undefined],
        ['Ivanov@ya-ya.RU', undefined],
        ['i.ivanov@sub.do-main.ru', undefined],
        ['i.ivanov@sub-sub.sub.do-main.ru', undefined],
      ])('validEmailCases(%s)=%s', (email, expected) => {
        expect(validator(email)).toBe(expected);
      });
      it.each([
        ['12@;;;mail.ry', message],
        ['petrov@mail:ru', message],
        ['1234abc@>yandex.ru', message],
        ['sidorovKK@;;mail.ru', message],
        ['sdfsfsdfsd@ddd;ru', message],
        ['sdfsfsdf....@ddd.ru', message],
        ['PetrovII@YANDEX....com', message],
      ])('nonValidEmailCases(%s)=%s', (email, expected) => {
        expect(validator(email)).toBe(expected);
      });
    });
    describe('doesNotMatchPhone', () => {
      const message = 'failed';
      const params = { path: 'phoneNumber' };
      const validator = loginValidators.doesNotMatchPhone({ message, params });
      const phone = '+79212323424';

      it('without phoneNumber', () => {
        expect(validator(phone, { email: 'Ivan@mail.ru' })).toBeUndefined();
      });

      it('phoneNumber is not string', () => {
        expect(validator(phone, { phoneNumber: 89_212_323_424, email: 'Ivan@mail.ru' })).toBeUndefined();
      });

      it('phoneNumber has small length', () => {
        expect(validator(phone, { phoneNumber: '555555' })).toBeUndefined();
      });

      it('value match with phoneNumber', () => {
        expect(validator(phone, { phoneNumber: phone })).toBe(message);
      });
    });

    describe('login', () => {
      const message = 'ERROR';
      const params = {
        phoneField: 'phone',
        passwordField: 'password',
      };
      const PHONE = '+79991234567';
      const PASSWORD = 'qW123456';
      const loginValidator = loginValidators.login({ message, params });
      const buildDto = ({ phone = PHONE, password = PASSWORD }: { phone?: string; password?: string } = {}) => ({
        phone,
        password,
      });

      it.each([
        ['1', message],
        ['123456', message],
      ])('length failed for `%s`', login => {
        expect(loginValidator(login)).toBeDefined();
      });

      it('failed on invalid characters', () => {
        expect(loginValidator('abcdef@')).toBeDefined();
      });

      it('failed on match phone', () => {
        expect(loginValidator(`a${PHONE.slice(2)}`, buildDto())).toBe(message);
      });

      it('failed on match password', () => {
        expect(loginValidator(PASSWORD, buildDto())).toBe(message);
      });

      it('pass a email', () => {
        expect(loginValidator('mail@example.com')).toBeDefined();
      });
    });
  });
  describe('documentsValidators', () => {
    describe('ogrnOgrnipCalcKey', () => {
      const message = 'failed';
      const validator = documentsValidators.ogrnOgrnipCalcKey({ message });

      it.each([
        ['', undefined],
        ['540816794364', undefined],
        ['316547600092783', undefined],
        ['7708643668', undefined],
        ['111547600092783', message],
      ])('innOrgnValue', (value, expected) => {
        expect(validator(value)).toBe(expected);
      });
    });
  });
});
