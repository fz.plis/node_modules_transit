import { getFio } from '../text';

describe('getFio', () => {
  it.each([
    ['Иванов', 'Иван', 'Иванович', 'Иванов Иван Иванович'],
    ['Сидоров', 'Константин', '', 'Сидоров Константин '],
    ['Андреев', '', 'Андреевич', 'Андреев  Андреевич'],
    ['', 'Петр', 'Алексеевич', ' Петр Алексеевич'],
  ])('getFioUtil', (familyName, firstName, middleName, expected) => {
    expect(getFio({ familyName, firstName, middleName })).toBe(expected);
  });
});
