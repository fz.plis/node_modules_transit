import React from 'react';
import { render, act } from '@testing-library/react';
import { useLocalStorage } from '../use-local-storage';

class LocalStorageMock {
  store: Record<string, string> = {};

  clear() {
    this.store = {};
  }

  getItem(key: string) {
    return this.store[key] || null;
  }

  setItem(key: string, value: unknown) {
    this.store[key] = String(value);
  }

  removeItem(key: string) {
    // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
    delete this.store[key];
  }

  key(index: number) {
    return Object.values(this.store)[index];
  }

  get length() {
    return Object.values(this.store).length;
  }
}

const TestHook: React.FC<{
  callback(): void;
}> = ({ callback }) => {
  callback();

  return null;
};

global.localStorage = new LocalStorageMock();

describe('useLocalStorage', () => {
  const key = 'testStorageKey';
  let value = '';
  let setValue: React.Dispatch<React.SetStateAction<string>> = () => {};

  beforeEach(() => {
    global.localStorage.clear();
  });

  it('должно инициализироваться из defaultValue', () => {
    expect(global.localStorage.getItem(key)).toBeNull();

    render(
      <TestHook
        callback={() => {
          [value] = useLocalStorage(key, 'defaultValue');
        }}
      />
    );

    expect(value).toStrictEqual('defaultValue');
  });

  it('должен корректно записывать в хранилище', () => {
    render(
      <TestHook
        callback={() => {
          [value, setValue] = useLocalStorage(key, 'defaultValue');
        }}
      />
    );

    expect(value).toStrictEqual('defaultValue');
    expect(global.localStorage.getItem(key)).toStrictEqual(JSON.stringify('defaultValue'));

    setValue('storageValue');

    // форсим ререндер, чтобы useEffect отработал
    act(() => {});

    expect(global.localStorage.getItem(key)).toStrictEqual(JSON.stringify('storageValue'));
  });

  it('должно инициализироваться из хранилища', () => {
    global.localStorage.setItem(key, JSON.stringify('storageValue'));

    render(
      <TestHook
        callback={() => {
          [value] = useLocalStorage(key, 'defaultValue');
        }}
      />
    );

    expect(value).toStrictEqual('storageValue');
  });
});
