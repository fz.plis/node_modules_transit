import { polling, MAX_NUMBER_OF_ATTEMPS_REACHED } from '../async';

describe('polling', () => {
  const job = (args: any[]) => {
    if (args.length > 0) {
      return Promise.resolve(args);
    }

    return Promise.reject(args);
  };

  it('checker возвращает true', async () => {
    const checker = (res: any) => !!res;

    const poolingFunc = polling(job, checker, 0, 0);

    return expect(poolingFunc(['123'])).resolves.toStrictEqual(['123']);
  });

  it('checker возвращает false', () => {
    const checker = (res: any[]) => res.length > 1;

    const poolingFunc = polling(job, checker, 0, 1);

    expect.assertions(1);

    return poolingFunc(['123']).catch(e => {
      expect(e).toStrictEqual(Error(MAX_NUMBER_OF_ATTEMPS_REACHED));
    });
  });

  it('poolingFunc принимает пустой массив', () => {
    const checker = (res: any) => !!res;

    const poolingFunc = polling(job, checker, 0, 0);

    expect.assertions(1);

    return poolingFunc([]).catch(e => {
      expect(e).toStrictEqual([]);
    });
  });
});
