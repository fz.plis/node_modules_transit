import type { IActionWithAuth } from 'src';
import { Icons } from '../../../../ui/src';
import { AUTH_ERROR_MAP, AUTH_REQUEST_CONFIG } from '../../constants';
import {
  createTransformObject,
  transformError,
  getAvailableActions,
  transformFromApi,
  getAvailableItems,
  createAuthRequestFunc,
} from '../auth';
import * as requestModule from '../request/request';
import * as tokenHelpers from '../token-helper';
import * as tokenStorage from '../token-storage';

describe('transform object', () => {
  const object = { branchId: '123' };

  const transformedObject = { BRANCHID: '123' };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('отдает трансформированный объект', () => {
    const keyTransformer = (val: string) => val.toUpperCase();

    expect(createTransformObject(keyTransformer)(object)).toStrictEqual(transformedObject);
  });

  it('transformError без response', () => {
    const errWithoutResponse = { response: '' };

    return expect(transformError(errWithoutResponse)).rejects.toStrictEqual(errWithoutResponse);
  });

  it('transformError c response.status === 400', () => {
    const errWithResponse = { response: { data: { error: 'any_error' }, status: 400 } };

    const code = AUTH_ERROR_MAP[errWithResponse.response.data.error];
    const expected = {
      response: {
        data: {
          errorInfo: {
            code: code || errWithResponse.response.status,
          },
          data: transformFromApi(errWithResponse.response.data),
        },
      },
    };

    return expect(transformError(errWithResponse)).rejects.toStrictEqual(expected);
  });

  it('transformError без условий', () => {
    const errWithResponse = { response: { data: { error: 'user_is_blocked' }, status: 195 } };

    return expect(transformError(errWithResponse)).rejects.toStrictEqual(errWithResponse);
  });
});

const getToken = jest.spyOn(tokenStorage, 'getToken');

const parseToken = jest.spyOn(tokenHelpers, 'parseToken');

const request = jest.spyOn(requestModule, 'request');

describe('getAvailableActions', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const action = {
    action: jest.fn(),
  };

  (parseToken as jest.Mock).mockImplementation(val => ({ authorities: [val] }));

  const actions: IActionWithAuth[] = [{ icon: Icons.Account, label: 'string', authorities: ['123'], name: 'name', action }];
  const { authorities, ...rest } = actions[0];

  it('token существует', () => {
    (getToken as jest.Mock).mockImplementation(() => '123');
    expect(getAvailableActions(actions)).toStrictEqual([rest]);
  });

  it('без token', () => {
    (getToken as jest.Mock).mockImplementation(() => '');
    expect(getAvailableActions(actions)).toStrictEqual([]);
  });
});

describe('getAvailableItems', () => {
  it('без токена', () => {
    expect(getAvailableItems([{ authorities: ['123'], anotherProp: '321' }])).toStrictEqual([]);
  });

  it('token существует', () => {
    (getToken as jest.Mock).mockImplementation(() => '123');
    expect(getAvailableItems([{ authorities: ['123'], anotherProp: '321' }])).toStrictEqual([{ anotherProp: '321' }]);
  });
});

describe('createAuthRequestFunc', () => {
  it('прокинут параметр redirectUri', async () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    const params = {
      transform: jest.fn(data => ({ ...data[0] })),
      data: [{ prop1: '123' }],
      redirectUri: 'uri',
    };

    (request as jest.Mock).mockImplementation(() => Promise.resolve({ data: params }));

    const r: any = await createAuthRequestFunc('url')(params.transform)(params.data, params.redirectUri);

    expect(r.data).toStrictEqual([{ prop1: '123' }]);
    expect(requestModule.request).toHaveBeenCalledWith({
      ...AUTH_REQUEST_CONFIG,
      url: 'url',
      data: { prop1: '123', redirect_uri: 'uri' },
    });
  });
});
