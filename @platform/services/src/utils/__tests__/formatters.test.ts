import { formatDateTime, formatMobilePhone, prepareText } from '../formatters';

describe('formatMobilePhone', () => {
  it.each([
    ['+79899999999', '+7 (989) 999-99-99'],
    ['', ''],
    ['999999', '99 (999) 9__-__-__'],
    ['1', '1_ (___) ___-__-__'],
  ])('formatMobilePhone(%s)=%s', (phone, expected) => {
    expect(formatMobilePhone(phone)).toBe(expected);
  });
});

describe('prepareText', () => {
  it.each([
    [['1', '2', '3'], ',', '1,2,3'],
    [['1', undefined, '3'], ' ', '1 3'],
    [['1', undefined, '3'], undefined, '1, 3'],
    // eslint-disable-next-line no-template-curly-in-string
  ])('prepareText(%s, %s)=${%s}', (array, separator, expected) => {
    expect(prepareText(array, separator)).toBe(expected);
  });
});

describe('formatDateTime', () => {
  const dt = '2021-01-01T12:00:00+03:00';

  it('utc', () => {
    expect(formatDateTime(dt)).toBe('01/01/2021 9:00:00 AM');
  });

  it('local', () => {
    expect(formatDateTime(dt, true)).toBe('01/01/2021 12:00:00 PM');
  });
});
