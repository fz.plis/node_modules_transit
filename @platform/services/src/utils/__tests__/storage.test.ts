import { clearLocalStorage } from '../storage';

describe('clearLocalStorage', () => {
  const excludedStorageKey = 'GLOBAL_STORAGE_KEY';
  const storageKey = 'SOME_KEY';
  const initData = JSON.stringify({ a: 1 });

  beforeEach(() => {
    localStorage.setItem(excludedStorageKey, initData);
    localStorage.setItem(storageKey, initData);
  });

  it('clear storage, except excluded keys', () => {
    clearLocalStorage();
    expect(localStorage.getItem(excludedStorageKey)).toBe(initData);
    expect(localStorage.getItem(storageKey)).toBeNull();
  });

  it('completely clear storage', () => {
    clearLocalStorage(true);
    expect(localStorage.getItem(excludedStorageKey)).toBeNull();
    expect(localStorage.getItem(storageKey)).toBeNull();
  });
});
