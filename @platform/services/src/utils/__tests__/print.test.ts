import { printBase64 } from '../print';

describe('printBase64', () => {
  const open = jest.spyOn(window, 'open').mockReturnValue(global.window);
  const base64 = `TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0
  aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1
  c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0
  aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdl
  LCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=
  `;

  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('если msSaveBlob не существует', () => {
    const focus = jest.spyOn(window, 'focus').mockImplementation(() => {});
    const print = jest.spyOn(window, 'print').mockImplementation(() => {});

    Object.defineProperty(global.URL, 'createObjectURL', { value: jest.fn() });

    printBase64(base64, '.ext');

    /**
     * Сделано исключение правил т.к createObjectURL не поддерживается в opera mini, ie11.
     */
    // eslint-disable-next-line compat/compat
    expect(URL.createObjectURL).toHaveBeenCalled();
    expect(open).toHaveBeenCalled();
    expect(focus).toHaveBeenCalled();
    expect(print).toHaveBeenCalled();
  });

  it('если msSaveBlob существует', () => {
    window.navigator.msSaveBlob = () => true;

    const write = jest.spyOn(window.document, 'write').mockImplementation();

    Object.defineProperty(window.document, 'execCommand', { value: jest.fn() });

    printBase64(base64, '.ext');

    expect(open).toHaveBeenCalled();
    expect(window.document.execCommand).toHaveBeenCalled();
    expect(write).toHaveBeenCalled();
  });
});
