import React from 'react';
import { render } from '@testing-library/react';
import { useRedirect } from '../use-redirect';

const mockHistoryPush = jest.fn();

// мокаем push для проверки работы редиректа
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

/**
 * Компонент для тестирования хуков, сделано по аналогии с использованием TestHook в react-hooks-testing-library
 * {@link https://github.com/testing-library/react-hooks-testing-library/blob/master/src/pure.js}.
 *
 * @param props - Параметры.
 * @param props.callback - Колбэк, который должен установить результат хука снаружи скоупа.
 */
const TestHook: React.FC<{
  callback(): void;
}> = ({ callback }) => {
  callback();

  return null;
};

/**
 * Фукнция для получения результата хука из компонента, сделано по аналогии с использованием TestHook в react-hooks-testing-library
 * {@link https://github.com/testing-library/react-hooks-testing-library/blob/master/src/pure.js}.
 *
 * @param callback - Колбэк, который должен установить результат хука снаружи скоупа.
 */
const testHook = (callback: () => void) => {
  render(<TestHook callback={callback} />);
};

describe('useRedirect', () => {
  const testUrl = 'https://test';
  const testState = { userId: '123' };
  let redirectCallback: () => void;

  beforeEach(() => {
    testHook(() => {
      redirectCallback = useRedirect(testUrl);
    });
  });

  it('должно вернуть функцию', () => {
    expect(redirectCallback).toBeInstanceOf(Function);
  });

  it('должно редиректнуть на корректный URL с помощью react-router', () => {
    redirectCallback();
    expect(mockHistoryPush).toHaveBeenCalledWith(testUrl, undefined);
  });

  it('в react-router должен передаться state', () => {
    testHook(() => {
      redirectCallback = useRedirect(testUrl, testState);
    });
    redirectCallback();
    expect(mockHistoryPush).toHaveBeenLastCalledWith(testUrl, testState);
  });
});
