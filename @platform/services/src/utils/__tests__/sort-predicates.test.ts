import { byLabel } from '../sort-predicates';

describe('byLabel', () => {
  const a = { label: 'A' };
  const b = { label: 'B' };

  it('должна вернуться 1', () => {
    expect(byLabel(a, b)).toBe(-1);
  });

  it('должна вернуться -1', () => {
    expect(byLabel(b, a)).toBe(1);
  });

  it('должна вернуться 0', () => {
    expect(byLabel(a, a)).toBe(0);
  });
});
