import { truncateWithEllipsis } from '../string';

describe('truncateWithEllipsis', () => {
  it('должна вернуться исходная строка без изменений', () => {
    expect(truncateWithEllipsis('abc', 3)).toBe('abc');
  });

  it('должна вернуться усечённая строка оканчивающаяся троеточием', () => {
    expect(truncateWithEllipsis('abcd', 3)).toBe('ab...');
  });
});
