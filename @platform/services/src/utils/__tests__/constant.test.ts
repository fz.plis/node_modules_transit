import { constant } from '../constant';

enum TEST_STATUS {
  ONE = 'ONE',
  TWO = 'TWO',
  THREE = 'THREE',
}

const LIKE_ENUM_CONST = {
  [TEST_STATUS.ONE]: 'one',
  [TEST_STATUS.TWO]: 'two',
  [TEST_STATUS.THREE]: 'three',
};

const testNum = 4;

describe('constant', () => {
  it('works', () => {
    const testConst = constant<typeof TEST_STATUS>().of<string>().set('ONE', 'one').set('TWO', 'two').set('THREE', 'three').create();

    expect(testConst.THREE).toBe('three');
    expect(testConst.ONE).toBe('one');
    expect(testConst.TWO).toBe('two');
  });

  it('works with type', () => {
    const testConst = constant<typeof LIKE_ENUM_CONST>()
      .of<string>()
      .set(TEST_STATUS.ONE, 'one')
      .set(TEST_STATUS.TWO, 'two')
      .set(TEST_STATUS.THREE, 'three')
      .create();

    expect(testConst.ONE).toBe('one');
    expect(testConst.TWO).toBe('two');
    expect(testConst.THREE).toBe('three');
  });

  it('with number as value', () => {
    const testConst = constant<typeof TEST_STATUS>().of<number>().set('ONE', 1).set('TWO', 2).set('THREE', 3).create();

    expect(testConst.ONE).toBe(1);
    expect(testConst.TWO).toBe(2);
    expect(testConst.THREE).toBe(3);
  });

  it('with params', () => {
    const testConst = constant(TEST_STATUS).of(testNum).set('ONE', 1).set('TWO', 2).set('THREE', 3).create();

    expect(testConst.ONE).toBe(1);
    expect(testConst.TWO).toBe(2);
    expect(testConst.THREE).toBe(3);
  });

  it('with default value', () => {
    const testConst = constant(TEST_STATUS).of(testNum).set('ONE').set('TWO').set('THREE').create();

    expect(testConst.ONE).toBe(4);
    expect(testConst.TWO).toBe(4);
    expect(testConst.THREE).toBe(4);
  });

  it('set array of keys', () => {
    const testConst = constant(TEST_STATUS).of(testNum).set(['ONE', 'TWO'], 1).set('THREE', 3).create();

    expect(testConst.ONE).toBe(1);
    expect(testConst.TWO).toBe(1);
    expect(testConst.THREE).toBe(3);
  });

  it('set array of keys with defaults', () => {
    const testConst = constant(TEST_STATUS).of(testNum).set(['ONE', 'TWO']).set('THREE', 3).create();

    expect(testConst.ONE).toBe(4);
    expect(testConst.TWO).toBe(4);
    expect(testConst.THREE).toBe(3);
  });
});
