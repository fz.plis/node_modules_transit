import type { IAttachementsMasterParams } from '../attachements-master';
import { attachementsMaster } from '../attachements-master';

const add = jest.fn();
const getBuffer = jest.fn();
const paramsUploadFile = jest.fn();

describe('attachementsMaster', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const attachmentsMethods = {
    toggleItem: jest.fn(),
    set: jest.fn(),
    setItem: jest.fn(),
    add,
    remove: jest.fn(),
    removeByIndex: jest.fn(),
    mergeToItem: jest.fn(),
  };

  const params: IAttachementsMasterParams<any> = {
    attachmentsMethods,
    bufferMethods: attachmentsMethods,
    getAttachments: jest.fn(() => [{ file: { id: 1 }, accessToken: '123' }]),
    getBuffer: getBuffer.mockImplementation(() => [
      {
        id: 1,
        name: '123',
        size: 321,
        isLoading: false,
        disabled: false,
        error: 'string',
        file: {
          name: 'name',
          type: 'type',
          size: 123,
          lastModified: 31,
          arrayBuffer: jest.fn(),
          slice: jest.fn(),
          stream: jest.fn(),
          text: jest.fn(),
        },
        accessToken: '123',
      },
    ]),
    downloadFile: jest.fn(),
    uploadFile: paramsUploadFile,
    transformer: jest.fn((_input: any) => ({
      id: 1,
      name: '123',
      size: 321,
      isLoading: false,
      disabled: false,
      error: 'string',
      file: {
        name: 'name',
        type: 'type',
        size: 123,
        lastModified: 31,
        arrayBuffer: jest.fn(),
        slice: jest.fn(),
        stream: jest.fn(),
        text: jest.fn(),
      },
      accessToken: '123',
    })),
    maxSize: 555,
  };

  it('вызов getMaxSize метода', () => {
    const { getMaxSize } = attachementsMaster(params);

    expect(getMaxSize()).toBe(555);
  });

  it('ошибка maxSize в uploadFile', () => {
    const { uploadFile } = attachementsMaster(params);

    const args = [
      {
        arrayBuffer: jest.fn(),
        slice: jest.fn(),
        stream: jest.fn(),
        text: jest.fn(),
        lastModified: 31,
        name: 'name',
        size: 555,
        type: 'type',
      },
    ];

    uploadFile(args);

    expect(params.uploadFile).toHaveBeenCalledTimes(0);
    expect(getBuffer).toHaveBeenCalledTimes(3);
  });
});
