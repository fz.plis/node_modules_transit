import { getDictionaryService } from '../default-dictionary-service';
import { metadataToRequestParams } from '../dictionary-service-helper';
import { request } from '../request/request';

jest.mock('../request/request', () => ({
  request: jest.fn(() => Promise.resolve()),
}));

describe('getDictionaryService', () => {
  const url = 'http://google.com';
  const { getList, getCounter, create, get } = getDictionaryService(url);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('getList', () => {
    const metaData = {
      pageSize: 20,
      offset: 0,
    };

    void getList(metaData);

    expect(request).toHaveBeenLastCalledWith({
      url: `${url}/get-page`,
      method: 'POST',
      data: metadataToRequestParams(metaData),
    });
  });

  it('getCounter', () => {
    const metaData = {
      pageSize: 20,
      offset: 0,
    };

    const category = 'category';

    void getCounter({ category, ...metaData });

    expect(request).toHaveBeenLastCalledWith({
      url: `${url}/category-count`,
      method: 'POST',
      data: metadataToRequestParams(metaData),
    });
  });
  it('create', async () => {
    const dataObj = {
      id: '1',
      prop: '123',
    };

    (request as jest.Mock).mockImplementation(data => Promise.resolve({ data: { ...data } }));

    const data = await create(dataObj);

    expect(data).toStrictEqual({ prop: '123' });
  });
  it('create выкидывает ошибку', async () => {
    (request as jest.Mock).mockImplementation(data => Promise.resolve({ data: { ...data, errorInfo: 'error' } }));

    const dataObj = {
      id: '1',
      prop: '123',
    };

    expect.assertions(1);

    await create(dataObj).catch(data => {
      expect(data).toBe('error');
    });
  });
  it('get выкидывает ошибку', async () => {
    (request as jest.Mock).mockImplementation(data => Promise.resolve({ data: { ...data, errorInfo: 'error' } }));

    expect.assertions(1);

    await get('123').catch(e => {
      expect(e).toStrictEqual({
        response: {
          data: {
            errorInfo: 'error',
          },
        },
      });
    });
  });
});
