import type { IAppConfig, ICryptoModule } from '../interfaces';

/**
 * Проверка используемой версии плагина.
 *
 * @param version Версия плагина.
 * @param minVersion Минимальная версия плагина.
 */
export const canUseCryptoPlugin = (version: string | null, minVersion?: string) => {
  let comparerResult = false;

  if (!minVersion) {
    return true;
  }

  if (!version) {
    return false;
  }

  if (version === minVersion) {
    return true;
  }

  const currentPluginVersion = version.split('.');
  const minPluginVersion = minVersion.split('.');

  for (const [i, element] of currentPluginVersion.entries()) {
    if (Number(element) > Number(minPluginVersion[i] || 0)) {
      comparerResult = true;
      break;
    }
  }

  return comparerResult;
};

/** Проверка актуальности версии криптомодуля по предоставленному инстансу. */
export const isOutdatedCryptoModule = async (cryptoModule: ICryptoModule, pluginsConfig: IAppConfig['cryptoPlugins']) => {
  const { bss: currentBssVersion, sft: currentSftVersion } = await cryptoModule.getAvailableCryptoModulesVersion();
  const {
    BSS: { minVersion: minBssVersion },
    SFT: { minVersion: minSftVersion },
  } = pluginsConfig;

  // если в ответах двух методов не получена версия плагина, то считаем устаревшим или отсутствующим
  if (!currentBssVersion && !currentSftVersion) {
    return true;
  }

  const isSftOutdated = Boolean(currentSftVersion) && !canUseCryptoPlugin(currentSftVersion!, minSftVersion);
  const isBssOutdated = Boolean(currentBssVersion) && !canUseCryptoPlugin(currentBssVersion!, minBssVersion);

  // если версия одного из плагинов устарела, считаем устаревшим или отсутствующим
  return isSftOutdated || isBssOutdated;
};
