interface IEventParams {
  bubbles?: boolean;
  cancelable?: boolean;
  detail?: any;
}

export const CustomEvent = (((type: string, defaultParams: IEventParams) => {
  const event: CustomEvent = document.createEvent('CustomEvent');

  const params = { bubbles: false, cancelable: false, detail: undefined, ...defaultParams };

  event.initCustomEvent(type, params.bubbles, params.cancelable, params.detail);

  return event;
}) as unknown) as new (type: string, eventParams: IEventParams) => CustomEvent<unknown>;
