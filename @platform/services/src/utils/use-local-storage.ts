import { useEffect, useState } from 'react';
import { useEventListener, isIe, isSafari } from '@platform/ui';
import { eventEmitter } from './event-emitter';

const ee = eventEmitter();

/**
 * Хук для сохранения произвольных данных в localStorage.
 *
 * @param key Ключ.
 * @param defaultValue Начальное значение.
 */
export const useLocalStorage = <T>(key: string, defaultValue: T): [T, React.Dispatch<React.SetStateAction<T>>] => {
  const stored = localStorage.getItem(key);
  const initialValue = stored ? JSON.parse(stored) : defaultValue;
  const [value, setValue] = useState(initialValue);

  // Событие 'storage' не поддерживается в IE и Safari см. https://developer.mozilla.org/en-US/docs/Web/API/Window/storage_event
  if (!isIe() && !isSafari()) {
    // подписываемся на изменения 'storage',
    // чтобы синхронизировать работу в нескольких вкладках браузера.

    // FIXME: Здесь баг, хук запускается в блоке if/else.
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEventListener('storage', (event: StorageEvent) => {
      if (event.key === key) {
        const newValue = JSON.parse(event.newValue!);

        if (newValue !== value) {
          setValue(newValue);
        }
      }
    });
  }

  // инициализация эвент эмиттера
  useEffect(() => {
    ee.on(key, setValue);

    return () => {
      // финализация эвент эмиттера
      ee.off(key, setValue);
    };
  }, [key]);

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));

    // триггерим значение, если оно изменилось
    // это позволяет сихронизировать стейт между множеством useLocalStorage в пределах одной вкладки.
    ee.trigger(key, value);
  }, [key, value]);

  return [value, setValue];
};
