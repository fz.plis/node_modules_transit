export const mimeTypeToExt = (mimeType: string) => {
  if (!mimeType || typeof mimeType !== 'string') {
    throw new Error('ext is required and have to be a string');
  }

  const ext = mimeType.toLowerCase();

  switch (ext) {
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
      return 'doc';
    case 'application/msword':
      return 'docx';
    case 'application/vnd.ms-excel':
      return 'xls';
    case 'vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      return 'xlsx';
    case 'text/html':
      return 'html';
    case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
      return 'pptx';
    case 'application/pdf':
      return 'pdf';
    default:
      return 'zip';
  }
};
