/**
 * Достигнуто максимальное число попыток.
 */
export const MAX_NUMBER_OF_ATTEMPS_REACHED = 'MAX_NUMBER_OF_ATTEMPS_REACHED';

export const polling = <TJobResult, TArgs extends any[]>(
  job: (...args: TArgs) => Promise<TJobResult>,
  checker: (result: TJobResult) => boolean,
  interval: number,
  maxCount: number = 0
) => {
  let count = 0;

  const pollingFunc = (...args: TArgs): Promise<TJobResult> =>
    new Promise((resolve, reject) => {
      count = count + 1;

      job(...args)
        .then(result => {
          if (checker(result)) {
            resolve(result);
          } else {
            if (maxCount > 0 && count >= maxCount) {
              throw new Error(MAX_NUMBER_OF_ATTEMPS_REACHED);
            }

            setTimeout(() => {
              pollingFunc(...args).then(resolve, reject);
            }, interval);
          }
        })
        .catch(reject);
    });

  return pollingFunc;
};
