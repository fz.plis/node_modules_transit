import { conditions } from '@platform/core';
import type { IMetaData, ICollectionResponse, ISearchByName } from '../interfaces';
import { metadataToRequestParams } from './dictionary-service-helper';
import { request } from './request';

export const getDictionaryService = <TRow extends { id: string }, TCreate = TRow>(url: string) => {
  const getList = (metaData: IMetaData): Promise<ICollectionResponse<TRow>> =>
    request({
      url: `${url}/get-page`,
      method: 'POST',
      data: metadataToRequestParams(metaData),
    }).then(r => ({
      data: r.data.data.page.list,
      total: r.data.data.page.total,
    }));

  return {
    getList,
    getCounter: ({ category, ...metaData }: IMetaData) =>
      request({
        url: `${url}/category-count`,
        method: 'POST',
        data: metadataToRequestParams(metaData),
      }).then(d => d.data.data.data),
    get: (id: string): Promise<TRow> =>
      request({
        url: `${url}/${id}`,
      }).then(d => {
        if (d.data.errorInfo) {
          throw {
            response: {
              data: {
                errorInfo: d.data.errorInfo,
              },
            },
          };
        }

        return d.data.data.data as TRow;
      }),
    create: (payload: TCreate) => {
      const { id, ...dataToSend } = payload as any;

      return request({
        url,
        method: 'POST',
        data: { data: dataToSend },
      }).then(r => {
        const { data, errorInfo } = r.data;

        if (errorInfo) {
          throw errorInfo;
        }

        return data.data;
      });
    },
    searchByName: <T extends keyof TRow>(params: ISearchByName, fieldName: T) =>
      getList({
        pageSize: params.pageSize!,
        offset: params.offset!,
        filters: {
          name: {
            fieldName,
            value: params.search,
            condition: conditions.contains,
          },
        },
      }),
  };
};
