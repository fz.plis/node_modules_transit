/**
 * Возвращает массив всех результатов при сопоставлении строки с регулярным выражением.
 *
 * @param val Строка для поиска.
 * @param regexp Регулярное выражение.
 */
export const matchAll = (val: string, regexp: RegExp): RegExpMatchArray[] | null => {
  const matches: RegExpMatchArray[] = [];

  val.replace(regexp, (...args) => {
    const arr: RegExpMatchArray = [...args];
    const extras = arr.splice(-2);

    arr.index = +extras[0];
    arr.input = extras[1];
    matches.push(args);

    return '';
  });

  return matches.length > 0 ? matches : null;
};

/**
 * Усекает строку, если её длина больше параметра maxlength,
 * усечённая строка будет содержать maxlength-1 первых символов исходной строки плюс "...".
 *
 * @param str - Строка которую необходимо усечь.
 * @param maxlength - Все строки длина которых выше этого параметра, будут усекаться.
 *
 * @example
 * // returns 'abc...'
 * truncate('abcdef', 4);.
 */
export const truncateWithEllipsis = (str: string, maxlength: number) =>
  str.length > maxlength ? `${str.slice(0, maxlength - 1)}...` : str;
