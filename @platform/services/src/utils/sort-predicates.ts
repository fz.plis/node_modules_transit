/**
 * Используется как предикат в Array.sort.
 * После сортировки массив объектов будет отсортирован по возрастанию поля label,
 * в лексикографическом порядке.
 *
 * @param a - Объект для сравнения.
 * @param b - Объект для сравнения.
 *
 * @example
 * // returns [{label: 'Исполнен'}, {label: 'Ошибка'} ]
 * [{label: 'Ошибка'}, {label: 'Исполнен'} ].sort(byLabel)
 */
export const byLabel = <T extends { label: string }>(a: T, b: T): -1 | 0 | 1 => {
  if (a.label < b.label) {
    return -1;
  }

  if (a.label > b.label) {
    return 1;
  }

  return 0;
};
