import { conditions } from '@platform/core';
import type { IMetaData, ICollectionResponse, ISearchByName } from '../interfaces';
import { metadataToRequestParams } from './dictionary-service-helper';
import { request } from './request';

export const getNewDictionaryService = <TRow>(url: string) => {
  const getList = (metaData: IMetaData): Promise<ICollectionResponse<TRow>> =>
    request({
      url: `${url}/get-page`,
      method: 'POST',
      data: metadataToRequestParams(metaData),
    }).then(r => ({
      data: r.data.page,
      total: r.data.size,
    }));

  return {
    getList,
    getCounter: (metaData: IMetaData) => {
      // Status category теперь один элемент в массиве and. выдрать наверно надо
      const { statusCategory, ...filter } = metadataToRequestParams(metaData).params.filter;

      return request({
        url: `${url}/category`,
        method: 'POST',
        data: { params: { filter } },
      }).then(r => r.data.data);
    },
    get: async (id: string): Promise<TRow> =>
      request({
        url: `${url}/${id}`,
        method: 'GET',
        data: { id },
      }).then(d => {
        const { data, code, message } = d.data;

        if (code) {
          throw {
            response: {
              data: {
                errorInfo: {
                  code,
                  message,
                },
              },
            },
          };
        }

        return data;
      }),
    getByIds: async (id: string, clientId: string): Promise<TRow> =>
      request({
        url: `${url}/${id}`,
        method: 'GET',
        data: { id, clientId },
      }).then(d => d.data.data.data),
    search: async (metaData: IMetaData): Promise<ICollectionResponse<TRow>> =>
      request({
        url: `${url}/ContextSearch`,
        method: 'POST',
        data: metadataToRequestParams(metaData),
      }).then(r => ({
        data: r.data.data.page.list,
        total: r.data.data.page.total,
      })),
    searchByName: <T extends keyof TRow>(params: ISearchByName, fieldName: T) =>
      getList({
        pageSize: params.pageSize!,
        offset: params.offset!,
        filters: {
          name: {
            fieldName,
            value: params.search,
            condition: conditions.contains,
          },
        },
      }),
  };
};
