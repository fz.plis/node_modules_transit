import { CONFERENCE_STORAGE_KEY, DONT_SHOW_DBO_INFORMATION_STORAGE_KEY } from '../constants';

/**
 * Ключи, которые не требуется удалять.
 */
const EXCLUDED_STORAGE_KEYS = ['GLOBAL_STORAGE_KEY', CONFERENCE_STORAGE_KEY, DONT_SHOW_DBO_INFORMATION_STORAGE_KEY];

/**
 * Метод очищения localStorage.
 *
 * @param completelyClear Флаг принудительного очищения всего хранилища.
 */
export const clearLocalStorage = (completelyClear: boolean = false) => {
  if (completelyClear) {
    localStorage.clear();
  } else {
    const excludedData: Record<string, string> = EXCLUDED_STORAGE_KEYS.reduce(
      (acc, key) => ({ ...acc, [key]: localStorage.getItem(key) }),
      {}
    );

    localStorage.clear();

    EXCLUDED_STORAGE_KEYS.forEach(key => localStorage.setItem(key, excludedData[key]));
  }
};
