# Генератор констант (experimental)

Генератор констант предоставляет api для краткой записи "типизированных" констант с использованием проверки типов typescript.
Встречаются случаи, когда требуется создать константу с ключами в виде значений enum и каким либо значением по этому ключу.
При обычной объявлении запись получается громоздкой.

Хелпер **constant** позволяет сократить дублируемый код и добавить проверки типов.

Например
```js
// enum статусов документа
export declare enum REG_DOC_STATUS {
  NONE = "NONE",
  DRAFT = "DRAFT",
  COMPLETED = "COMPLETED",
  DELETED = "DELETED",
  FORMED = "FORMED",
  DOCS_LOAD = "DOCS_LOAD",
  IN_PROCESS = "IN_PROCESS",
  READY_FOR_PROCESSING = "READY_FOR_PROCESSING",
  DATA_ERROR = "DATA_ERROR",
  EXPORTED_TO_ESK = "EXPORTED_TO_ESK",
  REPLACING_DOCS = "REPLACING_DOCS",
  APPROVED = "APPROVED",
  REFUSED = "REFUSED",
  REJECTED = "REJECTED",
  EXECUTED = "EXECUTED"
}

// объявим константу-перечисление индикаторов статуса документа для скроллера
// привычным способом
 export const REG_DOC_CLIENT_STATUS_BY_COLOR: { [P in keyof typeof REG_DOC_STATUS]: DOCUMENT_STATUS_TYPE } = {
  [REG_DOC_STATUS.DRAFT]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.DELETED]: DOCUMENT_STATUS_TYPE.ERROR,
  [REG_DOC_STATUS.FORMED]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.COMPLETED]: DOCUMENT_STATUS_TYPE.SUCCESS,
  [REG_DOC_STATUS.DOCS_LOAD]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.IN_PROCESS]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.READY_FOR_PROCESSING]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.DATA_ERROR]: DOCUMENT_STATUS_TYPE.ERROR,
  [REG_DOC_STATUS.EXPORTED_TO_ESK]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.REPLACING_DOCS]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.APPROVED]: DOCUMENT_STATUS_TYPE.INPROGRESS,
  [REG_DOC_STATUS.REFUSED]: DOCUMENT_STATUS_TYPE.ERROR,
  [REG_DOC_STATUS.REJECTED]: DOCUMENT_STATUS_TYPE.ERROR,
  [REG_DOC_STATUS.EXECUTED]: DOCUMENT_STATUS_TYPE.SUCCESS,
  [REG_DOC_STATUS.NONE]: DOCUMENT_STATUS_TYPE.INPROGRESS,
}

// и и использованием хелпера
export const NEW_REG_DOC_CLIENT_STATUS_BY_COLOR = constant(REG_DOC_STATUS)
  .of(DOCUMENT_STATUS_TYPE.INPROGRESS)
  .set(['DELETED', 'DATA_ERROR', 'REFUSED', 'REJECTED'], DOCUMENT_STATUS_TYPE.ERROR)
  .set(['COMPLETED', 'EXECUTED'], DOCUMENT_STATUS_TYPE.SUCCESS)
  .create()

// обе константы будут иметь тип

const REG_DOC_CLIENT_STATUS_BY_COLOR: {
  readonly NONE: DOCUMENT_STATUS_TYPE;
  readonly DRAFT: DOCUMENT_STATUS_TYPE;
  readonly COMPLETED: DOCUMENT_STATUS_TYPE;
  readonly DELETED: DOCUMENT_STATUS_TYPE;
  readonly FORMED: DOCUMENT_STATUS_TYPE;
  ... 9 more ...;
  readonly EXECUTED: DOCUMENT_STATUS_TYPE;
}
```

Видно что запись получается значительно короче.

## Хелпер constant

В дальнейшем в документе будет использоваться следующий интерфейс

```js
enum TEST_STATUS {
  ONE = 'ONE',
  TWO = 'TWO',
  THREE = 'THREE',
}
```

Хелпер имеет следущий тип

```js
export interface IConstantFactory<T, R, U> {
  set: <K extends keyof U>(key: K | K[], val?: R) => IConstantFactory<T, R, Omit<U, K>>
  create: () => { [P in keyof T]: R }
}

export interface IConstantHelper<T> {
  of: <TVal>(def?: TVal) => IConstantFactory<T, TVal, T>
}

export declare constant = <T extends any>(initialVal?: T): IConstantHelper<T> 
```

На вход **constant** принимает либо тип, либо объект. При передаче объекта в качестве параметра в дальнейшем возможна начальная инициалиция значений по ключам этого объекта. В результате вернется объект с единственной функцией **of**, предназначенной для определения типа возвращаемого значения.

Далее следует указать тип возвращаемого значения и значение по умолчанию. Если в **constant** был передан объект, то по всем ключам объекта будут проинициализированы переденные значения.

После вызова **of** вернется объект-фабрика для инициализации значений по ключам константы.

Объект-фабрика имеет функции **set** для установки значений по ключам исходного типа и **create** для генерации итогового объекта-константы.

Пример

```js
const testConst = constant<typeof TEST_STATUS>() // инициализируем фабрику с типом ключей TEST_STATUS
  .of<string>()  // указываем тип string для значений по ключам
  .set('ONE', 'one')  // указываем сами значения
  .set('TWO', 'two')
  .set('THREE', 'three')
  .create()  // возвращаем объект-константу

// итоговый тип оюъекта
const testConst: {
    readonly ONE: string;
    readonly TWO: string;
    readonly THREE: string;
}
```

Такой подход дает дополнительные возможности проверки типов, например ошибочное добавление повторящихся ключей, передачу в качестве значения, отличного от ранее указанного типа, добавление "лишних" ключей

Например

```js
const testConst = constant<typeof TEST_STATUS>() 
  .of<string>() 
  .set('ONE', 'one')  
  .set('TWO', 'two')
  .set('THREE', 'three')
  .create()  // ok

const testConst = constant<typeof TEST_STATUS>() 
  .of<string>() 
  .set('ONE', 'one')  
  .set('TWO', 'two')
  .set('ONE', 'three') // ts-error: Argument of type '"ONE"' is not assignable to parameter of type '"THREE" | "THREE"[]'
  .create() 

const testConst = constant<typeof TEST_STATUS>() 
  .of<string>() 
  .set('ONE', 'one')  
  .set('TWO', 'two')
  .set('ONE', 4) // ts-error: Argument of type '4' is not assignable to parameter of type 'string'
  .create() 

const testConst = constant<typeof TEST_STATUS>()
  .of<string>()
  .set('ONE', 'one')
  .set('TWO', 'two')
  .set('THREE', 'three')
  .set('')   // ts-error: Argument of type '""' is not assignable to parameter of type 'never[]'
  .create()
```

Также при каждом последующем вызове **set** умельшается число доступных вариантов значений ключей

```js
const testConst = constant<typeof TEST_STATUS>() 
  .of<string>() 
  .set('ONE', 'one')  //  доступно ONE, TWO, THREE
  .set('TWO', 'two') //  доступно TWO, THREE
  .set('THREE', 'three') // THREE
  .create() 
```

Для большего удобства можно передавать объекты в **constant**.
Так следует делать, если требуется проинициализировать с помощью функции **of** начальные значения. Переопределить указанные значения при этом можно с помощью **set**. Так же можно не указывать передаваемое в **set** значение, при этом будут использовано значение по умолчанию, переданное в **of**

Нужно учесть, что если в **constant** был передан тип, то значения по умолчанию не будут проинициализированы

Например

```js
const testConst = constant(TEST_STATUS)
  .of('hello')
  .set('ONE')
  .set('TWO')
  .set('THREE', 'three')
  .create()

console.log(testConst.ONE) // hello
console.log(testConst.TWO) // hello
console.log(testConst.THREE) // three

// используем тип
const testConst = constant<typeof TEST_STATUS>()
  .of('hello')
  .set('ONE')
  .set('TWO')
  .set('THREE', 'three')
  .create()

console.log(testConst.ONE) // undefined
console.log(testConst.TWO) // undefined
console.log(testConst.THREE) // three
```

В **set** также можно передавать массивы ключей, при этом так же в списке будут доступны только не использованные ранее ключи.

```js
const testConst = constant(TEST_STATUS)
  .of('hello')
  .set(['ONE', 'TWO'])
  .set('THREE', 'three') // доступно THREE 
  .create()

console.log(testConst.ONE) // hello
console.log(testConst.TWO) // hello
console.log(testConst.THREE) // three
```