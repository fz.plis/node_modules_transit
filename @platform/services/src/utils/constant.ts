export interface IConstantFactory<T, R, U> {
  set<K extends keyof U>(key: K | K[], val?: R): IConstantFactory<T, R, Omit<U, K>>;
  create(): { [P in keyof T]: R };
}

export interface IConstantHelper<T> {
  of<TVal>(def?: TVal): IConstantFactory<T, TVal, T>;
}

export const constant = <T extends Record<string, unknown>>(initialVal?: T): IConstantHelper<T> => {
  const result: any = {};

  const factory = <U, TVal>(def: TVal): IConstantFactory<T, TVal, U> => ({
    set: <K extends keyof U>(key: K | K[], val: TVal = def) => {
      const keys = Array.isArray(key) ? key : [key];

      keys.forEach(x => (result[x] = val));

      return factory(def);
    },
    create: () => result,
  });

  return {
    of: <TResult>(def?: TResult) => {
      if (initialVal) {
        Object.keys(initialVal).forEach(key => (result[key] = (initialVal as any)[key]));
      }

      return factory<T, TResult>(def!);
    },
  };
};
