import type { IFilters, OperatorFilterInfo } from '@platform/core';
import { conditions } from '@platform/core';
import type { IMetaData, ISortSettings, SORT_DIRECTION } from '../interfaces';

export const EMPTY_FILTER = '';

const getPaging = (offset: number, pageSize: number) => ({
  offset,
  limit: pageSize,
});

const getSort = (sort?: Record<string, SORT_DIRECTION>) => {
  if (sort) {
    const field = Object.keys(sort)[0];

    if (field) {
      return {
        field,
        direction: (sort[field] || '').toUpperCase(),
      };
    }
  }
};

const getMultiSort = (multiSort?: ISortSettings) =>
  multiSort ? Object.entries(multiSort).map(([field, direction]) => ({ field, direction: direction.toUpperCase() })) : undefined;

const getFilter = (filters: IFilters | undefined) => {
  if (filters) {
    return Object.keys(filters).reduce((acc: any, curr: any) => {
      if ((EMPTY_FILTER === filters[curr].value || undefined === filters[curr].value) && filters[curr].condition !== conditions.notEq) {
        return acc;
      }

      const curFilters = acc[(filters[curr] as OperatorFilterInfo).fieldName] || {};

      acc[(filters[curr] as OperatorFilterInfo).fieldName] = {
        ...curFilters,
        [filters[curr].condition || 'eq']: filters[curr].value,
      };

      return acc;
    }, {});
  }

  return {};
};

export const metadataToRequestParams = (meta: IMetaData) => {
  const { pageSize, offset, sort, filters, category, orgId, branchId, userId, textQuery, multiSort, search } = meta;

  const filtersObj = getFilter(filters);

  if (category) {
    filtersObj.statusCategory = category;
  }

  if (textQuery) {
    filtersObj.textQuery = textQuery;
  }

  if (orgId) {
    filtersObj.clientUuid = { eq: orgId };
  }

  if (userId) {
    filtersObj.userId = { eq: userId };
  }

  if (branchId) {
    filtersObj.branchId = branchId;
  }

  return {
    params: {
      paging: getPaging(offset, pageSize),
      sort: getSort(sort),
      multiSort: getMultiSort(multiSort),
      str: search === undefined ? undefined : search,
      filter: Object.keys(filtersObj).length > 0 ? filtersObj : undefined,
    },
  };
};
