import type { IActionConfig, IExecuteResult } from '@platform/core';
import { dialog } from '@platform/ui';

export const chainFatalHandler = (...configs: Array<IActionConfig<any, any>['fatalHandler']>) => (
  context: any,
  result: IExecuteResult<any, any>
) => {
  void configs.reduce<Promise<any>>(
    (chain, handler = () => {}) => chain.catch(fatalResult => handler(context, fatalResult)),
    Promise.reject(result)
  );
};

// eslint-disable-next-line @eco/no-missing-localization
export const notAvailableInDemo = () => dialog.showAlert('Недоступно в демо режиме');
