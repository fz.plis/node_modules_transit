import { AUTH_ERROR_MAP, AUTH_REQUEST_CONFIG } from '../constants';
import type {
  ILoginPasswordRequest,
  ILoginApiRequest,
  ILoginCertificateRequest,
  ILoginOtpTotpRequest,
  IActionWithAuth,
  IActionWebInfo,
  IAuthoritiesObject,
} from '../interfaces';
import { ERROR, GRANT_TYPE } from '../interfaces';
import { request } from './request';
import { parseToken } from './token-helper';
import { getToken } from './token-storage';

export const createTransformObject = (keyTransformer: (key: string) => string) => <
  T extends Record<string, any>,
  TResult extends Record<string, any>
>(
  obj: T
) =>
  Object.keys(obj).reduce((result: any, key) => {
    result[keyTransformer(key)] = obj[key];

    return result;
  }, {}) as TResult;

const toLowerCamelCase = (val: string): string => val.replace(/_(.)/, (_, symb: string) => symb.toUpperCase());

export const fromLowerCamelCase = (val: string): string => val.replace(/([A-Z])/, str => `_${str.toLowerCase()}`);

export const transformToApi = createTransformObject(fromLowerCamelCase);
export const transformFromApi = createTransformObject(toLowerCamelCase);

export const transformError = (err: any) => {
  if (!err.response) {
    return Promise.reject(err);
  }

  if (err.response.status === ERROR.BAD_REQUEST) {
    const code = AUTH_ERROR_MAP[err.response.data.error];

    return Promise.reject({
      response: {
        data: {
          errorInfo: {
            code: code || err.response.status,
          },
          data: transformFromApi(err.response.data),
        },
      },
    });
  }

  return Promise.reject(err);
};

export const createAuthRequestFunc = <TRequest>(url: string) => <TData, TResult>(transform: (data: TData) => TRequest) => (
  data: TData,
  redirectUri?: string
): Promise<TResult> => {
  const d: any = transform(data);

  if (redirectUri) {
    d.redirect_uri = redirectUri;
  }

  return (
    request({
      ...AUTH_REQUEST_CONFIG,
      url,
      data: d,
    })
      .then(r => r.data)
      /* // DEBUG ERRORS
      .then(r => {
        throw {
          response: { status: ERROR.BAD_REQUEST, data: { error: AUTH_ERROR.TOKEN_ATTEMPS_EXHAUSTED } },
        }
        return r.data
      }) */
      .catch(transformError)
  );
};

export const transformPassword = ({ password, username, userEnvironment }: ILoginPasswordRequest): ILoginApiRequest => ({
  password,
  username,
  grant_type: GRANT_TYPE.PASSWORD,
  user_environment: userEnvironment,
});

export const transformCertificate = ({
  certData,
  signData,
  token,
  cryptoPlugin,
  userEnvironment,
  cryptoPluginVersion,
}: ILoginCertificateRequest): ILoginApiRequest => ({
  grant_type: GRANT_TYPE.MFA_CERTIFICATE,
  mfa_token: token,
  mfa_certificate: certData,
  mfa_signed_data: signData,
  crypto_plugin: cryptoPlugin,
  crypto_plugin_version: cryptoPluginVersion,
  user_environment: userEnvironment,
});

export const transformOtpTotp = ({ token, code, mfaType, userEnvironment }: ILoginOtpTotpRequest): ILoginApiRequest => ({
  grant_type: mfaType,
  mfa_token: token,
  mfa_password: code,
  user_environment: userEnvironment,
});

/**
 * Получить список доступных текущему пользователю действий.
 *
 * @deprecated Используйте const { getAvailableActions } = useAuth() или const { getAvailableActions } = getAuthValue().
 */
export const getAvailableActions = (actions: IActionWithAuth[]): Array<IActionWebInfo<any, any>> => {
  const token = getToken();

  if (token) {
    const { authorities: userAuthorities } = parseToken(token);

    return actions.reduce<Array<IActionWebInfo<any, any>>>((result, curr) => {
      const { authorities, ...rest } = curr;

      if (authorities?.every(x => userAuthorities.includes(x))) {
        result.push(rest);
      }

      return result;
    }, []);
  }

  return [];
};

/**
 * Получить список доступных текущему пользователю элементов.
 *
 * @deprecated Используйте const { getAvailableItems } = useAuth() или const { getAvailableItems } = getAuthValue().
 */
export const getAvailableItems = <T>(items: Array<IAuthoritiesObject & T>) => {
  const token = getToken();
  const { authorities: userAuthorities } = parseToken(token!);

  return items.reduce<T[]>((acc, val) => {
    if (val.authorities?.every(x => userAuthorities.includes(x))) {
      const { authorities, ...item } = val;

      return [...acc, item] as T[];
    }

    return acc;
  }, []);
};
