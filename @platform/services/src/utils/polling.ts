import type { IPollingResponse } from '../interfaces';

const DEFAULT_POLLING_INTERVAL = 10;

const interval = (job: () => Promise<void>, time = DEFAULT_POLLING_INTERVAL * 1000) => {
  setTimeout(() => {
    void job();
  }, time);
};

export const pollingRequest = <T extends IPollingResponse>(fetcher: () => Promise<T>, setter: (val: any) => void): Promise<void> =>
  fetcher()
    .then(data => {
      setter(data);

      interval(() => pollingRequest(fetcher, setter), data.pollingInterval * 1000);
    })
    .catch(() => interval(() => pollingRequest(fetcher, setter), DEFAULT_POLLING_INTERVAL * 1000));
