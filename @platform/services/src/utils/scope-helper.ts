/**
 * Область приложения.
 */
export enum SCOPE {
  /**
   * Пользователь Клиента.
   */
  CLIENT = 'CLIENT',
  /**
   * Пользователь Банка.
   */
  ADMIN = 'ADMIN',
}

// Устанавливаем клиентский scope по-умолчанию.
let scope = SCOPE.CLIENT;

/**
 * Функция установки области приложения.
 *
 * @param newScope Область приложения.
 */
export const setScope = (newScope: SCOPE): void => {
  scope = newScope;
};

/**
 * Функция проверки области приложения.
 */
export const isClientScope = (): boolean => scope === SCOPE.CLIENT;
