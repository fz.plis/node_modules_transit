import type { CityVisitingManagerDto, IServerResp } from '../interfaces';
import { request } from './request';

/**
 * Справочник городов выездных менеджеров.
 */
export const getCityVisitingManagerService = (url: string) => ({
  /**
   * Получить данные из справочника городов выездных менеджеров для продукта ОНБ для переданного подразделения.
   *
   * @description Привилегии: hasAuthority('BRANCH_GET_PAGE').
   */
  async findDocPackZone(branchId: string): Promise<IServerResp<CityVisitingManagerDto>> {
    const { data: response } = await request({
      url: `${url}/docpack-zone/${branchId}`,
      method: 'GET',
    });

    return { data: response.data, errorInfo: { code: response.code, message: response.message } };
  },
});
