import React, { useCallback, useEffect, useMemo } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { guid } from '@platform/tools/istore';
import type { IAppSidebarOptionProps, IButtonAction } from '@platform/ui';
import {
  AppLayout as CommonLayout,
  AppSidebar as Sidebar,
  AppTopline as Topline,
  Box,
  BUTTON,
  dialog,
  Gap,
  Horizon,
  Icons,
  LoaderOverlay,
  MENU_ITEM_TYPES,
  Notification,
  Sticky,
  SYSTEM_NOTIFICATION_TYPE,
  Typography,
} from '@platform/ui';
import {
  callStreamAction,
  COMMON_STREAM_URL,
  errorHandler,
  FromStream,
  getFio,
  NOTIFICATIONS_AUTHORITIES,
  parseUrlSearch,
  useAppConfig,
  useRedirect,
} from '../../..';
import { FatalErrorBoundary, FatalErrorContent } from '../../../components';
import { RESERVE_ACCOUNT_AUTHORITIES, OPEN_ACCOUNT_AUTHORITIES, REG_DOC_AUTHORITIES } from '../../../constants/client';
import { useAuth } from '../../../core/auth';
import { useUnit } from '../../../domains';
import type { IMenuItem } from '../../../interfaces';
import { USER_CONTEXT_TYPE } from '../../../interfaces';
import { locale } from '../../../localization';
import { getBaseUrl } from '../../../utils';
import { getSidebarOptions } from '../../../utils/get-sidebar-options';
import { checkGuards, segmentGuard } from '../../components/route-guards';
import { mainDomain, notificationDomain, selectedOrganizationId } from '../../domains';
import { newOpenAccountService } from '../../services';
import { useUser } from '../../user';

export interface ILayoutProps {
  onChangeSelectedOrgId?(val: string): void;
  disableReserveButton?: boolean;
  disableOpenAccountButton?: boolean;
  disableRegDocButton?: boolean;
  disableOfferButton?: boolean;
  hideImportantNotifications?: boolean;
  beforeLogout?(): Promise<boolean>;
  beforeOrgChange?(): Promise<boolean>;
  onClickExit?(): void;
}

export const MainLayout: React.FC<ILayoutProps> = ({
  children,
  onChangeSelectedOrgId = () => {},
  disableReserveButton = false,
  disableOpenAccountButton = false,
  disableRegDocButton = false,
  beforeOrgChange = () => Promise.resolve(true),
  beforeLogout = () => Promise.resolve(true),
  hideImportantNotifications = false,
}) => {
  const { config, reload } = useAppConfig();
  const { push } = useHistory();
  const { path } = useRouteMatch();
  const baseUrl = getBaseUrl(path);
  const prevOrgId = useUnit(selectedOrganizationId);
  const { importantNotificationMessage, showImportantNotification, notificationCount, importantNotificationUrl } = useUnit(
    notificationDomain
  );

  const { fatalErr, hasAuthority, logout, hasAllowedContextType, token, redirectToLogin } = useAuth();
  const { userOrganizations, user } = useUser();

  useEffect(() => {
    if (!token) {
      redirectToLogin();
    }
  }, [token, redirectToLogin]);

  const notificationIsOpened = useMemo(() => !hideImportantNotifications && showImportantNotification, [
    hideImportantNotifications,
    showImportantNotification,
  ]);

  const organizationsOptions = useMemo(() => {
    const options = userOrganizations
      .filter(x => x)
      .map(x => ({
        value: x.id,
        label: x.shortName || x.fullName,
      }));

    let organizationsOption: any[] = [];

    if (options.length === 0) {
      organizationsOption = [{ value: '', label: locale.action.OrganizationNotDefined.label }];
    }

    if (options.length === 1) {
      organizationsOption = options;
    }

    if (options.length > 1) {
      organizationsOption = [{ value: '', label: locale.action.organizations.label }, ...options];
    }

    return organizationsOption;
  }, [userOrganizations]);

  const menuItems = useMemo(() => {
    if (!config || !user) {
      return [];
    }

    const checkForAuthority = ({ authorities = [], options, segments = [], allowedContexts }: IMenuItem): boolean => {
      if (typeof allowedContexts !== 'undefined' && !hasAllowedContextType(allowedContexts)) {
        return false;
      }

      // проверяем привилегии
      if (authorities.length > 0 && !hasAuthority(...authorities)) {
        return false;
      }

      // проверяем сегменты
      const [segmnetAvailable] = checkGuards({ segments }, segmentGuard);

      if (!segmnetAvailable) {
        return false;
      }

      // если пункт должен быть доступен только для ECO, проверяем, что у пользователя нет привелегии DBO
      // это костыльное решение следующей проблемы: пункты только для ECO, доступны так-же и DBO пользователям, потому, что они
      // имеют доступ к пунктам ECO и DBO, а мы хотим сделать так, чтобы пункты для ECO были доступны только ECO пользователям.
      if (allowedContexts?.length === 1 && allowedContexts[0] === USER_CONTEXT_TYPE.ECO) {
        return !hasAllowedContextType([USER_CONTEXT_TYPE.DBO]);
      }

      // если у юзера есть привилегии и сегменты, то аналогичную проверку проводим в дочерних
      if (options) {
        return options.some(checkForAuthority);
      }

      // все проверки пройдены
      return true;
    };

    const recursiveFilter = (items: IMenuItem[]): IMenuItem[] =>
      items.filter(checkForAuthority).map(item => (item.options?.length ? { ...item, options: recursiveFilter(item.options) } : item));

    return getSidebarOptions(recursiveFilter(config.menu));
  }, [config, user, hasAuthority, hasAllowedContextType]);

  const userName = useMemo(
    () =>
      user
        ? getFio({
            familyName: user.secondName,
            firstName: user.firstName,
            middleName: user.patronymic,
          })
        : '',
    [user]
  );

  const hasNotification = useMemo(() => hasAuthority(NOTIFICATIONS_AUTHORITIES.USER_NOTIFICATION_GET_PAGE), [hasAuthority]);

  const goToProfile = useRedirect(COMMON_STREAM_URL.PROFILE);

  const goToNotifications = useRedirect(COMMON_STREAM_URL.NOTIFICATIONS);

  const openFirstAccount = useRedirect(`${COMMON_STREAM_URL.REGDOC}/new`);

  const reserveAccount = useRedirect(`${COMMON_STREAM_URL.RESERVE_ACCOUNT}/new`);

  const openAccount = useCallback(async () => {
    try {
      const isNewO2 = await newOpenAccountService.checkIfNewO2IsAvailable();

      if (isNewO2) {
        push(`${COMMON_STREAM_URL.NEW_OPEN_ACCOUNT}/new`);
      } else {
        push(`${COMMON_STREAM_URL.OPEN_ACCOUNTS}/new`);
      }
    } catch (e) {
      push(`${COMMON_STREAM_URL.OPEN_ACCOUNTS}/new`);
    }
  }, [push]);

  const onClickOption = useCallback(
    ([url]: string[], { type, value }: IAppSidebarOptionProps) => {
      if (url === 'feedback') {
        return;
      }

      if (type === MENU_ITEM_TYPES.STREAM && value) {
        const [streamName, method] = value.split('/');
        const [methodName, methodParams] = method.split('?');
        const params = methodParams ? parseUrlSearch(`?${methodParams}`) : undefined;

        return callStreamAction(streamName, methodName, params);
      }

      if (url?.toLowerCase().startsWith('http')) {
        const params: any = {
          token,
        };

        const transformedUrl = url.replace(/{{(.*?)}}/g, (_, val) => {
          const p = params[val];

          if (typeof p === 'undefined') {
            return '';
          }

          return p;
        });

        window.open(transformedUrl, '_blank');
      } else {
        push(url, guid());
      }
    },
    [push, token]
  );

  const goToImportantNotifications = useCallback(() => {
    push(importantNotificationUrl);
  }, [push, importantNotificationUrl]);

  const showCloseConfirmDialog = useCallback(
    () =>
      new Promise<void>((resolve, reject) => {
        dialog.showConfirmation(locale.dialog.importantNotification.postponeInfo, resolve, {
          okButtonText: locale.labels.yes,
          cancelButtonText: locale.labels.no,
          onClose: reject,
        });
      }),
    []
  );

  const closeImportantNotifications = useCallback(
    id => async () => {
      const { postponeNotification, canPostpone, setShowImportantNotification } = mainDomain.methods.notificationDomain;

      try {
        await postponeNotification(id);
      } catch (e) {
        errorHandler()(e);
      }

      if (canPostpone()) {
        setShowImportantNotification(false);
      } else {
        try {
          await showCloseConfirmDialog();
          push(`${COMMON_STREAM_URL.NOTIFICATIONS}/important`);
        } catch (e) {
          setShowImportantNotification(false);
        }
      }
    },
    [showCloseConfirmDialog, push]
  );

  const changeOrgId = mainDomain.methods.selectedOrganizationId.set;

  const actions: IButtonAction[] = useMemo(() => {
    const buttonConfigs = [
      {
        label: locale.action.reserveAccount,
        onClick: reserveAccount,
        disabled: disableReserveButton,
        name: 'reserve',
        buttonType: BUTTON.PRIMARY,
        authorities: [RESERVE_ACCOUNT_AUTHORITIES.CREATE],
      },
      {
        label: locale.action.openAccountWithSign,
        onClick: openAccount,
        disabled: disableOpenAccountButton,
        name: 'open-account',
        buttonType: BUTTON.PRIMARY,
        authorities: [OPEN_ACCOUNT_AUTHORITIES.CREATE],
      },
      {
        label: locale.action.regDoc,
        onClick: openFirstAccount,
        disabled: disableRegDocButton,
        name: 'recDoc',
        buttonType: BUTTON.PRIMARY,
        authorities: [REG_DOC_AUTHORITIES.CREATE],
      },
    ];

    // Если юзер не авторизован, то пропускаем проверку привилегий, чтобы не получить робота.
    // Скрываем кнопку "Открыть счет" для пользователя ДБО-БСС
    if (!!user && !hasAllowedContextType([USER_CONTEXT_TYPE.DBO])) {
      return buttonConfigs.filter(({ authorities }) => hasAuthority(...authorities));
    }

    return [];
  }, [
    user,
    hasAllowedContextType,
    reserveAccount,
    disableReserveButton,
    openAccount,
    disableOpenAccountButton,
    openFirstAccount,
    disableRegDocButton,
    hasAuthority,
  ]);

  const onClickExit = useCallback(() => {
    void beforeLogout().then((yes: boolean) => {
      if (yes) {
        logout();
      }
    });
  }, [beforeLogout, logout]);

  const onChangeOrganization = useCallback(
    (orgId: string) => {
      void beforeOrgChange().then((yes: boolean) => {
        if (yes) {
          changeOrgId(orgId);

          if (prevOrgId !== orgId) {
            onChangeSelectedOrgId(orgId);
          }
        }
      });
    },
    [beforeOrgChange, changeOrgId, prevOrgId, onChangeSelectedOrgId]
  );

  // запрашиваем конфиг при рендере лейаута, потому что он нужен не везде.
  React.useEffect(() => {
    if (!config) {
      reload();
    }
  }, [config, reload]);

  if (fatalErr) {
    return <FatalErrorContent />;
  }

  if (!user.userId || !config) {
    return <LoaderOverlay opened />;
  }

  return token && user.userId ? (
    <CommonLayout
      showImportantNotification={notificationIsOpened}
      sidebar={<Sidebar options={menuItems} value={[baseUrl]} onClickOption={onClickOption} />}
      topline={
        <Topline
          actionLabel={locale.action.create}
          actions={actions}
          hasNotification={hasNotification}
          importantNotification={
            notificationIsOpened &&
            importantNotificationMessage && (
              <Sticky top>
                <Notification
                  Icon={Icons.Danger}
                  content={
                    <Horizon>
                      <Typography.P fill="BASE">{importantNotificationMessage.message}</Typography.P>
                      <Gap.XS />
                      <Box clickable onClick={goToImportantNotifications}>
                        <Typography.P fill="BASE" style={{ textDecoration: 'underline' }}>
                          {locale.action.show}
                        </Typography.P>
                      </Box>
                    </Horizon>
                  }
                  type={SYSTEM_NOTIFICATION_TYPE.WARNING}
                  onClose={closeImportantNotifications(importantNotificationMessage.notificationId)}
                />
              </Sticky>
            )
          }
          notificationsCount={notificationCount}
          organizationValue={prevOrgId} /* TODO: Поменять название пропса */
          organizationsOption={organizationsOptions}
          useName={userName}
          onChangeOrganization={onChangeOrganization}
          onClickExit={onClickExit}
          onClickNotification={goToNotifications}
          onClickUser={goToProfile}
        />
      }
    >
      <FatalErrorBoundary>{children}</FatalErrorBoundary>

      {/* Кнопка чата */}
      {config.chatVisibility && (
        <FromStream loader={<Box />} name="chat">
          {Chat => <Chat.ChatWidget />}
        </FromStream>
      )}
    </CommonLayout>
  ) : null;
};
