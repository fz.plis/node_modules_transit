import React, { createContext, useCallback, useContext, useEffect, useRef, useState } from 'react';
import { to } from '@platform/core';
import { dialog } from '@platform/ui';
import { hideLoader, openApproveConsentModal, showLoader } from '../components';
import { COMMON_STREAM_URL, CONSENT_TYPE, DONT_SHOW_DBO_INFORMATION_STORAGE_KEY, NOTIFICATIONS_AUTHORITIES } from '../constants';
import { getAppConfigItem, useAppConfig } from '../core/app-config';
import { useAuth } from '../core/auth';
import { healthCheck } from '../core/health-check';
import { setCryptoPlugin, getCryptoModule } from '../crypto';
import { CRYPTO_PLUGINS_FEATURE } from '../crypto/olk/constants';
import type { IBankClient, IParsedToken, IUserConsent, IUserData } from '../interfaces';
import { USER_CONTEXT_TYPE, USER_PASSED_FACTOR_TYPE, USER_ROLE, UserType } from '../interfaces';
import { locale } from '../localization';
import { isOutdatedCryptoModule } from '../utils';
import { DboInformationDialog, OutdatedCryptomoduleDialog } from '../views';
import { UpdateContactInfo } from '../views/update-contact-info';
import { UPDATE_CONTACT_INFO_TYPE } from '../views/update-contact-info/constants';
import { mainDomain } from './domains';
import { authService, dictionaryService, uaaService } from './services';

/**
 * Контекст пользователя банка.
 */
interface IUserContext {
  /**
   * Id Сертификата под которым пользователь зашел в систему.
   */
  defaultUserCertId: string | undefined;
  /**
   * Фатальная ошибка загрузки пользователя.
   */
  fatalErr: boolean;
  /**
   * Флаг показывающий, что пользователь является ДБО-пользователем.
   */
  isDboUser: boolean;
  /**
   * Данные о пользователе.
   */
  user: IUserData;
  /**
   * Список организаций.
   */
  userOrganizations: IBankClient[];
  /**
   * Перезагрузить список организаций, повторно проинициализировав их в контексте и в mainDomain.
   */
  reloadOrganizations(): void;
  /**
   * Тип пользователя.
   */
  userType: UserType;
  /**
   * Установить параметры пользователя.
   *
   * @param user Пользователь.
   */
  setUser(user: IUserData): void;
  /**
   * Метод обновления email.
   *
   * @param onClose - Закрытие диалогового окна.
   */
  updateUserEmail(onClose: () => void): void;
  /**
   * Метод обновления телефона.
   *
   * @param onClose - Закрытие диалогового окна.
   */
  updateUserPhone(onClose: () => void, disableResultStep?: boolean): void;
}

let defaultContext = {
  defaultUserCertId: '',
  fatalErr: false,
  isDboUser: false,
  user: ({} as unknown) as IUserData,
  userOrganizations: [] as IBankClient[],
  reloadOrganizations: () => {},
  userType: UserType.CLIENT,
  setUser: (_: IUserData) => {},
  updateUserEmail: (onClose: () => void = () => {}) => {
    dialog.show('update-user-email', UpdateContactInfo, { updateType: UPDATE_CONTACT_INFO_TYPE.EMAIL }, onClose);
  },
  updateUserPhone: (onClose: () => void = () => {}, disableResultStep?: boolean) => {
    dialog.show('update-user-phone', UpdateContactInfo, { updateType: UPDATE_CONTACT_INFO_TYPE.PHONE, disableResultStep }, onClose);
  },
};

export const UserContext = createContext<IUserContext>(defaultContext);

const setTokenData = (token: IParsedToken) => {
  defaultContext = {
    ...defaultContext,
    defaultUserCertId: token.default_sign_certificate || '',
    isDboUser: token.user_context_types.includes(USER_CONTEXT_TYPE.DBO),
  };

  return defaultContext;
};

const setUserData = (user: IUserData, orgs: IBankClient[]) => {
  defaultContext = {
    ...defaultContext,
    user,
    userOrganizations: orgs,
  };

  return defaultContext;
};

export const getUserValue = () => defaultContext;

export const getUserItem = <K extends keyof Omit<IUserContext, 'setUser'>>(key: K): IUserContext[K] => getUserValue()[key];

const consentErrorDialog = (
  updateConsent: (version: number) => Promise<IUserConsent[]>,
  logout: () => void,
  version: number
): Promise<IUserConsent[]> => {
  let isOk = false;

  return new Promise(resolve =>
    dialog.showError(locale.labels.repeatOnMoreTimeLater, {
      header: locale.labels.systemError,
      okButtonText: locale.action.repeat,
      cancelButtonText: locale.action.close,
      onOk: () => (isOk = true),
      onClose: async () => {
        if (isOk) {
          resolve(await updateConsent(version));
        }

        logout();
        resolve([]);
      },
    })
  );
};

export const UserProvider: React.FC = ({ children }) => {
  const { config } = useAppConfig();
  const { redirectToLogin, logout, token, tokenString, roles, config: authConfig, fatalErr: authFatalErr, authorities } = useAuth();
  const [fatalErr, setFatalErr] = useState(false);
  const [user, setUser] = useState<IUserData>({} as IUserData);
  const [userOrganizations, setUserOrganizations] = useState<IBankClient[]>([]);
  const pluginIsLoaded = useRef(false);
  const healthCheckIsLoaded = useRef(false);

  const updateConsentVersion = useCallback(
    async (version: number): Promise<IUserConsent[]> => {
      showLoader();

      const [userData, updateConsentErr] = await to(uaaService.addConsent(CONSENT_TYPE.CONSENT_OF_PERSONAL_DATA, version));

      hideLoader();

      if (updateConsentErr) {
        return consentErrorDialog(updateConsentVersion, logout, version);
      }

      return userData!.consents;
    },
    [logout]
  );

  const loadOrganizations = useCallback(async (): Promise<IBankClient[]> => {
    const [orgs, error] = await to(dictionaryService.bankClient.getUserOrganizations());

    if (error) {
      return [];
    }

    return orgs!;
  }, []);

  const reloadOrganizations = useCallback(async () => {
    const orgs = await loadOrganizations();

    setUserOrganizations(orgs);

    // не делаем лишний сетевой запрос, а сразу присваиваем организации
    // код ниже взят из метода loadOrganizations в mainDomain
    // (см. platform/services/src/client/domains/main-domain.ts)

    mainDomain.methods.userOrganizations.set(orgs);

    if (orgs.length === 1) {
      mainDomain.methods.selectedOrganizationId.set(orgs[0].id);
    }
  }, [loadOrganizations]);

  const isConfirmedUser = useCallback(userData => userData.phoneConfirmed && userData.emailConfirmed, []);

  const showApprovePersonalDataConsent = useCallback(
    (version: number): Promise<IUserConsent[] | null> =>
      new Promise(resolve => {
        openApproveConsentModal({
          type: CONSENT_TYPE.CONSENT_OF_PERSONAL_DATA,
          onClose: async (value?: boolean) => {
            if (value) {
              resolve(await updateConsentVersion(version));
            } else {
              logout();
            }
          },
        });
      }),
    [updateConsentVersion, logout]
  );

  const showNotConfirmedDataDialog = useCallback(
    (userData, userContexts: USER_CONTEXT_TYPE[], userPassedFactorTypes: USER_PASSED_FACTOR_TYPE[]) => {
      const isDboUser = userContexts.includes(USER_CONTEXT_TYPE.DBO);
      const isPasswordFactor = userPassedFactorTypes.includes(USER_PASSED_FACTOR_TYPE.PASSWORD);

      if (isDboUser && isPasswordFactor) {
        return;
      }

      if (!isConfirmedUser(userData)) {
        dialog.show('update-contact-info', UpdateContactInfo, {}, async isConfirmed => {
          if (isConfirmed === true) {
            if (userPassedFactorTypes.includes(USER_PASSED_FACTOR_TYPE.TOKEN_EXCHANGE)) {
              location.reload();

              return;
            }

            const orgs = await loadOrganizations();

            setUserOrganizations(orgs);
            setUser(oldUserData => ({
              ...oldUserData,
              phoneConfirmed: true,
              emailConfirmed: true,
            }));
          } else {
            logout();
          }
        });
      }
    },
    [setUser, isConfirmedUser, loadOrganizations, logout]
  );

  /** Функция показа сообщения о устаревшем криптоплагине. */
  const showOutdatedCryptomoduleDialog = useCallback((onClose?: () => void) => {
    dialog.show('outdated-cryptomodule-dialog', OutdatedCryptomoduleDialog, undefined, onClose);
  }, []);

  /** Функция показа сообщения о новых функциях для БСС-пользователей, отображение зависит от конфига. */
  const showDboInformation = useCallback(() => {
    const isModalEnabled =
      getAppConfigItem('displayInfoTechModalForDBO') && localStorage.getItem(DONT_SHOW_DBO_INFORMATION_STORAGE_KEY) !== 'true';

    if (isModalEnabled) {
      dialog.show('dbo-information-dialog', DboInformationDialog);
    }
  }, []);

  /**
   * Функция информирования БСС-пользователей, имеет два варианта поведения:
   * 1 — если криптомодуль устарел, сперва показывается сообщение с предложением обновить криптомодуль, после закрытия сообщения информируем пользователя о новых функциях.
   * 2 — если криптомодуль актуальной версии, информируем пользователя о новых функциях.
   *
   * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005211 - ЭФ с информацией о устаревшем криптомодуле.
   * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005123 - ЭФ с информацией о новых функциях.
   */
  const informDboUsers = useCallback(() => {
    const cryptoModule = getCryptoModule();
    const cryptoPlugins = getAppConfigItem(CRYPTO_PLUGINS_FEATURE);

    void isOutdatedCryptoModule(cryptoModule, cryptoPlugins).then(isOutdated => {
      if (isOutdated) {
        showOutdatedCryptomoduleDialog(showDboInformation);
      } else {
        showDboInformation();
      }
    });
  }, [showDboInformation, showOutdatedCryptomoduleDialog]);

  const handleUserChange = useCallback(
    ({ detail: newUserData }) => {
      setUser(oldUserData => ({
        ...oldUserData,
        ...newUserData,
      }));
    },
    [setUser]
  );

  useEffect(() => {
    if (!token) {
      return;
    }

    setTokenData(token);
    mainDomain.methods.user.setToken(tokenString, authorities);

    const load = async () => {
      const isClientUserRestricted = token?.roles && token.roles.includes(USER_ROLE.CLIENT_USER_RESTRICTED);

      if (isClientUserRestricted) {
        window.location.pathname = COMMON_STREAM_URL.PROFILE;

        return;
      }

      const [[userInfo, userLoadErr], [lastConsentVersion]] = await Promise.all([
        to(authService.profile()),
        to(dictionaryService.consent.getLastVersion(CONSENT_TYPE.CONSENT_OF_PERSONAL_DATA)),
      ]);

      if (userLoadErr) {
        redirectToLogin();

        return;
      }

      const { id, roles: userRoles, userType, ...rest } = userInfo!;

      let data = {
        ...rest,
        userId: id,
      };

      const orgs = await loadOrganizations();

      const consentOfPersonalData = (data.userConsents || []).find(
        consent => consent.consentType === CONSENT_TYPE.CONSENT_OF_PERSONAL_DATA
      );

      mainDomain.methods.user.load({
        roles: roles as USER_ROLE[],
        userData: data,
        userOrganizations: orgs,
      });

      if (!consentOfPersonalData) {
        const res = await showApprovePersonalDataConsent(lastConsentVersion);

        data = {
          ...data,
          userConsents: res ? res : data.userConsents,
        };
      }

      setUserData((data as unknown) as IUserData, orgs);

      setUser((data as unknown) as IUserData);

      setUserOrganizations(orgs);

      showNotConfirmedDataDialog(data, token.user_context_types, token.user_passed_factor_types);

      // игнорируем путь для лендинга, отображается только для Пользователь.<Тип
      // каталога пользователя>=BSS_DBO_320
      if (window.location.pathname !== '/' && token.user_context_types.includes(USER_CONTEXT_TYPE.DBO)) {
        informDboUsers();
      }

      if (token.authorities.includes(NOTIFICATIONS_AUTHORITIES.USER_NOTIFICATION_COUNT)) {
        mainDomain.methods.notificationDomain.getCount();
      }
    };

    load().catch(() => {
      setFatalErr(true);
      mainDomain.methods.hasFatalError.set(true);
    });

    window.addEventListener('userChanging', handleUserChange);

    return () => window.removeEventListener('userChanging', handleUserChange);
  }, [
    token,
    authorities,
    handleUserChange,
    loadOrganizations,
    redirectToLogin,
    roles,
    showNotConfirmedDataDialog,
    showApprovePersonalDataConsent,
    informDboUsers,
    tokenString,
  ]);

  useEffect(() => {
    mainDomain.methods.authConfig.set(authConfig!);
  }, [authConfig]);

  useEffect(() => {
    mainDomain.methods.hasFatalError.set(authFatalErr);
    setFatalErr(authFatalErr);
  }, [authFatalErr]);

  useEffect(() => {
    if (config && user.cryptoPlugin && !pluginIsLoaded.current) {
      setCryptoPlugin(user.cryptoPlugin);
      pluginIsLoaded.current = true;
    }
  }, [config, user.cryptoPlugin]);

  useEffect(() => {
    if (config?.healthCheckInterval && !healthCheckIsLoaded.current) {
      healthCheckIsLoaded.current = true;

      healthCheck.refreshStreamHealth(config.healthCheckInterval);
    }
  }, [config?.healthCheckInterval]);

  return (
    <UserContext.Provider
      value={{
        userOrganizations,
        reloadOrganizations,
        fatalErr,
        setUser,
        defaultUserCertId: defaultContext.defaultUserCertId,
        isDboUser: defaultContext.isDboUser,
        user: (user as unknown) as IUserData,
        userType: UserType.CLIENT,
        updateUserEmail: defaultContext.updateUserEmail,
        updateUserPhone: defaultContext.updateUserPhone,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

UserProvider.displayName = 'UserProvider';

export const useUser = () => useContext(UserContext);
