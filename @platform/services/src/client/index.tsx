import { formatMoney, setLocale as setBigNumberLocale, moneyInRub } from '@platform/tools/big-number';
import { formatDate, formatMonthShort, setLocale as setDateTimeLocale } from '@platform/tools/date-time';
import { registerFormatter, localeChangeSubscribe, getLocale } from '@platform/tools/localization';
import type * as commonInstance from '..';
import { SCOPE, setScope } from '../utils';
import * as clientInstance from './index.common';

setScope(SCOPE.CLIENT);

registerFormatter('moneyInWords', moneyInRub);
registerFormatter('money', formatMoney);
registerFormatter('date', formatDate);
registerFormatter('monthShort', formatMonthShort);

localeChangeSubscribe((locale: string) => {
  setDateTimeLocale(locale);
  setBigNumberLocale(locale);
});
setDateTimeLocale(getLocale());
setBigNumberLocale(getLocale());

export * from './index.common';
export const common: typeof commonInstance = clientInstance;

export const alias = '@platform/services';
export * from './layouts';
export * from './client-app';
export * from './hocs';
export * from './components';
export * from './user';
export * from './domains';
