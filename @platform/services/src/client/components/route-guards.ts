import type { IParsedToken, IClientTokenInfo } from '../../interfaces';
import { getToken, parseToken } from '../../utils';
import { SegmentGuardError, Dbo3GuardError } from './errors';

export interface IRouteContextOptions {
  /**
   * Сегменты, подлежащие проверке.
   */
  segments?: string[];
  /**
   * Контексты юзера, подлежащие проверке.
   */
  userContext?: string[];
}

export type ContextGuardParameters = IRouteContextOptions & { orgId?: string };

/**
 * Функция, принимающая данные токена и контекст проверки.
 * Должна выбрасывать ошибку, если проверка не пройдена.
 *
 * @param tokenInfo Данные токена.
 * @param contextOptions Конфиг для сравнение данных токена.
 * @throws RouteGuardError.
 */
export type ContextGuard<T extends IParsedToken = IParsedToken> = (tokenInfo: T, contextOptions: ContextGuardParameters) => void;

/**
 * Функция возвращает `Set` доступных сегментов из `token` по `orgId`.
 * Если данные по организации отстуствуют в токене, то возвращает пустой Set.
 *
 * @param token Данные токена юзера.
 * @param orgId Id выбранной организации.
 */
export const getSegmentsFromToken = (token: IClientTokenInfo, orgId: string): Set<string> => {
  const { availableSegments = {} } = token;

  if (orgId) {
    return new Set(availableSegments[orgId]);
  }

  return new Set(([] as string[]).concat(...Object.values(availableSegments)));
};

/**
 * Проверяет данные токена на наличие указанных в конфиге сегментов по переданному идентификатору организации.
 * Если ни один сегмент не присутствует в токене, то выбрасывается ошибка.
 *
 * @throws SegmentGuardError.
 */
export const segmentGuard: ContextGuard<IClientTokenInfo> = (tokenInfo, { segments = [], orgId = '' }) => {
  if (segments.length === 0) {
    return;
  }

  const userSegments = getSegmentsFromToken(tokenInfo, orgId);

  if (userSegments.size === 0) {
    throw new SegmentGuardError({ received: [...userSegments], requested: segments });
  }

  const segmentAvailable = segments.some(x => userSegments.has(x));

  if (!segmentAvailable) {
    throw new SegmentGuardError({ received: [...userSegments], requested: segments });
  }
};

/**
 * Проверяет данные токена на наличие указанных в конфиге контекстов юзера.
 * Если хотя бы один контекст из конфига отсутствует в токене, то выбрасывается ошибка.
 *
 * @throws Dbo3GuardError.
 */
export const dbo3Guard: ContextGuard = (tokenInfo, { userContext = [] }) => {
  if (userContext.length === 0) {
    return;
  }

  const { user_context_types: userContextTypes = [] } = tokenInfo;

  if (userContextTypes.length === 0) {
    throw new Dbo3GuardError({ received: userContextTypes, requested: userContext });
  }

  const valueMap = new Set<string>(userContextTypes);
  const available = userContext.every(x => valueMap.has(x));

  if (!available) {
    throw new Dbo3GuardError({ received: userContextTypes, requested: userContext });
  }
};

/**
 * Функция берет токен и последовательно пропускает его через каждый `guard` из `guards`
 * на соответствие общему конфигу `options`.
 * Проверка идет последовательно через все `guards` до первой выброшенной ошибки.
 *
 * @param options Конфиг проверки, содержащий id организации, список сегментов и контекстов.
 * @param guards Список `guard`, через который будет идти проверка токена на соответствие конфигу `options`.
 * @returns Кортеж, содержащий признак прохождения проверки и первую встреченную ошибку.
 */
export const checkGuards = <T extends IParsedToken>(
  options: ContextGuardParameters,
  ...guards: Array<ContextGuard<T>>
): [boolean, Error?] => {
  const token = getToken();

  if (!token) {
    return [true];
  }

  const tokenInfo = parseToken(token) as T;

  try {
    guards.forEach(x => x(tokenInfo, options));

    return [true];
  } catch (error) {
    return [false, error];
  }
};
