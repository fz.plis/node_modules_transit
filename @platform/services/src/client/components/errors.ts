import type { FallBackComponentType } from '../../components';
import type { ErrorGuardHanlder } from './fallbacks';

/**
 * Абстрактная класс для создания на его основе классов ошибок в функциях `guard`.
 *
 * Дополнительно хранит затребованные настройки и фактически полученные параметры
 * для вывода их каким-либо образом, если потребуется.
 */
export abstract class RouteGuardError<T> extends Error {
  /**
   * Внутренний код ошибки, должен быть уникален для каждого наследника.
   */
  public abstract code: string;
  /**
   * Затребованные `guard'ом` параметры.
   */
  public readonly requested: T;
  /**
   * Фактические параметры из токена.
   */
  public readonly received: T;

  constructor({ message, requested, received }: { message?: string; requested: T; received: T }) {
    super(message);

    this.requested = requested;
    this.received = received;
  }
}

/**
 * Ошибка доступа из-за несоответствия сегментов.
 */
export class SegmentGuardError extends RouteGuardError<string[]> {
  code = 'SegmentGuardError';
}

/**
 * Ошибка доступа из-за несоответствия контекста юзера.
 */
export class Dbo3GuardError extends RouteGuardError<string[]> {
  code = 'Dbo3GuardError';
}

export const guardHanldler = (checker: (err: Error) => boolean, comp: FallBackComponentType): ErrorGuardHanlder => err => {
  if (checker(err)) {
    return comp;
  }
};

/**
 * Функция, возвращающая type guard для ошибки, унаследованной от RouteGuardError.
 *
 * @param code Внутренний код ошибки.
 */
export const creareRouteTypeGuard = <T extends RouteGuardError<unknown>>(code: string) => (err: Error): err is T =>
  (err as RouteGuardError<unknown>).code === code;

/**
 * Type guard, проверяющий ошибку на соответствие Dbo3GuardError.
 */
export const isDbo3GuardError = creareRouteTypeGuard<Dbo3GuardError>('Dbo3GuardError');

/**
 * Type guard, проверяющий ошибку на соответствие SegmentGuardError.
 */
export const isSegmentGuardError = creareRouteTypeGuard<SegmentGuardError>('SegmentGuardError');
