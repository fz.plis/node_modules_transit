import React from 'react';
import type { RouteProps } from 'react-router-dom';
import { SecureRoute } from '../../../components';
import { USER_CONTEXT_TYPE } from '../../../interfaces';
import { isDbo3GuardError } from '../errors';
import { SegmentFallback, DboFallback } from '../fallbacks/built-in-fallbacks';
import { chainFallbacks } from '../fallbacks/chain-fallbacks';
import { dbo3Guard, segmentGuard } from '../route-guards';
import { useRouteGuards } from '../use-client-guards';

const Fallback = chainFallbacks([err => (isDbo3GuardError(err) ? DboFallback : null)], SegmentFallback);

const guards = [dbo3Guard, segmentGuard];

/**
 * Секьюрный роут, ограничивающий доступ для юзеров ЭКО, и организаций,
 * не имеющим доступ к сегментам переданным в пропс `availableSegments`.
 */
export const Dbo3Route: React.FC<RouteProps & { availableSegments: string[] }> = ({ availableSegments, ...props }) => {
  const [isAuthenticated, err] = useRouteGuards({
    segments: availableSegments,
    userContext: [USER_CONTEXT_TYPE.DBO],
    guards,
  });

  return <SecureRoute {...props} authErr={err} fallback={Fallback} isAuthenticated={isAuthenticated} />;
};

Dbo3Route.displayName = 'Dbo3Route';
