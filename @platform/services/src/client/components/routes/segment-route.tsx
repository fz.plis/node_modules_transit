import React from 'react';
import type { RouteProps } from 'react-router-dom';
import { SecureRoute } from '../../../components';
import { SegmentFallback } from '../fallbacks/built-in-fallbacks';
import { segmentGuard } from '../route-guards';
import { useRouteGuards } from '../use-client-guards';

/**
 * Секьюрный роут, ограничивающий доступ для организаций,
 * не имеющим доступ к сегментам переданным в пропс `availableSegments`.
 *
 * @deprecated C 2.08.2021 Используйте GuardRoute вместо.
 */
export const SegmentRoute: React.FC<RouteProps & { availableSegments: string[] }> = ({ availableSegments, ...props }) => {
  const [isAuthenticated, err] = useRouteGuards({
    segments: availableSegments,
    guards: [segmentGuard],
  });

  return <SecureRoute {...props} authErr={err} fallback={SegmentFallback} isAuthenticated={isAuthenticated} />;
};

SegmentRoute.displayName = 'SegmentRoute';
