import { REQUEST_FUNCTION } from '../../../../../constants';
import type { IBankClient } from '../../../../../interfaces';
import { BANK_CLIENT_STATUS, PRODUCT_TYPE, USER_CONTEXT_TYPE } from '../../../../../interfaces';
import { ERROR_TYPES, FORM_VIEW_AUTHORITY } from '../constants';
import { checkRouteAvailability, getErrorViewType } from '../utils';

describe('checkRouteAvailability', () => {
  test.each([
    ['route1', ['route1', 'route2'], true],
    ['route2', ['route1', 'route3'], false],
    [['route1', 'route4', 'route5'], ['route1', 'route2'], true],
    [['route2', 'route4'], ['route1', 'route3'], false],
  ])('роут c привилегиями: %s и пользовательскими привилегиями: %s должен быть %s', (authority, userAuthorities, result) => {
    expect(checkRouteAvailability(authority, userAuthorities)).toBe(result);
  });
});

describe('getErrorViewType', () => {
  test.each([
    [['auth1', 'auth2'], [{}] as IBankClient[], [USER_CONTEXT_TYPE.ECO], undefined, ERROR_TYPES.MFA],
    [[FORM_VIEW_AUTHORITY], [{}] as IBankClient[], [USER_CONTEXT_TYPE.ECO], undefined, ERROR_TYPES.AUTHORITY],
    [[FORM_VIEW_AUTHORITY], [] as IBankClient[], [USER_CONTEXT_TYPE.ECO], REQUEST_FUNCTION.O2_VIEW, ERROR_TYPES.ALL_CLIENT_ECO],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.ACTIVE }] as IBankClient[],
      [USER_CONTEXT_TYPE.ECO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.ACTIVE_CLIENT_ECO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.ACTIVE }] as IBankClient[],
      [USER_CONTEXT_TYPE.DBO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.ACTIVE_CLIENT_DBO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.ACTIVE, clientAgreements: [{ productType: PRODUCT_TYPE.LK_MP }] }] as IBankClient[],
      [USER_CONTEXT_TYPE.DBO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.ACTIVE_CLIENT_DBO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.ACTIVE, clientAgreements: [{ productType: PRODUCT_TYPE.CONSTITUTION }] }] as IBankClient[],
      [USER_CONTEXT_TYPE.ECO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_ECO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.ACTIVE, clientAgreements: [{ productType: PRODUCT_TYPE.CONSTITUTION }] }] as IBankClient[],
      [USER_CONTEXT_TYPE.DBO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_DBO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.POTENTIAL }] as IBankClient[],
      [USER_CONTEXT_TYPE.DBO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.POTENTIAL_CLIENT_DBO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.POTENTIAL }] as IBankClient[],
      [USER_CONTEXT_TYPE.ECO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.POTENTIAL_CLIENT_ECO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.POTENTIAL }, { status: BANK_CLIENT_STATUS.POTENTIAL }] as IBankClient[],
      [USER_CONTEXT_TYPE.DBO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.ALL_CLIENT_DBO,
    ],
    [
      [FORM_VIEW_AUTHORITY],
      [{ status: BANK_CLIENT_STATUS.POTENTIAL }, { status: BANK_CLIENT_STATUS.POTENTIAL }] as IBankClient[],
      [USER_CONTEXT_TYPE.ECO],
      REQUEST_FUNCTION.O2_VIEW,
      ERROR_TYPES.ALL_CLIENT_ECO,
    ],
  ])('результат выполнения с параметрами %s, %s, %s, %s должен быть %s', (authorities, organizations, contextTypes, type, res) => {
    expect(getErrorViewType(authorities, organizations, contextTypes, type)).toBe(res);
  });
});
