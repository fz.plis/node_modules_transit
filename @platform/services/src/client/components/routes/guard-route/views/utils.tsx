import React from 'react';
import { Gap, PrimaryButton, RegularButton } from '@platform/ui';
import { locale } from '../../../../../localization';
import { ERROR_TYPES } from '../constants';

export const getMessage = (type: ERROR_TYPES) => {
  switch (type) {
    case ERROR_TYPES.MFA:
      return {
        message: locale.guardRouter.errors.mfa.message,
        code: 'E001',
      };
    case ERROR_TYPES.AUTHORITY:
      return {
        message: locale.guardRouter.errors.authority.message,
        code: 'E002',
      };
    case ERROR_TYPES.ALL_CLIENT_ECO:
      return {
        title: locale.guardRouter.errors.allClientEco.title,
        message: locale.guardRouter.errors.allClientEco.message,
        code: 'E003',
      };
    case ERROR_TYPES.ALL_CLIENT_DBO:
      return {
        message: locale.guardRouter.errors.allClientDbo.message,
        code: 'E002',
      };
    case ERROR_TYPES.POTENTIAL_CLIENT_ECO:
      return {
        title: locale.guardRouter.errors.potentialClientEco.title,
        message: locale.guardRouter.errors.potentialClientEco.message,
        code: 'E004',
      };
    case ERROR_TYPES.POTENTIAL_CLIENT_DBO:
      return {
        message: locale.guardRouter.errors.potentialClientDbo.message,
        code: 'E005',
      };
    case ERROR_TYPES.ACTIVE_CLIENT_ECO:
      return {
        message: locale.guardRouter.errors.activeClientEco.message,
        code: 'E006',
      };
    case ERROR_TYPES.ACTIVE_CLIENT_DBO:
      return {
        message: locale.guardRouter.errors.activeClientDbo.message,
        code: 'E009',
      };
    case ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_ECO:
      return {
        message: locale.guardRouter.errors.activeClientConstitutionEco.message,
        code: 'E008',
      };
    case ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_DBO:
      return {
        message: locale.guardRouter.errors.activeClientConstitutionDbo.message,
        code: 'E009',
      };
    default:
      return {
        code: 'E001',
      };
  }
};

export const getActions = (type: ERROR_TYPES, toLogin: () => void, toConstitution: () => void) => {
  switch (type) {
    case ERROR_TYPES.ALL_CLIENT_ECO:
    case ERROR_TYPES.POTENTIAL_CLIENT_ECO:
    case ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_ECO:
      return <PrimaryButton onClick={toLogin}>{locale.action.loginClientBank}</PrimaryButton>;
    case ERROR_TYPES.ACTIVE_CLIENT_ECO: {
      return (
        <>
          <PrimaryButton onClick={toConstitution}>{locale.action.toConstitution}</PrimaryButton>
          <Gap.XS />
          <RegularButton onClick={toLogin}>{locale.action.loginClientBank}</RegularButton>
        </>
      );
    }
    case ERROR_TYPES.ACTIVE_CLIENT_DBO:
      return <PrimaryButton onClick={toConstitution}>{locale.action.toConstitution}</PrimaryButton>;

    default:
      return null;
  }
};
