import React from 'react';
import { Adjust, Pattern, Typography, Gap, Horizon, SpecialIcons, Box } from '@platform/ui';
import { COMMON_STREAM_URL } from '../../../../../constants';
import { useAuth } from '../../../../../core/auth';
import { locale } from '../../../../../localization';
import { useRedirect } from '../../../../../utils';
import { MainLayout } from '../../../../layouts';
import { useUser } from '../../../../user';
import type { ERROR_TYPES } from '../constants';
import { getActions, getMessage } from './utils';

/**
 * Свойства компонента недоступности роута.
 */
interface IRouteErrorProps {
  /**
   * Тип ошибки.
   */
  type: ERROR_TYPES;
}

/**
 * Компонент недоступности роута.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=6363519
 */
export const RouteError: React.FC<IRouteErrorProps> = ({ type }) => {
  const { logout } = useAuth();
  const { userOrganizations } = useUser();
  const { message, title, code } = getMessage(type);
  const params = userOrganizations[0]?.clientId ? `?ordId=${userOrganizations[0].clientId}` : '';
  const toConstitution = useRedirect(`${COMMON_STREAM_URL.CONSTITUTION}/new${params}`);
  const actions = getActions(type, logout, toConstitution);
  const consultantMessage = locale.guardRouter.errors.consultant.message({ code });

  return (
    <MainLayout>
      <Adjust pad={'X3L'}>
        <Pattern>
          <Pattern.Span size={6}>
            {title && (
              <>
                <Typography.H3>{title}</Typography.H3>
                <Gap.XL />
              </>
            )}

            <Horizon align="TOP">
              <SpecialIcons.Warning />
              <Gap />
              <Box>
                <Typography.PLong>{message}</Typography.PLong>
                <Gap />
                <Typography.PLong>{consultantMessage}</Typography.PLong>
              </Box>
            </Horizon>
            <Gap.XL />
            <Box>
              <Horizon>{actions}</Horizon>
            </Box>
          </Pattern.Span>
        </Pattern>
      </Adjust>
    </MainLayout>
  );
};

RouteError.displayName = 'RouteError';
