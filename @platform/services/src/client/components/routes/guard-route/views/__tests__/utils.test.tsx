import React from 'react';
import { Gap, PrimaryButton, RegularButton } from '@platform/ui';
import { locale } from '../../../../../../localization';
import { ERROR_TYPES } from '../../constants';
import { getActions, getMessage } from '../utils';

describe('getMessage', () => {
  test.each([
    [ERROR_TYPES.MFA, { message: locale.guardRouter.errors.mfa.message, code: 'E001' }],
    [ERROR_TYPES.AUTHORITY, { message: locale.guardRouter.errors.authority.message, code: 'E002' }],
    [
      ERROR_TYPES.ALL_CLIENT_ECO,
      { title: locale.guardRouter.errors.allClientEco.title, message: locale.guardRouter.errors.allClientEco.message, code: 'E003' },
    ],
    [ERROR_TYPES.ALL_CLIENT_DBO, { message: locale.guardRouter.errors.allClientDbo.message, code: 'E002' }],
    [
      ERROR_TYPES.POTENTIAL_CLIENT_ECO,
      {
        title: locale.guardRouter.errors.potentialClientEco.title,
        message: locale.guardRouter.errors.potentialClientEco.message,
        code: 'E004',
      },
    ],
    [ERROR_TYPES.POTENTIAL_CLIENT_DBO, { message: locale.guardRouter.errors.potentialClientDbo.message, code: 'E005' }],
    [ERROR_TYPES.ACTIVE_CLIENT_ECO, { message: locale.guardRouter.errors.activeClientEco.message, code: 'E006' }],
    [ERROR_TYPES.ACTIVE_CLIENT_DBO, { message: locale.guardRouter.errors.activeClientDbo.message, code: 'E009' }],
    [ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_ECO, { message: locale.guardRouter.errors.activeClientConstitutionEco.message, code: 'E008' }],
    [ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_DBO, { message: locale.guardRouter.errors.activeClientConstitutionDbo.message, code: 'E009' }],
  ])('результат выполнения для %s должен быть %s, %s', (type, result) => {
    expect(getMessage(type)).toStrictEqual(result);
  });
});

describe('getActions', () => {
  const toLogin = jest.fn();
  const toConstitution = jest.fn();

  test.each([
    [ERROR_TYPES.MFA, toLogin, toConstitution, null],
    // eslint-disable-next-line react/jsx-key
    [ERROR_TYPES.ALL_CLIENT_ECO, toLogin, toConstitution, <PrimaryButton onClick={toLogin}>{locale.action.loginClientBank}</PrimaryButton>],
    [
      ERROR_TYPES.POTENTIAL_CLIENT_ECO,
      toLogin,
      toConstitution,
      // eslint-disable-next-line react/jsx-key
      <PrimaryButton onClick={toLogin}>{locale.action.loginClientBank}</PrimaryButton>,
    ],
    [
      ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_ECO,
      toLogin,
      toConstitution,
      // eslint-disable-next-line react/jsx-key
      <PrimaryButton onClick={toLogin}>{locale.action.loginClientBank}</PrimaryButton>,
    ],
    [
      ERROR_TYPES.ACTIVE_CLIENT_DBO,
      toLogin,
      toConstitution,
      // eslint-disable-next-line react/jsx-key
      <PrimaryButton onClick={toConstitution}>{locale.action.toConstitution}</PrimaryButton>,
    ],
    [
      ERROR_TYPES.ACTIVE_CLIENT_ECO,
      toLogin,
      toConstitution,
      // eslint-disable-next-line react/jsx-key
      <>
        <PrimaryButton onClick={toConstitution}>{locale.action.toConstitution}</PrimaryButton>
        <Gap.XS />
        <RegularButton onClick={toLogin}>{locale.action.loginClientBank}</RegularButton>
      </>,
    ],
  ])('результат выполнения для %s, %s, %s должен быть %s', (type, toLoginFn, toConstFn, result) => {
    expect(getActions(type, toLoginFn, toConstFn)).toStrictEqual(result);
  });
});
