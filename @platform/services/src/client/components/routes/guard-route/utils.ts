import { REQUEST_FUNCTION } from '../../../../constants';
import type { IBankClient } from '../../../../interfaces';
import { BANK_CLIENT_STATUS, PRODUCT_TYPE, USER_CONTEXT_TYPE } from '../../../../interfaces';
import { ERROR_TYPES, FORM_VIEW_AUTHORITY } from './constants';

const isPotentialClient = ([client]: IBankClient[]) => client.status === BANK_CLIENT_STATUS.POTENTIAL;

const isActiveClientWithConstitution = ([client]: IBankClient[]) =>
  client.status === BANK_CLIENT_STATUS.ACTIVE &&
  (client.clientAgreements || []).some(clientAgreement => clientAgreement.productType === PRODUCT_TYPE.CONSTITUTION);

const isActiveClientWithoutConstitution = ([client]: IBankClient[]) =>
  client.status === BANK_CLIENT_STATUS.ACTIVE &&
  (client.clientAgreements || []).every(clientAgreement => clientAgreement.productType !== PRODUCT_TYPE.CONSTITUTION);

/**
 * Проверка доступности роута для пользователя.
 *
 * @param authority Привилегии роута.
 * @param userAuthorities Пользовательские привилегии.
 */
export const checkRouteAvailability = (authority: string[] | string, userAuthorities: string[]) => {
  const authorities = Array.isArray(authority) ? authority : [authority];

  return authorities.some(routeAuthority => userAuthorities.includes(routeAuthority));
};

/**
 * Получить тип ошибки.
 *
 * @param userAuthorities Привилегии пользователя.
 * @param userOrganizations Организации пользователя.
 * @param contextTypes Контекст пользователя.
 * @param type Тип функции.
 */
export const getErrorViewType = (
  userAuthorities: string[],
  userOrganizations: IBankClient[],
  contextTypes: USER_CONTEXT_TYPE[],
  type?: REQUEST_FUNCTION
): ERROR_TYPES => {
  if (!userAuthorities.includes(FORM_VIEW_AUTHORITY)) {
    return ERROR_TYPES.MFA;
  }

  const isDbo = contextTypes.includes(USER_CONTEXT_TYPE.DBO);

  if (type && Object.values(REQUEST_FUNCTION).includes(type)) {
    switch (userOrganizations.length) {
      case 0:
        return ERROR_TYPES.ALL_CLIENT_ECO;
      case 1:
        if (isPotentialClient(userOrganizations)) {
          if (isDbo) {
            return ERROR_TYPES.POTENTIAL_CLIENT_DBO;
          }

          return ERROR_TYPES.POTENTIAL_CLIENT_ECO;
        } else if (isActiveClientWithConstitution(userOrganizations)) {
          if (isDbo) {
            return ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_DBO;
          }

          return ERROR_TYPES.ACTIVE_CLIENT_CONSTITUTION_ECO;
        } else if (isActiveClientWithoutConstitution(userOrganizations)) {
          if (isDbo) {
            return ERROR_TYPES.ACTIVE_CLIENT_DBO;
          }

          return ERROR_TYPES.ACTIVE_CLIENT_ECO;
        }

        break;
      default: {
        if (isDbo) {
          return ERROR_TYPES.ALL_CLIENT_DBO;
        }

        return ERROR_TYPES.ALL_CLIENT_ECO;
      }
    }
  }

  return ERROR_TYPES.AUTHORITY;
};
