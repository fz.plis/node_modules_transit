/**
 * Типы ошибок недоступности роута.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=6363003
 */
export enum ERROR_TYPES {
  /**
   * Ошибка аутентификацию.
   */
  MFA = 'MFA',
  /**
   * Функция недоступна.
   */
  AUTHORITY = 'AUTHORITY',
  /**
   * Сервис недоступен.
   */
  ALL_CLIENT_ECO = 'ALL_CLIENT_ECO',
  /**
   * Недостаточно полномочий.
   */
  ALL_CLIENT_DBO = 'ALL_CLIENT_DBO',
  /**
   * Сервис недоступен для выбранной организации.
   */
  POTENTIAL_CLIENT_ECO = 'POTENTIAL_CLIENT_ECO',
  /**
   * У организации нет счета.
   */
  POTENTIAL_CLIENT_DBO = 'POTENTIAL_CLIENT_DBO',
  /**
   * Нет соглашения ЭКО пользователь.
   */
  ACTIVE_CLIENT_CONSTITUTION_ECO = 'ACTIVE_CLIENT_CONSTITUTION_ECO',
  /**
   * Нет соглашения ДБО пользователь.
   */
  ACTIVE_CLIENT_CONSTITUTION_DBO = 'ACTIVE_CLIENT_CONSTITUTION_DBO',
  /**
   * Недостаточно полномочий у активной организации ЭКО.
   */
  ACTIVE_CLIENT_ECO = 'ACTIVE_CLIENT_ECO',
  /**
   * Недостаточно полномочий у активной организации ДБО.
   */
  ACTIVE_CLIENT_DBO = 'ACTIVE_CLIENT_DBO',
}

export const FORM_VIEW_AUTHORITY = 'UAA.WEB_FORM_VIEW';
