import React from 'react';
import type { RouteProps } from 'react-router';
import { Route } from 'react-router-dom';
import type { REQUEST_FUNCTION } from '../../../../constants';
import { useAuth } from '../../../../core/auth';
import { useUser } from '../../../user';
import { checkRouteAvailability, getErrorViewType } from './utils';
import { RouteError } from './views';

/**
 * Интерфейс защищенного роута.
 */
interface IGuardRouteProps extends RouteProps {
  /**
   * Привилегии, которыми закрыт роут.
   */
  authority: string[] | string;
  /**
   * Тип функции.
   */
  type?: REQUEST_FUNCTION;
}

/**
 * Защищенный роут.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=6363003
 *
 * @example <GuardRoute authority={'SCROLLER_VIEW'} type="DSF_CLIENT_VIEW" ... />
 */
export const GuardRoute: React.FC<IGuardRouteProps> = ({ authority, type, ...props }) => {
  const { authorities, token } = useAuth();
  const { userOrganizations } = useUser();
  const isAvailable = checkRouteAvailability(authority, authorities);

  if (isAvailable) {
    return <Route {...props} />;
  }

  const error = getErrorViewType(authorities, userOrganizations, token!.user_context_types, type);

  return <RouteError type={error} />;
};

GuardRoute.displayName = 'GuardRoute';
