import React from 'react';
import { useUnit } from '../../domains';
import type { IParsedToken } from '../../interfaces';
import { selectedOrganizationId } from '../domains';
import type { IRouteContextOptions, ContextGuard } from './route-guards';
import { checkGuards } from './route-guards';

selectedOrganizationId.mount('main.user.selectedOrganization');

/**
 * Хук, определяющий доступность блока по информации из токена и выбранной организации.
 * Возвращает кортеж, с признаком прохождения авторизации и ,опционально, ошибкой,
 * пойманной одним из `guards`.
 *
 * @example
 * ```
 * // проверка конекста доступа к сегменту
 * const [isAuthenticated, err] = useRouteGuards({ segments: ['CAS_DBO_BSS_FRAME'], guards: [segmentGuard] })
 * ```
 */
export const useRouteGuards = <T extends IParsedToken>({
  segments,
  userContext,
  guards,
}: IRouteContextOptions & { guards: Array<ContextGuard<T>> }) => {
  const orgId = useUnit(selectedOrganizationId);

  return React.useMemo(() => checkGuards({ segments, userContext, orgId }, ...guards), [segments, userContext, orgId, guards]);
};
