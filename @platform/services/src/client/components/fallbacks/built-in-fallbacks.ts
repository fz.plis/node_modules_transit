import { router } from '@platform/core';
import type { IButtonAction } from '@platform/ui';
import { BUTTON } from '@platform/ui';
import { locale } from '../../../localization';
import { mainDomain } from '../../domains';
import { fallbackHoc } from './guard-fallback';

// В качестве label используется геттер, чтобы не падала локализация.
const dboActions: IButtonAction[] = [
  {
    get label() {
      return locale.fallback.dbo.signin;
    },
    name: 'logout',
    onClick: mainDomain.methods.user.logout,
    buttonType: BUTTON.PRIMARY,
  },
  {
    get label() {
      return locale.actions.back;
    },
    name: 'back',
    onClick: router.methods.goBack,
    buttonType: BUTTON.REGULAR,
  },
];

const defaultActions: IButtonAction[] = [
  {
    get label() {
      return locale.actions.back;
    },
    name: 'back',
    onClick: router.methods.goBack,
    buttonType: BUTTON.REGULAR,
  },
];

/**
 * Fallback-страница с сообщением о недоступности для юзера ЭКО.
 *
 * Используется при проверке на контекст юзера БСС.
 */
export const DboFallback = fallbackHoc(
  { actions: dboActions, description: () => locale.fallback.dbo.text, header: () => locale.fallback.header },
  'DboFallback'
);

/**
 * Fallback-страница с сообщением о недоступности функционала для выбранной организации.
 *
 * Используется для информирования об отсутствии доступа, если у организации нет требуемых сегментов.
 */
export const SegmentFallback = fallbackHoc(
  { actions: defaultActions, description: () => locale.fallback.segment.text, header: () => locale.fallback.header },
  'SegmentFallback'
);

/**
 * Дефолтная fallback-страница с сообщением об отсутствии прав.
 *
 * Используется в общем случае.
 */
export const DefaultFallback = fallbackHoc({ actions: defaultActions }, 'DefaultFallback');
