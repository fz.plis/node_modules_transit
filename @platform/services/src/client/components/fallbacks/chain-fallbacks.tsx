import React from 'react';
import type { FallBackComponentType } from '../../../components';
import { DefaultFallback } from './built-in-fallbacks';

/**
 * Функция, определяющая компонент по описанию ошибки.
 */
export type ErrorGuardHanlder = (error: Error) => FallBackComponentType | null | undefined;

/**
 * Возвращает функцию, выбирающую компонент в зависимости от переданной ошибки.
 * Перебор ведется до первого успешного результата.
 * Если он не дал результатов, то выдается `def`.
 *
 * @param fallbacks Массив функций перебора ошибок.
 * @param def Компонент Fallback, используемый если перебор не дал результатов.
 *
 * @example
 * ```
 * // массив функций типа ErrorGuardHanlder
 * const fallbacks = [...];
 * // создаем функцию перебора
 * const interator = findFallbackByErr(fallbacks, SomeComponent);
 * // Выбранной компонент для рендера ошибки
 * const Comp = iterator(err);
 * ```
 */
export const findFallbackByErr = (fallbacks: ErrorGuardHanlder[], def: FallBackComponentType) => (err: Error): FallBackComponentType => {
  let Comp: FallBackComponentType | null | undefined;

  for (const func of fallbacks) {
    Comp = func(err);

    if (Comp) {
      break;
    }
  }

  if (!Comp) {
    Comp = def;
  }

  return Comp;
};

/**
 * Хок, возвращающий компонент, автоматически выбирающий какой компонент
 * использовать для отображения ошибки.
 *
 * @param fallbacks Массив функций перебора ошибки.
 * @param def Компонент, используемый если перебор не дал результатов.
 *
 * @example
 * ```
 * // массив функций типа ErrorGuardHanlder
 * const fallbacks = [...];
 * // компонент для рендера ошибки
 * const MyFallback = chainFallbacks(fallbacks)
 * // использование
 * <MyFallback error={someErr} />
 * ```
 */
export const chainFallbacks = (fallbacks: ErrorGuardHanlder[], def: FallBackComponentType = DefaultFallback): FallBackComponentType => {
  const iterator = findFallbackByErr(fallbacks, def);

  const FallBackComponent: FallBackComponentType = ({ error, ...props }) => {
    const Comp = React.useMemo(() => (error ? iterator(error) : def), [error]);

    return <Comp {...props} error={error} />;
  };

  FallBackComponent.displayName = 'FallBackComponent';

  return FallBackComponent;
};
