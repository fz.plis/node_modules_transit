import React from 'react';
import type { IButtonAction } from '@platform/ui';
import { Adjust, Pattern, Typography, Gap, Horizon, SpecialIcons, ButtonsList } from '@platform/ui';
import type { IFallbackRouteProps } from '../../../components';
import { locale } from '../../../localization';
import { MainLayout } from '../../layouts';

export interface IFallbackActionsProps {
  /**
   * Список действий, доступных со страницы.
   */
  actions?: IButtonAction[];
}

export interface IGuardFallbackConfigurableProps extends IFallbackActionsProps {
  /**
   * Заголовок на странице.
   */
  header?: string | (() => string);
  /**
   * Подробное сообщение/расшифровка ошибки.
   */
  description?: string | (() => string);
}

export type IGuardFallbackProps = IFallbackRouteProps & IGuardFallbackConfigurableProps;

/**
 * Компонент-заглушка, предоставляющая шаблон для кастомизации страницы с сообщением об ограничении доступа.
 *
 * Позволяет передать заголовок страницы, текст сообщения и список кнопок на странице.
 */
export const GuardFallback: React.FC<IGuardFallbackProps> = React.memo(
  ({ actions = [], description = locale.fallback.notEnoughRight.text, header = locale.fallback.notEnoughRight.header }) => {
    // Пропсы могут передаваться как функции, чтобы не было проблем при сборке логина - там падала локализация.
    const descriptionText = React.useMemo(() => (typeof description === 'function' ? description() : description), [description]);
    const headerText = React.useMemo(() => (typeof header === 'function' ? header() : header), [header]);

    return (
      <MainLayout>
        <Adjust pad={'X3L'}>
          <Pattern>
            <Pattern.Span size={6}>
              <Typography.H3>{headerText}</Typography.H3>
              <Gap.XL />
              <Horizon align="TOP">
                <SpecialIcons.Warning />
                <Gap />
                <Typography.PLong>{descriptionText}</Typography.PLong>
              </Horizon>
              {actions && (
                <>
                  <Gap.XL />
                  <ButtonsList actions={actions} />
                </>
              )}
            </Pattern.Span>
          </Pattern>
        </Adjust>
      </MainLayout>
    );
  }
);

GuardFallback.displayName = 'GuardFallback';

/**
 * Хок, прокидывающий в `GuardFallback` заголовок `header`, описание `description` и `actions` на странице.
 *
 * @param options Настройки кастомизации шаблона.
 * @param displayName DisplayName компонента, созданного через этот hoc.
 */
export const fallbackHoc = (
  options: IGuardFallbackConfigurableProps,
  displayName = 'GuardFallbackHoc(unknown)'
): React.FC<IGuardFallbackProps> => {
  const Comp: React.FC<IGuardFallbackProps> = React.memo(
    ({ description = options.description, header = options.header, actions = options.actions }) => (
      <GuardFallback actions={actions} description={description} header={header} />
    )
  );

  Comp.displayName = displayName;

  return Comp;
};
