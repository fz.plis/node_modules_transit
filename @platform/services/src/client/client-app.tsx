import React from 'react';
import type { BrowserHistoryBuildOptions } from 'history';
import { createBrowserHistory } from 'history';
import ReactDOM from 'react-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { router } from '@platform/core';
import { dialog, ThemeProvider } from '@platform/ui';
import { FatalErrorBoundary, Loader, NotFoundContent, ServiceUnavailable } from '../components';
import { AppConfig, getSettingsLoader, getFallbackConfigLoader } from '../core/app-config';
import { AuthProvider } from '../core/auth';
import { getAppMainComponent } from '../core/get-app-component';
import { setHealthCheck } from '../core/health-check';
import { olkCryptoModule, setCryptoModule } from '../crypto';
import { addSetOrganizationIdHeaderMiddleware, SCOPE, setScope } from '../utils';
import { withLayout } from '../views';
import { MainLayout } from './layouts';
import { authService, uaaService } from './services';
import { UserProvider, UserContext } from './user';

addSetOrganizationIdHeaderMiddleware();

const NotFound = withLayout(MainLayout, NotFoundContent);
const LoadingStream = withLayout(MainLayout, Loader);
const Unavailable = () => (
  <MainLayout>
    <ServiceUnavailable />
  </MainLayout>
);

type UserConfirmationFunc = BrowserHistoryBuildOptions['getUserConfirmation'];

export const getUserConfirmation: UserConfirmationFunc = (message, callback) =>
  dialog.showConfirmation(message, () => callback(true), { onCancel: () => callback(false) });

export const history = createBrowserHistory({
  getUserConfirmation,
});

router.methods.connectToHistoryInstance(history);

const App = getAppMainComponent({
  NotFound,
  LoadingStream,
  Unavailable,
  history,
});

const queryClient = new QueryClient({ defaultOptions: { queries: { refetchOnWindowFocus: false } } });

const getSettings = getSettingsLoader(
  () => Promise.reject(true), // appSettingsService.getByCode(SETTING_CODE.MENU),
  () => Promise.reject(true), // appSettingsService.getByCode(SETTING_CODE.SETTINGS),
  getFallbackConfigLoader('/api/config-service/config/configuration/pr/lb/config.json')
);

export const renderApp = () =>
  ReactDOM.render(
    <ThemeProvider>
      <FatalErrorBoundary>
        <QueryClientProvider client={queryClient}>
          <AuthProvider authLoader={authService.getAuthConfiguration} authoritiesLoader={uaaService.getAuthorities}>
            <AppConfig loader={getSettings}>
              <UserProvider>
                <UserContext.Consumer>{({ user }) => <App user={user} />}</UserContext.Consumer>
              </UserProvider>
            </AppConfig>
          </AuthProvider>
        </QueryClientProvider>
      </FatalErrorBoundary>
    </ThemeProvider>,
    document.getElementById('app')
  );

setCryptoModule(olkCryptoModule);

setHealthCheck(() => Promise.reject(true)); //  appSettingsService.getByCode(SETTING_CODE.HEALTH_CHECK));

setScope(SCOPE.CLIENT);
