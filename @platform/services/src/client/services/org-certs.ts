import { HEADERS_PARAM } from '../../constants';
import type {
  ICertResponse,
  IValidateResult,
  ICertValidateResult,
  IDigest,
  ICertSignature,
  ICertParseResponse,
  IOrgCertFileInfo,
} from '../../interfaces';
import type { ICert } from '../../interfaces/org-cert';
import type { IOrganizationCertificatesService } from '../../services/helpers/org-certs';
import { getNewOrgCertsService, certificateResponse } from '../../services/helpers/org-certs';
import { getFileName } from '../../utils';
import { request } from '../../utils/request';

export interface IOrgCertsService extends IOrganizationCertificatesService {
  getAccredited(): Promise<string[]>;
  parse(rawCertificate: string): Promise<IOrgCertFileInfo>;
  uploadFile(
    attachmentId: string,
    orgOfficialCertId: string,
    requestDate: string,
    signature?: ICertSignature,
    orgKpp?: string
  ): Promise<ICertValidateResult>;
  validate(attachmentId: string, orgOfficialCertId: string): Promise<IValidateResult>;
  getDigest(certificateValue: string, date: string, orgKpp?: string): Promise<IDigest>;
  getCertificate(id: string): Promise<ICert>;
  downloadCaAccredited(): Promise<{
    fileName: string;
    type: string;
    data: string;
  }>;
}

const CLIENT_DICTIONARY_URL = '/api/client-dictionary/dictionary/client';
const SIGNATURE_URL = '/api/signature/sign';

/**
 * Сертификаты представителя клиента Банка.
 */
export const orgCertsService: IOrgCertsService = {
  ...getNewOrgCertsService(CLIENT_DICTIONARY_URL, SIGNATURE_URL),
  getAccredited: async () => {
    const { data: response } = await request({
      method: 'GET',
      url: `${SIGNATURE_URL}/ca/accredited`,
    });

    return response.list;
  },
  parse: async rawCertificate => {
    const { data: response } = await request({
      url: `${SIGNATURE_URL}/certificate/parse`,
      method: 'POST',
      data: {
        rawCertificate,
      },
    });
    const { data, message, code } = response as ICertResponse<ICertParseResponse>;

    if (code) {
      throw message;
    }

    return {
      issuer: data.authorityName,
      validFrom: data.validFrom,
      validTo: data.validTo,
      organizationName: data.subjectOrganization,
      serialNumber: data.serialNumber,
      whomIssued: `${data.subjectSurname}${data.subjectGivenName ? ' ' + data.subjectGivenName : ''}`,
      thumbprint: data.footprint,
      subjectCommonName: data.subject,
      subjectOrganization: data.subjectOrganization,
      subjectOrganizationalUnit: data.subjectOrganizationalUnit,
      subjectLocality: data.subjectLocality,
      subjectCountry: data.subjectCountry,
      subjectEmail: data.subjectEmail,
      subjectGivenName: data.subjectGivenName,
      subjectSurname: data.subjectSurname,
      subjectTitle: data.subjectTitle,
      subjectInn: data.subjectInn,
      subjectOgrn: data.subjectOgrn,
    };
  },
  uploadFile: async (attachmentId, orgOfficialCertId, requestDate, signature, orgKpp) => {
    const response = await request({
      url: `${CLIENT_DICTIONARY_URL}/certificate/upload/attachment`,
      method: 'POST',
      data: {
        attachmentId,
        orgOfficialCertId,
        requestDate,
        signature,
        orgKpp,
      },
    });

    return certificateResponse<ICertValidateResult>(response);
  },
  validate: async (attachmentId, orgOfficialCertId) => {
    const response = await request({
      url: `${CLIENT_DICTIONARY_URL}/certificate/validate/attachment`,
      method: 'POST',
      data: {
        attachmentId,
        orgOfficialCertId,
      },
    });

    return certificateResponse<IValidateResult>(response);
  },
  getDigest: async (certificateValue, date, orgKpp) => {
    const response = await request({
      url: `${CLIENT_DICTIONARY_URL}/org-official-cert/digest/new`,
      method: 'POST',
      data: {
        certificateValue,
        date,
        orgKpp,
      },
    });

    return certificateResponse<IDigest>(response);
  },

  getCertificate: id =>
    request({
      url: `${SIGNATURE_URL}/certificate/${id}`,
    }).then(res => res.data.data),

  downloadCaAccredited: () =>
    request({
      url: `${SIGNATURE_URL}/ca/export/accredited-and-work`,
      responseType: 'blob',
    }).then(result => {
      const param = result.headers[HEADERS_PARAM.CONTENT_DISPOSITION] || '';

      return {
        fileName: getFileName(param),
        type: result.headers[HEADERS_PARAM.CONTENT_TYPE],
        data: result.data,
      };
    }),
};
