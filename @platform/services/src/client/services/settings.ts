import { getNewAppSettings } from '../../services/helpers';

const SETTINGS_URL = '/api/settings-client/settings/system';
const PUBLIC_SETTINGS_URL = '/api/settings-client/settings/public/system';

export const appSettingsService = getNewAppSettings(SETTINGS_URL);
export const appPublicSettingsService = getNewAppSettings(PUBLIC_SETTINGS_URL);
