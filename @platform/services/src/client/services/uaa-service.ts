import type { CONSENT_TYPE, SYSTEM_TYPE } from '../../constants';
import { API_UAA_URL } from '../../constants';
import type {
  IServerResp,
  ICompleteRegisterResponse,
  IUpdateUserResponse,
  IUserInfo,
  IRegisterRequestData,
  IRegisterResponse,
  IRestorePasswordRequest,
  IRestorePasswordResponse,
  ICompleteRestorePasswordRequest,
  ICompleteRestorePasswordResponse,
  IChangePasswordResponse,
  IChangePhoneNumberRequest,
  IChangeEmailRequest,
  IMfaChains,
  IConsentRequest,
  IUserConsent,
  IGenerateCaptchaResponse,
  IChangePasswordDboResponse,
  IDataResponse,
  IUserAuthoritiesResponse,
  ITokenResponse,
} from '../../interfaces';
import { ERROR } from '../../interfaces';
import { request } from '../../utils';

const UAA_SERVICE_URL = `${API_UAA_URL}/clientuser`;
const UAA_SERVICE_MFA_URL = `${API_UAA_URL}/mfachain/user`;

const handleResult = <T>(r: { data: IServerResp<T> }) => {
  if (r.data.errorInfo) {
    throw { response: { data: r.data } };
  }

  return r.data.data;
};

const handleDboResult = (r: { data: IChangePasswordDboResponse }) => {
  if (r.data.error) {
    throw r.data;
  }

  return r.data.data;
};

export const uaaService = {
  register: (data: IRegisterRequestData & { addConsentRq: IConsentRequest }) =>
    request<IServerResp<IRegisterResponse>>({
      url: `${UAA_SERVICE_URL}/register`,
      method: 'POST',
      data,
      disableTokenHeader: true,
    }).then(handleResult),
  completeRegister: (requestId: string, smsCode: string) =>
    request<IServerResp<ICompleteRegisterResponse>>({
      url: `${UAA_SERVICE_URL}/completeRegister`,
      method: 'POST',
      data: { requestId, smsCode },
      disableTokenHeader: true,
    }).then(handleResult),
  generate: () =>
    request({
      url: `${UAA_SERVICE_URL}/captcha`,
      method: 'POST',
      disableTokenHeader: true,
    }).then((r: { data: IServerResp<IGenerateCaptchaResponse> }) => r.data.data),
  restorePassword: (data: IRestorePasswordRequest) =>
    request<IServerResp<IRestorePasswordResponse>>({
      url: `${UAA_SERVICE_URL}/restorePassword`,
      method: 'POST',
      data,
      disableTokenHeader: true,
    }).then(handleResult),
  completeRestorePassword: (data: ICompleteRestorePasswordRequest) =>
    request({
      url: `${UAA_SERVICE_URL}/completeRestorePassword`,
      method: 'POST',
      data,
      disableTokenHeader: true,
    }).then((r: { data: IServerResp<ICompleteRestorePasswordResponse> }) => {
      if (r.data.errorInfo) {
        const { errorInfo } = r.data;

        throw {
          response: {
            data: {
              errorInfo: {
                message: errorInfo.message,
                code: errorInfo.code === ERROR.PASS_REC_USER_BLOCKED ? ERROR.USER_BLOCKED : errorInfo.code,
              },
            },
          },
        };
      }

      return r.data.data;
    }),
  changePassword: (oldPassword: string, newPassword: string) =>
    request<IServerResp<IChangePasswordResponse>>({
      url: `${UAA_SERVICE_URL}/changePassword`,
      method: 'POST',
      data: { oldPassword, newPassword },
    }).then(handleResult),
  changePasswordDbo: (data: { newPassword: string; newPasswordConfirm: string; oldPassword: string }) =>
    request<IChangePasswordDboResponse>({
      url: `${UAA_SERVICE_URL}/changePasswordDbo`,
      method: 'PUT',
      data,
    }).then(handleDboResult),
  changePhoneNumber: (data: IChangePhoneNumberRequest) =>
    request<IServerResp<ITokenResponse>>({
      url: `${UAA_SERVICE_URL}/changePhoneNumber`,
      method: 'POST',
      data,
    }).then(handleResult),
  changeEmail: (data: IChangeEmailRequest) =>
    request<IServerResp<ITokenResponse>>({
      url: `${UAA_SERVICE_URL}/changeEmail`,
      method: 'POST',
      data,
    }).then(handleResult),
  updateUser: (data: IUserInfo) =>
    request<IServerResp<IUpdateUserResponse>>({
      url: `${UAA_SERVICE_URL}/update`,
      method: 'POST',
      data,
    }).then(handleResult),
  completePhoneChange: (requestId: string, smsCode: string) =>
    request<IServerResp<ICompleteRegisterResponse>>({
      url: `${UAA_SERVICE_URL}/completePhoneChange`,
      method: 'POST',
      data: { requestId, smsCode },
    }).then(handleResult),
  completeEmailChange: (requestId: string, emailCode: string) =>
    request<IServerResp<ICompleteRegisterResponse>>({
      url: `${UAA_SERVICE_URL}/completeEmailChange`,
      method: 'POST',
      data: { requestId, emailCode },
    }).then(handleResult),
  hideNotification: () =>
    request({
      url: `${UAA_SERVICE_URL}/hideNotification`,
      method: 'POST',
    }).then(() => true),
  renewConfirm: (confirmationId: string, disableTokenHeader: boolean = true) =>
    request<IServerResp<ITokenResponse>>({
      url: `${UAA_SERVICE_URL}/renewConfirm`,
      method: 'POST',
      data: { confirmationId },
      disableTokenHeader,
    }).then(handleResult),
  getAvailableMfaChain: () =>
    request({
      url: `${UAA_SERVICE_MFA_URL}/available`,
      method: 'GET',
    }).then((r: { data: IServerResp<IMfaChains[]> }) => r.data.data),
  getDefaultMfaChain: () =>
    request({
      url: `${UAA_SERVICE_MFA_URL}/default`,
      method: 'GET',
    }).then((r: { data: IServerResp<IMfaChains[]> }) => r.data.data),
  changeMfaChain: (mfaChainName: string) =>
    request({
      url: `${UAA_SERVICE_MFA_URL}/select/${mfaChainName}`,
      method: 'POST',
    }).then((r: { data: IServerResp<IMfaChains> }) => r.data.data),
  /**
   * Запрос получения токена внешней системы.
   *
   * @param systemType Тип внешней системы.
   * @param redirectUri URL адрес, на который будет перенаправлен пользователь в случае успешной авторизации.
   * @returns URL адрес для перенаправления пользователя во внешнюю систему.
   */
  getExternalToken: (systemType: SYSTEM_TYPE, redirectUri?: string): Promise<string> =>
    request<{ redirectUri: string }>({
      url: `${API_UAA_URL}/external/oauth/token`,
      method: 'POST',
      data: { systemType, redirectUri },
    }).then(({ data: response }) => response.redirectUri),
  addConsent: (type: CONSENT_TYPE, version: number) =>
    request({
      method: 'POST',
      url: `${API_UAA_URL}/consent/add`,
      data: {
        consentType: type,
        consentVersion: version,
      },
    }).then(({ data }) => {
      if (data?.data) {
        return data.data as { consents: IUserConsent[] };
      }

      throw data?.message;
    }),
  hasConsent: (type: CONSENT_TYPE, version: string) =>
    request({
      method: 'GET',
      url: `${API_UAA_URL}/consent/has/${type}/${version}`,
    }).then(res => res.data),
  findByUid: (id: string) =>
    request({
      url: `${UAA_SERVICE_URL}/uid/${id}`,
    }).then(r => {
      if (r.data.errorInfo) {
        throw { response: { data: r.data } };
      }

      return r.data.data.data;
    }),
  getAuthorities: () =>
    request<IDataResponse<IUserAuthoritiesResponse>>({
      url: `${API_UAA_URL}/user-info/user-authorities`,
    }).then(res => res.data.data),
};
