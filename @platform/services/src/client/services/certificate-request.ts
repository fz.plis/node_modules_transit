import type { ICertificateRequest, ICertificateRequestHistory } from '../../interfaces';
import { createCertificateRequestService } from '../../services/helpers';
import { getNewDictionaryService } from '../../utils';
import { request } from '../../utils/request';

const clientUrl = '/api/client-dictionary/dictionary/client/cert-request';

const { getList, get, getCounter } = getNewDictionaryService<ICertificateRequest>(clientUrl);

/**
 * Сервис по работе с заявками на регистрацию клиента.
 */
export const certificateRequestService = {
  getList,
  get,
  getCounter,
  getHistory: (id: string): Promise<ICertificateRequestHistory[]> =>
    request({
      url: `${clientUrl}/history/${id}`,
    }).then(d => d.data.list),
  ...createCertificateRequestService(clientUrl),
};
