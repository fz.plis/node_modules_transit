import { createAttachmentService } from '../../services/helpers';

const ATTACHMENT_SERVICE_EXTERNAL_URL = '/api/filestorage/external';

export const attachmentService = createAttachmentService(ATTACHMENT_SERVICE_EXTERNAL_URL);
