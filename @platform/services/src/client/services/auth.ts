import { API_UAA_URL, API_OAUTH_URL, CLIENT_ID, AUTH_REQUEST_CONFIG } from '../../constants';
import type {
  ILoginApiRequest,
  ILoginPasswordRequest,
  ILoginResponse,
  ILoginCertificateRequest,
  ILoginOtpTotpRequest,
  IApproval,
  IClientProfile,
  IMfaCertificateResponse,
  IAuthConfiguration,
  IAuthResponse,
  IAuthApiResponse,
  IMfaChains,
  IChangePasswordDboResponse,
  IChangePasswordDboFormResponse,
  ICodeMfaChallenge,
  IServerResp,
} from '../../interfaces';
import { AUTH_RESPONSE_TYPE } from '../../interfaces';
import {
  createAuthRequestFunc,
  transformPassword,
  transformCertificate,
  transformOtpTotp,
  request,
  queryString,
  transformError,
  transformFromApi,
} from '../../utils';

const API_MFA_URL = `${API_OAUTH_URL}/mfa/challenge`;
const API_APPROVE_URL = `${API_OAUTH_URL}/approvals`;
const LOGIN_URL = `${API_UAA_URL}/login`;

const loginRequest = createAuthRequestFunc<ILoginApiRequest>(LOGIN_URL);

export const authService = {
  getAuthConfiguration: () =>
    request<IAuthConfiguration>({
      disableTokenHeader: true,
      url: `${API_UAA_URL}/.well-known/openid-configuration`,
      method: 'GET',
    }).then(r => r.data),
  login: {
    byPassword: loginRequest<ILoginPasswordRequest, ILoginResponse>(transformPassword),
    byCertificate: loginRequest<ILoginCertificateRequest, ILoginResponse>(transformCertificate),
    byOtp: loginRequest<ILoginOtpTotpRequest, ILoginResponse>(transformOtpTotp),
    byTotp: loginRequest<ILoginOtpTotpRequest, ILoginResponse>(transformOtpTotp),
    changePassword: (remoteId: number, data: { newPassword: string; newPasswordConfirm: string; oldPassword: string }) =>
      request<IChangePasswordDboResponse>({
        url: `${LOGIN_URL}/change/password/${remoteId}`,
        method: 'PUT',
        data,
      }).then(r => r.data),
    getChangePasswordFormState: (remoteId: number) =>
      request<IChangePasswordDboFormResponse>({
        url: `${LOGIN_URL}/change/password/${remoteId}/form-state`,
      }).then(r => r.data),
  },
  certificateChallenge: (token: string): Promise<IMfaCertificateResponse> =>
    request({
      disableTokenHeader: true,
      data: {
        token,
      },
      url: `${API_MFA_URL}/certificate`,
      method: 'POST',
    })
      .then(r => r.data)
      .catch(transformError),
  otpChallenge: (token: string): Promise<ICodeMfaChallenge> =>
    request<IServerResp<ICodeMfaChallenge>>({
      disableTokenHeader: true,
      data: {
        token,
      },
      url: `${API_MFA_URL}/otp`,
      method: 'POST',
    })
      .then(r => r.data.data)
      .catch(transformError),
  totpChallenge: (token: string): Promise<ICodeMfaChallenge> =>
    request<IServerResp<ICodeMfaChallenge>>({
      disableTokenHeader: true,
      data: {
        token,
      },
      url: `${API_MFA_URL}/totp`,
      method: 'POST',
    })
      .then(r => r.data.data)
      .catch(transformError),
  listChallenge: (token: string): Promise<IMfaChains[]> =>
    request({
      data: {
        token,
      },
      url: `${API_MFA_URL}/list`,
      method: 'POST',
    })
      .then(r => r.data)
      .catch(transformError),
  selectChallenge: (token: string, chainName: string): Promise<ILoginResponse> =>
    request({
      data: {
        token,
        chainName,
      },
      url: `${API_MFA_URL}/select`,
      method: 'POST',
    })
      .then(r => r.data)
      .catch(transformError),
  approvals: {
    getById: (approveId: string): Promise<IApproval> =>
      request({ method: 'GET', url: `${API_APPROVE_URL}/${approveId}`, disableTokenHeader: true }).then(response => response.data),
    approve: (approveId: string): Promise<IApproval> =>
      request({ method: 'POST', url: `${API_APPROVE_URL}/${approveId}/approve`, disableTokenHeader: true }).then(response => response.data),
    deny: (approveId: string) =>
      request({ method: 'POST', url: `${API_APPROVE_URL}/${approveId}/deny`, disableTokenHeader: true }).then(response => response.data),
  },
  profile: (): Promise<IClientProfile> =>
    request({ url: `${API_UAA_URL}/profile`, method: 'GET' }).then(response => response.data.data.data),
  authorize: () =>
    request({
      ...AUTH_REQUEST_CONFIG,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      url: `${API_OAUTH_URL}/authorize`,
      data: queryString({ client_id: CLIENT_ID, response_type: AUTH_RESPONSE_TYPE.TOKEN }),
    })
      .then(r => transformFromApi<IAuthApiResponse, IAuthResponse>(r.data))
      .catch(transformError),
  logout: () =>
    request({
      url: `${API_UAA_URL}/logout`,
      disableTokenHeader: true,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      data: queryString({ body: 'exit' }),
    })
      .then(r => r.data)
      .catch(transformError),
};

export const AUTH_REQUEST_DATA = {
  client_id: CLIENT_ID,
  response_type: AUTH_RESPONSE_TYPE.TOKEN,
};
