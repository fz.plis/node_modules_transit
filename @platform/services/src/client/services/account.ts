import { getAccountUrl } from '../../constants';
import { createAccountClientService } from '../../services/helpers';

export const accountService = createAccountClientService(getAccountUrl());
