import { API_UAA_URL } from '../../constants';
import type { IServerResp, IGenerateCaptchaResponse } from '../../interfaces';
import { request } from '../../utils';

export const captchaService = {
  getCaptchaUrl: (id: string) => `${API_UAA_URL}/captcha?id=${id}`,
  generate: () =>
    request({
      url: `${API_UAA_URL}/captcha`,
      method: 'POST',
      disableTokenHeader: true,
    }).then((r: { data: IServerResp<IGenerateCaptchaResponse> }) => r.data.data),
};
