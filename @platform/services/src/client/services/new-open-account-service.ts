import { request } from '../../utils';

const openAccountUrl = '/api/opening-request/account-opening-contract';

/** Клиентский сервис для откртия о2+. */
export const newOpenAccountService = {
  checkIfNewO2IsAvailable: (): Promise<{ data: boolean }> =>
    request({
      url: `${openAccountUrl}/available`,
      method: 'GET',
    }).then(r => r.data.data),
};
