import {
  getOkopfUrl,
  getOkfsfUrl,
  getActivityTypeUrl,
  getClientOfficialUrl,
  getCountryUrl,
  getAccountTypeUrl,
  getBranchUrl,
  getBankClientUrl,
  getGkUrl,
  getCurrencyUrl,
  getFiasUrl,
  getConsentUrl,
  getCityVisitingManagerUrl,
} from '../../constants/service-urls';
import type {
  IBaseEntity,
  IOkopf,
  IOkfs,
  ICurrency,
  IActivityType,
  ICountry,
  IAccountType,
  IGozContract,
  IBankClient,
  IMetaData,
  ICollectionResponse,
} from '../../interfaces';
import { createBankClientService } from '../../services/helpers';
import { createBranchV2Service, createClientBranchService } from '../../services/helpers/branch';
import { createClientOffical } from '../../services/helpers/client-official';
import { getDictionaryService, request, metadataToRequestParams } from '../../utils';
import { getCityVisitingManagerService } from '../../utils/city-visiting-manager-service';
import { getDefaultConsentService } from '../../utils/default-consent-service';
import { getDefaultSearchService } from '../../utils/default-search-service';

const createDictionary = <TEntity extends IBaseEntity>(baseUrl: string) => {
  const { get, getList, searchByName } = getDictionaryService<TEntity>(baseUrl);

  return {
    get,
    getList,
    searchByName,
  };
};

const bankClientUrl = getBankClientUrl();
const branchV2Url = `${getBranchUrl()}/v2`;

const okopf = createDictionary<IOkopf>(getOkopfUrl());
const okfs = createDictionary<IOkfs>(getOkfsfUrl());
const currency = createDictionary<ICurrency>(getCurrencyUrl());
const activityType = createDictionary<IActivityType>(getActivityTypeUrl());
const country = createDictionary<ICountry>(getCountryUrl());
const accountType = createDictionary<IAccountType>(getAccountTypeUrl());
const gk = createDictionary<IGozContract>(getGkUrl());
const clientOfficial = createClientOffical(getClientOfficialUrl());
const branch = createClientBranchService(getBranchUrl());
const branchV2 = {
  ...createBranchV2Service(branchV2Url),
  bicPage: (metaData: IMetaData): Promise<ICollectionResponse<string>> =>
    request({
      url: `${branchV2Url}/bic-page`,
      method: 'POST',
      data: metadataToRequestParams(metaData),
    }).then(r => ({
      data: r.data.page,
      total: r.data.size,
    })),
};
const bankClient = {
  ...createBankClientService(bankClientUrl),
  getUserOrganizations: (): Promise<IBankClient[]> =>
    request({
      url: `${bankClientUrl}/user`,
    }).then((r: any) => r.data.data.data),
};
const fias = getDefaultSearchService(getFiasUrl());
const consent = getDefaultConsentService(getConsentUrl());
const cityVisitingManager = getCityVisitingManagerService(getCityVisitingManagerUrl());

export const dictionaryService = {
  okopf,
  okfs,
  currency,
  activityType,
  country,
  accountType,
  gk,
  bankClient,
  clientOfficial,
  branch,
  branchV2,
  fias,
  consent,
  cityVisitingManager,
};
