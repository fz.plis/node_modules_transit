import { conditions } from '../../constants';
import { SORT_DIRECTION } from '../../interfaces';
import type { IBaseEntity } from '../../interfaces';
import { getNewDictionaryService, request } from '../../utils';

const GATEWAY_URL = '/api/cert-enroll-client';
const CERT_ENROLL_URL = `${GATEWAY_URL}/unq-cert-request`;
const { getList } = getNewDictionaryService<IBaseEntity>(CERT_ENROLL_URL);

/** Перечисление значений для проверки возможности создания заявки, используется только в методах сервиса. */
enum CHECK_CREATE_NEW_RESULT {
  /** Возможно создать заявку. */
  VALID = 'VALID',
  /** Невозможно создать заявку. */
  INVALID = 'INVALID',
  /** В процессе определения произошла ошибка. */
  ERROR = 'ERROR',
}

/** Сервис по проверке доступности создания заявок НЭП. */
export const certEnrollService = {
  /**
   * Проверить возможность создания новой заявки.
   *
   * @param spoId Идентификатор СПО для которого делаем проверку.
   */
  checkCreateNew: async (spoId: string): Promise<boolean> => {
    const { data: response } = await request({
      url: `${CERT_ENROLL_URL}/check/can-create-new/${spoId}`,
    });

    return response.data === CHECK_CREATE_NEW_RESULT.VALID;
  },

  /**
   * Получить последнюю активную заявку.
   *
   * @param spoId Идентификатор СПО в разрезе которого получаем заявки.
   */
  getLastActiveRequest: async (spoId: string): Promise<IBaseEntity | undefined> => {
    const { data: response } = await getList({
      pageSize: 1,
      offset: 0,
      filters: {
        activeRequest: {
          value: true,
          condition: conditions.eq,
          fieldName: 'activeRequest',
        },
        orgOfficialCertId: {
          value: spoId,
          condition: conditions.eq,
          fieldName: 'orgOfficialCertId',
        },
      },
      sort: {
        createdAt: SORT_DIRECTION.DESC,
      },
    });

    return response.length > 0 ? response[0] : undefined;
  },
};
