import { snapshot } from '@platform/tools/istore';
import { verifySign } from '../../fake-data/open-account';
import type {
  IOpenAccount,
  IServerResp,
  IDigest,
  ISignVerify,
  ISign,
  IClientContractsResponse,
  ISaveResponse,
  IUniversalSignResponse,
} from '../../interfaces';
import { createOpenAccountService } from '../../services/helpers/open-request';
import { request, createSaveResponseHandler, openAccountTranslator } from '../../utils';

const transformValidation = createSaveResponseHandler(openAccountTranslator);

const OPEN_ACCOUNT_BASE_URL = '/api/opening-request';

export const OPEN_ACCOUNT_URL = `${OPEN_ACCOUNT_BASE_URL}/request`;

const SERVER_DELAY = 0;
const emulator = <TArgs = any[], TResult = any>(cb: (args: TArgs, ...other: any[]) => any, delay: number = SERVER_DELAY) => (
  arg: TArgs,
  ...other: any[]
): Promise<TResult> => new Promise(resolve => setTimeout(() => resolve(cb(arg, ...other)), delay));

export const openAccountService = {
  ...createOpenAccountService(OPEN_ACCOUNT_URL),
  newDocument: (): Promise<IOpenAccount> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/new`,
      method: 'GET',
    }).then(result => result.data.data),
  create: (openAccountDto: IOpenAccount): Promise<ISaveResponse<IOpenAccount>> => {
    const { id, userId, date, number, status, openedAccounts, ...dataToSend } = openAccountDto;

    return request({
      url: OPEN_ACCOUNT_URL,
      method: 'POST',
      data: { data: dataToSend },
    }).then(r => transformValidation<IOpenAccount>(r.data));
  },
  update({ id, userId, date, number, status, openedAccounts, ...dataToSend }: IOpenAccount): Promise<ISaveResponse<IOpenAccount>> {
    return request({
      url: OPEN_ACCOUNT_URL,
      method: 'PUT',
      data: { id, data: snapshot(dataToSend) },
    }).then(r => transformValidation<IOpenAccount>(r.data));
  },
  delete(requestId: string): Promise<IServerResp<IOpenAccount>> {
    return request({
      url: `${OPEN_ACCOUNT_URL}/delete`,
      method: 'POST',
      data: { data: requestId },
    }).then(result => {
      const { data, errorInfo } = result.data;

      return { errorInfo, data };
    });
  },
  digest: (id: string): Promise<IServerResp<IDigest>> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/generate-digest`,
      method: 'POST',
      data: { data: id },
    }).then(resp => resp.data),
  getSignData: (id: string): Promise<IUniversalSignResponse> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/generate-sign-data`,
      method: 'POST',
      data: { data: id },
    }).then(r => r.data.data),
  verifySign: emulator<string, IServerResp<ISignVerify>>(id => ({ data: verifySign(id) })),
  sign: (data: ISign): Promise<IOpenAccount> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/sign`,
      method: 'POST',
      data,
    }).then(r => r.data.data),
  removeSign: (id: string): Promise<IOpenAccount> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/remove-sign`,
      method: 'POST',
      data: { data: id },
    }).then(r => r.data.data),
  send: (id: string): Promise<IServerResp<IOpenAccount>> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/send`,
      method: 'POST',
      data: { data: id },
    }).then(r => ({
      data: r.data,
    })),
  getReservedAccounts: (id: string): Promise<IClientContractsResponse> =>
    request({
      url: `${OPEN_ACCOUNT_BASE_URL}/contracts/${id}`,
      method: 'GET',
      data: { id },
    }).then(r => r.data.data),
  getLocResource: () =>
    request({
      url: `${OPEN_ACCOUNT_BASE_URL}/i18n/ru_Ru`,
    }).then(x => x.data),
};
