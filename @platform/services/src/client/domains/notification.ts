import dayjs from 'dayjs';
import { object } from '@platform/tools/istore';
import { COMMON_STREAM_URL } from '../../constants';
import type { IRenderNotification, IUserNotification, INotificationDomain } from '../../interfaces';
import { USER_NOTIFICATION_POSTPONEMENT_MODE } from '../../interfaces';
import { userNotificationService } from '../../services-shared';
import { errorHandler, pollingRequest, preparedNotification } from '../../utils';

export const notificationDomain = object<INotificationDomain>({
  notificationCount: 0,
  importantCount: 1,
  importantNotificationMessage: undefined,
  importantNotificationUrl: COMMON_STREAM_URL.NOTIFICATIONS,
  showImportantNotification: false,
  importantNotifications: [],
})
  .withMethods(m => ({
    merge: m.merge,
    setNotificationCount: (count: number) => {
      m.merge({ notificationCount: count });
    },
    setImportantNotificationMessage: (message: IRenderNotification) => {
      m.merge({ importantNotificationMessage: message });
    },
    setImportantNotificationUrl: (url: string) => {
      m.merge({ importantNotificationUrl: url });
    },
    setShowImportantNotification: (isShow: boolean) => {
      m.merge({ showImportantNotification: isShow });
    },
    setShowImportantNotificationCount: (importantCount: number) => {
      m.merge({ importantCount });
    },
    setImportantNotifications: (notifications: IUserNotification[]) => {
      m.merge({ importantNotifications: notifications });
    },
  }))
  .withMethods((m, { getState }) => ({
    ...m,
    getImportantNotification: async (importantCount: number = 1000) => {
      try {
        const result = await userNotificationService.getImportantNotification({
          pageSize: importantCount,
          offset: 0,
        });

        m.setImportantNotifications(result);

        m.setShowImportantNotification(Boolean(result?.length));

        m.setImportantNotificationMessage(preparedNotification(result));

        if (result.length === 1) {
          m.setImportantNotificationUrl(`${COMMON_STREAM_URL.NOTIFICATIONS}?id=${result[0].id}`);
        } else if (result.length > 1) {
          m.setImportantNotificationUrl(`${COMMON_STREAM_URL.NOTIFICATIONS}/important`);
        }
      } catch (e) {
        errorHandler()(e);
      }
    },
    getPostponedNotifications: () => {
      const { importantNotifications } = getState();

      return importantNotifications.filter(notification => {
        const {
          executionDeadline,
          type: { postponementMode },
        } = notification;

        const isPermanent = postponementMode === USER_NOTIFICATION_POSTPONEMENT_MODE.PERMANENT;
        const canPostpone =
          postponementMode === USER_NOTIFICATION_POSTPONEMENT_MODE.TEMPORARY &&
          (!executionDeadline || dayjs(executionDeadline).isBefore(dayjs()));

        return isPermanent || canPostpone;
      });
    },
  }))
  .withMethods((m, { getState }) => ({
    ...m,
    getCount: () => {
      void pollingRequest(userNotificationService.getUnreadMessageCount, async data => {
        const currentCount = getState().notificationCount;

        if (currentCount !== data.count) {
          m.setNotificationCount(data.count);
          await m.getImportantNotification(data.importantCount);
        }
      });
    },
    canPostpone: () => m.getPostponedNotifications().length === getState().importantNotifications.length,
    postponeNotifications: () => {
      const postponeNotifications = m.getPostponedNotifications();

      return Promise.all(
        postponeNotifications.map(notification =>
          userNotificationService.postpone(notification.id, dayjs().add(1, 'd').format('YYYY-MM-DD'))
        )
      );
    },
    postponeNotification: (id: string) => userNotificationService.postpone(id, dayjs().add(1, 'd').format('YYYY-MM-DD')),
  }))
  .withName('main.notifications')
  .asPermanent();

notificationDomain.mount('main.notifications');
