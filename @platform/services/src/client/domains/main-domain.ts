import { domain, listValue, value } from '@platform/tools/istore';
import { dialog } from '@platform/ui';
import { notifications, authConfig, hasFatalError } from '../../domains/common-domain';
import { user as commonUser } from '../../domains/user';
import type { IBankClient, IParsedToken, IUserData } from '../../interfaces';
import { UserType, USER_CONTEXT_TYPE, USER_ROLE } from '../../interfaces';
import { parseToken } from '../../utils/token-helper';
import { UpdateContactInfo } from '../../views/update-contact-info';
import { UPDATE_CONTACT_INFO_TYPE } from '../../views/update-contact-info/constants';
import { dictionaryService } from '../services';
import { notificationDomain } from './notification';

let resolveProfile = (_: any) => {};
const profilePromise = new Promise<IParsedToken>(resolve => {
  resolveProfile = resolve;
});

export const user = commonUser
  .withMethods((m, { getState }) => ({
    ...m,
    loadOrganizations: () => {
      const { data } = getState();

      if (data) {
        return dictionaryService.bankClient.getUserOrganizations().then(orgs => {
          // eslint-disable-next-line @typescript-eslint/no-use-before-define
          mainDomain.methods.userOrganizations.set(orgs);

          if (orgs.length === 1) {
            // eslint-disable-next-line @typescript-eslint/no-use-before-define
            mainDomain.methods.selectedOrganizationId.set(orgs[0].id);
          }
        });
      }

      return Promise.resolve();
    },
    checkNotConfirmedData: () => !(getState().data?.phoneConfirmed && getState().data?.emailConfirmed),
  }))
  .withMethods((m, s) => ({
    ...m,
    load: ({
      userData,
      roles,
      userOrganizations,
    }: {
      userData: Partial<IUserData>;
      roles: USER_ROLE[];
      userOrganizations: IBankClient[];
    }) => {
      m.updateData(userData);

      m.setRoles(roles);

      // eslint-disable-next-line @typescript-eslint/no-use-before-define
      mainDomain.methods.userOrganizations.set(userOrganizations);

      if (userOrganizations.length === 1) {
        // eslint-disable-next-line @typescript-eslint/no-use-before-define
        mainDomain.methods.selectedOrganizationId.set(userOrganizations[0].id);
      }

      resolveProfile(userData);
    },
    asyncProfile: () => profilePromise,
    showNotConfirmedDataDialog: (onClose: () => void = () => {}) => {
      const noRestrictedRole = !s.getState().roles.includes(USER_ROLE.CLIENT_USER_RESTRICTED);

      if (noRestrictedRole && m.checkNotConfirmedData()) {
        dialog.show('update-contact-info', UpdateContactInfo, {}, onClose);
      }
    },
    updateUserPhone: (onClose: () => void = () => {}, disableResultStep?: boolean) => {
      dialog.show('update-userPhone', UpdateContactInfo, { updateType: UPDATE_CONTACT_INFO_TYPE.PHONE, disableResultStep }, onClose);
    },
    updateUserEmail: (onClose: () => void = () => {}) => {
      dialog.show('update-userEmail', UpdateContactInfo, { updateType: UPDATE_CONTACT_INFO_TYPE.EMAIL }, onClose);
    },
  }))
  .withMethods((m, { getState }) => ({
    ...m,
    setToken(token: string, authorities: string[]) {
      m.setToken(token, authorities);
      m.setUserType(UserType.CLIENT);
    },
    /**
     * Пока делаем проверку на юзера dbo3 через этот метод, ждем аналитики.
     */
    isDbo3User() {
      const tokenInfo = parseToken(getState().token);

      return tokenInfo.user_context_types.includes(USER_CONTEXT_TYPE.DBO);
    },
    getDefaultUserCertId() {
      const tokenInfo = parseToken(getState().token);

      return tokenInfo.default_sign_certificate;
    },
  }));

export const userOrganizations = listValue<IBankClient>([]).withName('main.user.organizations').asPermanent();

export const selectedOrganizationId = value<string>('').withName('main.user.selectedOrganization').asPermanent();

/**
 * @deprecated C 1 февраля Используйте useUser, useAuth вместо.
 */
export const mainDomain = domain({
  user,
  userOrganizations,
  selectedOrganizationId,
  notifications,
  authConfig,
  hasFatalError,
  notificationDomain,
});

mainDomain.mount('main');
