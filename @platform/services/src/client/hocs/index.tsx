import React from 'react';
import type { IParentCommunicator, IParentCommunicatorHandlers } from '@platform/communicator';
import { getParentCommunicator } from '@platform/communicator';
import { router } from '@platform/core';
import { LoaderOverlay, dialog, Font, NOTIFICATION_TYPE } from '@platform/ui';
import { locale } from '../../localization';
import { mainDomain } from '../domains';
import css from './styles.scss';

export interface IExternalModuleProps {
  name: string;
  src: string;
  children(children: React.ReactNode, onChangeOrgId: (org: string) => void): React.ReactNode;
}

export interface IExternalModuleState {
  isLoading: boolean;
  showLoaderByDemand: boolean;
  timeout: boolean;
}

export class ExternalModule extends React.Component<IExternalModuleProps, IExternalModuleState> {
  constructor(props: IExternalModuleProps) {
    super(props);

    this.communicator = getParentCommunicator(props.name, this.getHandlers());
  }

  state = {
    isLoading: true,
    showLoaderByDemand: false,
    timeout: false,
  };

  componentWillUnmount() {
    this.communicator.destroy();
  }

  communicator: IParentCommunicator;

  getHandlers = (): IParentCommunicatorHandlers => ({
    validateMetadata: () => true,
    handleHandshakeTimeout: () => {
      this.setState({ timeout: true });
    },
    handleFinishHandshake: () => {
      this.setState({ isLoading: false });
    },
    handleGetInfoRequest: () => ({
      profile: mainDomain.state.user.data!,
      token: mainDomain.state.user.token,
      roles: mainDomain.state.user.roles,
    }),
    handleGoToUrl: url => {
      router.methods.push(url);
    },
    handleGetOrganizationsRequest: () => mainDomain.state.userOrganizations,
    handleGetSelectedOrg: () => mainDomain.state.selectedOrganizationId,
    handleShowOveraly: () => this.setState({ showLoaderByDemand: true }),
    handleHideOverlay: () => this.setState({ showLoaderByDemand: false }),
    handleShowAlert: options =>
      new Promise<void>(resolve => {
        dialog.showAlert(options.text, {
          okButtonText: options.okButtonText,
          header: options.header,
          onClose: () => resolve(void 0),
          onOk: () => resolve(void 0),
        });
      }),
    handleShowConfirmation: options =>
      new Promise(resolve => {
        dialog.showConfirmation(options.text, () => resolve(true), {
          okButtonText: options.okButtonText,
          cancelButtonText: options.cancelButtonText,
          header: options.header,
          onClose: () => resolve(false),
          onCancel: () => resolve(false),
        });
      }),
    handleShowSuccessPushNotification: params => {
      mainDomain.methods.notifications.add({
        header: params.header,
        message: params.message,
        type: NOTIFICATION_TYPE.SUCCESS,
      });
    },
    handleShowErrorPushNotification: params => {
      mainDomain.methods.notifications.add({
        header: params.header,
        message: params.message,
        type: NOTIFICATION_TYPE.ERROR,
      });
    },
    handleShowWarningPushNotification: params => {
      mainDomain.methods.notifications.add({
        header: params.header,
        message: params.message,
        type: NOTIFICATION_TYPE.WARNING,
      });
    },
    handleShowInfoPushNotification: params => {
      mainDomain.methods.notifications.add({
        header: params.header,
        message: params.message,
        type: NOTIFICATION_TYPE.INFO,
      });
    },
  });

  handleChangeOfOrgId = (orgId: string) => {
    this.communicator.sendChangedOrgId(orgId);
  };

  render() {
    let content: React.ReactNode = null;

    const { name, src, children } = this.props;
    const { timeout, isLoading, showLoaderByDemand } = this.state;

    if (timeout) {
      content = <Font fill="WARNING">{locale.errors.loadingModule}</Font>;
    } else {
      content = (
        <>
          {(isLoading || showLoaderByDemand) && <LoaderOverlay opened />}
          <iframe className={css.externalModule} id={name} src={src} style={{ display: isLoading ? 'none' : 'block' }} />
        </>
      );
    }

    return typeof children === 'function' ? children(content, this.handleChangeOfOrgId) : content;
  }
}
