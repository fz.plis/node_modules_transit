import axios from 'axios';
import { listValue, guid, value, domain } from '@platform/tools/istore';
import { dialog } from '@platform/ui';
import type { IPollingOptions, ATTACHMENT_STATUS, ICheckUploadOptions, IFileUploadOptions, IFileBuffer } from '../interfaces';
import { ERROR, FILE_UPLOAD_STATE } from '../interfaces';
import { locale } from '../localization';
import { checkIfForbiddenFiles, errorHandler } from '../utils';

export interface IBaseAttachment {
  id: string;
  status: ATTACHMENT_STATUS;
}

export interface IAttachmentConfig<T extends IBaseAttachment> extends IPollingOptions {
  upload(file: File, options?: IFileUploadOptions): Promise<T>;
  checkUploadProgress(id: string, options?: ICheckUploadOptions): Promise<T>;
  maxSize?: number;
  /** Набор MIME для проверки файлов. */
  accept?: string[];
  multi?: boolean;
  /** Список расширений файлов, без точки. */
  supportFormats?: string[];
}

export const attachments = <T extends IBaseAttachment>(
  {
    upload,
    checkUploadProgress,
    maxSize,
    accept = [],
    interval = 500,
    maxTries = 600,
    multi = false,
    supportFormats = ['pdf'],
  }: IAttachmentConfig<T>,
  initialState: IFileBuffer[] = []
) => {
  const cancel: Record<string, () => void> = {};

  return domain({
    buffer: listValue<IFileBuffer>(initialState),
    error: value<any>(undefined),
    accept: value(accept),
    supportFormats: value(supportFormats),
  })
    .withMethods((m, s) => ({
      ...m,
      findIndexById(id: string) {
        return s.buffer.findIndex(x => x.id === id);
      },
      clearCancel(id: string) {
        // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
        delete cancel[id];
      },
    }))
    .withMethods(m => ({
      ...m,
      updateProgress(fileId: string, progress: number) {
        const fileIndex = m.findIndexById(fileId);

        if (fileIndex >= 0) {
          m.buffer.mergeToItem(fileIndex, { progress });
        }
      },
      setError(id: string, err: any, errText?: string) {
        m.clearCancel(id);

        const fileIndex = m.findIndexById(id);

        if (fileIndex >= 0) {
          m.buffer.mergeToItem(fileIndex, {
            isLoading: false,
            status: FILE_UPLOAD_STATE.UPLOADED,
            errorObj: err,
            progress: undefined,
            error: errText || locale.files.error.isMalware,
          });
        }
      },
      endLoad(id: string) {
        m.clearCancel(id);

        const fileIndex = m.findIndexById(id);

        if (fileIndex >= 0) {
          m.buffer.mergeToItem(fileIndex, { status: FILE_UPLOAD_STATE.UPLOADED, isLoading: false, progress: undefined });
        }
      },
      startMalwareCheck(id: string) {
        m.clearCancel(id);

        const fileIndex = m.findIndexById(id);

        if (fileIndex >= 0) {
          m.buffer.mergeToItem(fileIndex, { status: FILE_UPLOAD_STATE.MALWARE_CHECK, isLoading: true, progress: 0 });
        }
      },
      markAsMalware(id: string) {
        const fileIndex = m.findIndexById(id);

        if (fileIndex >= 0) {
          m.buffer.mergeToItem(fileIndex, {
            isLoading: false,
            isMalware: true,
            errorObj: locale.files.error.isMalware,
            error: locale.files.error.isMalware,
          });
        }
      },
    }))
    .withMethods(({ updateProgress, setError, ...m }, s) => {
      const remove = (file: IFileBuffer, onRemove: (info: IFileBuffer) => void = () => {}) => {
        if (file.isLoading) {
          const cancelFunc = cancel[file.id];

          if (cancelFunc) {
            m.clearCancel(file.id);

            cancelFunc();
          }
        }

        m.buffer.remove(file);
        onRemove(file);
      };

      return {
        setAccept: (newAccepts: string[]) => m.accept.set(newAccepts),
        setSupportFormats: (newSupportFormats: string[]) => m.supportFormats.set(newSupportFormats),
        remove,
        getAcceptFileType() {
          return s.accept.join(',');
        },
        fill(buffer: Array<Pick<IFileBuffer, 'error' | 'id' | 'name' | 'size' | 'status'>>) {
          m.buffer.add(buffer);
        },
        clearBuffer() {
          s.buffer.forEach(x => remove(x));
        },
        getMaxSize: () => maxSize,
        upload: (files: File[], onEndLoad: (entity: T) => void = () => {}) => {
          if (!multi && files.length > 1) {
            dialog.showAlert(locale.files.error.singleOnly);

            return;
          }

          const filesSize = files.reduce((sum, x) => sum + x.size, 0);
          const maxSizeReached = maxSize && filesSize > maxSize;

          if (maxSizeReached) {
            const { multi: mul, single } = locale.files.error.dataSizeExceeded;
            const message = multi ? mul : single;

            dialog.showAlert(
              message({
                size: maxSize!,
              })
            );

            return;
          } else if (filesSize === 0) {
            const { multi: mul, single } = locale.files.error.dataSizeNotEnough;
            const message = multi ? mul : single;

            dialog.showAlert(message);

            return;
          }

          if (checkIfForbiddenFiles(files, s.accept, s.supportFormats)) {
            // TODO: подумать над маппингом mime-type в расширения
            dialog.showAlert(`${locale.files.error.invalidExt} ${s.supportFormats.join(', ')}`);

            return;
          }

          files.forEach(file => {
            const id = guid();

            m.buffer.add({
              id,
              name: file.name,
              size: file.size,
              progress: 0,
              status: FILE_UPLOAD_STATE.UPLOADING,
              isLoading: true,
            });

            upload(file, {
              onUploadProgress: (e: ProgressEvent) => updateProgress(id, e.loaded / e.total),
              onCancel: c => {
                cancel[id] = c;
              },
            })
              .then(result => {
                m.startMalwareCheck(id);

                return checkUploadProgress(result.id, {
                  interval,
                  maxTries,
                  onUploadProgress: (e: ProgressEvent) => {
                    updateProgress(id, e.loaded / e.total);
                  },
                  onCancel: c => {
                    cancel[id] = c;
                  },
                });
              })
              .then(result => {
                m.endLoad(id);
                onEndLoad(result);
              })
              .catch(err => {
                if (axios.isCancel(err)) {
                  const fileInfo = s.buffer.find(x => x.id === id);

                  remove(fileInfo!);
                } else {
                  setError(id, err);
                }

                throw err;
              })
              .catch(
                errorHandler({
                  [ERROR.FILE_MALWARE]: () => {
                    m.markAsMalware(id);
                  },
                  [ERROR.UNSUPPORTED_MEDIA_TYPE]: err => {
                    setError(id, err, locale.files.error.invalidExtenstions);
                  },
                })
              );
          });
        },
      };
    });
};
