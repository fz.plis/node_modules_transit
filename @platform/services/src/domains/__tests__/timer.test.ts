import { setDefaultStore, Store } from '@platform/tools/istore';
import { timer } from '../timer';

describe('timer', () => {
  beforeEach(() => {
    setDefaultStore(new Store());
  });

  it('start', () => {
    const timerInst = timer(10).mount('test');

    timerInst.methods.start();
    expect(timerInst.state.isTicking).toBe(true);
    expect(timerInst.state.value).toBe(10);
  });

  it('stop', () => {
    const timerInst = timer(20).mount('test');

    timerInst.methods.stop();
    expect(timerInst.state.isTicking).toBe(false);
    expect(timerInst.state.value).toBe(20);
  });

  it('reset', () => {
    const timerInst = timer(5).mount('test');

    timerInst.methods.reset();
    expect(timerInst.state.isTicking).toBe(false);
    expect(timerInst.state.value).toBe(5);
  });

  it('continue', () => {
    const timerInst = timer(5).mount('test');

    timerInst.methods.continue();
    expect(timerInst.state.isTicking).toBe(true);
    expect(timerInst.state.value).toBe(5);
  });
});
