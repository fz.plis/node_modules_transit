import { setDefaultStore, Store } from '@platform/tools/istore';
import { step } from '../steps';

describe('steps', () => {
  const steps = ['1', '2', '3'];

  beforeEach(() => {
    setDefaultStore(new Store());
  });

  it('start', () => {
    const stepsInst = step(steps).mount('test');

    expect(stepsInst.methods.nextStep()).toBe('2');
  });
  it('prevStep', () => {
    const stepsInst = step(steps).mount('test');

    expect(stepsInst.methods.prevStep()).toBeUndefined();
  });
  it('isLastStep', () => {
    const stepsInst = step(steps, '3').mount('test');

    expect(stepsInst.methods.isLastStep()).toBe(true);
  });
  it('isFirstStep', () => {
    const stepsInst = step(steps, '1').mount('test');

    expect(stepsInst.methods.isFirstStep()).toBe(true);
  });
  it('jump', () => {
    const stepsInst = step(steps).mount('test');

    stepsInst.methods.jump('2');
    expect(stepsInst.state).toBe('2');
  });
  it('fn next with last step', () => {
    const stepsInst = step(steps, '3').mount('test');

    stepsInst.methods.next();
    expect(stepsInst.state).toBe('3');
  });
  it('next', () => {
    const stepsInst = step(steps).mount('test');

    stepsInst.methods.next();
    expect(stepsInst.state).toBe('2');
  });
  it('prev', () => {
    const stepsInst = step(steps, '2').mount('test');

    stepsInst.methods.prev();
    expect(stepsInst.state).toBe('1');
  });
  it('fn prev with first step', () => {
    const stepsInst = step(steps, '1').mount('test');

    stepsInst.methods.prev();
    expect(stepsInst.state).toBe('1');
  });
  it('getSteps', () => {
    const stepsInst = step(steps).mount('test');

    expect(stepsInst.methods.getSteps()).toBe(steps);
  });
});
