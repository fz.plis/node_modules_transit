import { validation as coreValidation } from '@platform/core';
import { validators } from '../utils';

export const validation: typeof coreValidation = rulesGetter => coreValidation(rulesGetter, validators);
