import { value } from '@platform/tools/istore';

export const step = <T>(steps: T[], initialStep?: T) =>
  value(initialStep || steps[0])
    .withMethods((m, { getState }) => ({
      ...m,
      getIndex: () => steps.indexOf(getState()),
    }))
    .withMethods(m => ({
      ...m,
      isLastStep: () => m.getIndex() === steps.length - 1,
      isFirstStep: () => m.getIndex() === 0,
      nextStep: () => steps[m.getIndex() + 1],
      prevStep: () => steps[m.getIndex() - 1],
    }))
    .withMethods(m => ({
      nextStep: m.nextStep,
      prevStep: m.prevStep,
      isLastStep: m.isLastStep,
      isFirstStep: m.isFirstStep,
      jump(nextStep: T) {
        if (steps.includes(nextStep)) {
          m.set(nextStep);
        }
      },
      next: () => {
        if (m.isLastStep()) {
          return;
        }

        m.set(m.nextStep());
      },
      prev: () => {
        if (m.isFirstStep()) {
          return;
        }

        m.set(m.prevStep());
      },
      getSteps: () => steps,
    }));
