import { value, listValue, domain } from '@platform/tools/istore';
import type { IAuthConfiguration } from '../interfaces';

/**
 * Используется для `getNotificationsDomain`.
 *
 * @deprecated Используется только для `istore` и будет удален.
 */
export interface INotificationsUnitSettings {
  autocloseTimeout?: number;
  permanentName?: string;
}

/**
 * Используется для `INotification`.
 *
 * @deprecated Используется только для `istore` и будет удален.
 */
export enum NOTIFICATION_TYPE {
  ERROR = 'ERROR',
  INFO = 'INFO',
  SUCCESS = 'SUCCESS',
  WARNING = 'WARNING',
}

/**
 * Используется для `getNotificationsDomain`.
 *
 * @deprecated Используется только для `istore` и будет удален.
 */
export interface INotification {
  date: string;
  header: string;
  link?: string;
  linkText?: string;
  message: string;
  onPush?(url: string): void;
  type: NOTIFICATION_TYPE;
}

/**
 * Возвращает домен нотификации.
 *
 * @param params Настройки домена нотификации.
 * @returns Домен нотификации.
 * @deprecated Конструкции с использованием istore не должны использоваться.
 */
export const getNotificationsDomain = (params: INotificationsUnitSettings) => {
  let visibleValue = listValue<INotification>();
  let archiveValue = listValue<INotification>();

  if (params.permanentName) {
    visibleValue = visibleValue.asPermanent().withName(`${params.permanentName}.visible`);
    archiveValue = visibleValue.asPermanent().withName(`${params.permanentName}.archive`);
  }

  return domain({
    visible: visibleValue,
    archive: archiveValue,
  }).withMethods(({ visible, archive }) => ({
    add: (data: Omit<INotification, 'date'>) => {
      const notification = {
        ...data,
        date: new Date().toISOString(),
      };

      visible.add(notification);
      archive.add(notification);
      setTimeout(() => visible.remove(notification), params.autocloseTimeout || 5000);
    },
    close: visible.remove,
  }));
};

export const notifications = getNotificationsDomain({
  permanentName: 'notifications',
}).mount('main.notifications');

export const authConfig = value<IAuthConfiguration | undefined>(undefined).withName('main.authConfig').asPermanent();

export const hasFatalError = value<boolean>(false).withName('main.hasFatalError').asPermanent();
