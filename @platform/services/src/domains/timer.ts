import { domain, value } from '@platform/tools/istore';

/**
 * Creates new count down timer domain.
 */
export const timer = (inititalValue: number) =>
  domain({
    isTicking: value<boolean>(false),
    value: value<number>(inititalValue),
  }).withMethods((m, s) => {
    const defaultInterval = 1000;

    // я убрал NodeJS.Timeout так как была ошибка  Type 'number' is not assignable to type 'Timeout', если будет время надо разобарться
    let jobId: any;

    let timingInterval: number = defaultInterval;

    const stop = () => {
      clearInterval(jobId);
      m.isTicking.set(false);
    };

    const reset = () => {
      stop();
      m.value.set(inititalValue);
    };

    const setTiming = () => {
      m.isTicking.set(true);

      jobId = setInterval(() => {
        try {
          if (s.value > 0) {
            m.value.set(s.value - 1);
          } else {
            stop();
          }
        } catch (e) {
          clearInterval(jobId);
        }
      }, timingInterval);
    };

    const start = (interval: number = defaultInterval) => {
      timingInterval = interval;
      reset();
      setTiming();
    };

    return {
      /**
       * Starts new countdown from `intitalValue` with `interval` ms.
       */
      start,
      /**
       * Stops current countdown.
       */
      stop,
      /**
       * Stops current countdown and reset timing to `intitalValue`.
       */
      reset,
      /**
       * Continues current countdown from last stop.
       *
       * If there is no countdown starts new countdown from `intitalValue` with 1000 ms interval.
       */
      continue: setTiming,
    };
  });
