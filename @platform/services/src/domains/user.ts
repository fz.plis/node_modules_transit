import { object } from '@platform/tools/istore';
import type { IActionWebInfo, IActionWithAuth, IButtonConfig, IUserData, USER_ROLE, UserType, USER_CONTEXT_TYPE } from '../interfaces';
import { clearLocalStorage, getOauthProvider, getToken } from '../utils';
import { CustomEvent } from '../utils/custom-event';
import { parseToken } from '../utils/token-helper';

export interface IUserDomainState {
  token: string;
  data?: IUserData;
  type?: UserType;
  roles: USER_ROLE[];
  /**
   * Список привилегий.
   */
  authorities: string[];
}

export const user = object<IUserDomainState>({
  token: '',
  roles: [],
  authorities: [],
})
  .withMethods((m, { getState }) => ({
    merge: m.merge,
    setToken: (token: string): void => {
      m.merge({ token });
    },
    setUserType: (userType: UserType) => {
      m.merge({ type: userType });
    },
    updateData: (data: Partial<IUserData>) => {
      const newUserData = { ...getState().data!, ...data };

      m.merge({ data: newUserData });

      const updateEvent = new CustomEvent('userChanging', {
        detail: newUserData,
      });

      window.dispatchEvent(updateEvent);
    },
    setRoles: (roles: USER_ROLE[]) => {
      if (!Array.isArray(roles)) {
        throw new Error(`invalid roles object, it should be an array of string but got ${JSON.stringify(roles)}`);
      }

      m.merge({ roles });
    },
    /**
     * Проверяет наличие хотя бы одной из указанных ролей у юзера.
     *
     * @deprecated Use `hasAuthority`.
     */
    isAvailableFor: (...roles: USER_ROLE[]) => getState().roles.some(r => roles.includes(r)),
    hasAuthority(...userAuthorities: string[]): boolean {
      const { authorities } = getState();

      return userAuthorities.every(x => authorities.includes(x));
    },
    hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean {
      const tkn = getToken()!;

      if (!tkn) {
        return false;
      }

      const { user_context_types: userContextTypes } = parseToken(tkn);

      return allowedContexts.some(x => userContextTypes.includes(x));
    },
  }))
  .withMethods(({ merge, ...m }) => {
    // используется для хранения id отложенного логаута, полученного с setTimeout.
    // убран NodeJS.Timeout, при сборке для стенда ошибка Type 'number'
    // is not assignable to type 'Timeout', хотя локально собирается без проблем.
    let timeout: any;

    const logout = () => {
      // при logout-е очищаем localStorage
      clearLocalStorage();

      clearTimeout(timeout);

      const url = getOauthProvider().buildLogoutUrl(window.location.href);

      window.location.replace(url);
    };

    return {
      ...m,
      logout,
      setToken: (token: string, authorities: string[]) => {
        m.setToken(token);
        merge({ authorities });
      },
      getAvailableActions: (actions: IActionWithAuth[]) =>
        actions.reduce<Array<IActionWebInfo<any, any>>>((result, curr) => {
          const { authorities, ...rest } = curr;

          if (authorities && m.hasAuthority(...authorities)) {
            result.push(rest);
          }

          return result;
        }, []),
    };
  })
  .withMethods(m => ({
    ...m,
    getAvailableActionsWithCategory: (actions: Record<string, IActionWithAuth[]>) => {
      const actionAuth: IButtonConfig<any, any> = {};

      Object.keys(actions).forEach(key => {
        actionAuth[key] = m.getAvailableActions(actions[key]);
      });

      return actionAuth;
    },
  }))
  .withName('main.user')
  .asPermanent();

export type UserDomain = typeof user;
