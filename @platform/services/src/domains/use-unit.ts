import React from 'react';
import type { Unit } from '@platform/tools/istore';

export const useUnit = <S, M>(unit: Unit<S, M>) => {
  const [state, setState] = React.useState<S>();

  React.useEffect(() =>
    unit.subscribe(s => {
      if (s !== state) {
        setState(s);
      }
    })
  );

  return unit.state;
};
