import type { IMetaData, ICollectionResponse, ISearchByName } from './interfaces';

export const fake = (..._: any[]): any => {
  throw new Error(
    // eslint-disable-next-line @eco/no-missing-localization
    `Вы пытаетесь использовать бандл common который не содержит реализации импортируемых объектов.
      Используйте @platform/services/client или  @platform/services/admin`
  );
};

export const fakeDictionaryService = <TRow extends { id: string }, TCreate = TRow>() => {
  const getList = (metaData: IMetaData): Promise<ICollectionResponse<TRow>> => fake(metaData);

  return {
    getList,
    getCounter: (metaData: IMetaData) => fake(metaData),
    get: (id: string): Promise<TRow> => fake(id),
    create: (data: TCreate) => fake(data),
    searchByName: <T extends keyof TRow>(params: ISearchByName, fieldName: T): Promise<ICollectionResponse<TRow>> =>
      fake(params, fieldName),
  };
};
