import { to } from '@platform/core';
import type { IBaseEntity, IActionConfig, IActionContext } from '../interfaces';

export interface ICopyService {
  copy(id: string): Promise<IBaseEntity>;
}

export const copy: IActionConfig<IActionContext<ICopyService>, IBaseEntity> = {
  action: ({ done, fatal, addSucceeded }, { service, showLoader, hideLoader }) => async ([doc]: [IBaseEntity]) => {
    showLoader({ delay: 500 });

    const [result, err] = await to(service.copy(doc.id));

    hideLoader();
    fatal(err);
    addSucceeded(result!);
    done();
  },
};
