import type { IActionConfig } from '@platform/core';
import { to } from '@platform/core';
import type { IOption } from '@platform/ui';
import { FILE_EXTENSIONS } from '../constants';
import type { IBaseEntity, IActionContext, IExportActionSettings, IExportService } from '../interfaces';
import { FORM_TYPE, EXPORT_TYPE } from '../interfaces';
import { showFile } from '../utils';

const runExport = (service: IExportService, settings: IExportActionSettings, documents: IBaseEntity[]) => {
  const extension = settings.extension.toUpperCase();
  const data = {
    docIds: documents.map(el => el.id),
    format: extension,
    splitReport: settings.splitReport,
  };

  return settings.selectedValue === EXPORT_TYPE.DOCUMENTS_LIST ? service.exportList(data) : service.export(data);
};

const DEFAULT_EXPORT_SETTINGS = {
  extension: FILE_EXTENSIONS.PDF,
  splitReport: true,
  selectedValue: EXPORT_TYPE.DOCUMENTS,
};

const exportWithoutDialogExport = <TContext extends IActionContext<IExportService>>(
  userSettings?: IExportActionSettings
): IActionConfig<TContext, any> => ({
  action: ({ addSucceeded, done, fatal }, { service }) => async documents => {
    const settings = { ...DEFAULT_EXPORT_SETTINGS, ...userSettings };
    const [result, err] = await to(runExport(service, settings, documents));

    fatal(err);
    addSucceeded(result!.data);

    showFile(result!.data, result!.fileName, result!.type);

    done();
  },
});

export interface IExportDialogOptions {
  options: IOption[];
  locale?: {
    header: string;
    text: string;
  };
  value?: any;
}

const exportWithDialog = <TContext extends IActionContext<IExportService>>({
  options,
  locale,
  value = EXPORT_TYPE.DOCUMENTS,
}: IExportDialogOptions): IActionConfig<TContext, any> => ({
  action: ({ addSucceeded, done, fatal }, { service, showExportSetting }) => async documents => {
    const [settings, closeOrError] = await to(showExportSetting(documents.length, FORM_TYPE.EXPORT, value, options, locale));

    if (closeOrError === true) {
      done();
    }

    fatal(closeOrError);

    const [result, err] = await to(runExport(service, settings!, documents));

    fatal(err);
    addSucceeded(result!.data);
    showFile(result!.data, result!.fileName, result!.type);
    done();
  },
});

export const exportDoc = {
  withDialog: exportWithDialog,
  withoutDialog: exportWithoutDialogExport,
};
