import type { IActionContext, IActionConfig, ISignService, ICertificateUserContext, IBaseEntity, IUserDataContext } from '../interfaces';
import { locale } from '../localization';
import { chainFatalHandler } from '../utils/actions';
import type { send as sendAction, ISendService } from './send';

export type SignAction = IActionConfig<IActionContext<ISignService> & ICertificateUserContext & IUserDataContext, IBaseEntity>;

export interface ISignSendConfig {
  sign: SignAction;
  send: typeof sendAction;
}

export type SignSendContext = IActionContext<ISendService & ISignService> & ICertificateUserContext;

const defaultFatalHandler = ({ showError }: SignSendContext) => showError(locale.action.error);

export const signSend = ({ sign, send }: ISignSendConfig): IActionConfig<SignSendContext, any> => {
  const { fatalHandler: fatalHandlerSign, succeededHandler: succeededHandlerSign, ...signConfig } = sign;
  const { fatalHandler: fatalHandlerSend, succeededHandler: succeededHandlerSend, ...sendConfig } = send;
  const fatalHandler = chainFatalHandler(fatalHandlerSign, fatalHandlerSend, defaultFatalHandler);

  return {
    fatalHandler,
    action: ({ done, fatal, addSucceeded, addFailed, execute }) => async (...args: any[]) => {
      let isSign = true;
      let succeededSign;

      // проверерка доступен ли документ для подписи. Если произашло исключение считаем что документ уже подписан
      try {
        sign.guardians!.forEach(guardian => {
          guardian(args[0]);
        });
      } catch (err) {
        isSign = false;
      }

      if (isSign) {
        const { failed, succeeded, error } = await execute(signConfig as any, ...args);

        succeededSign = succeeded;
        fatal(error);

        // считаем, что если нет успешно подписанных документов и "проваленных", то юзер нажал отмена в диалоге подписи
        if (failed.length === 0 && succeeded.length === 0) {
          done();

          return;
        }

        addFailed(...failed);
      }

      const { succeeded: sended, error: sendErr, failed: failedSend } = await execute(sendConfig as any, succeededSign || args[0]);

      fatal(sendErr);

      addSucceeded(...sended);
      addFailed(...failedSend);
      done();
    },
    succeededHandler: ({ showSuccess }, docs) => showSuccess(locale.signSend.succeded({ count: docs.length })),
    failedHandler: ({ showWarning }) => showWarning(locale.action.error),
  };
};
