import { getBankHistoryAction } from './bank-history';
import { checkAuthority } from './check-authority';
import { downloadXml } from './download-xml';
import { ecoSign } from './eco-sign';
import { history } from './history';
import { signInstallCrypto, checkSignInstallCrypto, getInstallCryptoAction } from './install-crypto';
import { openXml } from './open-xml';
import { repeatSend } from './repeat-send';
import { send } from './send';
import { signSend } from './sign-send';
import { universalSign } from './universal-sign';
import { universalSignVerify } from './universal-sign-verify';
import { validateSign } from './validate-sign';
import { verifySign } from './verify-sign';

export * from './base';
export * from './context';
export * from './export';
export * from './print';
export * from './copy';

export { IHistoryService } from './history';
export { SignAction } from './sign-send';

export const action = {
  universalSign,
  ecoSign,
  send,
  signSend,
  verifySign,
  validateSign,
  repeatSend,
  history,
  openXml,
  downloadXml,
  checkAuthority,
  universalSignVerify,
  signInstallCrypto,
  checkSignInstallCrypto,
  getInstallCryptoAction,
  getBankHistoryAction,
};
