import type { IActionConfig } from '@platform/core';
import { to } from '@platform/core';
import type { IOption } from '@platform/ui';
import { FILE_EXTENSIONS } from '../constants/common';
import type { IActionContext, IExportActionSettings, IPrintService, IPrintResponseDoc } from '../interfaces';
import { FORM_TYPE, EXPORT_TYPE } from '../interfaces';
import { locale } from '../localization';
import { printBase64 } from '../utils';

const DEFAULT_PRINT_SETTINGS = {
  extension: FILE_EXTENSIONS.HTML,
  splitReport: true,
  selectedValue: EXPORT_TYPE.DOCUMENTS,
};

const showLimitCountDialog = async (
  showConfirm: any,
  settings: IExportActionSettings,
  docLength: number,
  maxPrintDoc: number
): Promise<boolean> => {
  const asList = settings.selectedValue === EXPORT_TYPE.DOCUMENTS_LIST;

  if (!asList && docLength > maxPrintDoc) {
    const [, cancel] = await to(
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      showConfirm(
        locale.print.confirmCount.context({
          doc: locale.print.documents.count({ count: maxPrintDoc }),
          count: maxPrintDoc,
          total: docLength,
        }),
        {
          header: locale.print.confirmCount.header,
          okButtonText: locale.print.text,
        }
      )
    );

    if (cancel) {
      return Promise.resolve(false);
    }
  }

  return Promise.resolve(true);
};

const printOneBlock = <TContext extends IActionContext<IPrintService>>(
  service: IPrintService,
  printCount: number,
  settings?: IExportActionSettings
): IActionConfig<TContext, IPrintResponseDoc> => ({
  action: ({ addSucceeded, done, fatal, execute }, { showConfirm, maxPrintDoc }) => async (docs: any[]) => {
    const asList = settings!.selectedValue === EXPORT_TYPE.DOCUMENTS_LIST;
    const d = docs.length > maxPrintDoc && !asList ? docs.slice(0, maxPrintDoc) : docs.slice(0);
    const docIds = d.map(el => `${el.id}`);

    const extension = FILE_EXTENSIONS.HTML.toUpperCase();
    const data = {
      docIds,
      format: extension,
      splitReport: settings!.splitReport,
    };
    const [result, err] = await to(asList ? service.printList(data) : service.print(data));

    fatal(err);
    addSucceeded(result!);
    printBase64(result!.content, extension);

    if (docs.length > maxPrintDoc && !asList) {
      const count = printCount * maxPrintDoc + d.length;
      const doc = locale.print.documents.count({ count });
      const [, cancel] = await to(
        showConfirm(
          `${locale.print.confirmPrint.context({
            count,
            total: docs.length,
            doc,
          })}, ${locale.print.confirmLastPrint.context({
            last: docs.length - count,
          })}`,
          {
            header: locale.print.confirmCount.header,
            okButtonText: locale.action.yes,
          }
        )
      );

      if (cancel) {
        done();
      }

      const { succeeded, error } = await execute(printOneBlock(service, printCount, settings));

      fatal(error);
      addSucceeded(...(succeeded as any));
      // eslint-disable-next-line no-param-reassign
      printCount += 1;
    } else {
      done();
    }
  },
});

const printWithoutDialog = <TContext extends IActionContext<IPrintService>>(
  externalSettings?: IExportActionSettings
): IActionConfig<TContext, IPrintResponseDoc> => ({
  action: ({ addSucceeded, execute, fatal, done }, { service }) => async (documents: any[]) => {
    const settings = { ...DEFAULT_PRINT_SETTINGS, ...externalSettings };
    const { succeeded, error } = await execute(printOneBlock(service, 0, settings), documents);

    fatal(error);
    addSucceeded(...succeeded);
    done();
  },
});

const printWithDialog = <TContext extends IActionContext<IPrintService>>(
  exportOptions: IOption[],
  settingsLocale?: any
): IActionConfig<TContext, IPrintResponseDoc> => ({
  action: ({ addSucceeded, done, fatal, execute }, { service, showConfirm, showExportSetting, maxPrintDoc }) => async (
    documents: any[]
  ) => {
    const [settings, closeOrError] = await to(
      showExportSetting(documents.length, FORM_TYPE.PRINT, EXPORT_TYPE.DOCUMENTS, exportOptions, settingsLocale)
    );

    if (closeOrError === true) {
      done();
    }

    fatal(closeOrError);

    const [resp] = await to(showLimitCountDialog(showConfirm, settings!, documents.length, maxPrintDoc));

    if (!resp) {
      done();
    }

    const { succeeded, error } = await execute(printOneBlock(service, 0, settings!), documents);

    fatal(error);
    addSucceeded(...succeeded);
  },
});

export const printDoc = {
  withDialog: printWithDialog,
  withoutDialog: printWithoutDialog,
};
