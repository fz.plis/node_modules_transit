import type { IActionConfig } from '@platform/core';
import { to } from '@platform/core';
import { getCryptoModule } from '../crypto';
import type { IBaseContext, IBaseEntity } from '../interfaces';
import { locale } from '../localization';
import { isClientScope } from '../utils';

export const getInstallCryptoAction = (getHeader?: () => string): IActionConfig<IBaseContext, IBaseEntity> => ({
  action: ({ done, fatal }, { openCryptoInstaller, showLoader, hideLoader }) => async () => {
    const crypto = getCryptoModule();

    if (!isClientScope()) {
      done();
    }

    showLoader();

    const [isInstalled] = await to(crypto.installationCheck());

    hideLoader();

    if (isInstalled) {
      done();
    }

    const res = await openCryptoInstaller({ header: getHeader?.() });

    if (!res) {
      fatal(true);
    }

    done();
  },
});

export const checkSignInstallCrypto = getInstallCryptoAction(() => locale.cryptoInstaller.checkSignTitle);
export const signInstallCrypto = getInstallCryptoAction();
