import type { FilterInfo } from '@platform/core';
import { to } from '@platform/core';
import type {
  IActionConfig,
  IActionContext,
  IAuthorityResponse,
  IAuthorityResult,
  IBankClient,
  ICertificateUserContext,
  ISignService,
  IUserCertificate,
  DOCUMENT_TYPE_CODE,
  IUserDataContext,
} from '../interfaces';
import { AUTHORITY_VALIDATORS, ERROR, USER_CONTEXT_TYPE } from '../interfaces';

const CLIENT_UUID_FIELD = 'clientUuid';

const AuthoritiesKeys: Record<AUTHORITY_VALIDATORS, string> = {
  [AUTHORITY_VALIDATORS.EIO]: 'hasEioAuthority',
  [AUTHORITY_VALIDATORS.CONSTITUTION]: 'hasConstitutionAuthority',
  [AUTHORITY_VALIDATORS.CERTIFICATE]: 'hasCertificateAuthority',
};

const checkCertificateAuthority = (authorities: IAuthorityResponse[], certs: IUserCertificate[]) =>
  authorities.reduce<IAuthorityResult[]>((acc, val) => {
    const certificate = certs.find(cert => cert.id === val.certificateId);

    const checkResult = val.validatorResults.reduce<{ [key in AUTHORITY_VALIDATORS]?: boolean }>(
      (validatorsResults, { result, type }) => ({ ...validatorsResults, [AuthoritiesKeys[type]]: result }),
      {}
    ) as IAuthorityResult['checkResult'];

    const errorMessages = val.validatorResults.reduce<{ [key in AUTHORITY_VALIDATORS]?: string }>(
      (messages, { type, message }) => ({
        ...messages,
        [AUTHORITY_VALIDATORS[type]]: message ?? '',
      }),
      {}
    );

    const authority: IAuthorityResult = {
      checkResult,
      errorMessages,
      error: !val.result,
      certificate: certificate!,
      authorizingDocument: val.authorizingDocument,
      clientPosition: val.clientPosition,
    };

    return [...acc, authority];
  }, []);

export const checkAuthority: IActionConfig<
  IActionContext<ISignService> & ICertificateUserContext & IUserDataContext,
  IAuthorityResult[]
> = {
  action: ({ done, fatal, addSucceeded, addFailed }, { certificateUserService, showLoader, hideLoader, getUserInfo }) => async (
    bankClient: IBankClient,
    productCodes: string[] = [],
    validators: AUTHORITY_VALIDATORS[] = [AUTHORITY_VALIDATORS.EIO, AUTHORITY_VALIDATORS.CERTIFICATE, AUTHORITY_VALIDATORS.CONSTITUTION],
    productType?: DOCUMENT_TYPE_CODE
  ) => {
    const userInfo = getUserInfo();

    showLoader();

    const [resp, err]: any = await to(
      certificateUserService.getList({
        pageSize: 10,
        offset: 0,
        filters: {
          clientUuid: ({
            fieldName: CLIENT_UUID_FIELD,
            value: bankClient.clientId,
          } as unknown) as FilterInfo,
        },
      })
    );

    fatal(err);

    const certs: IUserCertificate[] = resp.data;

    if (certs.length === 0) {
      hideLoader();
      fatal({ response: { data: { errorInfo: { code: ERROR.CRYPTO_CERTS_NOT_FOUND } } } });
    }

    const [authorities, authoritiesError] = await to(
      certificateUserService.checkAuthority({
        certificateIds: certs.map(cert => cert.id),
        clientAbsId: bankClient.absId,
        clientEskId: bankClient.externalClientGuid,
        clientInnKio: bankClient.innKio,
        clientKpp: bankClient.kpp,
        clientName: bankClient.fullName,
        clientOgrn: bankClient.ogrnOgrip,
        documentType: productType,
        validators: userInfo.user_context_types.includes(USER_CONTEXT_TYPE.DBO) ? [AUTHORITY_VALIDATORS.EIO] : validators,
        productCodes,
      })
    );

    hideLoader();

    if (!authorities || authoritiesError) {
      fatal(authoritiesError);

      return;
    }

    const checkResults = checkCertificateAuthority(authorities, certs);
    const successResults = checkResults.filter(res => !res.error);

    addSucceeded(successResults);

    const errorResults = checkResults.filter(res => res.error);

    addFailed(errorResults);
    done();
  },
};
