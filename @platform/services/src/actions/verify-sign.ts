import { groupAction, to } from '@platform/core';
import { getCryptoModule } from '../crypto';
import type {
  IServerResp,
  IDigest,
  IActionContext,
  IBaseEntity,
  ISignVerify,
  IActionConfig,
  ICertificateUserContext,
  ISignatureDoc,
} from '../interfaces';
import { locale } from '../localization';
import type { IVersifySignResult } from '../views';
import { checkSignInstallCrypto } from './install-crypto';

export interface ISignRequestData {
  certificateId: string;
  digest: string;
  documentId: string;
  signTypeId: string;
  signature: string;
}

export interface ISignService {
  digest(id: string): Promise<IServerResp<IDigest>>;
  sign(data: ISignRequestData): Promise<IBaseEntity>;
  verifySign(id: string): Promise<IServerResp<ISignVerify>>;
}

export const verifySign: IActionConfig<IActionContext<ISignService> & ICertificateUserContext, IVersifySignResult<any>> = {
  action: ({ done, fatal, addSucceeded, execute }, { service, certificateUserService, showLoader, hideLoader }) => async (
    docs: Array<IBaseEntity & { signature: ISignatureDoc; signCheckAt?: string }>
  ) => {
    const triggerFatal = (errors: any) => {
      if (errors) {
        hideLoader();
        fatal(errors);
      }
    };

    const installRes = await execute(checkSignInstallCrypto);

    if (installRes.error) {
      done();
    }

    showLoader();

    // берем дайджесты
    const [digests = [], err] = await to(
      Promise.all(
        docs.map(x => {
          // Если версия СПП = 1, то возвращаем digest из ДТО
          if (x.signature.sppVersion === 1) {
            return { data: { value: x.signature.value.digest, signFields: [] } };
          }

          // берем дайджесты на сервере
          return service.digest(x.id);
        })
      )
    );

    triggerFatal(err);

    // Получаем данные по УЦ и СОС
    const [certificateAuthorities, errCertAuthorities] = await to(
      Promise.all(
        docs.map(x =>
          certificateUserService.getVerifyData({
            // Если дата проверки подписи банком не передана, проверяем на текущую дату
            date: x.signCheckAt ?? new Date().toISOString(),
            verifySignFields: [
              {
                certificateId: x.signature.certificate.id,
                signData: x.signature.value.signature,
                signId: x.signature.id,
              },
            ],
          })
        )
      )
    );

    triggerFatal(errCertAuthorities);

    // обращаемся к криптомодулю
    const crypto = getCryptoModule();

    // Проверяем подписи в криптомодуле
    const [valids, cadesEss] = await to(
      Promise.all(
        docs.map((x, idx) => {
          const authorities = certificateAuthorities![idx][x.signature.id];

          return crypto.verifySign(
            x.signature.value.signature,
            [digests![idx].data.value],
            x.signature.certificate.value,
            authorities?.caCertificates,
            authorities?.crls
          );
        })
      )
    );

    triggerFatal(cadesEss);

    // собираем результаты проверок
    addSucceeded(
      ...docs.map((x, index) => ({
        digest: {
          signFields: digests![index].data.signFields,
          value: digests![index].data.value,
        },
        signature: {
          signedAt: x.signature.signedAt,
          signature: x.signature.certificate.value,
        },
        fio: x.signature.certificate.subject,
        issuer: valids![index].data?.issuer,
        serialNumber: x.signature.certificate.serialNumber,
        validFrom: x.signature.certificate.validFrom,
        validTo: x.signature.certificate.validTo,
        valid: !!valids![index].valid,
        document: x,
        error: valids![index].error,
      }))
    );
    hideLoader();
    done();
  },
  succeededHandler: ({ showSignVerify }, results) => showSignVerify(results),
  fatalHandler: ({ showError }) => showError(locale.error.text, locale.verifySign.fatal),
  failedHandler: ({ showWarning }, docs) => showWarning(locale.verifySign.failed({ count: docs.length })),
  guardians: [groupAction],
};
