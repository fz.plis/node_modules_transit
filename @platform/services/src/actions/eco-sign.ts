import { groupAction, to } from '@platform/core';
import type {
  IActionContext,
  IBaseEntity,
  IActionConfig,
  ISignService,
  ICertificateUserContext,
  IUserDataContext,
  IUserCertificate,
  IUniversalSignResultSummary,
  IUniversalSignEntity,
} from '../interfaces';
import { ERROR, SIGN_RESULT_CODE } from '../interfaces';
import { locale } from '../localization';
import { signInstallCrypto } from './install-crypto';
import { universalSign } from './universal-sign';

const { succeededHandler, ...signAction } = universalSign;

/**
 * Новый экшн для подписи. Использует под капотом компонент универсальной подписи.
 */
export const ecoSign: IActionConfig<IActionContext<ISignService> & ICertificateUserContext & IUserDataContext, IBaseEntity> = {
  action: ({ done, fatal, addSucceeded, execute }, { service, showLoader, hideLoader }) => async (
    docs: IBaseEntity[],
    defaultCertId?: string,
    detached?: boolean
  ) => {
    const triggerFatal = (err: any) => {
      if (err) {
        hideLoader();
        fatal(err);
      } else {
        fatal(err);
      }
    };

    const installRes = await execute(signInstallCrypto);

    if (installRes.error) {
      done();
    }

    const defaultError = { response: { data: { errorInfo: { code: ERROR.CRYPTO_INTERNAL_SERVER_ERROR } } } };
    const initData: { certificates: IUserCertificate[]; signDocuments: IUniversalSignEntity[] } = { certificates: [], signDocuments: [] };

    // Получаем данные по документу для универсальной подписи
    showLoader();

    const [signData, signDataErr] = await to(Promise.all(docs.map(({ id }) => service.getSignData?.(id))));

    hideLoader();

    triggerFatal(signDataErr);

    // Создаем общие коллекции для всех сертификатов и документов
    const { certificates, signDocuments } =
      signData?.reduce((acc, data) => {
        // Убираем дубликаты сертификатов
        const certs = [...new Set([...(acc?.certificates || []), ...(data?.certificates || [])])];

        return { certificates: certs, signDocuments: [...(acc?.signDocuments || []), ...(data?.signDocuments || [])] };
      }, initData) ?? initData;

    if (certificates.length === 0) {
      triggerFatal({ response: { data: { errorInfo: { code: ERROR.CRYPTO_CERTS_NOT_FOUND } } } });
    }

    // Отправляем данные в экшн универсальной подписи
    const { succeeded = [] }: { succeeded: IUniversalSignResultSummary[] } = await execute(
      signAction,
      signDocuments,
      certificates,
      defaultCertId,
      detached
    );

    if (succeeded.length === 0) {
      done();
    }

    if (succeeded[0].resultCode === SIGN_RESULT_CODE.ERROR) {
      triggerFatal(defaultError);
    }

    const { certificateId, sign: signedData } = succeeded[0];

    const signTypeId = '33d59944-5f13-4d56-bcef-f121f2462a96'; // TODO Разобраться зачем бэку нужен этот параметр

    showLoader();

    // отправляем результат подписи документа на бек
    const [signResult, signErr] = await to(
      Promise.all(
        signedData.map(({ documentId, signDataResult = [] }) =>
          service.sign({
            documentId,
            signature: signDataResult[0]?.signature,
            digest: signDataResult[0]?.digest,
            signTypeId,
            certificateId: certificateId!,
          })
        )
      )
    );

    triggerFatal(signErr);

    signResult?.forEach(result => {
      // С бэка может прилететь 200 с сообщением signature is invalid
      if (result) {
        addSucceeded(result);
      } else {
        triggerFatal(defaultError);
      }
    });

    hideLoader();

    done();
  },
  fatalHandler: ({ showSignError }, context) => {
    showSignError(context);
  },
  failedHandler: ({ showWarning }, docs) => showWarning(locale.sign.failed({ count: docs.length })),
  guardians: [groupAction],
};
