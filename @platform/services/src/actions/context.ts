import React from 'react';
import { router as coreRouter } from '@platform/core';
import type { IConfirmationExtraParams } from '@platform/ui';
import { dialog, appendToPortal, deleteFromPortal, LoaderOverlay } from '@platform/ui';
import { SignCheckDialog, CryptoInstaller } from '../components';
import type {
  IBaseContext,
  IRouterContext,
  FORM_TYPE,
  IExportActionSettings,
  IUserCertificate,
  IParsedToken,
  IUniversalSignEntity,
  IUniversalSignVerifyActionResult,
  ISignedAttachment,
  IBaseEntity,
  ShowAdminHistoryDialogParams,
} from '../interfaces';
import { SIGN_DATA_TYPE } from '../interfaces';
import { parseToken, showSignError } from '../utils';
import type { RenderDocument, RenderSummary, IVersifySignResult } from '../views';
import { Sign, VerifySign, UniversalSign, BankHistoryDialog } from '../views';
import { ExportDialog } from '../views/Export';

export interface ISignConfig<TDoc = any> {
  /**
   * Функция, возвращающая представление просмотра документа при подписи.
   */
  renderDocument: RenderDocument<TDoc>;
  /**
   * Функция, возвращающая краткое содержаниие всех документов при подписи (например, выбрано n документов на сумму x).
   */
  renderSummary?: RenderSummary<TDoc>;
  /**
   * Функция возвращающая наименование документа (нужно для проверки подписи).
   */
  docNameGetter(doc: TDoc): string;
}

/**
 * Параметры контекста.
 */
export interface IContextOptions {
  /** Параметры, которые будут переданы в модалку подписи - `signDialog`. */
  sign?: ISignConfig;

  /** Параметры кастомного роутера.  Если не передавать, то будет использован роутер по умолчанию. */
  router?: IRouterContext;

  /**
   * Максимальное количество печатаемых документов.
   *
   * @default 30
   */
  maxPrintDoc?: number;
}

const LOADER_NAME = 'action-loader';
const signNotImplemented = 'You must pass `sign` field to `createContext` for using `signDialog` function';

const { connectToHistoryInstance, getHistoryInstance, ...routerMethods } = coreRouter.methods;

const baseContext = {
  showExportSetting: (numberOfDocuments: number, formType: FORM_TYPE, defaultValue: string, options: any, userlocale: any) =>
    new Promise((resolve, reject) =>
      dialog.show(
        'claimsExport',
        ExportDialog,
        {
          numberOfDocuments,
          onExportClick: resolve,
          formType,
          defaultValue,
          options,
          userlocale,
        },
        () => reject(true)
      )
    ),
};

const printContext = {
  showExportSetting: (
    numberOfDocuments: number,
    formType: FORM_TYPE,
    defaultValue: string,
    options: any,
    userlocale: any
    // eslint-disable-next-line sonarjs/no-identical-functions
  ): Promise<IExportActionSettings> =>
    // eslint-disable-next-line sonarjs/no-identical-functions
    new Promise((resolve, reject) =>
      dialog.show(
        'claimsExport',
        ExportDialog,
        {
          numberOfDocuments,
          onExportClick: resolve,
          formType,
          defaultValue,
          options,
          userlocale,
        },
        () => reject(true)
      )
    ),
};

let parsedToken: IParsedToken;

export const createContext = ({ sign, router = routerMethods, maxPrintDoc = 30 }: IContextOptions = {}): IBaseContext => ({
  ...baseContext,
  ...printContext,
  maxPrintDoc,
  router,
  getUserInfo: () => {
    const token = parsedToken || parseToken(localStorage.getItem('token')!);

    return token;
  },
  showSignError: result => showSignError(result.error),
  showSuccess: (title: string, content?: string, params?: IConfirmationExtraParams) =>
    dialog.showSuccessAlert(content || '', { header: title, ...params }),
  showError: (title: string, content?: string) => dialog.showAlert(content || '', { header: title }),
  showWarning: (title: string, content?: string) => dialog.showAlert(content || '', { header: title }),
  showConfirm: (content: string, params?: IConfirmationExtraParams) =>
    new Promise<void>((resolve, reject) =>
      dialog.showConfirmation(content, resolve, {
        ...params,
        onClose: () => reject(true),
      })
    ),
  signDialog(documents: any[], certs: IUserCertificate[], selectedCertificate?: IUserCertificate) {
    if (!sign) {
      throw new Error(signNotImplemented);
    }

    const { docNameGetter, ...signDialogProps } = sign;

    return new Promise((resolve, reject) =>
      dialog.show(
        'sign-dialog',
        Sign,
        {
          ...signDialogProps,
          documents,
          certs,
          selectedCertificate,
          onSignClick(thumbprint: string, cert: IUserCertificate) {
            resolve({ thumbprint, cert });
          },
        },
        () => reject(true)
      )
    );
  },
  universalSignDialog(signDocuments: IUniversalSignEntity[], certs: IUserCertificate[], selectedCertificate?: IUserCertificate) {
    if (!sign) {
      throw new Error(signNotImplemented);
    }

    const { docNameGetter, ...signDialogProps } = sign;

    return new Promise((resolve, reject) =>
      dialog.show(
        'sign-dialog',
        UniversalSign,
        {
          ...signDialogProps,
          documents: signDocuments,
          certs,
          selectedCertificate,
          onSignClick(thumbprint: string, cert: IUserCertificate) {
            resolve({ thumbprint, cert });
          },
        },
        () => reject(true)
      )
    );
  },
  showSignVerify(data: Array<IVersifySignResult<any>>) {
    if (!sign) {
      throw new Error(signNotImplemented);
    }

    dialog.show('verify-sign', VerifySign, { data, docNameGetter: sign.docNameGetter });
  },
  showLoader(options = {}) {
    appendToPortal(LOADER_NAME, () => React.createElement(LoaderOverlay, { ...options, opened: true }));
  },
  hideLoader() {
    deleteFromPortal(LOADER_NAME);
  },
  universalSignVerifyDialog([data]: IUniversalSignVerifyActionResult[][]) {
    if (!sign) {
      throw new Error(signNotImplemented);
    }

    dialog.show('universal-sign-verify-dialog', SignCheckDialog, {
      data: data.map(({ signObject, type, result }) => ({
        documentTitle:
          // eslint-disable-next-line @eco/no-missing-localization
          type === SIGN_DATA_TYPE.DOCUMENT ? sign.docNameGetter(signObject) : `Вложение ${(signObject as ISignedAttachment).fileName!}`,
        signInfos: result,
      })),
    });
  },
  openCryptoInstaller: (params = {}) =>
    new Promise(resolve => dialog.show('cryptoInstaller', CryptoInstaller, params, status => resolve(status))),
  showAdminHistoryDialog: <TDoc extends IBaseEntity>({
    historyList,
    doc,
    getHeader,
    getBankClientName,
    getStatusLabel,
    onUserNameClick,
  }: ShowAdminHistoryDialogParams<TDoc>) =>
    dialog.show('bankHistory', BankHistoryDialog, {
      data: historyList,
      header: getHeader(doc),
      bankClient: getBankClientName(doc),
      getStatusLabel,
      onUserNameClick,
    }),
});
