import { groupAction, to } from '@platform/core';
import { getCryptoModule, signErrorDialog } from '../crypto';
import type {
  IActionContext,
  IUniversalSignEntity,
  IActionConfig,
  ISignService,
  ICertificateUserContext,
  IUserDataContext,
  IUserCertificate,
  IUniversalSignResultEntity,
  IUniversalSignDataResult,
  IUniversalSignResultSummary,
} from '../interfaces';
import { SIGN_RESULT_CODE } from '../interfaces';
import { locale } from '../localization';

export const universalSign: IActionConfig<
  IActionContext<ISignService> & ICertificateUserContext & IUserDataContext,
  IUniversalSignResultSummary
> = {
  action: ({ done, fatal, addSucceeded }, { universalSignDialog, showLoader, hideLoader, defaultUserCertId }) => async (
    docs: IUniversalSignEntity[],
    certs: IUserCertificate[],
    defaultCertId?: string,
    detached?: boolean
  ) => {
    const triggerFatal = (err: any) => {
      if (err) {
        hideLoader();
        fatal(err);
      } else {
        fatal(err);
      }
    };

    // обращаемся к криптомодулю
    const crypto = getCryptoModule();

    const signCerts = defaultCertId ? certs.filter(x => x.id === defaultCertId) : certs;
    const selectedCert = defaultCertId ? signCerts?.[0] : signCerts.find((cert: IUserCertificate) => cert.id === defaultUserCertId);

    const [result, cancel] = await to(universalSignDialog(docs, signCerts, selectedCert));

    if (!result || cancel) {
      hideLoader();
      done();

      return;
    }

    const { cert, thumbprint } = result;

    // инициализируем криптопровайдер
    const [, initErr] = await to(crypto.init({ certData: cert.data, containerNameLocation: cert.containerNameLocation }));

    triggerFatal(initErr);

    const signResults: IUniversalSignResultEntity[] = [];
    let userCancelError;
    let resultCode;

    // Перебор всех подписываемых документов
    for (const signData of docs) {
      const digests = signData.signDataList?.map(({ digest }) => digest);

      showLoader();

      // eslint-disable-next-line no-await-in-loop
      const [signatures, signError] = await to(crypto.batchSign(thumbprint, digests, undefined, detached));

      hideLoader();

      // Если при подписи очередного документа возникла ошибка,
      // показываем диалог с выбором: Продолжить подпись других документов или Прервать процесс подписи
      if (signError) {
        const error = signError?.response?.data?.data;
        // eslint-disable-next-line no-await-in-loop
        const [_, no] = await to(signErrorDialog.universalSignError(signData.signDataList, error));

        signResults.push({ documentId: signData.documentId, resultCode: SIGN_RESULT_CODE.ERROR, error: error.text });

        if (no) {
          resultCode = SIGN_RESULT_CODE.ERROR;
          userCancelError = locale.signDialog.userCancelErrorMessage;
          break;
        }
      } else {
        const signDataResult: IUniversalSignDataResult[] =
          signatures?.map((signature, idx) => ({
            signDate: new Date().toISOString(),
            signature,
            resultCode: SIGN_RESULT_CODE.SUCCESS,
            id: signData.signDataList[idx].id,
            type: signData.signDataList[idx].type,
            digest: signData.signDataList[idx].digest,
          })) || [];

        signResults.push({ documentId: signData.documentId, resultCode: SIGN_RESULT_CODE.SUCCESS, signDataResult });
      }
    }

    const failedResults = signResults.filter(res => res.resultCode === SIGN_RESULT_CODE.ERROR);
    const hasSuccessResults = signResults.length !== failedResults.length;

    // Ставим успешный код завершения если операция подписи хотя бы одного документа завершилась корректно
    resultCode = resultCode || (hasSuccessResults ? SIGN_RESULT_CODE.SUCCESS : SIGN_RESULT_CODE.ERROR);

    const error = userCancelError || failedResults[0]?.error;

    const signResult: IUniversalSignResultSummary = {
      sign: signResults,
      resultCode,
      ...(resultCode === SIGN_RESULT_CODE.ERROR && { error }),
      ...(resultCode === SIGN_RESULT_CODE.SUCCESS && { certificateId: cert.id }),
    };

    addSucceeded(signResult);
    done();
  },
  guardians: [groupAction],
};
