import { singleAction, to } from '@platform/core';
import type {
  IBaseEntity,
  IActionContext,
  IHistoryResponse,
  BankHistoryDialogData,
  IActionConfig,
  BankClientName,
  IBankClient,
} from '../interfaces';
import { locale } from '../localization';

/**
 * Сервис получения истории изменений.
 */
export interface IHistoryService {
  /**
   * Метод получения записей истории изменений.
   *
   * @param id - Id документа.
   */
  getHistory(id: string): Promise<IHistoryResponse[]>;
}

export interface BankHistoryActionParams<TDoc extends IBaseEntity> {
  /**
   * Функция, возвращающая заголовок модального окна.
   *
   * @param arg - Базовая сущность.
   */
  getHeader?(arg: TDoc): string;

  /**
   * Функция, возвращающая локализованный статус.
   *
   * @param arg Сущность истории.
   */
  getStatusLabel(arg: string): string;

  /**
   * Функция, возвращающая краткое или полное наименование организации.
   *
   * @param arg - Базовая сущность.
   */
  getBankClientName?(arg: TDoc): BankClientName;

  /**
   * Обработчик при нажатии на имя юзера.
   *
   * @param userId Id пользователя.
   */
  onUserNameClick?(userId: string): void;
}

/**
 * Стандартная функция для получения наименования клиента.
 *
 * @param doc - Документ.
 */
const getBankClientNameBase = <TDoc extends IBaseEntity & { bankClient?: IBankClient }>(doc: TDoc): BankClientName => ({
  clientName: doc.bankClient?.shortName || doc.bankClient?.fullName,
});

/**
 * Стандартная функция для получения заголовка.
 *
 * @param doc - Документ.
 */
const getHeaderBase = <TDoc extends IBaseEntity & { number?: number; date?: string }>(doc: TDoc) => {
  const { date, number } = doc;

  if (number && date) {
    return locale.historyDialog.header({ date, number });
  }

  return '';
};

export const getBankHistoryAction = <TDoc extends IBaseEntity>({
  getHeader = getHeaderBase,
  getStatusLabel,
  getBankClientName = getBankClientNameBase,
  onUserNameClick,
}: BankHistoryActionParams<TDoc>): IActionConfig<IActionContext<IHistoryService>, BankHistoryDialogData> => ({
  action: ({ done, fatal }, { service, showLoader, hideLoader, showAdminHistoryDialog }) => async ([doc]: [IBaseEntity]) => {
    showLoader();

    const [result, err] = await to(service.getHistory(doc.id));

    hideLoader();
    fatal(err);

    showAdminHistoryDialog({ historyList: result!, doc, getHeader, getBankClientName, getStatusLabel, onUserNameClick });

    done();
  },
  fatalHandler: () => {},
  failedHandler: () => {},
  guardians: [singleAction],
});
