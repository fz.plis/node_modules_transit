import { singleAction } from '@platform/core';
import type { IActionConfig, IBaseContext, IXmlContent } from '../interfaces';
import { decodeXml } from '../utils';

/**
 * Открытие XML-файла в новой вкладке браузера.
 */
export const openXml = <TContext extends IBaseContext, TDoc extends IXmlContent>(
  xmlBodyField = 'xmlBody',
  filePrefix = ''
): IActionConfig<TContext, TDoc> => ({
  action: ({ done }) => async ([doc]) =>
    new Promise<void>(() => {
      const blob = new Blob([decodeXml(doc[xmlBodyField])], {
        type: 'text/xml',
      });

      if (window.navigator?.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, `${filePrefix}${doc.id}.xml`);
      } else {
        const url = URL.createObjectURL(blob);

        window.open(url);
        URL.revokeObjectURL(url);
      }

      done();
    }),
  guardians: [singleAction],
});
