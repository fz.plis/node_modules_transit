import { groupAction, to } from '@platform/core';
import type { IBaseEntity, IActionContext, IActionConfig } from '../interfaces';

export interface IValidateSignServive {
  validateSign(id: string): Promise<IBaseEntity>;
}

export const validateSign: IActionConfig<IActionContext<IValidateSignServive>, IBaseEntity> = {
  action: ({ done, fatal, addSucceeded }, { service, showLoader, hideLoader }) => async (docs: IBaseEntity[]) => {
    showLoader({ delay: 500 });

    const [result, err] = await to(Promise.all(docs.map(x => service.validateSign(x.id))));

    hideLoader();
    fatal(err);

    addSucceeded(...(result || []));
    done();
  },
  succeededHandler: () => {},
  fatalHandler: () => {},
  failedHandler: () => {},
  guardians: [groupAction],
};
