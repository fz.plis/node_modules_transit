import { groupAction, to } from '@platform/core';
import type { IActionContext, IServerResp, IBaseEntity, IActionConfig } from '../interfaces';
import { locale } from '../localization';

export interface ISendService {
  send(id: string): Promise<IServerResp<any>>;
}

export const send: IActionConfig<IActionContext<ISendService>, IBaseEntity> = {
  action: ({ done, fatal, addSucceeded }, { service, showLoader, hideLoader }) => async (docs: IBaseEntity[]) => {
    showLoader({ delay: 500 });

    const [result, err] = await to(Promise.all(docs.map(x => service.send(x.id))));

    hideLoader();
    fatal(err);

    addSucceeded(...result!.map(el => el.data));
    done();
  },
  succeededHandler: ({ showSuccess }, docs) => showSuccess(locale.send.succeded({ count: docs.length })),
  fatalHandler: ({ showError }, { error }) => showError(locale.send.fatal, error!.message),
  failedHandler: ({ showWarning }, docs) => showWarning(locale.send.failed({ count: docs.length })),
  guardians: [groupAction],
};
