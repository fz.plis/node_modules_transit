import { singleAction, to } from '@platform/core';
import type { IBaseEntity, IActionContext, IHistoryResponse, IHistoryDialogData, IActionConfig } from '../interfaces';

export interface IHistoryService {
  getHistory(id: string): Promise<IHistoryResponse[]>;
}

export const history: IActionConfig<IActionContext<IHistoryService>, IHistoryDialogData<any>> = {
  action: ({ done, fatal, addSucceeded }, { service, showLoader, hideLoader }) => async (docs: IBaseEntity[]) => {
    showLoader();

    const [result, err] = await to(Promise.all(docs.map(x => service.getHistory(x.id))));

    hideLoader();
    fatal(err);

    addSucceeded(
      ...docs.map((x, idx) => ({
        document: x,
        historyList: result![idx],
      }))
    );
    done();
  },
  fatalHandler: () => {},
  failedHandler: () => {},
  guardians: [singleAction],
};
