import { singleAction } from '@platform/core';
import type { IActionConfig, IBaseContext, IXmlContent } from '../interfaces';
import { decodeXml, extToMimeType, showFile } from '../utils';

/**
 * Скачивание XML-файла.
 */
export const downloadXml = <TContext extends IBaseContext, TDoc extends IXmlContent>(
  xmlBodyField = 'xmlBody',
  filePrefix = ''
): IActionConfig<TContext, TDoc> => ({
  action: () => ([doc]) =>
    new Promise<void>(() => {
      const blob = new Blob([decodeXml(doc[xmlBodyField])], {
        type: 'text/xml',
      });

      showFile(blob, `${filePrefix}${doc.id}`, extToMimeType('xml'));
    }),
  guardians: [singleAction],
});
