import { groupAction, to } from '@platform/core';
import { USER_TYPE_LABELS, VALIDATE_SIGN_ERROR_LABELS } from '../constants';
import { CRYPTO_PLUGIN_TYPE, getCryptoModule } from '../crypto';
import { getCryptoPluginType } from '../crypto/olk/crypto-module-service';
import type {
  IActionContext,
  IActionConfig,
  IBaseSignedDocument,
  IUniversalSignVerifyActionResult,
  ISignedAttachment,
  ISignatureDoc,
  IBaseSignedDocumentService,
  AttachmentService,
} from '../interfaces';
import { SIGN_DATA_TYPE, ERROR } from '../interfaces';
import { locale } from '../localization';
import { certificateUserService } from '../services-shared';
import { getFio } from '../utils';
import { checkSignInstallCrypto } from './install-crypto';

/**
 * Информация о подписи.
 */
interface ISignatureInfo {
  /**
   * Дайджест.
   */
  digest: string;
  /**
   * Подпись.
   */
  signature: ISignatureDoc;
  /**
   * Сертификат.
   */
  certificate: string;
  /**
   * Тип подписываемого объекта.
   */
  type: SIGN_DATA_TYPE;
  /**
   * Подписываемый объект.
   */
  signObject: IBaseSignedDocument | ISignedAttachment;
}

export type IUniversalSignVerifyActionContext<TService> = IActionContext<TService> & {
  getDigestVersion?(document: IBaseSignedDocument, signId: string): number;
  downloadBase64: AttachmentService['downloadBase64'];
};

export const universalSignVerify: IActionConfig<
  IUniversalSignVerifyActionContext<IBaseSignedDocumentService>,
  IUniversalSignVerifyActionResult[]
> = {
  action: ({ done, fatal, addSucceeded, execute }, { showLoader, hideLoader, service, getDigestVersion, downloadBase64 }) => async (
    documents: IBaseSignedDocument[]
  ) => {
    let result: IUniversalSignVerifyActionResult[] = [];

    const triggerFatal = (errors: any) => {
      if (errors) {
        hideLoader();
        fatal(errors);
      } else {
        fatal(errors);
      }
    };

    const installRes = await execute(checkSignInstallCrypto);

    if (installRes.error) {
      done();
    }

    showLoader();

    const cryptoPluginType = getCryptoPluginType();

    for (const document of documents) {
      if (document.signatures.length === 0) {
        continue;
      }

      // eslint-disable-next-line no-await-in-loop
      const docSigns: ISignatureInfo[] = await Promise.all(
        document.signatures.map(async signature => {
          let digest = signature.value?.digest;

          if (getDigestVersion && service.getDigest) {
            try {
              const version = getDigestVersion(document, signature.id);

              const { value } = await service.getDigest(document.id, version);

              digest = value;
            } catch (e) {
              triggerFatal({ message: locale.errors.fatalGetDigest });
            }
          }

          return {
            digest,
            signature,
            certificate: signature?.certificate?.value,
            type: SIGN_DATA_TYPE.DOCUMENT,
            signObject: document,
          };
        })
      );

      const attachments = document.attachments ?? [];
      const withAttachments = attachments.some(x => x.signatures?.length);

      let attachmentsSigns: ISignatureInfo[] = [];

      if (withAttachments) {
        const getSignatureInfos = async (attachment: ISignedAttachment): Promise<any> => {
          const { attachmentId, accessToken, signatures } = attachment;

          const file = await downloadBase64(attachmentId!, accessToken);

          return signatures.map(signature => {
            const digest = file.data;

            return {
              digest,
              signature,
              certificate: signature.certificate.value,
              type: SIGN_DATA_TYPE.ATTACHMENT,
              signObject: attachment,
            };
          });
        };

        // eslint-disable-next-line no-await-in-loop
        const [files, filesErr] = await to(
          attachments.reduce<Promise<any[]>>(async (acc, item) => {
            const infos = await getSignatureInfos(item);

            return (await acc).concat(infos);
          }, Promise.resolve([]))
        );

        triggerFatal(filesErr);

        if (files) attachmentsSigns = files;
      }

      const signaturesData = [...docSigns, ...attachmentsSigns];

      // Получаем данные по УЦ и СОС
      // eslint-disable-next-line no-await-in-loop
      const [additionalVerifyData, errCertAuthorities] = await to(
        certificateUserService.getAdditionalVerifyData({
          signaturesData: signaturesData.map(({ signature }) => ({
            signData: signature.value.signature,
            signId: signature.id,
            signDate: signature.signedAt,
            uaaUserId: signature.ownerId,
          })),
        })
      );

      triggerFatal(errCertAuthorities);

      const crypto = getCryptoModule();

      // проверяем сигнатуры
      // eslint-disable-next-line no-await-in-loop
      const [cryptoModuleResponse, cryptoModuleError] = await to(
        Promise.all(
          signaturesData.map(({ digest, signature, certificate: crt }) => {
            const additionalData = additionalVerifyData!.additionalData.find(x => x.signId === signature.id);

            return crypto
              .verifySign(
                signature.value.signature,
                [digest],
                crt,
                additionalData!.caCertificates,
                additionalData!.revokedCertificates,
                cryptoPluginType === CRYPTO_PLUGIN_TYPE.BSS ? additionalData!.providerInfo : undefined
              )
              .then(res => ({ id: signature.id, ...res }));
          })
        )
      );

      triggerFatal(cryptoModuleError);

      if (cryptoModuleResponse !== null) {
        const getSignaturesData = (signature: ISignatureDoc) => {
          const cryptoModuleInfo = cryptoModuleResponse.find(x => x.id === signature.id);
          const additionalData = additionalVerifyData!.additionalData.find(x => x.signId === signature.id);

          let valid = false;
          let errorCode;
          let errorText;

          if (!cryptoModuleInfo?.valid) {
            errorCode = cryptoModuleInfo?.error?.code?.toString();
            errorText = cryptoModuleInfo?.error?.description;
          } else if (additionalData?.error) {
            errorCode = additionalData.error;
            errorText = VALIDATE_SIGN_ERROR_LABELS[additionalData.error];
          } else {
            valid = true;
          }

          return {
            fio: additionalData!.signerInfo ? getFio(additionalData!.signerInfo) : '',
            date: signature.signedAt,
            role: additionalData!.signerInfo ? USER_TYPE_LABELS[additionalData!.signerInfo.type] : '',
            certificate: {
              owner: signature.certificate.subject,
              organization: cryptoModuleInfo?.data?.subject?.organization || '',
              number: signature.certificate.serialNumber,
              creator: cryptoModuleInfo?.data?.issuer?.commonName || '',
              validFrom: signature.certificate.validFrom,
              validTo: signature.certificate.validTo,
            },
            valid,
            errorCode,
            errorText,
          };
        };

        const docResult: IUniversalSignVerifyActionResult = {
          signObject: document,
          type: SIGN_DATA_TYPE.DOCUMENT,
          result: document.signatures.map(getSignaturesData),
        };

        result.push(docResult);

        if (withAttachments) {
          const attachmentsResult: IUniversalSignVerifyActionResult[] = attachments.map(attachment => ({
            signObject: attachment,
            type: SIGN_DATA_TYPE.ATTACHMENT,
            result: attachment.signatures.map(getSignaturesData),
          }));

          result = [...result, ...attachmentsResult];
        }
      }
    }

    hideLoader();

    if (result.length > 0) {
      addSucceeded(result);
    } else {
      triggerFatal({ response: { data: { errorInfo: { code: ERROR.CRYPTO_SIGN_NOT_CORRECT } } } });
    }

    done();
  },
  guardians: [groupAction],
  succeededHandler: ({ universalSignVerifyDialog }, results) => universalSignVerifyDialog(results),
  fatalHandler: ({ showError }, { error }) => showError(locale.verifySign.fatal, error!.message),
};
