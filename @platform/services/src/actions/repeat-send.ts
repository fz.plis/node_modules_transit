import { groupAction, to } from '@platform/core';
import type { IBaseEntity, IActionContext, IActionConfig } from '../interfaces';
import { locale } from '../localization';

export interface IValidateSignServive {
  repeatSend(id: string): Promise<IBaseEntity>;
}

export const repeatSend: IActionConfig<IActionContext<IValidateSignServive>, IBaseEntity> = {
  action: ({ done, fatal, addSucceeded }, { service, showLoader, hideLoader }) => async (docs: IBaseEntity[]) => {
    showLoader({ delay: 500 });

    const [validateSign, err] = await to(Promise.all(docs.map(x => service.repeatSend(x.id))));

    hideLoader();
    fatal(err);

    addSucceeded(...(validateSign || []));
    done();
  },
  succeededHandler: ({ showSuccess }, docs) =>
    showSuccess(locale.upload.succeded({ count: docs.length }), locale.unload.successfullyUnloadDocument),
  fatalHandler: ({ showSignError }, context) => {
    showSignError(context);
  },
  failedHandler: ({ showWarning }, docs) => showWarning(locale.unload.failed({ count: docs.length })),
  guardians: [groupAction],
};
