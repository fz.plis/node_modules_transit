import type { IActionConfig } from '@platform/core';
import { to } from '@platform/core';
import type { IBaseContext, IBaseEntity, ICrudService, IActionContext } from '../interfaces';
import { locale } from '../localization';

export const create = <TContext extends IBaseContext, TDoc>(baseUrl: string): IActionConfig<TContext, TDoc> => ({
  action: ({ done }, { router }) => async () => {
    router.push(`${baseUrl}/new`);

    done();

    return Promise.resolve();
  },
});

export const open = <TContext extends IBaseContext, TDoc extends IBaseEntity>(
  urlGetter: string | ((doc: TDoc) => string)
): IActionConfig<TContext, TDoc> => ({
  action: ({ done }, { router }) => async ([doc]: [TDoc]) => {
    const url = typeof urlGetter === 'string' ? `${urlGetter}/${doc.id}` : urlGetter(doc);

    router.push(url);

    done();

    return Promise.resolve();
  },
});

export interface IRemoveDialogOptions {
  confirmText?(count: number): string;
  dialogTitle?: string;
}

export const remove = <TContext extends IActionContext<ICrudService<IBaseEntity>>, TDoc extends IBaseEntity>({
  confirmText = count => locale.dialog.delete.text({ count }),
  dialogTitle = locale.dialog.remove.title,
}: IRemoveDialogOptions = {}): IActionConfig<TContext, TDoc> => ({
  action: ({ addSucceeded, done, fatal }, { service, showConfirm }) => async (documents: TDoc[]) => {
    const [, no] = await to(
      showConfirm(confirmText(documents.length), {
        header: dialogTitle,
        okButtonText: locale.action.remove,
        // eslint-disable-next-line @eco/no-non-ascii-symbols
        cancelButtonText: locale.action.сancel,
      })
    );

    if (no) {
      done();
    }

    const [, err] = await to(Promise.all(documents.map(el => service.delete(el.id))));

    fatal(err);
    addSucceeded(...documents);

    done();
  },
  succeededHandler: ({ showSuccess }, [documents]) =>
    showSuccess(locale.statuses.successful, locale.dialog.delete.removed({ count: documents.length })),
  fatalHandler: ({ showError }) => showError(locale.dialog.error.title, locale.dialog.error.delete),
});
