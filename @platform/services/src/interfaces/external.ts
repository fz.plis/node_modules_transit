export { SORT_DIRECTION, ICollectionResponse } from '@platform/core';
// eslint-disable-next-line @typescript-eslint/consistent-type-imports
export type IActionConfig<TContext, TResult> = import('@platform/core').IActionConfig<TContext, TResult>;
export { IValidators, IValidatorParams } from '@platform/core';
