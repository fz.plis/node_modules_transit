import type { IFilters as CoreFilters } from '@platform/core';
import type { CRYPTO_PLUGIN_TYPE } from '../crypto';
import type { IBaseEntity, USER_ROLE } from './entities';
import type { ERROR } from './errors';
import type { SORT_DIRECTION } from './external';

export type IFilters = Record<
  string,
  {
    value: any;
    fieldName: string;
    condition?: string;
  }
>;

export type ISortSettings = Record<string, SORT_DIRECTION>;

export interface IMetaData {
  pageSize: number;
  offset: number;
  sort?: ISortSettings;
  multiSort?: ISortSettings;
  filters?: CoreFilters;
  category?: string;
  search?: string;
  textQuery?: string;
  orgId?: string;
  branchId?: string;
  userId?: string;
}

export interface ISearchByName {
  search?: string;
  offset?: number;
  pageSize?: number;
}

export interface IClientSearch extends ISearchByName {
  filters?: CoreFilters;
}

export interface IUserContext {
  certificateSN: string;
  absId: string;
  type: USER_CONTEXT_TYPE;
  remoteId: number;
}

/**
 * Тип входа в систему.
 */
export enum USER_PASSED_FACTOR_TYPE {
  /**
   * Вход по сертификату.
   */
  MFA_CERTIFICATE = 'MFA_CERTIFICATE',
  /**
   * Вход по otp.
   */
  MFA_OTP = 'MFA_OTP',
  /**
   * Вход по totp.
   */
  MFA_TOTP = 'MFA_TOTP',
  /**
   * Вход по паролю.
   */
  PASSWORD = 'PASSWORD',
  /**
   * Обмен токенами.
   */
  TOKEN_EXCHANGE = 'TOKEN_EXCHANGE',
}

export interface IParsedToken {
  jti: string;
  exp: number;
  firstName: string;
  userId: string;
  login: string;
  patronymic: string;
  phoneNumber: string;
  secondName: string;
  roles: USER_ROLE[];
  user_context: IUserContext[];
  user_context_types: USER_CONTEXT_TYPE[];
  /**
   * Типы входа.
   */
  user_passed_factor_types: USER_PASSED_FACTOR_TYPE[];
  authorities: string[];
  default_sign_certificate?: string;
}

export interface IClientTokenInfo extends IParsedToken {
  /**
   * Доступные сегментыв разрезе организаций.
   */
  availableSegments: Record<string, string[]>;
}

export interface IUserData {
  firstName: string;
  secondName: string;
  patronymic: string;
  userId: string;
  login: string;
  phoneNumber: string;
  email: string;
  emailConfirmed: boolean;
  phoneConfirmed: boolean;
  shouldNotification: boolean;
  canChangeContactData: boolean;
  /**
   * Тип криптоплагина используемого пользователем.
   */
  cryptoPlugin: CRYPTO_PLUGIN_TYPE;
  /**
   * Признак игнорирования недоступности сервисов.
   */
  closedServicesAvailable?: boolean;
}

export enum USER_CONTEXT_TYPE {
  ECO = 'ECO',
  DBO = 'BSS_DBO_320',
}

export interface IServerResp<T, TErrorCode = ERROR> {
  data: T;
  errorInfo?: {
    code: TErrorCode;
    message: string;
  };
}

export interface IServerDataResp<T> {
  data: T;
  error?: {
    code: string;
    message: string;
  };
}

export interface IServerValidationResult {
  fieldName: string;
  messages: string[];
}

export interface IServerValidation {
  errors: IServerValidationResult[];
  warnings: IServerValidationResult[];
}

export interface ISaveResponse<T extends IBaseEntity> {
  data: T;
  validation?: IServerValidation;
  code?: number;
  message?: string;
}

// бекендское API

export enum API_VALIDATION_RESULT {
  VALID = 'VALID',
  INVALID = 'INVALID',
}

export enum VALIDATION_LEVEL {
  WARNING = 'WARNING',
  CRITICAL = 'CRITICAL',
}

export interface IBaseValidationResult {
  result: API_VALIDATION_RESULT;
}

export interface IBaseValidationEntity extends IBaseValidationResult {
  id: string;
  name: string;
}

export interface IValidationCheckResult extends IBaseValidationResult {
  fieldName: string;
  message?: {
    message: string;
    params?: Record<string, number | string>;
  };
}

export interface IControlValidationResult extends IBaseValidationEntity {
  checkResults: IValidationCheckResult[];
  level: VALIDATION_LEVEL;
}

export interface IGroupValidationResult extends IBaseValidationEntity {
  controlResults: IControlValidationResult[];
}

export interface IApiValidationResult extends IBaseValidationResult {
  groupResults: IGroupValidationResult[];
}

export enum EXPORT_TYPE {
  DOCUMENTS = 'DOCUMENTS',
  DOCUMENTS_GOZ = 'DOCUMENTS_GOZ',
  DOCUMENTS_LIST = 'DOCUMENTS_LIST',
  REQUEST_DOC = 'REQUEST_DOC',
}

export type IRouterParams = Record<string, any>;

export interface IRouterMatchProps<T extends IRouterParams> {
  isExact: boolean;
  path: string;
  url: string;
  params: T;
}

export interface IRouterInjectedProps<T extends IRouterParams> {
  location: {
    pathname: string;
    search: string;
    hash: string;
    state?: any;
    key: string;
  };
  match: IRouterMatchProps<T>;
}

export enum AUTHORITY_VALIDATORS {
  EIO = 'EIO',
  CERTIFICATE = 'CERTIFICATE',
  CONSTITUTION = 'CONSTITUTION',
}

/**
 * Код продукта.
 */
export enum PRODUCT_TYPE {
  CONSTITUTION = 'CONSTITUTION',
  LK_UVED = 'LK_UVED',
  LK_MP = 'LK_MP',
}

/**
 * Режим акцепта для заявки на сертификат.
 */
export enum ACCEPT_MODE {
  ESK_AUTO = 'ESK_AUTO',
  ESK_ORGANIZATION = 'ESK_ORGANIZATION',
  POTENTIAL_ORGANIZATION = 'POTENTIAL_ORGANIZATION',
}

/**
 * Интерфейс интервального опроса.
 */
export interface IPollingResponse {
  /**
   * Интервал обновления уведомлений.
   */
  pollingInterval: number;
}

/**
 * Интерфейс запроса.
 */
export interface IDataResponse<T> {
  data: T;
  code: number;
  message?: string;
}
