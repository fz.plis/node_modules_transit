import { locale } from '../localization';

export enum ERROR {
  // CUSTOM
  NO_CONNECTION = 1,

  // Аутентификация
  ONETIME_PASSWORD_EXPIRED = 100, // Время действия одноразового пароля истекло
  INVALID_VERIFICATION_CODE = 101, // Неверное значение кода подтверждения
  ACCOUNT_NOT_FOUND = 102, // Модальное окно или текст перед кнопкой продолжить
  CONFIRMATION_ATTEMPTS_EXHAUSTED = 103, // Исчерпано количество попыток ввода кода подтверждения
  INCORRECT_LOGIN_PASSWORD = 104, // Введены неверно логин или пароль
  PASSWORD_ATTEMPTS_EXHAUSTED = 105, // Исчерпано количество попыток ввода пароля
  MFA_TOTP_USER_NOT_FOUND = 189, // Ненайдены totp настройки для пользователя
  // Многофакторная аутентификация
  /**
   * Неправильный токен.
   */
  MFA_INVALID_TOKEN_VALUE = 190,
  /**
   * Не истек срок действия смс-кода.
   */
  SMS_TIMEOUT_NOT_EXPIRED = 191,
  MFA_INVALID_CREDENTIALS = 192, // неправильные креды смс код
  LOGIN_REQUIRED = 193, // требуется логин
  APPROVE_REQUESTED = 194, // требуется аппрув
  USER_BLOCKED = 195, // юзер заблокирован
  INVALID_CREDENTIALS = 196, // неправильные креды (логин/пароль)
  MFA_TOKEN_ATTEMPS_EXHAUSTED = 197, // Исчерпано количество попыток ввода кода подтверждения
  MFA_TOKEN_EXPIRED = 198, // истекло время жизни токена авторизации
  MFA_REQUIRED = 199, // требуется многофакторная аутентификация

  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
  NOT_FOUND = 404, // запись не найдена
  METHOD_NOT_ALLOWED = 405, // Метод API отсутствует
  UNSUPPORTED_MEDIA_TYPE = 415,
  LOCKED = 420,
  OPTIMISTIC_LOCK = 423,

  FILE_MALWARE = 600, // файл не прошел проверку антивирусом
  IN_STOP_LIST = 644, // Клиент в стоп-листе

  // Ошибки крипты
  CRYPTO_NOT_FOUND = 800, // крипто-модуль не установлен
  CRYPTO_UNAUTHORIZE = 801, // отказано в доступе, неправильные ключи, неправильный пароль
  CRYPTO_CERTS_NOT_FOUND = 802, // отсутствуют сертификаты
  CRYPTO_INCORRECT_AUTHORIZATION_INFO = 803, // Передана не корректная информация для авторизации
  CRYPTO_SIGN_NOT_CORRECT = 804, // Подпись не верна
  CRYPTO_VERIFYING_CERTIFICATE_CHAIN = 805, // Ошибка при проверке цепочки сертификатов
  CRYPTO_INVALID_SIGN_CERTIFICATE = 806, // Неверный сертификат подписи
  CRYPTO_MODULE_VERSION_INVALID = 807, // Некорректная версия криптомодуля
  CRYPTO_INTERNAL_SERVER_ERROR = 899,
  // Регистрация
  UNEXPECTED_SYSTEM_ERROR = 500, // Непредвиденная системная ошибка
  LOGIN_OR_EMAIL_EXIST = 901, // Введен логин или email, который есть в системе
  EMAIL_EXIST = 902, // Введен email, который есть в системе
  PREREGISTERED_USER_NOT_FOUND = 903, // Предварительно зарегистрированный пользователь для указанного подтверждения не найден
  LOGIN_NOT_FOUND = 904, // Пользователь с указанным логином не найден
  PASS_REC_USER_BLOCKED = 905, // юзер заблокирован приходит (при востановлении пароля)
  USER_NOT_CLIENT = 906, // Пользователь не является клиентским пользователем
  INCORRECT_PASSWORD = 907, // Некорректный пароль
  PASSWORD_UNSAFE = 908, // Введенный пароль не отвечает требованиям парольной политики
  PASSWORD_USE_ANOTHER_USER = 909, // Введенный пароль уже используется другим пользователем
  EMAIL_DELIVERY_ERROR = 910, // Ошибка при доставке электронной почты
  CAPTCHA_ERROR = 911, // Неверно введена последовательность символов с капчи
  LOGOUT_DUE_TO_EXEEDED_TRYES = 913, // Превышено количество попыток ввода кода подтверждения / пароля. Ваша сессия завершена.

  ONETIME_PASSWORD_SEND_TIMEOUT = 950, // Таймаут отправки кодов подтверждения
  VERIFICATION_CODESEND_FAILED = 951, // Ошибка проверки кода подтверждения
  ONETIME_PASSWORD_EXPIRED_REG = 952, // Время действия одноразового пароля истекло (при регистрации)
  CONFIRMATION_ATTEMPTS_EXHAUSTED_REG = 953, // Количество попыток ввода кода подтверждения истекло
  FAILED_SEND_CODE = 954, // Не удалось выслать код на телефон
  CRYPTOMODULE_NOT_INSTALLED = 955, // Криптомодуль не установлен
  CERTIFICATES_NOT_FOUND = 956, // Нет доступных сертификатов

  /**
   * Превышен лимит отправленных SMS.
   */
  NUMBER_OF_TRIES_EXCEEDED_SEND_SMS = 940,

  CERTIFICATES_NOT_RIGHT_SIGN = 957, // Недостаточно прав для подписи документа, поскольку Вы не являетесь ЕИО (единоличным исполнительным органом)
  VALIDATION_SERVER_CONTROLS_ERROR = 999, // Проверка серверными контролями не пройдена
}

export const PASS_ERRORS = {
  get PASSWORD_TOO_SHORT() {
    return locale.errors.passwordErrors.minLength;
  },
  get PASSWORD_TOO_LONG() {
    return locale.errors.passwordErrors.maxLength;
  },
  get PASSWORD_DOES_NOT_CONTAIN_LOWER_ALPHABET_SYMBOLS() {
    return locale.errors.passwordErrors.lowerCase;
  },
  get PASSWORD_DOES_NOT_CONTAIN_UPPER_ALPHABET_SYMBOLS() {
    return locale.errors.passwordErrors.upperCase;
  },
  get PASSWORD_DOES_NOT_CONTAIN_NUMERIC_SYMBOLS() {
    return locale.errors.passwordErrors.minNum;
  },
  get PASSWORD_DOES_NOT_CONTAIN_SPECIAL_SYMBOLS() {
    return locale.errors.passwordErrors.minSymb;
  },
  get PASSWORD_CONTAINS_TOO_FEW_DIFFERENT_SYMBOL_GROUPS() {
    return locale.errors.passwordErrors.minGroup;
  },
  get PASSWORD_CONTAINS_UNKNOWN_SYMBOLS() {
    return locale.errors.passwordErrors.unacceptable;
  },
  get PASSWORD_MATCHES_LOGIN() {
    return locale.errors.passwordErrors.coincidence;
  },
};
