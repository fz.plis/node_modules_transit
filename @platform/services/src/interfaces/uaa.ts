import type { API_VALIDATION_RESULT, USER_CONTEXT_TYPE } from './common';
import type {
  UserType,
  RESOURCE_TYPE,
  IUserInfo,
  PASSWORD_VALIDATION_ERRORS,
  MFA_CHAINS_STAGES,
  IBaseEntity,
  BANK_CLIENT_TYPE,
} from './entities';

export interface IResource {
  description: string;
  id: string;
  name: string;
  resourceType: RESOURCE_TYPE;
}

export interface IPrivileges {
  description: string;
  id: string;
  name: string;
  resources: IResource[];
}

export interface IMfaChains {
  id: string;
  name: string;
  stages: MFA_CHAINS_STAGES[];
  title: string;
}

export interface IMfaCatalogChain {
  activeByDefault: boolean;
  catalog: string;
  used: boolean;
  userType: UserType;
}

export interface IMfaAvailableChain extends IMfaChains {
  catalogs?: IMfaCatalogChain[];
}

export interface IRole {
  bankAllowed: boolean;
  clientAllowed: boolean;
  description: string;
  id: string;
  name: string;
  privileges: IPrivileges[];
}

/**
 * Привилегия.
 */
export interface IUserAuthority extends IBaseEntity {
  /**
   * Код привилегии.
   */
  code: string;
  /**
   * Описание.
   */
  description: string;
}

/**
 * Тип связанной с сегментом сущности.
 */
export enum SEGMENT_ENTITY_TYPE {
  USER = 'USER',
  CLIENT_OFFICIAL = 'CLIENT_OFFICIAL',
  BRANCH_OFFICIAL = 'BRANCH_OFFICIAL',
}

/**
 * Тип сегмента.
 */
export enum SEGMENT_TYPE {
  CLIENT_SEGMENT = 'CLIENT_SEGMENT',
  BANK_SEGMENT = 'BANK_SEGMENT',
}

/**
 * Сегмент доступа.
 */
export interface IAccessSegment extends IBaseEntity {
  /**
   * Код сегмента.
   */
  code: string;
  /**
   * Описание сегмента.
   */
  description: string;
  /**
   * Наименование сегмента.
   */
  name: string;
  /**
   * Тип связанной с сегментом сущности.
   */
  segmentEntityType: SEGMENT_ENTITY_TYPE;
  /**
   * Тип сегмента.
   */
  segmentType: SEGMENT_TYPE;
}

/**
 * Привилегия роли.
 */
export interface IRoleAuthority {
  /**
   * ID привилегии роли.
   */
  id: string;
  /**
   * Привилегия.
   */
  authority: IUserAuthority;
  /**
   * Признак отредактированной вручную записи.
   */
  edited: boolean;
  /**
   * Признак активной записи.
   */
  enabled: boolean;
}

/**
 * Роль доступа.
 */
export interface IAccessRole {
  /**
   * Привилегии роли.
   */
  authorities: IRoleAuthority[];
  /**
   * Код роли.
   */
  code: string;
  /**
   * Описание.
   */
  description: string;
  /**
   * ID роли.
   */
  id: string;
  /**
   * Наименование роли.
   */
  name: string;
  /**
   * Сегмент доступа.
   */
  segment: IAccessSegment;
  /**
   * Тип роли.
   */
  type: UserType;
}

/**
 * Информация о клиенте.
 */
export interface IUaaUserClientInfo {
  /**
   * Наименование клиента (организации).
   */
  name: string;
  /**
   * ИНН/КИО.
   */
  inn: string;
  /**
   * ОГРН/ОГРНИП.
   */
  ogrn: string;
  /**
   * Признак резидента.
   */
  isResident: boolean;
  /**
   * Тип клиента.
   */
  clientType: BANK_CLIENT_TYPE;
}

/**
 * Подразделение пользователя.
 */
export interface IUaaRoleBranch {
  /**
   * ID филиала.
   */
  branchId: string;
  /**
   * ID записи.
   */
  id: string;
  /**
   * Наименование подразделения.
   */
  name: string;
  /**
   * Номер подразделения.
   */
  number: string;
  /**
   * Идентификатор филиала.
   */
  parentBranchId: string;
}

/**
 * Информация о пользователе добавившего роль.
 */
export interface IUaaRoleCreator {
  /**
   * ID пользователя.
   */
  id: string;
  /**
   * Имя пользователя.
   */
  firstName: string;
  /**
   * Фамилия пользователя.
   */
  familyName: string;
  /**
   * Отчество пользователя.
   */
  middleName: string;
  /**
   * Логин пользователя.
   */
  login: string;
}

/**
 * Роль пользователя.
 */
export interface IUaaRole {
  /**
   * ID правила доступа.
   */
  accessRuleId: string;
  /**
   * ID представителя клиента.
   */
  clientOfficialId?: string;
  /**
   * Информация о клиенте.
   */
  clientInfo: IUaaUserClientInfo;
  /**
   * Комментарий.
   */
  comments: string;
  /**
   * ID пользователя добавившего роль.
   */
  createdUserId: string;
  /**
   * ID роли пользователя.
   */
  id: string;
  /**
   * Роль.
   */
  role: IAccessRole;
  /**
   * Дата и время добавления правила.
   */
  createdAt: string;
  /**
   * Филиалы роли.
   */
  branches: IUaaRoleBranch[];
  /**
   * Информация о пользователе добавившего роль.
   */
  creator: IUaaRoleCreator;
}

/**
 * Клиентский пользователь.
 */
export interface IUaaUser {
  /**
   * Признак блокировки.
   */
  blocked: boolean;
  /**
   * @deprecated
   * Список привилегий.
   */
  effectivePrivileges: IPrivileges[];
  /**
   * Список привилегий.
   */
  effectiveUaaPrivileges: string[];
  /**
   * ID пользователя.
   */
  id: string;
  /**
   * @deprecated
   * Список ролей.
   */
  roles: IRole[];
  /**
   * Признак временной блокировки.
   */
  temporaryBlockedUntil: string;
  /**
   * Тип пользователя.
   */
  userType: UserType;
  /**
   * Список цепочек аутентификации.
   */
  mfaChains: IMfaChains[];
  /**
   * Активная цепочка аутентификации.
   */
  activeMfaChain: IMfaAvailableChain;
  /**
   * Коллекция ролей.
   */
  uaaRoles: IUaaRole[];
}

export interface IDbo320Certificate {
  absId: string;
  containerNameLocation: string;
  data: string;
  id: string;
  organizationName: string;
  signParams: string;
  userName: string;
}

export interface IDbo320Context {
  certificates: IDbo320Certificate[];
  login: string;
  remoteId: number;
  userId: string;
}

/**
 * Клиентский пользователь.
 */
export interface IClientUser {
  /**
   * Признак подтвержденного пользователя.
   */
  confirmed: boolean;
  /**
   * Признак подтверждения использования данных.
   */
  isDataUsageApproved: boolean;
  /**
   * Наименование каталога.
   */
  catalog: USER_CONTEXT_TYPE;
  /**
   * Email.
   */
  email: string;
  /**
   * Признак подтвержденного email.
   */
  emailConfirmed?: boolean;
  /**
   * Идентификатор клиента в ЕСК.
   */
  eskId?: string;
  /**
   * Идентификатор клиента в F1.
   */
  f1Id?: string;
  /**
   * Фамилия.
   */
  familyName: string;
  /**
   * Имя.
   */
  firstName: string;
  /**
   * Идентификатор.
   */
  id: string;
  /**
   * Время последнего входа.
   */
  lastLogin: string;
  /**
   * Время последней смены пароля.
   */
  lastPasswordChange: string;
  /**
   * Логин.
   */
  login: string;
  /**
   * Отчество.
   */
  middleName: string;
  /**
   * Номер телефона.
   */
  phoneNumber: string;
  /**
   * Признак подтвержденного телефона.
   */
  phoneConfirmed?: boolean;
  /**
   * Признак получения уведомлений о небходимости смены email/телефона.
   */
  shouldNotification?: boolean;
  /**
   * Время после которого необходимо показывать уведомление о небходимости смены email/телефона.
   */
  showNotificationAfter?: string;
  /**
   * Признак не персонализированной учетной записи.
   */
  notPersonalized?: boolean;
  /**
   * Пользователь.
   */
  uaaUser: IUaaUser;
  /**
   * Признак игнорирования недоступности сервисов.
   */
  closedServicesAvailable?: boolean;
}

export interface IBankUser {
  lastLogin: string;
  id: string;
  email: string;
  familyName: string;
  firstName: string;
  login: string;
  middleName: string;
  phoneNumber: string;
  uaaUser: IUaaUser;
  uaaUserId: string;
}

export interface IUpdateUserResponse {
  needEmailConfirmation: boolean;
  needPhoneNumberConfirmation: boolean;
  phoneNumberConfirmationId?: string;
}

export interface IRegisterRequestData {
  captchaId: string;
  captchaText: string;
  details: IUserInfo;
  isDataUsageApproved: boolean;
}

export interface IRegisterResponse {
  /**
   * Запрос на подтверждение.
   */
  confirmationRequest: ITokenResponse;
  passwordValidationErrors: string[];
}

export interface IChangePasswordResponse {
  passwordValidationErrors: string[];
}

export interface IRestorePasswordRequest {
  captchaId: string;
  captchaText: string;
  login: string;
  newPassword: string;
}

export interface IRestorePasswordResponse {
  /**
   * Запрос на подтверждение.
   */
  confirmationRequest: ITokenResponse;
  passwordValidationErrors: PASSWORD_VALIDATION_ERRORS[];
}

export interface ICompleteRestorePasswordRequest {
  requestId: string;
  smsCode: string;
}

export interface ICompleteRestorePasswordResponse {
  codeCorrect: string;
  needPasswordChange: boolean;
  triesLeft: number;
  clientUserId: string;
  uaaUserId: string;
  token: string;
}

export interface IGenerateCaptchaResponse {
  data: string;
}

/** Завершение регистрации клиента. */
export interface ICompleteRegisterResponse {
  /** Идентификатор пользователя Клиента. */
  clientUserId?: string;
  /** Верно ли введен SMS или E-mail код. */
  codeCorrect: boolean;
  /** Идентификатор сессии, если в результате запроса произведена аутентификация клиента. */
  sessionId?: string;
  /** Количество оставшихся попыток ввода. */
  triesLeft: number;
  /** Идентификатор пользователя UAA. */
  uaaUserId?: string;
}

export interface IChangePhoneNumberRequest {
  phoneNumber: string;
}

export interface IChangeEmailRequest {
  email: string;
}

/**
 * Конфликтная роль.
 */
export interface IRoleConflict {
  /**
   * Код конфликтной роли.
   */
  conflictRoleCode: string;
  /**
   * Наименование конфликтной роли.
   */
  conflictRoleName: string;
}

/**
 * Проверка конфликтности ролей.
 */
export interface IRoleConflictResponse {
  /**
   * Наименование поля.
   */
  fieldName: string;
  /**
   * Результат проверки.
   */
  result: API_VALIDATION_RESULT;
  /**
   * Данные проверки.
   */
  message?: {
    /**
     * Сообщение.
     */
    message: string;
    /**
     * Параметры.
     */
    params: {
      /**
       * Список конфликтных ролей.
       */
      conflictRoles: IRoleConflict[];
      /**
       * Код роли.
       */
      roleCode: string;
      /**
       * Наименование роли.
       */
      roleName: string;
    };
  };
}

/**
 * Технический пользователь.
 */
export interface ITechnicalUser {
  /**
   * ID пользователя.
   */
  id: string;
  /**
   * Признак ручного создания.
   */
  manuallyCreated: boolean;
  /**
   * Логин пользователя.
   */
  name: string;
  /**
   * Назначение.
   */
  description?: string;
}

/**
 * Интерфейс ответа привилегий.
 */
export interface IUserAuthoritiesResponse {
  /**
   * Список привилегий.
   */
  authorities: string[];
  /**
   * Список клиентских привилегий.
   */
  clientAuthorities: Record<string, string[]>;
}

/**
 * Интерфейс ответа токена.
 */
export interface ITokenResponse {
  /**
   * Идентификатор токена.
   */
  id: string;
  /**
   * Дата и время жизни токена.
   */
  expiredAt: string;
}

/**
 * Интерфейс ответа тотп/отп.
 */
export interface ICodeMfaChallenge {
  /**
   * Идентификатор токена.
   */
  token: string;
  /**
   * Идентификатор пользователя.
   */
  userId: string;
  /**
   * Время жизни токена.
   */
  expiresIn: string;
}
