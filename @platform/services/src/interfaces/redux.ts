import type { IReduxStore } from './store';

export interface IAction {
  type: string;
  payload: Record<string, unknown> | undefined;
}
export type GetStateType = () => IReduxStore;
export type ThunkActionType = (dispatch: DispatchType, getState: GetStateType) => any;
export type DispatchType = (action: IAction | ThunkActionType) => any;
