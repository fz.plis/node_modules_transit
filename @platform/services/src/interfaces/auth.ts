import type { CRYPTO_PLUGIN_TYPE } from '../crypto';

export enum GRANT_TYPE {
  MFA_CERTIFICATE = 'mfa_certificate',
  MFA_OTP = 'mfa_otp',
  MFA_TOTP = 'mfa_totp',
  PASSWORD = 'password',
}

export enum AUTH_RESPONSE_TYPE {
  TOKEN = 'token',
  CODE = 'code',
}

/**
 * Ошибки авторизации.
 */
export enum AUTH_ERROR {
  /**
   * Требуется многофакторная авторизация.
   */
  MFA_REQUIRED = 'mfa_required',
  /**
   * Неправльная пара логин/пароль.
   */
  INVALID_CREDENTIALS = 'invalid_credentials',
  /**
   * Неправльная смс код.
   */
  MFA_INVALID_CREDENTIALS = 'mfa_invalid_credentials',
  /**
   * У пользователя нет сертификатов.
   */
  MFA_USER_CERTIFICATES_NOT_AVAILABLE = 'mfa_user_certificates_not_available',
  /**
   * Не истек срок жизни предыдущего смс.
   */
  MFA_ALREADY_HAS_ACTIVE_CHALLENGE = 'mfa_already_has_active_challenge',
  /**
   * Неправильный мфа-токен.
   */
  MFA_INVALID_TOKEN_VALUE = 'mfa_invalid_token_value',
  /**
   * Истекло время жизни токена.
   */
  TOKEN_EXPIRED = 'mfa_token_expired',
  /**
   * Исчерпано количество попыток ввода кода подтверждения.
   */
  TOKEN_ATTEMPS_EXHAUSTED = 'mfa_token_check_attempts_exhausted',
  /**
   * Требуется аппрув.
   */
  APPROVE_REQUESTED = 'requested_approval',
  /**
   * Требуется логин.
   */
  LOGIN_REQUIDER = 'login_required',
  /**
   * Юзер заблокирован.
   */
  USER_BLOCKED = 'user_is_blocked',
  /**
   * Неправльный totp код.
   */
  MFA_TOTP_USER_NOT_FOUND = 'mfa_totp_user_not_found',
  /**
   * Ошибка проверки ключа.
   */
  MFA_CERTIFICATE_SIGN_NOT_VALID = 'mfa_certificate_sign_not_valid',
}

export interface IMfaAvailableCertificates {
  containerNameLocation: string;
  data: string;
  organizationName: string;
  userName: string;
}

/**
 * Доступные сертификаты пользователя.
 */
export interface IMfaUser {
  id?: string;
  /**
   * ФИО пользователя.
   */
  userName: string;
  /**
   * Наименование организации.
   */
  organizationName: string;
  /**
   * Строковое представление сертификата.
   */
  data: string;
  /**
   * Параметр подписи “Container Name Location”.
   */
  containerNameLocation?: string;
}

/**
 * Многофакторная аутентификация по сертификату.
 */
export interface IMfaCertificateResponse {
  /**
   * Доступные сертификаты.
   */
  availableCertificates: IMfaUser[];
  /**
   * Дайджест.
   */
  digest: string;
  /**
   * Идентификатор токена.
   */
  token: string;
  /**
   * Идентификатор пользователя.
   */
  userId: string;
}

export interface IAuthResponse {
  accessToken: string;
  expiresIn: string;
  refreshToken: string;
  tokenType: string;
}

/**
 * Запрос авторизации.
 */
export interface IAuthRequest {
  /**
   * Доступные криптоплагины пользователя.
   */
  userEnvironment?: string;
}

/**
 * Авторизация по сертификату.
 */
export interface ILoginCertificateRequest extends IAuthRequest {
  /**
   * Токен.
   */
  token: string;
  /**
   * Данные сертификата.
   */
  certData: string;
  /**
   * Подписанные данные.
   */
  signData: string;
  /**
   * Тип криптоплагина.
   */
  cryptoPlugin: CRYPTO_PLUGIN_TYPE;
  /**
   * Версия криптоплагина.
   */
  cryptoPluginVersion: string;
}

/**
 * Авторизация через otp.
 */
export interface ILoginOtpTotpRequest extends IAuthRequest {
  /**
   * Токен.
   */
  token: string;
  /**
   * Код.
   */
  code: string;
  /**
   * Тип авторизации.
   */
  mfaType: GRANT_TYPE;
}

/**
 * Авторизацию по логину и паролю.
 */
export interface ILoginPasswordRequest extends IAuthRequest {
  /**
   * Логин.
   */
  username: string;
  /**
   * Пароль.
   */
  password: string;
}

export interface IAuthErrorBase {
  error: string;
  errorDescription: string;
}

export interface ILoginError extends IAuthErrorBase {
  mfaToken: string;
  mfaType: GRANT_TYPE;
  selectAvailable?: boolean;
  cryptoPlugin: CRYPTO_PLUGIN_TYPE;
}

export interface IApproveRequiredError extends IAuthErrorBase {
  approvalRequestId: string;
}

export interface IAuthorizeError extends IAuthErrorBase {
  loginUri: string;
}

export interface ILoginResponse {
  login: string;
  sessionId: string;
  redirectUri: string;
}

/** ОБъект ответа при смене пароля. */
export interface IChangePasswordDboResponse {
  /** Данные. */
  data: number;
  error?: {
    /** Код об ошибке. */
    code: string;
    /** Сообщение об ошибке. */
    message: string;
    /** ID для Captcha. */
    captchaId?: string;
  };
}

/** ОБъект ответа для формирования формы при смене пароля. */
export interface IChangePasswordDboFormResponse {
  /** ID для Captcha. */
  captchaId?: string;
  /** Признак необходимости отображения captcha на форме. */
  captchaRequired: boolean;
}

export interface IScope {
  id: string;
  description: string;
}

export interface IApproval {
  applicationId: string;
  expiresIn: string;
  id: string;
  userId: string;
  scopes: IScope[];
  requestUri: string;
  callbackUri: string;
}

export interface IAuthApiRequest {
  client_id: string;
  response_type: AUTH_RESPONSE_TYPE;
}

/**
 * Запрос логина.
 */
export interface ILoginApiRequest {
  /**
   * Ссылка на запрос авторизации.
   */
  authorization_request_ref?: string;
  /**
   * Тип авторизации.
   */
  grant_type: GRANT_TYPE;
  /**
   * Авторизация по сертификату.
   */
  mfa_certificate?: string;
  /**
   * Авторизация по логину и паролю.
   */
  mfa_password?: string;
  /**
   * Возможность выбора цепочки.
   */
  select_available?: boolean;
  /**
   * Подписанные данные.
   */
  mfa_signed_data?: string;
  /**
   * Авторизация по токену.
   */
  mfa_token?: string;
  /**
   * Пароль.
   */
  password?: string;
  /**
   * Логин.
   */
  username?: string;
  /**
   * Тип криптоплагина.
   */
  crypto_plugin?: CRYPTO_PLUGIN_TYPE;
  /**
   * Версия криптоплагина.
   */
  crypto_plugin_version?: string;
  /**
   * Пользовательское окружение.
   */
  user_environment?: string;
}

export interface IAuthConfiguration {
  authorization_endpoint: string;
  introspection_endpoint: string;
  default_page_uri: string;
  issuer: string;
  jwks_uri: string;
  response_types_supported: string[];
  token_endpoint: string;
  userinfo_endpoint: string;
  logout_uri: string;
  login_uri: string;
}

export interface IOAuthError {
  error: string;
  error_description: string;
}

export interface IOAuthSuccess {
  access_token: string;
  state?: string;
  expires_id: string;
}

export interface IAuthApiResponse {
  access_token: string;
  expires_in: string;
  refresh_token: string;
  token_type: string;
}

/**
 * Бизнес-сегменты приложения.
 */
export enum SEGMENTS {
  /**
   * Заявка на открытие дополнительного счета.
   */
  CAS_ACC_OPEN_SECOND_REQ = 'CAS_ACC_OPEN_SECOND_REQ',
  /**
   * Акцепт оферты.
   */
  CAS_OFFER_RKO_ACCEPTANCE_REQ = 'CAS_OFFER_RKO_ACCEPTANCE_REQ',
  /**
   * Заявка на присоединение к ГПБ Бизнес-Онлайн (Конституция. Заявление 1).
   */
  CAS_OFFER_JOIN_GPB_BO1_REQ = 'CAS_OFFER_JOIN_GPB_BO1_REQ',
  /**
   * Заявка на регистрацию / блокировку уполномоченного лица клиента (Конституция. Заявление 2).
   */
  CAS_OFFER_JOIN_GPB_BO2_REQ = 'CAS_OFFER_JOIN_GPB_BO2_REQ',
  /**
   * Документ свободного формата (Письма).
   */
  CAS_FREE_FORMAT_DOC = 'CAS_FREE_FORMAT_DOC',
  /**
   * Фрэйм ДБО БСС для главной страницы.
   */
  CAS_DBO_BSS_FRAME = 'CAS_DBO_BSS_FRAME',
  /**
   * Бесшовный переход в ДБО БСС.
   */
  CAS_SWITCHING_TO_DBO_BSS = 'CAS_SWITCHING_TO_DBO_BSS',
  /**
   * Заявка на присоединение к ЛК УВЭД.
   */
  CAS_OFFER_JOIN_PO_UVED = 'CAS_OFFER_JOIN_PO_UVED',
  /**
   * Бесшовный переход в ЛК УВЭД (Обработка).
   */
  CAS_SWITCHING_TO_UVED_PROCESS = 'CAS_SWITCHING_TO_UVED_PROCESS',
  /**
   * Бесшовный переход в ЛК УВЭД (Просмотр).
   */
  CAS_SWITCHING_TO_UVED_VIEW = 'CAS_SWITCHING_TO_UVED_VIEW',
  /**
   * Задолженности Клиентов в ФНС.
   */
  FNS = 'CAS_FNS',
}
