import type { IActionInfo, ICollectionResponse, IExecuteResult, router } from '@platform/core';
import type { IOption, IConfirmationExtraParams, WebIcon } from '@platform/ui';
import type { ISignInfo } from '../components';
import type { IVersifySignResult } from '../views';
import type { IDownloadedAttachment } from './attachment';
import type { ISaveResponse, IServerResp, IMetaData, IUserData, IParsedToken, AUTHORITY_VALIDATORS } from './common';
import type {
  IUserCertificate,
  IVerifySignByDateRequest,
  IVerifySignByDateResponce,
  IAuthorityRequest,
  IAuthorityResponse,
  IUniversalSignResponse,
} from './crypto';
import type {
  IBaseEntity,
  IExportRequest,
  IExportResponse,
  IPrintResponseDoc,
  IDigest,
  IUniversalSignEntity,
  IBaseSignedDocument,
  ISignedAttachment,
  SIGN_DATA_TYPE,
  UserType,
  IHistoryResponse,
} from './entities';

export interface IActionWebInfo<TContext, TResult> extends IActionInfo<TContext, TResult> {
  icon: WebIcon;
  label: string;
}

export interface IActionWithAuth extends IActionWebInfo<any, any> {
  authorities: string[];
}

export type IButtonConfig<TContext, TResult> = Record<string, Array<IActionWebInfo<TContext, TResult>>>;

type RouterMethods = typeof router.methods;

export type IRouterContext = Pick<RouterMethods, 'go' | 'goBack' | 'goForward' | 'push' | 'replace'>;

export enum FORM_TYPE {
  PRINT = 'PRINT',
  EXPORT = 'EXPORT',
}

/**
 * Параметры для модального окна истории изменений админа.
 */

export interface ShowAdminHistoryDialogParams<TDoc extends IBaseEntity> {
  /**
   * Список записей истории изменений.
   */
  historyList: IHistoryResponse[];
  /**
   * Документ, для которого запрошена история изменений.
   */
  doc: TDoc;
  /**
   * Функция, возвращающая заголовок модального окна.
   *
   * @param arg Документ.
   */
  getHeader(arg: TDoc): string;

  /**
   * Функция, возвращающая краткое или полное наименование организации.
   *
   * @param arg Документ.
   */
  getBankClientName(arg: TDoc): BankClientName;

  /**
   * Функция, возвращающая локализованный статус.
   *
   * @param arg Статус.
   */
  getStatusLabel(arg: string): string;

  /**
   * Обработчик при нажатии на имя юзера.
   *
   * @param userId - Id пользователя.
   */
  onUserNameClick?(userId: string): void;
}

export interface IBaseContext {
  /** Функция для получения информации текущего пользователя. */
  getUserInfo(): IParsedToken;

  /**
   * Показывает модалку, которая оповещает, что произошла ошибка при подписи документа.
   * Так-же будут отображены детали ошибки и ee описание.
   *
   * @param result Результат исполнения экшна.
   */
  showSignError(result: IExecuteResult<any, any>): void;

  /**
   * Показывает модалку, которая оповещает, что действие выполнено успешно.
   *
   * @param title Заголовок модалки.
   * @param content Содержимое модалки.
   * @param params Допонительные параметры.
   */
  showSuccess(title: string, content?: string, params?: IConfirmationExtraParams): void;

  /**
   * Показывает модалку, которая оповещает, что выполнение действия было провалено.
   *
   * @param title Заголовок модалки.
   * @param content Содержимое модалки.
   */
  showError(title: string, content?: string): void;

  /**
   * Показывает модалку с предупреждением.
   *
   * @param title Заголовок модалки.
   * @param content Содержимое модалки.
   */
  showWarning(title: string, content?: string): void;

  /**
   * Показывает модалку и возвращает промис, который будет зарезовлен или отклонен в зависимости от нажатой кнопки.
   *
   * @param content Содержимое модалки.
   * @param params Параметры модалки.
   */
  showConfirm(content: string, params?: IConfirmationExtraParams): Promise<any>;

  /**
   * Вызывает модалку для подписи.
   *
   * @param docs Документы для подписи.
   * @param certs Сертификаты.
   * @param selectedCertificate Предвыбранный сертификат.
   * @returns Промис, который будет зарезовлен при клике на "Подписать" и отклонен при закрытии или отмены модалки.
   */
  signDialog(
    docs: any[],
    certs: IUserCertificate[],
    selectedCertificate?: IUserCertificate
  ): Promise<{ thumbprint: string; cert: IUserCertificate }>;

  /**
   * Вызывает модалку для универсальной подписи.
   *
   * @param docs Документы для подписи.
   * @param certs Сертификаты.
   * @param selectedCertificate Предвыбранный сертификат.
   * @returns Промис, который будет зарезовлен при клике на "Подписать" и отклонен при закрытии или отмены модалки.
   */
  universalSignDialog(
    signDocuments: IUniversalSignEntity[],
    certs: IUserCertificate[],
    selectedCertificate?: IUserCertificate
  ): Promise<{ thumbprint: string; cert: IUserCertificate }>;

  /**
   * Вызывает модалку с результатами валидации подписи.
   *
   * @param results Результаты валидации.
   */
  showSignVerify(results: Array<IVersifySignResult<any>>): void;

  /** Показывает лоадер, который накрывает весь вьюпорт и запрещает взаимодействи с интерфейсом.
   *
   * @param options Параметры лоадера.
   */
  showLoader(options?: ILoaderOptions): void;

  /** Закрывает лоадер. */
  hideLoader(): void;

  /**
   * Откроет модалку для выбора настроек экспорта документа.
   *
   * @param numberOfDocuments Количество документов.
   * @param formType Тип формы - печать или экспорт.
   * @param defaultValue Формат по умолчанию.
   * @param options Доступные форматы.
   * @param locale Локаль.
   * @returns Промис, который вернет выбранные настройки.
   */
  showExportSetting(
    numberOfDocuments: number,
    formType: FORM_TYPE,
    defaultValue: string,
    options?: IOption[],
    locale?: any
  ): Promise<IExportActionSettings>;

  /** Методы роутера. */
  router: IRouterContext;

  /** Максимальное количество документов для печати. */
  maxPrintDoc: number;

  /**
   * Вызывает модалку с результатами валидации универсальной подписи.
   *
   * @param data Результаты валидации.
   */
  universalSignVerifyDialog(data: IUniversalSignVerifyActionResult[][]): void;

  /** Кнопка для экспорта подписи. */
  exportSignButton?: JSX.Element;

  /**
   * Открыть диалог установки криптомодуля.
   */
  openCryptoInstaller(params: IOpenCryptoModalActionParams): Promise<boolean>;

  /**
   * Диалог просмотра истории изменений на админской части.
   *
   */
  showAdminHistoryDialog<TDoc extends IBaseEntity>({
    historyList,
    doc,
    getHeader,
    getBankClientName,
    getStatusLabel,
    onUserNameClick,
  }: ShowAdminHistoryDialogParams<TDoc>): void;
}

export interface ILoaderOptions {
  text?: string;
  delay?: number;
}

export interface IActionServiceContext<TService> {
  service: TService;
}

export interface ICrudService<TDoc extends IBaseEntity> {
  delete(id: string): Promise<ISaveResponse<TDoc>>;
}

export interface IExportService {
  export(data: IExportRequest): Promise<IExportResponse>;
  exportList(data: IExportRequest): Promise<IExportResponse>;
}

export interface IPrintService {
  print(data: IExportRequest): Promise<IPrintResponseDoc>;
  printList(data: IExportRequest): Promise<IPrintResponseDoc>;
}

/**
 * Интерфейс сервиса подписи.
 */
export interface ISignService {
  /**
   * Получение дайджеста подписи.
   *
   * @param id Идентификатор документа.
   *
   * @deprecated
   */
  digest?(id: string): Promise<IServerResp<IDigest>>;

  /**
   * Метод сохранения данных на сервере.
   *
   * @param data Сохраняемые данные.
   */
  sign(data: ISignRequestData): Promise<IBaseEntity>;

  /**
   * Получение подписываемых данных.
   *
   * @param id Идентификатор документа.
   */
  getSignData?(id: string): Promise<IUniversalSignResponse>;
}

export interface ICertificateService {
  getList(metaData: IMetaData): Promise<ICollectionResponse<IUserCertificate>>;
  get(id: string): Promise<IUserCertificate>;
  getVerifyData(data: IVerifySignByDateRequest): Promise<IVerifySignByDateResponce>;
  checkAuthority(metaData: IAuthorityRequest): Promise<IAuthorityResponse[]>;
}

export interface ISignRequestData {
  certificateId: string;
  digest: string;
  documentId: string;
  signTypeId: string;
  signature: string;
}

export interface ICertificateUserContext {
  certificateUserService: ICertificateService;
  defaultUserCertId: string;
}

export interface IUserDataContext {
  userData: IUserData | Promise<IUserData>;
}

export type IActionContext<TService> = IActionServiceContext<TService> & IBaseContext;

export interface IHistoryDialogData<T> {
  document: T;
  historyList: any[];
}

export interface IExportActionSettings {
  selectedValue?: string;
  extension: string;
  splitReport: boolean;
}

export interface ISignatureDoc {
  certificate: {
    id: string;
    value: string;
    serialNumber: string;
    subject: string;
    validFrom: string;
    validTo: string;
  };
  sppVersion?: number;
  signedAt: string;
  id: string;
  ownerId: string;
  typeId: string;
  value: {
    digest: string;
    signature: string;
  };
}

export interface IAuthorityResult {
  certificate: IUserCertificate;
  error: boolean;
  checkResult: { [key in AUTHORITY_VALIDATORS]: boolean };
  errorMessages: { [key in AUTHORITY_VALIDATORS]?: string };
  /** Должность представителя. */
  clientPosition?: string;
  /** Документ позволяющий подписать заявление. */
  authorizingDocument?: string;
}

/**
 * Свойства результата действия универсальной проверки подписей.
 */
export interface IUniversalSignVerifyActionResult {
  /**
   * Документ или вложение.
   */
  signObject: IBaseSignedDocument | ISignedAttachment;
  /**
   * Результат проверки.
   */
  result: ISignInfo[];
  /**
   * Тип объекта подписи.
   */
  type: SIGN_DATA_TYPE;
}

/**
 * Формат ответа запроса дайджеста.
 */
export interface IDigestResult {
  /** Дайджест. */
  value: string;
  /** Версия. */
  version: number;
}

/**
 * Свойства сервиса документа для банковской АРМ.
 */
export interface IBaseSignedDocumentService {
  /**
   * Метод скачивания вложения в формате base64.
   */
  downloadBase64(id: string): Promise<IDownloadedAttachment>;
  /**
   * Метод получения дайджеста.
   */
  getDigest?(documentId: string, version: number): Promise<IDigestResult>;
}

/**
 * Свойства действия открытия модального окна установки криптомодуля.
 */
export interface IOpenCryptoModalActionParams {
  /**
   * Заголовок модального окна.
   */
  header?: string;
}

/**
 * Данные для отображения сущности истории в модальном окне истории изменений.
 */
export interface BankHistoryData {
  /**
   * Дата и время присвоения статуса документу.
   */
  date: string;
  /**
   * Тип пользователя.
   */
  userType?: UserType;
  /**
   * Имя пользователя.
   */
  userName: string;
  /**
   * Комментарий для Клиента.
   */
  commentForClient?: string;
  /**
   * Комментарий для Банка.
   */
  commentForBank?: string;
  /**
   * Комментарий для Клиента (в некоторых сервисах именование поле для комментария отличается).
   */
  infoForClient?: string;
  /**
   * Комментарий для Банка (в некоторых сервисах именование поле для комментария отличается).
   */
  infoForBank?: string;
  /**
   * Ссылка на сущность “Пользователь”.
   */
  userId: string;
  /**
   * Идентификатор записи истории.
   */
  id: string;
  /**
   * Статус записи истории.
   */
  status: string;
}

/**
 * Наименование клиента.
 */
export interface BankClientName {
  /**
   * Сокращенное либо полное наименование.
   */
  clientName?: string;
}

/**
 * Параметры, передаваемые в диалог истории изменений.
 */
export interface BankHistoryDialogData {
  /**
   * Коллекция записей истории изменений.
   */
  data: IHistoryResponse[];
  /**
   * Наименование клиента.
   */
  bankClient: BankClientName;
  /**
   * Заголовок.
   */
  header: string;
}
