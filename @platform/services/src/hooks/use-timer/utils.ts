import dayjs from 'dayjs';

/**
 * Получить время работы таймера.
 *
 * @param startDateTime Начальная дата и время.
 * @param endDateTime Конечная дата и время.
 */
export const getTimerInterval = (startDateTime: number | string, endDateTime: number | string) =>
  dayjs(endDateTime).subtract(dayjs(startDateTime).valueOf()).unix();
