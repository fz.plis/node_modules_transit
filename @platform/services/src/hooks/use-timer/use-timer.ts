import { useCallback, useState, useRef, useEffect } from 'react';
import type { ITimer } from '../../interfaces';

const DEFAULT_INTERVAL = 1;

/**
 * Хук обратного отсчета с заданным интервалом.
 *
 * @param initialValue Начальное значение таймера в секундах.
 *
 * @example
 * const { value, start, stop, reset, restart } = useTimer(120);
 * start();
 */
export const useTimer = (initialValue: number): ITimer => {
  const [value, setValue] = useState(initialValue);
  const timer = useRef<ReturnType<typeof setInterval> | null>(null);
  const timerInstance = useRef(false);

  useEffect(() => {
    timerInstance.current = true;

    return () => {
      timerInstance.current = false;
    };
  });

  const stop = useCallback(() => {
    if (timer.current) {
      clearInterval(timer.current);
    }
  }, [timer]);

  const reset = useCallback(
    (timerValue?: number) => {
      stop();

      if (timerInstance.current) {
        setValue(timerValue || initialValue);
      }
    },
    [stop, initialValue]
  );

  const start = useCallback(
    (interval: number = DEFAULT_INTERVAL) => {
      timer.current = setInterval(() => {
        if (timerInstance.current) {
          setValue(prevValue => {
            if (prevValue > 0) {
              return prevValue - 1;
            }

            stop();

            return 0;
          });
        }
      }, interval * 1000);
    },
    [stop, timer, setValue]
  );

  const restart = useCallback(
    (timerValue?: number) => {
      reset(timerValue);
      start();
    },
    [reset, start]
  );

  return {
    value,
    start,
    stop,
    reset,
    restart,
  };
};
