import { getTimerInterval } from '../utils';

describe('utils', () => {
  it('интервал должен быть равен 2с', () => {
    expect(getTimerInterval('2020-10-10T10:10:00', '2020-10-10T10:10:02')).toBe(2);
  });
});
