import { renderHook, act } from '@testing-library/react-hooks';
import { useTimer } from '../use-timer';

jest.useFakeTimers();

describe('use-timer', () => {
  afterAll(() => {
    jest.clearAllTimers();
  });

  it('начальное значение таймера должно быть равно 120', () => {
    const { result } = renderHook(() => useTimer(120));

    result.current.start();

    expect(result.current.value).toBe(120);
  });

  it('через 120с значение должно быть равным 0', () => {
    const { result } = renderHook(() => useTimer(120));

    result.current.start();

    act(() => {
      jest.advanceTimersByTime(120_000);
    });

    expect(result.current.value).toBe(0);
  });

  it('сброс значения. значение должно быть равным 120', () => {
    const { result } = renderHook(() => useTimer(120));

    result.current.start();

    act(() => {
      jest.advanceTimersByTime(50_000);
    });

    act(() => {
      result.current.reset();
    });

    expect(result.current.value).toBe(120);
  });

  it('значение должно быть равным 70', () => {
    const { result } = renderHook(() => useTimer(120));

    result.current.start();

    act(() => {
      jest.advanceTimersByTime(50_000);
    });

    expect(result.current.value).toBe(70);
  });

  it('таймер должен корректно остановиться и продолжить свою работу', () => {
    const { result } = renderHook(() => useTimer(120));

    result.current.start();

    expect(result.current.value).toBe(120);

    act(() => {
      jest.advanceTimersByTime(50_000);
      result.current.stop();
    });

    expect(result.current.value).toBe(70);

    act(() => {
      jest.advanceTimersByTime(50_000);
    });

    expect(result.current.value).toBe(70);

    result.current.start();

    act(() => {
      jest.advanceTimersByTime(50_000);
    });

    expect(result.current.value).toBe(20);
  });

  it('таймер должен корректно перезапуститься', () => {
    const { result } = renderHook(() => useTimer(120));

    act(() => {
      result.current.start();
    });

    expect(result.current.value).toBe(120);

    act(() => {
      jest.advanceTimersByTime(40_000);
      result.current.stop();
    });

    expect(result.current.value).toBe(80);

    act(() => {
      result.current.restart();
    });

    expect(result.current.value).toBe(120);

    act(() => {
      jest.advanceTimersByTime(50_000);
    });

    expect(result.current.value).toBe(70);
  });
});
