import { formatMoney, setLocale as setBigNumberLocale, moneyInRub } from '@platform/tools/big-number';
import { formatDate, formatMonthShort, setLocale as setDateTimeLocale } from '@platform/tools/date-time';
import { registerFormatter, localeChangeSubscribe, getLocale } from '@platform/tools/localization';
import type * as commonInstance from '..';
import { setScope, SCOPE } from '../utils';
import * as adminInstance from './index.common';

setScope(SCOPE.ADMIN);

registerFormatter('moneyInWords', moneyInRub);
registerFormatter('money', formatMoney);
registerFormatter('date', formatDate);
registerFormatter('monthShort', formatMonthShort);

localeChangeSubscribe((locale: string) => {
  setDateTimeLocale(locale);
  setBigNumberLocale(locale);
});
setDateTimeLocale(getLocale());
setBigNumberLocale(getLocale());

export * from './index.common';
export const common: typeof commonInstance = adminInstance;

export const alias = '@platform/services';

export * from './domains';
export * from './layouts';
export * from './admin-app';
export * from './user';
