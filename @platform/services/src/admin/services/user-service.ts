import { API_UAA_INTERNAL_URL } from '../../constants';
import type { IUser, IServerResp, IUaaUser, IDbo320Context, IRoleConflictResponse } from '../../interfaces';
import { request } from '../../utils';

const USER_DICTIONARY_URL = `${API_UAA_INTERNAL_URL}/user`;

export interface IUserServiceData {
  data: string;
}

export const userService = {
  block: (userId: string): Promise<IServerResp<IUserServiceData>> =>
    request({
      url: `${USER_DICTIONARY_URL}/block`,
      method: 'POST',
      data: `"${userId}"`,
    }).then(r => ({ data: r.data.data })),
  blockList: (ids: string[]): Promise<IServerResp<IUserServiceData>> =>
    request({
      url: `${USER_DICTIONARY_URL}/blockList`,
      method: 'POST',
      data: ids,
    }).then(r => ({ data: r.data.data })),
  unblock: (userId: string): Promise<IServerResp<IUser>> =>
    request({
      url: `${USER_DICTIONARY_URL}/unblock`,
      method: 'POST',
      data: `"${userId}"`,
    }).then(r => ({ data: r.data.data })),
  unblockList: (ids: string[]): Promise<IServerResp<IUserServiceData>> =>
    request({
      url: `${USER_DICTIONARY_URL}/unblockList`,
      method: 'POST',
      data: ids,
    }).then(r => ({ data: r.data.data })),
  update: (data: IUaaUser): Promise<IServerResp<IUaaUser>> => {
    const { id } = data as any;

    return request({
      url: `${USER_DICTIONARY_URL}`,
      method: 'PUT',
      data: { data, id },
    }).then(r => ({ data: r.data.data }));
  },
  dbo320Context: (id: string): Promise<IServerResp<IDbo320Context>> =>
    request({
      url: `${USER_DICTIONARY_URL}/dbo320/${id}`,
      method: 'GET',
      data: { id },
    }).then(r => ({ data: r.data.data })),
  /**
   * Проверка конфликта ролей.
   *
   * @param roleId ID добавляемой роли.
   * @param roleIds  Список ID ролей пользователя.
   */
  isRoleConflict: (roleId: string, roleIds?: string[]) =>
    request<IRoleConflictResponse>({
      method: 'POST',
      url: `${USER_DICTIONARY_URL}/is-role-conflict/${roleId}`,
      data: roleIds,
    }).then(res => res.data),
};
