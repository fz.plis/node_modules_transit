import type { AxiosResponse } from 'axios';
import { getBranchUrl } from '../../constants/service-urls';
import type { IBranchV2 } from '../../interfaces';
import { createBranchV2Service } from '../../services/helpers/branch';
import { request } from '../../utils';

const branchV2Url = `${getBranchUrl(true)}/v2`;

const axiosResponse = (r: AxiosResponse) => {
  const { data, code, message } = r.data;

  if (code) {
    throw {
      response: {
        data: {
          errorInfo: {
            code,
            message,
          },
        },
      },
    };
  }

  return data;
};

export const branchV2 = {
  ...createBranchV2Service(branchV2Url),
  /** Получить все действующие филиалы. */
  getAllActiveFilials: (): Promise<string[]> =>
    request({
      url: `${branchV2Url}/filial-branches`,
    }).then(r => r.data.list),
  /** Получить действующий филиал по идентификатору филиала или подразделения. */
  findFilialById: (branchId: string): Promise<IBranchV2> =>
    request({
      url: `${branchV2Url}/filial/${branchId}`,
    }).then(axiosResponse),
  /** Поиск действующего подразделения по коду подразделения. */
  findByAbsCode: (absCode: string): Promise<IBranchV2> =>
    request({
      url: `${branchV2Url}/find-by-abs-code/${absCode}`,
    }).then(axiosResponse),
  /** Поиск действующих подразделений по списку кодов подразделений. */
  findByAbsCodes: (absCodes: string[]): Promise<IBranchV2[]> =>
    request({
      url: `${branchV2Url}/find-by-abs-codes`,
      method: 'POST',
      data: absCodes,
    }).then(r => r.data.list),
  /** Поиск действующих подразделений по БИК. */
  findByBic: (bic: string): Promise<IBranchV2[]> =>
    request({
      url: `${branchV2Url}/find-by-bic/${bic}`,
    }).then(r => r.data.list),
  /** Поиск действующего филиала или подразделения по наименованию. */
  findByName: (name: string): Promise<IBranchV2[]> =>
    request({
      url: `${branchV2Url}/find-by-name/${name}`,
    }).then(r => r.data.list),
  /** Получить действующий филиал по его номеру в АБС. */
  findFilialByAbsNumber: (absNumber: string): Promise<IBranchV2> =>
    request({
      url: `${branchV2Url}/find-filial-by-abs-number/${absNumber}`,
    }).then(axiosResponse),
  /** Поиск действующего филиала по БИК. */
  findFilialByBic: (bic: string): Promise<IBranchV2> =>
    request({
      url: `${branchV2Url}/find-filial-by-bic/${bic}`,
    }).then(axiosResponse),
  /** Получить действующий головной офис. */
  getHeadUnit: (): Promise<IBranchV2> =>
    request({
      url: `${branchV2Url}/head-unit`,
    }).then(axiosResponse),
  /** Получить список действующих подразделений банка, находящихся в том же филиале, что и текущее подразделение. */
  getSameFilial: (branchId: string): Promise<IBranchV2[]> =>
    request({
      url: `${branchV2Url}/same-filial/${branchId}`,
    }).then(r => r.data.list),
  /** Синхронизировать филиалы со справочником КНОСИС. */
  sync: (): Promise<void> =>
    request({
      url: `${branchV2Url}/sync`,
      method: 'PUT',
    }).then(axiosResponse),
};
