import { API_UAA_INTERNAL_URL } from '../../constants';
import type { ILoginApiRequest, ILoginPasswordRequest, ILoginResponse, IAuthConfiguration } from '../../interfaces';
import { request, transformError, queryString, createAuthRequestFunc, transformPassword } from '../../utils';

const bankLoginRequest = createAuthRequestFunc<ILoginApiRequest>(`${API_UAA_INTERNAL_URL}/login`);

export const authService = {
  getAuthConfiguration: () =>
    request<IAuthConfiguration>({
      disableTokenHeader: true,
      url: `${API_UAA_INTERNAL_URL}/.well-known/openid-configuration`,
      method: 'GET',
    }).then(r => r.data),
  login: {
    byPassword: bankLoginRequest<ILoginPasswordRequest, ILoginResponse>(transformPassword),
  },
  logout: () =>
    request({
      url: `${API_UAA_INTERNAL_URL}/logout`,
      method: 'POST',
      disableTokenHeader: true,
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      data: queryString({ body: 'exit' }),
    })
      .then(r => r.data)
      .catch(transformError),
};
