import { CLIENT_DICTIONARY_URL_INTERNAL } from '../../constants';
import type { IBranchOfficial } from '../../interfaces';
import { getDictionaryService, request } from '../../utils';

const BRANCH_OFFICIAL_URL = `${CLIENT_DICTIONARY_URL_INTERNAL}/branch-official`;

export const branchOfficialService = {
  ...getDictionaryService<IBranchOfficial>(BRANCH_OFFICIAL_URL),
  getUserBranches: (userId: string): Promise<IBranchOfficial[]> =>
    request({
      url: `${BRANCH_OFFICIAL_URL}/branches/${userId}`,
    }).then((r: any) => r.data.data.data),
  updateUserBranches: (userId: string, branchesIds: string[]): Promise<IBranchOfficial[]> =>
    request({
      url: `${BRANCH_OFFICIAL_URL}/update-branches`,
      method: 'PUT',
      data: {
        id: userId,
        data: branchesIds,
      },
    }).then(r => r.data.data.data),
};
