import { HEADERS_PARAM } from '../../constants';
import type {
  ACCEPT_MODE,
  ICertificateRequest,
  ICertificateRequestAccept,
  ICertificateRequestHistory,
  IEskOrganization,
} from '../../interfaces';
import { createCertificateRequestService } from '../../services/helpers';
import { getFileName, getNewDictionaryService } from '../../utils';
import { request } from '../../utils/request';

const clientUrl = '/api/client-dictionary-internal/dictionary/client';
const baseUrl = `${clientUrl}/cert-request`;

export const certificateRequestService = {
  ...getNewDictionaryService<ICertificateRequest>(baseUrl),
  ...createCertificateRequestService(baseUrl),
  /** Выгрузка реквизитов ЭП для проверки. */
  exportSign: (requestId: string) =>
    request({
      url: `${baseUrl}/${requestId}/export/sign`,
      responseType: 'blob',
    }).then(result => {
      const param = result.headers[HEADERS_PARAM.CONTENT_DISPOSITION] || '';

      return {
        fileName: getFileName(param),
        type: result.headers[HEADERS_PARAM.CONTENT_TYPE],
        data: result.data,
      };
    }),
  /** Получить историю изменений заявления по его идентификатору. */
  getHistory: (id: string): Promise<ICertificateRequestHistory[]> =>
    request({
      url: `${baseUrl}/history/${id}`,
    }).then(d => d.data.list),
  /** Одобрить запрос на регистрацию сертификата ЭП. */
  accept: (
    acceptMode: ACCEPT_MODE,
    certRequestId: string,
    eskOrganization?: IEskOrganization,
    personEskId?: string,
    noteFromBank?: string
  ): Promise<ICertificateRequestAccept> =>
    request({
      url: `${baseUrl}/accept`,
      method: 'POST',
      data: {
        acceptMode,
        certRequestId,
        eskOrganization,
        personEskId,
        noteFromBank,
      },
    }).then(res => res.data.data),
  /** Отклонить запрос на регистрацию сертификата ЭП. */
  reject: (certRequestId: string, noteFromBank?: string) =>
    request({
      url: `${baseUrl}/reject`,
      method: 'POST',
      data: {
        certRequestId,
        noteFromBank,
      },
    }).then(res => res.data.data),
  /** Повторить выгрузку. */
  repeatSend: (certRequestId: string): Promise<ICertificateRequest> =>
    request({
      url: `${baseUrl}/repeat-send`,
      method: 'POST',
      data: {
        data: certRequestId,
      },
    }).then(res => res.data.data),
};
