import type { IOrgCert, IOrgCertFileInfo, ICertResponse, ICertParseResponse, IAddCertificateResponse } from '../../interfaces';
import type { IOrganizationCertificatesService } from '../../services/helpers/org-certs';
import { getNewOrgCertsService, certificateResponse } from '../../services/helpers/org-certs';
import { request } from '../../utils/request';

export interface IOrgCertsService extends IOrganizationCertificatesService {
  getAccredited(): Promise<string[]>;
  reject(orgOfficialCertId: string, noteForClient?: string): Promise<IOrgCert>;
  accept(orgOfficialCertId: string, noteForClient?: string): Promise<IOrgCert>;
  parse(rawCertificate: string): Promise<IOrgCertFileInfo>;
  addUserCertificate(rawCertificate: string, bankClientId: string, userId: string): Promise<IAddCertificateResponse>;
}

const CLIENT_DICTIONARY_URL = '/api/client-dictionary-internal/internal/dictionary/client';
const SIGNATURE_URL = '/api/signature-internal/internal/sign';

/**
 * Сертификаты представителя клиента Банка.
 */
export const orgCertsService: IOrgCertsService = {
  ...getNewOrgCertsService(CLIENT_DICTIONARY_URL, SIGNATURE_URL),
  getAccredited: async () => {
    const { data: response } = await request({
      method: 'GET',
      url: `${SIGNATURE_URL}/ca/accredited`,
    });

    return response.list;
  },
  reject: async (orgOfficialCertId, noteForClient) => {
    const response = await request({
      url: `${CLIENT_DICTIONARY_URL}/org-official-cert/reject`,
      method: 'POST',
      data: {
        orgOfficialCertId,
        noteForClient,
      },
    });

    return certificateResponse(response);
  },
  accept: async (orgOfficialCertId, noteForClient) => {
    const response = await request({
      url: `${CLIENT_DICTIONARY_URL}/org-official-cert/accept`,
      method: 'POST',
      data: {
        orgOfficialCertId,
        noteForClient,
      },
    });

    return certificateResponse(response);
  },

  parse: async rawCertificate => {
    const { data: response } = await request({
      url: `${SIGNATURE_URL}/certificate/parse`,
      method: 'POST',
      data: {
        rawCertificate,
      },
    });
    const { data, message, code } = response as ICertResponse<ICertParseResponse>;

    if (code) {
      throw message;
    }

    return {
      issuer: data.authorityName,
      validFrom: data.validFrom,
      validTo: data.validTo,
      organizationName: data.subjectOrganization,
      serialNumber: data.serialNumber,
      whomIssued: `${data.subjectSurname}${data.subjectGivenName ? ' ' + data.subjectGivenName : ''}`,
      thumbprint: data.footprint,
      subjectCommonName: data.subject,
      subjectOrganization: data.subjectOrganization,
      subjectOrganizationalUnit: data.subjectOrganizationalUnit,
      subjectLocality: data.subjectLocality,
      subjectCountry: data.subjectCountry,
      subjectEmail: data.subjectEmail,
      subjectGivenName: data.subjectGivenName,
      subjectSurname: data.subjectSurname,
      subjectTitle: data.subjectTitle,
      subjectInn: data.subjectInn,
      subjectOgrn: data.subjectOgrn,
    };
  },
  addUserCertificate: (rawCertificate: string, bankClientId: string, userId: string) =>
    request<IAddCertificateResponse>({
      method: 'POST',
      url: `${CLIENT_DICTIONARY_URL}/org-official-cert`,
      data: {
        bankClientId,
        userId,
        certificateData: rawCertificate,
      },
    }).then(res => res.data),
};
