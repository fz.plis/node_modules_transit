import { API_UAA_INTERNAL_URL } from '../../constants';
import type { IServerResp, ISession } from '../../interfaces';
import { getDictionaryService, request } from '../../utils';

const SESSION_URL = `${API_UAA_INTERNAL_URL}/sessions`;

export const sessionsService = {
  ...getDictionaryService<ISession>(SESSION_URL),
  remove: (id: string): Promise<IServerResp<boolean>> =>
    request({
      url: `${SESSION_URL}/${id}`,
      method: 'DELETE',
    }).then(r => ({ data: r.data.data })),
  /**
   * Закрывает все активные сессии клиентских пользователей.
   *
   * @returns Массив закрытых сессиий клиентов.
   */
  closeActiveClientSessions(): Promise<ISession[]> {
    return request({
      url: `${SESSION_URL}/client-active`,
      method: 'DELETE',
    }).then(r => r.data.list);
  },
};
