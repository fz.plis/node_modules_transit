import { API_UAA_INTERNAL_URL } from '../../constants';
import type { ITotp, IServerResp } from '../../interfaces';
import { request } from '../../utils';

const TOTP_URL = `${API_UAA_INTERNAL_URL}/totp`;

const handleResult = <T extends { data: any }>(r: { data: IServerResp<T> }) => {
  const { data, errorInfo } = r.data;

  if (errorInfo) {
    throw errorInfo;
  }

  return data.data;
};

export const totpService = {
  get(id: string): Promise<ITotp> {
    return request({
      url: `${TOTP_URL}/${id}`,
    }).then(d => {
      if (d.data.errorInfo) {
        throw {
          response: {
            data: {
              errorInfo: d.data.errorInfo,
            },
          },
        };
      }

      return d.data.data.data as ITotp;
    });
  },
  create(data: ITotp): Promise<IServerResp<ITotp>> {
    return request({
      url: TOTP_URL,
      method: 'POST',
      data: { data },
    }).then(handleResult);
  },
  update(id: string, data: ITotp): Promise<IServerResp<ITotp>> {
    return request({
      url: TOTP_URL,
      method: 'PUT',
      data: { id, data },
    }).then(handleResult);
  },
  remove: (id: string): Promise<IServerResp<boolean>> =>
    request({
      url: `${TOTP_URL}/${id}`,
      method: 'DELETE',
    }).then(r => ({ data: r.data.data })),
};
