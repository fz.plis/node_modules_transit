import { API_UAA_INTERNAL_URL } from '../../constants';
import type { IServerResp, IMfaChains, IMfaAvailableChain } from '../../interfaces';
import { getDictionaryService, request } from '../../utils';

const MFA_CHAINS_URL = `${API_UAA_INTERNAL_URL}/mfachain`;

const handleResult = <T extends { data: any }>(r: { data: IServerResp<T> }) => {
  const { data, errorInfo } = r.data;

  return { errorInfo, data: data.data };
};

export const mfaChainsService = {
  ...getDictionaryService<IMfaChains>(MFA_CHAINS_URL),
  update(data: IMfaChains): Promise<IServerResp<IMfaChains>> {
    return request({
      url: MFA_CHAINS_URL,
      method: 'PUT',
      data: { data, id: data.id },
    }).then(handleResult);
  },
  available(id: string): Promise<IServerResp<IMfaAvailableChain[]>> {
    return request({
      url: `${MFA_CHAINS_URL}/user/available/${id}`,
      method: 'GET',
    }).then(handleResult);
  },
  default(id: string): Promise<IServerResp<IMfaAvailableChain[]>> {
    return request({
      url: `${MFA_CHAINS_URL}/user/default/${id}`,
      method: 'GET',
    }).then(handleResult);
  },
};
