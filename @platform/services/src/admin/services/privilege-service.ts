import { API_UAA_INTERNAL_URL } from '../../constants';
import type { IPrivileges, IServerResp } from '../../interfaces';
import { getDictionaryService, request } from '../../utils';

const PRIVILEGE_URL = `${API_UAA_INTERNAL_URL}/privilege`;

const handleResult = <T extends { data: any }>(r: { data: IServerResp<T> }) => {
  const { data, errorInfo } = r.data;

  return { errorInfo, data: data.data };
};

export const privilegeService = {
  ...getDictionaryService<IPrivileges>(PRIVILEGE_URL),
  update(data: IPrivileges): Promise<IServerResp<IPrivileges>> {
    return request({
      url: PRIVILEGE_URL,
      method: 'PUT',
      data: { data, id: data.id },
    }).then(handleResult);
  },
  delete(id: string): Promise<IServerResp<IPrivileges>> {
    return request({
      url: `${PRIVILEGE_URL}/${id}`,
      method: 'DELETE',
    }).then(handleResult);
  },
};
