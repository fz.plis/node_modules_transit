import { API_UAA_INTERNAL_URL } from '../../constants';
import type { IResource } from '../../interfaces';
import { getDictionaryService } from '../../utils';

const RESOURCE_SERVICE_URL = `${API_UAA_INTERNAL_URL}/resource`;

export const resourceService = getDictionaryService<IResource>(RESOURCE_SERVICE_URL);
