import axios from 'axios';
import { noop } from '@platform/ui';
import { HEADERS_PARAM } from '../../constants';
import type { IAttachment, IFileUploadOptions, IDownloadedAttachment } from '../../interfaces';
import { createAttachmentService } from '../../services/helpers';
import { getFileName, request } from '../../utils';

const ATTACHMENT_SERVICE_INTERNAL_URL = '/api/filestorage-internal/filestorage/internal';

export const attachmentService = {
  ...createAttachmentService(ATTACHMENT_SERVICE_INTERNAL_URL),
  /**
   * Метод для скачивания файла в формате Base64.
   *
   * @param attachmentId Идентификатор файла.
   */
  downloadBase64: async (attachmentId: string): Promise<IDownloadedAttachment> => {
    const { headers, data } = await request({
      url: `${ATTACHMENT_SERVICE_INTERNAL_URL}/${attachmentId}/data/base64`,
    });

    return {
      fileName: getFileName(headers[HEADERS_PARAM.CONTENT_DISPOSITION] || ''),
      type: headers[HEADERS_PARAM.CONTENT_TYPE],
      data,
    };
  },
  /**
   * Метод для загрузки вложения.
   *
   * @param file - Объекта файла.
   * @param file.onUploadProgress Обработчик для прогресса загрузки вложения.
   * @param file.onCancel Обработчик отмены.
   */
  upload: (file: File, { onUploadProgress = noop, onCancel = noop }: IFileUploadOptions = {}): Promise<IAttachment> => {
    const formData = new FormData();

    formData.append('file', file, file.name);

    return request({
      onUploadProgress,
      cancelToken: new axios.CancelToken(onCancel),
      url: `${ATTACHMENT_SERVICE_INTERNAL_URL}/form-data/upload`,
      method: 'POST',
      data: formData,
    }).then(d => d.data.data);
  },
};
