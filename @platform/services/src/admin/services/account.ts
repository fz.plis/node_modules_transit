import { getAccountUrl } from '../../constants/service-urls';
import type { IAccount } from '../../interfaces';
import { createAccountClientService } from '../../services/helpers';
import { getDictionaryService } from '../../utils';

const accountUrl = getAccountUrl(true);

export const accountService = {
  ...getDictionaryService<IAccount>(accountUrl),
  ...createAccountClientService(accountUrl),
};
