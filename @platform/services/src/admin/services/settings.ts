import type { ISetting, IServerResp } from '../../interfaces';
import { getNewAppSettings, saveAppSettingsResponse } from '../../services/helpers';
import { getNewDictionaryService, request } from '../../utils';

const SETTINGS_URL = '/api/settings-bank/settings/system';
const SETTINGS_ADMIN_URL = '/api/settings-bank/settings/admin';

const { getList, get } = getNewDictionaryService<ISetting>(SETTINGS_ADMIN_URL);

export const appSettingsService = {
  ...getNewAppSettings(SETTINGS_URL),
  get,
  getList,
  create: (data: ISetting) =>
    request<IServerResp<ISetting>>({
      method: 'POST',
      url: SETTINGS_ADMIN_URL,
      data: {
        data,
      },
    }).then(saveAppSettingsResponse),
  update: (data: ISetting) =>
    request<IServerResp<ISetting>>({
      method: 'PUT',
      url: SETTINGS_ADMIN_URL,
      data: {
        data,
        id: data.id,
      },
    }).then(saveAppSettingsResponse),
  delete: (id: string) =>
    request<IServerResp<ISetting>>({
      method: 'DELETE',
      url: `${SETTINGS_ADMIN_URL}/${id}`,
    }).then(saveAppSettingsResponse),
};
