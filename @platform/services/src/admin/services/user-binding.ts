import { API_URL, CLIENT_DICTIONARY_SERVICE_URL } from '../../constants';
import type { IServerResp, IBindF1DTO, IClientOfficial } from '../../interfaces';
import { request } from '../../utils';

export const USER_BINDING_URL = `${API_URL}/${CLIENT_DICTIONARY_SERVICE_URL}-internal//user-binding/bind-client-f1`;

export const userBindingService = {
  bindClient(dto: IBindF1DTO): Promise<IServerResp<IClientOfficial>> {
    return request({
      url: USER_BINDING_URL,
      method: 'POST',
      data: dto,
    }).then(result => {
      const { data, errorInfo } = result.data;

      return { errorInfo, data: data.data };
    });
  },
};
