import { getLocaleUrl } from '../../constants';
import { getDefaultLocaleService } from '../../utils';

export const dictionaryLocaleService = getDefaultLocaleService(getLocaleUrl(true));
