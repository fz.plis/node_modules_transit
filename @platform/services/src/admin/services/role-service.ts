import { conditions, API_UAA_INTERNAL_URL } from '../../constants';
import type { IRole, IServerResp, IMetaData } from '../../interfaces';
import { getDictionaryService, request } from '../../utils';

export const ROLE_URL = `${API_UAA_INTERNAL_URL}/role`;

const handleResult = <T extends { data: any }>(r: { data: IServerResp<T> }) => {
  const { data, errorInfo } = r.data;

  return { errorInfo, data: data.data };
};

export const roleService = {
  ...getDictionaryService<IRole>(ROLE_URL),
  update(data: IRole): Promise<IServerResp<IRole>> {
    return request({
      url: ROLE_URL,
      method: 'PUT',
      data: { data, id: data.id },
    }).then(handleResult);
  },
  delete(id: string): Promise<IServerResp<IRole>> {
    return request({
      url: `${ROLE_URL}/${id}`,
      method: 'DELETE',
    }).then(handleResult);
  },
  getClientRoles: (metaData: IMetaData) => {
    metaData.filters = {
      clientAllowed: {
        fieldName: 'clientAllowed',
        value: true,
        condition: conditions.eq,
      },
    };

    return roleService.getList(metaData);
  },
  getBankRoles: (metaData: IMetaData) => {
    metaData.filters = {
      bankAllowed: {
        fieldName: 'bankAllowed',
        value: true,
        condition: conditions.eq,
      },
    };

    return roleService.getList(metaData);
  },
};
