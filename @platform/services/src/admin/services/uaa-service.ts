import { conditions, API_UAA_INTERNAL_URL } from '../../constants';
import type {
  IServerResp,
  IClientUser,
  ICompleteRegisterResponse,
  ISearchByName,
  IRegisterRequestData,
  IRegisterResponse,
  IBankUser,
  IUserInfo,
  IUpdateUserResponse,
  IUaaRole,
  ITechnicalUser,
  IUserAuthoritiesResponse,
  IDataResponse,
} from '../../interfaces';
import { request, getDictionaryService, getNewDictionaryService } from '../../utils';

const UAA_CLIENT_USER_URL = `${API_UAA_INTERNAL_URL}/admin/clientuser`;
const UAA_BANK_USER_URL = `${API_UAA_INTERNAL_URL}/admin/bankuser`;
const UAA_TECHNICAL_USER_URL = `${API_UAA_INTERNAL_URL}/admin/technical-user`;

const handleResult = <T>(r: { data: IServerResp<T> }) => {
  if (r.data.errorInfo) {
    throw { response: { data: r.data } };
  }

  return r.data.data;
};

export const clientUser = {
  ...getDictionaryService<IClientUser>(UAA_CLIENT_USER_URL),
  getEnriched: (id: string): Promise<IClientUser> =>
    request({
      url: `${UAA_CLIENT_USER_URL}/enriched/${id}`,
    }).then(res => res.data.data.data),
  getByUaaUserId: (id: string): Promise<IClientUser> =>
    request({
      url: `${UAA_CLIENT_USER_URL}/findByUaaId`,
      method: 'POST',
      data: { id },
    }).then(r => {
      if (r.data.errorInfo) {
        throw { response: { data: r.data } };
      }

      return r.data.data.data;
    }),
  create: (data: IRegisterRequestData) =>
    request<IServerResp<IRegisterResponse>>({
      url: UAA_CLIENT_USER_URL,
      method: 'POST',
      data,
    }).then(handleResult),
  update: (data: IClientUser): Promise<IServerResp<IClientUser>> => {
    const { id, ...dataToSend } = data as any;

    return request({
      url: UAA_CLIENT_USER_URL,
      method: 'PUT',
      data: { data: dataToSend, id },
    }).then(r => ({ data: r.data.data }));
  },
  find: (params: ISearchByName, fieldName: string) =>
    clientUser.getList({
      pageSize: params.pageSize!,
      offset: params.offset!,
      filters: {
        name: {
          fieldName,
          value: params.search,
          condition: conditions.contains,
        },
      },
    }),
  completeRegister: (requestId: string, smsCode: string) =>
    request<IServerResp<ICompleteRegisterResponse>>({
      url: `${UAA_CLIENT_USER_URL}/completeRegister`,
      method: 'POST',
      data: { requestId, smsCode },
    }).then(handleResult),
  completePhoneChange: (requestId: string, smsCode: string) =>
    request<IServerResp<ICompleteRegisterResponse>>({
      url: `${UAA_CLIENT_USER_URL}/completePhoneChange`,
      method: 'POST',
      data: { requestId, smsCode },
    }).then(handleResult),
  getRoles: (userId: string) =>
    request<IServerResp<IUaaRole[]>>({
      method: 'GET',
      url: `${UAA_CLIENT_USER_URL}/${userId}/roles`,
    }).then(res => res.data.data),
  addRole: (userId: string, role: IUaaRole) =>
    request({
      method: 'POST',
      url: `${UAA_CLIENT_USER_URL}/${userId}/assign-role`,
      data: role,
    }).then(res => res.data),
  removeRole: (userId: string, roleIds: string[]) =>
    request({
      method: 'POST',
      url: `${UAA_CLIENT_USER_URL}/${userId}/revoke-role`,
      data: roleIds,
    }).then(res => res.data),
  executeAccessRules: (userId: string): Promise<IServerResp<IClientUser>> =>
    request({
      method: 'POST',
      url: `${API_UAA_INTERNAL_URL}/admin/access-rule/execute-for-client-user/${userId}`,
    }).then(res => res.data),
};

const bankUser = {
  ...getDictionaryService<IBankUser>(UAA_BANK_USER_URL),
  getCurrent: () =>
    request<{ data: IServerResp<IBankUser> }>({
      url: `${UAA_BANK_USER_URL}/getDetails`,
      method: 'POST',
    }).then(resp => resp.data.data.data),
  searchAd: (searchText: string) =>
    request({
      url: `${UAA_BANK_USER_URL}/searchAd`,
      method: 'POST',
      data: {
        searchText,
      },
    }).then((resp: { data: IServerResp<IBankUser[]> }) => resp.data),
  completeRegister: (requestId: string, smsCode: string) =>
    request<IServerResp<ICompleteRegisterResponse>>({
      url: `${UAA_BANK_USER_URL}/completeRegister`,
      method: 'POST',
      data: { requestId, smsCode },
    }).then(handleResult),
  update: (data: IUserInfo, id?: string) =>
    request<IServerResp<IUpdateUserResponse>>({
      url: UAA_BANK_USER_URL,
      method: 'PUT',
      data: { data, id },
    }).then(handleResult),
  getRoles: (id: string) =>
    request<IServerResp<IUaaRole[]>>({
      url: `${UAA_BANK_USER_URL}/${id}/roles`,
      method: 'GET',
    }).then(handleResult),
  addRole: (id: string, role: IUaaRole): Promise<IServerResp<Record<string, unknown>>> =>
    request({
      url: `${UAA_BANK_USER_URL}/${id}/assign-role`,
      method: 'POST',
      data: role,
    }).then(r => ({ data: r.data })),
  editRole: (id: string, role: IUaaRole): Promise<IServerResp<IUaaRole>> =>
    request({
      url: `${UAA_BANK_USER_URL}/${id}/edit-role`,
      method: 'POST',
      data: role,
    }).then(r => ({ data: r.data })),
  deleteRoles: (id: string, roleIds: string[]): Promise<IServerResp<Record<string, unknown>>> =>
    request({
      url: `${UAA_BANK_USER_URL}/${id}/revoke-role`,
      method: 'POST',
      data: roleIds,
    }).then(r => ({ data: r.data })),
};

const technicalUser = {
  ...getNewDictionaryService<ITechnicalUser>(UAA_TECHNICAL_USER_URL),
  get: (id: string) =>
    request<IServerResp<ITechnicalUser>>({
      url: `${UAA_TECHNICAL_USER_URL}/${id}`,
      method: 'GET',
    }).then(handleResult),
  create: (user: ITechnicalUser) =>
    request<IServerResp<ITechnicalUser>>({
      url: UAA_TECHNICAL_USER_URL,
      method: 'POST',
      data: {
        data: user,
      },
    }).then(handleResult),
  update: ({ id, ...user }: ITechnicalUser) =>
    request<IServerResp<ITechnicalUser>>({
      url: UAA_TECHNICAL_USER_URL,
      method: 'PUT',
      data: {
        id,
        data: user,
      },
    }).then(handleResult),
  getRoles: (id: string) =>
    request<IServerResp<IUaaRole[]>>({
      url: `${UAA_TECHNICAL_USER_URL}/${id}/roles`,
      method: 'GET',
    }).then(handleResult),
  addRole: (id: string, role: IUaaRole) =>
    request({
      url: `${UAA_TECHNICAL_USER_URL}/${id}/assign-role`,
      method: 'POST',
      data: role,
    }).then(handleResult),
  editRole: (id: string, role: IUaaRole) =>
    request({
      url: `${UAA_TECHNICAL_USER_URL}/${id}/edit-role`,
      method: 'POST',
      data: role,
    }).then(handleResult),
  deleteRoles: (id: string, roleIds: string[]) =>
    request({
      url: `${UAA_TECHNICAL_USER_URL}/${id}/revoke-role`,
      method: 'POST',
      data: roleIds,
    }).then(handleResult),
};

export const uaaService = {
  clientUser,
  bankUser,
  technicalUser,
  getUserInfo: () =>
    request<IServerResp<{ data: IBankUser }>>({
      url: `${UAA_BANK_USER_URL}/getDetails`,
      method: 'POST',
    }).then(handleResult),
  getAuthorities: () =>
    request<IDataResponse<IUserAuthoritiesResponse>>({
      url: `${API_UAA_INTERNAL_URL}/user-info/user-authorities`,
    }).then(res => res.data.data),
};
