import {
  getOkfsfUrl,
  getOkopfUrl,
  getCurrencyUrl,
  getActivityTypeUrl,
  getClientOfficialUrl,
  getCountryUrl,
  getAccountTypeUrl,
  getBankClientUrl,
  getGkUrl,
  getBranchUrl,
  getFiasUrl,
  CLIENT_DICTIONARY_URL_INTERNAL,
} from '../../constants/service-urls';
import type {
  IOkfs,
  IOkopf,
  ICurrency,
  IActivityType,
  IClientOfficial,
  ICountry,
  IAccountType,
  IBankClient,
  IGozContract,
  IServerResp,
  IDocumentType,
  ISearchF1DTO,
  IBranch,
} from '../../interfaces';
import { createBankClientService } from '../../services/helpers';
import { createClientBranchService } from '../../services/helpers/branch';
import { createClientOffical } from '../../services/helpers/client-official';
import { getDictionaryService, getNewDictionaryService, request } from '../../utils';
import { getDefaultSearchService } from '../../utils/default-search-service';
import { branchV2 } from './branch';

const okfs = getDictionaryService<IOkfs>(getOkfsfUrl(true));
const okopf = getDictionaryService<IOkopf>(getOkopfUrl(true));
const currency = getDictionaryService<ICurrency>(getCurrencyUrl(true));
const activityType = getDictionaryService<IActivityType>(getActivityTypeUrl(true));
const country = getDictionaryService<ICountry>(getCountryUrl(true));
const accountType = getDictionaryService<IAccountType>(getAccountTypeUrl(true));
const gk = getDictionaryService<IGozContract>(getGkUrl(true));
const documentType = getNewDictionaryService<IDocumentType>(`${CLIENT_DICTIONARY_URL_INTERNAL}/document-type`);
const fias = getDefaultSearchService(getFiasUrl(true));

const clientOfficialUrl = getClientOfficialUrl(true);

const handleResult = <T extends { data: any }>(r: { data: IServerResp<T> }) => {
  const { data, errorInfo } = r.data;

  return { errorInfo, data: data.data };
};

const clientOfficial = {
  ...getDictionaryService<IClientOfficial>(clientOfficialUrl),
  ...createClientOffical(clientOfficialUrl),
  getListClientOfficialForUser: (id: string): Promise<IServerResp<IClientOfficial[]>> =>
    request({
      url: `${clientOfficialUrl}/user/${id}`,
    }).then(handleResult),
  delete: (id: string): Promise<IServerResp<IClientOfficial>> =>
    request({
      url: `${clientOfficialUrl}/${id}`,
      method: 'DELETE',
    }).then(handleResult),
};
const bankClientUrl = getBankClientUrl(true);

const bankClient = {
  ...getDictionaryService<IBankClient>(bankClientUrl),
  ...createBankClientService(bankClientUrl),
  getUserClients: (userId: string): Promise<IBankClient[]> =>
    request({
      url: `${bankClientUrl}/user-org/${userId}`,
    }).then((r: any) => r.data.data.data),
  block: (id: string): Promise<IServerResp<IBankClient>> =>
    request({
      url: `${bankClientUrl}/block`,
      method: 'PUT',
      data: { id },
    }).then(r => ({ data: r.data.data })),
  unblock: (id: string): Promise<IServerResp<IBankClient>> =>
    request({
      url: `${bankClientUrl}/unblock`,
      method: 'PUT',
      data: { id },
    }).then(r => ({ data: r.data.data })),
  update: (data: IBankClient): Promise<IServerResp<IBankClient>> => {
    const { id, ...dataToSend } = data as any;

    return request({
      url: `${bankClientUrl}`,
      method: 'PUT',
      data: { data: dataToSend, id },
    }).then(r => ({ data: r.data.data, errorInfo: r.data.errorInfo }));
  },
  searchF1: (data: ISearchF1DTO): Promise<IServerResp<IBankClient[]>> =>
    request({
      url: `${bankClientUrl}/search-f1`,
      method: 'POST',
      data,
    }).then(r => ({ data: r.data.data.data })),
};

const branchUrl = getBranchUrl(true);

const branch = {
  ...getDictionaryService<IBranch>(branchUrl),
  ...createClientBranchService(branchUrl),
};

export const dictionaryService = {
  okopf,
  okfs,
  currency,
  activityType,
  country,
  accountType,
  gk,
  clientOfficial,
  branch,
  branchV2,
  bankClient,
  documentType,
  fias,
};
