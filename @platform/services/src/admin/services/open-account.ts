import type { IOpenAccount, IServerResp, IExportResponse } from '../../client';
import { HEADERS_PARAM } from '../../constants/headers-param';
import { createOpenAccountService } from '../../services/helpers/open-request';
import { request, getFileName } from '../../utils';

export const OPEN_ACCOUNT_URL = '/api/opening-request-bank/request';
export const openAccountService = {
  ...createOpenAccountService(OPEN_ACCOUNT_URL),
  repeatSend: (id: string): Promise<IServerResp<IOpenAccount>> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/repeat-send`,
      method: 'POST',
      data: { data: id },
    }).then(resp => resp),
  reject: (id: string): Promise<IOpenAccount> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/reject`,
      method: 'POST',
      data: { data: id },
    }).then(r => r.data.data),
  exportWithSign: (id: string): Promise<IExportResponse> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/export-sign`,
      method: 'POST',
      data: { data: id },
    }).then(r => ({
      type: 'TXT',
      fileName: getFileName(r.headers[HEADERS_PARAM.CONTENT_DISPOSITION] || ''),
      data: r.data,
    })),
};
