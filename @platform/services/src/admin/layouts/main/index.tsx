import React, { useCallback, useEffect, useMemo } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import type { IButtonAction, IAppSidebarOptionProps } from '@platform/ui';
import { BUTTON } from '@platform/ui';
import { getFio, useAuth, useRedirect } from '../../..';
import { FatalErrorBoundary, FatalErrorContent } from '../../../components';
import { CommonLayout, Topline, Sidebar } from '../../../components/injectable';
import { useAppConfig } from '../../../core/app-config';
import type { IMenuItem } from '../../../interfaces';
import { locale } from '../../../localization';
import { getBaseUrl } from '../../../utils';
import { getSidebarOptions } from '../../../utils/get-sidebar-options';
import { ADMIN_STREAM_URL } from '../../constants';
import { useUser } from '../../user';

export interface ILayoutProps {
  actions?: IButtonAction[];
  disableReserveButton?: boolean;
  disableRegDocButton?: boolean;
  beforeLogout?(): Promise<boolean>;
  onClickExit?(): void;
}

const REG_DOC_AUTHORITIES = ['DOC_PACK.CREATE', 'DOC_PACK.UPDATE'];
const RESERVE_AUTHORITIES = ['DOC_PACK.CREATE', 'DOC_PACK.UPDATE'];

export const MainLayout: React.FC<ILayoutProps> = ({
  children,
  actions: actionsFromProps = [],
  disableReserveButton = false,
  disableRegDocButton = false,
  beforeLogout = () => Promise.resolve(true),
}) => {
  const { push } = useHistory();
  const { path } = useRouteMatch();
  const baseUrl = getBaseUrl(path);
  const { fatalErr, logout, hasAuthority, hasAllowedContextType, token, redirectToLogin } = useAuth();
  const { user } = useUser();
  const { config } = useAppConfig();
  const reserveAccount = useRedirect(`${ADMIN_STREAM_URL.RESERVE_ACCOUNT}/new`);
  const openFirstAccount = useRedirect(`${ADMIN_STREAM_URL.REGDOC}/new`);

  useEffect(() => {
    if (!token) {
      redirectToLogin();
    }
  }, [token, redirectToLogin]);

  const userName = useMemo(
    () =>
      user
        ? getFio({
            familyName: user.secondName,
            firstName: user.firstName,
            middleName: user.patronymic,
          })
        : '',
    [user]
  );

  const actions: IButtonAction[] = [];
  const canOpenFirstAccount = useMemo(() => hasAuthority(...REG_DOC_AUTHORITIES), [hasAuthority]);
  const canReserveAccount = useMemo(() => hasAuthority(...RESERVE_AUTHORITIES), [hasAuthority]);

  // вычисляем пункты меню
  const menuItems = useMemo(() => {
    if (!config) {
      return [];
    }

    const checkForAuthority = ({ allowedContexts, authorities = [], options }: IMenuItem): boolean => {
      if (typeof allowedContexts !== 'undefined' && !hasAllowedContextType(allowedContexts)) {
        return false;
      }

      // проверяем привилегии
      if (authorities.length > 0 && !hasAuthority(...authorities)) {
        return false;
      }

      // если у юзера есть привилегии и сегменты, то аналогичную проверку проводим в дочерних
      if (options) {
        return options.some(checkForAuthority);
      }

      // все проверки пройдены
      return true;
    };

    const recursiveFilter = (items: IMenuItem[] = []): IMenuItem[] =>
      items.filter(checkForAuthority).map(item => (item.options?.length ? { ...item, options: recursiveFilter(item.options) } : item));

    return getSidebarOptions(recursiveFilter(config.menu));
  }, [config, hasAuthority, hasAllowedContextType]);

  if (canReserveAccount) {
    actions.push({
      label: locale.action.reserveAccount,
      onClick: reserveAccount,
      disabled: disableReserveButton,
      name: 'reserve',
      buttonType: BUTTON.PRIMARY,
    });
  }

  if (canOpenFirstAccount) {
    actions.push({
      label: locale.action.regDoc,
      onClick: openFirstAccount,
      disabled: disableRegDocButton,
      name: 'recDoc',
      buttonType: BUTTON.PRIMARY,
    });
  }

  actions.push(...actionsFromProps);

  const onClickExit = useCallback(() => {
    void beforeLogout().then((yes: boolean) => {
      if (yes) {
        logout();
      }
    });
  }, [beforeLogout, logout]);

  const goToProfile = useCallback(() => {
    const { userId } = user;

    push(`${ADMIN_STREAM_URL.BANK_USERS}/${userId}`);
  }, [user, push]);

  const onClickOption = useCallback(
    ([url]: any[], _: IAppSidebarOptionProps) => {
      if (url === 'feedback') {
        return;
      }

      push(url);
    },
    [push]
  );

  if (fatalErr) {
    return <FatalErrorContent />;
  }

  return token && user.userId ? (
    <CommonLayout
      sidebar={<Sidebar options={menuItems} value={[baseUrl]} onClickOption={onClickOption} />}
      topline={
        <Topline
          actionLabel={locale.action.openAccount}
          actions={actions}
          useName={userName} /* TODO: Поменять название пропса */
          onClickExit={onClickExit}
          onClickUser={goToProfile}
        />
      }
    >
      <FatalErrorBoundary>{children}</FatalErrorBoundary>
    </CommonLayout>
  ) : null;
};
