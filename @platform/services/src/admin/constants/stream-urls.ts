import { COMMON_STREAM_URL } from '../../constants';

export const ADMIN_STREAM_URL = {
  ...COMMON_STREAM_URL,
  BANK_CLIENTS: '/security/clients',
  IMS: '/ims',
  CLIENT_USERS: '/security/client-users',
  TECHNICAL_USERS: '/security/technical-users',
  BANK_USERS: '/security/bank-users',
  PRIVILEGES: '/security/privileges',
  ROLES: '/security/role',
  SESSIONS: '/security/sessions',
  ACCREDITED_CENTER_KEYS: '/security/accredited-center-keys',
  ACCREDITED_CENTER: '/security/accredited-center',
  CERTIFICATES: '/security/certificates',
  CRL: '/security/crl',
  CERTIFICATES_REQUESTS: '/security/certificates-requests',
  ACCESS_RULES: '/security/access-rules',
  RKO_SERVICE_MANAGEMENT: '/rko-service-management',
  ACCESS_SEGMENTS: '/security/access-segments',
  BRANCH: '/security/branch',
};
