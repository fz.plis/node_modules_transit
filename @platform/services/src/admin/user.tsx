import React, { createContext, useContext, useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { LoaderOverlay } from '@platform/ui';
import { PLATFORM_QUERY_NAMES } from '../constants';
import { useAuth } from '../core/auth';
import { setCryptoPlugin, CRYPTO_PLUGIN_TYPE } from '../crypto';
import type { IUserData } from '../interfaces';
import { mainDomain } from './domains';
import { uaaService } from './services';

/**
 * Контекст банковского пользователя.
 */
interface IUserContext {
  /**
   * Пользователь.
   */
  user: IUserData;
  /**
   * Установить параметры пользователя.
   *
   * @param user Пользователь.
   */
  setUser(user: Partial<IUserData>): void;
  /**
   * Признак ошибки загрузки пользователя.
   */
  fatalErr: boolean;
}

let defaultContext = {
  fatalErr: false,
  setUser: (_: IUserData) => {},
  user: ({} as unknown) as IUserData,
};

const setUserData = (user: IUserData) => {
  defaultContext = {
    ...defaultContext,
    user,
  };

  return defaultContext;
};

export const getUserValue = () => defaultContext;

export const getUserItem = <K extends keyof Omit<IUserContext, 'setUser'>>(key: K): IUserContext[K] => getUserValue()[key];

const UserContext = createContext<IUserContext>(defaultContext);

export const UserProvider: React.FC = ({ children }) => {
  const { redirectToLogin, token, tokenString, config: authConfig, fatalErr: authFatalErr, authorities } = useAuth();
  const [user, setUser] = useState({});

  const { data: userInfo, isError: fatalErr, isLoading } = useQuery(PLATFORM_QUERY_NAMES.USER_INFO, uaaService.getUserInfo);

  if (!token || fatalErr) {
    redirectToLogin();
  }

  useEffect(() => {
    if (!token) {
      return;
    }

    mainDomain.methods.user.setToken(tokenString, authorities);

    if (!userInfo) {
      return;
    }

    const { uaaUserId, familyName, firstName, middleName, phoneNumber, login, email } = userInfo.data;

    setCryptoPlugin(CRYPTO_PLUGIN_TYPE.SFT);

    const userData = {
      firstName,
      phoneNumber,
      login,
      email,
      userId: uaaUserId,
      secondName: familyName,
      patronymic: middleName,
    };

    setUser(userData);

    mainDomain.methods.user.load({ userData, roles: token.roles });

    setUserData((userData as unknown) as IUserData);
  }, [token, authorities, tokenString, userInfo]);

  useEffect(() => {
    mainDomain.methods.authConfig.set(authConfig!);
  }, [authConfig]);

  useEffect(() => {
    mainDomain.methods.hasFatalError.set(authFatalErr);
  }, [authFatalErr]);

  return (
    <UserContext.Provider
      value={{
        fatalErr,
        setUser,
        user: (user as unknown) as IUserData,
      }}
    >
      <>
        {isLoading && <LoaderOverlay doNotUsePortal opened />}
        {Boolean((user as IUserData)?.userId) && children}
      </>
    </UserContext.Provider>
  );
};

UserProvider.displayName = 'UserProvider';

export const useUser = () => useContext(UserContext);
