import { domain, listValue, value } from '@platform/tools/istore';
import { CRYPTO_PLUGIN_TYPE } from '../../crypto';
import { setCryptoPlugin } from '../../crypto/olk';
import { authConfig, hasFatalError } from '../../domains/common-domain';
import { user as commonUser } from '../../domains/user';
import type { IBranch, IUserData, USER_ROLE } from '../../interfaces';
import { UserType } from '../../interfaces';

export { hasFatalError } from '../../domains/common-domain';

export const user = commonUser
  .withMethods(m => ({
    ...m,
    load: ({ userData, roles }: { userData: Partial<IUserData>; roles: USER_ROLE[] }) => {
      m.updateData(userData);
      m.setRoles(roles);
    },
  }))
  .withMethods(m => ({
    ...m,
    setToken(val: string, authorities: string[]) {
      m.setToken(val, authorities);
      m.setUserType(UserType.BANK);
    },
    setCryptoPlugin: () => setCryptoPlugin(CRYPTO_PLUGIN_TYPE.SFT),
  }));

user.mount();

export const userBranches = listValue<IBranch>([]).withName('main.user.userBranches').asPermanent();

export const selectedBranchId = value<string>('').withName('main.user.selectedBranchId').asPermanent();

/**
 * @deprecated C 1 февраля Используйте useUser, useAuth вместо.
 */
export const mainDomain = domain({
  user,
  userBranches,
  selectedBranchId,
  authConfig,
  hasFatalError,
});

mainDomain.mount('main');
