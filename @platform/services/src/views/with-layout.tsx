import React from 'react';
import { getSelectors, router } from '@platform/core';
import { domain } from '@platform/tools/istore';
import { FatalErrorBoundary } from '../components';
import { NON_SECURE_ROUTES } from '../constants';

export const withLayout = (Layout: React.ComponentType<any>, Content: React.ComponentType<any>) => {
  const pageDomain = domain({ router });

  const { asRoot, useDomain } = getSelectors('___', pageDomain);

  return asRoot(() => {
    const hasLayout = useDomain(s => !NON_SECURE_ROUTES.includes(s.router.location.pathname));

    if (hasLayout) {
      return (
        <FatalErrorBoundary>
          <Layout>
            <FatalErrorBoundary>
              <Content />
            </FatalErrorBoundary>
          </Layout>
        </FatalErrorBoundary>
      );
    }

    return (
      <FatalErrorBoundary>
        <Content />
      </FatalErrorBoundary>
    );
  });
};
