/**
 * Поля записи истории изменений.
 */
export const HISTORY_FIELDS = {
  CHANGE_ID: 'changedIn',
  STATUS: 'status',
  USER_TYPE: 'userType',
  USER_NAME: 'userName',
  COMMENT_FOR_CLIENT: 'commentForClient',
  COMMENT_FOR_BANK: 'commentForBank',
};
