import dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from '../../constants';
import type { BankHistoryData, IHistoryResponse } from '../../interfaces';

/**
 * Функция-преобразователь для данных сущности истории.
 *
 * @param data - Сущность истории.
 * @param getStatusLabel = Функция, возвращающая локализованный статус.
 */
export const converterData = (data: IHistoryResponse, getStatusLabel: (arg: string) => string): BankHistoryData => ({
  date: dayjs(data.changedIn).format(DATE_TIME_FORMAT),
  userType: data.userType,
  userName: data.userName,
  commentForClient: data.commentForClient || data.infoForClient,
  commentForBank: data.commentForBank || data.infoForBank,
  userId: data.userId,
  id: data.id,
  status: getStatusLabel(data.status),
});
