import React from 'react';
import { Typography, Gap, WithInfoTooltip } from '@platform/ui';

/** Компонент отображения блока информации в строке истории изменений. */
export const HistoryNode: React.FC<{
  value: string;
  dataField: string;
  hasTooltip?: boolean;
  tooltipText?: string;
  onClick?(): void;
}> = ({ value, dataField, hasTooltip, tooltipText, onClick }) => (
  <>
    <Gap.XS />
    {hasTooltip ? (
      <WithInfoTooltip text={tooltipText || value}>
        {ref => (
          <Typography.Text data-field={dataField} innerRef={ref} line="COLLAPSE" title={value} onClick={onClick}>
            {value}
          </Typography.Text>
        )}
      </WithInfoTooltip>
    ) : (
      <Typography.Text data-field={dataField} line="COLLAPSE" title={value} onClick={onClick}>
        {value}
      </Typography.Text>
    )}
    <Gap.XS />
  </>
);

HistoryNode.displayName = 'HistoryNode';
