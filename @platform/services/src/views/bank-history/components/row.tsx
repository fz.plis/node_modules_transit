import React from 'react';
import { Line } from '@platform/ui';
import { HISTORY_DIALOG_USER_TYPE, HISTORY_DIALOG_USER_TYPE_TOOLTIPS } from '../../../constants';
import type { BankHistoryData } from '../../../interfaces';
import { UserType } from '../../../interfaces';
import { formatToShortFio } from '../../../utils';
import { HISTORY_FIELDS } from '../constants';
import { HistoryGrid } from './history-grid';
import { HistoryNode } from './history-node';

/**
 * Пропы для записи истории изменений.
 */
export interface IHistoryRowProps {
  /**
   * Данные для отображения.
   */
  data: BankHistoryData;
  /**
   * Обработчик нажатия на имя юзера.
   */
  onUserNameClick?(): void;
}

export const HistoryRow: React.FC<IHistoryRowProps> = ({ data, onUserNameClick }) => {
  const userName = React.useMemo(() => {
    switch (data.userType) {
      case UserType.BANK:
        return data.userName;
      case UserType.CLIENT:
        return formatToShortFio(data.userName);
      default:
        return '-';
    }
  }, [data]);

  return (
    <>
      <HistoryGrid>
        <HistoryNode dataField={HISTORY_FIELDS.CHANGE_ID} value={data.date} />
        <HistoryNode dataField={HISTORY_FIELDS.STATUS} value={data.status} />
        <HistoryNode
          hasTooltip
          dataField={HISTORY_FIELDS.USER_TYPE}
          tooltipText={HISTORY_DIALOG_USER_TYPE_TOOLTIPS[data.userType!]}
          value={HISTORY_DIALOG_USER_TYPE[data.userType!]}
        />
        <HistoryNode hasTooltip dataField={HISTORY_FIELDS.USER_NAME} value={userName} onClick={onUserNameClick} />
        <HistoryNode hasTooltip dataField={HISTORY_FIELDS.COMMENT_FOR_CLIENT} value={data.commentForClient || '—'} />
        <HistoryNode hasTooltip dataField={HISTORY_FIELDS.COMMENT_FOR_BANK} value={data.commentForBank || '—'} />
      </HistoryGrid>
      <Line />
    </>
  );
};

HistoryRow.displayName = 'HistoryRow';
