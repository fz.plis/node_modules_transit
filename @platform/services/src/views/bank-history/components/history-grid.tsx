import React from 'react';
import { Pattern, ROLE } from '@platform/ui';

/** Фиксированный массив `ReactNode` элементов длины 6. */
type FixedChildrenArray = React.ReactNode[] & { 0: React.ReactNode; length: 6 };

interface IHistoryGridProps {
  children: FixedChildrenArray;
}

/**
 * Обёртка для компонентов отображения истории, поделит 6 потомков на равные
 * части через `Pattern.Span` размера 2, количество потомков гарантируется
 * типизацией.
 * Используется для гарантии корректного расположения элементов при
 * просмотре истории, количество элементов и размер колонок взяты из постановки
 * по ЭФ просмотра истории изменений.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=351894
 */
export const HistoryGrid: React.FC<IHistoryGridProps> = ({ children }) => (
  <Pattern gap="SM" role={ROLE.LISTITEM}>
    {React.Children.map(children, child => (
      <Pattern.Span size={2}>{child}</Pattern.Span>
    ))}
  </Pattern>
);

HistoryGrid.displayName = 'HistoryGrid';
