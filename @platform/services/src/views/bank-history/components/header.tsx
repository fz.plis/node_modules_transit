import React from 'react';
import { Typography } from '@platform/ui';
import { locale } from '../../../localization';
import { HistoryGrid } from './history-grid';

export const HistoryHeader: React.FC = () => (
  <HistoryGrid>
    <Typography.Text fill="FAINT">{locale.historyDialog.date}</Typography.Text>
    <Typography.Text fill="FAINT">{locale.historyDialog.status}</Typography.Text>
    <Typography.Text fill="FAINT">{locale.historyDialog.userType}</Typography.Text>
    <Typography.Text fill="FAINT">{locale.historyDialog.userName}</Typography.Text>
    <Typography.Text fill="FAINT">{locale.historyDialog.infoForClient}</Typography.Text>
    <Typography.Text fill="FAINT">{locale.historyDialog.infoForBank}</Typography.Text>
  </HistoryGrid>
);

HistoryHeader.displayName = 'HistoryHeader';
