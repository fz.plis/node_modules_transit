import React, { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import type { BankHistoryData } from 'src/admin';
import { to } from '@platform/core';
import { Box, Gap, Line, Typography, LayoutScroll, DialogTemplate, ROLE, PrimaryButton } from '@platform/ui';
import { ADMIN_STREAM_URL } from '../../admin/constants';
import { clientUser } from '../../admin/services';
import { showLoader, hideLoader } from '../../components';
import type { IHistoryResponse, BankClientName } from '../../interfaces';
import { UserType } from '../../interfaces';
import { locale } from '../../localization';
import { isClientScope } from '../../utils';
import { HistoryHeader, HistoryRow } from './components';
import { converterData } from './helpers';
import css from './style.scss';

export interface IBankHistoryDialogProps {
  /**
   * Коллекция записей истории изменений.
   */
  data: IHistoryResponse[];
  /**
   * Наименование клиента.
   */
  bankClient: BankClientName;
  /**
   * Заголовок.
   */
  header: string;
  /**
   * Обработчик нажатия на имя юзера.
   */
  onUserNameClick?(userId: string): void;
  /**
   * Обработчик закрытия модального окна.
   */
  onClose(): void;

  /**
   * Функция, возвращающая локализованный статус записи истории.
   *
   * @param arg - Запись истории.
   */
  getStatusLabel(arg: string): string;
}

export const BankHistoryDialog: React.FC<IBankHistoryDialogProps> = ({
  header,
  bankClient,
  getStatusLabel,
  data,
  onClose,
  onUserNameClick,
}) => {
  const { push } = useHistory();

  const onClick = useCallback(
    async (historyData: BankHistoryData) => {
      const { userId, userType } = historyData;

      // Из клиентской части не предусмотрен переход в профили других юзеров.
      if (userType === UserType.TECHNICAL || isClientScope()) {
        return;
      }

      if (onUserNameClick) {
        onUserNameClick(userId);
      }

      if (userType === UserType.BANK) {
        push(`${ADMIN_STREAM_URL.BANK_USERS}/${userId}`);
      } else if (userType === UserType.CLIENT) {
        showLoader();

        const [resp, err] = await to(clientUser.getByUaaUserId(userId));

        hideLoader();

        if (err) {
          return;
        }

        push(`${ADMIN_STREAM_URL.CLIENT_USERS}/${resp!.id}`);
      }

      onClose();
    },
    [onClose, onUserNameClick, push]
  );

  const historyList = useMemo(
    () =>
      data.map(historyRow => {
        const historyData = converterData(historyRow, getStatusLabel);

        return <HistoryRow key={historyRow.id} data={historyData} onUserNameClick={() => onClick(historyData)} />;
      }),
    [data, getStatusLabel, onClick]
  );

  const content = (
    <Box className={css.wrapper}>
      <Typography.H3>{header}</Typography.H3>
      {bankClient.clientName && (
        <>
          <Gap />
          <Typography.Text fill="FAINT">{locale.client.label}</Typography.Text>
          <Typography.PBold data-field="bankClient" line={'COLLAPSE'} title={bankClient.clientName}>
            {bankClient.clientName}
          </Typography.PBold>
        </>
      )}
      <Gap.LG />
      <HistoryHeader />
      <Gap.XS />
      <Line />
      <Gap.XS />
      <LayoutScroll className={css.scrollArea} role={ROLE.LISTBOX}>
        {historyList}
      </LayoutScroll>
    </Box>
  );

  return (
    <DialogTemplate
      content={content}
      footerAddon={
        <PrimaryButton dimension="SM" onClick={onClose}>
          {locale.action.close}
        </PrimaryButton>
      }
      header={''}
      onClose={onClose}
    />
  );
};

BankHistoryDialog.displayName = 'BankHistoryDialog';
