import React from 'react';
import type { IOption } from '@platform/ui';
import { Box, Horizon, Gap, Font } from '@platform/ui';
import type { IUniversalSignDetails, IUniversalSignEntity } from '../../interfaces';
import { CollapsedPanel } from './collapsed-panel';
import css from './styles.scss';

export const SignDetailsBlock: React.FC<IUniversalSignDetails> = ({ label, signFieldsViewList = [] }) => {
  const renderRow = (item: IOption) => (
    <>
      <Horizon>
        <Gap.XL />
        <Box className={css.detailRow}>
          <Font.Faint line="NOWRAP" volume="SM">
            {item.label}
          </Font.Faint>
          <Font.Base volume="SM">{item.value}</Font.Base>
        </Box>
      </Horizon>
      <Gap.XS />
    </>
  );

  return (
    <Horizon>
      <Gap.XL />
      <CollapsedPanel header={label}>{signFieldsViewList.map(renderRow)}</CollapsedPanel>
    </Horizon>
  );
};

SignDetailsBlock.displayName = 'SignDetailsBlock';

export interface ISignDetailsProps {
  documents: IUniversalSignEntity[];
}

export const SignDetails: React.FC<ISignDetailsProps> = ({ documents }) => (
  <Box>
    {documents.map(({ signDataList }) =>
      signDataList?.map(({ label, signDataViewList }) => (
        <>
          <CollapsedPanel header={label}>{signDataViewList?.map(SignDetailsBlock)}</CollapsedPanel>
          <Gap.XS />
        </>
      ))
    )}
  </Box>
);

SignDetails.displayName = 'SignDetails';
