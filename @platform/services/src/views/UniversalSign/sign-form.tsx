import React from 'react';
import { Typography, BUTTON, DialogTemplate, Gap, Box, Font } from '@platform/ui';
import type { IUserCertificate, IUniversalSignEntity } from '../../interfaces';
import { locale } from '../../localization';
import { CertificateList } from './certificate-list';
import { SignDetails } from './sign-details';
import css from './styles.scss';

export interface IUniversalSignDocumentsProps {
  documents: IUniversalSignEntity[];
}

export interface IUniversalSignDocumentProps {
  document: IUniversalSignEntity;
}

type RenderSummary = (props: IUniversalSignDocumentsProps) => React.ReactNode;
type RenderDocument = (props: IUniversalSignDocumentProps) => React.ReactNode;

export interface IUniversalSignProps extends IUniversalSignDocumentsProps {
  certs: IUserCertificate[];
  renderSummary?: RenderSummary;
  renderDocument: RenderDocument;
  onClose(): void;
  onSignClick(thumbprint: string, cert: IUserCertificate): void;
  selectedCertificate?: IUserCertificate;
}

interface ISignState {
  currentCertificate: IUserCertificate | null;
  userCertificates: IUserCertificate[];
}

const Summary: React.FC<IUniversalSignDocumentsProps> = ({ documents }) => (
  <Font.Faint volume="MD">
    {locale.signDialog.summary}: {documents.length}
  </Font.Faint>
);

Summary.displayName = 'Summary';

export class UniversalSign extends React.Component<IUniversalSignProps, ISignState> {
  static defaultProps: any = {
    certs: [],
    renderSummary: (props: IUniversalSignDocumentsProps) => <Summary {...props} />,
  };

  constructor(props: IUniversalSignProps) {
    super(props);

    const { selectedCertificate, certs } = props;
    let currentCertificate: IUserCertificate | null;
    let userCertificates: IUserCertificate[];

    if (selectedCertificate) {
      currentCertificate = selectedCertificate;
    } else {
      currentCertificate = certs.length === 1 ? certs[0] : null;
    }

    if (currentCertificate) {
      const { id } = currentCertificate;

      userCertificates = [currentCertificate, ...certs.filter(cert => cert.id !== id)];
    } else {
      userCertificates = certs;
    }

    this.state = {
      currentCertificate,
      userCertificates,
    };
  }

  renderContent() {
    const { documents, renderSummary } = this.props;
    const { currentCertificate, userCertificates } = this.state;

    return (
      <Box className={css.signBox}>
        <Typography.PBold>{locale.menuService.certificates}</Typography.PBold>
        <Gap.SM />
        <CertificateList options={userCertificates} selectedOption={currentCertificate} onSelectOption={this.onSelectCertificate} />

        <Typography.PBold>{locale.signDialog.detailedInfo}</Typography.PBold>
        <Gap.SM />
        <Box>{renderSummary!({ documents })}</Box>

        <Gap.SM />
        <SignDetails documents={documents} />
      </Box>
    );
  }

  private handleSignClick = () => {
    const { onSignClick, onClose } = this.props;

    // TODO: пока нет бекенда используем заглушки
    onSignClick('', this.state.currentCertificate!);
    onClose();
  };
  private handleCancel = () => this.props.onClose();

  private onSelectCertificate = (certificate: IUserCertificate) => {
    this.setState({
      currentCertificate: certificate,
    });
  };

  render() {
    const { onClose } = this.props;
    const { currentCertificate } = this.state;

    const dialogActions = [
      {
        name: 'sign',
        label: locale.action.sign,
        onClick: this.handleSignClick,
        buttonType: BUTTON.PRIMARY,
        disabled: !currentCertificate,
      },
      {
        name: 'cancel',
        label: locale.action.cancel,
        onClick: this.handleCancel,
      },
    ];

    return <DialogTemplate actions={dialogActions} content={this.renderContent()} header={locale.dialog.sign.header} onClose={onClose} />;
  }
}
