import React from 'react';
import { Box, Horizon, Gap, ServiceIcons, useToggle } from '@platform/ui';

export interface ICollapsedPanelProps extends React.AllHTMLAttributes<HTMLDivElement> {
  header?: React.ReactNode;
  expand?: boolean;
}

export const CollapsedPanel: React.FC<ICollapsedPanelProps> = ({ header, expand, children, ...restProps }) => {
  const [expanded, toggleExpanded] = useToggle(expand);
  const Arrow = expanded ? ServiceIcons.ChevronDown : ServiceIcons.ChevronRight;

  return (
    <Box {...restProps}>
      <Box clickable onClick={toggleExpanded}>
        <Horizon>
          <Arrow fill={'ACCENT'} scale={'SM'} />
          <Gap.XS />
          {header}
        </Horizon>
      </Box>

      {expanded && (
        <>
          <Gap.XS />
          {children}
        </>
      )}
      <Gap.XS />
    </Box>
  );
};

CollapsedPanel.displayName = 'CollapsedPanel';
