import React from 'react';
import { Box, LayoutScroll, Gap, RadioGroup, Typography } from '@platform/ui';
import type { IUserCertificate } from '../../interfaces';

export interface IUserListProps {
  options?: IUserCertificate[];
  selectedOption: IUserCertificate | null;
  onSelectOption?(option: IUserCertificate): void;
}

export const CertificateList: React.FC<IUserListProps> = props => {
  const { options = [], selectedOption, onSelectOption } = props;

  return (
    <>
      <LayoutScroll autoHeight autoHeightMax={240} autoHide={false} hideTracksWhenNotNeeded={false}>
        <RadioGroup
          name={'certificateList'}
          options={options.map(option => ({
            label: (
              <Box>
                <Typography.P>{option.subject || '-'}</Typography.P>
                <Gap.XS />
                <Typography.Text fill={'FAINT'}>{option.issuedBy}</Typography.Text>
              </Box>
            ),
            value: option,
          }))}
          value={selectedOption}
          onChange={onSelectOption}
        />
      </LayoutScroll>
      <Gap.XL />
    </>
  );
};

CertificateList.displayName = 'CertificateList';
