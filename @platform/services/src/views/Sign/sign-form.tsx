import React from 'react';
import { Typography, BUTTON, DialogTemplate, Gap, Box, Font, Pagination } from '@platform/ui';
import { CertificateList } from '../../components/certificate-list';
import type { IUserCertificate } from '../../interfaces';
import { locale } from '../../localization';
import type { IWithDetailProps } from './shared';
import { DetailsLink, withDetails } from './shared';
import css from './styles.scss';

export interface ISignDocumentsProps<T> {
  documents: T[];
}

export interface ISignDocumentProps<T> {
  document: T;
}

export type RenderSummary<T> = (props: ISignDocumentsProps<T>) => React.ReactNode;
export type RenderDocument<T> = (props: ISignDocumentProps<T>) => React.ReactNode;

export interface ISignProps<T> extends ISignDocumentsProps<T>, IWithDetailProps {
  certs: IUserCertificate[];
  renderSummary?: RenderSummary<T>;
  renderDocument: RenderDocument<T>;
  onClose(): void;
  onSignClick(thumbprint: string, cert: IUserCertificate): void;
  selectedCertificate: IUserCertificate;
}

export interface ISignState {
  docIndex: number;
  currentCertificate: IUserCertificate | null;
  userCertificates: IUserCertificate[];
}

export const Summary: React.FC<ISignDocumentsProps<any>> = ({ documents }) => (
  <Font.Faint volume="MD">Выбрано документов: {documents.length}</Font.Faint>
);

Summary.displayName = 'Summary';

export const Sign = withDetails<ISignProps<any>>()(
  class SignComponent extends React.Component<ISignProps<any>, ISignState> {
    static defaultProps: any = {
      certs: [],
      renderSummary: (props: ISignDocumentsProps<any>) => <Summary {...props} />,
    };

    constructor(props: ISignProps<any>) {
      super(props);

      const { selectedCertificate, certs } = props;
      let currentCertificate: IUserCertificate | null;
      let userCertificates: IUserCertificate[];

      if (selectedCertificate) {
        currentCertificate = selectedCertificate;
      } else {
        currentCertificate = certs.length === 1 ? certs[0] : null;
      }

      if (currentCertificate) {
        const { id } = currentCertificate;

        userCertificates = [currentCertificate, ...certs.filter(cert => cert.id !== id)];
      } else {
        userCertificates = certs;
      }

      this.state = {
        currentCertificate,
        userCertificates,
        docIndex: 0,
      };
    }

    renderContent() {
      const { documents, renderSummary, renderDocument, onShowDetailsChange, showDetails } = this.props;
      const { docIndex, currentCertificate, userCertificates } = this.state;

      return (
        <Box className={css.signBox}>
          <Box>{renderSummary!({ documents })}</Box>
          {userCertificates.length > 1 && (
            <>
              <Gap.XL />
              <Typography.P fill="FAINT">{locale.forms.signForm.selectSigner}</Typography.P>
              <Gap.XS />
              <CertificateList options={userCertificates} selectedOption={currentCertificate} onSelectOption={this.onSelectCertificate} />
            </>
          )}
          <Gap.XL />
          <DetailsLink showDetails={showDetails} onShowDetailsChange={onShowDetailsChange} />
          <Gap.XL />
          {showDetails && <Box>{renderDocument({ document: documents[docIndex] })}</Box>}
        </Box>
      );
    }

    renderPagination() {
      const { docIndex } = this.state;
      const { documents, showDetails } = this.props;

      if (!showDetails || documents.length === 1) {
        return null;
      }

      return <Pagination page={docIndex + 1} totalPages={documents.length} onPageChange={this.setPage} />;
    }

    private handleSignClick = () => {
      const { onSignClick, onClose } = this.props;

      // TODO: пока нет бекенда используем заглушки
      onSignClick('', this.state.currentCertificate!);
      onClose();
    };
    private handleCancel = () => this.props.onClose();
    private setPage = (page: number) => this.setState({ docIndex: page - 1 });
    private onSelectCertificate = (certificate: IUserCertificate) => {
      this.setState({
        currentCertificate: certificate,
      });
    };

    render() {
      const { onClose } = this.props;
      const { currentCertificate } = this.state;

      const dialogActions = [
        {
          name: 'sign',
          label: locale.action.sign,
          onClick: this.handleSignClick,
          buttonType: BUTTON.PRIMARY,
          disabled: !currentCertificate,
        },
        {
          name: 'cancel',
          label: locale.action.cancel,
          onClick: this.handleCancel,
        },
      ];

      return (
        <DialogTemplate
          actions={dialogActions}
          content={this.renderContent()}
          footerAddon={this.renderPagination()}
          header={locale.dialog.sign.header}
          onClose={onClose}
        />
      );
    }
  }
);
