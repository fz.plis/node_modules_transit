import React from 'react';
import type { ILinkProps } from '@platform/ui';
import { Link, Font, Pattern, Gap } from '@platform/ui';
import { locale } from '../../localization';

export interface IWithDetailProps {
  showDetails: boolean;
  onShowDetailsChange(val: boolean): void;
}

export type IDetailsLinkProps = IWithDetailProps & Omit<ILinkProps, 'onClick'>;

export interface IDetailRowProps {
  label: string;
  value?: number | string;
  dataField?: string;
}

export class DetailsLink extends React.Component<IDetailsLinkProps> {
  private toggle = () => {
    const { showDetails, onShowDetailsChange } = this.props;

    onShowDetailsChange(!showDetails);
  };

  render() {
    const { showDetails, onShowDetailsChange, ...props } = this.props;

    const linkText = showDetails ? locale.action.details.hide : locale.action.details.show;

    return (
      <Link volume="MD" {...props} onClick={this.toggle}>
        {linkText}
      </Link>
    );
  }
}

export const SubHeader: React.FC = ({ children }) => (
  <Pattern>
    <Pattern.Span size={12}>
      <Font volume="SM" weight="BOLD">
        {children}
      </Font>
    </Pattern.Span>
  </Pattern>
);

SubHeader.displayName = 'SubHeader';

const EMPTY_VALUE = '-';

export const DetailRow: React.FC<IDetailRowProps> = ({ label, value, dataField }) => (
  <>
    <Gap />
    <Pattern>
      <Pattern.Span size={6}>
        <Font.Faint volume="SM">{label}</Font.Faint>
      </Pattern.Span>
      <Pattern.Span size={6}>
        <Font data-field={dataField} line="COLLAPSE" title={(value || EMPTY_VALUE).toString()} volume="SM">
          {value || EMPTY_VALUE}
        </Font>
      </Pattern.Span>
    </Pattern>
  </>
);

DetailRow.displayName = 'DetailRow';

export const DetailMultiRow: React.FC<IDetailRowProps> = ({ label, value, dataField }) => (
  <>
    <Gap />
    <Pattern>
      <Pattern.Span size={12}>
        <Font.Faint volume="SM">{label}</Font.Faint>
        <Font data-field={dataField} line="COLLAPSE" title={(value || EMPTY_VALUE).toString()} volume="SM">
          {value || EMPTY_VALUE}
        </Font>
      </Pattern.Span>
    </Pattern>
  </>
);

DetailRow.displayName = 'DetailMultiRow';

export const withDetails = <T extends IWithDetailProps>(
  showDetails: boolean = false
): ((WrappedComponent: React.ComponentType<T>) => any) => (WrappedComponent: React.ComponentType<T>) =>
  // eslint-disable-next-line react/no-multi-comp
  class WithDetails extends React.Component<Omit<T, 'onShowDetailsChange' | 'showDetails'>> {
    state = {
      showDetails,
    };

    toggle = (value: boolean) => this.setState({ showDetails: value });

    render() {
      return <WrappedComponent {...(this.props as T)} {...this.state} onShowDetailsChange={this.toggle} />;
    }
  };
