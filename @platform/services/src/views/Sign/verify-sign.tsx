/* eslint-disable react/no-multi-comp */
import React from 'react';
import dayjs from 'dayjs';
import type { IButtonAction } from '@platform/ui';
import {
  DialogTemplate,
  Typography,
  SpecialIcons,
  LayoutScroll,
  DOCUMENT_STATUS_TYPE,
  Horizon,
  Status,
  Gap,
  Box,
  Font,
} from '@platform/ui';
import { DATE_TIME_FORMAT, DATE_FORMAT } from '../../constants';
import type { IGPBCryptoModuleIssuer } from '../../crypto';
import type { ISignVerify, IVerifySignError } from '../../interfaces';
import { locale } from '../../localization';
import type { IWithDetailProps } from './shared';
import { DetailsLink, DetailMultiRow, withDetails } from './shared';
import * as css from './styles.scss';

export interface IVersifySignResult<T> extends ISignVerify {
  valid: boolean;
  document: T;
  fio?: string;
  issuer?: IGPBCryptoModuleIssuer;
  serialNumber?: string;
  validFrom?: string;
  validTo?: string;
  error?: IVerifySignError;
}

export interface IDocNameRenderProps<T> {
  docNameGetter(doc: T): string;
}

export interface IVerifySignProps<T> extends IDocNameRenderProps<T> {
  data: Array<IVersifySignResult<T>>;
  onClose(): void;
}

export interface IVerifySignSingleProps<T> extends IDocNameRenderProps<T>, IWithDetailProps {
  data: IVersifySignResult<T>;
}

export interface IVerifySignMultiProps<T> extends IDocNameRenderProps<T> {
  data: Array<IVersifySignResult<T>>;
}

export interface IVerifySignSingleState {
  showDetails: boolean;
}

export const validLocale = (valid: boolean) => (valid ? locale.verifySign.valid : locale.verifySign.invalid);

export interface IResultBlockProps<T> extends IDocNameRenderProps<T>, IWithDetailProps {
  valid?: boolean;
  data: Array<IVersifySignResult<T>>;
}

export const ResultBlock = withDetails<IResultBlockProps<any>>()(
  // eslint-disable-next-line react/no-multi-comp
  class ResultBlock extends React.Component<IResultBlockProps<any>> {
    render() {
      const { valid, data, showDetails, onShowDetailsChange, docNameGetter } = this.props;
      const status = valid ? DOCUMENT_STATUS_TYPE.SUCCESS : DOCUMENT_STATUS_TYPE.ERROR;
      const text = validLocale(!!valid);

      return (
        <Box>
          <Horizon>
            <Status type={status} />
            <Gap />
            <Font.Faint volume="SM">{text}</Font.Faint>
            <Gap />
            <Font volume="SM" weight="BOLD">
              {data.length}
            </Font>
            <Gap />
            {data.length > 0 && <DetailsLink showDetails={showDetails} onShowDetailsChange={onShowDetailsChange} />}
          </Horizon>
          {showDetails && (
            <>
              <Gap.XS />
              <LayoutScroll autoHeight autoHeightMax={200}>
                <Font.Faint volume="SM">
                  <ul>
                    {data.map(x => (
                      <li key={x.document.id}>{docNameGetter(x.document)}</li>
                    ))}
                  </ul>
                </Font.Faint>
              </LayoutScroll>
            </>
          )}
        </Box>
      );
    }
  }
);

export const VerifySignSingle = withDetails<IVerifySignSingleProps<any>>()(
  // eslint-disable-next-line react/no-multi-comp
  class VerifySignSingle extends React.Component<IVerifySignSingleProps<any>> {
    renderInfo() {
      const { data, docNameGetter } = this.props;
      const { document, signature, fio, serialNumber, validFrom, validTo, issuer, error } = data;
      const docName = docNameGetter(document);

      return (
        <LayoutScroll autoHeight autoHeightMax={300}>
          {error && (
            <>
              <Font volume="MD" weight="BOLD">
                {`${locale.verifySign.error.code} ${error.code} — ${error.description}`}
              </Font>
              <Gap.LG />
            </>
          )}
          <Typography.PLead>{docName}</Typography.PLead>
          <Gap.LG />
          <Font volume="MD" weight="BOLD">
            {locale.verifySign.header}
          </Font>
          <DetailMultiRow label={locale.verifySign.fields.signature.signDate} value={dayjs(signature.signedAt).format(DATE_TIME_FORMAT)} />
          <DetailMultiRow label={locale.verifySign.fields.signature.certificate} value={serialNumber} />
          <DetailMultiRow label={locale.verifySign.fields.signature.nameOfSignatory} value={fio} />
          <DetailMultiRow
            label={locale.verifySign.fields.signature.validPeriod}
            value={`${dayjs(validFrom).format(DATE_FORMAT)} — ${dayjs(validTo).format(DATE_FORMAT)}`}
          />
          {issuer && <DetailMultiRow label={locale.verifySign.fields.signature.issuerName} value={issuer.commonName} />}
        </LayoutScroll>
      );
    }

    render() {
      const { data, showDetails, onShowDetailsChange } = this.props;
      const bodyText = validLocale(data.valid);

      return (
        <Box className={css.verifySignBox}>
          <Font volume="MD">{bodyText}</Font>
          <Gap.LG />
          <Box>
            <DetailsLink showDetails={showDetails} onShowDetailsChange={onShowDetailsChange} />
          </Box>
          <Gap.SM />
          {showDetails && <Box>{this.renderInfo()}</Box>}
        </Box>
      );
    }
  }
);

// eslint-disable-next-line react/no-multi-comp
export class VerifySignMulti extends React.Component<IVerifySignMultiProps<any>> {
  render() {
    const { data, docNameGetter } = this.props;
    const { valid, inValid } = data.reduce(
      (result: any, x) => {
        const target = x.valid ? result.valid : result.inValid;

        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        target.push(x);

        return result;
      },
      { valid: [], inValid: [] }
    );

    return (
      <Box>
        <Font volume="SM">
          {locale.verifySign.verifiedDocs} {data.length}
        </Font>
        <ResultBlock valid data={valid} docNameGetter={docNameGetter} />
        <ResultBlock data={inValid} docNameGetter={docNameGetter} />
      </Box>
    );
  }
}

export class VerifySign<T> extends React.Component<IVerifySignProps<T>> {
  constructor(props: IVerifySignProps<T>) {
    super(props);

    this.dialogActions = [
      {
        name: 'close',
        label: locale.action.close,
        onClick: this.handleCancel,
      },
    ];
  }

  private dialogActions: IButtonAction[] = [];

  renderIcon() {
    const { data } = this.props;

    if (data.length > 1) {
      return;
    }

    const [first] = data;

    return first.valid ? SpecialIcons.Success : SpecialIcons.Warning;
  }

  renderContent() {
    const { data, docNameGetter } = this.props;

    if (this.isSingle()) {
      return <VerifySignSingle data={data[0]} docNameGetter={docNameGetter} />;
    }

    return <VerifySignMulti data={data} docNameGetter={docNameGetter} />;
  }

  private handleCancel = () => this.props.onClose();
  private isSingle = () => this.props.data.length === 1;

  render() {
    const { onClose } = this.props;

    return (
      <DialogTemplate
        actions={this.dialogActions}
        content={this.renderContent()}
        header={locale.dialog.verifySign.header}
        icon={this.renderIcon()}
        onClose={onClose}
      />
    );
  }
}
