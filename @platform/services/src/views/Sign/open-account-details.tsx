import React from 'react';
import { pathGenerator } from '@platform/core';
import { formatMoney } from '@platform/tools/big-number';
import { formatDate } from '@platform/tools/date-time';
import { formatAccountCode } from '@platform/tools/localization';
import { Box, Font, Pattern, Gap, LayoutScroll } from '@platform/ui';
import type { IOpenAccount, IOpenAccountInfo } from '../../interfaces';
import { STATEMENT_TYPE, ACCOUNT_TYPE } from '../../interfaces';
import { locale } from '../../localization';
import { SubHeader, DetailRow } from './shared';

const acc = pathGenerator<IOpenAccount>();
const accInfo = acc.carry('accountInfos', 0);

export interface IOpenAccountDetailsProps {
  document: IOpenAccount;
}

export interface IDetailRowProps {
  label: string;
  value?: number | string;
}

const formatBool = (val: boolean) => (val ? locale.action.yes : locale.action.no);
const formatStatementType = (statement: STATEMENT_TYPE) =>
  statement === STATEMENT_TYPE.STANDARD ? locale.statementType.standart : locale.statementType.extended;

export class OpenAccountDetails extends React.Component<IOpenAccountDetailsProps> {
  renderHeader() {
    const { document } = this.props;

    return (
      <Font volume="MD" weight="REGULAR">
        {locale.openAccount.docName({ docNumber: document.number, docDate: document.date })}
      </Font>
    );
  }

  renderDocInfo() {
    const { document } = this.props;

    return (
      <>
        <SubHeader>{locale.openAccount.main}</SubHeader>
        <DetailRow dataField={acc('number')} label={locale.openAccount.fields.docNumber} value={document.number} />
        <DetailRow dataField={acc('date')} label={locale.openAccount.fields.docDate} value={formatDate(document.date)} />
        <DetailRow
          dataField={acc('confirmNoChanges')}
          label={locale.openAccount.fields.confirmNoChanges}
          value={formatBool(document.confirmNoChanges)}
        />
        <DetailRow
          dataField={acc('bankClient', 'resident')}
          label={locale.openAccount.fields.isResident}
          value={formatBool(document.bankClient!.resident)}
        />
        <DetailRow dataField={acc('bankClient', 'innKio')} label={locale.openAccount.fields.innKio} value={document.bankClient!.innKio} />
        <DetailRow dataField={acc('bankClient', 'kpp')} label={locale.openAccount.fields.kpp} value={document.bankClient!.kpp} />
        <DetailRow dataField={acc('bankClient', 'ogrn')} label={locale.openAccount.fields.ogrn} value={document.bankClient!.ogrn} />
        <DetailRow dataField={acc('bankClient', 'name')} label={locale.openAccount.fields.client.name} value={document.bankClient!.name} />
        <DetailRow
          dataField={acc('contract', 'number')}
          label={locale.openAccount.fields.contract.contractNumber}
          value={document.contract.number}
        />
        <DetailRow
          dataField={acc('contract', 'date')}
          label={locale.openAccount.fields.contract.date}
          value={formatDate(document.contract.date)}
        />
        <DetailRow
          dataField={acc('opopAccount', 'number')}
          label={locale.openAccount.fields.contract.accountNumber}
          value={formatAccountCode(document.opopAccount.number)}
        />
        <DetailRow
          dataField={accInfo('needConnectMobileBank')}
          label={locale.openAccount.fields.onlineBanking}
          value={formatBool(document.accountInfos[0].needConnectMobileBank)}
        />
        {document.accountInfos && document.accountInfos[0].accountTypeCode !== ACCOUNT_TYPE.CHECKING && (
          <DetailRow
            dataField={acc('isRulesJoined')}
            label={locale.openAccount.fields.gozAgree}
            value={formatBool(document.isRulesJoined)}
          />
        )}
        {document.contract && !!document.contract.isCommissionDebitingRequired && (
          <DetailRow
            dataField={acc('commissionAccount', 'number')}
            label={locale.openAccount.fields.commission.accountNumber}
            value={formatAccountCode(document.commissionAccount ? document.commissionAccount.number : '')}
          />
        )}
      </>
    );
  }

  renderReserved() {
    const { document } = this.props;
    const { accountInfos, branch } = document;

    if (accountInfos.length === 0 || accountInfos[0].reservedAccounts.length === 0) {
      return null;
    }

    return (
      <>
        <SubHeader>{locale.openAccount.reserved}</SubHeader>
        {accountInfos.map(a => this.renderReserveItem(a, branch))}
      </>
    );
  }

  renderReserveItem(info: IOpenAccountInfo, branch: IOpenAccount['branch']) {
    return (
      <React.Fragment key={info.reservedAccounts[0].number}>
        <Gap />
        <Pattern>
          <Pattern.Span size={12}>
            <Font volume="SM" weight="BOLD">
              {locale.account({ accountNum: info.reservedAccounts[0].number })}
            </Font>
          </Pattern.Span>
        </Pattern>
        <DetailRow
          dataField={accInfo('reservedAccounts', 0, 'number')}
          label={locale.openAccount.fields.accountInfo.accountNumber}
          value={formatAccountCode(info.reservedAccounts[0].number)}
        />
        <DetailRow
          dataField={accInfo('accountTypeDescription')}
          label={locale.openAccount.fields.accountInfo.accountType}
          value={info.accountTypeDescription}
        />
        <DetailRow dataField={acc('branch', 'name')} label={locale.openAccount.fields.accountInfo.branch} value={branch?.name} />
        <DetailRow dataField={acc('branch', 'bic')} label={locale.openAccount.fields.branchBic} value={branch?.bic} />
        <DetailRow
          dataField={acc('branch', 'correspondentAccount')}
          label={locale.openAccount.fields.korAcc}
          value={branch?.correspondentAccount}
        />
        <DetailRow
          dataField={accInfo('statementType')}
          label={locale.openAccount.fields.accountInfo.statementType}
          value={info.statementType && formatStatementType(info.statementType)}
        />
        <DetailRow
          dataField={accInfo('govContractInfo', 'contractId')}
          label={locale.openAccount.fields.accountInfo.govContract.identify}
          value={info.govContractInfo?.contractId}
        />
        <DetailRow
          dataField={accInfo('govContractInfo', 'number')}
          label={locale.openAccount.fields.accountInfo.govContract.number}
          value={info.govContractInfo && info.govContractInfo.number!}
        />
        <DetailRow
          dataField={accInfo('govContractInfo', 'date')}
          label={locale.openAccount.fields.accountInfo.govContract.date}
          value={info.govContractInfo && formatDate(info.govContractInfo.date!)}
        />
        <DetailRow
          dataField={accInfo('govContractInfo', 'amount')}
          label={locale.openAccount.fields.accountInfo.amount}
          value={info.govContractInfo?.amount && formatMoney(info.govContractInfo.amount)}
        />
        <DetailRow
          dataField={accInfo('govContractCustomerInfo', 'inn')}
          label={locale.openAccount.fields.accountInfo.agentInn}
          value={info.govContractCustomerInfo?.inn}
        />
        <DetailRow
          dataField={accInfo('govContractCustomerInfo', 'accountNumber')}
          label={locale.openAccount.fields.custumerAccountNumber}
          value={info.govContractCustomerInfo?.accountNumber}
        />
        <DetailRow
          dataField={accInfo('govContractCustomerInfo', 'name')}
          label={locale.openAccount.fields.accountInfo.agentName}
          value={info.govContractCustomerInfo?.name}
        />
      </React.Fragment>
    );
  }

  renderNewAccounts() {
    const { document } = this.props;
    const { accountInfos } = document;

    if (!accountInfos || accountInfos.length === 0 || accountInfos[0].openingAccountsInfos.length === 0) {
      return null;
    }

    return (
      <>
        <SubHeader>{locale.openAccount.newAccounts}</SubHeader>
        {accountInfos.map(this.renderNewAccount)}
      </>
    );
  }

  renderNewAccount(info: IOpenAccountInfo, index: number) {
    return (
      <React.Fragment key={`new_account_${index}`}>
        <DetailRow
          dataField={accInfo('openingAccountsInfos', 0, 'currencyCode')}
          label={locale.openAccount.fields.newAccountsInfo.info.currency.name}
          value={info.openingAccountsInfos[0].currencyCode}
        />
        <DetailRow
          dataField={accInfo('openingAccountsInfos', 0, 'currencyNumericCode')}
          label={locale.openAccount.fields.newAccountsInfo.info.currency.code}
          value={info.openingAccountsInfos[0].currencyNumericCode}
        />
        <DetailRow
          dataField={accInfo('openingAccountsInfos', 0, 'count')}
          label={locale.openAccount.fields.newAccountsInfo.info.count}
          value={info.openingAccountsInfos[0].count}
        />
      </React.Fragment>
    );
  }

  render() {
    return (
      <Box>
        {this.renderHeader()}
        <LayoutScroll autoHeight autoHeightMax={300}>
          <Gap />
          {this.renderDocInfo()}
          <Gap />
          {this.renderReserved()}
          <Gap />
          {this.renderNewAccounts()}
        </LayoutScroll>
      </Box>
    );
  }
}
