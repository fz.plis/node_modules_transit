import React from 'react';
import { Adjust, Box, Gap, Horizon, LayoutScroll, Link, PrimaryButton, SpecialIcons, Typography } from '@platform/ui';
import { FEEDBACK_EMAIL, FEEDBACK_PHONE } from '../../constants';
import { locale } from '../../localization';
import { Assets } from './assets';
import css from './styles.scss';

/** Bullet-список с синей точкой, ожидает массив компонентов в качестве `children`. */
const BulletList: React.FC = ({ children }) => (
  <ul className={css.blueBulletDots}>
    {React.Children.map(children, (child, idx) => (
      <>
        {idx > 0 && <Gap.SM />}
        <li>{child}</li>
      </>
    ))}
  </ul>
);

BulletList.displayName = 'BulletList';

/** Список доступных функций на главной странице. */
const MainPageFeatures: React.FC = () => (
  <>
    <Typography.PBold>{locale.dboInformationDialog.mainPageFeatures.header}</Typography.PBold>
    <Gap />
    <BulletList>
      <Typography.Text>{locale.dboInformationDialog.mainPageFeatures.feature1}</Typography.Text>
      <Typography.Text>{locale.dboInformationDialog.mainPageFeatures.feature2}</Typography.Text>
      <Typography.Text>{locale.dboInformationDialog.mainPageFeatures.feature3}</Typography.Text>
    </BulletList>
  </>
);

MainPageFeatures.displayName = 'MainPageFeatures';

/** Список новых функций ГБО. */
const GboFeatures: React.FC = () => (
  <>
    <Typography.PBold>{locale.dboInformationDialog.gboFeatures.header}</Typography.PBold>
    <Gap />
    <BulletList>
      <Typography.Text>{locale.dboInformationDialog.gboFeatures.feature1}</Typography.Text>
      <Typography.Text>{locale.dboInformationDialog.gboFeatures.feature2}</Typography.Text>
      <Typography.Text>{locale.dboInformationDialog.gboFeatures.feature3}</Typography.Text>
      <Typography.Text>{locale.dboInformationDialog.gboFeatures.feature4}</Typography.Text>
      <Typography.Text>{locale.dboInformationDialog.gboFeatures.feature5}</Typography.Text>
      <Typography.Text>{locale.dboInformationDialog.gboFeatures.feature6}</Typography.Text>
    </BulletList>
  </>
);

GboFeatures.displayName = 'GboFeatures';

/** Блок с информацией про обратную связь. */
const Feedback: React.FC = () => (
  <>
    <Typography.P align="CENTER">{locale.dboInformationDialog.feedback.header}</Typography.P>
    <Gap.LG />
    <Typography.P align="CENTER">
      <Typography.PBold inline>{locale.dboInformationDialog.feedback.part1Bold}</Typography.PBold>
      <Gap.X2S inline />
      {locale.dboInformationDialog.feedback.part2}
      <Gap.X2S inline />
      <Link href={`mailto:${FEEDBACK_EMAIL}`} volume="MD">
        {FEEDBACK_EMAIL}
      </Link>
      <Gap.X2S inline />
      {locale.dboInformationDialog.feedback.part3}
      <Gap.X2S inline />
      <Typography.PBold inline>{FEEDBACK_PHONE}</Typography.PBold>
    </Typography.P>
    <Gap.LG />
    <Box.Faint className={Adjust.getPadClass(['XS', 'MD'])}>
      <Horizon align="TOP">
        <SpecialIcons.InfoColored scale="MD" />
        <Gap.XS />
        <Typography.P>{locale.dboInformationDialog.feedback.info}</Typography.P>
      </Horizon>
    </Box.Faint>
  </>
);

Feedback.displayName = 'Feedback';

/**
 * Наполнение ЭФ "Вывод информационного уведомления при входе в АС Экосистема для пользователей ДБО БСС".
 * Включает в себя кнопку просмотра PDF-файла с информацией для новых пользователей.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005123
 */
export const DboInformationContent: React.FC = () => {
  const openDocument = () => {
    window.open(Assets.PdfDocument);
  };

  return (
    <LayoutScroll autoHeight autoHeightMax="calc(100vh - 200px)">
      <Adjust hor="CENTER">
        <Assets.ShoutIcon />
        <Gap />
        <Typography.H2>{locale.dboInformationDialog.header}</Typography.H2>
      </Adjust>
      <Gap />
      <Typography.Text>{locale.dboInformationDialog.content1}</Typography.Text>
      <Gap.SM />
      <Typography.Text>{locale.dboInformationDialog.content2}</Typography.Text>
      <Gap.XL />
      <Adjust hor="CENTER">
        <PrimaryButton dimension="LG" onClick={openDocument}>
          {locale.dboInformationDialog.viewDocuments}
        </PrimaryButton>
      </Adjust>
      <Gap.X2L />
      <MainPageFeatures />
      <Gap.XL />
      <GboFeatures />
      <Gap.XL />
      <Feedback />
      <Gap.LG />
      <Typography.Text align="CENTER">{locale.dboInformationDialog.ending.part1}</Typography.Text>
      <Gap />
      <Typography.Text align="CENTER">{locale.dboInformationDialog.ending.part2}</Typography.Text>
    </LayoutScroll>
  );
};

DboInformationContent.displayName = 'DboInformationContent';
