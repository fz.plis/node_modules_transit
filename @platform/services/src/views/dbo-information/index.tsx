import React from 'react';
import { Box, DialogTemplate, Input } from '@platform/ui';
import { DONT_SHOW_DBO_INFORMATION_STORAGE_KEY } from '../../constants';
import { locale } from '../../localization';
import { useLocalStorage } from '../../utils';
import { DboInformationContent } from './content';
import css from './styles.scss';

/**
 * ЭФ "Вывод информационного уведомления при входе в АС Экосистема для пользователей ДБО БСС".
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005123
 */
export const DboInformationDialog: React.FC<{
  onClose(): void;
}> = ({ onClose }) => {
  const [dontShowAgain, setDontShowAgain] = useLocalStorage(DONT_SHOW_DBO_INFORMATION_STORAGE_KEY, false);

  return (
    <Box className={css.modal}>
      <DialogTemplate
        content={<DboInformationContent />}
        footerAddon={<Input.Checkbox label={locale.dboInformationDialog.dontShowAgain} value={dontShowAgain} onChange={setDontShowAgain} />}
        header={''}
        onClose={onClose}
      />
    </Box>
  );
};

DboInformationDialog.displayName = 'DboInformationDialog';
