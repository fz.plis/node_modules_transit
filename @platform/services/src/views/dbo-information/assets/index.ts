// является названием файла
// eslint-disable-next-line @eco/no-missing-localization
import PdfDocument from './Клиент-Банк.WEB. Новый пользовательский интерфейс.15.06.2021.pdf';

export const Assets = {
  /** Иконка для отображения на ЭФ. */
  ShoutIcon: require('./shout.svg'),
  /** Документ для просмотра. */
  PdfDocument,
};
