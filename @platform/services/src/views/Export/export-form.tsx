import React from 'react';
import type { IOption } from '@platform/ui';
import { Input, DialogTemplate, Gap, BUTTON } from '@platform/ui';
import { FILE_EXTENSIONS } from '../../constants';
import type { IExportActionSettings } from '../../interfaces';
import { EXPORT_TYPE, FORM_TYPE } from '../../interfaces';
import { locale } from '../../localization';
import css from './export.scss';

export interface IExportProps {
  numberOfDocuments: number;
  formType: FORM_TYPE;
  options: IOption[];
  defaultValue: string;
  onClose(): void;
  onExportClick(settings: IExportActionSettings): void;
  userlocale: { header: string; text: string };
}

export const ExportDialog: React.FC<IExportProps> = ({
  onClose,
  formType,
  userlocale,
  onExportClick,
  numberOfDocuments,
  defaultValue,
  options,
}) => {
  const [type, setType] = React.useState(numberOfDocuments > 1 ? EXPORT_TYPE.DOCUMENTS_LIST : defaultValue);
  const [extension, setExtension] = React.useState(FILE_EXTENSIONS.PDF);
  const [splitReport, setSplitReport] = React.useState(true);

  const onClick = React.useCallback(() => {
    onExportClick({
      selectedValue: type,
      extension,
      splitReport,
    });
    onClose();
  }, [onExportClick, type, extension, splitReport, onClose]);

  const onChangeType = React.useCallback(
    (value: string) => {
      setType(value);

      if (value === EXPORT_TYPE.DOCUMENTS && formType === FORM_TYPE.PRINT) {
        setExtension(FILE_EXTENSIONS.HTML);
        setSplitReport(false);
      } else {
        setExtension(FILE_EXTENSIONS.PDF);
        setSplitReport(true);
      }
    },
    [setType, setExtension, setSplitReport, formType]
  );

  const extensionOption = React.useMemo(
    () => [
      {
        value: FILE_EXTENSIONS.PDF,
        label: locale.fileExtensions.pdf,
        disabled: type !== EXPORT_TYPE.DOCUMENTS_LIST,
      },
      {
        value: FILE_EXTENSIONS.XLS,
        label: locale.fileExtensions.xls,
        disabled: type !== EXPORT_TYPE.DOCUMENTS_LIST,
      },
    ],
    [type]
  );

  return (
    <DialogTemplate
      actions={[
        {
          name: 'Ok',
          label: userlocale.text,
          onClick,
          buttonType: BUTTON.PRIMARY,
        },
      ]}
      content={
        <>
          <Input.RadioGroup name={'exportTypeRadio'} options={options} value={type} onChange={onChangeType} />
          {formType === FORM_TYPE.EXPORT && (
            <>
              <Gap.XS />
              <Input.RadioGroup
                horizontal
                className={css.cont}
                name={'extensionRadio'}
                options={extensionOption}
                value={extension}
                onChange={setExtension}
              />
            </>
          )}
        </>
      }
      header={userlocale.header}
      onClose={onClose}
    />
  );
};
