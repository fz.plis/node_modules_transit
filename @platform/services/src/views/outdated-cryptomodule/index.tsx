import React, { useState, useEffect } from 'react';
import { to } from '@platform/core';
import { Adjust, DialogTemplate, Gap, PrimaryButton, RegularButton, Typography } from '@platform/ui';
import { getAppConfigItem } from '../../core/app-config';
import { getCryptoModule, getCryptoPluginApp } from '../../crypto';
import { CRYPTO_PLUGINS_FEATURE } from '../../crypto/olk/constants';
import { locale } from '../../localization';
import { isOutdatedCryptoModule } from '../../utils';
import { Assets } from './assets';

const INTERVAL = 3000;
const AWAIT_TIME_RESPONSE = 5 * 60 * 1000;

const OutdatedCryptomoduleContent: React.FC = () => (
  <Adjust hor="CENTER">
    <Assets.CertificatesIcon />
    <Gap />
    <Typography.H3>{locale.outdatedCryptomoduleDialog.header}</Typography.H3>
    <Gap />
    <Typography.P>{locale.outdatedCryptomoduleDialog.content}</Typography.P>
  </Adjust>
);

OutdatedCryptomoduleContent.displayName = 'OutdatedCryptomoduleContent';

/**
 * ЭФ установка ПО для работы с ЭП по результатам успешной авторизации/аутентификации.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005211
 */
export const OutdatedCryptomoduleDialog: React.FC<{
  onClose(): void;
}> = ({ onClose }) => {
  const [isInstalling, setInstalling] = useState(false);

  const openInstallerLink = () => {
    setInstalling(true);
    window.open(getCryptoPluginApp());
  };

  useEffect(() => {
    if (!isInstalling) {
      return;
    }

    const cryptoModule = getCryptoModule();
    const cryptoPlugins = getAppConfigItem(CRYPTO_PLUGINS_FEATURE);

    const jobId = setInterval(async () => {
      const [installed] = await to(cryptoModule.installationCheck());
      const [isOutdated] = await to(isOutdatedCryptoModule(cryptoModule, cryptoPlugins));

      if (installed && !isOutdated) {
        clearInterval(jobId);
        onClose();
      }
    }, INTERVAL);

    setTimeout(() => {
      clearInterval(jobId);
      setInstalling(false);
    }, AWAIT_TIME_RESPONSE);

    return () => clearInterval(jobId);
  }, [isInstalling, onClose]);

  return (
    <DialogTemplate
      content={<OutdatedCryptomoduleContent />}
      // TODO: actions не поддерживают смену установку loading
      footerAddon={
        <>
          <PrimaryButton dimension="SM" loading={isInstalling} onClick={openInstallerLink}>
            {locale.outdatedCryptomoduleDialog.install}
          </PrimaryButton>
          <Gap.XS />
          <RegularButton dimension="SM" onClick={onClose}>
            {locale.outdatedCryptomoduleDialog.cancel}
          </RegularButton>
        </>
      }
      header={''}
      onClose={onClose}
    />
  );
};

OutdatedCryptomoduleDialog.displayName = 'OutdatedCryptomoduleDialog';
