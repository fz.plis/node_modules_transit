import React, { useContext } from 'react';
import type { FormRenderProps } from 'react-final-form';
import { ACTIONS, Box, Modal, Font, Typography, ServiceIcons, RegularButton, Horizon, PrimaryButton, Gap, Fields } from '@platform/ui';
import { locale } from '../../../localization';
import { CONFIRM_SPO_FORM_FIELDS } from '../constants';
import { ConfirmSpoContext } from '../context';
import type { IConfirmSpoForm } from '../interface';
import css from './styles.scss';

/** Содержимое формы "Изменение статуса сертификата". */
export const ConfirmSpo: React.FC<FormRenderProps<IConfirmSpoForm>> = ({ handleSubmit }) => {
  const { title, description, onClose } = useContext(ConfirmSpoContext);

  return (
    <Box>
      <form onSubmit={handleSubmit}>
        <Modal className={css.modal}>
          <Box clickable className={css.closeMark} data-action={ACTIONS.CLOSE} onClick={onClose}>
            <ServiceIcons.Close fill="FAINT" />
          </Box>
          <Typography.H3>{title}</Typography.H3>
          <Gap.LG />
          <Typography.P>{description}</Typography.P>
          <Gap.XS />
          <Typography.Text>
            <Font.Faint>{locale.confirmSpoForm.commentForClient}</Font.Faint>
          </Typography.Text>
          <Fields.TextArea name={CONFIRM_SPO_FORM_FIELDS.NOTE_FOR_CLIENT} />
          <Gap.XL />
          <Horizon>
            <PrimaryButton onClick={handleSubmit}>{locale.continue}</PrimaryButton>
            <Gap.XS />
            <RegularButton onClick={onClose}>{locale.action.close}</RegularButton>
          </Horizon>
        </Modal>
      </form>
    </Box>
  );
};

ConfirmSpo.displayName = 'ConfirmSpo';
