import { check, composeValidation, createValidator } from '@platform/core';
import { transformFormValidation } from '@platform/ui';
import { err, validators } from '../../utils/validators';
import { CONFIRM_SPO_FORM_FIELDS } from './constants';
import type { IConfirmSpoForm } from './interface';

const rules = composeValidation(check(CONFIRM_SPO_FORM_FIELDS.NOTE_FOR_CLIENT).on(err.notEmpty()));

export const dsfTypeValidation = createValidator(rules, validators);

export const validateConfirmSpoForm = (values: IConfirmSpoForm) => transformFormValidation(values, dsfTypeValidation.validate);
