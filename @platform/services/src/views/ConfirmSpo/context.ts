import { createContext } from 'react';
import type { IConfirmProps } from './interface';

export const ConfirmSpoContext = createContext<IConfirmProps>({
  onClose: () => {},
  onConfirm: () => {},
  title: '',
  description: '',
  validate: true,
});
