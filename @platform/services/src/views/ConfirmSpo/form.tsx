import React, { useCallback } from 'react';
import { Form } from '@platform/ui';
import { ConfirmSpoContext } from './context';
import type { IConfirmProps, IConfirmSpoForm } from './interface';
import { validateConfirmSpoForm } from './validation';
import { ConfirmSpo } from './view';

/** Форма "Изменение статуса сертификата". */
export const ConfirmSpoModal: React.FC<IConfirmProps> = ({ title, description, validate, onConfirm, onClose }) => {
  const onSubmit = useCallback(
    (values: IConfirmSpoForm) => {
      onConfirm(values.noteForClient);
      onClose();
    },
    [onClose, onConfirm]
  );

  return (
    <ConfirmSpoContext.Provider value={{ title, description, validate, onConfirm, onClose }}>
      <Form validateOnBlur render={ConfirmSpo} validate={validate ? validateConfirmSpoForm : undefined} onSubmit={onSubmit} />
    </ConfirmSpoContext.Provider>
  );
};

ConfirmSpoModal.displayName = 'ConfirmSpoModal';
