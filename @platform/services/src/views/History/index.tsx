import React, { useCallback } from 'react';
import { Box, Gap, Line, Typography, LayoutScroll, DialogTemplate, ROLE, PrimaryButton } from '@platform/ui';
import type { IHistoryDialogData, IHistoryResponse } from '../../interfaces';
import { locale } from '../../localization';

export interface IHistoryRowProps<T extends IHistoryResponse> {
  data: T;
  hasAdmin: boolean;
  onHistoryRowLinkClick?(): void;
}

export interface IHistoryDialog {
  data: IHistoryDialogData<any>;
  num: string;
  date: string;
  clientName: string;
  row: React.ComponentType<IHistoryRowProps<any>>;
  header: React.ReactNode;
  hasAdmin: boolean;
  onHistoryRowLinkClick(userId: string): void;
  onClose(): void;
}

export const HistoryDialog: React.FC<IHistoryDialog> = ({
  num,
  date,
  clientName,
  header,
  row: Row,
  data,
  onClose,
  onHistoryRowLinkClick,
  hasAdmin,
}) => {
  const onClick = useCallback(
    (userId: string) => {
      onHistoryRowLinkClick(userId);
      onClose();
    },
    [onClose, onHistoryRowLinkClick]
  );
  const content = (
    <Box style={{ width: '850px', height: '360px' }}>
      <Typography.H3>{locale.dialog.history.header}</Typography.H3>
      <Typography.H3 data-field="numberAndDate">{locale.dialog.history.numberAndDate({ num, date })}</Typography.H3>
      <Gap />
      <Typography.Text fill="FAINT">{locale.client.label}</Typography.Text>
      <Typography.H3 data-field="clientName" line={'COLLAPSE'} title={clientName}>
        {clientName}
      </Typography.H3>
      <Gap.LG />
      {header}
      <Gap.XS />
      <Line />
      <Gap.XS />
      <LayoutScroll role={ROLE.LISTBOX} style={{ maxHeight: '180px' }}>
        {data.historyList.map((historyRow, idx) => (
          // eslint-disable-next-line react/no-array-index-key
          <Row key={idx} data={historyRow} hasAdmin={hasAdmin} onHistoryRowLinkClick={() => onClick(historyRow.userId)} />
        ))}
      </LayoutScroll>
    </Box>
  );

  return (
    <DialogTemplate
      content={content}
      footerAddon={
        <PrimaryButton dimension="SM" onClick={onClose}>
          {locale.action.close}
        </PrimaryButton>
      }
      header={''}
      onClose={onClose}
    />
  );
};
