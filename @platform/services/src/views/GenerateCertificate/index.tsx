import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Font,
  Link,
  Modal,
  Box,
  ServiceIcons,
  Typography,
  Gap,
  RadioGroup,
  Adjust,
  Horizon,
  SpecialIcons,
  PrimaryButton,
  RegularButton,
  ACTIONS,
  dialog,
} from '@platform/ui';
import type { IRadioGroupOption } from '@platform/ui';
import { certEnrollService, orgCertsService } from '../../client/services';
import { COMMON_STREAM_URL } from '../../constants';
import { getAuthValue } from '../../core/auth';
import { REMOTE_SOURCE } from '../../interfaces';
import { locale } from '../../localization';
import { showFile, useRedirect } from '../../utils';
import { CERTIFICATES_TYPES, UNQ_CERT_CREATE_PRIVILEGE } from './constants';
import css from './styles.scss';

/**
 * Получение опций по выпуску КЭП/НЭП. При отсутствии роли
 * `CLIENT_USER_EDIT_DBO` опция НЭП будет отключена.
 *
 * @param certSource - Источник сертификата, при источнике БСС опция КЭП будет
 * отключена.
 */
const getAvailableActionsOptions = (certSource?: REMOTE_SOURCE): IRadioGroupOption[] => {
  const { hasAuthority } = getAuthValue();
  // для сертификатов БСС не доступен выпуск КЭП, подробности у Вадима Селезнева
  const isQesDisabled = certSource === REMOTE_SOURCE.BSS_DBO_320;

  return [
    {
      value: CERTIFICATES_TYPES.QES,
      disabled: isQesDisabled,
      get label() {
        return locale.modal.generateCertificate.options.qes;
      },
    },
    {
      value: CERTIFICATES_TYPES.UES,
      disabled: !hasAuthority(UNQ_CERT_CREATE_PRIVILEGE),
      get label() {
        return locale.modal.generateCertificate.options.ues;
      },
    },
  ];
};

const InfoQES: React.FC = () => {
  const getFile = async () => {
    const response = await orgCertsService.downloadCaAccredited();

    showFile(response.data, response.fileName, response.type);
  };

  return (
    <Font lineHeight={'MD'}>
      {locale.modal.generateCertificate.options.qesInfo.part1}{' '}
      <Link volume={'MD'} onClick={getFile}>
        {locale.modal.generateCertificate.options.qesInfo.link}
      </Link>{' '}
      {locale.modal.generateCertificate.options.qesInfo.part2}
    </Font>
  );
};

interface IInfoProps {
  type: CERTIFICATES_TYPES;
}

const Info: React.FC<IInfoProps> = ({ type }) => {
  if (type === CERTIFICATES_TYPES.QES) {
    return <InfoQES />;
  }

  return <Font lineHeight={'MD'}>{locale.modal.generateCertificate.options.uesInfo}</Font>;
};

interface IGenerateCertificateState {
  /** ID представителя клиента. */
  clientId: string;
  /** Именование клиента. */
  client: string;
  /** ИНН. */
  inn: string;
  /** ФИО владельца сертификата. */
  owner: string;
  /** Источник сертификата, временно отсутствует при инициализации. */
  remoteSource?: REMOTE_SOURCE;
}

interface IGenerateCertificateProps {
  /**
   * Id Сертификата.
   */
  orgCertId: string;
  /**
   * Метод закрытия окна.
   */
  onClose(): void;
  /**
   * Дополнительный метод для выполнения перед переходом на следующий этап.
   */
  onContinue?(): void;
}

export const GenerateCertificate: React.FC<IGenerateCertificateProps> = ({ orgCertId, onClose, onContinue = () => {} }) => {
  const { push } = useHistory();
  const [type, setType] = useState('');
  const [certOwnerInfo, setCertOwnerInfo] = useState<IGenerateCertificateState>({
    clientId: '',
    client: '',
    inn: '',
    owner: '',
  });

  const goToQesWizard = useRedirect(`${COMMON_STREAM_URL.PROFILE}/certs/${certOwnerInfo.clientId}`, { orgCertId });
  const goToEsignNew = useRedirect(`${COMMON_STREAM_URL.ESIGN}/new`, { orgOfficialCertId: orgCertId });

  const handleContinue = () => {
    switch (type) {
      case CERTIFICATES_TYPES.QES:
        goToQesWizard();
        onClose();
        break;

      case CERTIFICATES_TYPES.UES:
        certEnrollService
          .checkCreateNew(orgCertId)
          .then(canCreateNewRequest => {
            if (canCreateNewRequest) {
              goToEsignNew();
            } else {
              return certEnrollService.getLastActiveRequest(orgCertId).then(result => push(`${COMMON_STREAM_URL.ESIGN}/${result!.id}`));
            }
          })
          .catch(() => {
            dialog.showAlert(locale.generateCertificate.certFileLoadError, {
              header: locale.error.text,
            });
          })
          .finally(onClose);
        break;

      default:
        break;
    }

    onContinue();
  };

  useEffect(() => {
    const fetchOrgCer = async () => {
      const orgCert = await orgCertsService.get(orgCertId);

      if (orgCert) {
        setCertOwnerInfo({
          clientId: orgCert.clientOfficial.client.id,
          client: orgCert.clientOfficial.client.fullName,
          inn: orgCert.clientOfficial.client.innKio,
          owner: orgCert.certOwnerName,
          remoteSource: orgCert.remoteSource,
        });
      }
    };

    void fetchOrgCer();
  }, [orgCertId]);

  return (
    <Modal className={css.modal}>
      <Box clickable className={css.closeMark} data-action={ACTIONS.CLOSE} onClick={onClose}>
        <ServiceIcons.Close fill={'FAINT'} />
      </Box>
      <Typography.H3>{locale.modal.generateCertificate.header}</Typography.H3>
      <Gap />

      <Typography.Text fill={'FAINT'}>{locale.forms.certificateRequest.label.organization}</Typography.Text>
      <Typography.Text>{certOwnerInfo.client}</Typography.Text>
      <Gap />

      <Typography.Text fill={'FAINT'}>{locale.forms.certificateRequest.label.innKio}</Typography.Text>
      <Typography.Text>{certOwnerInfo.inn}</Typography.Text>
      <Gap />

      <Typography.Text fill={'FAINT'}>{locale.modal.generateCertificate.owner}</Typography.Text>
      <Typography.Text>{certOwnerInfo.owner}</Typography.Text>
      <Gap />

      <Typography.H3>{locale.modal.generateCertificate.selectCertificateType}</Typography.H3>
      <Gap />

      <RadioGroup
        addon={option => (
          <>
            <Gap />
            <Box.Faint className={Adjust.getPadClass(['XS', 'MD'])}>
              <Horizon align={'TOP'}>
                <SpecialIcons.InfoColored />
                <Gap.SM />
                <Info type={option.value} />
              </Horizon>
            </Box.Faint>
          </>
        )}
        options={getAvailableActionsOptions(certOwnerInfo.remoteSource)}
        value={type}
        onChange={setType}
      />
      <Gap.XL />

      <Horizon>
        <PrimaryButton disabled={!type} onClick={handleContinue}>
          {locale.continue}
        </PrimaryButton>
        <Gap.XS />
        <RegularButton onClick={onClose}>{locale.action.cancel}</RegularButton>
      </Horizon>
    </Modal>
  );
};
