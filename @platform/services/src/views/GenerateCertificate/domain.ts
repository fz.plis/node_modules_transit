import { router } from '@platform/core';
import { domain, value } from '@platform/tools/istore';
import { getSelectors } from '@platform/tools/istore-react';
import { orgCertsService } from '../../client/services';
import { COMMON_STREAM_URL } from '../../constants';
import { getAppConfigItem } from '../../core/app-config';
import { showFile } from '../../utils';
import { CERTIFICATES_TYPES } from './constants';

export interface IGenerateCertificateModalDomain {
  orgCertId: string;
  onClose(): void;
  onContinue?(): void;
}

const getDomain = ({ orgCertId, onContinue = () => {} }: IGenerateCertificateModalDomain) =>
  domain({
    router,
    type: value(''),
    certOwnerInfo: value({
      clientId: '',
      client: '',
      inn: '',
      owner: '',
    }),
  }).withMethods((m, s) => ({
    ...m,
    handleTypeChange: (val: CERTIFICATES_TYPES) => {
      m.type.set(val);
    },
    handleContinue: () => {
      switch (s.type) {
        case CERTIFICATES_TYPES.UES:
          m.router.push(getAppConfigItem('regenQESUrl'));
          break;

        case CERTIFICATES_TYPES.QES:
          m.router.push(`${COMMON_STREAM_URL.PROFILE}/certs/${s.certOwnerInfo.clientId}`, { orgCertId });
          break;

        default:
          break;
      }

      onContinue();
    },
    init: async () => {
      const orgCert = await orgCertsService.get(orgCertId);

      m.certOwnerInfo.set({
        clientId: orgCert.clientOfficial.client.id,
        client: orgCert.clientOfficial.client.fullName,
        inn: orgCert.clientOfficial.client.innKio,
        owner: orgCert.certOwnerName,
      });
    },
    getFile: async () => {
      const response = await orgCertsService.downloadCaAccredited();

      showFile(response.data, response.fileName, response.type);
    },
  }));

export const { asRoot, useDomain, bind, call } = getSelectors('generateCertificateDialog', getDomain, {
  onMount: ({ methods }) => {
    void methods.init();
  },
});
