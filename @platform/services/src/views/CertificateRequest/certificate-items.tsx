import React from 'react';
import { CertificateRow } from './certificate-item';

interface ICertificateRows {
  rows: Record<string, any>;
  labelsPath: Record<string, any>;
}

export class CertificateRows extends React.Component<ICertificateRows> {
  render() {
    const { rows, labelsPath } = this.props;

    return Object.keys(rows).map(key => <CertificateRow key={rows[key]} label={labelsPath[key]} value={rows[key]} />);
  }
}
