import React from 'react';
import { Typography, Gap } from '@platform/ui';
import { locale } from '../../localization';
import { formatDateTime } from '../../utils';
import { CertificateRows } from './certificate-items';
import type { ICertificateRequestProps } from './interface';

export const CertificateRequest: React.FC<ICertificateRequestProps> = ({ certificateRequest }) => {
  const certificate = certificateRequest.certParsedData;

  return (
    <>
      <Typography.PBold>{locale.forms.certificateRequest.label.mainInfo}</Typography.PBold>
      <Gap.SM />
      <CertificateRows
        labelsPath={locale.forms.certificateRequest.label}
        rows={{
          organization: certificateRequest.orgName,
          fio: certificateRequest.fio,
          innKio: certificateRequest.innKio,
          kpp: certificateRequest.kpp || '-',
          ogrn: certificateRequest.ogrn,
        }}
      />
      {certificate && (
        <>
          <Gap.XL />
          <Typography.PBold>{locale.forms.certificateRequest.label.certInfo}</Typography.PBold>
          <Gap.SM />
          <CertificateRows
            labelsPath={locale.forms.certificateRequest.label}
            rows={{
              serialNumber: certificate.serialNumber ?? '-',
              certificateValidFrom: certificate?.validFrom ? formatDateTime(certificate.validFrom) : '-',
              certificateValidTo: certificate?.validTo ? formatDateTime(certificate.validTo) : '-',
              certificateIssuer: certificate.authorityName ?? '-',
              thumbprint: certificate.footprint ?? '-',
            }}
          />
          <Gap.XL />
          <Typography.PBold>{locale.forms.certificateRequest.label.certificateSubject}</Typography.PBold>
          <Gap.SM />
          <CertificateRows
            labelsPath={locale.forms.certificateRequest.label.certificate}
            rows={{
              commonName: certificate.subjectCommonName,
              organization: certificate.subjectOrganization,
              organizationUnit: certificate.subjectOrganizationalUnit,
              location: certificate.subjectLocality,
              country: certificate.subjectCountry,
              email: certificate.subjectEmail,
              title: certificate.subjectTitle,
              name: certificate.subjectGivenName,
              lastName: certificate.subjectSurname,
            }}
          />
        </>
      )}
    </>
  );
};

CertificateRequest.displayName = 'CertificateRequest';
