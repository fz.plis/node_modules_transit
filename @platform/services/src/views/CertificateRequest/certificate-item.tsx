import React from 'react';
import { ConfirmationRow, Line } from '@platform/ui';
import type { ISubjectProps } from './interface';

export const CertificateRow: React.FC<ISubjectProps> = ({ label, value }) =>
  value ? (
    <>
      <ConfirmationRow multiline label={label} value={value} />
      <Line fill="FAINT" />
    </>
  ) : null;

CertificateRow.displayName = 'CertificateRow';
