import type { ICertificateRequest } from '../../interfaces';

export interface ICertificateRequestProps {
  certificateRequest: ICertificateRequest;
}

export interface ISubjectProps {
  label: string;
  value?: string;
}
