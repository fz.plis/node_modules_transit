import React, { useMemo } from 'react';
import cn from 'classnames';
import { Box, Adjust, Gap, ServiceIcons, isIe, ACTIONS } from '@platform/ui';
import { STEPS } from './constants';
import { UpdateContactProvider, useUpdateContactContext } from './context';
import type { IUpdateContactProviderProps } from './interfaces';
import css from './styles.scss';
import { ChangePhoneModal, ChangeEmailModal, Result } from './view';
import { MainStep } from './view/main-step';

const views = {
  [STEPS.MAIN]: MainStep,
  [STEPS.UPDATE_PHONE]: ChangePhoneModal,
  [STEPS.UPDATE_EMAIL]: ChangeEmailModal,
  [STEPS.RESULT]: Result,
};

const Step: React.FC = () => {
  const { step } = useUpdateContactContext();
  const Comp = useMemo(() => views[step], [step]);

  return <Comp />;
};

Step.displayName = 'Step';

export const UpdateContactInfoContainer: React.FC = () => {
  const { onClose } = useUpdateContactContext();

  return (
    <Box.Base className={cn(isIe() ? css.containerIe : css.container, Adjust.getPadClass(['XL', 'XL']))}>
      <Box clickable className={css.closeMark} data-action={ACTIONS.CLOSE} onClick={onClose}>
        <ServiceIcons.Close fill={'FAINT'} scale={'LG'} />
      </Box>
      <Gap />
      <Step />
    </Box.Base>
  );
};

UpdateContactInfoContainer.displayName = 'UpdateContactInfoContainer';

export const UpdateContactInfo: React.FC<IUpdateContactProviderProps> = props => (
  <UpdateContactProvider {...props}>
    <UpdateContactInfoContainer />
  </UpdateContactProvider>
);

UpdateContactInfo.displayName = 'UpdateContactInfo';
