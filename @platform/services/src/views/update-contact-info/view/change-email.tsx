import React from 'react';
import { useFormState } from 'react-final-form';
import { Label, Horizon, Gap, Typography, Fields, PrimaryButton, RegularButton } from '@platform/ui';

import { locale } from '../../../localization';
import { FORM_FIELD } from '../constants';
import { useUpdateContactContext } from '../context';
import { ConfirmStep } from './confirm-step';
import css from './style.scss';

const EditEmailStep: React.FC = () => {
  const { cancel } = useUpdateContactContext();

  return (
    <>
      <Typography.P>{locale.dialog.updateContactInfo.updateEmail.text}</Typography.P>
      <Gap />
      <Label text={locale.dialog.updateContactInfo.updateEmail.field.email}>
        <Horizon className={css.flexHorizon}>
          <Horizon.Spacer />
          <Fields.Text name={FORM_FIELD.EMAIL} />
        </Horizon>
      </Label>

      <Gap.XL />

      <Horizon>
        <PrimaryButton type="submit">{locale.action.accept}</PrimaryButton>
        <Gap.XS />
        <RegularButton onClick={cancel}>{locale.action.cancel}</RegularButton>
      </Horizon>
    </>
  );
};

EditEmailStep.displayName = 'EditEmailStep';

export const ChangeEmailModal: React.FC = () => {
  const { challengeId } = useUpdateContactContext();
  const {
    values: { [FORM_FIELD.EMAIL]: email },
  } = useFormState();
  const showCodeConfirmation = Boolean(challengeId);

  return (
    <>
      <Gap.X2S />
      {showCodeConfirmation ? (
        <>
          <Typography.H3>{locale.dialog.updateContactInfo.confirmEmail.header}</Typography.H3>
          <Gap />
          <ConfirmStep>
            {locale.dialog.updateContactInfo.confirm.emailCodeMessageFirst}
            <br />
            <b>{email}</b> {locale.dialog.updateContactInfo.confirm.emailCodeMessageSecond} <br />
            <br />
            {locale.dialog.updateContactInfo.confirm.emailCodeNote}
          </ConfirmStep>
        </>
      ) : (
        <>
          <Typography.H3>{locale.dialog.updateContactInfo.updateEmail.header}</Typography.H3>
          <Gap />
          <EditEmailStep />
        </>
      )}
      <Gap.XL />
    </>
  );
};

ChangeEmailModal.displayName = 'ChangeEmailModal';
