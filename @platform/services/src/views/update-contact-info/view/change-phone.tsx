import React from 'react';
import { useFormState } from 'react-final-form';
import { Label, Gap, Horizon, Typography, PrimaryButton, RegularButton, Fields } from '@platform/ui';
import { locale } from '../../../localization';
import { formatMobilePhone } from '../../../utils';
import { FORM_FIELD } from '../constants';
import { useUpdateContactContext } from '../context';
import { ConfirmStep } from './confirm-step';
import css from './style.scss';

const EditPhoneStep: React.FC = () => {
  const { onClose } = useUpdateContactContext();

  return (
    <>
      <Typography.P>{locale.dialog.updateContactInfo.updatePhone.text}</Typography.P>
      <Gap.LG />
      <Label text={locale.dialog.updateContactInfo.updatePhone.field.phoneNumber}>
        <Horizon className={css.flexHorizon}>
          <Fields.Phone name={FORM_FIELD.PHONE} />
        </Horizon>
      </Label>

      <Gap.XL />

      <Horizon>
        <PrimaryButton type="submit">{locale.action.accept}</PrimaryButton>
        <Gap.XS />
        <RegularButton onClick={onClose}>{locale.action.cancel}</RegularButton>
      </Horizon>
    </>
  );
};

EditPhoneStep.displayName = 'EditPhoneStep';

export const ChangePhoneModal: React.FC = () => {
  const { challengeId } = useUpdateContactContext();
  const {
    values: { [FORM_FIELD.PHONE]: phoneNumber },
  } = useFormState();
  const showCodeConfirmation = Boolean(challengeId);

  return (
    <>
      {showCodeConfirmation ? (
        <>
          <Typography.H3>{locale.dialog.updateContactInfo.confirmPhone.header}</Typography.H3>
          <Gap />
          <ConfirmStep>
            {locale.dialog.updateContactInfo.confirm.smsCodeMessageFirst}
            <br />
            <b>{formatMobilePhone(phoneNumber)}</b> {locale.dialog.updateContactInfo.confirm.smsCodeMessageSecond}
            <br />
            <br />
            {locale.dialog.updateContactInfo.confirm.smsCodeNote}
          </ConfirmStep>
        </>
      ) : (
        <>
          <Typography.H3>{locale.dialog.updateContactInfo.updatePhone.header}</Typography.H3>
          <Gap />
          <EditPhoneStep />
        </>
      )}
      <Gap.XL />
    </>
  );
};

ChangePhoneModal.displayName = 'ChangePhoneModal';
