import React from 'react';
import { useFormState } from 'react-final-form';
import { Label, Gap, Horizon, Font, Timer, PrimaryButton, RegularButton, Fields } from '@platform/ui';
import { locale } from '../../../localization';
import { INPUT_WIDTH, FORM_FIELD } from '../constants';
import { useUpdateContactContext } from '../context';
import type { IFormProps } from '../interfaces';

const CODE_LENGTH = 6;

export const ConfirmStep: React.FC = ({ children }) => {
  const {
    values: { [FORM_FIELD.CODE]: codeValue },
  } = useFormState<IFormProps>();
  const { timerValue, cancel, codeLifeTime } = useUpdateContactContext();

  const submitCodeConfirmationCodeText = timerValue ? locale.continue : locale.dialog.updateContactInfo.confirm.sendAgain;
  const submitCodeButtonDisabled = Boolean(codeValue?.length !== CODE_LENGTH && timerValue);

  return (
    <>
      <Font volume="MD">{children}</Font>

      <Gap />
      <Label text={locale.dialog.updateContactInfo.confirm.smsCode}>
        <Horizon>
          <Fields.ConfirmationCode name={FORM_FIELD.CODE} width={INPUT_WIDTH} />
          <Gap />
          <Timer percent={(timerValue / codeLifeTime) * 100} time={timerValue} />
        </Horizon>
      </Label>

      <Gap.X2L />

      <Horizon>
        <PrimaryButton disabled={submitCodeButtonDisabled} type="submit">
          {submitCodeConfirmationCodeText}
        </PrimaryButton>
        <Gap.XS />
        <RegularButton onClick={cancel}>{locale.action.cancel}</RegularButton>
      </Horizon>
    </>
  );
};

ConfirmStep.displayName = 'ConfirmStep';
