import React from 'react';
import { Typography, Gap, Horizon, PrimaryButton, RegularButton } from '@platform/ui';
import { locale } from '../../../localization';
import { useUpdateContactContext } from '../context';

export const MainStep: React.FC = () => {
  const { onClose, next } = useUpdateContactContext();

  return (
    <>
      <Typography.H3>{locale.dialog.updateContactInfo.mainStep.header}</Typography.H3>
      <Gap.SM />
      <Typography.P>{locale.dialog.updateContactInfo.mainStep.text}</Typography.P>
      <Gap.XL />
      <Gap.LG />
      <Horizon>
        <PrimaryButton onClick={next}>{locale.dialog.updateContactInfo.mainStep.action.update}</PrimaryButton>
        <Gap.SM />
        <RegularButton onClick={onClose}>{locale.dialog.updateContactInfo.mainStep.action.logout}</RegularButton>
      </Horizon>
    </>
  );
};

MainStep.displayName = 'MainStep';
