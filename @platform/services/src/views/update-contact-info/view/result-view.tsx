import React from 'react';
import { Typography, Gap, Horizon, SpecialIcons, PrimaryButton } from '@platform/ui';
import { locale } from '../../../localization';
import { RESULT_TYPE } from '../constants';
import { useUpdateContactContext } from '../context';

export const Result: React.FC = () => {
  const {
    resultForm: { type, text },
    next,
  } = useUpdateContactContext();
  const isError = type === RESULT_TYPE.ERROR;
  const Icon = isError ? SpecialIcons.Error : SpecialIcons.Success;

  return (
    <>
      <Typography.H3>{isError ? locale.error.text : locale.done}</Typography.H3>
      <Gap />
      <Horizon>
        <Icon scale={'MD'} />
        <Gap.SM />
        <Typography.P>{text}</Typography.P>
      </Horizon>
      <Gap.LG />
      <PrimaryButton onClick={next}>{locale.continue}</PrimaryButton>
    </>
  );
};

Result.displayName = 'Result';
