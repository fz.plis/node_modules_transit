import React, { createContext, useCallback, useContext, useEffect, useRef, useState } from 'react';
import type { FormApi } from 'final-form';
import { to } from '@platform/core';
import { dialog, Form, transformFormValidation } from '@platform/ui';
import { authService, uaaService } from '../../client/services';
import { showLoader, hideLoader } from '../../components';
import { SMS_CODE_LIFETIME } from '../../constants';
import { useAuth } from '../../core/auth';
import { getTimerInterval, useTimer } from '../../hooks';
import type { IClientProfile } from '../../interfaces';
import { ERROR } from '../../interfaces';
import { locale } from '../../localization';
import { errorHandler, CustomEvent } from '../../utils';
import { FORM_FIELD, RESULT_TYPE, STEPS, UPDATE_CONTACT_INFO_TYPE } from './constants';
import type { IFormProps, IUpdateContactProviderProps } from './interfaces';
import { emailValidation, phoneValidation } from './validation-rules';

/**
 * Результирующая форма.
 */
interface IResultForm {
  /**
   * Контент формы.
   */
  text: string;
  /**
   * Тип формы.
   */
  type: RESULT_TYPE;
}

/**
 * Свойства контекста обновления данных пользователя.
 */
interface IUpdateContextContext {
  /**
   * Идентификатор подтверждения.
   */
  challengeId: string;
  /**
   * Результирующая форма.
   */
  resultForm: IResultForm;
  /**
   * Значение обратного счета.
   */
  timerValue: number;
  /**
   * Время жизни кода.
   */
  codeLifeTime: number;
  /**
   * Текущий шаг.
   */
  step: STEPS;
  /**
   * Отменить операцию обновления данных.
   */
  cancel(): void;
  /**
   * Переход на следующий шаг.
   */
  next(): void;

  /**
   * Закрытие диалога.
   */
  onClose(): void;
}

const UpdateContactContext = createContext<IUpdateContextContext>({} as IUpdateContextContext);

export const UpdateContactProvider: React.FC<IUpdateContactProviderProps> = ({ onClose, updateType, disableResultStep, children }) => {
  const profile = useRef<IClientProfile | null>(null);
  const { logout } = useAuth();
  const [codeLifeTime, setCodeLifeTime] = useState(SMS_CODE_LIFETIME);
  const { value: timerValue, restart: timerRestart } = useTimer(SMS_CODE_LIFETIME);
  const [step, setStep] = useState(STEPS.MAIN);
  const [resultForm, setResultForm] = useState({
    text: '',
    type: RESULT_TYPE.SUCCESSFULLY,
  });

  const [formInitialValues, setFormInitialValues] = useState<IFormProps>({
    phone: '',
    email: '',
    code: '',
  });

  const [challengeId, setChallengeId] = useState('');

  const updatePhoneChallenge = async ({ phone }: IFormProps) => {
    const [res] = await to(uaaService.changePhoneNumber({ phoneNumber: phone }));

    return res || '';
  };

  const updateEmailChallenge = async ({ email }: IFormProps) => {
    const [res] = await to(uaaService.changeEmail({ email }));

    return res || '';
  };

  const updateChallenge = useCallback(
    async (values: IFormProps) => {
      const updateChallengeAction = step === STEPS.UPDATE_PHONE ? updatePhoneChallenge : updateEmailChallenge;

      showLoader();

      const [res] = await to(updateChallengeAction(values));

      hideLoader();

      if (res) {
        const lifeTime = getTimerInterval(Date.now(), res.expiredAt);

        setChallengeId(res.id);
        setCodeLifeTime(lifeTime);
        timerRestart(lifeTime);
      }
    },
    [step, timerRestart]
  );

  const resendCode = useCallback(async () => {
    showLoader();

    const [res] = await to(uaaService.renewConfirm(challengeId, false));

    hideLoader();

    if (res) {
      const lifeTime = getTimerInterval(Date.now(), res.expiredAt);

      setCodeLifeTime(lifeTime);

      timerRestart(lifeTime);
    }
  }, [challengeId, timerRestart]);

  const sendCode = useCallback(
    async (values: IFormProps) => {
      const task = step === STEPS.UPDATE_PHONE ? uaaService.completePhoneChange : uaaService.completeEmailChange;

      showLoader();

      const [res, err] = await to(task(challengeId, values.code));

      hideLoader();

      if (err) {
        errorHandler({
          [ERROR.VERIFICATION_CODESEND_FAILED]: () => {
            setResultForm({
              text: locale.error.verificationCodesendFailed,
              type: RESULT_TYPE.ERROR,
            });
          },
          [ERROR.LOGOUT_DUE_TO_EXEEDED_TRYES]: () => {
            dialog.showAlert(locale.dialog.updateContactInfo.confirm.exceededCodeEntryAttempts, {
              onClose: () => {
                setTimeout(() => {
                  logout();
                }, 400);
              },
            });
          },
        })(err);

        return;
      }

      if (res?.codeCorrect) {
        const newData =
          step === STEPS.UPDATE_PHONE ? { phoneNumber: values.phone, phoneConfirmed: true } : { email: values.email, emailConfirmed: true };

        // TODO: убрать событие, когда избавимся от mainDomain
        const updateEvent = new CustomEvent('userChanging', {
          detail: newData,
        });

        profile.current = {
          ...profile.current!,
          ...newData,
        };

        window.dispatchEvent(updateEvent);

        setResultForm({
          text: locale.dialog.updateContactInfo.successfullyConfirmed({
            text:
              step === STEPS.UPDATE_PHONE
                ? locale.dialog.updateContactInfo.updatePhone.field.phoneNumber
                : locale.dialog.updateContactInfo.updateEmail.field.email,
          }),
          type: RESULT_TYPE.SUCCESSFULLY,
        });

        if (disableResultStep) {
          onClose();
        } else {
          setStep(STEPS.RESULT);
        }
      } else {
        return {
          [FORM_FIELD.CODE]: {
            error: locale.dialog.updateContactInfo.confirm.incorrectValueCode,
          },
        };
      }
    },
    [challengeId, disableResultStep, logout, onClose, step]
  );

  const next = useCallback(() => {
    if (!profile.current?.phoneConfirmed && step === STEPS.MAIN) {
      setStep(STEPS.UPDATE_PHONE);
    } else if (!profile.current?.emailConfirmed && profile.current?.phoneConfirmed && step !== STEPS.UPDATE_EMAIL) {
      setChallengeId('');
      setStep(STEPS.UPDATE_EMAIL);
    } else if (profile.current?.emailConfirmed && profile.current?.phoneConfirmed) {
      onClose(true);
    }
  }, [onClose, step]);

  const cancel = useCallback(() => {
    if (updateType === UPDATE_CONTACT_INFO_TYPE.EMAIL || updateType === UPDATE_CONTACT_INFO_TYPE.PHONE) {
      onClose();
    } else {
      setStep(STEPS.MAIN);
    }
  }, [onClose, updateType]);

  useEffect(() => {
    const load = async () => {
      showLoader();

      const [profileRes] = await to(authService.profile());

      if (profileRes) {
        const { phoneNumber, email } = profileRes;

        profile.current = profileRes;
        setFormInitialValues({
          code: '',
          email: email || '',
          phone: phoneNumber,
        });
      }

      hideLoader();
    };

    void load();
  }, []);

  useEffect(() => {
    switch (updateType) {
      case UPDATE_CONTACT_INFO_TYPE.EMAIL:
        setStep(STEPS.UPDATE_EMAIL);
        break;
      case UPDATE_CONTACT_INFO_TYPE.PHONE:
        setStep(STEPS.UPDATE_PHONE);
        break;
      default:
        break;
    }
  }, [updateType]);

  const submitForm = useCallback(
    async (values: IFormProps, formApi: FormApi<IFormProps>) => {
      if (challengeId) {
        if (timerValue) {
          const err = await sendCode(values);

          if (!err) {
            formApi.change(FORM_FIELD.CODE, '');
          }

          return err;
        }

        await resendCode();
      } else {
        await updateChallenge(values);
      }
    },
    [challengeId, resendCode, sendCode, timerValue, updateChallenge]
  );

  const validation = (values: IFormProps) => {
    if (!profile.current) {
      return {};
    }

    const { phoneNumber, email, login, emailConfirmed, phoneConfirmed } = profile.current;

    switch (step) {
      case STEPS.UPDATE_PHONE:
        if (values.phone === phoneNumber && phoneConfirmed) {
          return {
            [FORM_FIELD.PHONE]: {
              error: locale.validation.newNumberMatchesCurrent,
            },
          };
        }

        if (login.includes(values.phone.slice(2))) {
          return {
            [FORM_FIELD.PHONE]: {
              error: locale.validation.newNumberMatchesLogin,
            },
          };
        }

        return transformFormValidation(values, phoneValidation().validate);
      case STEPS.UPDATE_EMAIL:
        if (values.email === email && emailConfirmed) {
          return {
            [FORM_FIELD.EMAIL]: {
              error: locale.validation.newEmailMatchesCurrent,
            },
          };
        }

        return transformFormValidation(values, emailValidation().validate);
      default:
        return {};
    }
  };

  return (
    <UpdateContactContext.Provider
      value={{
        codeLifeTime,
        challengeId,
        resultForm,
        timerValue,
        step,
        cancel,
        next,
        onClose,
      }}
    >
      <Form
        validateOnBlur
        initialValues={formInitialValues}
        render={({ handleSubmit }) => <form onSubmit={handleSubmit}>{children}</form>}
        validate={validation}
        onSubmit={submitForm}
      />
    </UpdateContactContext.Provider>
  );
};

UpdateContactProvider.displayName = 'UpdateContactProvider';

export const useUpdateContactContext = () => useContext(UpdateContactContext);
