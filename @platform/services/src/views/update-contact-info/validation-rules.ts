import { composeValidation, check, createValidator } from '@platform/core';
import { locale } from '../../localization';
import { err, validators } from '../../utils';
import { FORM_FIELD } from './constants';

export const confirmValidationRules = composeValidation(check(FORM_FIELD.CODE).on(err.notEmpty()));

export const phoneNumberValidationRules = () =>
  composeValidation(
    check(FORM_FIELD.PHONE).on(
      err.notEmpty(),
      err.exactLength(12, locale.validation.invalidNumber),
      err.matchPattern(/^\+79\d+/, locale.validation.phoneMustBeBegin)
    )
  );

export const emailValidationRules = composeValidation(check(FORM_FIELD.EMAIL).on(err.notEmpty(), err.email()));

export const phoneValidation = () => createValidator(phoneNumberValidationRules(), validators);
export const emailValidation = () => createValidator(emailValidationRules, validators);
export const codeValidation = () => createValidator(confirmValidationRules, validators);
