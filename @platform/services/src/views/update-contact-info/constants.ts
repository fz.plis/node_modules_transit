import { pathGenerator } from '@platform/core';

/**
 * Поля формы.
 */
export enum FORM_FIELD {
  /**
   * Телефон.
   */
  PHONE = 'phone',
  /**
   * Email.
   */
  EMAIL = 'email',
  /**
   * Код.
   */
  CODE = 'code',
}

export enum STEPS {
  MAIN = 'MAIN',
  UPDATE_PHONE = 'UPDATE_PHONE',
  UPDATE_EMAIL = 'UPDATE_EMAIL',
  RESULT = 'RESULT',
}

export enum UPDATE_CONTACT_INFO_TYPE {
  CONFIRM = 'CONFIRM',
  PHONE = 'PHONE',
  EMAIL = 'EMAIL',
}

export const enum RESULT_TYPE {
  ERROR = 'ERROR',
  SUCCESSFULLY = 'SUCCESSFULLY',
}

export const INPUT_WIDTH = '200px';

export const paths = {
  email: pathGenerator<{
    email: string;
    captchaId: string;
    captchaText: string;
  }>(),
  phone: pathGenerator<{
    phoneNumber: string;
    captchaId: string;
    captchaText: string;
  }>(),
  confirm: pathGenerator<{ code: string }>(),
};
