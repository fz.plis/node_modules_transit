import type { UPDATE_CONTACT_INFO_TYPE } from './constants';

/**
 * Свойства формы.
 */
export interface IFormProps {
  /**
   * Телефон.
   */
  phone: string;
  /**
   * Email.
   */
  email: string;
  /**
   * Код.
   */
  code: string;
}

/**
 * Свойства обновления данных пользователя.
 */
export interface IUpdateContactProviderProps {
  /**
   * Метод закрытия диалогового окна.
   *
   * @param isConfirmed Признак подтверждения данных.
   */
  onClose(isConfirmed?: boolean): void;
  /**
   * Тип обновления.
   */
  updateType?: UPDATE_CONTACT_INFO_TYPE;
  /**
   * Не показывать результирующий шаг.
   */
  disableResultStep?: boolean;
}
