import { DOCUMENT_TYPE_CODE } from '../interfaces';
import { locale } from '../localization';

export const DOCUMENT_TYPE_CODE_LABELS: Record<DOCUMENT_TYPE_CODE, string> = {
  get [DOCUMENT_TYPE_CODE.ACC_OPEN_FIRST_REQ]() {
    return locale.requestType.regdoc;
  },
  get [DOCUMENT_TYPE_CODE.ACC_OPEN_SECOND_REQ]() {
    return locale.requestType.booking;
  },
  get [DOCUMENT_TYPE_CODE.ACC_RESERV_REQ]() {
    return locale.requestType.reserve;
  },
  get [DOCUMENT_TYPE_CODE.OFFER_RKO_ACCEPTANCE_REQ]() {
    return locale.requestType.offer;
  },
  get [DOCUMENT_TYPE_CODE.OFFER_JOIN_GPB_BO1_REQ]() {
    return locale.requestType.constitution;
  },
  get [DOCUMENT_TYPE_CODE.ADD_BLOCK_USER_GPBBO]() {
    return locale.requestType.constitution2new;
  },
  get [DOCUMENT_TYPE_CODE.OFFER_JOIN_GPB_BO2_REQ]() {
    return locale.requestType.constitution2;
  },
  get [DOCUMENT_TYPE_CODE.OFFER_JOIN_CCERT]() {
    return locale.requestType.ccert;
  },
  get [DOCUMENT_TYPE_CODE.FREE_FORMAT_DOC]() {
    return locale.requestType.free;
  },
  get [DOCUMENT_TYPE_CODE.OFFER_JOIN_PO_UVED]() {
    return locale.requestType.uved;
  },
  get [DOCUMENT_TYPE_CODE.ADD_BLOCK_USER_UVED]() {
    return locale.requestType.userUved;
  },
  get [DOCUMENT_TYPE_CODE.ACC_RKO_CERVICE_PMT_ADD]() {
    return locale.requestType.rko;
  },
  get [DOCUMENT_TYPE_CODE.OFFER_GEN_CERT]() {
    return locale.requestType.offerGenCert;
  },
  get [DOCUMENT_TYPE_CODE.ACC_OPEN_SECOND_CONTRACT_REQ]() {
    return locale.requestType.newBooking;
  },
  get [DOCUMENT_TYPE_CODE.OFFER_JOIN_PO_MP]() {
    return locale.requestType.lkmp;
  },
  get [DOCUMENT_TYPE_CODE.ADD_BLOCK_USER_MP]() {
    return locale.requestType.lkmpAccess;
  },
  get [DOCUMENT_TYPE_CODE.CAR_LEASING]() {
    return locale.requestType.carLease;
  },
  get [DOCUMENT_TYPE_CODE.CORP_LEASING]() {
    return locale.requestType.corpLease;
  },
  get [DOCUMENT_TYPE_CODE.GPB_DOG_ACC_OPEN_AUTH_CHECK]() {
    return locale.requestType.checkPriviligeO2;
  },
  get [DOCUMENT_TYPE_CODE.GPB_ACC_OPEN_AUTH_CHECK]() {
    return locale.requestType.checkPriviligeOpen;
  },
  get [DOCUMENT_TYPE_CODE.REQUEST_FOR_ACCOUNT_INFO]() {
    return locale.requestType.requestForAccountInfo;
  },
  get [DOCUMENT_TYPE_CODE.NULL_PRODUCT]() {
    return ''; // локаль не требуется
  },
};
