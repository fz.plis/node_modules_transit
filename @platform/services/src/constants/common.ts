import type { IOption } from '@platform/ui';
import { locale } from '../localization';
import { USER_TYPE_LABELS } from './auth';

export const SMS_CODE_LIFETIME = 120;
export const DATE_FORMAT = 'DD.MM.YYYY';
export const DATE_TIME_FORMAT = 'DD.MM.YYYY HH:mm:ss';
export const DATE_TIME_FORMAT_WITHOUT_SEC = 'DD.MM.YYYY HH:mm';
export const FILE_EXTENSIONS = {
  HTML: 'html',
  PDF: 'pdf',
  ZIP: 'zip',
  XLS: 'xls',
  CSV: 'csv',
  RTF: 'rtf',
};

/**
 * Типы внешних систем.
 */
export enum SYSTEM_TYPE {
  /**
   * ЭТП.
   */
  ETP = 'ETP_GPB',
}

/**
 * Типы согласий.
 */
export enum CONSENT_TYPE {
  /**
   * Согласие о предоставлении персональных данных.
   */
  CONSENT_OF_PERSONAL_DATA = 'CONSENT_OF_PERSONAL_DATA',
}

/** Тип сертификата, КЭП/НЭП. */
export enum CERTIFICATE_TYPE {
  /** КЭП — Квалифицированная электронная подпись. */
  QUALIFIED_SIGN = 'QUALIFIED_SIGN',
  /** НЭП — Неквалифицированная электронная подпись. */
  UNQUALIFIED_SIGN = 'UNQUALIFIED_SIGN',
}

/**
 * Продукты ГПБ БО.
 */
export enum GPB_PRODUCTS {
  /**
   * Соглашение о присоединении к ГПБ БО.
   */
  CONSTITUTION = 'CONSTITUTION',
  /**
   * Соглашение о подключении сервиса внешнеэкономической деятельности.
   */
  LK_UVED = 'LK_UVED',
  /**
   * Соглашение о подключении сервиса материального пулинга.
   */
  LK_MP = 'LK_MP',
}

/**
 * Наименование продуктов ГПБ.
 */
export const GPB_PRODUCTS_LABELS: Record<GPB_PRODUCTS, string> = {
  get [GPB_PRODUCTS.CONSTITUTION]() {
    return locale.products.constitution;
  },
  get [GPB_PRODUCTS.LK_MP]() {
    return locale.products.lkMp;
  },
  get [GPB_PRODUCTS.LK_UVED]() {
    return locale.products.lkUved;
  },
};

/**
 * Список продуктов ГПБ.
 */
export const GPB_PRODUCTS_OPTS: IOption[] = Object.values(GPB_PRODUCTS).map(item => ({
  get label() {
    return GPB_PRODUCTS_LABELS[item];
  },
  value: item,
}));

/**
 * Локализованные типы пользователя.
 */
export const HISTORY_DIALOG_USER_TYPE = {
  ...USER_TYPE_LABELS,
  get BANK() {
    return locale.historyDialog.bankUserType;
  },
  get TECHNICAL() {
    return locale.historyDialog.technicalUserType;
  },
};

/**
 * Локализованные подсказки для типов пользователя.
 */
export const HISTORY_DIALOG_USER_TYPE_TOOLTIPS = {
  get BANK() {
    return locale.historyDialog.hint.userType.bank;
  },
  get CLIENT() {
    return locale.historyDialog.hint.userType.client;
  },
  get TECHNICAL() {
    return locale.historyDialog.hint.userType.technical;
  },
};

/**
 * Доступные заявки системы.
 */
export enum REQUEST_FUNCTION {
  /**
   * [О2+] Клиент: Функция просмотра списка заявок.
   */
  O2_VIEW = 'O2_VIEW',
  /**
   * [РКО] Клиент: Функция просмотра списка заявок.
   */
  RKO_VIEW = 'RKO_VIEW',
  /**
   * [АО_РКО] Клиент: Функция просмотра списка заявок.
   */
  AO_RKO_VIEW = 'AO_RKO_VIEW',
  /**
   * [Конст] Клиент: Функция просмотра списка заявок.
   */
  CONSTITUTION_VIEW = 'CONSTITUTION_VIEW',
  /**
   * [Конст_З2] Клиент: Функция просмотра списка заявок.
   */
  CONSTITUTION_32_VIEW = 'CONSTITUTION_VIEW',
  /**
   * [АО_УВЭД_З1] Клиент: Функция просмотра списка заявок.
   */
  AO_UVED_1_VIEW = 'AO_UVED_1_VIEW',
  /**
   * [АО_УВЭД_З2] Клиент: Функция просмотра списка заявок.
   */
  AO_UVED_2_VIEW = 'AO_UVED_2_VIEW',
  /**
   * [АО_МП_З1] Клиент: Функция просмотра списка заявок.
   */
  AO_MP_1_VIEW = 'AO_MP_1_VIEW',
  /**
   * [АО_МП_З2] Клиент: Функция просмотра списка заявок.
   */
  AO_MP_2_VIEW = 'AO_MP_2_VIEW',
  /**
   * [ДСФ_ЭКО] Клиент: Функция просмотра списка документов в банк.
   */
  DSF_BANK_VIEW = 'DSF_BANK_VIEW',
  /**
   * [ДСФ_ЭКО] Клиент: Функция просмотра списка документов из Банка.
   */
  DSF_CLIENT_VIEW = 'DSF_CLIENT_VIEW',
  /**
   * [ФНС] Клиент: Функция просмотра списка налоговых задолженностей Клиента.
   */
  FNS_VIEW = 'FNS_VIEW',
  /**
   * [Справки] Клиент: Функция просмотра Журнала "Справки".
   */
  REFERENCE_VIEW = 'REFERENCE_VIEW',
}
