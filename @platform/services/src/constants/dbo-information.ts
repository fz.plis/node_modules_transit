/** Ключ в localeStorage для управления показом диалога. */
export const DONT_SHOW_DBO_INFORMATION_STORAGE_KEY = 'DBO_INFORMATION_MODAL';
/** E-mail для обратной связи. */
export const FEEDBACK_EMAIL = 'dbo@gazprombank.ru';
/** Телефон обратной связи. */
export const FEEDBACK_PHONE = '8 (800) 100-11-89';
