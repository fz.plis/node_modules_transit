/**
 * Идентификаторы пунктов меню, имещющих саб-меню.
 * Необходимы для возможности раскрыть нужное саб-меню из прикладного стрима.
 * Должны совпадать с id в конфигах resources/app.config.*.json.
 */
export const SIDEBAR_ITEMS_CONFIG = {
  PAYMENTS: 'menuService.payments',
  TREASURY: 'menuService.treasury',
  PRODUCTS: 'menuService.products',
  CLAIMS: 'menuService.claims',
};
