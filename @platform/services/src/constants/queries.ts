export const PLATFORM_QUERY_NAMES = {
  OPEN_ID_CONFIGURATION: '@platform/open-id-configuration',
  APP_CONFIG: '@platform/app-config',
  USER_INFO: '@platform/user-info',
  USER_AUTHORITIES: '@platform/user-authorities',
};
