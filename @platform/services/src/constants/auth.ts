import { AUTH_ERROR, ERROR } from '../interfaces';
import { locale } from '../localization';

/**
 * Используется для реста authorize, уникальный ключ нашего приложения.
 */
export const CLIENT_ID = process.env.CLIENT_ID || '';

export const AUTH_REQUEST_CONFIG = {
  disableTokenHeader: true,
  method: 'POST' as 'POST',
};

/**
 * Маппинг кодов ошибок авторизации на общие коды.
 */
export const AUTH_ERROR_MAP: Record<string, ERROR> = {
  [AUTH_ERROR.LOGIN_REQUIDER]: ERROR.LOGIN_REQUIRED,
  [AUTH_ERROR.APPROVE_REQUESTED]: ERROR.APPROVE_REQUESTED,
  [AUTH_ERROR.MFA_REQUIRED]: ERROR.MFA_REQUIRED,
  [AUTH_ERROR.MFA_INVALID_CREDENTIALS]: ERROR.MFA_INVALID_CREDENTIALS,
  [AUTH_ERROR.INVALID_CREDENTIALS]: ERROR.INVALID_CREDENTIALS,
  [AUTH_ERROR.TOKEN_EXPIRED]: ERROR.MFA_TOKEN_EXPIRED,
  [AUTH_ERROR.TOKEN_ATTEMPS_EXHAUSTED]: ERROR.MFA_TOKEN_ATTEMPS_EXHAUSTED,
  [AUTH_ERROR.USER_BLOCKED]: ERROR.USER_BLOCKED,
  [AUTH_ERROR.MFA_USER_CERTIFICATES_NOT_AVAILABLE]: ERROR.CRYPTO_CERTS_NOT_FOUND,
  [AUTH_ERROR.MFA_ALREADY_HAS_ACTIVE_CHALLENGE]: ERROR.SMS_TIMEOUT_NOT_EXPIRED,
  [AUTH_ERROR.MFA_INVALID_TOKEN_VALUE]: ERROR.MFA_INVALID_TOKEN_VALUE,
  [AUTH_ERROR.MFA_TOTP_USER_NOT_FOUND]: ERROR.MFA_TOTP_USER_NOT_FOUND,
  [AUTH_ERROR.MFA_CERTIFICATE_SIGN_NOT_VALID]: ERROR.CRYPTO_INVALID_SIGN_CERTIFICATE,
};

// Имена полей из enum'a SEGMENTS из interfaces
export const SEGMENTS_LABELS = {
  get CAS_ACC_OPEN_SECOND_REQ() {
    return locale.segments.cas_acc_open_second_req;
  },
  get CAS_OFFER_RKO_ACCEPTANCE_REQ() {
    return locale.segments.cas_offer_rko_acceptance_req;
  },
  get CAS_OFFER_JOIN_GPB_BO1_REQ() {
    return locale.segments.cas_offer_join_gpb_bo1_req;
  },
  get CAS_OFFER_JOIN_GPB_BO2_REQ() {
    return locale.segments.cas_offer_join_gpb_bo2_req;
  },
  get CAS_FREE_FORMAT_DOC() {
    return locale.segments.cas_free_format_doc;
  },
  get CAS_DBO_BSS_FRAME() {
    return locale.segments.cas_dbo_bss_frame;
  },
  get CAS_SWITCHING_TO_DBO_BSS() {
    return locale.segments.cas_switching_to_dbo_bss;
  },
  get CAS_OFFER_JOIN_PO_UVED() {
    return locale.segments.cas_offer_join_po_uved;
  },
  get CAS_SWITCHING_TO_UVED_PROCESS() {
    return locale.segments.cas_switching_to_uved_process;
  },
  get CAS_SWITCHING_TO_UVED_VIEW() {
    return locale.segments.cas_switching_to_uved_view;
  },
};

/**
 * Локализованные типы пользователя.
 */
export const USER_TYPE_LABELS = {
  get BANK() {
    return locale.userType.bank;
  },
  get CLIENT() {
    return locale.userType.client;
  },
  get TECHNICAL() {
    return locale.userType.technical;
  },
};
