import { VALIDATE_SIGN_ERRORS } from '../interfaces';
import { locale } from '../localization';

/**
 * Локализованные ошибки проверки подписи.
 */
export const VALIDATE_SIGN_ERROR_LABELS = {
  /**
   * Не найдена информация о подписанте или сертификате.
   */
  get [VALIDATE_SIGN_ERRORS.CANT_RETRIEVE_INFO_ABOUT_SIGNER_OR_CERTIFICATE]() {
    return locale.crypto.error.cantRetrieveInfoAboutSignerOrCertificate;
  },
};
