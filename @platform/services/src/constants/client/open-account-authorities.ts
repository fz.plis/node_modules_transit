/**
 * Привилегии на отрытие дополнительного счета.
 */
export const OPEN_ACCOUNT_AUTHORITIES = {
  /** Функция создания заявки. */
  CREATE: 'ACC_OPEN_CONTRACT.CREATE',
};
