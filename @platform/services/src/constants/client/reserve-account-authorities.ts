/**
 * Привилегии для резервирования счёта.
 */
export const RESERVE_ACCOUNT_AUTHORITIES = {
  /** Функция создания заявки. */
  CREATE: 'RESERV_REQUEST_GET_NEW',
};
