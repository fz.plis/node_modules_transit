/**
 * Привилегии на открытие первого счёта.
 */
export const REG_DOC_AUTHORITIES = {
  /** Функция создания заявки. */
  CREATE: 'DOC_PACK.CREATE',
};
