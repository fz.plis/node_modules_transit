export const COMMON_STREAM_URL = {
  MAINPAGE: '/mainpage',
  ACCOUNTS: '/accounts',
  RESERVE_ACCOUNT: '/accounts/reserve',
  OPEN_ACCOUNTS: '/accounts/open',
  NEW_OPEN_ACCOUNT: '/open-account',
  REGDOC: '/regdoc',
  PROFILE: '/profile',
  OFFER_LIST: '/offer/list',
  REDIRECT_BSS: '/redirect/bss',
  CONSTITUTION: '/constitution',
  LETTERS: '/dsf',
  NOTIFICATIONS: '/notifications',
  RKO_SERVICE_MANAGEMENT: '/rko-service-management',
  DEBTS: '/debt',
  /**
   * ЛК Увед.
   */
  UVED: '/uved',
  /**
   * Предоставление доступа сотдруднику к ЛК УВЕД.
   */
  UVED_ACCESS: '/uved-access',
  /**
   * Материальный пулинг.
   */
  LK_MP: '/mp',
  /**
   * Предоставление доступа сотдруднику к Материальному пулингу.
   */
  LK_MP_ACCESS: '/mp-access',
  /**
   * Сервис генерации сертификатов НЭП.
   */
  ESIGN: '/esign',
  /** Конституция 32, выдача УЛ и мегадоверенности. */
  CONSTITUTION_ACCESS: '/constitution-access',
  /** Конституция З2, ЭФ предоставление доступа. */
  CONSTITUTION_ACCESS_FORM: '/constitution-access/access',
  /** Конституция З2, ЭФ получение доверенности. */
  CONSTITUTION_ISSUE_ATTORNEY: '/constitution-access/attorney',
  /** Справки. */
  INQUIRY: '/inquiry',
};
