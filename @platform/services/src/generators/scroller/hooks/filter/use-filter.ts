import { useState, useCallback } from 'react';
import { getDefaultValue, useStorageSync, getHookValue, FILTER_TYPE } from '../utils';
import type { IFilterField } from './interface';
import { getFilterDefaultValues, getTags, mapDataToFilter } from './utils';

interface IFilter {
  fields: Record<string, IFilterField>;
  labels: Record<string, string>;

  /**
   * Ключ, по которому будем доставать данные из хранилища. Если не передан, то
   * вместо хранилища (localstorage) будет использован обычный React.state.
   * */
  storageKey?: string;

  /** Игнорировать содержимое хранилища при инициализации и форсировать инициализацию из дефолтных значений конфига. */
  forceFromDefault?: boolean;
}

const filterType = FILTER_TYPE.FILTER;

export const useFilter = ({ fields, labels, storageKey, forceFromDefault }: IFilter) => {
  const [values, setValues] = useState(() => {
    const storageSettings = storageKey
      ? {
          storageKey,
          filterType,
          forceInitFromConfigValue: forceFromDefault,
        }
      : undefined;

    return getDefaultValue(getFilterDefaultValues(fields), storageSettings);
  });

  const useStorageSyncConfig = storageKey ? { filterType, value: values, storageKey } : undefined;

  getHookValue(useStorageSync, useStorageSyncConfig);

  const [opened, setOpened] = useState(false);
  const handleOpenFilter = useCallback(() => {
    setOpened(true);
  }, [setOpened]);

  const handleClose = useCallback(() => {
    setOpened(false);
  }, [setOpened]);

  const handleOk = useCallback(
    formValues => {
      setOpened(false);
      setValues(formValues);
    },
    [setOpened, setValues]
  );

  const handleClear = useCallback(() => {
    handleOk({});
  }, [handleOk]);

  const handleRemoveTag = useCallback(
    (key: string) => {
      const { [key]: removedTag, ...newValues } = values;

      handleOk(newValues);
    },
    [handleOk, values]
  );

  return {
    filterValues: mapDataToFilter(values, fields),
    tagsPanel: {
      tags: getTags(values, labels),
      onClick: handleOpenFilter,
      onRemoveAllTags: handleClear,
      onRemoveTag: handleRemoveTag,
    },
    filterPanel: {
      values,
      opened,
      onClose: handleClose,
      onOk: handleOk,
      onClear: handleClear,
    },
  };
};
