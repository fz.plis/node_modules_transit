import type { conditions } from '@platform/core';

/**
 * Интерфейс поля фильтра.
 */
export interface IFilterField {
  /**
   * Наименование поля фильтра.
   */
  fieldName?: string;
  /**
   * Условие сравнения поля фильтра.
   */
  condition: keyof typeof conditions;
  /**
   * Начальное значение поля фильтра.
   */
  value: unknown;
  /**
   * Форматирование значения поля фильтра.
   *
   * @param value Значение поля фильтра.
   */
  formatter?(value: unknown): unknown;
}
