import { filters, getFilterDefaultValues, getTags, mapDataToFilter } from '../utils';

describe('getFilterDefaultValues', () => {
  it('дефолтным значением для фильтра должен быть объект {name: "Ivan": age: 23}', () => {
    const expectedValue = getFilterDefaultValues({
      age: filters.eq(23),
      name: filters.eq('Ivan'),
    });

    expect(expectedValue).toStrictEqual({ name: 'Ivan', age: 23 });
  });
});

describe('mapDataToFilter', () => {
  it("значение фильтра должно быть { name: { value: 'Ivan', condition: 'eq', fieldName: 'name' }", () => {
    const fields = {
      name: filters.eq('Ivan'),
      children: filters.in([]),
    };

    const expectedValue = mapDataToFilter({ name: 'Ivan', children: [] }, fields);

    expect(expectedValue).toStrictEqual({ name: { value: 'Ivan', condition: 'eq', fieldName: 'name' } });
  });

  it(`значение фильтра должно быть {"dateFrom":{"condition":"ge","value":"2018-12-10","fieldName":"date"},
  "dateTo":{"condition":"le","value":"2018-12-12","fieldName":"date"}}`, () => {
    const fields = {
      dateFrom: filters.ge(undefined, 'date'),
      dateTo: filters.le(undefined, 'date'),
    };

    const expectedValue = mapDataToFilter({ dateFrom: '2018-12-10', dateTo: '2018-12-12' }, fields);

    expect(expectedValue).toStrictEqual({
      dateFrom: { condition: 'ge', value: '2018-12-10', fieldName: 'date' },
      dateTo: { condition: 'le', value: '2018-12-12', fieldName: 'date' },
    });
  });
});

describe('getTags', () => {
  it('значение тэгов должно быть [{"value":"name","label":"Имя"}]', () => {
    const expectedValue = getTags({ name: 'Ivan', age: undefined }, { name: 'Имя', age: 'Возраст' });

    expect(expectedValue).toStrictEqual([{ value: 'name', label: 'Имя' }]);
  });
});
