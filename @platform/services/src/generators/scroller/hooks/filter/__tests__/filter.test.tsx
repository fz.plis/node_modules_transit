import React from 'react';
import { shallow } from 'enzyme';
import { Filter } from '../filter';

describe('Filter', () => {
  it('компонент должен отрисоваться', () => {
    const values = {
      name: 'Ivan',
      age: 23,
    };

    const onOk = jest.fn();
    const onClear = jest.fn();

    const component = shallow(<Filter opened values={values} onClear={onClear} onOk={onOk} />);

    expect(component).toMatchSnapshot();
  });
});
