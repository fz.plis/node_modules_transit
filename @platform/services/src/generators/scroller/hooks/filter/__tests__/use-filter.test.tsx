import { renderHook, act } from '@testing-library/react-hooks';
import { useFilter } from '../use-filter';
import { filters } from '../utils';

describe('useFilter', () => {
  it('фильтр должен корректно создаться', () => {
    const { result } = renderHook(() =>
      useFilter({
        fields: {
          name: filters.eq('Ivan'),
          age: filters.eq(23),
        },
        labels: { name: 'Имя', age: 'Возраст' },
      })
    );

    expect(result.current.filterPanel.values).toStrictEqual({ name: 'Ivan', age: 23 });
  });

  const renderUseFilter = () =>
    renderHook(() =>
      useFilter({
        fields: {
          name: filters.eq('Ivan'),
        },
        labels: { name: 'Имя', age: 'Возраст' },
      })
    );

  it('значени фильтра должно быть {"name":{"condition":"eq","value":"Petya","fieldName":"name"}}', () => {
    const { result } = renderUseFilter();

    act(() => {
      result.current.filterPanel.onOk({ name: 'Petya' });
    });

    expect(result.current.filterValues).toStrictEqual({ name: { condition: 'eq', value: 'Petya', fieldName: 'name' } });
  });

  it('фильтр должен быть пустым', () => {
    const { result } = renderUseFilter();

    act(() => {
      result.current.filterPanel.onClear();
    });

    expect(result.current.filterValues).toStrictEqual({});
  });
});
