import React from 'react';
import type { AnyObject } from 'final-form';
import type { IModalProps } from '@platform/ui';
import { Form, Filter as UIFilter } from '@platform/ui';
import { errorHandler } from '../../../../utils';

/**
 * Интерфейс компонента фильтры.
 */
export interface IFilterProps extends IModalProps {
  /**
   * Значение фильтра.
   */
  values: AnyObject;
  /**
   * Применить фильтры.
   *
   * @param values Значения фильтра.
   */
  onOk(values: AnyObject): void;
  /**
   * Очистить фильтр.
   */
  onClear(): void;
  /**
   * Функция определяющая доступность кнопки "Применить".
   *
   * @param values Значения фильтра.
   */
  okDisabled?(values: AnyObject): boolean;
  /**
   * Заголовок модального окна фильтра.
   */
  header?: string;
  /**
   * Функция, проверяющая форму на валидность.
   *
   * @param values Значения фильтра.
   */
  validate?(values: AnyObject): Promise<Record<string, unknown>> | Record<string, unknown>;
  /**
   * Поле для передачи стилей модального окна фильтра.
   */
  className?: string;
}

export const Filter: React.FC<IFilterProps> = ({
  header,
  values,
  opened,
  onOk,
  onClear,
  onClose,
  okDisabled,
  children,
  validate,
  className,
}) => (
  <Form
    initialValues={values}
    render={props => (
      <UIFilter
        className={className}
        header={header}
        okDisabled={!props.dirty || (validate && props.invalid) || okDisabled?.(props.values)}
        opened={opened}
        onClear={() => {
          onClear();
          window.requestAnimationFrame(() => {
            props.form.reset();
          });
        }}
        onClose={() => {
          onClose?.();
          window.requestAnimationFrame(() => {
            props.form.reset(values);
          });
        }}
        onOk={() => {
          props.form.submit()?.catch(errorHandler);
        }}
      >
        <form onSubmit={props.handleSubmit}>{children}</form>
      </UIFilter>
    )}
    validate={validate}
    onSubmit={formValues => onOk(formValues)}
  />
);

Filter.displayName = 'ScrollerFilter';
