import type { AnyObject } from 'final-form';
import type { conditions, IFilters } from '@platform/core';
import type { IOption } from '@platform/ui';
import type { IFilterField } from './interface';

const getSimpleFilter = (condition: keyof typeof conditions) => (
  value: unknown,
  fieldName?: string,
  formatter?: (value: unknown) => unknown
) => ({
  condition,
  value,
  fieldName,
  formatter,
});

const isEmptyValue = (value: unknown) =>
  value === undefined || value === null || (Array.isArray(value) && value.length === 0) || value === '';

export const filters = {
  eq: getSimpleFilter('eq'),
  contains: getSimpleFilter('contains'),
  in: getSimpleFilter('in'),
  ge: getSimpleFilter('ge'),
  le: getSimpleFilter('le'),
  notEq: getSimpleFilter('notEq'),
  and: getSimpleFilter('and'),
  or: getSimpleFilter('or'),
};

export const getFilterDefaultValues = (fields: Record<string, IFilterField>): Record<string, unknown> =>
  Object.entries(fields).reduce(
    (acc, [key, item]) => ({
      ...acc,
      [key]: item.value,
    }),
    {}
  );

export const mapDataToFilter = (values: AnyObject, filterFields: Record<string, IFilterField>) =>
  Object.keys(filterFields).reduce<IFilters>((acc, key) => {
    const filterField = filterFields[key];
    const { formatter, ...filterParams } = filterField;
    let formDataValue = values[key];

    if (isEmptyValue(formDataValue)) {
      return acc;
    }

    if (formatter) {
      formDataValue = formatter(formDataValue);
    }

    const dataField = {
      [key]: {
        ...filterParams,
        value: formDataValue,
        fieldName: filterField.fieldName || key,
      },
    };

    return {
      ...acc,
      ...dataField,
    };
  }, {});

export const getTags = (values: AnyObject, labels: Record<string, string>): IOption[] =>
  Object.entries(values).reduce<IOption[]>((acc, [key, value]) => {
    if (isEmptyValue(value)) {
      return acc;
    }

    return [
      ...acc,
      {
        value: key,
        label: labels[key],
      },
    ];
  }, []);
