import { useEffect, useMemo } from 'react';
import type { IFiltersStorageObject, IStorage } from '@platform/core';
import { createBaseStorage } from '@platform/core';
/**
 * Получить результат выполнения хука.
 *
 * @param hook Хук.
 * @param params Параметры хука.
 */
export const getHookValue = function <TVal, TResult>(hook: (...args: TVal[]) => TResult, params?: TVal) {
  if (!params || Object.keys(params).length === 0) {
    return null;
  }

  return hook(params);
};

export const sessionStorageWrapper: IStorage = {
  getItem: (key: string) => JSON.parse(sessionStorage.getItem(key) || 'null') as IFiltersStorageObject,
  removeItem: (key: string) => sessionStorage.removeItem(key),
  setItem: (key: string, value: unknown) => sessionStorage.setItem(key, JSON.stringify(value)),
};

const [scrollerSettings] = createBaseStorage(sessionStorageWrapper);

enum SCROLLER_FILTER {
  CATEGORY = 'CATEGORY',
  FILTER = 'FILTER',
  SORT = 'SORT',
}

export const FILTER_TYPE: Record<SCROLLER_FILTER, keyof IFiltersStorageObject> = {
  CATEGORY: 'category',
  FILTER: 'filters',
  SORT: 'sort',
};

export interface IUseStorageSync<K extends keyof IFiltersStorageObject, T extends IFiltersStorageObject[K]> {
  /** Дефолтное значение фильтра, если хранилище пустое. */
  value: T;
  /** Тип фильтра `filters` | `category` | `sort`. */
  filterType: K;
  /** Ключ, по которому будет происходить доступ к хранилищу. */
  storageKey: string;
}

/** Хук для синхронизации настроек скроллера в `SessionStorage`. */
export const useStorageSync = <K extends keyof IFiltersStorageObject, T extends IFiltersStorageObject[K]>(
  config: IUseStorageSync<K, T>
) => {
  const storage = useMemo(() => scrollerSettings(config.storageKey), [config.storageKey]);

  useEffect(() => {
    const storageSettings = storage.getItem();

    storage.setItem({ ...storageSettings, [config.filterType]: config.value });
  }, [config.filterType, config.value, storage]);
};

export interface IStorageSettings<K extends keyof IFiltersStorageObject> {
  /** Тип фильтра `filters` | `category` | `sort`. */
  filterType: K;
  /** Ключ, по которому будет происходить доступ к хранилищу. */
  storageKey: string;
  /** Флаг, который определяет нужно ли инициализировать значение из переданного аргумента, игнорируя хранилище. */
  forceInitFromConfigValue?: boolean;
}

/** Утилита для получения значений для инициализации фильтра. */
export const getDefaultValue = <K extends keyof IFiltersStorageObject, T extends IFiltersStorageObject[K]>(
  configValue: T,
  storageSettings?: IStorageSettings<K>
): T => {
  if (storageSettings) {
    if (storageSettings.forceInitFromConfigValue) {
      return configValue;
    }

    const storageValue = scrollerSettings(storageSettings.storageKey).getSection(storageSettings.filterType) as T;

    return storageValue || configValue;
  }

  return configValue;
};
