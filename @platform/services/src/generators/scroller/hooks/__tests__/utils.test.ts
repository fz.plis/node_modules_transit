import { getHookValue } from '../utils';

interface ITestHook {
  param: string;
}

const useTestHook = ({ param }: ITestHook) => ({
  test: param,
});

describe('getHookValue', () => {
  it('значение хука должно быть null', () => {
    const value = getHookValue(useTestHook, {} as ITestHook);

    expect(value).toBeNull();
  });

  it('значение хука должно быть { test: "1"}', () => {
    const value = getHookValue(useTestHook, { param: '1' } as ITestHook);

    expect(value).toStrictEqual({ test: '1' });
  });
});
