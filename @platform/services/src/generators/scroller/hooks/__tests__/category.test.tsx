import { renderHook, act } from '@testing-library/react-hooks';
import type { IMetaData } from '../../../../interfaces';
import { useCategory } from '../category';

const categories = [
  { name: 'ALL', desc: 'all', count: 5 },
  { name: 'UNREAD', desc: 'unread', count: 9 },
  { name: 'IMPORTANT', desc: 'important', count: 0 },
];

const fetcher = jest.fn((_: IMetaData): Promise<any> => new Promise(resolve => resolve(categories)));

const requestParams: IMetaData = {
  category: 'ALL',
  pageSize: 50,
  offset: 0,
};

const categoryTranslator = (key: string) => key;

describe('use-category', () => {
  it('render', async () => {
    const { result, waitForNextUpdate } = renderHook(() =>
      useCategory({ fetcher, defaultCategory: 'ALL', requestParams, locale: categoryTranslator })
    );

    await waitForNextUpdate();

    act(() => {
      void result.current.methods.reload();
    });

    await waitForNextUpdate();

    expect(result.current.categories).toStrictEqual(
      categories.map(cat => ({
        ...cat,
        label: categoryTranslator(cat.desc),
        value: cat.name,
      }))
    );
  });

  it('fetcher have been called with requestParams', () => {
    expect(fetcher).toHaveBeenCalledWith(requestParams);
  });

  it('reload', () => {
    expect(fetcher).toHaveBeenCalledTimes(2);
  });
});
