import { useState, useCallback, useEffect, useRef } from 'react';
import type { ICategory } from '@platform/ui';
import type { IMetaData } from '../../../interfaces';
import { errorHandler } from '../../../utils';
import { getDefaultValue, useStorageSync, getHookValue, FILTER_TYPE } from './utils';

const filterType = FILTER_TYPE.CATEGORY;

/**
 * Хук категорий.
 */
interface IUserCategory {
  /**
   * Метод получения списка категорий скроллера.
   *
   * @param metaData Параметры запроса списка категорий.
   */
  fetcher(metaData: IMetaData): Promise<Array<{ name: string; value: string; desc: string }>>;
  /**
   * Категория по-умолчанию.
   */
  defaultCategory: string;
  /**
   * Параметры запроса списка категорий.
   */
  requestParams: IMetaData;
  /**
   * Ресурс локализации.
   *
   * @param key Ключ.
   */
  locale(key: string): string;

  /**
   * Ключ, по которому будем доставать данные из хранилища. Если не передан, то
   * вместо хранилища (localstorage) будет использован React.useState.
   * */
  storageKey?: string;

  /** Игнорировать содержимое хранилища при инициализации и форсировать инициализацию из дефолтных значений конфига. */
  forceFromDefault?: boolean;

  /**
   * Отключает обработку сетевой ошибки.
   * По умолчанию `false`. При ошибке будет показывать модалку с сообщением ошибки.
   *
   * @default false
   */
  disableErrorHandling?: boolean;
}

/**
 * Хук для работы с категориями скроллера.
 *
 * @example
 * const myComp = () => {
 *  const { categories, category, methods } = useCategory({ fetcher, defaultCategory, requestParams, locale })
 *  return <ScrollerManager
 *    categories={categories}
 *    category={category}
 *    onCategoryChange={methods.setCategory}
 *  />
 * }
 */
export const useCategory = ({
  fetcher,
  defaultCategory,
  requestParams,
  locale,
  storageKey,
  forceFromDefault,
  disableErrorHandling = false,
}: IUserCategory) => {
  const [hasError, setHasError] = useState(false);
  const [error, setError] = useState();
  const [category, setCategory] = useState(() => {
    const storageSettings = storageKey
      ? {
          storageKey,
          filterType,
          forceInitFromConfigValue: forceFromDefault,
        }
      : undefined;

    return getDefaultValue(defaultCategory, storageSettings);
  });

  const useStorageSyncConfig = storageKey ? { filterType, value: category, storageKey } : undefined;

  getHookValue(useStorageSync, useStorageSyncConfig);

  const [categories, setCategories] = useState<ICategory[]>([]);
  const isCanceled = useRef(false);

  const load = useCallback(async () => {
    isCanceled.current = false;

    try {
      const data = await fetcher({ ...requestParams, category });

      if (!isCanceled.current) {
        setCategories(
          data.map(item => ({
            ...item,
            value: item.name,
            label: locale(item.desc),
          }))
        );
      }
    } catch (e) {
      if (!isCanceled.current) {
        setHasError(true);
        setError(e);

        if (!disableErrorHandling) {
          errorHandler()(e);
        }
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fetcher, isCanceled.current, JSON.stringify(requestParams), locale, category]);

  useEffect(() => {
    void load();

    return () => {
      isCanceled.current = true;
    };
  }, [load]);

  return {
    categories,
    category,
    methods: {
      setCategory: (newCategory: string) => {
        setCategory(newCategory);
      },
      reload: load,
    },
    state: {
      hasError,
      error,
    },
  };
};
