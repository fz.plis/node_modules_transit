import { useCallback, useEffect, useState } from 'react';
import type { IScrollerToolbarProps, ISortField } from '@platform/ui';
import { SORT_DIRECTION } from '@platform/ui';
import { FILTER_TYPE, useStorageSync, getHookValue, getDefaultValue } from './utils';

type UseToolbarSort = (config: {
  sortFields: ISortField[];
  byDefault?: {
    fieldName: string;
    direction: SORT_DIRECTION;
  };
  onChangeSort?(fieldName: string, direction: SORT_DIRECTION): void;

  /**
   * Ключ, по которому будем доставать данные из хранилища. Если не передан, то
   * вместо хранилища (localstorage) будет использован обычный React.state.
   * */
  storageKey?: string;
}) => [Pick<IScrollerToolbarProps, 'onChangeSort' | 'sortBy' | 'sortDirection' | 'sortFields'>, Record<string, SORT_DIRECTION>];

const filterType = FILTER_TYPE.SORT;

/**
 * Хук, возвращающий props сортировки, а также параметры для запроса списка.
 *
 * Если не передавать в хук значение сортировки по-умолчанию, то применится
 * сортировка по первому элементу sortFields в нисходящем направлении.
 *
 * @example
 * const myComp = () => {
 *  const [toolbarSortProps, sortRequestParam] = useToolbarSort({ sortFields: [{ name: 'date', label: 'Дата' }], onChangeSort: () => {} })
 *  useEffect(() => {
 *    fetcher({ sort: sortRequestParam }).then(({ data }) => console.log(data))
 *  }, [sortRequestParam])
 *  return <ScrollerToolbar {...toolbarSortProps} />
 * }
 */
export const useToolbarSort: UseToolbarSort = ({ sortFields, byDefault, onChangeSort, storageKey }) => {
  const [sort, setSort] = useState(() => {
    const defaultValue = byDefault
      ? {
          [byDefault.fieldName]: byDefault.direction,
        }
      : { [sortFields[0].name]: SORT_DIRECTION.DESC };

    const storageSettings = storageKey
      ? {
          storageKey,
          filterType,
        }
      : undefined;

    return getDefaultValue(defaultValue, storageSettings);
  });

  const useStorageSyncConfig = storageKey ? { filterType, value: sort, storageKey } : undefined;

  getHookValue(useStorageSync, useStorageSyncConfig);

  const sortEntries = Object.entries(sort);

  const handleChangeSort = useCallback(
    (fieldName, direction) => {
      if (onChangeSort) {
        onChangeSort(fieldName, direction);
      }

      setSort({ [fieldName]: direction });
    },
    [onChangeSort, setSort]
  );

  return [{ sortFields, sortBy: sortEntries[0][0], sortDirection: sortEntries[0][1], onChangeSort: handleChangeSort }, sort];
};

export interface IUseToolbarCheckbox<T> {
  rows: T[];
  onChangeCheckbox?(value: boolean): void;
}

/**
 * Хук, возвращающий props флажка панели инструментов.
 *
 * @example
 * const myComp = () => {
 *  const toolbarCheckboxProps = useToolbarCheckbox({ rows, selectedRows, onChangeCheckbox: () => {} })
 *  return <ScrollerToolbar {...toolbarCheckboxProps} />
 * }
 */
export const useToolbarCheckbox = <T>({ rows, onChangeCheckbox }: IUseToolbarCheckbox<T>) => {
  const [selectedRows, setSelectedRows] = useState<T[]>([]);
  const [checkboxValue, setCheckboxValue] = useState<boolean>(false);
  const [checkboxIndeterminate, setCheckboxIndeterminate] = useState<boolean>(false);

  const handleChangeCheckbox = useCallback(
    value => {
      if (onChangeCheckbox) {
        onChangeCheckbox(value);
      }

      setCheckboxValue(value);
      setCheckboxIndeterminate(false);

      if (value) {
        setSelectedRows(rows);
      } else {
        setSelectedRows([]);
      }
    },
    [onChangeCheckbox, setCheckboxValue, setCheckboxIndeterminate, rows]
  );

  useEffect(() => {
    const hasSelectedRows = rows?.length > 0 && selectedRows?.length > 0;

    if (hasSelectedRows && rows.length !== selectedRows.length) {
      setCheckboxValue(false);
      setCheckboxIndeterminate(true);
    } else if (hasSelectedRows && rows.length === selectedRows.length) {
      setCheckboxValue(true);
      setCheckboxIndeterminate(false);
    } else {
      setCheckboxValue(false);
      setCheckboxIndeterminate(false);
    }
  }, [rows, selectedRows, setCheckboxValue, setCheckboxIndeterminate]);

  const handleSelectedRows = useCallback(
    val => {
      setSelectedRows([...val]);

      if (val.length === 0) {
        setCheckboxValue(false);
        setCheckboxIndeterminate(false);
      } else if (val.length === rows.length) {
        setCheckboxValue(true);
        setCheckboxIndeterminate(false);
      } else {
        setCheckboxValue(false);
        setCheckboxIndeterminate(true);
      }
    },
    [setCheckboxValue, setCheckboxIndeterminate, setSelectedRows, rows]
  );

  return {
    checkboxValue,
    selectedRows,
    checkboxIndeterminate,
    onChangeCheckbox: handleChangeCheckbox,
    onChangeSelectedRows: handleSelectedRows,
  };
};
