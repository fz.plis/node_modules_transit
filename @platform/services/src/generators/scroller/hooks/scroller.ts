import { useCallback, useState, useEffect } from 'react';
import type { ICollectionResponse } from '@platform/core';
import type { IScroller } from '@platform/ui';
import type { IMetaData } from '../../../interfaces';
import { errorHandler } from '../../../utils';

interface IConfig<T> {
  fetcher(metaData: IMetaData): Promise<ICollectionResponse<T>>;
  requestParams?: Partial<IMetaData>;

  /**
   * Отключает обработку сетевой ошибки.
   * По умолчанию `false`. При ошибке будет показывать модалку с сообщением ошибки.
   *
   * @default false
   */
  disableErrorHandling?: boolean;
}

interface IUseScroller {
  methods: {
    reload(): Promise<void>;
  };
  props: Pick<IScroller, 'onIntersecting' | 'rows'>;
  /** Состояния скроллера. */
  state: {
    /** Состояние загрузки. */
    isLoading: boolean;
    /** Состояние, проверяющее наличие ошибок. Переходит в true, когда запрос вернул ошибку. */
    hasError: boolean;

    /** Содержит информацию об ошибке. */
    error: any;
  };
}

const defaultRequestParams = {
  pageSize: 50,
  offset: 0,
};

/**
 * Хук, возвращающий props скроллера.
 */
export const useScroller = <T = unknown>({ fetcher, requestParams, disableErrorHandling = false }: IConfig<T>): IUseScroller => {
  const [hasError, setHasError] = useState(false);
  const [error, setError] = useState();
  const [rows, setRows] = useState<T[]>([]);
  const [totalRows, setTotalRows] = useState<number>(0);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  /**
   * Переменная для защиты от срабатывания setState в момент извлечения компонента.
   */
  let isCanceled = false;

  /**
   * Функция, вызываемая на приближение к концу списка для дополнительной загрузки данных (loadMore).
   */
  const handleIntersecting = useCallback(async () => {
    if (!isLoading && totalRows !== rows.length) {
      setIsLoading(true);

      try {
        const { data, total } = await fetcher({ ...defaultRequestParams, offset: rows.length, ...requestParams });

        if (!isCanceled) {
          setRows([...rows, ...data]);
          setTotalRows(total);
        }
      } catch (e) {
        if (!isCanceled) {
          setHasError(true);
          setError(e);

          if (!disableErrorHandling) {
            errorHandler()(e);
          }
        }
      } finally {
        if (!isCanceled) {
          setIsLoading(false);
        }
      }
    }
  }, [isLoading, totalRows, rows, fetcher, requestParams, isCanceled, disableErrorHandling]);

  /**
   * Работа с remote.
   */
  const load = useCallback(
    async () => {
      setIsLoading(true);
      /**
       * Спиннер загрузки вставляется в конец списка
       * если при переключении категорий или изменении фильтров в скроллере больше записей,
       * чем помещается на экране, то спиннер уйдет за нижнюю границу экрана и его не будет видно
       * поэтому обнуляем массив записей, чтобы спиннер было видно.
       * При догрузке элементов через loadMore этого не требуется, потому, что мы видим конец списка и спиннер.
       */
      setRows([]);

      try {
        const { data, total } = await fetcher({ ...defaultRequestParams, ...requestParams });

        if (!isCanceled) {
          setRows(data);
          setTotalRows(total);
        }
      } catch (e) {
        setRows([]);
        setTotalRows(0);

        if (!isCanceled) {
          setHasError(true);
          setError(e);

          if (!disableErrorHandling) {
            errorHandler()(e);
          }
        }
      } finally {
        if (!isCanceled) {
          setIsLoading(false);
        }
      }
    },
    /**
     * Из-за того, что requestParams — это объект, в котором
     * мы не знаем какой вид будет принимать requestParams.filters
     * самый быстрый и безопасный способ подписаться на обновления такого объекта
     * JSON.stringify, так как строка гарантирует иммутабельность данных.
     */
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [fetcher, JSON.stringify(requestParams), setIsLoading, setRows, setTotalRows]
  );

  useEffect(() => {
    void load();

    return () => {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      isCanceled = true;
    };
  }, [load]);

  return {
    methods: { reload: load },
    props: { rows, onIntersecting: handleIntersecting },
    state: { isLoading, hasError, error },
  };
};
