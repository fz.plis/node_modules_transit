import React, { useCallback } from 'react';
import cn from 'classnames';
import { Typography, Pattern, Box, Horizon, SIZE, LimitWidth, Separator, Adjust, ServiceIcons, Gap } from '@platform/ui';
import type { IColumnHeader } from './interface';
import css from './styles.scss';

export interface IColumnHeaders {
  headers: IColumnHeader[];
  gap?: SIZE;
  /** Признак доступности кнопки Раскрыть всё. */
  withExpandRows?: boolean;
  /** Начальное состояние кнопки Раскрыть всё. */
  expandedAll?: boolean;
  /** Обработчик клика по кнопке Раскрыть всё. */
  setExpandedAll?(expanded: boolean): void;
}

export const ColumnHeaders: React.FC<IColumnHeaders> = ({ headers, gap = SIZE.MD, withExpandRows, expandedAll, setExpandedAll }) => {
  const Arrow = expandedAll ? ServiceIcons.DoubleChevronUp : ServiceIcons.DoubleChevronDown;
  const toggleExpanded = useCallback(() => setExpandedAll!(!expandedAll), [expandedAll, setExpandedAll]);

  const content = (
    <Box className={cn(Adjust.getPadClass(['SM', undefined]), css.row)} fill="BASE">
      <Pattern gap={gap}>
        {headers.map((header, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <Pattern.Span key={i} size={header.size}>
            <Horizon>
              {i === 0 && <Box style={{ width: '48px' }} />}
              <Typography.TextBold>{header.label}</Typography.TextBold>
              <Horizon.Spacer />
              {i !== headers.length - 1 && <Box border={['FAINT', 'MD']} className={css.headerSeparator} />}
            </Horizon>
          </Pattern.Span>
        ))}
      </Pattern>
    </Box>
  );

  return (
    <LimitWidth size={LimitWidth.SIZE.AUTO}>
      {withExpandRows ? (
        <Horizon className={css.columnHeaders}>
          <Box clickable className={css.expandIcon} onClick={toggleExpanded}>
            {<Arrow fill={'FAINT'} scale={'MD'} />}
          </Box>
          <Gap.XS />
          {content}
        </Horizon>
      ) : (
        content
      )}
      <Separator />
    </LimitWidth>
  );
};
