import React, { createContext } from 'react';
import type { IExecuter } from '@platform/core';

export const ScrollerExecuterContext = createContext<IExecuter<any>>({} as IExecuter<any>);

/** Хук, который можно использовать в дочерних элементах скроллера, чтобы получить доступ к executer-у. */
export const useScrollerExecuter = () => React.useContext(ScrollerExecuterContext);
