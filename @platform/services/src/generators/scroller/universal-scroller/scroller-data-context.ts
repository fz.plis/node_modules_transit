import { useContext, createContext } from 'react';

/**
 * Интерфейс данных скроллера.
 */
interface IScrollerData {
  /**
   * Текущая категория.
   */
  category?: string;
  /**
   * Текущее значение сортировки.
   */
  sort?: string;
  /**
   * Текущее значение фильтров.
   */
  filters?: Record<string, unknown>;
}

/**
 * Контекст текущей информации о скроллере.
 */
export const ScrollerDataContext = createContext<IScrollerData>({});

/**
 * Хук, который позволяет получить текущую информацию о скроллере.
 */
export const useScrollerData = () => useContext(ScrollerDataContext);
