import type React from 'react';
import type { WebIcon } from '@platform/ui';
import type { IActionWithAuth } from '../../../interfaces';
import type { useScrollerFeatures } from './scroller-features';

/** Экшн конфиг для кнопки с выпадающим списком экшнов. */
export interface IActionWithChildren {
  /** Иконка. */
  icon: WebIcon;

  /** Текст кнопки. */
  label: string;

  /** Название кнопки. */
  name: string;

  /** Массив вложенных экшнов. */
  children: IActionWithAuth[];
}

/** Общие параметры генератора скроллера. */
export interface ICommonScrollerGeneratorProps {
  /** Заголовок скроллера. */
  header?: string;

  /** Мэйнлэйаут, в который все будет обернуто, можно передать клиентский или админский. */
  mainLayout?: React.ComponentType;

  /** Видимость заголовка скроллера. Если true, то заголовок будет скрыт. */
  hideHeader?: boolean;

  /** Элемент, который будет показываться, если в таблице нет записей. */
  placeholder?: React.ReactElement<any, any>;

  /**
   * Метод смены категории.
   */
  handleCategoryChange?({
    filters,
    sort,
    category,
  }: {
    filters: UseScrollerFeatures['filtersPanelProps'];
    sort: UseScrollerFeatures['sortSettingsProps'];
    category: string;
  }): void;
}

export interface IToolbarActionsGetterConfig<TRow> {
  /** Активная категория. */
  category?: string;

  /** Массив выделенных строк. */
  selectedRows: TRow[];
}

/** Пропы для скроллера. */
export interface ScrollerPageProps {
  /**
   * Начальное значение категории, если передано этот проп, то оно будет испольоваться
   * вместо  значения из конфига для генератора или значения из хранилища.
   * */
  initialCategory?: string;

  /**
   * Начальное значение фильтров, если передано, то оно будет испольоваться
   * вместо  значения из конфига для генератора или значения из хранилища.
   * */
  initialFilters?: Record<string, unknown>;
}

/** Пропы для FilterNode. */
export interface IFilterButtonProps {
  /** Колбэк на нажатие кнопки. */
  onClick(): void;

  /** Количество примененных фильтров. */
  value: number;
}

export type UseScrollerFeatures = ReturnType<typeof useScrollerFeatures>;
