import type { IExecuter } from '@platform/core';
import { getActionButtons } from '@platform/core';
import type { IActionBarAction } from '@platform/ui';
import { getAuthItem } from '../../../core/auth';
import type { IActionWebInfo, IActionWithAuth } from '../../../interfaces';
import type { IGeneratorCategory, IGeneratorFilter } from '../interface';
import type { IActionWithChildren } from './interface';
// Ключи всех составляющих шапки.
type Parts = 'categories' | 'header' | 'toolbar' | 'topline';
// Конфиг для функции
type LayoutHeaderGetterConfig = Partial<Record<Parts, boolean>>;

// Конфиг значений, если модифицировали ключи, то ts подскажет что здесь нужны правки
const SIZE_MAP: Record<Parts, number> = {
  categories: 56,
  header: 70,
  toolbar: 66,
  topline: 64,
};

/** Функция, которая высчитывает высоту шапки скроллера, чтобы понять какая высота будет непосредственно у самого скроллера. */
export const getLayoutHeaderHeight = (config: LayoutHeaderGetterConfig) =>
  Object.entries(config).reduce((resultHeight, [key, value]) => resultHeight + SIZE_MAP[key as Parts] * Number(value), 0);

/** Тайпгард, который проверяет есть ли у экшна вложенные экшны или это обычный экшн. */
const isActionWithChildren = (action: IActionWithAuth | IActionWithChildren): action is IActionWithChildren =>
  (action as IActionWithChildren).children !== undefined;

/**
 * Функция, создаст кнопки тулбара из конфига (умеет обрабатывать вложенные экшны с children-ами).
 *
 * @throws Выбрасывает ошибку, если в конфиге встречаются дочерние экшены кроме как у первого элемента.
 * // FIXME: Исправить с помощью Typescript Spread/Rest Types.
 * */
export const getTransformedToolbarActions = <TRow, TExecuterContext>({
  actions,
  executer,
  selectedRows,
  actionsParams,
}: {
  actions: Array<IActionWithAuth | IActionWithChildren>;
  executer: IExecuter<TExecuterContext>;
  selectedRows: TRow[];
  actionsParams: unknown;
}): IActionBarAction[] => {
  if (actions.length === 0) {
    return [];
  }

  const getAvailableActions = getAuthItem('getAvailableActions');

  const result: IActionBarAction[] = [];

  const [actionWithPossibleChildren, ...restActions] = actions;
  const rowsWithParams = [selectedRows, actionsParams];

  // Первая кнопка "Создать" в списке может содержать вложенные экшны
  if (isActionWithChildren(actionWithPossibleChildren)) {
    // Если имеются вложенные экшны, трансформируем их отдельно, предварительно пропустив через валидатор
    const createButtonsList = getActionButtons(getAvailableActions(actionWithPossibleChildren.children), executer, rowsWithParams);

    // Конфигурируем первую кнопку "Создать" и добавляем к ней список вложенных экшнов.
    const createActonsList: IActionBarAction = {
      ...actionWithPossibleChildren,
      onClick: () => {},
      children: createButtonsList,
    };

    result.push(createActonsList);
  } else {
    result.push(...getActionButtons(getAvailableActions([actionWithPossibleChildren]), executer, rowsWithParams));
  }

  if (restActions.some(isActionWithChildren)) {
    throw new Error('children is available only for 1-st element');
  }

  result.push(
    ...getActionButtons<IActionWebInfo<any, any>>(getAvailableActions(restActions as IActionWithAuth[]), executer, rowsWithParams)
  );

  return result;
};

/**
 * Если передан defaultCategory, то он вернет config, заменив defaultCategory и добавит поле `ignoreStorageOnInit: true`,
 * иначе просто вернет config.
 * */
export const mergeCategoriesConfig = (config: IGeneratorCategory | undefined, defaultCategory?: string): IGeneratorCategory | undefined => {
  if (config) {
    return ({
      ...config,
      defaultCategory: defaultCategory || config.defaultCategory,
      forceFromDefault: Boolean(defaultCategory),
    } as unknown) as IGeneratorCategory;
  }
};

/**
 * Если передан initialFilters, то он вернет config,  заменив значения fields.value значениями из initialFilters
 * и добавит поле `ignoreStorageOnInit: true`, иначе просто вернет config.
 * */
export const mergeFiltersConfig = (config?: IGeneratorFilter, initialFilters?: Record<string, unknown>): IGeneratorFilter | undefined => {
  if (config) {
    // Проверяем initialFilters на пустоту
    const hasInitialFilters = initialFilters && Object.values(initialFilters).filter(val => val !== undefined).length > 0;

    if (hasInitialFilters) {
      // Если есть предустановленные значения фильтров, то создаем для этих полей отдельный конфиг
      const initialFilterFields = Object.entries(initialFilters!).reduce(
        (fields, [fieldName, initialValue]) => ({
          ...fields,
          [fieldName]: { ...config.fields[fieldName], value: initialValue },
        }),
        {}
      );

      return ({
        ...config,
        fields: {
          // Мержим оригинальный конфиг с конфигом для предустановленных фильтров
          ...config.fields,
          ...initialFilterFields,
        },
        forceFromDefault: true,
      } as unknown) as IGeneratorFilter;
    }

    return config;
  }
};
