import React from 'react';
import type { ICategoryTabsProps, ISortSettingsProps, IActionsBarProps, ICheckboxProps, IBreadcrumb } from '@platform/ui';
import { CategoryTabs, ScrollerManagerHeader, LimitWidth, Separator, Box, LoaderOverlay, FilterButton } from '@platform/ui';
import type { ISelectedDocumentsInfo } from '../../../components';
import { FatalErrorContent } from '../../../components';
import { COMMON_STREAM_URL } from '../../../constants';
import { useRedirect } from '../../../utils';
import { Filter as ScrollerFilter } from '../hooks';
import type { IFilterProps } from '../hooks/filter';
import type { IFilterButtonProps } from './interface';
import { ToolBar } from './toolbar';
import { getLayoutHeaderHeight } from './utils';

interface IScrollerPageLayout {
  /** Мэйнлэйаут, в который все будет обернуто, можно передать клиентский или админский. */
  mainLayout: React.ComponentType;

  /** Заголовок страницы. */
  pageTitle: string;

  /** Пропы для категорий. */
  categoryTabsProps?: ICategoryTabsProps;

  /** Пропы для чекбокса для управления выделением всех строк. */
  selectRowsCheckboxProps?: ICheckboxProps;

  /** Пропы для экшнбара. */
  actionsBarProps?: IActionsBarProps;

  /** Пропы для настроек сортировки. */
  sortSettingsProps?: ISortSettingsProps;

  /** Пропы для панели с фильтрами. */
  filtersPanelProps?: IFilterProps;

  /** Пропы для кнопки для открытия панели фильтров. */
  filterNodeProps?: IFilterButtonProps;

  /** Состояние загрузки данных. Если true, то будет показываться Loader. */
  isLoading: boolean;

  /** Информация о выбранных элементах. */
  selectedDocumentsInfo?: ISelectedDocumentsInfo;

  /** Хлебные крошки. */
  breadcrumbs?: IBreadcrumb[];

  /** Состояние заголовка скроллера. Если true, то заголовок будет скрыт. */
  hideHeader?: boolean;

  /** Высота скроллера. Если false, то высота скроллера будет во весь экран, даже если записей меньше. */
  compactHeight?: boolean;

  hasError?: boolean;
}

export const ScrollerPageLayout: React.FC<IScrollerPageLayout> = ({
  pageTitle: header,
  mainLayout,
  categoryTabsProps,
  filterNodeProps,
  filtersPanelProps,
  sortSettingsProps,
  selectRowsCheckboxProps,
  children,
  actionsBarProps,
  isLoading,
  selectedDocumentsInfo,
  breadcrumbs,
  hideHeader,
  compactHeight,
  hasError,
}) => {
  const hasToolbar = Boolean(selectRowsCheckboxProps || sortSettingsProps || actionsBarProps);

  // Если отсутствует прокидываемый лейаут, оборачиваем во фрагмент.
  const MainLayout = mainLayout || React.Fragment;

  const layoutHeaderHeight = getLayoutHeaderHeight({
    categories: Boolean(categoryTabsProps),
    toolbar: hasToolbar,
    topline: true,
    header: true,
  });

  const goHome = useRedirect(COMMON_STREAM_URL.MAINPAGE);

  if (hasError) {
    return (
      <MainLayout>
        <FatalErrorContent />
      </MainLayout>
    );
  }

  return (
    <MainLayout>
      {!hideHeader && (
        <>
          <ScrollerManagerHeader
            breadcrumbs={breadcrumbs}
            filterNode={filterNodeProps ? <FilterButton {...filterNodeProps} /> : undefined}
            header={header}
            onHomeClick={goHome}
          />
          <Separator />
        </>
      )}
      {isLoading ? (
        <LoaderOverlay opened={isLoading} />
      ) : (
        <>
          {categoryTabsProps && <CategoryTabs {...categoryTabsProps} />}
          {/* Убираем дефолтный паддинг, если лейаут равен реакт фрагменту. */}
          <LimitWidth removeDefaultPadding={mainLayout === React.Fragment} size={LimitWidth.SIZE.AUTO}>
            {hasToolbar && (
              <ToolBar
                actionsBarProps={actionsBarProps}
                checkBoxProps={selectRowsCheckboxProps}
                selectedDocumentsInfo={selectedDocumentsInfo}
                sortSettingsProps={sortSettingsProps}
              />
            )}
            <Box style={{ position: 'relative', ...(!compactHeight && { height: `calc(100vh - ${layoutHeaderHeight}px` }) }}>
              {children}
            </Box>
            {filtersPanelProps && <ScrollerFilter {...filtersPanelProps} />}
          </LimitWidth>
        </>
      )}
    </MainLayout>
  );
};
