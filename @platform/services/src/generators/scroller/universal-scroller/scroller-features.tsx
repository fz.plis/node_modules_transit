import React, { useCallback, useEffect, useRef } from 'react';
import type { ICollectionResponse, IExecuter, SORT_DIRECTION } from '@platform/core';
import { ACTION_ADDONS, applyMiddlewares, onSuccessMiddleware } from '@platform/core';
import type { IMetaData, IActionWithAuth } from '../../../interfaces';
import { errorHandler } from '../../../utils';
import { useFilter, useScroller, useToolbarCheckbox, useToolbarSort, useCategory, getHookValue } from '../hooks';
import type { IGeneratorCategory, IGeneratorFilter, IGeneratorSorting } from '../interface';
import type { IActionWithChildren, IToolbarActionsGetterConfig } from './interface';
import { getTransformedToolbarActions } from './utils';

type IToolbarActionsGetter = <TRow>(config: IToolbarActionsGetterConfig<TRow>) => Array<IActionWithAuth | IActionWithChildren>;

/**
 * Генератор скроллера.
 */
export interface IScrollerFeatures<TRow, TExecuterContext = unknown> {
  /**
   * Метод получения данных скроллера.
   *
   * @param metaData
   */
  fetcher(metaData: IMetaData): Promise<ICollectionResponse<TRow>>;

  /** Параметры категорий. */
  categories?: IGeneratorCategory;

  /** Параметры фильтров. */
  filters?: IGeneratorFilter;

  /** Параметры сортировки. */
  sorting?: IGeneratorSorting;

  /** Возможность выделения строк. */
  selectable?: boolean;

  /** Экшн экзекутор, через который будут исполняться действия скроллера и строк. */
  executer: IExecuter<TExecuterContext>;

  /** Действия для скроллера. Можно кидать как массив, так и геттер функцию. */
  toolbarActions?: Array<IActionWithAuth | IActionWithChildren> | IToolbarActionsGetter;

  /** Количество отображаемых действий.
   *
   * @default 3.
   */
  toolbarActionsVisibleCount?: number;

  /**
   * Ключ для доступа в хранилище. Этот параметр включает функцию синхронизации текущих значений
   * фильтров скроллера с хранилищем (localstorage).
   * */
  storageKey?: string;

  /**
   * Дополнительные параметры для действий со строками скроллера.
   * Будут переданы в дополнение к row в action.
   *
   * @example action: ({ done, fatal, addSucceeded }, { showLoader, hideLoader }) => async ([document], actionsParams) => ...
   */
  actionsParams?: unknown;
}

/** Хук, который отвечает за всю логику компонента скроллера,
 * возвращает пропы практически для всех элементов страницы со скроллером.
 */
export const useScrollerFeatures = <TRow, TExecuterContext>({
  fetcher,
  filters,
  sorting,
  categories,
  selectable = false,
  executer,
  toolbarActions,
  toolbarActionsVisibleCount = 3,
  storageKey,
  actionsParams,
}: IScrollerFeatures<TRow, TExecuterContext>) => {
  const filterResult = getHookValue(useFilter, filters ? { ...filters, storageKey } : undefined);
  const toolbarSortResult = getHookValue(useToolbarSort, sorting ? { ...sorting, storageKey } : undefined);
  const params = ({
    ...(toolbarSortResult ? { sort: toolbarSortResult[1] } : {}),
    filters: filterResult?.filterValues || {},
  } as unknown) as IMetaData;

  // Идентификатор того, обработана ли сетевая ошибка.
  const isNetworkErrorHandled = useRef(false);

  const categoryProps = getHookValue(
    useCategory,
    categories
      ? {
          ...categories,
          requestParams: params,
          storageKey,
          disableErrorHandling: true, // выключаем внутренний обработчик ошибок, т.к. мы будем обрабатывать их централизованно
        }
      : undefined
  );

  const { methods, state, props } = useScroller<TRow>({
    fetcher,
    disableErrorHandling: true, // выключаем внутренний обработчик ошибок, т.к. мы будем обрабатывать их централизованно
    requestParams: { ...params, category: categoryProps?.category },
  });

  // чтобы не показывать пользователю несколько модалок с ошибками
  // мы обрабатываем первую пойманную сетевую ошибку, остальные игнорируем
  useEffect(() => {
    const categoryError = categoryProps?.state.error;
    const scrollerError = state.error;
    const firstError = categoryError || scrollerError;

    // Если сетевых ошибок нет, то просто выходим.
    if (!firstError) {
      return;
    }

    // Если сетевая ошибка уже обработана, то просто выходим
    if (isNetworkErrorHandled.current) {
      return;
    }

    // Иначе обрабатываем сетевую ошибку и ставим флаг, что она обработана.
    errorHandler()(firstError);
    isNetworkErrorHandled.current = true;
  }, [categoryProps?.state.error, state.error]);

  const toolbarCheckboxResult = getHookValue(useToolbarCheckbox, selectable ? { rows: props.rows } : undefined);

  const [expandedRows, setExpandedRows] = React.useState<TRow[]>([]);

  const reloadScroller = useCallback(async () => {
    await categoryProps?.methods.reload();
    await methods.reload();
    toolbarCheckboxResult?.onChangeSelectedRows([]);
  }, [categoryProps, methods, toolbarCheckboxResult]);

  const handleCategoryChange = useCallback(
    (category: string) => {
      categoryProps?.methods.setCategory(category);
      toolbarCheckboxResult?.onChangeSelectedRows([]);
    },
    [categoryProps?.methods, toolbarCheckboxResult]
  );

  const handleSortChange = useCallback(
    (fieldName: string, order: SORT_DIRECTION) => {
      if (toolbarSortResult?.length) {
        toolbarSortResult[0].onChangeSort!(fieldName, order);
        toolbarCheckboxResult?.onChangeSelectedRows([]);
      }
    },
    [toolbarCheckboxResult, toolbarSortResult]
  );

  const executerWithSuccessMiddleware = React.useMemo(
    () =>
      applyMiddlewares<TExecuterContext>(
        onSuccessMiddleware(({ actionConfig }) => {
          const actionAddons: ACTION_ADDONS[] | undefined = actionConfig.addons;

          // Проверям на наличие аддонов и аддона DONT_RELOAD. Если есть - не перезагружаем скроллер.
          if (!actionAddons?.includes(ACTION_ADDONS.DONT_RELOAD)) {
            void reloadScroller();
          }
        })
      )(executer),
    [executer, reloadScroller]
  );

  const handledToolbarActions = React.useMemo(() => {
    if (toolbarActions) {
      let actions: Array<IActionWithAuth | IActionWithChildren> = [];

      if (typeof toolbarActions === 'function') {
        actions = toolbarActions({
          selectedRows: toolbarCheckboxResult?.selectedRows || [],
          category: categoryProps?.category || categories?.defaultCategory,
        });
      }

      if (Array.isArray(toolbarActions)) {
        actions = toolbarActions;
      }

      return getTransformedToolbarActions({
        selectedRows: toolbarCheckboxResult?.selectedRows || [],
        executer: executerWithSuccessMiddleware,
        actions,
        actionsParams,
      });
    }
  }, [
    toolbarActions,
    categoryProps?.category,
    toolbarCheckboxResult?.selectedRows,
    executerWithSuccessMiddleware,
    categories?.defaultCategory,
    actionsParams,
  ]);

  const FilterComponent = filters?.component;

  return {
    filtersPanelProps: filterResult
      ? { ...filterResult?.filterPanel, validate: filters?.validate, children: FilterComponent ? <FilterComponent /> : undefined }
      : undefined,
    filterNodeProps: filterResult
      ? {
          onClick: filterResult.tagsPanel.onClick,
          value: filterResult.tagsPanel.tags.length,
        }
      : undefined,
    sortSettingsProps: toolbarSortResult
      ? {
          direction: toolbarSortResult[0].sortDirection,
          sortBy: toolbarSortResult[0].sortBy,
          options: toolbarSortResult[0].sortFields!.map(({ label, name: value }) => ({ label, value })),
          onChange: handleSortChange,
        }
      : undefined,
    categoryTabsProps: categoryProps
      ? {
          categories: categoryProps.categories,
          category: categoryProps.category,
          onCategoryChange: handleCategoryChange,
          hasError: categoryProps.state.hasError,
        }
      : undefined,
    scrollerComponentProps: {
      ...state,
      ...props,
      reload: reloadScroller,
    },
    selectableRowsProps: toolbarCheckboxResult
      ? {
          selectedRows: toolbarCheckboxResult.selectedRows,
          onChangeSelectedRows: toolbarCheckboxResult.onChangeSelectedRows,
        }
      : undefined,
    expandableRowsProps: {
      expandedRows,
      onChangeExpandedRows: setExpandedRows,
    },
    selectRowsCheckboxProps: toolbarCheckboxResult
      ? {
          value: toolbarCheckboxResult.checkboxValue,
          onChange: toolbarCheckboxResult.onChangeCheckbox,
          indeterminate: toolbarCheckboxResult.checkboxIndeterminate,
        }
      : undefined,

    actionsBarProps:
      handledToolbarActions && handledToolbarActions.length > 0
        ? {
            actions: handledToolbarActions,
            visibleCount: toolbarActionsVisibleCount,
          }
        : undefined,
    executer: executerWithSuccessMiddleware,
    selectedDocumentsInfo: toolbarCheckboxResult
      ? {
          selected: toolbarCheckboxResult.selectedRows.length,
          total: props.rows.length,
        }
      : undefined,
  };
};
