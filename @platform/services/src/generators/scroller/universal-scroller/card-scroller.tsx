import React, { useMemo } from 'react';
import type { IRowTemplateProps } from '@platform/ui';
import { LimitWidth, Scroller, LayoutScrollComponent, ScrollerPlaceholder } from '@platform/ui';
import { ScrollerExecuterContext } from './context';
import type { ICommonScrollerGeneratorProps, ScrollerPageProps } from './interface';
import { ScrollerPageLayout } from './layout';
import { ScrollerDataContext } from './scroller-data-context';
import type { IScrollerFeatures } from './scroller-features';
import { useScrollerFeatures } from './scroller-features';
import styles from './styles.scss';
import { mergeCategoriesConfig, mergeFiltersConfig } from './utils';

type OnScroll = (scrollY: number) => void;

/** Пропы для скроллера с карточками. */
export interface ICardScrollerGeneratorProps<TRow, TExecuterContext>
  extends ICommonScrollerGeneratorProps,
    IScrollerFeatures<TRow, TExecuterContext> {
  card: React.ComponentType<Omit<IRowTemplateProps, 'row'> & { row: TRow }>;
}

/**
 * Получить скроллер карточек.
 *
 * @param props Свойства.
 * @param props.fetcher Метод получения данных скроллера.
 * @param props.categories Категории.
 * @param props.filters Фильтр.
 * @param props.sorting Сортировка.
 * @param props.selectable Можно выделять строки.
 * @param props.executer Экшн экзекутор, через который будут исполняться действия скроллера и строк.
 * @param props.header Заголовок скроллера.
 * @param props.actions Действия для скроллера в тулбаре (поддерживаются вложенные экшны с полем children).
 * @param props.actionsVisibleCount Количество отображаемых действий.
 * @param props.card Компонент карточки скроллера.
 * @param props.mainLayout Мэйнлэйаут, в который все будет обернуто, можно передать клиентский или админский.
 * @param props.placeholder Элемент, который будет показываться, если в таблице нет записей.
 * @param props.hideHeader Видимость заголовка скроллера.
 * @param props.removeDefaultPadding Флаг, при проставлении которого убирается дефолтный паддниг скроллера.
 * @param props.handleCategoryChange Метод смены категории.
 *
 * @example
  const rules = composeValidation(check('name').on(err.notEmpty()));
  const validateFunc = values => transformFormValidation(values, createValidator(rules, validators).validate)
  export const ScrollerPage = getCardScrollerPage({
    fetcher: periodicMoneyTransferService.getList,
    categories: {
      fetcher: periodicMoneyTransferService.getCounter,
      locale: getTranslator(LOCALIZATION_RESOURCE),
      defaultCategory: CATEGORY.ALL,
    },
    filters: {
      fields: FIELDS,
      labels: FILTER_FIELD_LABELS,
      component: Filter,
      validate: validateFunc,
    },
    sorting: {
      sortFields: SORT_FIELD_LABELS,
    },
    selectable: true,
    header: locale.scroller.bank.title,
    executer: getMoneyTransferExecuter(),
    actions: SCROLLER_ACTIONS,
    card: Card,
    handleCategoryChange: (params) => params.filters.onClear()
  })
 */
export const getCardScrollerPage = <TRow, TExecuterContext>({
  card,
  mainLayout = React.Fragment,
  hideHeader = false,
  header = '',
  categories,
  filters,
  placeholder = <ScrollerPlaceholder />,
  handleCategoryChange,
  ...scrollerFeaturesConfig
}: ICardScrollerGeneratorProps<TRow, TExecuterContext>) => {
  const ScrollerPage: React.FC<ScrollerPageProps> = ({ initialCategory, initialFilters }) => {
    const {
      categoryTabsProps,
      selectableRowsProps,
      filterNodeProps,
      sortSettingsProps,
      filtersPanelProps,
      scrollerComponentProps,
      selectRowsCheckboxProps,
      actionsBarProps,
      executer,
    } = useScrollerFeatures<TRow, TExecuterContext>({
      ...scrollerFeaturesConfig,
      categories: mergeCategoriesConfig(categories, initialCategory),
      filters: mergeFiltersConfig(filters, initialFilters),
    });

    // Реф, который хранит функцию, которая будет вызываться при скролле
    const onScroll = React.useRef<OnScroll>(() => {});

    // Функция для скроллера, которая перезапишет наш onScroll, чтобы подписаться на scrollEvent LayoutScrollComponent-а
    const subscribeToScrollY = React.useCallback((onScrollerScroll: OnScroll) => {
      onScroll.current = onScrollerScroll;

      const onOnmount = () => (onScroll.current = () => {});

      return onOnmount;
    }, []);

    const categoryProps = useMemo(() => {
      if (!categoryTabsProps) {
        return categoryTabsProps;
      }

      return {
        ...categoryTabsProps,
        onCategoryChange: (category: string) => {
          if (handleCategoryChange) {
            handleCategoryChange({
              filters: filtersPanelProps,
              sort: sortSettingsProps,
              category,
            });
          }

          if (categoryTabsProps) {
            categoryTabsProps.onCategoryChange(category);
          }
        },
      };
    }, [filtersPanelProps, sortSettingsProps, categoryTabsProps]);

    const hasError = scrollerComponentProps.hasError || categoryTabsProps?.hasError;

    return (
      <ScrollerDataContext.Provider
        value={{
          category: categoryTabsProps?.category,
          sort: sortSettingsProps?.sortBy,
          filters: filtersPanelProps?.values,
        }}
      >
        <ScrollerPageLayout
          actionsBarProps={actionsBarProps}
          categoryTabsProps={categoryProps}
          filterNodeProps={filterNodeProps}
          filtersPanelProps={filtersPanelProps}
          hasError={hasError}
          hideHeader={hideHeader}
          isLoading={categoryTabsProps?.categories?.length === 0}
          mainLayout={mainLayout}
          pageTitle={header}
          selectRowsCheckboxProps={selectRowsCheckboxProps}
          sortSettingsProps={sortSettingsProps}
        >
          <ScrollerExecuterContext.Provider value={executer}>
            <LayoutScrollComponent
              onScroll={e => {
                onScroll.current((e.target as Element).scrollTop);
              }}
            >
              <LimitWidth size={LimitWidth.SIZE.MD}>
                <Scroller
                  className={styles.itemsContainer}
                  rowTemplate={card}
                  subscribeToScrollY={subscribeToScrollY}
                  {...scrollerComponentProps}
                  {...selectableRowsProps}
                  placeholder={placeholder}
                />
              </LimitWidth>
            </LayoutScrollComponent>
          </ScrollerExecuterContext.Provider>
        </ScrollerPageLayout>
      </ScrollerDataContext.Provider>
    );
  };

  ScrollerPage.displayName = 'ScrollerPage';

  return ScrollerPage;
};
