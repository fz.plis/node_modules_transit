import React from 'react';
import type { ISortSettingsProps, IActionsBarProps, ICheckboxProps } from '@platform/ui';
import { Gap, Horizon, SortSettings, ActionsBar, Checkbox, Box, Adjust, ROLE } from '@platform/ui';
import type { ISelectedDocumentsInfo } from '../../../components/selected-documents-info';
import { SelectedDocumentsInfo } from '../../../components/selected-documents-info';

export interface IToolbarProps {
  checkBoxProps?: ICheckboxProps;
  actionsBarProps?: IActionsBarProps;
  sortSettingsProps?: ISortSettingsProps;
  selectedDocumentsInfo?: ISelectedDocumentsInfo;
}

export const ToolBar: React.FC<IToolbarProps> = ({ actionsBarProps, checkBoxProps, sortSettingsProps, selectedDocumentsInfo }) => (
  <Box.Base className={Adjust.getPadClass(['XL', undefined, 'MD', undefined])} role={ROLE.TOOLBAR}>
    <Horizon>
      {checkBoxProps && (
        <>
          <Checkbox {...checkBoxProps} />
          <Gap.X3L inline />
        </>
      )}
      {actionsBarProps && <ActionsBar {...actionsBarProps} />}

      {selectedDocumentsInfo && (
        <>
          <Gap.XL />
          <SelectedDocumentsInfo {...selectedDocumentsInfo} />
        </>
      )}

      <Horizon.Spacer />

      {sortSettingsProps && <SortSettings {...sortSettingsProps} />}
    </Horizon>
  </Box.Base>
);

ToolBar.displayName = 'ToolBar';
