import { to } from '@platform/core';
import type { ISetting } from '../interfaces';
import { request } from '../utils/request';

/**
 * Интерфейс доступности сервисов.
 */
type StreamHealth = Record<string, boolean>;

let streams: StreamHealth | null;
let healthCheckMethod: () => Promise<ISetting>;
const HEALTH_CHECK_URL = '/api/config-service/config/configuration/pr/lb/health-check.json';

const fallbackStreamHealth = async (): Promise<Record<string, boolean>> => {
  const [res] = await to(
    request<StreamHealth>({
      url: HEALTH_CHECK_URL,
      disableTokenHeader: true,
    })
  );

  streams = res?.data || {};

  return streams;
};

let getStreamsHealth = async (): Promise<Record<string, boolean>> => {
  try {
    const res = await healthCheckMethod();

    streams = res?.valueStr ? JSON.parse(res.valueStr) : {};

    return streams!;
  } catch (e) {
    getStreamsHealth = fallbackStreamHealth;

    return fallbackStreamHealth();
  }
};

const refreshStreamHealth = (refreshInterval: number) => {
  setInterval(getStreamsHealth, refreshInterval * 1000);
};

export const healthCheck = {
  getStream: async (streamName: string) => {
    if (!streams) {
      await getStreamsHealth();
    }

    return streams?.[streamName] ?? true;
  },
  refreshStreamHealth,
};

export const setHealthCheck = (method: () => Promise<ISetting>) => (healthCheckMethod = method);
