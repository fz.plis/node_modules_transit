import { getStream } from '@platform/tools/stream-loader';
import { dialog } from '@platform/ui';
import { locale } from '../localization';

export const callStreamAction = async <T extends Record<string, (...args: any) => any> = any>(
  streamName: string,
  method: keyof T,
  params?: unknown
) => {
  let streamAction = null;

  try {
    const stream: T = await getStream(streamName);

    const action = stream?.[method];

    if (action) {
      action(params);
      streamAction = action;
    }
  } catch (e) {
    dialog.showAlert(locale.error.callStreamError, {
      header: locale.error.text,
    });
  }

  return streamAction;
};
