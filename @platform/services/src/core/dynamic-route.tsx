import React, { useEffect, useCallback, useRef } from 'react';
import { Switch, useLocation } from 'react-router-dom';

export interface IDynamicRouteProps {
  routes: any[];
  loadRoutes(name: string): void;
  children: React.ReactNode;
}

export const DynamicRoute: React.FC<IDynamicRouteProps> = ({ loadRoutes, children, routes }) => {
  const { pathname } = useLocation();
  const memoisedLoad = useCallback(
    (loc: string) => {
      const name = loc.split('/').find((s: any) => s) || 'index';

      loadRoutes(name);
    },
    [loadRoutes]
  );
  const firstUpdate = useRef(true);

  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      setTimeout(() => memoisedLoad(pathname), 0);

      return;
    }

    memoisedLoad(pathname);
  }, [pathname, memoisedLoad]);

  return (
    <>
      {children}
      <Switch>{routes}</Switch>
    </>
  );
};
