import React from 'react';
import { useQuery } from 'react-query';
import { LoaderOverlay } from '@platform/ui';
import { PLATFORM_QUERY_NAMES } from '../constants';
import type { IAppConfig, ISetting } from '../interfaces';
import { request } from '../utils/request';

/**
 * Интерфейс контекста загружаемой конфигурации приложения.
 */
export interface IAppConfigContext {
  /**
   * Содержимое конфигурации с сервера.
   */
  config: IAppConfig;
  /**
   * Признак загрузки конфига с сервера.
   */
  loading: boolean;
  /**
   * Метод, запускающий загрузку.
   */
  reload(): void;
}

const defaultContext = ({ reload: () => {} } as unknown) as IAppConfigContext;

/**
 * React Context конфига приложения.
 */
export const AppConfigContext = React.createContext<IAppConfigContext>(defaultContext);

/**
 * Возвращает загруженный конфиг. Требуется для доступа вне React-приложения (экшены, крипта).
 */
export const getAppConfigValue = () => defaultContext.config;

/**
 * Возвращает элемент конфига.
 *
 * @param key Ключ элемента.
 */
export const getAppConfigItem = function <K extends keyof IAppConfig>(key: K): IAppConfig[K] {
  return getAppConfigValue()[key];
};

/**
 * Загружает конфиг по указанному урлу с сервиса конфигурации.
 *
 * @param loader Относительный адрес файла конфигурации в конфиг сервисе.
 * @returns Json-конфигурация приложения.
 */
const loadConfig = (loader: () => Promise<IAppConfig>): Promise<IAppConfig> =>
  loader().then(config => {
    defaultContext.config = config;

    return config;
  });

export interface IAppConfigProps {
  /**
   * Метод получения конфига.
   */
  loader(): Promise<IAppConfig>;
  /**
   * Если `true` - компонент сам загружает конфиг по указанному url.
   * При этом на время загрузки можно отображать компонент, переданный в `fallback`.
   *
   * @default false
   */
  autoload?: boolean;
  /**
   * Если autoload - true, этот компонент отображается во время загрузки конфига.
   *
   * @default <LoaderOverlay doNotUsePortal opened />
   */
  fallback?: NonNullable<React.ReactNode> | null;
}

/**
 * Компонент предоставляет `AppConfigContext.Provider`.
 *
 * @example
 * <AppConfig url="/api/config-service/config.json">...</AppConfig>
 */
export const AppConfig: React.FC<IAppConfigProps> = ({
  loader,
  children,
  autoload = false,
  // используем пропс doNotUsePortal, потому что на момент отрисовки портала еще может не быть.
  fallback = <LoaderOverlay doNotUsePortal opened />,
}) => {
  const { data: cfg, isLoading: loading, refetch: reload } = useQuery(PLATFORM_QUERY_NAMES.APP_CONFIG, () => loadConfig(loader), {
    enabled: false,
    initialData: defaultContext.config,
  });

  React.useEffect(() => {
    if (!cfg && autoload) {
      void reload();
    }
  }, [autoload, cfg, reload]);

  if (!cfg && autoload) {
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{fallback}</>;
  }

  return (
    <AppConfigContext.Provider value={{ loading, reload, config: (cfg as unknown) as IAppConfig }}>{children}</AppConfigContext.Provider>
  );
};

/**
 * Хук, возвращающий значение контекста из `AppConfigContext`.
 */
export const useAppConfig = () => React.useContext(AppConfigContext);

export const getSettingsLoader = (
  menuLoader: () => Promise<ISetting>,
  settingsLoader: () => Promise<ISetting>,
  fallbackConfigLoader: () => Promise<IAppConfig>
) => async () => {
  try {
    const [menuSettings, allSettings] = await Promise.all([menuLoader(), settingsLoader()]);

    const settings = JSON.parse(allSettings.valueStr!);
    const menu = JSON.parse(menuSettings.valueStr!);

    return {
      ...settings,
      menu,
    };
  } catch (e) {
    return fallbackConfigLoader();
  }
};

export const getFallbackConfigLoader = (url: string) => () =>
  request({
    url,
    disableTokenHeader: true,
  }).then(({ data }) => data);
