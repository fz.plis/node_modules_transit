import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { useQuery } from 'react-query';
import { dialog, LoaderOverlay } from '@platform/ui';
import { CLIENT_ID, PLATFORM_QUERY_NAMES } from '../constants';
import type {
  IActionWebInfo,
  IActionWithAuth,
  IAuthConfiguration,
  IButtonConfig,
  IParsedToken,
  IUserAuthoritiesResponse,
  USER_CONTEXT_TYPE,
  IAuthoritiesObject,
} from '../interfaces';
import { locale } from '../localization';
import {
  clearLocalStorage,
  getOauthProvider,
  getToken,
  OAuthProvider,
  parseToken,
  setOauthProvider,
  setToken as setTokenToStorage,
  tokenIsExpired,
} from '../utils';

/**
 * Интерфейс компонента auth.
 */
interface IAuthProps {
  /**
   * Метод загрузки данных аутентификации.
   */
  authLoader(): Promise<IAuthConfiguration>;
  /**
   * Метод загрузки привилегий.
   */
  authoritiesLoader(): Promise<IUserAuthoritiesResponse>;
}

/**
 * Контекст аутентификации.
 */
interface IAuthContext {
  /**
   * Привилегии пользователя.
   */
  authorities: string[];
  /**
   * Конфиг аутентификации.
   */
  config: IAuthConfiguration | null;
  /**
   * Фатальная ошибка загрузки.
   */
  fatalErr: boolean;
  /**
   * Провайдер аутентификации.
   */
  provider: OAuthProvider | null;
  /**
   * Список  ролей.
   */
  roles: string[];
  /**
   * Токен.
   */
  token: IParsedToken | null;
  /**
   * Строковое представление токена.
   */
  tokenString: string;
  /**
   * Получить список доступных текущему пользователю элементов.
   */
  getAvailableItems<T>(items: Array<IAuthoritiesObject & T>): T[];
  /**
   * Метод получения списка доступных действий.
   *
   * @param actions Список действий.
   */
  getAvailableActions(actions: IActionWithAuth[]): Array<IActionWebInfo<any, any>>;
  /**
   * Метод получения списка доступных действий по категории.
   *
   * @param actions Список действий.
   */
  getAvailableActionsWithCategory(actions: Record<string, IActionWithAuth[]>): IButtonConfig<any, any>;
  /**
   * Метод проверки контекста.
   *
   * @param allowedContexts Доступный контекст.
   */
  hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean;
  /**
   * Метод проверки привилегий.
   *
   * @param userAuthorities Список привилегий.
   */
  hasAuthority(...userAuthorities: string[]): boolean;
  /**
   * Метод выхода из системы.
   */
  logout(): void;
  /**
   * Метод редиректа на логин.
   */
  redirectToLogin(): void;
}

let defaultContext: IAuthContext = {
  authorities: [],
  config: null,
  fatalErr: false,
  roles: [],
  token: null,
  tokenString: '',
  provider: null,
  redirectToLogin: () => {},
  logout: () => {},
  hasAuthority: (..._: string[]) => false,
  hasAllowedContextType: (_: USER_CONTEXT_TYPE[]) => false,
  getAvailableActions: (_: IActionWithAuth[]) => [],
  getAvailableItems: _ => [],
  getAvailableActionsWithCategory: (_: Record<string, IActionWithAuth[]>) => ({}),
};

const redirectToLogin = () => {
  clearLocalStorage();

  const loginUrl = getOauthProvider().buildLoginUrl(window.location.href);

  window.location.replace(loginUrl);
};

const logout = () => {
  clearLocalStorage();

  const url = getOauthProvider().buildLogoutUrl(window.location.href);

  window.location.replace(url);
};

const setAuthValue = (token: IParsedToken, tokenString: string, authorities: string[]) => {
  const hasAuthority = (...userAuthorities: string[]) => userAuthorities.every(x => authorities.includes(x));

  const hasAllowedContextType = (allowedContexts: USER_CONTEXT_TYPE[]) => allowedContexts.some(x => token.user_context_types.includes(x));

  const getAvailableActions = (actions: IActionWithAuth[]) =>
    actions.reduce<Array<IActionWebInfo<any, any>>>((result, curr) => {
      const { authorities: userAuthorities, ...rest } = curr;

      if (authorities && hasAuthority(...userAuthorities)) {
        result.push(rest);
      }

      return result;
    }, []);

  const getAvailableItems = <T,>(items: Array<IAuthoritiesObject & T>) =>
    items.reduce<T[]>((acc, val) => {
      if (val.authorities?.every(x => authorities.includes(x))) {
        const { authorities: userAuthorities, ...item } = val;

        return [...acc, item] as T[];
      }

      return acc;
    }, []);

  const getAvailableActionsWithCategory = (actions: Record<string, IActionWithAuth[]>) => {
    const actionAuth: IButtonConfig<any, any> = {};

    Object.keys(actions).forEach(key => {
      actionAuth[key] = getAvailableActions(actions[key]);
    });

    return actionAuth;
  };

  defaultContext = {
    ...defaultContext,
    tokenString,
    authorities,
    token,
    roles: token.roles,
    getAvailableItems,
    getAvailableActions,
    getAvailableActionsWithCategory,
    hasAllowedContextType,
    hasAuthority,
    logout,
    redirectToLogin,
  };

  return defaultContext;
};

export const getAuthValue = () => defaultContext;

export const getAuthItem = <K extends keyof Omit<IAuthContext, 'config' | 'fatalErr' | 'provider'>>(key: K): IAuthContext[K] =>
  getAuthValue()[key];

const AuthContext = React.createContext<IAuthContext>(defaultContext);

export const AuthProvider: React.FC<IAuthProps> = ({ authLoader, authoritiesLoader, children }) => {
  const [config, setConfig] = useState<IAuthConfiguration | null>(null);
  const [authLoaded, setAuthLoaded] = useState(false);
  const [fatalErr, setFatalErr] = useState(false);
  const [provider, setProvider] = useState<OAuthProvider | null>(null);
  const [token, setToken] = useState<IParsedToken | null>(null);
  const [tokenString, setTokenString] = useState<string>('');
  const [authorities, setAuthorities] = useState<string[]>([]);
  const [roles, setRoles] = useState<string[]>([]);

  const { refetch: reloadConfig } = useQuery(PLATFORM_QUERY_NAMES.OPEN_ID_CONFIGURATION, authLoader, {
    enabled: false,
  });

  const { refetch: loadAuthorities } = useQuery(PLATFORM_QUERY_NAMES.USER_AUTHORITIES, authoritiesLoader, {
    enabled: false,
  });

  useEffect(() => {
    const load = async () => {
      const { data: fetchedConfig, isError } = await reloadConfig();

      if (isError) {
        setFatalErr(true);
        setAuthLoaded(true);

        return;
      }

      const { authorization_endpoint: authorizationEndpoint, logout_uri: logoutUri, login_uri: loginUri } = fetchedConfig!;
      const oauthProvider = OAuthProvider.of({
        authUri: authorizationEndpoint,
        clientId: CLIENT_ID,
        loginUri,
        logoutUri,
        responseType: 'token',
        // показываем ошибку в консоли, пока механизм не отработан полностью
        onError: console.error,
        onReceiveToken: ({ access_token: accessToken }) => {
          setTokenToStorage(accessToken);
          // убираем токен из строки поиска
          history.replaceState(null, '', window.location.pathname);
        },
      });

      setOauthProvider(oauthProvider);

      setConfig(fetchedConfig!);
      setProvider(oauthProvider);

      if (oauthProvider && window.location.hash) {
        oauthProvider.parseCallbackUrl(window.location.hash);
      }

      const authToken = getToken()!;

      if (!authToken) {
        setAuthLoaded(true);

        return;
      }

      let tokenData: IParsedToken | null = null;

      try {
        tokenData = parseToken(authToken);
      } catch (e) {
        logout();
      }

      const { data: authoritiesResponse, isError: authoritiesError } = await loadAuthorities();

      if (authoritiesError) {
        logout();
      }

      const allAuthorities = Array.from(
        new Set(
          Object.values(authoritiesResponse!.clientAuthorities).reduce((acc, val) => [...acc, ...val], authoritiesResponse!.authorities)
        )
      );

      const authValue = setAuthValue(tokenData!, authToken, allAuthorities);

      setToken(tokenData);
      setTokenString(authToken);

      if (tokenIsExpired(tokenData!)) {
        logout();

        return;
      }

      // если истекает через длительное время, то сбоит setTimeout
      // поставил таймер на сутки, либо exp, смотря что наступит раньше
      const time = Math.min(dayjs.unix(tokenData!.exp).diff(dayjs.utc(), 'millisecond'), 1000 * 60 * 60 * 24);

      setTimeout(() => {
        logout();
        dialog.showAlert(locale.alert.sessionEnded, {
          onClose: logout,
          onOk: logout,
        });
      }, time);

      setAuthorities(allAuthorities);
      setRoles(authValue.roles);
      setAuthLoaded(true);
    };

    load().catch(() => {
      setFatalErr(true);
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return authLoaded ? (
    <AuthContext.Provider
      value={{
        authorities,
        config,
        fatalErr,
        roles,
        token,
        tokenString,
        provider,
        redirectToLogin,
        logout,
        hasAuthority: defaultContext.hasAuthority,
        hasAllowedContextType: defaultContext.hasAllowedContextType,
        getAvailableItems: defaultContext.getAvailableItems,
        getAvailableActions: defaultContext.getAvailableActions,
        getAvailableActionsWithCategory: defaultContext.getAvailableActionsWithCategory,
      }}
    >
      {children}
    </AuthContext.Provider>
  ) : (
    <LoaderOverlay doNotUsePortal opened />
  );
};

AuthProvider.displayName = 'AuthProvider';

export const useAuth = () => React.useContext(AuthContext);
