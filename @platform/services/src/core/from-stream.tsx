import React from 'react';
import { getStream } from '@platform/tools/stream-loader';

export interface IFromStreamProps {
  name: string;
  streamInstance?: any;
  loader: React.ReactNode;
  children(stream: any): JSX.Element;
}

export const FromStream: React.FC<IFromStreamProps> = ({ name, loader, children }: IFromStreamProps) => {
  const [isLoading, setIsLoading] = React.useState(true);
  const stream = React.useRef<any>();

  React.useEffect(() => {
    void getStream(name).then(s => {
      stream.current = s;
      setIsLoading(false);
    });
  }, [setIsLoading, name]);

  if (isLoading) {
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return <>{loader}</>;
  }

  return children(stream.current);
};

FromStream.displayName = 'FromStream';
