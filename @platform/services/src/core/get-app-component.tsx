import React, { useCallback, useMemo, useRef, useState } from 'react';
import type { RouterProps } from 'react-router';
import { Route, Router } from 'react-router-dom';
import { getStream, getLoadedStreams } from '@platform/tools/stream-loader';
import { hideLoader, showLoader } from '../components';
import type { IUserData } from '../interfaces';
import { isClientScope } from '../utils';
import { DynamicRoute } from './dynamic-route';
import { healthCheck } from './health-check';

/**
 * Свойства компонента приложения.
 */
export interface IAppParams {
  /**
   * Компонент отображения загрузчика стрима.
   */
  LoadingStream: React.ComponentType<any>;
  /**
   * Страница "не найдено".
   */
  NotFound: React.ComponentType<any>;
  /**
   * Страница "стрим недоступен".
   */
  Unavailable: React.ComponentType<any>;
  /**
   * История.
   */
  history: RouterProps['history'];
}

export const getAppMainComponent = ({ LoadingStream, NotFound, history, Unavailable }: IAppParams) => {
  // оборачиваем в React.memo для предотвращения ререндера в случае изменения НЕ user в контексте сверху
  const App: React.FC<{ user?: IUserData }> = React.memo(({ user }) => {
    const [streamIsUnavailable, setStreamIsUnavailable] = useState(false);
    const [isLoadingStream, setIsLoadingStream] = useState(true);
    const stream = useRef('');
    const loadStream = useCallback(
      async (name: string) => {
        showLoader();

        let healthCheckStatus = true;

        if (isClientScope() && name !== 'index' && !user?.closedServicesAvailable) {
          healthCheckStatus = await healthCheck.getStream(name);
        }

        hideLoader();

        if (healthCheckStatus) {
          setStreamIsUnavailable(false);
        } else {
          setStreamIsUnavailable(true);

          return;
        }

        if (stream.current === name || !name) {
          return;
        }

        stream.current = name;

        setIsLoadingStream(true);
        getStream(name)
          .then(() => {
            setIsLoadingStream(false);
          })
          .catch(() => {
            setIsLoadingStream(false);
          });
      },
      [setIsLoadingStream, user]
    );

    const renderDefaultRoute = useCallback(() => {
      const comp = isLoadingStream ? LoadingStream : NotFound;

      return <Route key={'default'} component={comp} />;
    }, [isLoadingStream]);

    const routerNode = useMemo(() => {
      const streams = Object.values(getLoadedStreams()).filter(x => x);
      // eslint-disable-next-line prefer-spread
      const streamRoutes = [].concat.apply(
        [],
        streams.filter((x: any) => x.routes).map((x: any) => x.routes)
      );
      const routes = [
        ...(streamIsUnavailable ? [<Route key="error" component={Unavailable} />] : []),
        ...streamRoutes,
        renderDefaultRoute(),
      ];
      const components = streams.filter((x: any) => x.component).map((x: any) => x.component);

      return (
        <DynamicRoute loadRoutes={loadStream} routes={routes}>
          {components}
        </DynamicRoute>
      );
    }, [streamIsUnavailable, renderDefaultRoute, loadStream]);

    return (
      <Router history={history}>
        <Route>{routerNode}</Route>
      </Router>
    );
  });

  App.displayName = 'App';

  return App;
};
