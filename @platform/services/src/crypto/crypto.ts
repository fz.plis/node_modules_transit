import type { ICryptoModule } from '../interfaces';

let cryptoModule: ICryptoModule | undefined;

const moduleNotSpecified = () => {
  throw new Error('You must specified used crypto module by calling `setCryptoModule`');
};

export const getCryptoModule = (): ICryptoModule => {
  if (!cryptoModule) {
    moduleNotSpecified();
  }

  return cryptoModule!;
};

export const setCryptoModule = (val: ICryptoModule) => (cryptoModule = val);
