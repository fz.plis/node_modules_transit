import React from 'react';
import { Gap, Font, Box, Typography, CollapsedPanel, dialog, DIALOG_TYPE } from '@platform/ui';
import type { IUniversalSignData } from '..';
import { locale } from '../localization';
import { CRYPTO_ERROR_LOCALE } from './constants';
import css from './dialogs.scss';
import type { IError } from './olk/crypto-module-interface';
import { getCryptoPluginApp } from './olk/crypto-module-service';

export interface ISignErrorDialogProps {
  header: string;
  text: string;
  error?: IError;
}

export interface IErrorMessageProps {
  text: React.ReactNode;
}

export const ErrorMessage: React.FC<IErrorMessageProps> = ({ text, children }) => (
  <Box>
    <Typography.P className={css.wrap}>{text}</Typography.P>
    <Gap.XS />
    <Typography.P className={css.wrap}>{children}</Typography.P>
  </Box>
);

ErrorMessage.displayName = 'ErrorMessage';

export interface IErrorDetailsProps {
  error: IError;
}

export const ErrorDetails: React.FC<IErrorDetailsProps> = ({ error, children }) => (
  <CollapsedPanel
    header={
      <Font volume="SM" weight="BOLD">
        {children}
      </Font>
    }
  >
    <Typography.Text>{`${CRYPTO_ERROR_LOCALE.errorCode}: ${error.code}`}</Typography.Text>
    <Typography.Text>{error.text}</Typography.Text>
  </CollapsedPanel>
);

ErrorDetails.displayName = 'ErrorDetails';

const internalError = (error?: IError) => {
  const content = <ErrorMessage text={CRYPTO_ERROR_LOCALE.fatalText} />;
  const additionalContent = () => {
    if (!error) {
      return null;
    }

    return <ErrorDetails error={error}>{CRYPTO_ERROR_LOCALE.fatalCollapsedHeader}</ErrorDetails>;
  };

  dialog.showAlert(content, {
    additionalContent: additionalContent(),
    header: CRYPTO_ERROR_LOCALE.fatalHeader,
  });
};

export const keyAccessDeined = () => {
  const content = <ErrorMessage text={CRYPTO_ERROR_LOCALE.accessText1}>{CRYPTO_ERROR_LOCALE.accessText2}</ErrorMessage>;

  dialog.showAlert(content, {
    header: CRYPTO_ERROR_LOCALE.fatalHeader,
  });
};

export const moduleNotFound = () => {
  const content = (
    <ErrorMessage text={CRYPTO_ERROR_LOCALE.cryptomodulNotInstalledText1}>{CRYPTO_ERROR_LOCALE.cryptomodulNotInstalledText2}</ErrorMessage>
  );

  dialog.showError(content, {
    header: <Typography.H3 className={css.wrap}>{CRYPTO_ERROR_LOCALE.moduleNotInstalledHeader}</Typography.H3>,
    okButtonText: CRYPTO_ERROR_LOCALE.install,
    cancelButtonText: CRYPTO_ERROR_LOCALE.cancel,
    dialogType: DIALOG_TYPE.WARNING,
    onOk: () => {
      window.open(getCryptoPluginApp());
    },
  });
};

export const certificatesNotFound = () =>
  dialog.showAlert(<ErrorMessage text={locale.certificatesNotFound.text1}>{locale.certificatesNotFound.text2}</ErrorMessage>, {
    header: locale.certificatesNotFound.title,
  });

export const certificatesNotRightSign = () =>
  dialog.showAlert(<ErrorMessage text={locale.action.errorSignAndSend} />, {
    header: locale.action.error,
  });

export const signCertificatesNotFound = () => {
  dialog.showAlert(<ErrorMessage text={locale.signCertificatesNotFound.error.message}>{locale.certificatesNotFound.text2}</ErrorMessage>, {
    header: locale.signCertificatesNotFound.error.title,
  });
};

export const universalSignError = (signData: IUniversalSignData[], error: any) => {
  const docs = signData.map(({ label }) => label).join(', ');
  const additionalContent = () => {
    if (!error) {
      return null;
    }

    return <ErrorDetails error={error} />;
  };
  const content = <ErrorMessage text={locale.signDialog.signError({ docs })}>{additionalContent()}</ErrorMessage>;

  return new Promise<void>((resolve, reject) =>
    dialog.showConfirmation(content, resolve, {
      header: locale.sign.failed({ count: signData.length }),
      okButtonText: locale.action.signOthers,
      cancelButtonText: locale.action.cancelSign,
      onClose: () => reject(true),
    })
  );
};

export const signCertificateInvalid = () => {
  const content = <ErrorMessage text={CRYPTO_ERROR_LOCALE.invalidSign} />;

  dialog.showAlert(content, {
    header: CRYPTO_ERROR_LOCALE.fatalHeader,
  });
};

export const signErrorDialog = {
  internalError,
  keyAccessDeined,
  moduleNotFound,
  certificatesNotFound,
  certificatesNotRightSign,
  signCertificatesNotFound,
  universalSignError,
  signCertificateInvalid,
};
