/**
 * Тип подписи.
 */
export enum CRYPTO_MODULE_SIGN_TYPE {
  /**
   * Массив байт.
   */
  PLAIN = 'PLAIN',
  // прочие форматы
  SFT = 'SFT',
  CMS = 'CMS',
  CADES_BES = 'CAdES_BES',
  CADES_T = 'CAdES_T',
  CADES_X_LONG_TYPE_1 = 'CAdES_X_Long_Type_1',
  CADES_A = 'CAdES_A',
}

/**
 * Криптопровайдер.
 */
export interface ProviderInfo {
  /**
   * Идентификатор интерфейса.
   *
   * @description В случае если не указан, используется значение по-умолчанию. Для Windows — "CAPI", для прочих OS "JCA".
   */
  providerInterface?: PROVIDER_INTERFACE;
  /**
   * Наименование криптопровайдера.
   */
  providerName: string;
  /**
   * Тип провайдера.
   *
   * @description Заполняется для "CAPI".
   */
  providerType?: number;
  /**
   * Алгоритм криптопровадера.
   *
   * @description Заполняется для "JCP".
   */
  providerAlhoritm?: string;
}

export interface IContainerSCOMNative {
  '@type': CONTAINER_TYPE;
  // Путь к файлу с ключом
  keyFile: string;
  // Директория с ключами пользователя
  keyPath: string;
  // Путь к файлу сертификата
  certPath: string;
  // CA сертификаты
  CAFiles: any;
  // CRL файлы
  CRLFiles: any;
}

export interface IContainerDboBss {
  '@type': CONTAINER_TYPE;
  containerLocationName: string;
}

export interface IGPBCryptoModuleInitkey {
  // Идентификатор локали, по умолчанию ru_RU
  lcid?: string;
  // Информация о требуемом провайдере
  providerInfo?: ProviderInfo;
  // Сертификат в формате X509, в кодировке Base64
  X509data: string;
  // Информация о требуемом контейнере
  containerInfo?: IContainerDboBss | IContainerSCOMNative;
  // Данные на подпись для аутентификации
  authData?: string;
  signType: CRYPTO_MODULE_SIGN_TYPE;
}

export interface IGPBCryptoModuleResultInitKey {
  // Подписанные данные в кодировке base64
  signData: string;
  // Идентификатор сессии для контейнера ключей
  keySessID: string;
}

export interface IGPBCryptoModuleSign {
  // Идентификатор локали, по умолчанию ru_RU
  lcid?: string;
  // Идентификатор сессии для контейнера ключей
  keySessID: string;
  signType: CRYPTO_MODULE_SIGN_TYPE;
  // Массив данных для подписи в кодировке Base64
  data: string[];
  /**
   * Признак открепления подписи.
   */
  detached?: boolean;
}

export interface IGPBCryptoModuleSignResult {
  data: string;
  signType: CRYPTO_MODULE_SIGN_TYPE;
}

export interface IGPBCryptoModuleCheck {
  // Идентификатор локали, по умолчанию ru_RU
  // lcid?: string
  // keySessID: string
  providerInfo?: ProviderInfo;
  certificate: string;
  // Данные для подписи в кодировке Base64
  data: string[];
  // Массив подписанных данных
  signedData: IGPBCryptoModuleSignResult[];
  certificatesCA?: string[];
  CRLs?: string[];
}

export interface IIdentityFields {
  commonName?: string;
  organizationalUnit?: string;
  organization?: string;
  country?: string;
  state?: string;
  locality?: string;
  inn?: string;
  ogrn?: string;
  snils?: string;
  ogrnip?: string;
  email?: string;
  principalAttrsAsString?: string;
}

export interface IGPBCryptoModuleSubject {
  commonName?: string;
  organization?: string;
  country?: string;
  state?: string;
  locality?: string;
  identityFields?: IIdentityFields;
}

export interface IGPBCryptoModuleIssuer {
  commonName?: string;
  identityFields?: IIdentityFields;
}

export interface IGPBCryptoModuleCheckResponseData {
  serialNumber?: string;
  subject?: IGPBCryptoModuleSubject;
  issuer?: IGPBCryptoModuleIssuer;
  validFrom: number;
  validUntil: number;
  publicKey: string;
  body: string;
}

export interface IGPBCryptoModuleCheckResponse {
  error: IError;
  data: IGPBCryptoModuleCheckResponseData;
}

export interface IGPBCryptoModuleCertinfo {
  // Идентификатор локали, по умолчанию ru_RU
  lcid?: string;
  // Идентификатор сессии для контейнера ключей
  keySessID: string;
}

export interface IPBCryptoModuleCertificateInfo {
  serialNumber: string;
  subject: {
    commonName: string;
    organizationalUnit?: string;
    organization: string;
    country?: string;
    state: string;
    locality: string;
  };
  issuer: {
    commonName: string;
    country: string;
  };
  validFrom: string;
  validUntil: string;
}

export interface IError {
  code: 0;
  text: string;
}

export interface IGPBCryptoModuleSystemInfo {
  // Идентификатор локали, по умолчанию ru_RU
  lcid?: string;
  // IP адрес для которого требуется вернуть MAC адрес
  ipAddress: string;
}

export interface IGPCryptoModuleBResultSystemInfo {
  // Операционная система
  OS: string;
  // Версия OC
  OSVersion: string;
  // IP адрес полученный в запросе
  ipAddrees: string;
  // MAC адрес сетевого интерфейса с требуемым IP
  macAddrees: string;
}

export interface IKeyContainerSFT {
  fullName: string;
  shortName: string;
  persons: IGPCryptoModulePerson[];
}

export interface IGPCryptoModulePerson {
  name: string;
  position: string;
  keyContainers: IGPCryptoModuleKeyContainers[];
}

export interface IGPCryptoModuleKeyContainers {
  providerInfo: ProviderInfo;
  containerInfo?: IContainerDboBss | IContainerSCOMNative;
  certificateInfo: IPBCryptoModuleCertificateInfo;
  X509data: string;
}

/**
 * Идентификатор интерфейса криптопровайдера.
 */
export enum PROVIDER_INTERFACE {
  CAPI = 'CAPI',
  JCA = 'JCA',
  SCOM_MESPRO_4 = 'SCOM_MESPRO_4',
}

export enum CONTAINER_TYPE {
  SCOM = 'ContainerSCOMNative',
  DBOBSS = 'ContainerDboBss',
}

export enum CRYPTO_PLUGIN_TYPE {
  BSS = 'BSS',
  SFT = 'SFT',
}

/**
 * Реквизиты субъекта сертификата.
 */
export interface Subject {
  /**
   * Наименование (ФИО).
   */
  commonName: string;
  /**
   * Вид деятельности организации.
   */
  organizationUnit?: string;
  /**
   * Наименование организации.
   */
  organization?: string;
  /**
   * Код страны.
   */
  country?: string;
  /**
   * Область или регион.
   */
  state?: string;
  /**
   * Расположение.
   */
  locality?: string;
  /**
   * ИНН.
   */
  inn?: string;
  /**
   * ОГРН.
   */
  ogrn?: string;
  /**
   * ОГРНИП.
   */
  ogrnip?: string;
  /**
   * СНИЛС.
   */
  snils?: string;
  /**
   * Адрес электронной почты.
   */
  email?: string;
  /**
   * Параметры сертификата в строковом представлении.
   */
  principalAttrsAsString?: string;
}

/**
 * Реквизиты издателя сертификата.
 */
export type Issuer = Subject;

/**
 * Основные реквизиты сертификата.
 */
export interface CertificateInfo {
  /**
   * Серийный номер.
   */
  serialNumber: string;
  /**
   * Субъект.
   */
  subject: Subject;
  /**
   * Издатель.
   */
  issuer: Issuer;
  /**
   * Дата и время начала действия ключа.
   */
  validFrom: string;
  /**
   * Дата и время окончания действия ключа.
   */
  validUntil: string;
  /**
   * Открытый ключ.
   */
  publicKey: string;
  /**
   * Отпечаток.
   */
  thumbprint?: string;
  /**
   * Наименование средства ЭП.
   */
  cipfName: string[];
  /**
   * Классы средства ЭП.
   */
  cipfClasses: string[];
  /**
   * Содержимое сертификата в кодировке base64.
   */
  body: string;
}

/**
 * Входящие параметры метода генерации ключей.
 */
export interface GenerateKeyParams {
  /**
   * Поля запроса на сертификат.
   */
  subject: {
    /**
     * Наименование (ФИО).
     */
    commonName: string;
    /**
     * Отдел.
     */
    organizationUnit?: string;
    /**
     * Организация.
     */
    organization?: string;
    /**
     * Страна.
     */
    country?: string;
    /**
     * Область или регион.
     */
    state?: string;
    /**
     * Город или населенный пункт.
     */
    locality?: string;
    /**
     * ИНН.
     */
    inn?: string;
    /**
     * ОГРН.
     */
    ogrn?: string;
    /**
     * ОГРНИП.
     */
    ogrnip?: string;
    /**
     * СНИЛС.
     */
    snils?: string;
    /**
     * Адрес электронной почты.
     */
    email?: string;
    /**
     * Должность.
     */
    title?: string;
    /**
     * Улица.
     */
    street?: string;
    /**
     * ФИО.
     */
    fio?: string;
    /**
     * Фамилия.
     */
    surname?: string;
    /**
     * Имя.
     */
    givenname?: string;
  };
  /**
   * OID алгоритма электронной подписи.
   */
  keyAlg: string;
  /**
   * Длина ключа.
   */
  keyLength: string;
  /**
   * Наименование ключевого контейнера.
   */
  containerName?: string;
  /**
   * Наименование криптопровайдера.
   */
  cryptoProvider: string;
  /**
   * Список области использования ключа, расширение EKU сертификата.
   *
   * @description При необходимости указания нескольких значений OID — они передаются через символ ",".
   *
   * @example
   * "keyUsageOIDs": "1.3.6.1.5.5.7.3.4,1.3.6.1.5.5.7.3.2,1.3.6.1.4.1.311.10.3.12"
   */
  keyUsagesOIDs?: string;
  /**
   * Использование ключа, расширение "KeyUsage" сертификата.
   *
   * Значение параметра содержит список значений строковых констант, которые формирую значение поля "KeyUsage" запроса на сертификат.
   *
   * Значения в списке перечисляются через символ ",".
   *
   * На основании переданного списка значений "внутри» криптоплагина формируется битовая маска.
   *
   * В случае передачи "пустой строки" значение расширение "KeyUsage" в запросе на сертификат не заполняется (отсутствуют).
   *
   * @example
   * "keyUsageTypes": "DIGITAL_SIGNATURE,NON_REPUDIATION,DATA_ENCIPHERMENT"
   *
   * @see https://tools.ietf.org/html/rfc5280#section-4.2.1.3
   *
   */
  keyUsagesTypes: string;
  /**
   * Признак ключа — экспортируемый или нет.
   */
  exportableKey: boolean;
  /**
   * Шаблон сертификата.
   *
   * В формате: OID[(MajorVersion[.MinorVersion])], в скобках [] - необязательное включение.
   * Где OID – OID шаблона, MajorVersion – основная версия шаблона, MinorVersion – младшая версия шаблона.
   *
   * @example
   * "certificateTemplate": "1.2.643.2.2.46.0.8(1.0)"
   */
  certificateTemplate?: string;
}

/**
 * Результат генерации ключей.
 */
export interface GenerateKeyResult {
  /**
   * Описание ошибки.
   */
  error: {
    /**
     * Код ошибки.
     */
    code: number;
    /**
     * Текст ошибки.
     */
    text: string;
  };
  /**
   * Тело ответа.
   */
  data: {
    /**
     * Наименование ключевого контейнера.
     */
    containerName: string;
    /**
     * Данные запроса на сертификат в формате PKCS#10.
     */
    request: string;
  };
}
