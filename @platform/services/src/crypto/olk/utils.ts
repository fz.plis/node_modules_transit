import axios from 'axios';
import { getAppConfigItem } from '../../core/app-config';
import { CRYPTO_PLUGINS_FEATURE } from './constants';

const VERSION_REQUEST_TIMEOUT = 3 * 1000;

/**
 * Получить версию бсс плагина, установленного на компьютере пользователя.
 *
 * @throws Axios error.
 */
export const getBssPluginVersion = async () => {
  const bss = getAppConfigItem(CRYPTO_PLUGINS_FEATURE).BSS;

  try {
    const result = await axios.post(
      bss.url,
      {
        Plugin: 'AXTools',
        Method: 'GetProperty',
        Params: [{ Version: '' }],
      },
      {
        timeout: VERSION_REQUEST_TIMEOUT,
      }
    );

    return result.data.Result;
  } catch (e) {
    throw new Error(e);
  }
};

/**
 * Получить версию сфт плагина, установленного на компьютере пользователя.
 *
 * @throws Axios error.
 */
export const getSftPluginVersion = async () => {
  const sft = getAppConfigItem(CRYPTO_PLUGINS_FEATURE).SFT;

  try {
    const result = await axios.get(`${sft.url}/api/v1/version/service`, {
      timeout: VERSION_REQUEST_TIMEOUT,
    });

    return result.data.data;
  } catch (e) {
    throw new Error(e);
  }
};
