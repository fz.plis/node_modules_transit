import type { AxiosInstance, AxiosRequestConfig } from 'axios';
import axios from 'axios';
import { to } from '@platform/core';
import { isMac } from '@platform/ui';
import { getAppConfigItem } from '../../core/app-config';
import type { ICertFilter, IUserOrgCertificate } from '../../interfaces';
import { ERROR } from '../../interfaces';
import { canUseCryptoPlugin } from '../../utils/crypto-module';
import {
  CRYPTO_ERROR_CODE,
  CRYPTO_PLUGIN_AUTO_DETECTION,
  CRYPTO_PLUGIN_BY_DEFAULT,
  CRYPTO_PLUGINS_FEATURE,
  UNKNOWN_VERSION,
} from './constants';
import type {
  GenerateKeyParams,
  GenerateKeyResult,
  IError,
  IGPBCryptoModuleCertinfo,
  IGPBCryptoModuleCheck,
  IGPBCryptoModuleCheckResponse,
  IGPBCryptoModuleInitkey,
  IGPBCryptoModuleResultInitKey,
  IGPBCryptoModuleSign,
  IGPBCryptoModuleSignResult,
  IGPCryptoModuleBResultSystemInfo,
  IKeyContainerSFT,
} from './crypto-module-interface';
import { CRYPTO_PLUGIN_TYPE } from './crypto-module-interface';
import { getBssPluginVersion, getSftPluginVersion } from './utils';

const SERT_API_URL = '/api/v1';
const SERT_SAPI_URL = '/sapi/v1';

const CODE_MAP: Record<string, ERROR> = {
  [ERROR.NO_CONNECTION]: ERROR.CRYPTO_NOT_FOUND,
  [CRYPTO_ERROR_CODE.BAD_REQUEST]: ERROR.CRYPTO_INCORRECT_AUTHORIZATION_INFO,
  [CRYPTO_ERROR_CODE.USER_IS_DENIED_ACCESS]: ERROR.CRYPTO_UNAUTHORIZE,
  [CRYPTO_ERROR_CODE.UNAUTHORIZED]: ERROR.UNAUTHORIZED,
  [CRYPTO_ERROR_CODE.FORBIDDEN]: ERROR.FORBIDDEN,
  [CRYPTO_ERROR_CODE.NOT_FOUND]: ERROR.NOT_FOUND,
  [CRYPTO_ERROR_CODE.METHOD_NOT_ALLOWED]: ERROR.METHOD_NOT_ALLOWED,
  [CRYPTO_ERROR_CODE.UNSUPPORTED_MEDIA_TYPE]: ERROR.UNSUPPORTED_MEDIA_TYPE,
  [CRYPTO_ERROR_CODE.LOCKED]: ERROR.LOCKED,
  [CRYPTO_ERROR_CODE.INTERNAL_SERVER_ERROR]: ERROR.CRYPTO_INTERNAL_SERVER_ERROR,
  [ERROR.VALIDATION_SERVER_CONTROLS_ERROR]: ERROR.CRYPTO_CERTS_NOT_FOUND,
  [CRYPTO_ERROR_CODE.KEY_CONTAINER_NOT_FOUND]: ERROR.CERTIFICATES_NOT_FOUND,
};

export interface ICryptoModuleResponse {
  data?: any;
  error: IError;
}

/**
 * Плагин под которым пользователем должен авторизоваться в системе.
 */
let cryptoPluginType: CRYPTO_PLUGIN_TYPE = CRYPTO_PLUGIN_TYPE.SFT;
/**
 * Плагин под которым пользователь авторизовался в системе.
 */
let authByCryptoPlugin: { type: CRYPTO_PLUGIN_TYPE; version: string } | undefined;
const instance: AxiosInstance = axios.create({ baseURL: '' });
const PLUGIN_NETWORK_ERROR_MESSAGE = 'Network Error';
const VERSION_ERROR_MESSAGE = 'Plugin Version Error';

instance.interceptors.response.use(undefined, err => {
  let code = 0;

  if (err.response) {
    if (err.response.data?.code) {
      code = CODE_MAP[err.response.data.code];
    }
  } else {
    switch (err.message) {
      case PLUGIN_NETWORK_ERROR_MESSAGE:
        code = ERROR.CRYPTO_NOT_FOUND;
        break;

      case VERSION_ERROR_MESSAGE:
        code = ERROR.CRYPTO_MODULE_VERSION_INVALID;
        break;

      default:
        break;
    }
  }

  return Promise.reject({
    response: {
      data: {
        errorInfo: {
          code,
        },
        data: (err.response || {}).data,
      },
    },
  });
});

/**
 * Выбор используемого плагина для работы с подписью.
 *
 * При работе с плагином, проверяется установленная у пользователя версия плагина.
 * Если версии плагина пользователя, меньше, чем минимальная версия, которая используется на ЭКО,
 * то пользователю выводится ошибка.
 *
 * @param plugin Плагин пользователя.
 */
export const setCryptoPlugin = (plugin: CRYPTO_PLUGIN_TYPE = CRYPTO_PLUGIN_TYPE.SFT) => {
  cryptoPluginType = plugin;

  const plugins = getAppConfigItem(CRYPTO_PLUGINS_FEATURE);
  const checkAvailableCryptoModules = getAppConfigItem(CRYPTO_PLUGIN_AUTO_DETECTION);
  const availablePlugins = [
    { ...plugins[plugin], type: plugin },
    ...(checkAvailableCryptoModules
      ? Object.entries(plugins)
          .filter(([pluginType]) => plugin !== pluginType)
          .map(([type, userPlugin]) => ({
            ...userPlugin,
            type,
          }))
      : []),
  ];

  instance.interceptors.request.use(async axiosRequest => {
    if (instance.defaults.baseURL !== '') {
      return axiosRequest;
    }

    const pluginsResult = await Promise.all(
      availablePlugins.map(async ({ url, minVersion, type }) => {
        let availableRequest: Promise<string>;

        switch (type) {
          case CRYPTO_PLUGIN_TYPE.BSS:
            availableRequest = getBssPluginVersion();
            break;
          case CRYPTO_PLUGIN_TYPE.SFT:
            availableRequest = getSftPluginVersion();
            break;
          default:
            break;
        }

        const [res, err] = await to(availableRequest!);

        return {
          url,
          type,
          version: res,
          canUse: canUseCryptoPlugin(res, minVersion),
          err,
        };
      })
    );

    const workingPlugin = pluginsResult.find(axiosPlugin => axiosPlugin.canUse && !axiosPlugin.err);

    if (workingPlugin) {
      const { url, type: pluginType, version } = workingPlugin;

      // eslint-disable-next-line require-atomic-updates
      instance.defaults.baseURL = url;
      axiosRequest.baseURL = url;
      authByCryptoPlugin = {
        version: version || UNKNOWN_VERSION,
        type: pluginType as CRYPTO_PLUGIN_TYPE,
      };
    } else {
      const usePlugin = pluginsResult.find(axiosPlugin => !axiosPlugin.err);

      if (usePlugin) {
        cryptoPluginType = usePlugin.type as CRYPTO_PLUGIN_TYPE;

        throw new Error(VERSION_ERROR_MESSAGE);
      } else {
        throw new Error(PLUGIN_NETWORK_ERROR_MESSAGE);
      }
    }

    return axiosRequest;
  });
};

/**
 * Отдает тип криптоплагина.
 */
export const getCryptoPluginType = () => cryptoPluginType;

/**
 * Отдает URL для скачивания криптоплагина.
 */
export const getCryptoPluginApp = () => {
  const cryptoPluginsInstaller = getAppConfigItem('cryptoPluginsInstaller');

  const URLS = {
    [CRYPTO_PLUGIN_TYPE.BSS]: isMac() ? cryptoPluginsInstaller.BSS.nix : cryptoPluginsInstaller.BSS.win,
    [CRYPTO_PLUGIN_TYPE.SFT]: isMac() ? cryptoPluginsInstaller.SFT.nix : cryptoPluginsInstaller.SFT.win,
  };

  const cryptoPluginAutoDetection = getAppConfigItem(CRYPTO_PLUGIN_AUTO_DETECTION);
  const cryptoPluginByDefault = getAppConfigItem(CRYPTO_PLUGIN_BY_DEFAULT);

  if (cryptoPluginAutoDetection && cryptoPluginByDefault in CRYPTO_PLUGIN_TYPE) {
    return URLS[cryptoPluginByDefault];
  }

  return URLS[cryptoPluginType];
};

export const request = (params: AxiosRequestConfig) => instance(params).then(r => r.data.data);

export const cryptoModuleService = {
  // Номер версии крипто клиента
  serviceVersion: (): Promise<string> =>
    request({
      url: `${SERT_API_URL}/version/service`,
      method: 'GET',
    }),
  // Номер версии API v1
  version: (): Promise<string> =>
    request({
      url: `${SERT_API_URL}/version`,
      method: 'GET',
    }),
  getInitkey: (data: IGPBCryptoModuleInitkey): Promise<IGPBCryptoModuleResultInitKey> =>
    request({
      url: `${SERT_API_URL}/initkey`,
      method: 'POST',
      data,
    }),
  sign: (data: IGPBCryptoModuleSign): Promise<IGPBCryptoModuleSignResult[]> =>
    request({
      url: `${SERT_API_URL}/sign`,
      method: 'POST',
      data,
    }),
  check: (data: IGPBCryptoModuleCheck): Promise<IGPBCryptoModuleCheckResponse> =>
    instance({
      url: `${SERT_API_URL}/check`,
      method: 'POST',
      data,
    }).then(resp => resp.data),
  certinfo: (data: IGPBCryptoModuleCertinfo): Promise<IGPCryptoModuleBResultSystemInfo> =>
    request({
      url: `${SERT_API_URL}/certinfo`,
      method: 'POST',
      data,
    }),
  keycontainersSft: (): Promise<IKeyContainerSFT> =>
    request({
      url: `${SERT_API_URL}/keycontainers/sft`,
      method: 'POST',
    }),
  // Номер версии крипто клиента
  sapiServiceVersion: (): Promise<string> =>
    request({
      url: `${SERT_SAPI_URL}/version/service`,
      method: 'GET',
    }).then(result => result.data),
  // Номер версии SAPI v1
  sapiVersion: (): Promise<string> =>
    request({
      url: `${SERT_SAPI_URL}/version`,
      method: 'GET',
    }),
  sapiStop: () =>
    request({
      url: `${SERT_SAPI_URL}/stop`,
      method: 'POST',
    }),
  getUserStorageCertificates: (accreditedCerts: string[], certificateFilter: ICertFilter): Promise<IUserOrgCertificate[]> => {
    const cryptoModuleConfig = getAppConfigItem('cryptoModuleConfig');

    if (!cryptoModuleConfig) return Promise.resolve([]);

    const { cipfName, cipfClass } = cryptoModuleConfig;

    return instance({
      url: `${SERT_API_URL}/findcertificates`,
      method: 'POST',
      data: {
        cipfClass,
        cipfName,
        issuersSKI: accreditedCerts,
        certificates: [
          {
            subject: certificateFilter,
          },
        ],
      },
    })
      .then(result => {
        if (result?.data?.error?.code !== 0) {
          return [];
        }

        return result?.data?.certificates || [];
      })
      .catch(() => []);
  },
  /**
   * Получить все доступные пользователю криптоплагины.
   */
  getAvailableCryptoModulesVersion: async () => {
    const [bssRes, bssErr] = await to(getBssPluginVersion());
    const [sftRes, sftErr] = await to(getSftPluginVersion());
    const bss = bssErr ? undefined : bssRes || '';
    const sft = sftErr ? undefined : sftRes || '';

    return {
      bss,
      sft,
    };
  },
  /**
   * Получить плагин под которым авторизовался пользователь.
   */
  getAuthPlugin: () => authByCryptoPlugin,
  /**
   * Генерация ключей и запроса на сертификат.
   */
  generateKey: async (params: GenerateKeyParams): Promise<GenerateKeyResult['data']> =>
    request({
      url: `${SERT_API_URL}/generatekey`,
      method: 'POST',
      data: params,
    }),
};
