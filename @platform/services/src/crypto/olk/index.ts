import { values } from '@platform/core';
import type { ICryptoModule, IVerifySignResult } from '../../interfaces';
import { VERIFY_SIGN_ERROR } from '../../interfaces';
import { CRYPTO_MODULE_SIGN_TYPE, CONTAINER_TYPE } from './crypto-module-interface';
import { cryptoModuleService } from './crypto-module-service';

export { setCryptoPlugin } from './crypto-module-service';

export interface IOlkInitOptions {
  certData: string;
  containerNameLocation: string;
}

let keySessID = '';
let usageCertificate = '';
let cryptoSessionTimeout: any;
// убран NodeJS.Timeout, при сборке для стенда ошибка Type 'number'
// is not assignable to type 'Timeout', хотя локально собирается без проблем.
const CRYPTO_SESSION_TIME = 59 * 1000 * 60;

const init = ({ certData, containerNameLocation }: IOlkInitOptions) =>
  Promise.resolve(keySessID).then(id => {
    if (id && usageCertificate === certData) {
      return id;
    }

    /**
     * Если providerInfo не передали, то используется для Windows КриптоПРО CSP, для MAC - КриптоПРО JCP.
     */
    const containerInfo = containerNameLocation
      ? {
          '@type': CONTAINER_TYPE.DBOBSS,
          containerLocationName: containerNameLocation,
        }
      : undefined;

    return cryptoModuleService
      .version()
      .then(() =>
        cryptoModuleService.getInitkey({
          // providerInfo: {
          //   providerInterface: PROVIDER_INTERFACE.JCA,
          //   providerName: 'Crypto-Pro GOST R 34.10-2012 Strong Cryptographic Service Provider',
          //   providerType: 81,
          //   providerAlhoritm: 'GOST2012_512',
          // },
          containerInfo,
          signType: CRYPTO_MODULE_SIGN_TYPE.CMS,
          X509data: certData,
        })
      )
      .then(r => {
        keySessID = r.keySessID;
        usageCertificate = certData;

        if (cryptoSessionTimeout) {
          clearTimeout(cryptoSessionTimeout);
        }

        // У криптоплагинов протухает сессия каждые 60 минут
        // поэтому делаем хак, что каждые 59 минут
        // обнулять сессию
        cryptoSessionTimeout = setTimeout(() => {
          keySessID = '';
        }, CRYPTO_SESSION_TIME);

        return r.keySessID;
      });
  });

export const olkCryptoModule: ICryptoModule = {
  init,
  sign: (_, data, sessId?: string, detached?: boolean) =>
    cryptoModuleService
      .sign({ data: [data], keySessID: sessId || keySessID, signType: CRYPTO_MODULE_SIGN_TYPE.CMS, detached })
      .then(([first]) => first.data),
  batchSign: (_, signData, sessId?: string, detached?: boolean) =>
    cryptoModuleService
      .sign({ data: signData, keySessID: sessId || keySessID, signType: CRYPTO_MODULE_SIGN_TYPE.CMS, detached })
      .then(result => result.map(({ data }) => data)),
  // eslint-disable-next-line @typescript-eslint/default-param-last
  verifySign: (signature, data, certificate, certificatesCA = [], crls = [], providerName): Promise<IVerifySignResult> => {
    const providerInfo = providerName
      ? {
          providerName,
        }
      : undefined;

    return cryptoModuleService
      .check({
        certificate,
        data,
        signedData: [{ data: signature, signType: CRYPTO_MODULE_SIGN_TYPE.CMS }],
        certificatesCA,
        CRLs: crls,
        providerInfo,
      })
      .then(resp => {
        let error;

        if (resp.error.code !== 0) {
          error = {
            code: values(VERIFY_SIGN_ERROR).includes(resp.error.code) ? resp.error.code : VERIFY_SIGN_ERROR.UNKNOWD,
            description: resp.error.text,
          };
        }

        return {
          data: resp.data,
          valid: resp.error.code === 0,
          error,
        };
      });
  },
  getCertificates: () => cryptoModuleService.certinfo({ keySessID }).then((x: any) => x),
  installationCheck: cryptoModuleService.version,
  getUserStorageCertificates: cryptoModuleService.getUserStorageCertificates,
  getKeySession: async (certificate: string) =>
    cryptoModuleService.getInitkey({
      X509data: certificate,
      signType: CRYPTO_MODULE_SIGN_TYPE.PLAIN,
    }),
  getAvailableCryptoModulesVersion: cryptoModuleService.getAvailableCryptoModulesVersion,
};
