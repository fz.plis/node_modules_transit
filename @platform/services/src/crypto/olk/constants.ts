export enum CRYPTO_ERROR_CODE {
  NOT_FOUND = 1,
  METHOD_NOT_ALLOWED = 2,
  FORBIDDEN = 3,
  BAD_REQUEST = 4,
  UNSUPPORTED_MEDIA_TYPE = 5,
  UNAUTHORIZED = 6,
  LOCKED = 7,
  INTERNAL_SERVER_ERROR = 99,
  USER_IS_DENIED_ACCESS = 100,
  INVALID_AUTHORIZATION_INFO = 101,
  KEY_CONTAINER_NOT_FOUND = 102,
}

/**
 * Наименование параметра информации об используемых плагинах.
 */
export const CRYPTO_PLUGINS_FEATURE = 'cryptoPlugins';
/**
 * Наименование параметра автоопределения плагина.
 */
export const CRYPTO_PLUGIN_AUTO_DETECTION = 'cryptoPluginAutoDetection';
export const UNKNOWN_VERSION = 'unknown';
/**
 * Определяет какой криптоплагин будет предложен пользователю для установки
 * при нажатии на кнопку «Установить» в случае, когда плагин не обнаружен.
 */
export const CRYPTO_PLUGIN_BY_DEFAULT = 'cryptoPluginByDefault';
