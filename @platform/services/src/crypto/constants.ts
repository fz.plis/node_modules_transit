import { locale } from '../localization';

export const CRYPTO_ERROR_LOCALE = {
  get fatalHeader() {
    return locale.sign.fatalHeader;
  },
  get fatalText() {
    return locale.sign.fatalText;
  },
  get internalServerError() {
    return locale.dialog.sign.internalServerError;
  },
  get fatalCollapsedHeader() {
    return locale.sign.fatalCollapsedHeader;
  },
  get accessText1() {
    return locale.crypto.error.access.text1;
  },
  get accessText2() {
    return locale.crypto.error.access.text2;
  },
  get accessHeader() {
    return locale.crypto.error.access.header;
  },
  get moduleNotInstalledHeader() {
    return locale.crypto.error.cryptomodulNotInstalled;
  },
  get cryptomodulNotInstalledText1() {
    return locale.crypto.error.cryptomodulNotInstalledText1;
  },
  get cryptomodulNotInstalledText2() {
    return locale.crypto.error.cryptomodulNotInstalledText2;
  },
  get certsNotFound() {
    return locale.crypto.error.certsNotFound;
  },
  get install() {
    return locale.action.install;
  },
  get errorCode() {
    return locale.dialog.sign.errorCode;
  },
  get reTry() {
    return locale.action.reTry;
  },
  get cancel() {
    return locale.action.cancel;
  },
  get invalidSign() {
    return locale.crypto.error.invalidSign;
  },
};
