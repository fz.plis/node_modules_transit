import { fake } from '../fake';

export const dictionaryLocaleService = {
  get: (): Promise<Record<string, string>> => fake(),
};
