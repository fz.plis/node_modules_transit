import type { Canceler } from 'axios';
import { fake } from '../fake';
import type { IAttachment, ICheckUploadOptions, IDownloadedAttachment } from '../interfaces';

export const attachmentService = {
  getStatus: (attachmentId: string, onCancel?: (cancel: Canceler) => void) => fake(attachmentId, onCancel),
  upload: (file: File, options?: ICheckUploadOptions): Promise<IAttachment> => fake(file, options),
  checkUploadProgress: (attachmentId: string, options: ICheckUploadOptions = {}): Promise<IAttachment> => fake(attachmentId, options),
  download: (attachmentId: string, accessToken: string): Promise<IDownloadedAttachment> => fake(attachmentId, accessToken),
  downloadBase64: (attachmentId: string, accessToken?: string): Promise<IDownloadedAttachment> => fake(attachmentId, accessToken),
  downloadOneTimeData: (attachmentId: string, accessToken: string): Promise<IDownloadedAttachment> => fake(attachmentId, accessToken),
};
