import { fake, fakeDictionaryService } from '../fake';
import type { ICertificateRequest, ICertificateRequestHistory } from '../interfaces';

const { get, getList, getCounter } = fakeDictionaryService<ICertificateRequest>();

export const certificateRequestService = {
  get,
  getList,
  getCounter,
  getBySpoId: (_spoId: string): Promise<ICertificateRequest> => fake(),
  getHistory: (_id: string): Promise<ICertificateRequestHistory[]> => fake(),
};
