import type { IBranch, IBranchV2, ICollectionResponse, IMetaData } from '../../interfaces';
import { getDictionaryService, getNewDictionaryService, metadataToRequestParams, request } from '../../utils';

export const createClientBranchService = (baseUrl: string) => {
  const { getList, get, searchByName } = getDictionaryService<IBranch>(baseUrl);

  const clientPart = {
    getList,
    get,
    searchByName,
    localityList(): Promise<string[]> {
      return request({ url: `${baseUrl}/locality-list`, method: 'GET' }).then(result => result.data.data);
    },
    bicPage(metaData: IMetaData): Promise<ICollectionResponse<string>> {
      return request({
        url: `${baseUrl}/bic-page`,
        method: 'POST',
        data: metadataToRequestParams(metaData),
      }).then(r => ({
        data: r.data.data.page.list,
        total: r.data.data.page.total,
      }));
    },
  };

  return clientPart;
};

export const createBranchV2Service = (baseUrl: string) => {
  const { getList, get, getCounter, searchByName } = getNewDictionaryService<IBranchV2>(baseUrl);

  return {
    getList,
    get,
    getCounter,
    searchByName,
    async localityList(): Promise<string[]> {
      const { data: response } = await request({
        url: `${baseUrl}/locality-list`,
      });

      return response.list;
    },
  };
};
