import type { AxiosResponse } from 'axios';
import type { IDataResponse, IServerResp, ISetting } from '../../interfaces';
import { request } from '../../utils';

export const appSettingsResponse = <T>({ data }: AxiosResponse<IDataResponse<T>>) => {
  if (data.message) {
    throw data.message;
  }

  return data.data;
};

export const saveAppSettingsResponse = <T>({ data }: AxiosResponse<IServerResp<T>>) => {
  if (data.errorInfo) {
    throw data.errorInfo.message;
  }

  return data.data;
};

export const getNewAppSettings = (url: string) => ({
  getAll: () =>
    request<IDataResponse<ISetting[]>>({
      method: 'GET',
      url,
    }).then(appSettingsResponse),
  getByCode: (code: string) =>
    request<IDataResponse<ISetting>>({
      method: 'GET',
      url: `${url}/${code}`,
    }).then(appSettingsResponse),
});
