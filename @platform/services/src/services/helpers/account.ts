import type { IAccount, IAccountV2 } from '../../interfaces';
import { getDictionaryService, request } from '../../utils';

export const createAccountClientService = (baseUrl: string) => {
  const { getList, get, getCounter, searchByName } = getDictionaryService<IAccount>(baseUrl);

  const clientPart = {
    getList,
    get,
    getCounter,
    searchByName,
    list: (ids: string[]): Promise<IAccount[]> =>
      request({
        url: `${baseUrl}/list`,
        method: 'POST',
        data: { ids },
      }).then(result => result.data.data.data),
    listWithBalance: (ids: string[]): Promise<IAccountV2[]> =>
      request({
        url: `${baseUrl}/list-with-balance-head-unit`,
        method: 'POST',
        data: { list: ids },
      }).then(result => result.data.list),
  };

  return clientPart;
};
