import { HEADERS_PARAM } from '../../constants/headers-param';
import type { IExportRequest, IOpenAccount, IServerResp, IExportResponse, IPrintResponse, IHistoryResponse } from '../../interfaces';
import { request, getNewDictionaryService, getFileName } from '../../utils';

const exportFile = (result: any) => {
  const param = result.headers[HEADERS_PARAM.CONTENT_DISPOSITION] || '';

  return {
    fileName: getFileName(param),
    type: result.headers[HEADERS_PARAM.CONTENT_TYPE],
    data: result.data,
  };
};

export const createOpenAccountService = (baseUrl: string) => {
  const { getList, get, getCounter } = getNewDictionaryService<IOpenAccount>(baseUrl);

  return {
    getList,
    get,
    getCounter,
    validateSign: (id: string): Promise<IServerResp<IOpenAccount>> =>
      request({
        url: `${baseUrl}/validate-sign`,
        method: 'POST',
        data: { data: id },
      }).then(resp => resp),
    exportClaimOpenAccounts: (data: IExportRequest): Promise<IExportResponse> =>
      request({
        url: `${baseUrl}/export-claim-open-accounts`,
        method: 'POST',
        responseType: 'arraybuffer',
        data: { data },
      }).then(exportFile),
    exportClaimRulesGoz: (data: IExportRequest): Promise<IExportResponse> =>
      request({
        url: `${baseUrl}/export-claim-rules-goz`,
        method: 'POST',
        responseType: 'arraybuffer',
        data: { data },
      }).then(exportFile),
    exportInfoOpenAccounts: (data: IExportRequest): Promise<IExportResponse> =>
      request({
        url: `${baseUrl}/export-info-open-accounts`,
        method: 'POST',
        responseType: 'arraybuffer',
        data: { data },
      }).then(exportFile),
    exportListRequestOpenAccounts: (data: IExportRequest): Promise<IExportResponse> =>
      request({
        url: `${baseUrl}/export-list-request-open-accounts`,
        method: 'POST',
        responseType: 'arraybuffer',
        data: { data },
      }).then(exportFile),
    printClaimOpenAccounts: (data: IExportRequest): Promise<IPrintResponse> =>
      request({
        url: `${baseUrl}/print-claim-open-accounts`,
        method: 'POST',
        data: { data },
      }).then(d => d.data),
    printClaimRulesGoz: (data: IExportRequest): Promise<IPrintResponse> =>
      request({
        url: `${baseUrl}/print-claim-rules-goz`,
        method: 'POST',
        data: { data },
      }).then(d => d.data),
    printInfoOpenAccounts: (data: IExportRequest): Promise<IPrintResponse> =>
      request({
        url: `${baseUrl}/print-info-open-accounts`,
        method: 'POST',
        data: { data },
      }).then(d => d.data),
    printListRequestOpenAccounts: (data: IExportRequest): Promise<IPrintResponse> =>
      request({
        url: `${baseUrl}/print-list-request-open-accounts`,
        method: 'POST',
        data: { data },
      }).then(d => d.data),
    getHistory: (id: string): Promise<IHistoryResponse[]> =>
      request({
        url: `${baseUrl}/history/${id}`,
        method: 'GET',
        data: { id },
      }).then(response => response.data.list),
  };
};
