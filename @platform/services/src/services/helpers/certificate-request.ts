import type { ICertificateRequest } from '../../interfaces';
import { request } from '../../utils/request';

export const createCertificateRequestService = (baseUrl: string) => ({
  getBySpoId: (spoId: string): Promise<ICertificateRequest> =>
    request({
      method: 'GET',
      url: `${baseUrl}/by-org-official-cert-id/${spoId}`,
    }).then(res => res.data.data),
});
