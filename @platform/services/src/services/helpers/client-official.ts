import type { IClientOfficial } from '../../interfaces';
import { request } from '../../utils';

export const createClientOffical = (baseUrl: string) => ({
  getUserOfficials: (userId: string): Promise<IClientOfficial[]> =>
    request({
      url: `${baseUrl}/user/${userId}`,
    }).then(r => r.data.data.data),
});
