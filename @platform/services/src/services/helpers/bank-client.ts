import { conditions } from '../../constants/external';
import type { IBankClient, ISearchByName, IClientSearch } from '../../interfaces';
import { getDictionaryService } from '../../utils';

export const createBankClientService = (baseUrl: string) => {
  const { getList, get, searchByName } = getDictionaryService<IBankClient>(baseUrl);

  const clientPart = {
    getList,
    get,
    searchByName,
    searchByTextQuery: (params: IClientSearch) => {
      const config = {
        pageSize: params.pageSize!,
        offset: params.offset!,
        filters: params.filters ?? {},
        textQuery: params.search,
      };

      return getList(config);
    },
    searchOgrn: (params: ISearchByName) =>
      getList({
        pageSize: params.pageSize!,
        offset: params.offset!,
        filters: {
          ogrnOgrip: {
            fieldName: 'ogrnOgrip',
            value: params.search,
            condition: conditions.contains,
          },
          notOgrnOgrip: {
            fieldName: 'ogrnOgrip',
            value: null,
            condition: conditions.notEq,
          },
        },
      }),
    searchRegNumber: (params: ISearchByName) =>
      getList({
        pageSize: params.pageSize!,
        offset: params.offset!,
        filters: {
          ogrnOgrip: {
            fieldName: 'regNumber',
            value: params.search,
            condition: conditions.contains,
          },
          notOgrnOgrip: {
            fieldName: 'regNumber',
            value: null,
            condition: conditions.notEq,
          },
        },
      }),
  };

  return clientPart;
};
