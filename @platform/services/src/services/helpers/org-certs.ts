import type { ICertResponse, IFileData, IOrgCert, IMetaData, ICollectionResponse } from '../../interfaces';
import { request, getNewDictionaryService, metadataToRequestParams } from '../../utils';

export interface IOrganizationCertificatesService<T = IOrgCert> {
  getList(metaData: IMetaData): Promise<ICollectionResponse<T>>;
  get(id: string): Promise<T>;
  download(certificateId: string): Promise<IFileData>;
}

/**
 * Утилита, обработки ответа запросов по сертификату.
 *
 * @throws Сообщение от бека.
 */
export const certificateResponse = <T = IOrgCert>({ data: response }: { data: ICertResponse<T> }): T => {
  const { code, message, data } = response;

  if (code) {
    throw message;
  }

  return data;
};

/**
 * Утилита, возвращающая стандартные методы справочников клиента и подписи.
 *
 * @param clientDictionaryURL Путь до справочника клиентов.
 * @param signatureURL Путь до сервиса подписи.
 * @example
 * const CLIENT_DICTIONARY_URL = '/api/client-dictionary-internal/internal/dictionary/client'
 * const SIGNATURE_URL = `/api/signature-internal/internal/sign`
 * const organizationCertificatesService = {
 *   ...getNewOrgCertsService(CLIENT_DICTIONARY_URL, SIGNATURE_URL)
 *   myNewMethod: async () => {...}
 * }
 */
export const getNewOrgCertsService = (clientDictionaryURL: string, signatureURL: string): IOrganizationCertificatesService => {
  const { get } = getNewDictionaryService<IOrgCert>(`${clientDictionaryURL}/org-official-cert`);

  return {
    get,
    getList: async metaData => {
      const { data: response } = await request({
        url: `${clientDictionaryURL}/org-official-cert/get-page`,
        method: 'POST',
        data: metadataToRequestParams(metaData),
      });

      // TODO: Почему-то ответ запроса коллекции отличен от стандартного в getNewDictionaryService
      return {
        data: response.page.list,
        total: response.page.total,
      };
    },
    download: async certificateId => {
      const response = await request({
        method: 'GET',
        url: `${signatureURL}/certificate/${certificateId}/download`,
      });

      return certificateResponse(response);
    },
  };
};
