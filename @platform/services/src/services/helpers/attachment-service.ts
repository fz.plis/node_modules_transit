import type { Canceler } from 'axios';
import axios from 'axios';
import { isIe, noop } from '@platform/ui';
import { HEADERS_PARAM } from '../../constants';
import { ATTACHMENT_STATUS, ERROR } from '../../interfaces';
import type { IDownloadedAttachment, IAttachment, IFileUploadOptions, ICheckUploadOptions } from '../../interfaces';
import { getFileName, polling, request } from '../../utils';

/**
 * Возвращает экземпляр `ProgressEvent`
 * Для IE возвращает объект, имитирующий ProgressEvent (использовать с осторожностью).
 *
 * @param opt Параметры ProgressEvent.
 * @returns ProgressEvent.
 */
const initProgressEvent = (opt: ProgressEventInit): ProgressEvent => {
  if (isIe()) {
    return (opt as any) as ProgressEvent;
  }

  return new ProgressEvent('progress', opt);
};

const FINISH_STATUS = [ATTACHMENT_STATUS.STORED, ATTACHMENT_STATUS.MALWARE];
/**
 * Проверяем файл, файл считается загруженным, если он перешел в статус STORED или MALWARE.
 *
 * @param attachment Вложение.
 */
const checkFileStored = (attachment: IAttachment) => FINISH_STATUS.includes(attachment.status);
const DEFAULT_POLLING_INTERVAL = 500;

export const createAttachmentService = (baseUrl: string) => {
  /**
   * Метод для получения статуса загрузки.
   *
   * @param attachmentId Идентификатор файла.
   * @param onCancel Обработчик отмены.
   */
  const getStatus = (attachmentId: string, onCancel?: (cancel: Canceler) => void) =>
    request({
      url: `${baseUrl}/getStatus`,
      method: 'POST',
      data: {
        attachmentId,
      },
      cancelToken: onCancel && new axios.CancelToken(onCancel),
    }).then(r => r.data.data);

  const commonPart = {
    getStatus,
    /**
     * Метод для загрузки вложения.
     *
     * @param file - Объекта файла.
     * @param file.onUploadProgress Обработчик для прогресса загрузки вложения.
     * @param file.onCancel Обработчик отмены.
     */
    upload: (file: File, { onUploadProgress = noop, onCancel = noop }: IFileUploadOptions = {}): Promise<IAttachment> => {
      const formData = new FormData();

      formData.append('file', file, file.name);

      return request({
        onUploadProgress,
        cancelToken: new axios.CancelToken(onCancel),
        url: `${baseUrl}/upload`,
        method: 'POST',
        data: formData,
      }).then(d => d.data.data);
    },
    checkUploadProgress: (attachmentId: string, options: ICheckUploadOptions = {}) => {
      let counter = 0;

      /**
       * Функция выдает прогресс проверки антивирусом.
       *
       * @param attachId Идентификатор файла.
       * @param onCancel Обработчик отмены.
       */
      const checkWithProgressUpdate: typeof getStatus = (attachId, onCancel) => {
        counter += 1;

        if (options?.onUploadProgress) {
          options.onUploadProgress(initProgressEvent({ loaded: counter, total: options.maxTries }));
        }

        return getStatus(attachId, onCancel);
      };

      // максимальное время ожидания 5 минут
      const checkFile = polling(checkWithProgressUpdate, checkFileStored, options.interval || DEFAULT_POLLING_INTERVAL, options.maxTries);

      return checkFile(attachmentId, options ? options.onCancel : undefined).then(attach => {
        if (attach.status === ATTACHMENT_STATUS.MALWARE) {
          throw {
            response: {
              data: {
                errorInfo: {
                  code: ERROR.FILE_MALWARE,
                },
                data: attach,
              },
            },
          };
        }

        return attach;
      });
    },
    /**
     * Метод для скачивания файла.
     *
     * @param attachmentId Идентификатор файла.
     * @param accessToken Токен доступа к файлу.
     */
    download: async (attachmentId: string, accessToken: string): Promise<IDownloadedAttachment> => {
      const { headers, data } = await request({
        method: 'POST',
        responseType: 'arraybuffer',
        data: {
          accessToken,
          id: attachmentId,
        },
        url: `${baseUrl}/download`,
      });

      return {
        fileName: getFileName(headers[HEADERS_PARAM.CONTENT_DISPOSITION] || ''),
        type: headers[HEADERS_PARAM.CONTENT_TYPE],
        data,
      };
    },
    /**
     * Метод для скачивания файла в формате Base64.
     *
     * @param attachmentId Идентификатор файла.
     * @param accessToken Токен доступа к файлу.
     */
    downloadBase64: async (attachmentId: string, accessToken?: string): Promise<IDownloadedAttachment> => {
      const { headers, data } = await request({
        method: 'POST',
        data: {
          accessToken,
          id: attachmentId,
        },
        url: `${baseUrl}/download/base64`,
      });

      return {
        fileName: getFileName(headers[HEADERS_PARAM.CONTENT_DISPOSITION] || ''),
        type: headers[HEADERS_PARAM.CONTENT_TYPE],
        data,
      };
    },
    /**
     * Метод для одноразового скачивания файла.
     *
     * @param attachmentId Идентификатор файла.
     * @param accessToken Токен доступа к файлу.
     */
    downloadOneTimeData: async (attachmentId: string, accessToken: string): Promise<IDownloadedAttachment> => {
      const { headers, data } = await request({
        method: 'POST',
        responseType: 'arraybuffer',
        data: {
          accessToken,
          id: attachmentId,
        },
        url: `${baseUrl}/download/one-time-data`,
      });

      return {
        fileName: getFileName(headers[HEADERS_PARAM.CONTENT_DISPOSITION] || ''),
        type: headers[HEADERS_PARAM.CONTENT_TYPE],
        data,
      };
    },
  };

  return commonPart;
};
