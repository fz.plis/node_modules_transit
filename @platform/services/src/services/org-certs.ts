import { fakeDictionaryService, fake } from '../fake';
import type { IOrgCert, IFileData, IOrgCertFileInfo } from '../interfaces';

const { getList, get } = fakeDictionaryService<IOrgCert>();

export const orgCertsService = {
  get,
  getList,
  download: (_certificateId: string): Promise<IFileData> => fake(),
  parse: (_rawCertificate: string): Promise<IOrgCertFileInfo> => fake(),
};
