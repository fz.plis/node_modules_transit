import { fakeDictionaryService, fake } from '../fake';
import type { IAccount, IAccountV2 } from '../interfaces';

const { get, getList, getCounter } = fakeDictionaryService<IAccount>();

export const accountService = {
  get,
  getList,
  getCounter,
  list: (ids: string[]): Promise<IAccount[]> => fake(ids),
  listWithBalance: (ids: string[]): Promise<IAccountV2[]> => fake(ids),
};
