import { fake } from '../fake';
import type { ISetting } from '../interfaces';

export const appSettingsService = {
  getAll: (): Promise<ISetting[]> => fake(),
  getByCode: (code: string): Promise<ISetting> => fake(code),
};
