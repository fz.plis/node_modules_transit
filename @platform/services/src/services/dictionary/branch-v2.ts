import { fakeDictionaryService, fake } from '../../fake';
import type { IBranchV2 } from '../../interfaces';

const { get, getList, getCounter, searchByName } = fakeDictionaryService<IBranchV2>();

export const branchV2 = {
  get,
  getList,
  getCounter,
  searchByName,
  localityList: (): Promise<string[]> => fake(),
};
