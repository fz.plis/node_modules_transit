import { fakeDictionaryService, fake } from '../../fake';
import type { IBankClient, ISearchByName, ICollectionResponse } from '../../interfaces';

const { get, getList, searchByName } = fakeDictionaryService<IBankClient>();

export const bankClient = {
  get,
  getList,
  searchByName,
  searchOgrn: (params: ISearchByName): Promise<ICollectionResponse<IBankClient>> => fake(params),
  searchRegNumber: (params: ISearchByName): Promise<ICollectionResponse<IBankClient>> => fake(params),
};
