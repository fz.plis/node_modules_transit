import { fakeDictionaryService } from '../../fake';
import type { IActivityType } from '../../interfaces';

const { getList, get, searchByName } = fakeDictionaryService<IActivityType>();

export const activityType = {
  getList,
  get,
  searchByName,
};
