import { fakeDictionaryService } from '../../fake';
import type { IGozContract } from '../../interfaces';

const { getList, get, searchByName } = fakeDictionaryService<IGozContract>();

export const gk = {
  getList,
  get,
  searchByName,
};
