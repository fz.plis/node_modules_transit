import { fakeDictionaryService } from '../../fake';
import type { ICurrency } from '../../interfaces';

const { getList, get, searchByName } = fakeDictionaryService<ICurrency>();

export const currency = {
  getList,
  get,
  searchByName,
};
