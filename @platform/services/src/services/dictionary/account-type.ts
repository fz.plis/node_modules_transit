import { fakeDictionaryService } from '../../fake';
import type { IAccountType } from '../../interfaces';

const { getList, get, searchByName } = fakeDictionaryService<IAccountType>();

export const accountType = {
  getList,
  get,
  searchByName,
};
