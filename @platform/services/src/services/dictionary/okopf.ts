import { fakeDictionaryService } from '../../fake';
import type { IOkopf } from '../../interfaces';

const { getList, searchByName } = fakeDictionaryService<IOkopf>();

export const okopf = {
  getList,
  searchByName,
};
