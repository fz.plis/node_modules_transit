import { fakeDictionaryService } from '../../fake';
import type { IOkfs } from '../../interfaces';

const { getList, searchByName } = fakeDictionaryService<IOkfs>();

export const okfs = {
  getList,
  searchByName,
};
