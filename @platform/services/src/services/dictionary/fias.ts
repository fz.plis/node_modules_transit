import { fake } from '../../fake';
import type { IFiasResult } from '../../interfaces';

export const fias = {
  search: (query: string, count: number): Promise<IFiasResult[]> => fake(query, count),
};
