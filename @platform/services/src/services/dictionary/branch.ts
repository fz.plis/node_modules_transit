import { fakeDictionaryService, fake } from '../../fake';
import type { ICollectionResponse, IBranch, IMetaData } from '../../interfaces';

const { get, getList, searchByName } = fakeDictionaryService<IBranch>();

export const branch = {
  get,
  getList,
  searchByName,
  localityList: (): Promise<string[]> => fake(),
  bicPage: (metaData: IMetaData): Promise<ICollectionResponse<string>> => fake(metaData),
};
