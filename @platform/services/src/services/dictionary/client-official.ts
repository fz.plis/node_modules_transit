import { fake } from '../../fake';
import type { IClientOfficial } from '../../interfaces';

export const clientOfficial = {
  getUserOfficials: (userId: string): Promise<IClientOfficial[]> => fake(userId),
};
