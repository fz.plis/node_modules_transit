import { accountType } from './account-type';
import { activityType } from './activity-type';
import { bankClient } from './bank-client';
import { branch } from './branch';
import { branchV2 } from './branch-v2';
import { clientOfficial } from './client-official';
import { country } from './country';
import { currency } from './currency';
import { fias } from './fias';
import { gk } from './gk';
import { okfs } from './okfs';
import { okopf } from './okopf';

export const dictionaryService = {
  okfs,
  okopf,
  bankClient,
  currency,
  activityType,
  clientOfficial,
  country,
  accountType,
  branch,
  branchV2,
  gk,
  fias,
};
