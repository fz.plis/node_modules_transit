import { fakeDictionaryService } from '../../fake';
import type { ICountry } from '../../interfaces';

const { getList, get, searchByName } = fakeDictionaryService<ICountry>();

export const country = {
  getList,
  get,
  searchByName,
};
