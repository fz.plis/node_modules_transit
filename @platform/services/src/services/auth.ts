import { fake } from '../fake';
import type { IAuthConfiguration, ILoginPasswordRequest, ILoginResponse } from '../interfaces';

export const authService = {
  login: {
    byPassword: (data: ILoginPasswordRequest, redirectUri?: string | undefined): Promise<ILoginResponse> => fake(data, redirectUri),
  },
  logout: (): Promise<any> => fake(),
  getAuthConfiguration: (): Promise<IAuthConfiguration> => fake(),
};

export type AuthService = typeof authService;
