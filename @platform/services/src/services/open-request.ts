import { fakeDictionaryService, fake } from '../fake';
import type { IOpenAccount, IExportRequest } from '../interfaces';

const { get, getList, getCounter } = fakeDictionaryService<IOpenAccount>();

export const openAccountService = {
  get,
  getList,
  getCounter,
  exportClaimOpenAccounts: (_data: IExportRequest) => fake(),
  exportClaimRulesGoz: (_data: IExportRequest) => fake(),
  exportInfoOpenAccounts: (_data: IExportRequest) => fake(),
  exportListRequestOpenAccounts: (_data: IExportRequest) => fake(),
  printClaimOpenAccounts: (_data: IExportRequest) => fake(),
  printClaimRulesGoz: (_data: IExportRequest) => fake(),
  printInfoOpenAccounts: (_data: IExportRequest) => fake(),
  printListRequestOpenAccounts: (_data: IExportRequest) => fake(),
  validateSign: (_id: string) => fake(),
  getHistory: (_id: string) => fake(),
};
