import dayjs from 'dayjs';
import { snapshot } from '@platform/tools/istore';
import type { IOpenAccount, IDigest, ISignVerify, ISign, ISignResult, ISignature, ISignProfile } from '../interfaces';
import { OPEN_ACCOUNT_STATE } from '../interfaces';

/**
 * Хеш сертификата.
 */
export const fakeThumbprint = '1E9485EDB73EA7D7F6B8FB5E0983851585C8C887';

let openAccountFake: IOpenAccount[] = [];

const signatures: Record<string, ISignature> = {};

const addSignature = (docId: string, signature: string, profile: ISignProfile) => {
  const signObj = {
    profile,
    signature,
    signDate: dayjs().toISOString(),
  };

  signatures[docId] = signObj;

  return signObj;
};

const getSignatureByDocId = (docId: string) => signatures[docId];

export const addOrUpdateItem = (item: IOpenAccount) => {
  const index = openAccountFake.findIndex(x => x.id === item.id);

  if (index > -1) {
    openAccountFake[index] = snapshot(item);
  } else {
    openAccountFake = [...openAccountFake, snapshot(item)];
  }
};

export const deleteItem = (id: string) => {
  openAccountFake = openAccountFake.filter(x => x.id !== id);
};

export const newId = () => {
  let lastId = parseInt((openAccountFake[openAccountFake.length - 1] || { id: '0' }).id);

  lastId += 1;

  return lastId.toString();
};

export const getById = (id: string) => openAccountFake.find(x => x.id === id);

export const generateDigest = (id: string): IDigest => {
  const { status, ...dataForSign } = getById(id)!;
  const value = JSON.stringify(dataForSign);

  return {
    value,
    signFields: [],
    version: 0,
  };
};

export const verifySign = (id: string): ISignVerify => {
  const digest = generateDigest(id);
  const signature = getSignatureByDocId(id);

  return {
    digest,
    signature,
  };
};

export const sign = ({ documentId, signature }: ISign): ISignResult<IOpenAccount> => {
  const openAccount = getById(documentId);

  const updatedOpenAccount: IOpenAccount = {
    ...openAccount!,
    status: OPEN_ACCOUNT_STATE.SIGNED,
  };

  addOrUpdateItem(updatedOpenAccount);

  const profile: ISignProfile = {
    name: openAccount!.bankClient!.name,
    thumbprint: fakeThumbprint,
  };
  const signObj = addSignature(documentId, signature, profile);

  return {
    signature: signObj,
    document: updatedOpenAccount,
  };
};

export const send = (ids: string[]) => {
  const openAccounts = ids.map(getById).map(x => ({
    ...x!,
    status: OPEN_ACCOUNT_STATE.DELIVERED,
  }));

  openAccounts.forEach(addOrUpdateItem);

  return openAccounts;
};

export const EMPTY_OPEN_ACCOUNT: IOpenAccount = {
  bankClient: {
    extId: '',
    clientId: '',
    innKio: '',
    kpp: '',
    name: '',
    ogrn: '',
    resident: true,
    loginCertificateId: '',
    absId: '',
  },
  branch: {
    bic: '',
    id: '',
    name: '',
    correspondentAccount: '',
  },
  confirmNoChanges: false,
  commissionAccount: {
    number: '',
  },
  contract: {
    absId: '',
    date: '',
    number: '',
    isCommissionDebitingRequired: false,
  },
  date: '',
  id: '',
  number: '',
  opopAccount: {
    number: '',
  },
  status: OPEN_ACCOUNT_STATE.DRAFT,
  errors: '',
  userId: '',
  isRulesJoined: false,
  accountInfos: [],
  version: 0,
};

const OPEN_ACCOUNT_CATEGORIES = {
  ALL: 'ALL',
  IN_PROGRESS: 'IN_PROGRESS',
  IN_PROGRESS_BANK: 'IN_PROGRESS_BANK',
  FINISHED: 'FINISHED',
};

const OPEN_ACCOUNT_CATEGORIES_MAP = {
  IN_PROGRESS: [OPEN_ACCOUNT_STATE.DRAFT, OPEN_ACCOUNT_STATE.NEW],
  FINISHED: [
    OPEN_ACCOUNT_STATE.DELETED,
    OPEN_ACCOUNT_STATE.DELIVERED,
    OPEN_ACCOUNT_STATE.RECEIVED,
    OPEN_ACCOUNT_STATE.NOT_RECEIVED_BY_PROCESSING,
    OPEN_ACCOUNT_STATE.DETAILS_ERROR,
    OPEN_ACCOUNT_STATE.NEGATIVE_DECISION,
    OPEN_ACCOUNT_STATE.DENIED_FOR_OPEN,
    OPEN_ACCOUNT_STATE.PARTLY_EXECUTED,
    OPEN_ACCOUNT_STATE.EXECUTED,
  ],
};

export const getFakeOpenAccountFake = (category?: string): Record<string, IOpenAccount> => {
  const data = openAccountFake.filter(el => {
    if (OPEN_ACCOUNT_CATEGORIES.ALL === category || !category) {
      return true;
    } else if (OPEN_ACCOUNT_CATEGORIES.FINISHED === category) {
      return OPEN_ACCOUNT_CATEGORIES_MAP.FINISHED.includes(el.status);
    }

    return OPEN_ACCOUNT_CATEGORIES_MAP.IN_PROGRESS.includes(el.status);
  });
  const res: Record<string, IOpenAccount> = {};

  data.forEach((el, idx) => {
    const key = `${idx}`;

    res[key] = { ...el };
  });

  return res;
};

export const getFakeOpenAccountCategoryFake = () => ({
  [OPEN_ACCOUNT_CATEGORIES.ALL]: {
    count: openAccountFake.length,
    name: OPEN_ACCOUNT_CATEGORIES.ALL,
  },
  [OPEN_ACCOUNT_CATEGORIES.FINISHED]: {
    count: openAccountFake.filter(el => OPEN_ACCOUNT_CATEGORIES_MAP.FINISHED.includes(el.status)).length,
    name: OPEN_ACCOUNT_CATEGORIES.FINISHED,
  },
  [OPEN_ACCOUNT_CATEGORIES.IN_PROGRESS]: {
    count: openAccountFake.filter(el => OPEN_ACCOUNT_CATEGORIES_MAP.IN_PROGRESS.includes(el.status)).length,
    name: OPEN_ACCOUNT_CATEGORIES.IN_PROGRESS,
  },
});
