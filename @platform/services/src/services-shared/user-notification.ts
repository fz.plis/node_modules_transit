import type { IMetaData, IUserNotification, IUserNotificationCount } from '../admin';
import { request, getNewDictionaryService, metadataToRequestParams } from '../utils';

const URL = '/api/notification/user-notification';

export const userNotificationService = {
  ...getNewDictionaryService<IUserNotification>(URL),
  getCounter: (metaData: IMetaData) => {
    // Status category теперь один элемент в массиве and. выдрать наверно надо
    const { statusCategory, ...filter } = metadataToRequestParams(metaData).params.filter;

    return request({
      url: `${URL}/category-count`,
      method: 'POST',
      data: { params: { filter } },
    }).then(r => r.data.data.data);
  },
  markAsRead: (ids: string[]): Promise<Record<string, unknown>> =>
    request({
      url: `${URL}/mark-as-read`,
      method: 'POST',
      data: { list: ids },
    }).then(rest => rest.data),
  markAsUnread: (ids: string[]): Promise<Record<string, unknown>> =>
    request({
      url: `${URL}/mark-as-unread`,
      method: 'POST',
      data: { list: ids },
    }).then(rest => rest.data),
  postpone: (id: string, postponedUntil: string | null): Promise<IUserNotification> =>
    request({
      url: `${URL}/postpone`,
      method: 'POST',
      data: {
        id,
        postponedUntil,
      },
    }).then(response => response.data.data),
  getLocale: () =>
    request({
      //  TODO: хардкод, так как нет полноценной поддержки локализации
      url: '/api/notification/i18n/ru_Ru',
      method: 'GET',
    }).then(resp => resp.data),
  getUnreadMessageCount: (): Promise<IUserNotificationCount> =>
    request({
      url: `${URL}/count-not-read`,
      method: 'GET',
    }).then(resp => resp.data.data),
  getImportantNotification: (metaData: IMetaData): Promise<IUserNotification[]> =>
    request({
      url: `${URL}/important`,
      method: 'POST',
      data: metadataToRequestParams(metaData),
    }).then(resp => resp.data.page),
  execute: (id: string): Promise<IUserNotification> =>
    request({
      url: `${URL}/execute/${id}`,
      method: 'POST',
    }).then(response => response.data.data),
};
