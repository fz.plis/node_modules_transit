import type { IEgrulInfo, IServerResp } from '../interfaces';
import { EGRUL_SEARCH_TYPE } from '../interfaces';
import { request } from '../utils';

const baseUrl = '/api/reservation-request/egrul';

const egrulResponseHandler = (r: { data: IServerResp<{ data: IEgrulInfo }> }) => {
  if (r.data.errorInfo) {
    throw { response: { data: r.data } };
  }

  return r.data.data.data;
};

const inn = (id: string) => request({ url: `${baseUrl}/inn/${id}`, method: 'GET' }).then(egrulResponseHandler);

const ogrn = (id: string) => request({ url: `${baseUrl}/ogrn/${id}`, method: 'GET' }).then(egrulResponseHandler);

export const egrulService = {
  inn,
  ogrn,
  search: (mode: EGRUL_SEARCH_TYPE, id: string) => (mode === EGRUL_SEARCH_TYPE.INN ? inn(id) : ogrn(id)),
};
