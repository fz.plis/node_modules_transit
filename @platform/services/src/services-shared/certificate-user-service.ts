import type {
  IUserCertificate,
  IUserCertificateUpdateResponce,
  IAuthorityResponse,
  IAuthorityRequest,
  IVerifySignByDateResponce,
  IVerifySignByDateRequest,
  IAdditionalVerifySignRequestData,
  IAdditionalVerifySignResponseData,
} from '../interfaces';
import { request, getNewDictionaryService } from '../utils';

const USER_SIGN_BASE = '/api/signature/sign';
const USER_CERTIFICATE_URL = `${USER_SIGN_BASE}/certificate`;

export const certificateUserService = {
  ...getNewDictionaryService<IUserCertificate>(USER_CERTIFICATE_URL),
  updateUserCertificates: async (certificates: IUserCertificate[]): Promise<IUserCertificateUpdateResponce> =>
    request({
      url: `${USER_CERTIFICATE_URL}/update-user-certificates`,
      method: 'POST',
      data: { certificates },
    }).then(rest => rest.data),
  checkAuthority: async (data: IAuthorityRequest): Promise<IAuthorityResponse[]> =>
    request({
      url: `${USER_SIGN_BASE}/signature/v2/check-authority`,
      method: 'POST',
      data,
    }).then(rest => rest.data.data.results),
  getVerifyData: async (data: IVerifySignByDateRequest): Promise<IVerifySignByDateResponce> =>
    request({
      url: `${USER_SIGN_BASE}/signature/get-verify-data`,
      method: 'POST',
      data,
    }).then(rest => rest.data.data),
  getAdditionalVerifyData: async (data: IAdditionalVerifySignRequestData): Promise<IAdditionalVerifySignResponseData> =>
    request({
      url: `${USER_SIGN_BASE}/signature/get-additional-verify-data`,
      method: 'POST',
      data,
    }).then(rest => rest.data.data),
};
