export const settingsService = {
  get: () =>
    Promise.resolve({
      maxPrintCount: 30,
    }),
};
