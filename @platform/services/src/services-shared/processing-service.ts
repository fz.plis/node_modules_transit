import type { IAbsMeta, IBalancePositionResponse } from '../interfaces';
import { request } from '../utils';

const baseUrl = '/api/reservation-request/balance-position';

export const processingService = {
  balancePosition(data: IAbsMeta): Promise<string> {
    return request({
      data,
      url: `${baseUrl}/register-request`,
      method: 'POST',
    }).then(result => result.data.data.data);
  },
  checkPosition(id: string): Promise<IBalancePositionResponse> {
    return request({
      url: `${baseUrl}/check-request/${id}`,
      method: 'GET',
    }).then(result => {
      const { errorInfo, data } = result.data;

      if (errorInfo) {
        throw errorInfo;
      }

      return data.data;
    });
  },
};
