import type { ICheckClientMeta } from '../interfaces';
import { request } from '../utils';

const baseUrl = '/api/reservation-request/stop-list';

export const stopListService = {
  checkClient(data: ICheckClientMeta): Promise<boolean> {
    return request({
      data,
      url: `${baseUrl}/check-client`,
      method: 'POST',
    }).then(response => response.data.data);
  },
};
