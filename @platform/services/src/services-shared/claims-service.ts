import { HEADERS_PARAM } from '../constants/headers-param';
import type { IAccountClaims, IServerResp, IPrintResponseDoc } from '../interfaces';
import { request, getFileName, getDictionaryService } from '../utils';

export const CLAIMS_URL = '/api/reservation-request/request';
export const CLAIMS_DRAFT_URL = `${CLAIMS_URL}/draft`;

const fileResult = (result: any) => {
  const param = result.headers[HEADERS_PARAM.CONTENT_DISPOSITION] || '';

  return {
    fileName: getFileName(param),
    type: result.headers[HEADERS_PARAM.CONTENT_TYPE],
    data: result.data,
  };
};

export const claimsService = {
  ...getDictionaryService<IAccountClaims>(CLAIMS_URL),
  newDocument() {
    return request({ url: `${CLAIMS_URL}/new`, method: 'GET' }).then(result => result.data.data.data);
  },
  update(accountRequestData: IAccountClaims): Promise<IServerResp<IAccountClaims>> {
    return request({
      url: CLAIMS_URL,
      method: 'PUT',
      data: { data: accountRequestData, id: accountRequestData.id },
    }).then(result => {
      const { data, errorInfo } = result.data;

      return { errorInfo, data: data.data };
    });
  },
  draft(data: IAccountClaims): Promise<IAccountClaims> {
    return request({
      url: CLAIMS_DRAFT_URL,
      method: 'POST',
      data: { data },
    }).then(result => result.data.data.data);
  },

  updateDraft({ id, ...data }: IAccountClaims): Promise<IAccountClaims> {
    return request({
      url: CLAIMS_DRAFT_URL,
      method: 'PUT',
      data: { id, data },
    }).then(result => result.data.data.data);
  },

  send(ids: string[]): Promise<Array<IServerResp<IAccountClaims>>> {
    return request({
      url: `${CLAIMS_URL}/send`,
      method: 'POST',
      data: { ids },
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    }).then(result => result.data.map(({ errorInfo, data }: IServerResp<{ data: IAccountClaims }>) => ({ errorInfo, data: data.data })));
  },
  export(data: any) {
    return request({
      url: `${CLAIMS_URL}/export-notify-request`,
      method: 'POST',
      data: { data },
      responseType: 'blob',
    }).then(fileResult);
  },
  exportAcc(data: any) {
    return request({
      url: `${CLAIMS_URL}/export-notify-account`,
      method: 'POST',
      data: { data },
      responseType: 'blob',
    }).then(fileResult);
  },
  exportList(data: any) {
    return request({
      url: `${CLAIMS_URL}/export-request-list`,
      method: 'POST',
      data: { data },
      responseType: 'blob',
    }).then(fileResult);
  },
  exportAccList(data: any) {
    return request({
      url: `${CLAIMS_URL}/export-account-list`,
      method: 'POST',
      data: { data },
      responseType: 'blob',
    }).then(fileResult);
  },
  print(data: any): Promise<IPrintResponseDoc> {
    return request({
      url: `${CLAIMS_URL}/print-notify-request`,
      method: 'POST',
      data: { data },
    }).then(result => result.data.data.data[0]);
  },
  printAcc(data: any) {
    return request({
      url: `${CLAIMS_URL}/print-notify-account`,
      method: 'POST',
      data: { data },
    }).then(result => result.data.data.data[0]);
  },
  printList(data: any): Promise<IPrintResponseDoc> {
    return request({
      url: `${CLAIMS_URL}/print-request-list`,
      method: 'POST',
      data: { data },
    }).then(result => result.data.data.data[0]);
  },
  printAccList(data: any): Promise<IPrintResponseDoc> {
    return request({
      url: `${CLAIMS_URL}/print-account-list`,
      method: 'POST',
      data: { data },
    }).then(result => result.data.data.data[0]);
  },
  delete(ids: any) {
    return request({
      url: `${CLAIMS_URL}/delete`,
      method: 'POST',
      data: { ids },
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    }).then(result => result.data.map((x: IServerResp<{ data: IAccountClaims }>) => x.data.data));
  },
  getLocResource: () =>
    request({
      url: '/api/reservation-request/i18n/ru_Ru',
    }).then(x => x.data),
};
