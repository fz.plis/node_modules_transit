import dayjs from 'dayjs';
import { snapshot } from '@platform/tools/istore';
import { EMPTY_OPEN_ACCOUNT, verifySign } from '../fake-data/open-account';
import type {
  IMetaData,
  ICollectionResponse,
  IOpenAccount,
  IContract,
  IServerResp,
  IDigest,
  ISignVerify,
  ISign,
  IClientContractsResponse,
} from '../interfaces';
import { getNewDictionaryService, request } from '../utils';

export const OPEN_ACCOUNT_URL = '/api/opening-request/opening-request';

const contracts: IContract[] = [
  {
    id: 'contract_1',
    number: '123450364',
    date: dayjs('2019-02-10').toISOString(),
    accounts: [
      {
        id: 'account_1',
        accountNumber: '40606810000000001070',
      },
      {
        id: 'account_2',
        accountNumber: '40606810000000001071',
      },
    ],
    subsidiaryNumber: '001', // номер филиала
    // eslint-disable-next-line @eco/no-missing-localization
    subsidiaryName: 'Северный', // название филиала
    subsidiaryId: 'c6a644e1-8622-4f2a-92c8-59dc8ddb4d97', // id филиала
    subsidiaryBic: '044525823', // bic БАНКА
  },
  {
    id: '2',
    number: '987654321',
    date: dayjs('2019-03-21').toISOString(),
    accounts: [
      {
        id: 'account_1',
        accountNumber: '40606810000000001073',
      },
    ],
    subsidiaryNumber: '001', // номер филиала
    // eslint-disable-next-line @eco/no-missing-localization
    subsidiaryName: 'Северный', // название филиала
    subsidiaryId: 'c82d6f55-a070-4a3b-a069-f81e76d3ee25', // id филиала
    subsidiaryBic: '044525823', // bic БАНКА
  },
];

const SERVER_DELAY = 0;
const emulator = <TArgs = any[], TResult = any>(cb: (args: TArgs, ...other: any[]) => any, delay: number = SERVER_DELAY) => (
  arg: TArgs,
  ...other: any[]
): Promise<TResult> => new Promise(resolve => setTimeout(() => resolve(cb(arg, ...other)), delay));

/* const clObject = (data: any): any => {
  if (typeof data === 'string') {
    return data === '' ? undefined : data
  } else if (data === null) {
    return undefined
  } else if (Array.isArray(data)) {
    if (data.length) {
      const d: any[] = []
      data.forEach(el => {
        const r = clObject(el)
        return r ? d.push(r) : dayjs
      })
      return !!d.length ? d : undefined
    } else {
      return undefined
    }
  } else if (typeof data === 'object') {
    if (isNotEmptyObj(data)) {
      const d = Object.keys(data).reduce((result: object, key: string) => {
        const value = clObject(data[key])
        return value === undefined ? { ...result } : { ...result, [key]: value }
      }, {})
      return isNotEmptyObj(d) ? d : undefined
    }
    return
  } else {
    return data
  }
} */

const { getList, getCounter, get } = getNewDictionaryService<IOpenAccount>(OPEN_ACCOUNT_URL);

const returnResult = (result: any) => {
  const { data, errorInfo } = result.data;

  return { errorInfo, data };
};

export const openAccountService = {
  getList,
  getCounter,
  get,
  newDocument: emulator<any, IOpenAccount>(() => EMPTY_OPEN_ACCOUNT),
  create: (openAccountData: IOpenAccount): Promise<IOpenAccount> => {
    const { id, ...dataToSend } = openAccountData as any;

    return request({
      url: OPEN_ACCOUNT_URL,
      method: 'POST',
      data: { data: dataToSend },
    }).then(r => {
      const { data, errorInfo } = r.data;

      if (errorInfo) {
        throw errorInfo;
      }

      return data;
    });
  },
  update({ id, ...dataToSend }: IOpenAccount): Promise<IServerResp<IOpenAccount>> {
    return request({
      url: OPEN_ACCOUNT_URL,
      method: 'PUT',
      data: { id, data: snapshot(dataToSend) },
    }).then(returnResult);
  },
  delete(list: string[]): Promise<IServerResp<IOpenAccount>> {
    return request({
      url: `${OPEN_ACCOUNT_URL}/delete`,
      method: 'POST',
      data: { list },
    }).then(returnResult);
  },
  digest: (id: string): Promise<IServerResp<IDigest>> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/generate-digest`,
      method: 'POST',
      data: { data: id },
    }).then(resp => resp.data),
  verifySign: emulator<string, IServerResp<ISignVerify>>(id => ({ data: verifySign(id) })),
  sign: (data: ISign): Promise<IOpenAccount> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/sign`,
      method: 'POST',
      data,
    }).then(r => r.data.data),
  send: (ids: string[]): Promise<IServerResp<IOpenAccount[]>> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/send`,
      method: 'POST',
      data: { list: ids },
    }).then(r => ({
      data: r.data.list,
    })),
  getReservedAccounts: (id: string): Promise<IClientContractsResponse> =>
    request({
      url: `${OPEN_ACCOUNT_URL}/get-client-reserved-accounts/${id}`,
      method: 'GET',
      data: { id },
    }).then(r => r.data.data),
};

export const contractService = {
  getList: emulator<IMetaData, ICollectionResponse<IContract>>((_: IMetaData) => ({
    data: contracts,
    total: contracts.length,
  })),
};
