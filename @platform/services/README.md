# @platform/services

Основной платофрменный пакет, используемый в любом стриме.

## Миграция на свежие версии `@platform/services`

Обычно можно обновляться на самую свежию версию.
Если с момента последних изменений в стриме было выпущено несколько Breaking-версий, то нужно выполнить рекомендации по обновлению для самой свежей версии, а при необходимости провести операции, указанные в предыдущих руководствах.

### [Руководство по переходу на v3](./docs/migration-guide/v3.md)

### [Руководство по переходу на v4 или v5](./docs/migration-guide/v5.md)

## Установка и запуск

```
npm i -D -E @platform/services
```

При установке дополнительно ставит все остальные платформенные пакеты совместимой версии:

- @platform/root
- @platform/core
- @platform/ui
- @platform/communicator

### Использование в стримах

```ts
// Импорт общих функций для банковской и клиентской части.
import { /* Список импортируемого апи */ } from '@platform/services';
// Импорт клиентских функций.
import { /* Список импортируемого апи */ } from '@platform/services/client';
// Импорт банковских функций.
import { /* Список импортируемого апи */ } from '@platform/services/admin';

```

### Запуск

Можно запускать проект с проксированием запросов на разные стенды (sandbox, stage, release)

1. Запуск с прокси сервером sandbox (по умолчанию):
```
npm run start
npm run start:admin
```

2. Запуск с прокси сервером stage:
```
npm run rc
npm run rc:admin
```

3. Запуск с прокси сервером release:
```
npm run prod
npm run prod:admin
```

## Механизм авторизации

### Как это работает в теории

Какое-либо приложение обращается на урл авторизации сервиса, использующего oauth. Обычно этот урл имеет вид `https://example.com/authorize`. К этому урлу в search-параметрах дописывают, куда юзер должен вернуться после прохождения авторизации, идентификатор приложения, использующего oauth-сервис, запрашиваем скоуп доступа, опционально ключ сверки запроса и код `nonce` (number used once) для предотвращения replay-атак.

При обращении на этот урл возможны 3 сценария:
- юзер уже был авторизован
- юзер не был авторизован
- юзер не предоставил разрешения на запрашиваемый скоуп

#### Юзер был авторизован

Если юзер был авторизован, то он вернется на урл, указанный в запросе авторизации. Обычно, этот урл имеет вид `https://example.com/callback`. В search-параметрах ему возвращается либо описание ошибки, либо токен с клчом сверки запроса, отправленного ранее. Этот токен нужно использовать для доступа к запрашиваемым ресурсам.

#### Юзер не был авторизован

В этом случае юзер попадает на страницу логина. Там он проходит идентификацию и авторизацию (логин/пароль, смс-код, прочее). В случае успешной авторизации, он вернется на урл, который был указан в запросе авторизации.

#### юзер не предоставил разрешения на запрашиваемый скоуп

Даже после успешной авторизации, юзера могут отправить на страницу подтверждения доступа. Обыно на этой странице 2 кнопки (разрешить, запретить).

### Реализация

При старте приложения в `beforeUsage` происходит запрос урлов oauth-авторизации с бекенда.

Пример запроса и ответа

```bash
GET http://sandbox.jtc.ooo:21100/api/uaa/.well-known/openid-configuration
```

```json
{
  "issuer": "http://sandbox.jtc.ooo:9012/",
  "authorization_endpoint": "http://sandbox.jtc.ooo:9012/oauth/authorize",
  "token_endpoint": "http://sandbox.jtc.ooo:9012/oauth/token",
  "userinfo_endpoint": "http://sandbox.jtc.ooo:9012/oauth/open-id/userinfo",
  "introspection_endpoint": "http://sandbox.jtc.ooo:9012/oauth/token/introspect",
  "jwks_uri": "http://sandbox.jtc.ooo:9012/.well-known/jwks.json",
  "response_types_supported": ["token","id_token","code"],
  "login_uri: "http://sandbox.jtc.ooo:21120",
  "logout_uri: "http://sandbox.jtc.ooo:21120/logout"
}
```

В этом ответе нас интересуют `authorization_endpoint` и `logout_uri`. На первый урл приложение обращается за токеном авторизации, но второй для логаута из ЭКО.

Если при отправке сетевого запроса получили http-статус `401`, то пытаемся пройти oauth-авторизацию.
Идет редирект на url авторизации, полученный с бекенда.
В `search` урла авторизации добавляется идентификатор фронт-приложения `client_id`, урл редиректа `redirect_uri`.

Пример урла редиректа

```bash
http://sandbox.jtc.ooo:9012/oauth/authorize?response_type=token&redirect_uri=sandbox.jtc.ooo:21120/accounts&client_id=client_id
```

Далее, юзер проходит процедуру логина (или не проходит, если у него есть активная сессия).

В случае успеха, с логина юзер вернется на `<redirect_uri>?<search>`. И в `search` вернется либо ошибка, либо токен.

Примеры
```bash
// url с ошибкой

http://sandbox.jtc.ooo:21120/accounts?error=invalid_client&error_description=error_description

// урл с токеном
http://sandbox.jtc.ooo:21120/accounts?access_token=access_token&expires_in=86000
```

Как только приложение видит, что сейчас оно получило search-параметры, то читает код ошибки с урла. Если ошибки нет и есть `access_token`, то сохраняем `access_token` и убирает search-параметры из урла.

`access_token` сохраняется в localStorage и используется при сетевых запросах.

Чтобы совершить логаут, приложение редиректит на `logout_url` и с search-параметром `redirect_uri`.

Пример урла логаута

```bash
http://sandbox.jtc.ooo:2120/logout?redirect_uri=sandbox.jtc.ooo:21120/accounts/profile
```

При обращении на этот урл, фронт-приложение выполняет запрос логаута на бекенд. Запрос идет именно со страницы логина, потому что там хранятся куки сессии. Как только с бекенда получен ответ с логаута, приложение редиректит на `redirect_uri`. 

## Локализация

Объявляемые константы, использующие локализацию должны быть определены через функцию или геттер.

Так нужно делать потому, что в каком-либо стриме (например gpbeco-fe-login-page) произойдет обращение к локализации до того, как локализация `_services` будет загружена.

Будет выброшена ошибка:
```
index.js:1 Uncaught Error: Translate error: area _services not found!
```

### Примеры

Одиночная константа

```js
import { locale } from 'localization`

// Ошибка
export const LK_DEFAULT_PAGE_TITLE = locale.pageTitles.header

// Работает корректно
export const getDefaultPageTitle = () => locale.pageTitles.header
```

Объект с константами

```js
import { locale } from 'localization`

// Ошибка
const PASS_ERRORS = {
  PASSWORD_TOO_SHORT: return locale.errors.passwordErrors.minLength,
  PASSWORD_TOO_LONG: locale.errors.passwordErrors.maxLength,
}

// Работает корректно
const PASS_ERRORS = {
  get PASSWORD_TOO_SHORT() {
    return locale.errors.passwordErrors.minLength
  },
  get PASSWORD_TOO_LONG() {
    return locale.errors.passwordErrors.maxLength
  },
}
```

### Использование предварительного обработчика при смене организации

Если до смены организации нужно сделать дополнительную проверку, будь то дополнительная валидация или запрос, от которого зависит нужно менять организацию или нет, можно использовать **beforeOrgChange**

```js
  import { locale } from 'localization'
  import { dialog } from '@platform/ui/src/dialogs'

  return (
    <MainLayout
      beforeOrgChange={() =>
        new Promise<boolean>((resolve, reject) => {
          const extra = {
            onCancel: reject,
            okButtonText: locale.dialog.button.Yes,
          }
          // перед сменой организации, вызывается подтверждение действий пользователя
          dialog.showConfirmation("Вы уверены, что хотите поменять организацию?", () => resolve(true), extra)
        })
      }
    >
      {children}
    </MainLayout>
  )
```

## Доступность пунктов бокового меню

В настоящее время доступность пунктов меню регулируется двумя механизмами:
* привилегиями пользователя
* типом контекста (context_type) пользователя
* сегменты доступа

Тип контекста хранится в токене и бывает двух видов (на текущий момент): юзер ЭКО, юзер БСС

Настройку доступности отдельных пунктов меню необходимо производить в fe-services src/resources/app.config.client.prod.json

Пример настройки пункта Банковское сопровождение контрактов:
```json
{
  "icon": "Deposit",
  "label": "bsc.label",
  "path": "/redirect/bsc?uri=https%3A%2F%2Fexample.com",
  "authorities": ["BSS_SMOOTH_REDIRECT"],
  "allowedContexts": ["ECO", "BSS_DBO_320"],
  "segments": ["CAS_DBO_BSS_FRAME"]
}
```

В начале выполняется проверка по типу контекста. Контекст пользователя должен **присутствовать** в поле allowedContexts конфига.

Затем отфильтрованные по контексту пункты меню проходят проверку на привилегии пользователя. У пользователя должен быть **все** привилегии, указанные в поле authorities конфига.

## [Ограничение доступа к роутам](./docs/route-guards.md)

Можно ограничивать доступ к роутам стрима, используя компонент `SecureRoute`.
В компонент передается пропс `isAuthenticated`, если он равен `true`, то компонент продолжает работу как обычный `Route` из `react-router`. Если признак `isAuthenticated` - false, то рендерится компонент, переданный в `fallback`

```tsx
import React from 'react'
import type { RouteProps } from 'react-router';
import { SecureRoute } from '@platform/services';

const AdminPage: React.FC = () => <h1>Приватная страница</h1>;
const Fallback: React.FC = () => <h1>Требуется авторизация</h1>;
const checkauth = () => !!localStorage.getItem('token');

const RouteWithAuth: React.FC<RouteProps> = props => (
  <SecureRoute
    {...props}
    isAuthenticated={checkauth()}
    fallback={Fallback}
  />
);

export const routes = [
  <RouteWithAuth path="/admin" component={AdminPage}>
];

```

## Генератор страницы скроллера

[Руководство по генерации скроллера](./docs/generators/universal-scroller.md)

Чтобы не составлять каждый раз страницу скроллера разработаны вспомогательные функции:

* Для генерации скроллера с карточками, то используем функцию `getCardScrollerPage`. Как правило скроллер с карточками используется в клиентской части ([пример макета](https://app.zeplin.io/project/5e9e497d77ad30524b133b71/screen/5ec7b921658ec345e1203ced)).

* Для генерации скроллера с таблицей, нужно воспользоваться функцией `getRowScrollerPage`. Обычно такой скроллер встречается в банковской части, но может и в клиентской ([пример макета](https://app.zeplin.io/project/5e9e497d77ad30524b133b71/screen/5f436fde28bc5f2d497a0a80)).


### Миграция с существующих решений

* [Руководство по миграции с устаревшего скроллер-генератора карточек на новый](./docs/generators/migration-from-old-scroller-generator.md)

* [Руководство по миграции с хуков на генератор](./docs/generators/migration-from-scroller-hooks-to-generator.md)


### [Хуки, используемые в скроллере](./docs/generators/scroller-hooks.md)

## [Конфиг приложения](./docs/app-config.md)

## [Руководство по провайдеру аутентификации](./docs/auth.md)

## [Руководство по провайдеру элемента загрузки](./docs/loader.md)

## [Руководство по провайдеру пользователя](./docs/user.md)

## [Руководство по компоненту согласие](./docs/consent.md)

## [Руководство по вызову методов из других стримов](./docs/consent.md)

## [Руководство по использованию хука таймера(обратного отсчета)](./docs/timer.md)



