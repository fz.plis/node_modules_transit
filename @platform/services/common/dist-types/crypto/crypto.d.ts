import type { ICryptoModule } from '../interfaces';
export declare const getCryptoModule: () => ICryptoModule;
export declare const setCryptoModule: (val: ICryptoModule) => ICryptoModule;
