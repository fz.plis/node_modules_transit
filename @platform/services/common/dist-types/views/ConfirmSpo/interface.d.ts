/** Интерфейс формы "Изменение статуса сертификата". */
export interface IConfirmSpoForm {
    /** Комментарий для клиента. */
    noteForClient: string;
}
/** Пропы, передающиеся по вызову модального окна форма "Изменение статуса сертификата". */
export interface IConfirmProps {
    /** Функция, вызываемая по закрытию модального окна. */
    onClose(): void;
    /** Функция, вызываемая по нажатию на кнопку "Продолжить". */
    onConfirm(noteForClient: string | undefined): void;
    /** Заголовок модального окна. */
    title: string;
    /** Описание. */
    description: string;
    /** Наличие валидации в форме. */
    validate?: boolean;
}
