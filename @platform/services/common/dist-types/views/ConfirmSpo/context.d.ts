/// <reference types="react" />
import type { IConfirmProps } from './interface';
export declare const ConfirmSpoContext: import("react").Context<IConfirmProps>;
