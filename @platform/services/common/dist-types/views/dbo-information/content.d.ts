import React from 'react';
/**
 * Наполнение ЭФ "Вывод информационного уведомления при входе в АС Экосистема для пользователей ДБО БСС".
 * Включает в себя кнопку просмотра PDF-файла с информацией для новых пользователей.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=25005123
 */
export declare const DboInformationContent: React.FC;
