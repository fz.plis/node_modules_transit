import React from 'react';
import type { IHistoryResponse, BankClientName } from '../../interfaces';
export interface IBankHistoryDialogProps {
    /**
     * Коллекция записей истории изменений.
     */
    data: IHistoryResponse[];
    /**
     * Наименование клиента.
     */
    bankClient: BankClientName;
    /**
     * Заголовок.
     */
    header: string;
    /**
     * Обработчик нажатия на имя юзера.
     */
    onUserNameClick?(userId: string): void;
    /**
     * Обработчик закрытия модального окна.
     */
    onClose(): void;
    /**
     * Функция, возвращающая локализованный статус записи истории.
     *
     * @param arg - Запись истории.
     */
    getStatusLabel(arg: string): string;
}
export declare const BankHistoryDialog: React.FC<IBankHistoryDialogProps>;
