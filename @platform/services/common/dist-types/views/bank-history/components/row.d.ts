import React from 'react';
import type { BankHistoryData } from '../../../interfaces';
/**
 * Пропы для записи истории изменений.
 */
export interface IHistoryRowProps {
    /**
     * Данные для отображения.
     */
    data: BankHistoryData;
    /**
     * Обработчик нажатия на имя юзера.
     */
    onUserNameClick?(): void;
}
export declare const HistoryRow: React.FC<IHistoryRowProps>;
