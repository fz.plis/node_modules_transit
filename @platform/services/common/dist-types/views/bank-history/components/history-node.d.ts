import React from 'react';
/** Компонент отображения блока информации в строке истории изменений. */
export declare const HistoryNode: React.FC<{
    value: string;
    dataField: string;
    hasTooltip?: boolean;
    tooltipText?: string;
    onClick?(): void;
}>;
