import type { BankHistoryData, IHistoryResponse } from '../../interfaces';
/**
 * Функция-преобразователь для данных сущности истории.
 *
 * @param data - Сущность истории.
 * @param getStatusLabel = Функция, возвращающая локализованный статус.
 */
export declare const converterData: (data: IHistoryResponse, getStatusLabel: (arg: string) => string) => BankHistoryData;
