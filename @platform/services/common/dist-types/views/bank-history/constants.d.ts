/**
 * Поля записи истории изменений.
 */
export declare const HISTORY_FIELDS: {
    CHANGE_ID: string;
    STATUS: string;
    USER_TYPE: string;
    USER_NAME: string;
    COMMENT_FOR_CLIENT: string;
    COMMENT_FOR_BANK: string;
};
