import React from 'react';
import type { IUserCertificate } from '../../interfaces';
export interface IUserListProps {
    options?: IUserCertificate[];
    selectedOption: IUserCertificate | null;
    onSelectOption?(option: IUserCertificate): void;
}
export declare const CertificateList: React.FC<IUserListProps>;
