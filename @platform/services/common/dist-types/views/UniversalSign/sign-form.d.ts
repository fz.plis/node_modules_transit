import React from 'react';
import type { IUserCertificate, IUniversalSignEntity } from '../../interfaces';
export interface IUniversalSignDocumentsProps {
    documents: IUniversalSignEntity[];
}
export interface IUniversalSignDocumentProps {
    document: IUniversalSignEntity;
}
declare type RenderSummary = (props: IUniversalSignDocumentsProps) => React.ReactNode;
declare type RenderDocument = (props: IUniversalSignDocumentProps) => React.ReactNode;
export interface IUniversalSignProps extends IUniversalSignDocumentsProps {
    certs: IUserCertificate[];
    renderSummary?: RenderSummary;
    renderDocument: RenderDocument;
    onClose(): void;
    onSignClick(thumbprint: string, cert: IUserCertificate): void;
    selectedCertificate?: IUserCertificate;
}
interface ISignState {
    currentCertificate: IUserCertificate | null;
    userCertificates: IUserCertificate[];
}
export declare class UniversalSign extends React.Component<IUniversalSignProps, ISignState> {
    static defaultProps: any;
    constructor(props: IUniversalSignProps);
    renderContent(): JSX.Element;
    private handleSignClick;
    private handleCancel;
    private onSelectCertificate;
    render(): JSX.Element;
}
export {};
