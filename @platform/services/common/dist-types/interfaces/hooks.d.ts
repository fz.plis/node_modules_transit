/** Таймер.  */
export interface ITimer {
    /** Текущее значение.  */
    value: number;
    /** Метод запуска.  */
    start(interval?: number): void;
    /** Метод остановки.  */
    stop(): void;
    /** Метод сброса.  */
    reset(): void;
    /** Метод перезапуска.  */
    restart(interval?: number): void;
}
