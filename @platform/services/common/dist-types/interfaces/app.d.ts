import type { MenuIcons, PartnersIcons, MENU_ITEM_TYPES } from '@platform/ui';
import type { CRYPTO_PLUGIN_TYPE, GenerateKeyParams } from '../crypto/olk/crypto-module-interface';
import type { USER_CONTEXT_TYPE } from './common';
export interface IBaseMenuItem {
    /**
     * Key of feature.
     *
     * If exists menu item will be checked by experimental features.
     */
    key?: string;
    /**
     * Icon of menu item.
     */
    icon: keyof typeof MenuIcons;
    /**
     * Localization key.
     */
    label: string;
    /**
     * Url to redirect.
     *
     * If url starts with `http` app will open new tab.
     */
    path: string;
    /**
     * List of authorities.
     */
    authorities?: string[];
    /**
     * List of allowed context types.
     */
    allowedContexts?: USER_CONTEXT_TYPE[];
    /**
     * Список сегментов, требуемых для отображения пункта меню.
     */
    segments?: string[];
    /**
     * Признак раскрытия пункта.
     */
    collapsed?: boolean;
    /**
     * Тип элемента меню.
     */
    type?: MENU_ITEM_TYPES;
}
export interface IMenuItem extends IBaseMenuItem {
    /**
     * Список пунктов меню второго уровня.
     */
    options?: IBaseMenuItem[];
    /**
     * Идентификатор пункта меню для возможности раскрытия второго уровня из стрима.
     */
    key?: string;
}
/**
 * Конфиг криптомодуля.
 */
export interface ICryptoModuleConfig {
    /**
     * Тип криптографической защиты информации.
     */
    cipfClass: string[];
    /**
     * Наименование криптографической защиты информации.
     */
    cipfName: string;
}
/**
 * Конфиг криптоплагина.
 */
export interface ICryptoPlugin {
    /**
     * URL доступа к криптоплагину.
     */
    url: string;
    /**
     * Минимальная версия криптоплагина.
     */
    minVersion: string;
}
/**
 * Конфиг продукта.
 */
export interface IProductItem extends IBaseMenuItem {
    /**
     * Идентификатор пункта меню для возможности раскрытия второго уровня из стрима.
     */
    menuId?: string;
}
/**
 * Конфиг баннера.
 */
export interface IBannerItem {
    /**
     * Текст на баннере.
     */
    label: string;
    /**
     * Задний фон баннера.
     */
    image: string;
    /**
     * Ссылка куда ведёт баннер.
     */
    path: string;
}
/**
 * Партнер через которого осуществляется вход в систему.
 */
export interface IAuthByPartner {
    /**
     * URL аутентификации.
     */
    authUrl: string;
    /**
     * Наименование.
     */
    name: string;
    /**
     * Иконка.
     */
    icon: keyof typeof PartnersIcons;
}
/**
 * Вход через.
 */
export interface IAuthBy {
    /**
     * Признак включения входа через.
     */
    enabled: boolean;
    /**
     * Список партнеров.
     */
    partners: IAuthByPartner[];
    /**
     * Доступность входа через для старых браузеров.
     */
    availableForOldBrowser?: boolean;
}
/**
 * Конфиг приложения.
 */
export interface IAppConfig {
    /**
     * Меню.
     */
    menu: IMenuItem[];
    /**
     * Продукты (для Главной страницы).
     */
    products: IProductItem[];
    /**
     * Баннеры для главной страницы.
     */
    banners: IBannerItem[];
    /**
     * Адрес тестовой страницы ДБО БСС.
     */
    bss_iframe_url: string;
    /**
     * BSS iframe url.
     */
    bss_news_iframe_url: string;
    /**
     * URL главной страницы BSS.
     */
    bss_mainpage_iframe_url: string;
    /**
     * URL счетов BSS.
     */
    bss_accountpage_iframe_url: string;
    /**
     * URL запроса на отмену BSS.
     */
    bss_cancellationrequest_iframe_url: string;
    /**
     * URL платежных поручений BSS.
     */
    bss_paymentorders_iframe_url: string;
    /**
     * URL Платёжных требований BSS.
     */
    bss_payorderru_iframe_url: string;
    /**
     * URL Платёжных требований BSS из банка.
     */
    bss_payorderru_bank_iframe_url: string;
    /**
     * URL Инкассовых поручений BSS.
     */
    bss_collectionorder_iframe_url: string;
    /**
     * URL Инкассовых поручений BSS из банка.
     */
    bss_collectionorder_bank_iframe_url: string;
    /**
     * URL Заявления на акцепт BSS.
     */
    bss_gpbcancelaccept_iframe_url: string;
    /**
     * Адрес нового стенда БСС.
     */
    bss_test_ui_iframe_url: string;
    /**
     * URL импорта файлов BSS.
     */
    bss_import_iframe_url: string;
    /**
     * URL запросов выписки BSS.
     */
    bss_statementquery_iframe_url: string;
    /**
     * URL платежных поручений, требующих визирования BSS.
     */
    bss_paydocruvisa_iframe_url: string;
    /**
     * Признак автоопределения плагина.
     */
    cryptoPluginAutoDetection: boolean;
    /**
     * Криптоплагин по-умолчанию.
     */
    cryptoPluginByDefault: CRYPTO_PLUGIN_TYPE;
    /**
     * Конфиг криптомодуля.
     */
    cryptoModuleConfig: ICryptoModuleConfig;
    /**
     * Информация о криптоплагинах.
     */
    cryptoPlugins: Record<CRYPTO_PLUGIN_TYPE, ICryptoPlugin>;
    /**
     * Информация об установочных файлах криптоплагинов.
     */
    cryptoPluginsInstaller: Record<CRYPTO_PLUGIN_TYPE, {
        [key in 'nix' | 'win']: string;
    }>;
    /**
     * Урл перехода к перевыпуску сертификата.
     */
    regenQESUrl: string;
    /** Флаг для быстрого переключения дизайна главной страницы БСС. */
    newMainPageForBSS: boolean;
    /**
     * Если `true` - включает возможность выбора "Счёта участника закупок" в онбординге.
     */
    acc_open_first_on_acc_zak: boolean;
    /**
     * Если `true` - включает возможность выбора "Счёта участника закупок" в O2+NEW.
     */
    on_open_sec_acc_zak: boolean;
    /**
     * URL для перехода в ГПБ Факторинг (Отправить заявку).
     */
    factoring_send_url: string;
    /**
     * URL для перехода в ГПБ Факторинг (Для клиентов факторинга).
     */
    factoring_open_url: string;
    /**
     * Вход через.
     */
    authBy: IAuthBy;
    /**
     * Интервал обновления доступности сервисов (в секундах).
     */
    healthCheckInterval?: number;
    /**
     * Настройки криптомодуля для генерации ключа и запроса на сертификат НЭП.
     */
    cryptoModuleGenerateKeySettings: Omit<GenerateKeyParams, 'containerName' | 'subject'>;
    /**
     * Вызов IFRAME п/п на создание с предзаполненными полями.
     */
    fns_autocreatePaydocru: boolean;
    /**
     * Доступность кнопки чата.
     */
    chatVisibility: boolean;
    /**
     * Условие доступности выбора УЦ ЭТП ГПБ (в профиле).
     */
    show_uc_etpgpb_on_wizard: boolean;
    /**
     * Условие отображения разных версий блоков "Счета" главной страницы, полей ЭФ "Настройки счетов", ЭФ "Фильтры счетов".
     */
    main_page_on_eb: boolean;
    /**
     * Список допустимых издателей СПО (временное решение до появления соответствующего признака в СПО).
     */
    gpb_issuer_names: string[];
    /**
     * Настройка показа модалки для ДБО пользователя.
     * В рамках задачи GBO-13837 сделана модалка для показа ДБО БСС пользователю информация о том, что Клиент-Банк переведён на новую технологическую платформу.
     */
    displayInfoTechModalForDBO: boolean;
}
