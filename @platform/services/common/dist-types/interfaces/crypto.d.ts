import type { IGPBCryptoModuleCheckResponseData, IGPBCryptoModuleResultInitKey } from '../crypto/olk/crypto-module-interface';
import type { AUTHORITY_VALIDATORS } from './common';
import type { DOCUMENT_TYPE_CODE, IUniversalSignEntity, UserType } from './entities';
import type { ICertFilter } from './org-cert';
/**
 * Данные сертификата, пока используем то, что отдает КриптоПро.
 */
export interface ICertificate {
    hasPrivateKey: boolean;
    issuer: any;
    serialNumber: string;
    subject: any;
    thumbprint: string;
    certificate: string;
    validTo: Date;
    validFrom: Date;
}
/**
 * Функция подписи.
 *
 * @param thumbprint Хеш сертификата, которым подписываем.
 * @param data Подписываемые данные.
 * @param sessId Сессия подписи.
 * @param detached Признак открепления подписи.
 * @returns Строка подписи.
 */
export declare type SignFunc = (thumbprint: string, data: string, options?: any) => Promise<string>;
export declare type BatchSignFunc = (thumbprint: string, data: string[], sessId?: string, detached?: boolean) => Promise<string[]>;
export declare enum VERIFY_SIGN_ERROR {
    SIGN_NOT_CORRECT = 200,
    VERIFYING_CERTIFICATE_CHAIN = 201,
    INVALID_SIGN_CERTIFICATE = 202,
    UNKNOWD = "UNKNOWN"
}
/**
 * Интерфейс ошибки при проверке криптомодулем.
 */
export interface IVerifySignError {
    code: VERIFY_SIGN_ERROR;
    description: string;
}
/**
 * Интерфейс проверки валидности крипты.
 */
export interface IVerifySignResult {
    data?: IGPBCryptoModuleCheckResponseData;
    valid: boolean;
    error?: IVerifySignError;
}
/**
 * Функция проверки подписи.
 *
 * @param signature Хеш подписи.
 * @param data Подписываемые данные (дайджесты).
 * @param certificateId Id сертификата, которым были подписаны данные.
 * @param certificatesCA Список УЦ для данного сертификата.
 * @param crls Список СОС для данного сертификата.
 * @returns Строка подписи.
 */
export declare type VerifySignFunc = (signature: string, data: string[], certificateId: string, certificatesCA: string[], crls: string[], providerName?: string) => Promise<IVerifySignResult>;
/**
 * Интерфейс крипто модуля.
 */
export interface ICryptoModule {
    /**
     * Функция инициализации.
     */
    init(options?: any): Promise<any>;
    sign: SignFunc;
    batchSign: BatchSignFunc;
    verifySign: VerifySignFunc;
    getKeySession(certificate: string): Promise<IGPBCryptoModuleResultInitKey>;
    /**
     * Функция получения списка сертификатов.
     */
    getCertificates(): Promise<ICertificate[]>;
    /**
     * Функция проверки установлен ли крипто модуль.
     */
    installationCheck(): Promise<any>;
    /**
     * Функция получения списка сертификатов из хранилища.
     */
    getUserStorageCertificates(accreditedCerts: string[], certificateFilter: ICertFilter): Promise<IUserOrgCertificate[]>;
    /**
     * Функция получения версий доступных криптомодулей пользователя.
     */
    getAvailableCryptoModulesVersion(): Promise<{
        bss?: string;
        sft?: string;
    }>;
}
export interface IUserCertificate {
    absId: string;
    data: string;
    id: string;
    organizationName: string;
    serialNumber: string;
    validFrom: string;
    validTo: string;
    origin: string;
    userId: string;
    userName: string;
    signParams: string;
    containerNameLocation: string;
    ownerName?: string;
    issuerName?: string;
    /** Владелец сертификата. */
    subject?: string;
    /** Издатель сертификата. */
    issuedBy?: string;
}
export interface IUserCertificateCollectionResponse<TRow> {
    page: TRow[];
    size: number;
}
export interface IUserCertificateUpdateResponce {
    code: number;
    data: number;
    message: string;
}
export interface IAuthorityRequest {
    certificateIds: string[];
    clientAbsId: string;
    clientEskId?: string;
    clientInnKio: string;
    clientKpp: string;
    clientName: string;
    clientOgrn: string;
    documentType?: DOCUMENT_TYPE_CODE;
    validators: AUTHORITY_VALIDATORS[];
    productCodes: string[];
}
export interface IAuthorityResponse {
    certificateId: string;
    result: boolean;
    validatorResults: Array<{
        result: boolean;
        type: AUTHORITY_VALIDATORS;
        message?: string;
    }>;
    /** Должность представителя. */
    clientPosition?: string;
    /** Документ позволяющий подписать заявление. */
    authorizingDocument?: string;
}
export interface IUserOrgSubject {
    commonName: string;
    organizationalUnit: string;
    organization: string;
    country: string;
    state: string;
    locality: string;
    inn: string;
    ogrn?: string;
    snils: string;
    ogrnip?: string;
    email?: string;
    principalAttrsAsString: string;
}
export interface IUserOrgIssuer {
    commonName: string;
    organizationalUnit?: string;
    organization: string;
    country: string;
    state: string;
    locality: string;
    inn: string;
    ogrn?: string;
    snils?: string;
    ogrnip?: string;
    email?: string;
    principalAttrsAsString: string;
}
export interface IUserOrgCertificate {
    serialNumber: string;
    subject: IUserOrgSubject;
    issuer: IUserOrgIssuer;
    validFrom: string;
    validUntil: string;
    publicKey: string;
    thumbprint: string;
    cipfName: string;
    cipfClasses: string[];
    body: string;
}
export interface IVerifySignByDateRequest {
    date: string;
    verifySignFields: [
        {
            certificateId: string;
            signData: string;
            signId: string;
        }
    ];
}
export declare type IVerifySignByDateResponce = Record<string, {
    caCertificates: string[];
    crls: string[];
}>;
export declare enum AUTHORITIES_TYPES {
    RKO = "RKO"
}
export interface IUniversalSignResponse {
    certificates: IUserCertificate[];
    signDocuments: IUniversalSignEntity[];
}
/**
 * Данные о подписи.
 */
export interface IVerifySignData {
    /**
     * Подпись.
     */
    signData: string;
    /**
     * Дата проверки подписи.
     */
    signDate: string;
    /**
     * Идентификатор подписи.
     */
    signId: string;
    /**
     * Идентификатор пользователя подписавшего документ.
     */
    uaaUserId: string;
}
/**
 * Свойства запроса получения дополнительной информации проверки подписи.
 */
export interface IAdditionalVerifySignRequestData {
    /**
     * Данные о подписях.
     */
    signaturesData: IVerifySignData[];
}
export declare enum VALIDATE_SIGN_ERRORS {
    CANT_RETRIEVE_INFO_ABOUT_SIGNER_OR_CERTIFICATE = "CANT_RETRIEVE_INFO_ABOUT_SIGNER_OR_CERTIFICATE"
}
/**
 * Дополнительная информация о подписи.
 */
export interface IAdditionalVerifySignData {
    /**
     * УЦ.
     */
    caCertificates: string[];
    /**
     * СОС.
     */
    revokedCertificates: string[];
    /**
     * Информация о пользователе.
     */
    signerInfo: {
        /**
         * Фамилия.
         */
        familyName: string;
        /**
         * Имя.
         */
        firstName: string;
        /**
         * Отчество.
         */
        middleName: string;
        /**
         * Тип пользователя.
         */
        type: UserType;
        /**
         * Идентификатор пользователя.
         */
        uaaId: string;
    };
    /**
     * Дата проверки подписи.
     */
    signDate: string;
    /**
     * Идентификатор подписи.
     */
    signId: string;
    /**
     * Ошибка.
     */
    error: VALIDATE_SIGN_ERRORS;
    /**
     * Информация о провайдере.
     */
    providerInfo: string;
}
/**
 * Свойства ответа получения дополнительной информации проверки подписи.
 */
export interface IAdditionalVerifySignResponseData {
    /**
     * Дополнительная информация.
     */
    additionalData: IAdditionalVerifySignData[];
}
