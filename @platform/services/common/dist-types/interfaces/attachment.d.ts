import type { Canceler } from 'axios';
export declare enum ATTACHMENT_STATUS {
    UPLOADED = "UPLOADED",
    CHECKED = "CHECKED",
    MALWARE = "MALWARE",
    STORED = "STORED",
    CONSUMED = "CONSUMED",
    DELETED = "DELETED"
}
export interface IAttachment {
    contentType: string;
    createTime: string;
    dataHash: string;
    dataSize: number;
    fileName: string;
    id: string;
    status: ATTACHMENT_STATUS;
    statusTime: string;
}
export interface IFileObject {
    id: any;
    name: string;
    size: number;
    isLoading?: boolean;
    disabled?: boolean;
    error?: string;
}
export interface IFileUploadOptions {
    onUploadProgress?(e: ProgressEvent): void;
    onCancel?(cancel: Canceler): void;
}
export interface ICheckUploadOptions extends IFileUploadOptions, IPollingOptions {
}
export interface IPollingOptions {
    interval?: number;
    maxTries?: number;
}
export interface IFileBuffer extends IFileObject {
    progress?: number;
    status: FILE_UPLOAD_STATE;
    isMalware?: boolean;
    errorObj?: any;
}
export declare enum FILE_UPLOAD_STATE {
    UPLOADING = "UPLOADING",
    MALWARE_CHECK = "MALWARE_CHECK",
    UPLOADED = "UPLOADED"
}
/**
 * Скачанное вложение.
 */
export interface IDownloadedAttachment {
    /**
     * Имя файла.
     */
    fileName: string;
    /**
     * Тип.
     */
    type: string;
    /**
     * Бинарные данные.
     */
    data: ArrayBuffer;
}
/**
 * Интерфейс файлового сервиса.
 */
export interface AttachmentService {
    /**
     * Метод получения статуса загрузки.
     */
    getStatus(attachmentId: string, onCancel?: (cancel: Canceler) => void): Promise<IAttachment>;
    /**
     * Метод для загрузки вложения.
     */
    upload(file: File, options?: ICheckUploadOptions): Promise<IAttachment>;
    /**
     * Метод получения прогресса загрузки.
     */
    checkUploadProgress(attachmentId: string, options: ICheckUploadOptions): Promise<IAttachment>;
    /**
     * Метод для скачивания файла.
     */
    download(attachmentId: string, accessToken: string): Promise<IDownloadedAttachment>;
    /**
     * Метод для скачивания файла в формате Base64.
     */
    downloadBase64(attachmentId: string, accessToken?: string): Promise<IDownloadedAttachment>;
    /**
     * Метод для одноразового скачивания файла.
     */
    downloadOneTimeData(attachmentId: string, accessToken: string): Promise<IDownloadedAttachment>;
}
