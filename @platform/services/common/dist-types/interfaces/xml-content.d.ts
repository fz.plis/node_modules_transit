export interface IXmlContent {
    id: string;
    xmlBody: string;
}
