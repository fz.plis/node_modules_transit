import React from 'react';
import type { ISortSettingsProps, IActionsBarProps, ICheckboxProps } from '@platform/ui';
import type { ISelectedDocumentsInfo } from '../../../components/selected-documents-info';
export interface IToolbarProps {
    checkBoxProps?: ICheckboxProps;
    actionsBarProps?: IActionsBarProps;
    sortSettingsProps?: ISortSettingsProps;
    selectedDocumentsInfo?: ISelectedDocumentsInfo;
}
export declare const ToolBar: React.FC<IToolbarProps>;
