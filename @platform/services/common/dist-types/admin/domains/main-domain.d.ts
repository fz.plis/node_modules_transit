import type { IBranch, IUserData, USER_ROLE } from '../../interfaces';
import { UserType } from '../../interfaces';
export { hasFatalError } from '../../domains/common-domain';
export declare const user: import("@platform/tools/istore").Unit<import("../../domains/user").IUserDomainState, {
    setToken(val: string, authorities: string[]): void;
    setCryptoPlugin: () => void;
    load: ({ userData, roles }: {
        userData: Partial<IUserData>;
        roles: USER_ROLE[];
    }) => void;
    getAvailableActionsWithCategory: (actions: Record<string, import("../../interfaces").IActionWithAuth[]>) => import("../../interfaces").IButtonConfig<any, any>;
    logout: () => void;
    getAvailableActions: (actions: import("../../interfaces").IActionWithAuth[]) => import("../../interfaces").IActionWebInfo<any, any>[];
    setUserType: (userType: UserType) => void;
    updateData: (data: Partial<IUserData>) => void;
    setRoles: (roles: USER_ROLE[]) => void;
    isAvailableFor: (...roles: USER_ROLE[]) => boolean;
    hasAuthority(...userAuthorities: string[]): boolean;
    hasAllowedContextType(allowedContexts: import("../../interfaces").USER_CONTEXT_TYPE[]): boolean;
}>;
export declare const userBranches: import("@platform/tools/istore").Unit<IBranch[], {
    toggleItem: (item: IBranch) => void;
    set: (val: IBranch[]) => void;
    setItem: (index: number, item: IBranch) => void;
    add: (item: IBranch | IBranch[]) => void;
    remove: (item: IBranch | IBranch[]) => void;
    removeByIndex: (index: number) => void;
    mergeToItem: (index: number, obj: Partial<IBranch>) => void;
}>;
export declare const selectedBranchId: import("@platform/tools/istore").Unit<string, {
    set: (val: string) => void;
}>;
/**
 * @deprecated C 1 февраля Используйте useUser, useAuth вместо.
 */
export declare const mainDomain: import("@platform/tools/istore").Domain<{
    user: import("@platform/tools/istore").Unit<import("../../domains/user").IUserDomainState, {
        setToken(val: string, authorities: string[]): void;
        setCryptoPlugin: () => void;
        load: ({ userData, roles }: {
            userData: Partial<IUserData>;
            roles: USER_ROLE[];
        }) => void;
        getAvailableActionsWithCategory: (actions: Record<string, import("../../interfaces").IActionWithAuth[]>) => import("../../interfaces").IButtonConfig<any, any>;
        logout: () => void;
        getAvailableActions: (actions: import("../../interfaces").IActionWithAuth[]) => import("../../interfaces").IActionWebInfo<any, any>[];
        setUserType: (userType: UserType) => void;
        updateData: (data: Partial<IUserData>) => void;
        setRoles: (roles: USER_ROLE[]) => void;
        isAvailableFor: (...roles: USER_ROLE[]) => boolean;
        hasAuthority(...userAuthorities: string[]): boolean;
        hasAllowedContextType(allowedContexts: import("../../interfaces").USER_CONTEXT_TYPE[]): boolean;
    }>;
    userBranches: import("@platform/tools/istore").Unit<IBranch[], {
        toggleItem: (item: IBranch) => void;
        set: (val: IBranch[]) => void;
        setItem: (index: number, item: IBranch) => void;
        add: (item: IBranch | IBranch[]) => void;
        remove: (item: IBranch | IBranch[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<IBranch>) => void;
    }>;
    selectedBranchId: import("@platform/tools/istore").Unit<string, {
        set: (val: string) => void;
    }>;
    authConfig: import("@platform/tools/istore").Unit<import("../../interfaces").IAuthConfiguration | undefined, {
        set: (val: import("../../interfaces").IAuthConfiguration | undefined) => void;
    }>;
    hasFatalError: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
}, import("@platform/tools/istore").IOnlyMethods<{
    user: import("@platform/tools/istore").Unit<import("../../domains/user").IUserDomainState, {
        setToken(val: string, authorities: string[]): void;
        setCryptoPlugin: () => void;
        load: ({ userData, roles }: {
            userData: Partial<IUserData>;
            roles: USER_ROLE[];
        }) => void;
        getAvailableActionsWithCategory: (actions: Record<string, import("../../interfaces").IActionWithAuth[]>) => import("../../interfaces").IButtonConfig<any, any>;
        logout: () => void;
        getAvailableActions: (actions: import("../../interfaces").IActionWithAuth[]) => import("../../interfaces").IActionWebInfo<any, any>[];
        setUserType: (userType: UserType) => void;
        updateData: (data: Partial<IUserData>) => void;
        setRoles: (roles: USER_ROLE[]) => void;
        isAvailableFor: (...roles: USER_ROLE[]) => boolean;
        hasAuthority(...userAuthorities: string[]): boolean;
        hasAllowedContextType(allowedContexts: import("../../interfaces").USER_CONTEXT_TYPE[]): boolean;
    }>;
    userBranches: import("@platform/tools/istore").Unit<IBranch[], {
        toggleItem: (item: IBranch) => void;
        set: (val: IBranch[]) => void;
        setItem: (index: number, item: IBranch) => void;
        add: (item: IBranch | IBranch[]) => void;
        remove: (item: IBranch | IBranch[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<IBranch>) => void;
    }>;
    selectedBranchId: import("@platform/tools/istore").Unit<string, {
        set: (val: string) => void;
    }>;
    authConfig: import("@platform/tools/istore").Unit<import("../../interfaces").IAuthConfiguration | undefined, {
        set: (val: import("../../interfaces").IAuthConfiguration | undefined) => void;
    }>;
    hasFatalError: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
}>>;
