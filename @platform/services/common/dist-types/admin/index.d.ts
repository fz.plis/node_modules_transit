import type * as commonInstance from '..';
export * from './index.common';
export declare const common: typeof commonInstance;
export declare const alias = "@platform/services";
export * from './domains';
export * from './layouts';
export * from './admin-app';
export * from './user';
