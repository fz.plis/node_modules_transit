import React from 'react';
import type { IUserData } from '../interfaces';
/**
 * Контекст банковского пользователя.
 */
interface IUserContext {
    /**
     * Пользователь.
     */
    user: IUserData;
    /**
     * Установить параметры пользователя.
     *
     * @param user Пользователь.
     */
    setUser(user: Partial<IUserData>): void;
    /**
     * Признак ошибки загрузки пользователя.
     */
    fatalErr: boolean;
}
export declare const getUserValue: () => {
    fatalErr: boolean;
    setUser: (_: IUserData) => void;
    user: IUserData;
};
export declare const getUserItem: <K extends "fatalErr" | "user">(key: K) => IUserContext[K];
export declare const UserProvider: React.FC;
export declare const useUser: () => IUserContext;
export {};
