import type { IServerResp, IBindF1DTO, IClientOfficial } from '../../interfaces';
export declare const USER_BINDING_URL: string;
export declare const userBindingService: {
    bindClient(dto: IBindF1DTO): Promise<IServerResp<IClientOfficial>>;
};
