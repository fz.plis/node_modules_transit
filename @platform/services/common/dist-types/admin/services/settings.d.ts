import type { ISetting } from '../../interfaces';
export declare const appSettingsService: {
    get: (id: string) => Promise<ISetting>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ISetting>>;
    create: (data: ISetting) => Promise<ISetting>;
    update: (data: ISetting) => Promise<ISetting>;
    delete: (id: string) => Promise<ISetting>;
    getAll: () => Promise<ISetting[]>;
    getByCode: (code: string) => Promise<ISetting>;
};
