import type { IOrgCert, IOrgCertFileInfo, IAddCertificateResponse } from '../../interfaces';
import type { IOrganizationCertificatesService } from '../../services/helpers/org-certs';
export interface IOrgCertsService extends IOrganizationCertificatesService {
    getAccredited(): Promise<string[]>;
    reject(orgOfficialCertId: string, noteForClient?: string): Promise<IOrgCert>;
    accept(orgOfficialCertId: string, noteForClient?: string): Promise<IOrgCert>;
    parse(rawCertificate: string): Promise<IOrgCertFileInfo>;
    addUserCertificate(rawCertificate: string, bankClientId: string, userId: string): Promise<IAddCertificateResponse>;
}
/**
 * Сертификаты представителя клиента Банка.
 */
export declare const orgCertsService: IOrgCertsService;
