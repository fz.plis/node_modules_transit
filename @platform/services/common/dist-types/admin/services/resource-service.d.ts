import type { IResource } from '../../interfaces';
export declare const resourceService: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IResource>>;
    getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<IResource>;
    create: (payload: IResource) => Promise<any>;
    searchByName: <T extends keyof IResource>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IResource>>;
};
