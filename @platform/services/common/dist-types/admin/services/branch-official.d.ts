import type { IBranchOfficial } from '../../interfaces';
export declare const branchOfficialService: {
    getUserBranches: (userId: string) => Promise<IBranchOfficial[]>;
    updateUserBranches: (userId: string, branchesIds: string[]) => Promise<IBranchOfficial[]>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranchOfficial>>;
    getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<IBranchOfficial>;
    create: (payload: IBranchOfficial) => Promise<any>;
    searchByName: <T extends keyof IBranchOfficial>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranchOfficial>>;
};
