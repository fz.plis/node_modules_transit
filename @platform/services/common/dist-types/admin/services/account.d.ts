import type { IAccount } from '../../interfaces';
export declare const accountService: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccount>>;
    get: (id: string) => Promise<IAccount>;
    getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
    searchByName: <T extends keyof IAccount>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccount>>;
    list: (ids: string[]) => Promise<IAccount[]>;
    listWithBalance: (ids: string[]) => Promise<import("../../interfaces").IAccountV2[]>;
    create: (payload: IAccount) => Promise<any>;
};
