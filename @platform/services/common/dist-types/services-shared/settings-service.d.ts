export declare const settingsService: {
    get: () => Promise<{
        maxPrintCount: number;
    }>;
};
