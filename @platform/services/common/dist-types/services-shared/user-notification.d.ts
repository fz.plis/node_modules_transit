import type { IMetaData, IUserNotification, IUserNotificationCount } from '../admin';
export declare const userNotificationService: {
    getCounter: (metaData: IMetaData) => Promise<any>;
    markAsRead: (ids: string[]) => Promise<Record<string, unknown>>;
    markAsUnread: (ids: string[]) => Promise<Record<string, unknown>>;
    postpone: (id: string, postponedUntil: string | null) => Promise<IUserNotification>;
    getLocale: () => Promise<any>;
    getUnreadMessageCount: () => Promise<IUserNotificationCount>;
    getImportantNotification: (metaData: IMetaData) => Promise<IUserNotification[]>;
    execute: (id: string) => Promise<IUserNotification>;
    getList: (metaData: IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IUserNotification>>;
    get: (id: string) => Promise<IUserNotification>;
    getByIds: (id: string, clientId: string) => Promise<IUserNotification>;
    search: (metaData: IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IUserNotification>>;
    searchByName: <T extends keyof IUserNotification>(params: import("../admin").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IUserNotification>>;
};
