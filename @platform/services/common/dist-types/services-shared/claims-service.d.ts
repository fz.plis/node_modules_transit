import type { IAccountClaims, IServerResp, IPrintResponseDoc } from '../interfaces';
export declare const CLAIMS_URL = "/api/reservation-request/request";
export declare const CLAIMS_DRAFT_URL: string;
export declare const claimsService: {
    newDocument(): Promise<any>;
    update(accountRequestData: IAccountClaims): Promise<IServerResp<IAccountClaims>>;
    draft(data: IAccountClaims): Promise<IAccountClaims>;
    updateDraft({ id, ...data }: IAccountClaims): Promise<IAccountClaims>;
    send(ids: string[]): Promise<Array<IServerResp<IAccountClaims>>>;
    export(data: any): Promise<{
        fileName: string;
        type: any;
        data: any;
    }>;
    exportAcc(data: any): Promise<{
        fileName: string;
        type: any;
        data: any;
    }>;
    exportList(data: any): Promise<{
        fileName: string;
        type: any;
        data: any;
    }>;
    exportAccList(data: any): Promise<{
        fileName: string;
        type: any;
        data: any;
    }>;
    print(data: any): Promise<IPrintResponseDoc>;
    printAcc(data: any): Promise<any>;
    printList(data: any): Promise<IPrintResponseDoc>;
    printAccList(data: any): Promise<IPrintResponseDoc>;
    delete(ids: any): Promise<any>;
    getLocResource: () => Promise<any>;
    getList: (metaData: import("../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccountClaims>>;
    getCounter: ({ category, ...metaData }: import("../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<IAccountClaims>;
    create: (payload: IAccountClaims) => Promise<any>;
    searchByName: <T extends keyof IAccountClaims>(params: import("../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccountClaims>>;
};
