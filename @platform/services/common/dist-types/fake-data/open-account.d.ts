import type { IOpenAccount, IDigest, ISignVerify, ISign, ISignResult } from '../interfaces';
import { OPEN_ACCOUNT_STATE } from '../interfaces';
/**
 * Хеш сертификата.
 */
export declare const fakeThumbprint = "1E9485EDB73EA7D7F6B8FB5E0983851585C8C887";
export declare const addOrUpdateItem: (item: IOpenAccount) => void;
export declare const deleteItem: (id: string) => void;
export declare const newId: () => string;
export declare const getById: (id: string) => IOpenAccount | undefined;
export declare const generateDigest: (id: string) => IDigest;
export declare const verifySign: (id: string) => ISignVerify;
export declare const sign: ({ documentId, signature }: ISign) => ISignResult<IOpenAccount>;
export declare const send: (ids: string[]) => {
    status: OPEN_ACCOUNT_STATE;
    bankClient?: import("../interfaces").IBankClientInfo | undefined;
    branch: import("../interfaces").IBranchDetailsInfo;
    confirmNoChanges: boolean;
    commissionAccount: import("../interfaces").ICommissionInfo;
    commentForClient?: string | undefined;
    contract: import("../interfaces").IContractInfoDetails;
    date: string;
    id: string;
    errors: string;
    isRulesJoined: boolean;
    number: string;
    opopAccount: import("../interfaces").IOpopAccount;
    userId: string;
    accountInfos: import("../interfaces").IOpenAccountInfo[];
    openedAccounts?: {
        id: string;
    }[] | undefined;
    version: number;
}[];
export declare const EMPTY_OPEN_ACCOUNT: IOpenAccount;
export declare const getFakeOpenAccountFake: (category?: string | undefined) => Record<string, IOpenAccount>;
export declare const getFakeOpenAccountCategoryFake: () => {
    [x: string]: {
        count: number;
        name: string;
    };
};
