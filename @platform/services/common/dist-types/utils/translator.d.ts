export declare const openAccountTranslator: (key: string, params?: Record<string, string | number> | undefined) => string;
