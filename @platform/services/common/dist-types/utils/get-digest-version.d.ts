import type { IBaseSignedDocument } from '../interfaces';
/**
 * Получение версии подписанного документа из сертификата.
 * Если sppVersion хранится не в document.signatures.[x].sppVersion пути.
 * То нужно передать свою функцию, которая найдёт sppVersion в документе.
 *
 * Для использования нужно передать данную функцию в контекст.
 * /eco-tools-monorepo/platform/services/src/actions/universal-sign-verify.ts.
 *
 * @param document Любой документ с массивом signatures.
 * @param signId Id подписаемого сертификата сертификата.
 */
export declare const getDigestVersion: (document: IBaseSignedDocument, signId: string) => number | undefined;
