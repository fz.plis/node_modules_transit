import type { IMetaData, ICollectionResponse, ISearchByName } from '../interfaces';
export declare const getNewDictionaryService: <TRow>(url: string) => {
    getList: (metaData: IMetaData) => Promise<ICollectionResponse<TRow>>;
    getCounter: (metaData: IMetaData) => Promise<any>;
    get: (id: string) => Promise<TRow>;
    getByIds: (id: string, clientId: string) => Promise<TRow>;
    search: (metaData: IMetaData) => Promise<ICollectionResponse<TRow>>;
    searchByName: <T extends keyof TRow>(params: ISearchByName, fieldName: T) => Promise<ICollectionResponse<TRow>>;
};
