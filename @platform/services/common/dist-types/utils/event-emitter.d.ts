declare type F<T> = (args: T) => void;
/**
 * Типизированный эвент эмиттер для сихронизации состояния между хуками useLocalStorage.
 *
 * @see https://gist.github.com/mudge/5830382#gistcomment-3398873
 */
export declare const eventEmitter: <T>() => {
    on: (name: string, fn: F<T>) => void;
    trigger: (name: string, args: T) => false | undefined;
    off: (name: string, fn: F<T>) => void;
};
export {};
