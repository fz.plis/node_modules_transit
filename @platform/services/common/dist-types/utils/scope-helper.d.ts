/**
 * Область приложения.
 */
export declare enum SCOPE {
    /**
     * Пользователь Клиента.
     */
    CLIENT = "CLIENT",
    /**
     * Пользователь Банка.
     */
    ADMIN = "ADMIN"
}
/**
 * Функция установки области приложения.
 *
 * @param newScope Область приложения.
 */
export declare const setScope: (newScope: SCOPE) => void;
/**
 * Функция проверки области приложения.
 */
export declare const isClientScope: () => boolean;
