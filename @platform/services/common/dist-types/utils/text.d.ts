import type { IBranch } from '../interfaces';
export interface IUserName {
    /**
     * Фамилия.
     */
    familyName: string;
    /**
     * Имя.
     */
    firstName: string;
    /**
     * Отчество.
     */
    middleName: string | null | undefined;
}
export declare const getFio: ({ familyName, firstName, middleName }: IUserName) => string;
export declare const getBranchLabel: ({ name, number: branchNumber }: IBranch) => string;
