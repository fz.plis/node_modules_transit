export declare const getDefaultLocaleService: (url: string) => {
    get: () => Promise<Record<string, string>>;
};
