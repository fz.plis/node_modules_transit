/// <reference types="react" />
/**
 * Хук для сохранения произвольных данных в localStorage.
 *
 * @param key Ключ.
 * @param defaultValue Начальное значение.
 */
export declare const useLocalStorage: <T>(key: string, defaultValue: T) => [T, import("react").Dispatch<import("react").SetStateAction<T>>];
