export interface IConstantFactory<T, R, U> {
    set<K extends keyof U>(key: K | K[], val?: R): IConstantFactory<T, R, Omit<U, K>>;
    create(): {
        [P in keyof T]: R;
    };
}
export interface IConstantHelper<T> {
    of<TVal>(def?: TVal): IConstantFactory<T, TVal, T>;
}
export declare const constant: <T extends Record<string, unknown>>(initialVal?: T | undefined) => IConstantHelper<T>;
