import type { AxiosPromise, AxiosRequestConfig } from 'axios';
export interface IRequestParams extends AxiosRequestConfig {
    disableTokenHeader?: boolean;
}
export declare type RequestMiddleware = (config: AxiosRequestConfig) => Promise<AxiosRequestConfig>;
export declare const authRequestMiddleware: RequestMiddleware;
/**
 * Заменяет пустые строки на `undefined` перед отправкой на бекенд и убирает пустые объекты.
 */
export declare const emptyfy: (obj: any) => any;
export declare const emptyfyMiddleware: RequestMiddleware;
export declare const setOrganizationIdHeaderMiddleware: RequestMiddleware;
export declare const addSetOrganizationIdHeaderMiddleware: () => void;
export declare const addRequestMiddleware: (middleware: (config: AxiosRequestConfig) => Promise<AxiosRequestConfig>) => void;
export declare const request: <T = any>(params: IRequestParams) => AxiosPromise<T>;
