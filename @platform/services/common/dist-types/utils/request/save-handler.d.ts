import type { getTranslator } from '@platform/core';
import type { IApiValidationResult, IValidationCheckResult, IBaseEntity, ISaveResponse } from '../../interfaces';
declare type Translator = ReturnType<typeof getTranslator>;
declare type ICheckMap = Record<string, IValidationCheckResult[]>;
export interface IValidationMap {
    errors: ICheckMap;
    warnings: ICheckMap;
}
/**
 * Функция принимает функцию-переводчик и возвращает функцию для трансформирования серверных контролей к виду фронтовых.
 *
 * @param translator Функция-переводчик, которая принимает ключ ресурса локализации и возвращает локализованную строку.
 * Обычно это обертка над функцией `translate`, которая замкнута на определенную именованную область локализации - `area`.
 *
 * Возвращает `transformer` Функцию, которая принимает результаты запроса с сервера.
 */
export declare const createSaveResponseHandler: (translator: Translator) => <T extends IBaseEntity>({ data, validationResult, }: {
    validationResult?: IApiValidationResult | undefined;
    data: T;
}) => ISaveResponse<T>;
export {};
