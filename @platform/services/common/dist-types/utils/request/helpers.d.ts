import type { IGroupValidationResult, IValidationCheckResult, IServerValidationResult, IControlValidationResult } from '../../interfaces';
declare type ICheckMap = Record<string, IValidationCheckResult[]>;
interface IValidationMap {
    errors: ICheckMap;
    warnings: ICheckMap;
}
export declare type PathIterator = ReturnType<typeof pathTransformer>;
/**
 * Возвращает функцию, преобразующую путь до поля с учетом списочных сущностей в исходном объекте.
 *
 * @param entity Проверяемый объект.
 */
export declare const pathTransformer: <T extends Record<string, any>>(entity: T) => (fieldName: string) => string;
export declare const flatify: (localizator: (checkResult: IValidationCheckResult) => string) => (pathIterator: PathIterator) => (valMap: ICheckMap) => IServerValidationResult[];
export declare const extractFromControlResult: (allFields: IValidationMap, result: IControlValidationResult) => IValidationMap;
export declare const extractFromGroup: (allFields: IValidationMap, group: IGroupValidationResult) => {
    errors: ICheckMap;
    warnings: ICheckMap;
};
export {};
