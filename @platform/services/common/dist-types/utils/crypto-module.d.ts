import type { IAppConfig, ICryptoModule } from '../interfaces';
/**
 * Проверка используемой версии плагина.
 *
 * @param version Версия плагина.
 * @param minVersion Минимальная версия плагина.
 */
export declare const canUseCryptoPlugin: (version: string | null, minVersion?: string | undefined) => boolean;
/** Проверка актуальности версии криптомодуля по предоставленному инстансу. */
export declare const isOutdatedCryptoModule: (cryptoModule: ICryptoModule, pluginsConfig: IAppConfig['cryptoPlugins']) => Promise<boolean>;
