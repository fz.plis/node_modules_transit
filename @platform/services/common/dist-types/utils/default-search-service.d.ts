import type { IFiasResult } from '../interfaces';
export declare const getDefaultSearchService: (url: string) => {
    search: (query: string, count?: number) => Promise<IFiasResult[]>;
};
