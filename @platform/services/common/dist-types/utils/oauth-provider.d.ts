import type { IOAuthError, IOAuthSuccess } from '../interfaces';
export interface IOAuthProvider {
    buildLoginUrl(redirectUri: string): string;
    buildLogoutUrl(redirectUri: string): string;
    parseCallbackUrl(url: string): void;
}
export interface IOAuthProviderProps {
    clientId: string;
    authUri: string;
    loginUri: string;
    logoutUri: string;
    responseType: 'code' | 'token';
    onReceiveToken(result: IOAuthSuccess): void;
    onError(error: IOAuthError): void;
}
export declare class OAuthProvider implements IOAuthProvider {
    static of(props: IOAuthProviderProps): OAuthProvider;
    private options;
    private constructor();
    buildLoginUrl(redirectUri: string): string;
    buildLogoutUrl(redirectUri: string): string;
    parseCallbackUrl(url: string): void;
}
export declare const getOauthProvider: () => IOAuthProvider;
export declare const setOauthProvider: (provider: IOAuthProvider) => IOAuthProvider;
export declare const redirectToLogin: () => void;
