/**
 * Возвращает дату-время в фрмате ISO.
 *
 * @deprecated Следует использовать функцию `formatDateTime` из `@platform/tools/date-time`.
 */
export declare const formatDateTime: (dateStr: string, asLocal?: boolean) => string;
export declare const formatMobilePhone: (phone: string) => string;
/**
 * Преобразование массива строк в одну строку с разделителями.
 *
 * @param items {Array<string>}. Массив строк.
 * @param separator {string}. Разделитель.
 */
export declare const prepareText: (items: Array<string | undefined>, separator?: string) => string;
export declare const formatToShortFio: (fio: string) => string;
