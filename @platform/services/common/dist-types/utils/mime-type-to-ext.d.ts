export declare const mimeTypeToExt: (mimeType: string) => "pdf" | "html" | "zip" | "xls" | "doc" | "docx" | "xlsx" | "pptx";
