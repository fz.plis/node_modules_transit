import type { Path } from '@platform/tools/istore';
export declare const getStoreValue: (path: Path) => any;
