import { ERROR } from '../../interfaces';
/** Коды ошибок при загрузке файлов. */
export declare const FILE_UPLOAD_ERROR: {
    /** Отсутствие авторизации. */
    FORBIDDEN: ERROR;
    /** Формат содержимого не поддерживается сервером. */
    UNSUPPORTED_MEDIA_TYPE: ERROR;
    /** Файл не прошел проверку антивирусом. */
    FILE_MALWARE: ERROR;
    /** Ошибка загрузки. */
    NOT_LOADED_ERROR: number;
};
