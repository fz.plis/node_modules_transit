import type { IAttachment, IFileBuffer } from '../../interfaces';
import type { IAttachmentHookConfig } from './interfaces';
/**
 * Хук для загрузки файлов через `attachmentsService`, работает по аналогии с istore-доменом `attachments`.
 * В качестве обязательного параметра принимает attachmentService, куда передается файловый сервис для КЧ или БЧ.
 *
 * @example
 * ```
 * // пример использования с FileInput
 * const attachmentsProps = useAttachments({
 *   files,
 *   onEndLoad,
 *   onRemove,
 *   attachmentService
 * });
 *
 * return (
 *   <FileInput
 *     accept={attachmentsProps.accept}
 *     acceptText={'pdf'}
 *     files={attachmentsProps.buffer}
 *     maxSize={attachmentsProps.maxSize}
 *     multi={attachmentsProps.multi}
 *     onDeleteFile={attachmentsProps.remove}
 *     onFileUpload={attachmentsProps.onFileUpload}
 *   />
 * );
 * ```
 */
export declare const useAttachments: ({ onEndLoad, onRemove, multi, accept, maxSize, interval, maxTries, files: initFiles, attachmentsErrors, setIsLoading, supportFormats, attachmentService, }: IAttachmentHookConfig<IAttachment>) => {
    accept: string;
    buffer: IFileBuffer[];
    maxSize: number | undefined;
    multi: boolean;
    remove: (file: IFileBuffer) => void;
    removeAll: () => void;
    onFileUpload: (files: File[]) => void;
};
export * from './interfaces';
