interface IEventParams {
    bubbles?: boolean;
    cancelable?: boolean;
    detail?: any;
}
export declare const CustomEvent: new (type: string, eventParams: IEventParams) => CustomEvent<unknown>;
export {};
