/**
 * Привилегии для резервирования счёта.
 */
export declare const RESERVE_ACCOUNT_AUTHORITIES: {
    /** Функция создания заявки. */
    CREATE: string;
};
