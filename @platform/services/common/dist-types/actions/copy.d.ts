import type { IBaseEntity, IActionConfig, IActionContext } from '../interfaces';
export interface ICopyService {
    copy(id: string): Promise<IBaseEntity>;
}
export declare const copy: IActionConfig<IActionContext<ICopyService>, IBaseEntity>;
