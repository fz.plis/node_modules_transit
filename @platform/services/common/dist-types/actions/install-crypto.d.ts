import type { IActionConfig } from '@platform/core';
import type { IBaseContext, IBaseEntity } from '../interfaces';
export declare const getInstallCryptoAction: (getHeader?: (() => string) | undefined) => IActionConfig<IBaseContext, IBaseEntity>;
export declare const checkSignInstallCrypto: IActionConfig<IBaseContext, IBaseEntity>;
export declare const signInstallCrypto: IActionConfig<IBaseContext, IBaseEntity>;
