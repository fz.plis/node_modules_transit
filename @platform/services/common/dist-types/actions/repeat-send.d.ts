import type { IBaseEntity, IActionContext, IActionConfig } from '../interfaces';
export interface IValidateSignServive {
    repeatSend(id: string): Promise<IBaseEntity>;
}
export declare const repeatSend: IActionConfig<IActionContext<IValidateSignServive>, IBaseEntity>;
