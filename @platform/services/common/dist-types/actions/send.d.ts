import type { IActionContext, IServerResp, IBaseEntity, IActionConfig } from '../interfaces';
export interface ISendService {
    send(id: string): Promise<IServerResp<any>>;
}
export declare const send: IActionConfig<IActionContext<ISendService>, IBaseEntity>;
