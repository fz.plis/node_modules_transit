import type { IBaseContext, IRouterContext } from '../interfaces';
import type { RenderDocument, RenderSummary } from '../views';
export interface ISignConfig<TDoc = any> {
    /**
     * Функция, возвращающая представление просмотра документа при подписи.
     */
    renderDocument: RenderDocument<TDoc>;
    /**
     * Функция, возвращающая краткое содержаниие всех документов при подписи (например, выбрано n документов на сумму x).
     */
    renderSummary?: RenderSummary<TDoc>;
    /**
     * Функция возвращающая наименование документа (нужно для проверки подписи).
     */
    docNameGetter(doc: TDoc): string;
}
/**
 * Параметры контекста.
 */
export interface IContextOptions {
    /** Параметры, которые будут переданы в модалку подписи - `signDialog`. */
    sign?: ISignConfig;
    /** Параметры кастомного роутера.  Если не передавать, то будет использован роутер по умолчанию. */
    router?: IRouterContext;
    /**
     * Максимальное количество печатаемых документов.
     *
     * @default 30
     */
    maxPrintDoc?: number;
}
export declare const createContext: ({ sign, router, maxPrintDoc }?: IContextOptions) => IBaseContext;
