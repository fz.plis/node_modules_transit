import React from 'react';
import type { ICollapsedCheckInfo } from '../collapsed-check-info';
/**
 * Свойства элемента списка результата проверки.
 */
export declare type ISignCheckDialogData = Omit<ICollapsedCheckInfo, 'expanded'>;
/**
 * Свойства диалога просмотра результатов проверки подписи.
 */
export interface ISignCheckDialog {
    /**
     * Функция закрывания диалога.
     */
    onClose(): void;
    /**
     * Список результата проверки.
     */
    data: ISignCheckDialogData[];
    /**
     * Кнопка выгрузки реквизитов ЭП.
     */
    exportSignatureButton?: JSX.Element;
    /**
     * Кнопка сформировать протокол проверки подписей.
     */
    signatureVerificationButton?: JSX.Element;
}
/**
 * Диалог просмотра результатов проверки подписи.
 *
 * @param props Свойства компонента.
 * @param props.data Список результата проверки.
 * @param props.onClose Функция закрывания диалога.
 * @param props.exportSignatureButton Кнопка выгрузки рекизитов ЭП.
 * @param props.signatureVerificationButton Кнопка сформировать протокол проверки подписей.
 */
export declare const SignCheckDialog: React.FC<ISignCheckDialog>;
