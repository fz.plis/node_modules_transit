import type React from 'react';
import type { AUTHORITY_VALIDATORS, DOCUMENT_TYPE_CODE, IBankClient } from '../../interfaces';
/**
 * Элемент информера привилегий.
 */
export interface IAuthorityInformerProps {
    /**
     * Индекс элемента.
     */
    idx: number;
    /**
     * Выводимое сообщение.
     */
    message: React.ReactNode;
    /**
     * Закрыть информер.
     *
     * @param idx Индекс элемента.
     */
    onClose(idx: number): void;
}
/**
 * Привилегия.
 */
export interface IAuthorityProps {
    /**
     * Организация пользователя.
     */
    bankClient?: IBankClient;
    /**
     * Тип документа.
     */
    documentType?: DOCUMENT_TYPE_CODE;
    /**
     * Коды продуктов банка.
     */
    productCodes?: string[];
    /**
     * Опции проверки полномочий.
     */
    validators?: AUTHORITY_VALIDATORS[];
    /**
     * Сообщение по-умолчанию для проверки полномочий для сертификата.
     */
    certificateErrorDefaultMessage?: React.ReactNode;
    /**
     * Сообщение по-умолчанию для проверки полномочий для ЕИО.
     */
    eioErrorDefaultMessage?: React.ReactNode;
    /**
     * Сообщение по-умолчанию для проверки полномочий для конституцию.
     */
    constitutionErrorDefaultMessage?: React.ReactNode;
    /**
     * Сообщение по-умолчанию об отсутствии сертификата.
     */
    noCertificateErrorMessage?: React.ReactNode;
}
/**
 * Ошибка.
 */
export interface IErrorResult {
    /**
     * Тип валидатора.
     */
    type: AUTHORITY_VALIDATORS;
    /**
     * Сообщение.
     */
    message: React.ReactNode;
}
