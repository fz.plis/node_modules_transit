import type { IAuthorityProps, IErrorResult } from './interfaces';
/**
 * Хук привилегий.
 *
 * @param props Свойства.
 * @param props.bankClient Организация пользователя.
 * @param props.documentType Тип документа.
 * @param props.productCodes Коды продуктов банка.
 * @param props.validators Опции проверки полномочий.
 * @param props.certificateErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для сертификата.
 * @param props.constitutionErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для ЕИО.
 * @param props.eioErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для конституцию.
 * @param props.noCertificateErrorMessage Сообщение по-умолчанию об отсутствии сертификата.
 * @class
 */
export declare const useAuthority: ({ bankClient, documentType, productCodes, validators, certificateErrorDefaultMessage, constitutionErrorDefaultMessage, eioErrorDefaultMessage, noCertificateErrorMessage, }: IAuthorityProps) => {
    errors: IErrorResult[];
    hasAuthorityError: boolean;
    certificateError: boolean;
    isLoading: boolean;
    removeMessage: (idx: any) => void;
    clearMessages: () => void;
    reload: () => Promise<void>;
};
