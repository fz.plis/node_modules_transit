import React from 'react';
import type { ICryptoInstallerProps } from './interfaces';
export declare const CryptoInstaller: React.FC<ICryptoInstallerProps>;
export declare const openCryptoInstallerDialog: (header: string) => Promise<boolean>;
