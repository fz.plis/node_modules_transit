/**
 * Свойства инсталлятора криптомодуля.
 */
export interface ICryptoInstallerProps {
    /**
     * Заголовок диалогового окна.
     */
    header?: string;
    /**
     * Метод закрытия диалога.
     *
     * @param installedStatus Статус установки.
     */
    onClose(installedStatus: boolean): void;
}
