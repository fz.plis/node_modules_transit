import React from 'react';
export declare const Loader: React.FC;
/**
 * Показать лоадер на странице.
 */
export declare const showLoader: () => void;
/**
 * Скрыть лоадер со страницы.
 *
 * @param force Флаг скрытия лоадера.
 */
export declare const hideLoader: (force?: boolean | undefined) => void;
