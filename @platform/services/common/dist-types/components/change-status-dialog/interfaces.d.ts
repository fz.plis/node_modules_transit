import type { DOCUMENT_STATUS_TYPE, IOption } from '@platform/ui';
/** Текущий статус заявки. */
export interface ICurrentStatus extends IOption<string> {
    /** Тип цветового индикатора. */
    indicator: DOCUMENT_STATUS_TYPE;
}
/** Опция выпадающего списка, для выбора статуса. */
export interface IStatusOption extends ICurrentStatus {
    /** Если true - то при выборе статуса поле "Комментарий для клиента" становится обязательным. */
    requireCommentForClient: boolean;
}
/** Стейт формы смены статуса заявки. */
export interface IChangeStatusFormState {
    /** Статус на который надо сменить текущий. */
    newStatus?: string;
    /** Комментарий для клиента. */
    commentForClient?: string;
    /** Комментарий для банки. */
    commentForBank?: string;
}
