import React from 'react';
import type { IOptionTemplateProps } from '@platform/ui';
import type { IStatusOption, IChangeStatusFormState, ICurrentStatus } from './interfaces';
/** Цветовой индикатором и название статуса. */
export declare const StatusOptionTemplate: React.ForwardRefExoticComponent<Pick<IOptionTemplateProps<IStatusOption>, "option" | "selected" | "className" | "onClick" | "onMouseDown" | "extraSmall" | "indeterminate" | "active" | "withCheckbox" | "customLabel"> & React.RefAttributes<React.FC<IOptionTemplateProps<import("@platform/ui").IOption<any>>>>>;
/** Свойства компонента ChangeStatusDialog. */
export interface IChangeStatusDialogProps {
    /** Текущий статус заявки. */
    currentStatus: ICurrentStatus;
    /** Определяет надо ли отображать поле ввода комментария для банка. */
    hiddenBankComment?: boolean;
    /** Колбек закрытия окна. */
    onClose(): void;
    /** Колбек сабмита формы. */
    onSelectStatus(result: IChangeStatusFormState): void;
}
/**
 * Возвращает компонент диалог смены статуса заявки.
 *
 * @param availableStatuses - Статусы на которые можно сменить текущий.
 */
export declare const getChangeStatusDialog: (availableStatuses: IStatusOption[]) => React.FC<IChangeStatusDialogProps>;
