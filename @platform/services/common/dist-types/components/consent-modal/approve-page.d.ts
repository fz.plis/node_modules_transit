import React from 'react';
/**
 * Страница подтверждения согласия.
 */
interface IApproveConsentPageProps {
    /**
     * Принять согласие.
     *
     * @param isAccept Значение согласия.
     */
    onAccept(isAccept?: boolean): void;
    /**
     * Название кнопки.
     *
     * @param buttonLabel Название кнопки отправки согласия.
     * @default locale.continue
     */
    buttonLabel?: string;
    /**
     * Скрывать заголовок компонента.
     *
     * @param hideHeader Скрыть заголовок.
     */
    hideHeader?: boolean;
    /**
     * Текст возле чекбокса.
     *
     * @param acceptText Текст возле чекбокса.
     * @default locale.consent.labels.iAccept
     */
    acceptText?: string;
    /**
     * Текст возле чекбокса, представленный в виде ссылки.
     *
     * @param acceptWithPersonalDataText Текст возле чекбокса, представленный в виде ссылки.
     * @default locale.consent.labels.acceptWithPersonalData
     */
    acceptWithPersonalDataText?: string;
    /**
     * Лейбл принятия согласия в модальном окне.
     *
     * @param acceptLabel Лейбл принятия согласия в модальном окне.
     */
    acceptLabel?: string;
}
export declare const ApproveConsentPage: React.FC<IApproveConsentPageProps>;
export {};
