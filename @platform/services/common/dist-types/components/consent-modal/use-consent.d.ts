import type { CONSENT_TYPE } from '../../constants';
export declare const useConsent: (type: CONSENT_TYPE) => {
    isError: boolean;
    consentContent: string;
    consentHeader: string;
    consentLoaded: boolean;
    reload: () => Promise<void>;
};
