import type { ITimer } from '../../interfaces';
/**
 * Хук обратного отсчета с заданным интервалом.
 *
 * @param initialValue Начальное значение таймера в секундах.
 *
 * @example
 * const { value, start, stop, reset, restart } = useTimer(120);
 * start();
 */
export declare const useTimer: (initialValue: number) => ITimer;
