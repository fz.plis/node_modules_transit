import type { IFiasResult } from '../../interfaces';
export declare const fias: {
    search: (query: string, count: number) => Promise<IFiasResult[]>;
};
