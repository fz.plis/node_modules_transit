import type { ICollectionResponse, IBranch, IMetaData } from '../../interfaces';
export declare const branch: {
    get: (id: string) => Promise<IBranch>;
    getList: (metaData: IMetaData) => Promise<ICollectionResponse<IBranch>>;
    searchByName: <T extends keyof IBranch>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<ICollectionResponse<IBranch>>;
    localityList: () => Promise<string[]>;
    bicPage: (metaData: IMetaData) => Promise<ICollectionResponse<string>>;
};
