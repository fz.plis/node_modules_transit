import type { IExportRequest, IOpenAccount, IServerResp, IExportResponse, IPrintResponse, IHistoryResponse } from '../../interfaces';
export declare const createOpenAccountService: (baseUrl: string) => {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOpenAccount>>;
    get: (id: string) => Promise<IOpenAccount>;
    getCounter: (metaData: import("../../interfaces").IMetaData) => Promise<any>;
    validateSign: (id: string) => Promise<IServerResp<IOpenAccount>>;
    exportClaimOpenAccounts: (data: IExportRequest) => Promise<IExportResponse>;
    exportClaimRulesGoz: (data: IExportRequest) => Promise<IExportResponse>;
    exportInfoOpenAccounts: (data: IExportRequest) => Promise<IExportResponse>;
    exportListRequestOpenAccounts: (data: IExportRequest) => Promise<IExportResponse>;
    printClaimOpenAccounts: (data: IExportRequest) => Promise<IPrintResponse>;
    printClaimRulesGoz: (data: IExportRequest) => Promise<IPrintResponse>;
    printInfoOpenAccounts: (data: IExportRequest) => Promise<IPrintResponse>;
    printListRequestOpenAccounts: (data: IExportRequest) => Promise<IPrintResponse>;
    getHistory: (id: string) => Promise<IHistoryResponse[]>;
};
