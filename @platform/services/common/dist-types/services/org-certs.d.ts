import type { IOrgCert, IFileData, IOrgCertFileInfo } from '../interfaces';
export declare const orgCertsService: {
    get: (id: string) => Promise<IOrgCert>;
    getList: (metaData: import("../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOrgCert>>;
    download: (_certificateId: string) => Promise<IFileData>;
    parse: (_rawCertificate: string) => Promise<IOrgCertFileInfo>;
};
