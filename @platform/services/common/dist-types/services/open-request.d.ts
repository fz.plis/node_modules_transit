import type { IOpenAccount, IExportRequest } from '../interfaces';
export declare const openAccountService: {
    get: (id: string) => Promise<IOpenAccount>;
    getList: (metaData: import("../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOpenAccount>>;
    getCounter: (metaData: import("../interfaces").IMetaData) => any;
    exportClaimOpenAccounts: (_data: IExportRequest) => any;
    exportClaimRulesGoz: (_data: IExportRequest) => any;
    exportInfoOpenAccounts: (_data: IExportRequest) => any;
    exportListRequestOpenAccounts: (_data: IExportRequest) => any;
    printClaimOpenAccounts: (_data: IExportRequest) => any;
    printClaimRulesGoz: (_data: IExportRequest) => any;
    printInfoOpenAccounts: (_data: IExportRequest) => any;
    printListRequestOpenAccounts: (_data: IExportRequest) => any;
    validateSign: (_id: string) => any;
    getHistory: (_id: string) => any;
};
