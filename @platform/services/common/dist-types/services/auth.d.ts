import type { IAuthConfiguration, ILoginPasswordRequest, ILoginResponse } from '../interfaces';
export declare const authService: {
    login: {
        byPassword: (data: ILoginPasswordRequest, redirectUri?: string | undefined) => Promise<ILoginResponse>;
    };
    logout: () => Promise<any>;
    getAuthConfiguration: () => Promise<IAuthConfiguration>;
};
export declare type AuthService = typeof authService;
