export declare const callStreamAction: <T extends Record<string, (...args: any) => any> = any>(streamName: string, method: keyof T, params?: unknown) => Promise<NonNullable<T>[keyof T] | null>;
