import React from 'react';
export interface IDynamicRouteProps {
    routes: any[];
    loadRoutes(name: string): void;
    children: React.ReactNode;
}
export declare const DynamicRoute: React.FC<IDynamicRouteProps>;
