import type { Unit } from '@platform/tools/istore';
export declare const useUnit: <S, M>(unit: Unit<S, M>) => S;
