import type { IPollingOptions, ATTACHMENT_STATUS, ICheckUploadOptions, IFileUploadOptions, IFileBuffer } from '../interfaces';
export interface IBaseAttachment {
    id: string;
    status: ATTACHMENT_STATUS;
}
export interface IAttachmentConfig<T extends IBaseAttachment> extends IPollingOptions {
    upload(file: File, options?: IFileUploadOptions): Promise<T>;
    checkUploadProgress(id: string, options?: ICheckUploadOptions): Promise<T>;
    maxSize?: number;
    /** Набор MIME для проверки файлов. */
    accept?: string[];
    multi?: boolean;
    /** Список расширений файлов, без точки. */
    supportFormats?: string[];
}
export declare const attachments: <T extends IBaseAttachment>({ upload, checkUploadProgress, maxSize, accept, interval, maxTries, multi, supportFormats, }: IAttachmentConfig<T>, initialState?: IFileBuffer[]) => import("@platform/tools/istore").Domain<{
    buffer: import("@platform/tools/istore").Unit<IFileBuffer[], {
        toggleItem: (item: IFileBuffer) => void;
        set: (val: IFileBuffer[]) => void;
        setItem: (index: number, item: IFileBuffer) => void;
        add: (item: IFileBuffer | IFileBuffer[]) => void;
        remove: (item: IFileBuffer | IFileBuffer[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<IFileBuffer>) => void;
    }>;
    error: import("@platform/tools/istore").Unit<any, {
        set: (val: any) => void;
    }>;
    accept: import("@platform/tools/istore").Unit<string[], {
        set: (val: string[]) => void;
    }>;
    supportFormats: import("@platform/tools/istore").Unit<string[], {
        set: (val: string[]) => void;
    }>;
}, {
    setAccept: (newAccepts: string[]) => void;
    setSupportFormats: (newSupportFormats: string[]) => void;
    remove: (file: IFileBuffer, onRemove?: (info: IFileBuffer) => void) => void;
    getAcceptFileType(): string;
    fill(buffer: Array<Pick<IFileBuffer, 'error' | 'id' | 'name' | 'size' | 'status'>>): void;
    clearBuffer(): void;
    getMaxSize: () => number | undefined;
    upload: (files: File[], onEndLoad?: (entity: T) => void) => void;
}>;
