import type { IParsedToken, IClientTokenInfo } from '../../interfaces';
export interface IRouteContextOptions {
    /**
     * Сегменты, подлежащие проверке.
     */
    segments?: string[];
    /**
     * Контексты юзера, подлежащие проверке.
     */
    userContext?: string[];
}
export declare type ContextGuardParameters = IRouteContextOptions & {
    orgId?: string;
};
/**
 * Функция, принимающая данные токена и контекст проверки.
 * Должна выбрасывать ошибку, если проверка не пройдена.
 *
 * @param tokenInfo Данные токена.
 * @param contextOptions Конфиг для сравнение данных токена.
 * @throws RouteGuardError.
 */
export declare type ContextGuard<T extends IParsedToken = IParsedToken> = (tokenInfo: T, contextOptions: ContextGuardParameters) => void;
/**
 * Функция возвращает `Set` доступных сегментов из `token` по `orgId`.
 * Если данные по организации отстуствуют в токене, то возвращает пустой Set.
 *
 * @param token Данные токена юзера.
 * @param orgId Id выбранной организации.
 */
export declare const getSegmentsFromToken: (token: IClientTokenInfo, orgId: string) => Set<string>;
/**
 * Проверяет данные токена на наличие указанных в конфиге сегментов по переданному идентификатору организации.
 * Если ни один сегмент не присутствует в токене, то выбрасывается ошибка.
 *
 * @throws SegmentGuardError.
 */
export declare const segmentGuard: ContextGuard<IClientTokenInfo>;
/**
 * Проверяет данные токена на наличие указанных в конфиге контекстов юзера.
 * Если хотя бы один контекст из конфига отсутствует в токене, то выбрасывается ошибка.
 *
 * @throws Dbo3GuardError.
 */
export declare const dbo3Guard: ContextGuard;
/**
 * Функция берет токен и последовательно пропускает его через каждый `guard` из `guards`
 * на соответствие общему конфигу `options`.
 * Проверка идет последовательно через все `guards` до первой выброшенной ошибки.
 *
 * @param options Конфиг проверки, содержащий id организации, список сегментов и контекстов.
 * @param guards Список `guard`, через который будет идти проверка токена на соответствие конфигу `options`.
 * @returns Кортеж, содержащий признак прохождения проверки и первую встреченную ошибку.
 */
export declare const checkGuards: <T extends IParsedToken>(options: ContextGuardParameters, ...guards: ContextGuard<T>[]) => [boolean, Error?];
