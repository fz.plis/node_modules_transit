import React from 'react';
import type { RouteProps } from 'react-router';
import type { REQUEST_FUNCTION } from '../../../../constants';
/**
 * Интерфейс защищенного роута.
 */
interface IGuardRouteProps extends RouteProps {
    /**
     * Привилегии, которыми закрыт роут.
     */
    authority: string[] | string;
    /**
     * Тип функции.
     */
    type?: REQUEST_FUNCTION;
}
/**
 * Защищенный роут.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=6363003
 *
 * @example <GuardRoute authority={'SCROLLER_VIEW'} type="DSF_CLIENT_VIEW" ... />
 */
export declare const GuardRoute: React.FC<IGuardRouteProps>;
export {};
