import React from 'react';
import type { RouteProps } from 'react-router-dom';
/**
 * Секьюрный роут, ограничивающий доступ для организаций,
 * не имеющим доступ к сегментам переданным в пропс `availableSegments`.
 *
 * @deprecated C 2.08.2021 Используйте GuardRoute вместо.
 */
export declare const SegmentRoute: React.FC<RouteProps & {
    availableSegments: string[];
}>;
