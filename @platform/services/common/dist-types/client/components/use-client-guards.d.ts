import type { IParsedToken } from '../../interfaces';
import type { IRouteContextOptions, ContextGuard } from './route-guards';
/**
 * Хук, определяющий доступность блока по информации из токена и выбранной организации.
 * Возвращает кортеж, с признаком прохождения авторизации и ,опционально, ошибкой,
 * пойманной одним из `guards`.
 *
 * @example
 * ```
 * // проверка конекста доступа к сегменту
 * const [isAuthenticated, err] = useRouteGuards({ segments: ['CAS_DBO_BSS_FRAME'], guards: [segmentGuard] })
 * ```
 */
export declare const useRouteGuards: <T extends IParsedToken>({ segments, userContext, guards, }: IRouteContextOptions & {
    guards: ContextGuard<T>[];
}) => [boolean, (Error | undefined)?];
