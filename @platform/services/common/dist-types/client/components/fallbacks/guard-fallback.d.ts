import React from 'react';
import type { IButtonAction } from '@platform/ui';
import type { IFallbackRouteProps } from '../../../components';
export interface IFallbackActionsProps {
    /**
     * Список действий, доступных со страницы.
     */
    actions?: IButtonAction[];
}
export interface IGuardFallbackConfigurableProps extends IFallbackActionsProps {
    /**
     * Заголовок на странице.
     */
    header?: string | (() => string);
    /**
     * Подробное сообщение/расшифровка ошибки.
     */
    description?: string | (() => string);
}
export declare type IGuardFallbackProps = IFallbackRouteProps & IGuardFallbackConfigurableProps;
/**
 * Компонент-заглушка, предоставляющая шаблон для кастомизации страницы с сообщением об ограничении доступа.
 *
 * Позволяет передать заголовок страницы, текст сообщения и список кнопок на странице.
 */
export declare const GuardFallback: React.FC<IGuardFallbackProps>;
/**
 * Хок, прокидывающий в `GuardFallback` заголовок `header`, описание `description` и `actions` на странице.
 *
 * @param options Настройки кастомизации шаблона.
 * @param displayName DisplayName компонента, созданного через этот hoc.
 */
export declare const fallbackHoc: (options: IGuardFallbackConfigurableProps, displayName?: string) => React.FC<IGuardFallbackProps>;
