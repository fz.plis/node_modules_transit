/// <reference types="react" />
/**
 * Fallback-страница с сообщением о недоступности для юзера ЭКО.
 *
 * Используется при проверке на контекст юзера БСС.
 */
export declare const DboFallback: import("react").FC<import("./guard-fallback").IGuardFallbackProps>;
/**
 * Fallback-страница с сообщением о недоступности функционала для выбранной организации.
 *
 * Используется для информирования об отсутствии доступа, если у организации нет требуемых сегментов.
 */
export declare const SegmentFallback: import("react").FC<import("./guard-fallback").IGuardFallbackProps>;
/**
 * Дефолтная fallback-страница с сообщением об отсутствии прав.
 *
 * Используется в общем случае.
 */
export declare const DefaultFallback: import("react").FC<import("./guard-fallback").IGuardFallbackProps>;
