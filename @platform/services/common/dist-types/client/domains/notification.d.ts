import type { IRenderNotification, IUserNotification, INotificationDomain } from '../../interfaces';
export declare const notificationDomain: import("@platform/tools/istore").Unit<INotificationDomain, {
    getCount: () => void;
    canPostpone: () => boolean;
    postponeNotifications: () => Promise<IUserNotification[]>;
    postponeNotification: (id: string) => Promise<IUserNotification>;
    getImportantNotification: (importantCount?: number) => Promise<void>;
    getPostponedNotifications: () => IUserNotification[];
    merge: (updates: Partial<INotificationDomain>) => void;
    setNotificationCount: (count: number) => void;
    setImportantNotificationMessage: (message: IRenderNotification) => void;
    setImportantNotificationUrl: (url: string) => void;
    setShowImportantNotification: (isShow: boolean) => void;
    setShowImportantNotificationCount: (importantCount: number) => void;
    setImportantNotifications: (notifications: IUserNotification[]) => void;
}>;
