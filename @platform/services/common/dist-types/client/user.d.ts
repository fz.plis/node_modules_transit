import React from 'react';
import type { IBankClient, IUserData } from '../interfaces';
import { UserType } from '../interfaces';
/**
 * Контекст пользователя банка.
 */
interface IUserContext {
    /**
     * Id Сертификата под которым пользователь зашел в систему.
     */
    defaultUserCertId: string | undefined;
    /**
     * Фатальная ошибка загрузки пользователя.
     */
    fatalErr: boolean;
    /**
     * Флаг показывающий, что пользователь является ДБО-пользователем.
     */
    isDboUser: boolean;
    /**
     * Данные о пользователе.
     */
    user: IUserData;
    /**
     * Список организаций.
     */
    userOrganizations: IBankClient[];
    /**
     * Перезагрузить список организаций, повторно проинициализировав их в контексте и в mainDomain.
     */
    reloadOrganizations(): void;
    /**
     * Тип пользователя.
     */
    userType: UserType;
    /**
     * Установить параметры пользователя.
     *
     * @param user Пользователь.
     */
    setUser(user: IUserData): void;
    /**
     * Метод обновления email.
     *
     * @param onClose - Закрытие диалогового окна.
     */
    updateUserEmail(onClose: () => void): void;
    /**
     * Метод обновления телефона.
     *
     * @param onClose - Закрытие диалогового окна.
     */
    updateUserPhone(onClose: () => void, disableResultStep?: boolean): void;
}
export declare const UserContext: React.Context<IUserContext>;
export declare const getUserValue: () => {
    defaultUserCertId: string;
    fatalErr: boolean;
    isDboUser: boolean;
    user: IUserData;
    userOrganizations: IBankClient[];
    reloadOrganizations: () => void;
    userType: UserType;
    setUser: (_: IUserData) => void;
    updateUserEmail: (onClose?: () => void) => void;
    updateUserPhone: (onClose?: () => void, disableResultStep?: boolean | undefined) => void;
};
export declare const getUserItem: <K extends "fatalErr" | "defaultUserCertId" | "user" | "userType" | "userOrganizations" | "updateUserPhone" | "updateUserEmail" | "isDboUser" | "reloadOrganizations">(key: K) => IUserContext[K];
export declare const UserProvider: React.FC;
export declare const useUser: () => IUserContext;
export {};
