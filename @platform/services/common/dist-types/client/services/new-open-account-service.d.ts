/** Клиентский сервис для откртия о2+. */
export declare const newOpenAccountService: {
    checkIfNewO2IsAvailable: () => Promise<{
        data: boolean;
    }>;
};
