# Аутентификация

Контекст аутентфикации отвечает за вход и выход пользователя в/из системы.

Контекст имеет следующий интерфейс

```tsx
interface IAuthContext {
  authorities: string[]
  config: IAuthConfiguration | null
  fatalErr: boolean
  provider: OAuthProvider | null
  roles: string[]
  token: string
  getAvailableActions(actions: IActionWithAuth[]): IActionWebInfo<any, any>[]
  getAvailableActionsWithCategory(actions: { [key: string]: IActionWithAuth[] }): IButtonConfig<any, any>
  hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean
  hasAuthority(...userAuthorities: string[]): boolean
  logout(): void
  redirectToLogin(): void
}
```

Контекст предоставляет следующую информацию:

1. Токен пользователя
1. Конфиг аутентфикации
1. Ошибку загрузки токена
1. Информация о провайдере
1. Привилегии пользователя
1. Роли пользователя

Контекст предоставляет следующие методы

1. getAvailableActions - Доступные действия по привилегиям пользователя
1. getAvailableActionsWithCategory - Доступные действия для каждой категории по привилегиям пользователя
1. hasAllowedContextType - Позволяет проверить может ли пользовать ЭКО/ДБО выполнять действие
1. hasAuthority - Проверка привилегии пользователя
1. logout - метод выхода из системы
1. redirectToLogin -  метод перенаправления на логин

# Использование
Веб-приложение обернуто в компонент, предоставляющий React Context с методами и данными.

```tsx
ReactDOM.render(
  <ThemeProvider>
    <FatalErrorBoundary>
      <AppConfig url="/api/config-service/config/configuration/pr/lb/config.json">
        <AuthProvider authLoader={authService.getAuthConfiguration}>
          <App />
        </AuthProvider>
      </AppConfig>
    </FatalErrorBoundary>
  </ThemeProvider>,
  document.getElementById('app')
);
```

Далее из любого места приложения можно получить нужные данные

```tsx
const Page: React.FC = () => {
  // обращаемся к контексту аутентифкации.
  const { token, logout, authorities  } = useAuth();

  return (
    ...
  );
};
```

Для доступа к данным не из реакт-компонентов можно использовать функцию `getAuthValue` и `getAuthItem`

```tsx

const auth = getAuthValue()

const roles = getAuthItem('roles')

```
