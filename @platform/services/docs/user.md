# Информация о пользователе

Контекст пользователя позволяет получить информацию о пользователе

## Контекст клиента банка содержит следующие данные

```tsx
interface IUserContext {
  defaultUserCertId: string | undefined
  fatalErr: boolean
  isDboUser: boolean
  roles: string[]
  user: IUserData
  userOrganizations: IBankClient[]
  userType: UserType
  setUser(userData: Partial<IUserData>): void
  updateUserEmail(onClose: () => void): void
  updateUserPhone(onClose: () => void, disableResultStep?: boolean): void
}
```
, где:

1. defaultUserCertId - Идентификатор сертификата под которым пользователь зашел в систему
1. fatalErr - Фатальная ошибка загрузки пользователя
1. isDboUser - флаг показывает является ли пользователь ДБО пользоваетелем
1. user - информация о пользователе
1. userOrganizations - список организаций пользователя
1. userType - тип клиента
1. updateUserEmail - метод обновления email-a   
1. setUser - метод обновления/установки значения пользователя
1. updateUserPhone - метод обновления телефона

## Контекст пользователя банка содержит следующие данные

```tsx
interface IUserContext {
  roles: string[]
  user: IUserData
  setUser(userData: Partial<IUserData>): void
  fatalErr: boolean
}
```
, где:
1. user - информация о пользователе
1. fatalErr - Фатальная ошибка загрузки пользователя

# Использование
Веб-приложение обернуто в компонент, предоставляющий React Context с методами и данными пользователя.

```tsx
ReactDOM.render(
  <ThemeProvider>
    <FatalErrorBoundary>
      <AppConfig url="/api/config-service/config/configuration/pr/lb/config.json">
        <UserProvider>
          <App />
        </UserProvider>
      </AppConfig>
    </FatalErrorBoundary>
  </ThemeProvider>,
  document.getElementById('app')
);
```

Далее из любого места приложения можно получить нужные данные
```tsx
const Page: React.FC = () => {
  // обращаемся к контексту пользователя.
  const { user, userOrganizations } = useUser();

  return (
    ...
  );
};
```

Для доступа к данным не из реакт-компонентов можно использовать функцию `getUserValue` и `getUserItem`

```tsx

const auth = getUserValue()

const user = getUserItem('user')

```



