# Генератор скроллера

Существует 2 генератора:
* `getCardScrollerPage` - создает скроллер карточек (обычно используется в клиентской части [пример макета](https://app.zeplin.io/project/5e9e497d77ad30524b133b71/screen/5ec7b921658ec345e1203ced))
* `getRowScrollerPage` - создает скроллер в виде таблицы (обычно используется в банковской части [пример макета](https://app.zeplin.io/project/5e9e497d77ad30524b133b71/screen/5f436fde28bc5f2d497a0a80))

Их конфиги по большей части одинаковы за исключением нескольких полей.

Интерфейс общей части конфига:

```ts

interface IScrollerGeneratorProps<T> {
  fetcher: (metaData: IMetaData) => Promise<ICollectionResponse<T>>
  categories: {
    defaultCategory: string
    locale: (localeItem: string) => string
    fetcher: (metaData: IMetaData) => Promise<Array<{ name: string; value: string; desc: string }>>
  }
  filters?: {
    fields: { [key: string]: IFilterField }
    labels: { [key: string]: string }
    component: React.ComponentType
  }
  sorting?: {
    byDefault: {
      fieldName: string
      direction: SORT_DIRECTION
    }
    sortFields: ISortField[]
  }
  selectable?: boolean
  executer: IExecuter<TExecuterContext>
  toolbarActions?: IToolbarActionsGetter | Array<IActionWithChildren | IActionWithAuth>
  toolbarActionsVisibleCount?: number;
  header: string
  mainLayout: React.ComponentType
  storageKey?: string
  breadcrumbs?: IBreadcrumbsExtended[];
  hideHeader?: boolean;
  compactHeight?: boolean;
  actionsParams?: unknown;
  handleCategoryChange?({filters, sort}): void
}
```
где:

fetcher (обязательный параметр) - метод загрузки данных
 
categories (обязательный параметр) - параметр отвечающий за загрузку категорий скроллера

filters - параметр отображения фильтра

sorting - параметр отображения сортировки

selectable - параметр отвечающий за мультивыбор

executer - Экшн экзекутор, через который будут исполняться действия скроллера и строк.

toolbarActions - Массив действий в тулбаре скроллера или геттер, который получает текущую категорию и массив выделенных строк и должен вернуть массив действий.

toolbarActionsVisibleCount - Количество отображаемых действий в тулбаре. По умолчанию 3.

header - Заголовок скроллера

mainLayout - Мэйнлэйаут, в который все будет обернуто, можно передать клиентский или админский.

storageKey - Ключ, по которому будем доставать данные из хранилища. Если не передан, то вместо хранилища (localstorage) будет использован обычный React.useState.

breadcrumbs - Кастомные хлебный крошки. По умолчанию будут крошки вида: На главнвую -> Заголовок скроллера

hideHeader - Видимость заголовка скроллера. Если true, то заголовок будет скрыт

compactHeight - Высота скроллера. Если false, то высота скроллера будет во весь экран, даже если записей меньше

actionsParams - Дополнительные параметры для действий со строками скроллера

handleCategoryChange - Метод, вызываемый при смене категории



## Руководство к подготовке общей части конфига для генератора скроллера (на примере rko-service-management)

Перед созданием конфига генератора, необходимо подготовить следующие сущности, которые войдут в конфиг:

### `fetcher`

Нужен сервис метод `getList` для загрузки списка элементов

* `getList` - для загрузки списка элементов
* `getCounter` для запроса данных по категориям

### `categories`

* `fetcher` - обычно это сервис метод `getCounter` для запроса данных по категориям
* `locale` - транслэйтор, который мы получили через функцию `getTranslator`
* `defaultCategory` - дефолтная категория, это элемент из enum-а `CATEGORY`, обычно `CATEGORY.ALL`

### `filters`

`fields` - Правила фильтрации для каждого поля
```ts

import { filterFields } from '@platform/services'

const FIELDS = {
  [FILTER_FIELD.PRODUCT_TYPE_CODE]: filterFields.eq(undefined, FILTER_FIELD.PRODUCT_TYPE_CODE),
  [FILTER_FIELD.DATE_FROM]: filterFields.ge(undefined, FILTER_FIELD.DATE, formatStartDateTime),
  ...
};
```

`labels` - Лэйблы для полей фильтрации
```ts

const FILTER_FIELD_LABELS = {
  [FILTER_FIELD.PRODUCT_TYPE_CODE]: locale.filter.service.label,
  [FILTER_FIELD.DATE]: locale.filter.date.client.label,
  [FILTER_FIELD.DATE_FROM]: locale.filter.dateFrom.client.label,
  ...
};
```

`component` - Компонент для отображения полей фильтров
```tsx

const Filter:React.FC = () => (
  <Adjust pad={[undefined, 'XL']}>
    <Row label={FILTER_FIELD_LABELS[FILTER_FIELD.DATE]}>
      <Label position={LABEL_POSITION.LEFT} text={locale.filter.from}>
        <Fields.Date name={FILTER_FIELD.DATE_FROM} />
      </Label>
      <Gap />
      <Label position={LABEL_POSITION.LEFT} text={locale.filter.to}>
        <Fields.Date name={FILTER_FIELD.DATE_TO} />
      </Label>
    </Row>
    <Row label={FILTER_FIELD_LABELS[FILTER_FIELD.NUMBER]}>
      <Fields.Number name={FILTER_FIELD.NUMBER} />
    </Row>
    ...
  </Adjust>
);
```


### `sorting`
`sortFields` - массив объектов типа 

```ts

interface ISortField { 
  label: string
  name: string 
}
```
где
`name` - это название поля, по которому возможна сортировка
`label` - это то, как это поле будет отображаться в интерфейсе

```ts

const SORT_FIELDS = [
  { label: locale.scroller.sort.field.date, name: SORT_FIELD.DATE },
  { label: locale.scroller.sort.field.number, name: SORT_FIELD.NUMBER },
  { label: locale.scroller.sort.field.status, name: SORT_FIELD.DOCUMENT_STATUS },
];
```

`byDefault`- Настройки сортировки по умолчанию (необязательный параметр)
```ts

{
  fieldName: SORT_FIELD.DATE
  direction: SORT_DIRECTION.ASC
}
```


### `toolbarActions`
В качестве `toolbarActions` можно напрямую передавать массив экшнов или функцию, которая получит в параметрах конфиг вида:
```tsx
interface IToolbarActionsGetterConfig<TRow> {
  /** Активная категория. */
  category?: string;

  /** Массив выделенных строк. */
  selectedRows: TRow[];
}
```
которая вернет массив из обычных экшн-конфигов `IActionWithAuth`. Также генератор поддерживает  экшн конфиг с полем `children` (должен быть под 0-ым индексом в массиве), куда можно класть `IActionWithAuth`. Используется для конфигурации кнопки "Создать", по нажатию на которую выпадет дропдаун со списком документов [Zeplin](https://app.zeplin.io/project/5e9e497d77ad30524b133b71/screen/5f2ca25ec23ade28879ac690)

Создаем экшн конфиг для кнопки "Создать"
Для каждой кнопки из выпадающего списка, должен быть свой конфиг

```ts

export const CREATE_DOC1: IActionWithAuth = {
  icon: Icons.NewTemplate,
  label: locale.action.create.periodicMoneyTransfer.label,
  action: create,
  name: 'CREATE_BUTTON',
  authorities: [PRIVILEGE.CREATE],
};

export const CREATE_DOC2: IActionWithAuth = {
  icon: Icons.NewTemplate,
  label: locale.action.create.periodicMoneyTransfer.label,
  action: create,
  name: 'CREATE_BUTTON',
  authorities: [PRIVILEGE.CREATE],
};
```

Затем затем кладем эти конфиги в поле children для конфига кнопки "Создать"
```ts

/** Кнопка "Создать" в тулбаре скроллера с выпадающим списком других действий. */
export const CREATE_ACTIONS: IActionWithChildren = {
  label: locale.scroller.action.create.label,
  name: 'CREATE',
  icon: Icons.Plus,
  children: [CREATE_DOC1, CREATE_DOC2],
};
```

Добавляем получившийся конфиг в качестве 0-го элемента массива, при необходимости докинув другие конфиги
```ts

const SCROLLER_ACTIONS = [CREATE_ACTIONS, ...OTHER_ACTIONS as IActionWithAuth[]]

{
  getToolbarActions: ({category, selectedRows}) => SCROLLER_ACTIONS
}
```


### `executer`
В конфиг необходимо добавить экзекутор, через который будут исполнятся переданные экшны.
Переданный экзекутор, внутри генератора будет предварительно обвязан миддлварью, которая будет перезагружать данные скроллера после каждого успешно выполненного действия. 

Так же обвязанный экзекутор записывается в react-context, чтобы все дочерние компоненты (например компонент карточки скроллера) имели к нему доступ. Для этого есть хук `useScrollerExecuter`

```ts

import { useScrollerExecuter } from '@platform/services';
```

### `mainLayout`
это компонент, в который будет обернута вся страница, 

для клиентской части это
```ts

import { MainLayout } from '@platform/services/client';
```
для банковской
```ts

import { MainLayout } from '@platform/services/admin';
```

### `storageKey`
Передача этого параметра включает у скроллера функцию записи фильтров в хранилище и инициализацию из хранилища. Этот ключ должен быть уникальным у каждого скроллера, чтобы значения из разных скроллеров не перемешивались. Название стрима + название скроллера в качестве ключа должно подойти.
```tsx
{
  storageKey: 'rko-service-management/client';
}
```


## Генератор скроллера карточек

`getCardScrollerPage` принимает конфиг следующего вида

```ts

interface ICardScrollerGeneratorProps<TRow> extends IScrollerGeneratorProps<TRow>{
  /** Компонент карточки скроллера */
  card: React.ComponentType<Omit<IRowTemplateProps, 'row'> & { row: TRow }>
}  
```

### `card`
Это реакт компонент, который будет получать данные документа, который он отображает в качестве пропа `row`

```tsx

export interface ICardProps extends IRowTemplateProps {
  row: IPeriodicMoneyTransfer;
}

export const Card: React.FC<ICardProps> = ({ row }) => ...
```

Так-же, мы можем заюзать экзекутор с миддлварью из этого компонента

```tsx

import { useScrollerExecuter } from '@platform/services';

export const Card: React.FC<ICardProps> = ({ row }) => {
  const executer = useScrollerExecuter();
  ...
```

### Пример итогового конфига для скроллер-генератора карточек
```ts

export const ScrollerPage = getCardScrollerPage({
  fetcher: periodicMoneyTransferService.getList,
  categories: {
    fetcher: periodicMoneyTransferService.getCounter,
    locale: getTranslator(LOCALIZATION_RESOURCE),
    defaultCategory: CATEGORY.ALL,
  },
  filters: {
    fields: FIELDS,
    labels: FILTER_FIELD_LABELS,
    component: Filter,
  },
  sorting: {
    sortFields: SORT_FIELD_LABELS,
  },
  header: locale.scroller.bank.title,
  executer: moneyTransferExecuter,
  actions: SCROLLER_ACTIONS,
  card: Card,
  handleCategoryChange: ({filters, sort}) => filters.onClear()
});
``` 

## Генератор скроллера табличного типа

`getRowScrollerPage` принимает конфиг следующего вида

```ts

interface IRowScrollerGeneratorProps extends IScrollerGeneratorProps<TRow>{
   /** Колонки для омни таблицы */
  columns: IOmniTableColumn<TRow>[]

  /** Массив действий для строк или геттер для получения массива действий.*/
  rowActions?: RowActionsGetter | IActionWithAuth[]

  /** Компонент для рендера содержимого строки в раскрытом состоянии, эта опция включает возможность раскрытия и сворачивания строк. */
  rowDetailsTemplate?: React.ComponentType<{
    /** Данные строки */
    row: TRow
  }>

  /** Двойной клик по строке. Получает текущую категорию и данные строчки в качестве первого аргумента
   * и react-router history в качестве второго.
   * 
   * @example
   * 
   onRowDoubleClick: ({ row }, router) => {
      router.push(`${ADMIN_STREAM_URL.CONSTITUTION}/${row.id}`);
    }
    */
  onRowDoubleClick?(config: IOnRowDoubleClick<TRow>, router: RouterProps['history']): void;
}  
```


### `columns`
Составим конфиг колонок по [макету](https://app.zeplin.io/project/5e9e497d77ad30524b133b71/screen/5f436fde28bc5f2d497a0a80)

```tsx

const columns = [
    {
      title: locale.pmts.scroller.columnLabels.dateAndNumber,
      width: 106,
      selector(row) {
        return (
          <>
            <Typography.Text line="COLLAPSE" title={row.date}>
              {row.date}
            </Typography.Text>
            <Gap.XS />
            <Typography.SmallText inline fill="FAINT">
              {row.number}
            </Typography.SmallText>
          </>
        );
      },
    },
    {
      title: locale.pmts.scroller.columnLabels.sender,
      width: 212,
      selector(row) {
        return (
          <>
            <Typography.Text line="COLLAPSE" title={row.date}>
              {row.bankClientInfo?.shortName}
            </Typography.Text>
            <Gap.XS />
            <Typography.SmallText inline fill="FAINT">
              {locale.card.inn.label({ val: row.bankClientInfo?.inn || '' })}
            </Typography.SmallText>
            <Gap.X2S />
            <Typography.SmallText inline fill="FAINT">
              {locale.card.account.label({ val: formatAccountCode(row.senderAccountInfo?.accountNumber || '') })}
            </Typography.SmallText>
          </>
        );
      },
    },
    {
      title: locale.pmts.scroller.columnLabels.receiver,
      width: 215,
      selector(row) {
        return (
          <>
            <Typography.Text line="COLLAPSE" title={row.receiverInfo?.receiverName}>
              {row.receiverInfo?.receiverName}
            </Typography.Text>
            <Gap.XS />
            <Typography.SmallText inline fill="FAINT">
              {`${locale.card.inn.label({ val: row.receiverInfo?.receiverInnKio || '' })}, ${locale.card.kpp.label({
                val: row.receiverInfo?.receiverKpp || '',
              })}`}
            </Typography.SmallText>
            <Gap.X2S />
            <Typography.SmallText inline fill="FAINT">
              {`${row.receiverInfo?.bankName || ''}, ${locale.card.bic.label({
                val: row.receiverInfo?.bankBic || '',
              })}`}
            </Typography.SmallText>
          </>
        );
      },
    },
    {
      title: locale.pmts.scroller.columnLabels.serviceType,
      width: 170,
      selector() {
        return <Typography.Text line="BREAK">{locale.common.serviceType.periodicMoneyTransferAdd}</Typography.Text>;
      },
    },
    {
      title: locale.pmts.scroller.columnLabels.status,
      width: 184,
      selector(row) {
        return (
          <>
            <Box className={css.status}>
              <Status type={ADMIN_STATUS_COLOR[row.status]} />
              <Typography.Text line="BREAK">{ADMIN_STATUS_LABEL[row.status]}</Typography.Text>
            </Box>
            <Gap.SM />
            <Horizon>
              <Gap />
              <SecondRow collapse>{row.commentForBank || ''}</SecondRow>
            </Horizon>
          </>
        );
      },
    },
  ],
```

### `rowActions`
это обычный массив IActionWithAuth или функция, которая получит объект следующего вида
```ts
/** Конфиг для функции для получения массива экшнов. */
interface IRowActionsGetter<TRow> {
  /** Объект с данными текущей строчки. */
  row: TRow;

  /** Текущая категория. */
  category?: string;
}
```
и должна будет вернуть массив `IActionWithAuth`

```ts

const EXPORT_WITH_SIGN: IActionWithAuth = {
  icon: Icons.Export,
  label: locale.action.exportWithSign.label,
  action: exportWithSign,
  name: 'EXPORT_WITH_SIGN_BUTTON',
  authorities: [PRIVILEGE.UNLOAD_SIGN],
};

const VIEW: IActionWithAuth = {
  icon: Icons.Edit,
  label: locale.action.view.label,
  action: view,
  name: 'VIEW_BUTTON',
  authorities: [PRIVILEGE.VIEW],
};
...

/** Набор действий, вызываемых по кнопке "..." */
export const ROW_ACTIONS = [VIEW, EXPORT_WITH_SIGN, ...];
```

### `rowDetailsTemplate`

```tsx

const RowDetailsTemplate: React.FC<{row: IPeriodicMoneyTransfer}> = ({row}) => <Box>{row.id}</Box>
```

### `onRowDoubleClick`
Колбэк на двойной клик по строке. Получает текущую категорию и данные строчки в качестве первого аргумента и react-router history в качестве второго.

```tsx
{
  ...,
  onRowDoubleClick: ({ row }, router) => {
    router.push(`${ADMIN_STREAM_URL.CONSTITUTION}/${row.id}`);
  }
  ...,
}

```


### Пример итогового конфига
```ts

export const ScrollerPage = getRowScrollerPage({
  fetcher: periodicMoneyTransferService.getList,
  categories: {
    fetcher: periodicMoneyTransferService.getCounter,
    locale: translator,
    defaultCategory: CATEGORY.ALL,
  },
  filters: {
    fields: FILTER_FIELDS,
    labels: FILTER_FIELD_LABELS,
    validate: validateFilters,
    component: Filter,
  },
  sorting: {
    byDefault: {
      direction: SORT_DIRECTION.DESC,
      fieldName: SORT_FIELD.DATE,
    },
    sortFields: SORT_FIELD_LABELS,
  },
  selectable: true,
  actions: SCROLLER_ACTIONS,
  header: locale.scroller.bank.title,
  executer: moneyTransferExecutor,  
  mainLayout: MainLayout,
  columns: columns,
  rowActions: CARD_ACTIONS,
  rowDetailsTemplate: RowDetailsTemplate,
  handleCategoryChange: ({filters, sort}) => filters.onClear()
});

``` 


## Передача фильтров из стейта.
Чтобы инициализировать скроллер с фильтрами из стейта необходимо сделать обертку над скроллером, достать их оттуда и передать в качестве пропов в сгенерированный компонент.
Эти пропы имеют самый высокий приоритет при инициализации, и если они переданы то значения из конфига или из хранилища будут игнорированы. И значение в хранилище перезапишется значениями в этих пропах.
Так-же эти пропы используются только при инициализации начальных значений фильтров, если во время работы скроллера эти пропы изменятся, это ни как не повлияет на текущие значения фильтров.

```tsx
/** Пропы для скроллера. */
interface ScrollerPageProps {
  /** Начальное значение категории, если передано этот проп, то оно будет испольоваться
   * вместо  значения из конфига для генератора или значения из хранилища. */
  initialCategory?: string;

  /** Начальное значение фильтров, если передано, то оно будет испольоваться
   * вместо  значения из конфига для генератора или значения из хранилища. */
  initialFilters?: { [key: string]: unknown };
}
```

```tsx
const Scroller = getRowScrollerPage({...})

export const ScrollerPage: React.FC = () => {
  const history = useHistory();

  const location = useLocation();

  // Необходимо после извлечения стейта, сразу-же его сбросить, чтобы при перезагрузке страницы фильтры снова не подтянулись из стейта.
  React.useEffect(() => {
    history.replace(location.pathname);
  }, [history, location.pathname]);

  return <Scroller initialCategory={location.state?.category} initialFilters={location.state?.filters} />;
};
```

##Получение информации о скроллере
Для возможности получения информации о скроллере необходимо использовать хук `useScrollerData`

```tsx
import { useScrollerData } from '@platfrom/services'

const Filter: React.FC = () => {
  const {category, sort, filters} = useScrollerData()
  
  
  return (<div>Filter</div>)
}
```
