# Руководство по миграции со старого генератора на новый 

Большая часть конфига не изменилась, но пропы, которые мы кидали на сгенерированный `Scroller` вошли в конфиг генератора.

Раньше мы сначала генерировали скроллер, а затем создавали компонент, в котором использовали сгенерированный скроллер, прокинув на него необходимые пропы 

```tsx

 const Scroller = getScrollerPage({
  fetcher: periodicMoneyTransferService.getList,
  categories: {
    fetcher: periodicMoneyTransferService.getCounter,
    locale: getTranslator(LOCALIZATION_RESOURCE),
    defaultCategory: CATEGORY.ALL,
  },
  filters: {
    fields: FIELDS,
    labels: FILTER_FIELD_LABELS,
    component: Filter,
  },
  sorting: {
    sortFields: SORT_FIELD_LABELS,
  },
 });

export const ScrollerPage = () => (
  <MainLayout>
    <ScrollerContext.Provider value={{ getCardActions: getCardActions(CARD_ACTIONS) }}>
      <Scroller
        card={Card}
        getActions={(category, selectedRows, reload) => {
          const executor = getExecutor(reload);
          return getScrollerActionButtons({ [category]: SCROLLER_ACTIONS }, category, selectedRows, executor);
        }}
        header={locale.scroller.bank.title}
      />
    </ScrollerContext.Provider>
  </MainLayout>
);
 ```

## Сейчас сгенерированный скроллер можно сразу экспортировать и подставлять в роут

* проп onHomeClick больше не нужен, нажатие на кнопку "Домой" будет всегда редиректить на `/mainpage'
* поле конфига `showCheckBox` переименовался в `selectable`
* MainLayout теперь кладется в конфиг
* Больше нет нужды создавать контекст с executor-ом, это  делается внутри генератора, для доступа к контексту используйте хук `useScrollerExecuter` из `@platform/services`
* Больше не нужна функция-геттер для получения действий для тулбара скроллера, теперь массив из экшн конфигов можно сразу класть в конфиг генератора в поле `actions`

## Пример использования скроллера
```ts

export const ScrollerPage = getCardScrollerPage({
  fetcher: periodicMoneyTransferService.getList,
  categories: {
    fetcher: periodicMoneyTransferService.getCounter,
    locale: getTranslator(LOCALIZATION_RESOURCE),
    defaultCategory: CATEGORY.ALL,
  },
  filters: {
    fields: FIELDS,
    labels: FILTER_FIELD_LABELS,
    component: Filter,
  },
  sorting: {
    sortFields: SORT_FIELD_LABELS,
  },
  header: locale.scroller.bank.title,
  executer: moneyTransferExecuter,
  actions: SCROLLER_ACTIONS,
  card: Card,
  mainLayout: MainLayout,
});
``` 

