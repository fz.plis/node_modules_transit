# Hooks для скроллера

Для упрощения и ускорения работ по созданию новых страниц скроллера были разработаны несколько пользовательских хуков.

## useScroller

Формат входных данных:

```ts
interface IConfig<T> {
  fetcher(metaData: IMetaData): Promise<ICollectionResponse<T>>;
  requestParams?: Partial<IMetaData>;
  disableErrorHandling?: boolean;
}
```

где

* fetcher - функция для загрузки данных с апи
* requestParams - параметры запроса, такие как pageSize, filters, offset и т.д.
* disableErrorHandling - флаг для отключения внутреннего обработчика ошибок хука, обработка ошибок в данном случае ложится на разработчика, который использует этот хук.

Данный хук возвращает объект вида:

```ts

import { IScroller } from '@platform/ui' 

interface IUseScroller {
  methods: {
    reload: () => Promise<void>
  }
  props: Pick<IScroller, 'rows' | 'selectedRows' | 'onChangeSelectedRows' | 'onIntersecting'>
  state: {
    isLoading: boolean
    hasError: boolean
    error: any
  }
}
```

где

* methods.reload — функция для ручной загрузки данных, например обновить данные страницы скроллера после успешного действия над документом (подпись)
* props — набор props для компонента Scroller
* state.isLoading — флаг, показывающий что в данный момент скроллер находится в состоянии загрузки данных
* state.hasError — флаг, показывающий наличие ошибок. Переходит в true, когда запрос вернул ошибку.
* state.error — Содержит информацию об ошибке.

## useToolbarSort

Данный хук возвращает props сортировки для компонента ScrollerToolbar, а также параметры сортировки для запроса списка

## useToolbarCheckbox

Данный хук возвращает props флажка для компонента ScrollerToolbar

## useFilter

Данный хук возвращает props фильтра для компонента ScrollerManager и ScrollerFilter

## useCategory

Данный хук возвращает props `categories` для ScrollerManager

Это список категорий со счетчиками, также он возвращает метод перезагрузки

Формат выходных данных:

```tsx

{
  categories: [],
  methods: {
    reload: () => {},
  },
}
```

Так же хуки `useCategory`, `useFilter` и `useToolbarSort` умеют работать с хранилищем. Это значит, что они будут текущие значения записывать в хранилище и инициализироваться значениями из хранилища, и если там пусто, то тогда дефолтными значениями из конфига.
для этого у них в конфиге есть поле, которое включает эту возможность.

```tsx
{
  /** Ключ, по которому будем доставать данные из хранилища. Если не передан, то
   * вместо хранилища (localstorage) будет использован обычный React.useState. */
  storageKey?: string;
}
```

Этот ключ должен быть уникальным у каждого скроллера, чтобы значения из разных скроллеров не перемешивались. Название стрима + название скроллера в качестве ключа должно подойти.

### Пример использования хуков

```tsx

import React, { useState, useCallback } from 'react'
import { LayoutScroller, ScrollerManager, ScrollerToolbar, Scroller, FilterTags } from '@platform/ui'
import { ICollectionResponse } from '@platform/core'
import { useScroller, useToolbarSort, useFilter, useCategory, IMetaData, Filter as ScrollerFilter } from '@platform/services'
import { getTranslator } from 'utils'

interface IMyDocument {
  name: string
  dateOfBirth: string
}

const fakeFetcher = (metaData: IMetaData): Promise<ICollectionResponse<IMyDocument>> =>
  new Promise(resolve => {
    console.log(metaData)

    return resolve({
      data: [
        { name: 'Вася', dateOfBirth: '23.05.1993' },
        { name: 'Петя', dateOfBirth: '08.09.1993' },
      ],
      total: 2,
    })
  })

const fakeCategoryFetcher = (metaData: IMetaData): Promise<ICollectionResponse<IMyDocument>> =>
  new Promise(resolve => {
    console.log(metaData)

    return resolve([
        {"name":"ALL","desc":"user.notification.category.all.description","count":0},
        {"name":"UNREAD","desc":"user.notification.category.unread.description","count":0},
        {"name":"IMPORTANT","desc":"user.notification.category.important.description","count":0}
      ])
  })

const categoryTranslator = getTranslator('TEST');

const MyScrollerPage = () => {
  const { filterValues, filterPanel, tagsPanel } = useFilter({
    fields: {
      name: filterFields.eq('Ivan'),
      age: filterFields.eq(0),
    },
    labels: {
      name: 'Имя',
      age: 'Возраст'
    }  
  })
  const [category, setCategory] = useState<string>(categories[0].value)
  const [toolbarSort, sort] = useToolbarSort({
    sortFields: [
      { name: 'name', label: 'Имя' },
      { name: 'dateOfBirth', label: 'Дата рождения' },
    ],
  })
  const { props: scroller } = useScroller<IMyDocument>({
    fetcher: fakeFetcher,
    requestParams: { sort, category, filter: filterValues },
  })

  const requestParams = {
    pageSize: 50,
    offset: 0,
    sort,
    category,
    filter: filterValues,
  }
  
  const { categories } = useCategory(fakeCategoryFetcher, requestParams, categoryTranslator)

  const handleCategoryChange = useCallback((value: string) => {
    setCategory(value)
  }, [setCategory])

  return (
    <>
      <LayoutScroller
        manager={
          <ScrollerManager
            header="Пример использования"
            onHomeClick={() => {}}
            filterNode={<FilterTags {...tagsPanel} />}
            categories={categories}
            category={category}
            onCategoryChange={handleCategoryChange}
          />
        }
        toolbar={<ScrollerToolbar {...toolbarSort} showCheckbox />}
      >
        <Scroller {...scroller} rowTemplate={props => <div>{props.row.id}</div>} />
      </LayoutScroller>
      <ScrollerFilter {...filterPanel}>
        <Fields.Text name="name" />
        <Fields.Number name="age" />
      </ScrollerFilter>
    </>
  )
}

```
