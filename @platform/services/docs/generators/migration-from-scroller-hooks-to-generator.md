# Руководство по миграции с хуков на генератор скроллера карточек.

Генератор сам работает на тех же хуках, поэтому просто передаем в генератор конфиг, который мы использовали для хуков.

## Пример скроллера на хуках

```tsx

// Конфиг для useFilter переносится в конфиг генератора в поле filters
const { filterValues, filterPanel, tagsPanel } = useFilter({
  fields: FIELDS,
  labels: FILTER_FIELD_LABELS,
});

// Конфиг для useToolbarSort переносится в конфиг генератора в поле sorting
const [toolbarSort, sort] = useToolbarSort({
  sortFields: SORT_FIELD_LABELS,
});

// Это больше не нужно
const requestParams = {
  pageSize: 50,
  offset: 0,
  sort,
  filters: filterValues,
};

// Конфиг для useCategory переносится в конфиг генератора в поле categories
// за исключением поля requestParams
const {
  categories,
  category,
  methods: { setCategory, reload: reloadCategory },
} = useCategory({
  fetcher: periodicMoneyTransferService.getCounter,
  defaultCategory: CATEGORY.ALL,
  locale: categoryTranslator,
  requestParams,
});

// fetcher из конфига для useScroller переносится в корень конфига генератора в то-же поле
// requestParams - не нужен
const {
  props: scroller,
  state: { isLoading },
  methods: { reload: reloadScroller },
} = useScroller<IPeriodicMoneyTransfer>({
  fetcher: periodicMoneyTransferService.getList,
  requestParams: { ...requestParams, category },
});

const reload = useCallback(() => {
  void reloadCategory();
  void reloadScroller();
}, [reloadCategory, reloadScroller]);

// идет в поле actions
const scrollerActionButtons = getActionButtons(SCROLLER_ACTIONS, moneyTransferExecuter, [scroller.selectedRows]);

// переносится в поле executer без обвязки миддлварями, все происходит внутри генератора
const executer = useMemo(() => applyMiddlewares<IMoneyTransferContext>(onSuccessMiddleware(reload))(getMoneyTransferExecuter()), [
  reload,
]);

// больше не нужен, логика кнопки "Домой" привязывается вунтри генератора и всегда редиректит на '/mainpage'
const goHome = useRedirect(COMMON_STREAM_URL.MAINPAGE);

return (
  <>
  {/** MainLayout переносится в поле mainLayout генератора */}
  <MainLayout>
    <LayoutScroller
      manager={
        <ScrollerManager
          categories={categories}
          category={category}
          filterNode={<FilterTags {...tagsPanel} />}

          {/** header переносится в поле header генератора */}
          header={locale.scroller.title}
          onCategoryChange={setCategory}
          onHomeClick={goHome}
        />
      }
      toolbar={<ScrollerToolbar {...toolbarSort} actions={scrollerActionButtons} showCheckbox={false} />}
    >
      <LimitWidth>
        {/** Оборачивание в контекст провайдер происходит внутри генератора */}
        <ScrollerContext.Provider value={executer}>
          <Scroller {...scroller} rowTemplate={Card} />
        </ScrollerContext.Provider>
      </LimitWidth>
    </LayoutScroller>
    <Filter {...filterPanel} />
    {/* doNotUsePortal чтобы лоадер не перекрывал модалку */}
    <LoaderOverlay doNotUsePortal opened={isLoading} />
    <Gap.LG />
  </MainLayout>
  </>
);
 ```

## Тот-же скроллер через генератор

```ts

export const ScrollerPage = getCardScrollerPage({
  // конфиг для useScroller
  fetcher: periodicMoneyTransferService.getList, 

  // конфиг для useCategory без поля requestParams
  categories: {
    fetcher: periodicMoneyTransferService.getCounter,
    locale: getTranslator(LOCALIZATION_RESOURCE),
    defaultCategory: CATEGORY.ALL,
  },

  // конфиг для useFilter без поля requestParams
  filters: {
    fields: FIELDS,
    labels: FILTER_FIELD_LABELS,
    component: Filter,
  },

  // конфиг для useToolbarSort
  sorting: {
    sortFields: SORT_FIELD_LABELS,
  },

  // проп header для ScrollerManager
  header: locale.scroller.title,
  executer: moneyTransferExecuter,
  actions: SCROLLER_ACTIONS,
  card: Card,
  mainLayout: MainLayout,
});
``` 