## Универсальная проверка подписей

Для проверки подписей документа и его вложений создано действие `universalSignVerify`

Оно может быть стандартным способом подключено к скроллеру или форме документов

Пример:
```js 
export const VERIFY_SIGN: IActionWithAuth = {
  icon: Icons.PersonDocument,
  label: locale.action.verifySignature.label,
  action: action.universalSignVerify,
  name: 'VERIFY_SIGN',
  authorities: [PRIVILEGE.CHECK_SIGN],
};
```

Для корректной работы документ должен содержать массив подписей: `signatures` и, если требуется, массив вложений: `attachments`

```ts
export interface IBaseSignedDocument extends IBaseEntity {
  signatures: ISignatureDoc[]
  attachments?: ISignedAttachment[]
}
```

## Важно!

**Если необходима проверка подписи вложений на банке**, то в сервис который определяется в контексте экзекьютора необходимо добавить метод `downloadBase64`

Он необходим для скачивания файлов

```ts
export interface IBaseSignedDocumentService {
  downloadBase64: (id: string) => Promise<IFileData>
}
```

Это временное решение, поскольку внутреннее хранилище файлов не закрыто привилегиями и напрямую его использовать нельзя.