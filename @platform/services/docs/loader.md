# Контекст элемента загрузки

Служит для отображения элемента загрузки

## Контекст содержит следующие свойства и методы

```tsx
interface ILoaderContext {
  isLoading: boolean
  setIsLoading(state: boolean): void
}
```
, где 
1. isLoading - состояние загрузки
1. setIsLoading - метод установки состояния загрузки 

# Использование
Веб-приложение обернуто в компонент, предоставляющий React Context c методом setIsLoading.

```tsx
export const renderApp = () =>
  ReactDOM.render(
    <ThemeProvider>
      <FatalErrorBoundary>
        <AppConfig autoload url="/api/config-service/config/configuration/pr/lb/bank-config.json">
          <Auth authLoader={authService.getAuthConfiguration}>
            <UserProvider>
              <LoaderProvider>
                <App />
              </LoaderProvider>
            </UserProvider>
          </Auth>
        </AppConfig>
      </FatalErrorBoundary>
    </ThemeProvider>,
    document.getElementById('app')
  )
```

Далее из любого места приложения можно получить нужные данные

```tsx
const Page: React.FC = () => {
  // обращаемся к контексту аутентифкации.
  const { isLoading, setIsLoading } = useLoader();

  return (
    ...
  );
};
```
