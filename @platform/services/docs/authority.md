# Отображение ошибок привилегий пользователя

Позволяет упростить и унифицировать проверку полномочий во всех стримах

Для использования нужно добавить компонент Authority на страницу со следующими свойствами
```ts
/**
 * Привилегия
 */
export interface IAuthorityProps {
  /**
   * Организация пользователя
   */
  bankClient: IBankClient
  /**
   * Тип документа
   */
  documentType?: DOCUMENT_TYPE_CODE
  /**
   * Коды продуктов банка
   */
  productCodes?: string[]
  /**
   * Опции проверки полномочий
   */
  validators?: AUTHORITY_VALIDATORS[]
  /**
   * Сообщение по-умолчанию для проверки полномочий для сертификата
   */
  certificateErrorDefaultMessage?: (() => string) | string
  /**
   * Сообщение по-умолчанию для проверки полномочий для ЕИО
   */
  eioErrorDefaultMessage?: (() => string) | string
  /**
   * Сообщение по-умолчанию для проверки полномочий для конституцию
   */
  constitutionErrorDefaultMessage?: (() => string) | string
}
```

```ts
<Authority bankClient={bankCLient}>
  {hasAuthorityError => ReactNode}
</Authority>
```

Так же отдельно от компонент можно использовать hook useAuthority. На вход принимает те же свойства, что и компонент
Пример использования
```ts
const { errors, hasAuthorityError, removeMessage } = useAuthority({
    bankClient,
    documentType,
    productCodes,
    validators,
    certificateErrorDefaultMessage,
    constitutionErrorDefaultMessage,
    eioErrorDefaultMessage,
  })
```
