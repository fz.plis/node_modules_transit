# Ограничение доступа к роутам

Основной способ ограничить доступ к роутам - использовать компонент `GuardRoute`.

Он расширяет возможности `Route` из react-router-dom. Дополнительно в него можно передать привилегию и тип функции.

## Встроенные роуты

## SecureRoute

Компонент `SecureRoute` работает следующим образом:

- если передан пропс `isAuthenticated` - true, то работает как обычный `Route`.
- если пропс `isAuthenticated` - false, то рендерит вместо контента компонент, переданный в пропс `fallback`, и передает в него ошибку из пропса `authErr`, если она была передана.

```tsx
import React from 'react'
import type { RouteProps } from 'react-router';
import { SecureRoute } from '@platform/services';

const AdminPage: React.FC = () => <h1>Приватная страница</h1>
const Fallback: React.FC = () => <h1>Требуется авторизация</h1>;;
const checkauth = () => !!localStorage.getItem('token');

const RouteWithAuth: React.FC<RouteProps> = props => (
  <SecureRoute
    {...props}
    isAuthenticated={checkauth()}
    fallback={Fallback}
  />
);

export const routes = [
  <RouteWithAuth path="/admin" component={AdminPage}>
];
```

## GuardRoute

Компонент `GuardRoute` работает следующим образом:

1. Проверяет значение пропа `authority`. Если такая привилегия есть у пользователя, то роут работает как `Route` из библиотеки `react-router-dom`
1. Если привилегии у пользователя нет, то отображается ошибка в зависимости от пропа `type`.

```tsx
import React from 'react'
import { GuardRoute } from '@platform/services/client';

const Page = () => <div>Page</div>

export const routes = [
  <GuardRoute authority="PAGE_VIEW" type="PAGE_VIEW" path="/page" component={Page} />
];

```

## Хук useRouteGuards

Хук useRouteGuards позволяет определить есть ли у юзера доступ к чему либо на основе выбранной организации и данных токена.

В хук передается конфиг из требуемых сегментов и контекстов пользователя, а также массив функций `guards`, из него возвращается признак прохождения всех `guards` и первая встреченная ошибка, если проверка не пройдена.

*Его можно использовать в любом компоненте, не только в роутинге.*

```tsx
import { useRouteGuards, dbo3Guard } from '@platform/services/client'; // Импорт из client! Это важно 

const MyPage: React.FC = () => {
  const [isAuthenticated, err] = useRouteGuards({
    // Ограничиваем доступ для всех юзеров, кроме юзеров ДБО БСС
    userContext: [USER_CONTEXT_TYPE.DBO],
    // Передаем dbo3Guard, проверяющий контекст
    guards: [dbo3Guard]
  });

  // У юзера нет контекста БСС ДБО, рендерим заглушку
  if (!isAuthenticated) {
    return (
      <main>
        <h1>Доступ закрыт</h1>
        <p>{err.message}</p>
      </main>
    );
  }

  // Ок. Рендерим основной контент
  return <h1>Доступ есть</h1>
};

```

### Guards

В хук `useRouteGuards` в массив `guards` передаются специальные функции, проверяющие данные токена на соответствие конфигу.

Такая функция должна выбросить ошибку, если проверка данных токена не прошла, иначе молча завершиться.

Доступны 2 встроенных `guard`:

- segmentGuard - проверяет сегменты;
- dbo3Guard - проверяет контекст юзера.

Функции guards используются в функци `checkGuards`, который последовательно проверяет данные токена каждым `guards`.

### Использование кастомного guard

Чтобы написать свой guard, нужно сделать функцию, удовлетворяющую типу:

```ts
type ContextGuard<T extends IParsedToken = IParsedToken> = (tokenInfo: T, contextOptions: ContextGuardParameters) => void;

```

Например, напишем функцию проверяющую дату выдачи токена

```tsx
import dayjs from 'dayjs';
import { ContextGuard } from '@platform/services/client';

const expiredGuard: ContextGuard = tokenInfo => {
  // Берем дату из токена
  const dt = dayjs.unix(tokenInfo.exp);

  // Усли токен истек - выбрасываем ошибку
  if (dt.isBefore(dayjs())) {
    throw new Error('Токен истек')
  }
};

const PrivatePage: React.FC = () => {
  // проверяем не истек ли токен
  const [isTokenExpired, err] = useRouteGuards({ guards: [expiredGuard] });

  if (!isTokenExpired) {
    return (
      <main>
        <h1>Сессия истекла</h1>
        <p>{err.message}</p>
      </main>
    );
  }

  // Ок. Рендерим основной контент
  return <h1>Сессия активна</h1>
};

```

## Fallback-компоненты для SecureRoute

Есть стандартный компонент-шаблон страницы с информированием об отсутствии доступа - `GuardFallback`.

Ему можно указать заголовок header, описание ошибки description и кнопки на экране.

```tsx
import { GuardFallback } from '@platform/services/client';
import { BUTTON, IButtonAction } from '@platform/ui';

const actions: IButtonAction[] = [
  {
    label: 'Ок',
    name: 'login',
    onClick: () => {/* переход в логин */},
    buttonType: BUTTON.REGULAR,
  },
];

export const MyFallback: React.FC = () => {
  return (
    <GuardFallback
      header={'Ошибка доступа'}
      description={'Токен истек'}
      actions={actions}
    />
  )
};

```

### fallbackHoc

Для упрощения создания fallback-компонентов есть хок `fallbackHoc`.
Он позволяет прокинуть в `GuardFallback` его стандартные пропсы.

Перепишем `MyFallback` из примера выше с использованием этого HOC:

```tsx
import React from 'react'
import { GuardFallback, fallbackHoc } from '@platform/services/client';
import { BUTTON, IButtonAction } from '@platform/ui';

const actions: IButtonAction[] = [
  {
    label: 'Ок',
    name: 'login',
    onClick: () => {/* переход в логин */},
    buttonType: BUTTON.REGULAR,
  },
];

export const MyFallback = fallbackHoc({
  actions,
  header: 'Ошибка доступа',
  description: 'Токен истек',
});

```

Для ошибок контекста и сегментов есть готовые fallback:

- `DboFallback` - с кнопками "Войти" и "Назад"
- `SegmentFallback` - с кнопкой "Назад"

Компоненты отличаются заголовками и текстом сообщения.

А также есть `DefaultFallback` с сообщением общего вида и кнопкой "Назад".

```tsx
import { DboFallback, SegmentFallback, DefaultFallback } from '@platform/services/client';

const isDbo3user = (): boolean => { /* Проверка на контекст юзера БСС */ };
const isSegmentAvailable = (): boolean => { /* Проверка на сегменты юзера */ };
const hasAuthority = (): boolean => { /* Какая либо еще проверка */ };

export const routes = [
  <SecureRoute authErr={err} isAuthenticated={isDbo3user()} fallback={DboFallback} path='/dbo/stuff' />,
  <SecureRoute authErr={err} isAuthenticated={isSegmentAvailable()} fallback={SegmentFallback} path='/segment/stuff' />,
  <SecureRoute authErr={err} isAuthenticated={hasAuthority()} fallback={DefaultFallback} path='/private/stuff' />,
];

```

### chainFallbacks

Хук `useRouteGuards` может выбрасывать различные ошибки в зависимости от переданного ему конфига.
Если потребуется сделать несколько разных экранов в зависимости от вида ошибки, можно воспользоваться хоком `chainFallbacks`.

Он вернет компонент, который попытается сопоставить ошибку с одним из переданных ему fallback, или возьмет дефолтный fallback-компонент.

Сопоставление идет последовательно, до первого совпадения.

В хок нужно передать массив функций, удовлетовряющий интерфейсу:

```ts
type ErrorGuardHanlder = (error: Error) => FallBackComponentType | null | undefined

```

Например:

```ts
import { chainFallbacks } from '../fallbacks/chain-fallbacks';
import { isDbo3GuardError } from '../errors';
import { useRouteGuards } from '../use-client-guards';
import { USER_CONTEXT_TYPE } from '../../../interfaces';
import { dbo3Guard, segmentGuard } from '../route-guards';
import { SecureRoute } from '../../../components';
import { SegmentFallback, DboFallback } from '../fallbacks/built-in-fallbacks';

const Fallback = chainFallbacks([err => (isDbo3GuardError(err) ? DboFallback : null)], SegmentFallback);

```
