# Универсальная форма истории изменения заявок для банка

Действие getBankHistoryAction позволяет использовать универсальную ЭФ просмотра истории заявки сотрудником Банка

Использовать можно следующим образом, выделив в отдельное действие:
Пример:
```ts 
import {showHistoryAction} from 'actions/admin'

const SHOW_HISTORY: IActionWithAuth = {
    icon: Icons.History,
    label: locale.actions.showHistory,
    action: showHistoryAction,
    name: 'SHOW_HISTORY',
    authorities: [AUTHORITIES.VIEW_HISTORY],
};
```

```ts
// src/actions/admin/show-history.ts
import {action} from '@platform/services/admin'

/** Действие просмотра истории заявки. */
export const showHistoryAction: IActionConfig<IDocContext, BankHistoryDialogData> = action.getBankHistoryAction({
    getStatusLabel: x => STATUS_LABEL[x],
})
```

Либо можно вызвать действие напрямую в конфиге:

```ts 
import {action} from '@platform/services/admin'

const SHOW_HISTORY: IActionWithAuth = {
    icon: Icons.History,
    label: locale.actions.showHistory,
    action: action.getBankHistoryAction({ getStatusLabel: x => STATUS_LABEL[x] }),
    name: 'SHOW_HISTORY',
    authorities: [AUTHORITIES.VIEW_HISTORY],
};
```

Функция в качестве объекта принимает один обязательный и несколько необязательных параметров.

```ts
interface BankHistoryActionParams {
  /**
   * Функция, возвращающая заголовок модального окна.
   *
   * @param arg - Базовая сущность.
   */
  getHeader?(arg: IBaseEntity): string;

  /**
   * Функция, возвращающая локализованный статус.
   *
   * @param arg Сущность истории.
   */
  getStatusLabel(arg: string): string;

  /**
   * Функция, возвращающая краткое или полное наименование организации.
   *
   * @param arg - Базовая сущность.
   */
  getBankClientName?(arg: IBaseEntity): BankClientName;

  /**
   * Обработчик при нажатии на имя юзера.
   *
   * @param userId Id пользователя.
   */
  onUserNameClick?(userId: string): void;
}
```
Для корректной работы необходимо передать функцию, возвращающую локализованный статус в параметр **getStatusLabel**, он является обязательным.

**Необязательные параметры:**

**getHeader** - позволяет передать функцию, модифицирующую текст заголовка модального окна. 
В соответствии с постановкой во всех формах, кроме заявок на регистрацию КЭП/НЭП и заявок на генерацию НЭП, 
требуется выводить только номер документа, поэтому данный заголовок выводится по умолчанию.

```ts
// Стандартная функция для получения заголовка.
const getHeaderBase = (doc: IBaseEntity & { number: number }) => doc.number.toString();
```

```ts
// Пример использования.
// src/actions/admin/show-history.ts
import {action} from '@platform/services/admin'

const getHeader = (doc: IDocDTO) => `История изменений заявки № ${doc.number} от ${doc.date}`;

/** Действие просмотра истории заявки. */
export const showHistoryAction: IActionConfig<IDocContext, BankHistoryDialogData>  =action.getBankHistoryAction({
    getStatusLabel: x => STATUS_LABEL[x],
    getHeader,
})
```

**getBankClientName** - позволяет передать функцию, определяющую вывод наименования клиента для поля "Заявитель".
В большей части стримов значения для краткого или полного наименования берутеся из сущности bankClient, поэтому
по умолчанию вывод заявителя осуществляется оттуда.

```ts
// Стандартная функция для получения наименования клиента.
const getBankClientNameBase = (doc: IBaseEntity & { bankClient: IBankClient }): BankClientName => ({
    clientName: doc.bankClient?.shortName || doc.bankClient?.fullName,
});
```
```ts
// Пример использования.
// src/actions/admin/show-history.ts
import {action} from '@platform/services/admin'

const getHeader = (doc: IDocDTO) => `История изменений заявки № ${doc.number} от ${doc.date}`;
const getBankClientName = (doc: IDocDTO) => doc.bankClientInfo?.clientShortName ?? doc.bankClientInfo?.clientFullName

/** Действие просмотра истории заявки. */
export const showHistoryAction: IActionConfig<IDocContext, BankHistoryDialogData> = action.getBankHistoryAction({
    getStatusLabel: x => STATUS_LABEL[x],
    getHeader,
    getBankClientName
});
```

**onUserNameClick** - Обработчик при нажатии на имя юзера. Реализация для использования данного параметра пока не описана,
сделан "на будущее".

В сервис который определяется в контексте экзекьютора необходимо добавить метод для получения истории изменений:

```ts
/** Сервис-метод для получения истории документа. */
export interface IHistoryService {
    getHistory(id: string): Promise<IHistoryResponse[]>;
}
```

###Пример изменения действия запроса истории изменений на новую реализацию:

```ts
// Прежняя реализация, succeededHandler описывался на уровне стрима
// header и row также передавались на уровне стрима

// src/actions/admin/show-history.ts
import {HistoryDialog, dialog} from 'components'
import { HistoryHeader, HistoryRow } from 'components/show-history';
import {action} from '@platform/services/admin'

/** Действие просмотра истории заявки. */
export const showHistoryAction: IActionConfig<IAdminContext, IHistoryDialogData<IDoc>> = {
    ...action.history,
    succeededHandler: (_, [data]: [IHistoryDialogData<IDoc>]) =>
        dialog.show('history', HistoryDialog, {
            data,
            hasAdmin: true,
            clientName: data.document?.bankClient?.shortName ?? '',
            date: data.document?.date ?? '',
            header: React.createElement(HistoryHeader),
            num: data.document?.number?.toString() ?? '',
            onHistoryRowLinkClick: noop,
            row: HistoryRow,
        }),
};
```

```ts
// Новая реализация

// src/actions/admin/show-history.ts
import {action} from '@platform/services/admin'

/** Действие просмотра истории заявки. */
export const showHistoryAction: IActionConfig<IAdminContext, IHistoryDialogData<IDoc>> = action.getBankHistoryAction({
    getStatusLabel: x => STATUS_LABEL[x],
})
```
