# Использование библиотеки react-query

В платформу (v6.45.1) добавлена зависимость на библиотеку `react-query`, описание и документация - https://react-query.tanstack.com/

### Что оно делает?

Позволяет запрашивать данные с бека и управлять ими при помощи хука `useQuery` в компонентах, с помощью хука `useMutation` обновлять данные формы, также позволяет удобно обрабатывать ошибки, и с помощью хуков `useIsFetching` и `useIsMutating` управлять лоадером формы

### Пример использования

В стриме @eco-stubs, в форме профилей пользователей реализован пример использования, [ссылка на репозиторий](https://bitbucket.gboteam.ru/projects/ECO_FE/repos/eco-stubs/browse/src/pages/bss-user-profile/admin)

Основные моменты:

```tsx
export const TestForm: React.FC = () => {

  // управление лоадером
  const isFetching = useIsFetching([QUERY_NAMES.PROFILE]);
  const isMutating = useIsMutating({ mutationKey: MUTATION_NAMES.PROFILE });

  React.useEffect(() => {
    if (isFetching || isMutating) {
      showLoader();
    } else {
      hideLoader(true);
    }
  }, [isFetching, isMutating]);

  // загрузка данных и обработка ошибок
  const { data: profile, isError: profileError } = useQuery([QUERY_NAMES.PROFILE, id], () =>
    userProfileService.get(id).then(res => res.data)
  );
  const { data: user, isError: userError } = useQuery([QUERY_NAMES.PROFILE, id, QUERY_NAMES.USER], () =>
    userProfileService.getUser(id).then(res => res.data)
  );

  // получение данных из кеша
  const { data: customers = [], isError: customersError } = useQuery<Array<IOption<string>>>([
    QUERY_NAMES.PROFILE,
    id,
    QUERY_NAMES.CUSTOMERS,
  ]);
  const { isError: usersError } = useQuery([QUERY_NAMES.PROFILE, id, QUERY_NAMES.USERS]);

  const isError = profileError || userError || customersError || usersError;

  React.useEffect(() => {
    if (isError) {
      dialog.showAlert(locale.dialogs.errors.entityNotFound, {
        header: locale.statuses.error,
        onClose: goHome,
      });
    }
  }, [isError, goHome]);

  // изменение данных
  const { mutate } = useMutation<unknown, unknown, IBssUserProfileFormData>(
    async values => {
      const requestMethod = values.id ? userProfileService.edit : userProfileService.add;
      const res = await requestMethod(toDto(values, customers));

      return userProfileService.linkUser(res?.data.id, values.user);
    },
    {
      mutationKey: MUTATION_NAMES.PROFILE,
      onSuccess: () => {
        dialog.showSuccessAlert(locale.action.userProfile.save.success, { onClose: goHome });
      },
      onError: () => dialog.showError(locale.action.userProfile.save.error),
    }
  );

  const onSubmit = React.useCallback(values => mutate(values), [mutate]);

  return (
    <Form
      validateOnBlur
      initialValues={formData}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Content />
        </form>
      )}
      validate={validate}
      onSubmit={onSubmit}
    />
  );
};

export const Content: React.FC = () => {
  // загрузка данных
  const { data: customers = [] } = useQuery([QUERY_NAMES.PROFILE, id, QUERY_NAMES.CUSTOMERS], () =>
    customerService.getAll({ pageSize: 1000, offset: 0 }).then(res => getCustomerOptions(res.data))
  );
  const { data: users = [] } = useQuery([QUERY_NAMES.PROFILE, id, QUERY_NAMES.USERS], () =>
    userService.getAll({ pageSize: 1000, offset: 0 }).then(res => res.data.map(x => ({ value: x.id, label: x.login })))
  );

  return (
    <>
      <Label text={locale.form.customers}>
        <Fields.MultiSelect name={FIELDS.CUSTOMERS} options={customers} />
      </Label>
      <Gap.XL />

      <Label text={locale.form.user}>
        <Fields.Select name={FIELDS.USER} options={users} />
      </Label>
      <Gap.XL />
    </>
  );
};
```

