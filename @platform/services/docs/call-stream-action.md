# Метод для вызова методов из других стримов

Метод позволяет вызвать метод другого стрима

## Вызов метода другого стрима

Для вызова методго другого стрима используется функция `callStreamAction` с параметрами
1. stream - Наименование стрима
1. method - Метод стрима
1. params - Параметры метода

```tsx
import { callStreamAction } from '@platfrom/services'

callStreamAction('redirect', 'redirectToEtp')
```

