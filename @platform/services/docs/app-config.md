# Конфиг приложения

В конфиге могут храниться настраиваемые параметры системы:

- конфиг бокового меню;
- конфигурация для работы с криптоплагинами;
- адреса айфреймов БСС;
- [другие настраиваемые параметры](https://confluence.gboteam.ru/pages/viewpage.action?pageId=328459).

Эти параметры хранятся в виде `json`, доступном через config-service.

Конфиг доступен по относительному урлу:

- клиентская часть - `/api/config-service/config/configuration/pr/lb/config.json`;
- банковская часть - `/api/config-service/config/configuration/pr/lb/bank-config.json`.

Аналогичные урлы доступны в приложении логина.

## Использование конфига

Веб-приложение обернуто в компонент, предоставляющий React Context с конфигом.

```tsx
// пример клиентской части.
ReactDOM.render(
  <ThemeProvider>
    <FatalErrorBoundary>
      <AppConfig url="/api/config-service/config/configuration/pr/lb/config.json">
        <App />
      </AppConfig>
    </FatalErrorBoundary>
  </ThemeProvider>,
  document.getElementById('app')
);

// пример банковской части с использованием автозагрузки конфига.
ReactDOM.render(
  <ThemeProvider>
    <FatalErrorBoundary>
      <AppConfig autoload url="/api/config-service/config/configuration/pr/lb/bank-config.json">
        <App />
      </AppConfig>
    </FatalErrorBoundary>
  </ThemeProvider>,
  document.getElementById('app')
);
```

Затем из любого места приложения до контекста можно достучаться, используя хук `useAppConfig`.

```tsx
import { ExternalModule, MainLayout, useAppConfig  } from '@platform/services/client';

// Создаем страницу.
const MyPage: React.FC = () => (
  <MainLayout>
    <MyPageContent />
  </MainLayout>
);

const MyPageContent: React.FC = () => {
  // обращаемся к конфигу.
  const { config } = useAppConfig();

  return (
    <ExternalModule name="iframe-bss" src={config.iframe_url_from_config}>
      {(content) => <div id="iframe-content">{content}<div>}
    </ExternalModule>
  );
};
```

> В клиентской части конфиг грузится при первом рендере MainLayout.
> Поэтому лучше обращаться к конфигу в одном из дочерних компонентов, а не компонентов, использующих MainLayout.