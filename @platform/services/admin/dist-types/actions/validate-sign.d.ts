import type { IBaseEntity, IActionContext, IActionConfig } from '../interfaces';
export interface IValidateSignServive {
    validateSign(id: string): Promise<IBaseEntity>;
}
export declare const validateSign: IActionConfig<IActionContext<IValidateSignServive>, IBaseEntity>;
