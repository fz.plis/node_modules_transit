import type { IActionContext, IBaseEntity, IActionConfig, ISignService, ICertificateUserContext, IUserDataContext } from '../interfaces';
/**
 * Новый экшн для подписи. Использует под капотом компонент универсальной подписи.
 */
export declare const ecoSign: IActionConfig<IActionContext<ISignService> & ICertificateUserContext & IUserDataContext, IBaseEntity>;
