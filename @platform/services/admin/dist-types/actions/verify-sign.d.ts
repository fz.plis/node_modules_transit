import type { IServerResp, IDigest, IActionContext, IBaseEntity, ISignVerify, IActionConfig, ICertificateUserContext } from '../interfaces';
import type { IVersifySignResult } from '../views';
export interface ISignRequestData {
    certificateId: string;
    digest: string;
    documentId: string;
    signTypeId: string;
    signature: string;
}
export interface ISignService {
    digest(id: string): Promise<IServerResp<IDigest>>;
    sign(data: ISignRequestData): Promise<IBaseEntity>;
    verifySign(id: string): Promise<IServerResp<ISignVerify>>;
}
export declare const verifySign: IActionConfig<IActionContext<ISignService> & ICertificateUserContext, IVersifySignResult<any>>;
