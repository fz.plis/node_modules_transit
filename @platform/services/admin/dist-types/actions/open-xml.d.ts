import type { IActionConfig, IBaseContext, IXmlContent } from '../interfaces';
/**
 * Открытие XML-файла в новой вкладке браузера.
 */
export declare const openXml: <TContext extends IBaseContext, TDoc extends IXmlContent>(xmlBodyField?: string, filePrefix?: string) => import("@platform/core").IActionConfig<TContext, TDoc>;
