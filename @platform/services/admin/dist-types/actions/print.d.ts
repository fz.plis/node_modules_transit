import type { IActionConfig } from '@platform/core';
import type { IOption } from '@platform/ui';
import type { IActionContext, IExportActionSettings, IPrintService, IPrintResponseDoc } from '../interfaces';
export declare const printDoc: {
    withDialog: <TContext extends IActionContext<IPrintService>>(exportOptions: IOption[], settingsLocale?: any) => IActionConfig<TContext, IPrintResponseDoc>;
    withoutDialog: <TContext_1 extends IActionContext<IPrintService>>(externalSettings?: IExportActionSettings | undefined) => IActionConfig<TContext_1, IPrintResponseDoc>;
};
