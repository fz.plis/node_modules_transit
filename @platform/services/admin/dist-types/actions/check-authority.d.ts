import type { IActionConfig, IActionContext, IAuthorityResult, ICertificateUserContext, ISignService, IUserDataContext } from '../interfaces';
export declare const checkAuthority: IActionConfig<IActionContext<ISignService> & ICertificateUserContext & IUserDataContext, IAuthorityResult[]>;
