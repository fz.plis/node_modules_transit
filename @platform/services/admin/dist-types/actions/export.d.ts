import type { IActionConfig } from '@platform/core';
import type { IOption } from '@platform/ui';
import type { IActionContext, IExportActionSettings, IExportService } from '../interfaces';
export interface IExportDialogOptions {
    options: IOption[];
    locale?: {
        header: string;
        text: string;
    };
    value?: any;
}
export declare const exportDoc: {
    withDialog: <TContext extends IActionContext<IExportService>>({ options, locale, value, }: IExportDialogOptions) => IActionConfig<TContext, any>;
    withoutDialog: <TContext_1 extends IActionContext<IExportService>>(userSettings?: IExportActionSettings | undefined) => IActionConfig<TContext_1, any>;
};
