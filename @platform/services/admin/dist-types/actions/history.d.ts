import type { IActionContext, IHistoryResponse, IHistoryDialogData, IActionConfig } from '../interfaces';
export interface IHistoryService {
    getHistory(id: string): Promise<IHistoryResponse[]>;
}
export declare const history: IActionConfig<IActionContext<IHistoryService>, IHistoryDialogData<any>>;
