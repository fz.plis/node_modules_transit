import type { IActionConfig, IBaseContext, IXmlContent } from '../interfaces';
/**
 * Скачивание XML-файла.
 */
export declare const downloadXml: <TContext extends IBaseContext, TDoc extends IXmlContent>(xmlBodyField?: string, filePrefix?: string) => import("@platform/core").IActionConfig<TContext, TDoc>;
