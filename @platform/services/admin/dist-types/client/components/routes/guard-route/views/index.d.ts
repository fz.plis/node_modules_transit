import React from 'react';
import type { ERROR_TYPES } from '../constants';
/**
 * Свойства компонента недоступности роута.
 */
interface IRouteErrorProps {
    /**
     * Тип ошибки.
     */
    type: ERROR_TYPES;
}
/**
 * Компонент недоступности роута.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=6363519
 */
export declare const RouteError: React.FC<IRouteErrorProps>;
export {};
