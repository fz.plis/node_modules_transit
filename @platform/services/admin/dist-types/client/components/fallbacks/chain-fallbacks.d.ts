import type { FallBackComponentType } from '../../../components';
/**
 * Функция, определяющая компонент по описанию ошибки.
 */
export declare type ErrorGuardHanlder = (error: Error) => FallBackComponentType | null | undefined;
/**
 * Возвращает функцию, выбирающую компонент в зависимости от переданной ошибки.
 * Перебор ведется до первого успешного результата.
 * Если он не дал результатов, то выдается `def`.
 *
 * @param fallbacks Массив функций перебора ошибок.
 * @param def Компонент Fallback, используемый если перебор не дал результатов.
 *
 * @example
 * ```
 * // массив функций типа ErrorGuardHanlder
 * const fallbacks = [...];
 * // создаем функцию перебора
 * const interator = findFallbackByErr(fallbacks, SomeComponent);
 * // Выбранной компонент для рендера ошибки
 * const Comp = iterator(err);
 * ```
 */
export declare const findFallbackByErr: (fallbacks: ErrorGuardHanlder[], def: FallBackComponentType) => (err: Error) => FallBackComponentType;
/**
 * Хок, возвращающий компонент, автоматически выбирающий какой компонент
 * использовать для отображения ошибки.
 *
 * @param fallbacks Массив функций перебора ошибки.
 * @param def Компонент, используемый если перебор не дал результатов.
 *
 * @example
 * ```
 * // массив функций типа ErrorGuardHanlder
 * const fallbacks = [...];
 * // компонент для рендера ошибки
 * const MyFallback = chainFallbacks(fallbacks)
 * // использование
 * <MyFallback error={someErr} />
 * ```
 */
export declare const chainFallbacks: (fallbacks: ErrorGuardHanlder[], def?: FallBackComponentType) => FallBackComponentType;
