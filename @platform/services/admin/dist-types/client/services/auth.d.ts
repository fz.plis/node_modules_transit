import type { ILoginPasswordRequest, ILoginResponse, ILoginCertificateRequest, ILoginOtpTotpRequest, IApproval, IClientProfile, IMfaCertificateResponse, IAuthConfiguration, IAuthResponse, IMfaChains, IChangePasswordDboResponse, IChangePasswordDboFormResponse, ICodeMfaChallenge } from '../../interfaces';
import { AUTH_RESPONSE_TYPE } from '../../interfaces';
export declare const authService: {
    getAuthConfiguration: () => Promise<IAuthConfiguration>;
    login: {
        byPassword: (data: ILoginPasswordRequest, redirectUri?: string | undefined) => Promise<ILoginResponse>;
        byCertificate: (data: ILoginCertificateRequest, redirectUri?: string | undefined) => Promise<ILoginResponse>;
        byOtp: (data: ILoginOtpTotpRequest, redirectUri?: string | undefined) => Promise<ILoginResponse>;
        byTotp: (data: ILoginOtpTotpRequest, redirectUri?: string | undefined) => Promise<ILoginResponse>;
        changePassword: (remoteId: number, data: {
            newPassword: string;
            newPasswordConfirm: string;
            oldPassword: string;
        }) => Promise<IChangePasswordDboResponse>;
        getChangePasswordFormState: (remoteId: number) => Promise<IChangePasswordDboFormResponse>;
    };
    certificateChallenge: (token: string) => Promise<IMfaCertificateResponse>;
    otpChallenge: (token: string) => Promise<ICodeMfaChallenge>;
    totpChallenge: (token: string) => Promise<ICodeMfaChallenge>;
    listChallenge: (token: string) => Promise<IMfaChains[]>;
    selectChallenge: (token: string, chainName: string) => Promise<ILoginResponse>;
    approvals: {
        getById: (approveId: string) => Promise<IApproval>;
        approve: (approveId: string) => Promise<IApproval>;
        deny: (approveId: string) => Promise<any>;
    };
    profile: () => Promise<IClientProfile>;
    authorize: () => Promise<IAuthResponse>;
    logout: () => Promise<any>;
};
export declare const AUTH_REQUEST_DATA: {
    client_id: string;
    response_type: AUTH_RESPONSE_TYPE;
};
