import type { CONSENT_TYPE, SYSTEM_TYPE } from '../../constants';
import type { ICompleteRegisterResponse, IUpdateUserResponse, IUserInfo, IRegisterRequestData, IRegisterResponse, IRestorePasswordRequest, IRestorePasswordResponse, ICompleteRestorePasswordRequest, ICompleteRestorePasswordResponse, IChangePasswordResponse, IChangePhoneNumberRequest, IChangeEmailRequest, IMfaChains, IConsentRequest, IUserConsent, IGenerateCaptchaResponse, IUserAuthoritiesResponse, ITokenResponse } from '../../interfaces';
export declare const uaaService: {
    register: (data: IRegisterRequestData & {
        addConsentRq: IConsentRequest;
    }) => Promise<IRegisterResponse>;
    completeRegister: (requestId: string, smsCode: string) => Promise<ICompleteRegisterResponse>;
    generate: () => Promise<IGenerateCaptchaResponse>;
    restorePassword: (data: IRestorePasswordRequest) => Promise<IRestorePasswordResponse>;
    completeRestorePassword: (data: ICompleteRestorePasswordRequest) => Promise<ICompleteRestorePasswordResponse>;
    changePassword: (oldPassword: string, newPassword: string) => Promise<IChangePasswordResponse>;
    changePasswordDbo: (data: {
        newPassword: string;
        newPasswordConfirm: string;
        oldPassword: string;
    }) => Promise<number>;
    changePhoneNumber: (data: IChangePhoneNumberRequest) => Promise<ITokenResponse>;
    changeEmail: (data: IChangeEmailRequest) => Promise<ITokenResponse>;
    updateUser: (data: IUserInfo) => Promise<IUpdateUserResponse>;
    completePhoneChange: (requestId: string, smsCode: string) => Promise<ICompleteRegisterResponse>;
    completeEmailChange: (requestId: string, emailCode: string) => Promise<ICompleteRegisterResponse>;
    hideNotification: () => Promise<boolean>;
    renewConfirm: (confirmationId: string, disableTokenHeader?: boolean) => Promise<ITokenResponse>;
    getAvailableMfaChain: () => Promise<IMfaChains[]>;
    getDefaultMfaChain: () => Promise<IMfaChains[]>;
    changeMfaChain: (mfaChainName: string) => Promise<IMfaChains>;
    /**
     * Запрос получения токена внешней системы.
     *
     * @param systemType Тип внешней системы.
     * @param redirectUri URL адрес, на который будет перенаправлен пользователь в случае успешной авторизации.
     * @returns URL адрес для перенаправления пользователя во внешнюю систему.
     */
    getExternalToken: (systemType: SYSTEM_TYPE, redirectUri?: string | undefined) => Promise<string>;
    addConsent: (type: CONSENT_TYPE, version: number) => Promise<{
        consents: IUserConsent[];
    }>;
    hasConsent: (type: CONSENT_TYPE, version: string) => Promise<any>;
    findByUid: (id: string) => Promise<any>;
    getAuthorities: () => Promise<IUserAuthoritiesResponse>;
};
