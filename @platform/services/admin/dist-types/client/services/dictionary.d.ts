import type { IOkopf, IOkfs, ICurrency, IActivityType, ICountry, IAccountType, IGozContract, IBankClient, IMetaData, ICollectionResponse } from '../../interfaces';
export declare const dictionaryService: {
    okopf: {
        get: (id: string) => Promise<IOkopf>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<IOkopf>>;
        searchByName: <T extends keyof IOkopf>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<ICollectionResponse<IOkopf>>;
    };
    okfs: {
        get: (id: string) => Promise<IOkfs>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<IOkfs>>;
        searchByName: <T_1 extends keyof IOkfs>(params: import("../../interfaces").ISearchByName, fieldName: T_1) => Promise<ICollectionResponse<IOkfs>>;
    };
    currency: {
        get: (id: string) => Promise<ICurrency>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<ICurrency>>;
        searchByName: <T_2 extends keyof ICurrency>(params: import("../../interfaces").ISearchByName, fieldName: T_2) => Promise<ICollectionResponse<ICurrency>>;
    };
    activityType: {
        get: (id: string) => Promise<IActivityType>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<IActivityType>>;
        searchByName: <T_3 extends keyof IActivityType>(params: import("../../interfaces").ISearchByName, fieldName: T_3) => Promise<ICollectionResponse<IActivityType>>;
    };
    country: {
        get: (id: string) => Promise<ICountry>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<ICountry>>;
        searchByName: <T_4 extends keyof ICountry>(params: import("../../interfaces").ISearchByName, fieldName: T_4) => Promise<ICollectionResponse<ICountry>>;
    };
    accountType: {
        get: (id: string) => Promise<IAccountType>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<IAccountType>>;
        searchByName: <T_5 extends keyof IAccountType>(params: import("../../interfaces").ISearchByName, fieldName: T_5) => Promise<ICollectionResponse<IAccountType>>;
    };
    gk: {
        get: (id: string) => Promise<IGozContract>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<IGozContract>>;
        searchByName: <T_6 extends keyof IGozContract>(params: import("../../interfaces").ISearchByName, fieldName: T_6) => Promise<ICollectionResponse<IGozContract>>;
    };
    bankClient: {
        getUserOrganizations: () => Promise<IBankClient[]>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<IBankClient>>;
        get: (id: string) => Promise<IBankClient>;
        searchByName: <T_7 extends keyof IBankClient>(params: import("../../interfaces").ISearchByName, fieldName: T_7) => Promise<ICollectionResponse<IBankClient>>;
        searchByTextQuery: (params: import("../../interfaces").IClientSearch) => Promise<ICollectionResponse<IBankClient>>;
        searchOgrn: (params: import("../../interfaces").ISearchByName) => Promise<ICollectionResponse<IBankClient>>;
        searchRegNumber: (params: import("../../interfaces").ISearchByName) => Promise<ICollectionResponse<IBankClient>>;
    };
    clientOfficial: {
        getUserOfficials: (userId: string) => Promise<import("../../interfaces").IClientOfficial[]>;
    };
    branch: {
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<import("../../interfaces").IBranch>>;
        get: (id: string) => Promise<import("../../interfaces").IBranch>;
        searchByName: <T_8 extends keyof import("../../interfaces").IBranch>(params: import("../../interfaces").ISearchByName, fieldName: T_8) => Promise<ICollectionResponse<import("../../interfaces").IBranch>>;
        localityList(): Promise<string[]>;
        bicPage(metaData: IMetaData): Promise<ICollectionResponse<string>>;
    };
    branchV2: {
        bicPage: (metaData: IMetaData) => Promise<ICollectionResponse<string>>;
        getList: (metaData: IMetaData) => Promise<ICollectionResponse<import("../../interfaces").IBranchV2>>;
        get: (id: string) => Promise<import("../../interfaces").IBranchV2>;
        getCounter: (metaData: IMetaData) => Promise<any>;
        searchByName: <T_9 extends keyof import("../../interfaces").IBranchV2>(params: import("../../interfaces").ISearchByName, fieldName: T_9) => Promise<ICollectionResponse<import("../../interfaces").IBranchV2>>;
        localityList(): Promise<string[]>;
    };
    fias: {
        search: (query: string, count?: number) => Promise<import("../../interfaces").IFiasResult[]>;
    };
    consent: {
        getLastVersion: (type: import("..").CONSENT_TYPE) => Promise<any>;
        getContent: (type: import("..").CONSENT_TYPE) => Promise<any>;
    };
    cityVisitingManager: {
        findDocPackZone(branchId: string): Promise<import("../../interfaces").IServerResp<import("../../interfaces").CityVisitingManagerDto, import("../../interfaces").ERROR>>;
    };
};
