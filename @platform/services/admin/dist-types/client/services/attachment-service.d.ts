export declare const attachmentService: {
    getStatus: (attachmentId: string, onCancel?: ((cancel: import("axios").Canceler) => void) | undefined) => Promise<any>;
    upload: (file: File, { onUploadProgress, onCancel }?: import("..").IFileUploadOptions) => Promise<import("..").IAttachment>;
    checkUploadProgress: (attachmentId: string, options?: import("..").ICheckUploadOptions) => Promise<import("..").IAttachment>;
    download: (attachmentId: string, accessToken: string) => Promise<import("..").IDownloadedAttachment>;
    downloadBase64: (attachmentId: string, accessToken?: string | undefined) => Promise<import("..").IDownloadedAttachment>;
    downloadOneTimeData: (attachmentId: string, accessToken: string) => Promise<import("..").IDownloadedAttachment>;
};
