export declare const accountService: {
    getList: (metaData: import("..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("..").IAccount>>;
    get: (id: string) => Promise<import("..").IAccount>;
    getCounter: ({ category, ...metaData }: import("..").IMetaData) => Promise<any>;
    searchByName: <T extends keyof import("..").IAccount>(params: import("..").ISearchByName, fieldName: T) => Promise<import("@platform/core").ICollectionResponse<import("..").IAccount>>;
    list: (ids: string[]) => Promise<import("..").IAccount[]>;
    listWithBalance: (ids: string[]) => Promise<import("..").IAccountV2[]>;
};
