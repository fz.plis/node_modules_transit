import type { IBankClient, IParsedToken, IUserData } from '../../interfaces';
import { UserType, USER_CONTEXT_TYPE, USER_ROLE } from '../../interfaces';
export declare const user: import("@platform/tools/istore").Unit<import("../../domains/user").IUserDomainState, {
    setToken(token: string, authorities: string[]): void;
    /**
     * Пока делаем проверку на юзера dbo3 через этот метод, ждем аналитики.
     */
    isDbo3User(): boolean;
    getDefaultUserCertId(): string | undefined;
    load: ({ userData, roles, userOrganizations, }: {
        userData: Partial<IUserData>;
        roles: USER_ROLE[];
        userOrganizations: IBankClient[];
    }) => void;
    asyncProfile: () => Promise<IParsedToken>;
    showNotConfirmedDataDialog: (onClose?: () => void) => void;
    updateUserPhone: (onClose?: () => void, disableResultStep?: boolean | undefined) => void;
    updateUserEmail: (onClose?: () => void) => void;
    loadOrganizations: () => Promise<void>;
    checkNotConfirmedData: () => boolean;
    getAvailableActionsWithCategory: (actions: Record<string, import("../../interfaces").IActionWithAuth[]>) => import("../../interfaces").IButtonConfig<any, any>;
    logout: () => void;
    getAvailableActions: (actions: import("../../interfaces").IActionWithAuth[]) => import("../../interfaces").IActionWebInfo<any, any>[];
    setUserType: (userType: UserType) => void;
    updateData: (data: Partial<IUserData>) => void;
    setRoles: (roles: USER_ROLE[]) => void;
    isAvailableFor: (...roles: USER_ROLE[]) => boolean;
    hasAuthority(...userAuthorities: string[]): boolean;
    hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean;
}>;
export declare const userOrganizations: import("@platform/tools/istore").Unit<IBankClient[], {
    toggleItem: (item: IBankClient) => void;
    set: (val: IBankClient[]) => void;
    setItem: (index: number, item: IBankClient) => void;
    add: (item: IBankClient | IBankClient[]) => void;
    remove: (item: IBankClient | IBankClient[]) => void;
    removeByIndex: (index: number) => void;
    mergeToItem: (index: number, obj: Partial<IBankClient>) => void;
}>;
export declare const selectedOrganizationId: import("@platform/tools/istore").Unit<string, {
    set: (val: string) => void;
}>;
/**
 * @deprecated C 1 февраля Используйте useUser, useAuth вместо.
 */
export declare const mainDomain: import("@platform/tools/istore").Domain<{
    user: import("@platform/tools/istore").Unit<import("../../domains/user").IUserDomainState, {
        setToken(token: string, authorities: string[]): void;
        /**
         * Пока делаем проверку на юзера dbo3 через этот метод, ждем аналитики.
         */
        isDbo3User(): boolean;
        getDefaultUserCertId(): string | undefined;
        load: ({ userData, roles, userOrganizations, }: {
            userData: Partial<IUserData>;
            roles: USER_ROLE[];
            userOrganizations: IBankClient[];
        }) => void;
        asyncProfile: () => Promise<IParsedToken>;
        showNotConfirmedDataDialog: (onClose?: () => void) => void;
        updateUserPhone: (onClose?: () => void, disableResultStep?: boolean | undefined) => void;
        updateUserEmail: (onClose?: () => void) => void;
        loadOrganizations: () => Promise<void>;
        checkNotConfirmedData: () => boolean;
        getAvailableActionsWithCategory: (actions: Record<string, import("../../interfaces").IActionWithAuth[]>) => import("../../interfaces").IButtonConfig<any, any>;
        logout: () => void;
        getAvailableActions: (actions: import("../../interfaces").IActionWithAuth[]) => import("../../interfaces").IActionWebInfo<any, any>[];
        setUserType: (userType: UserType) => void;
        updateData: (data: Partial<IUserData>) => void;
        setRoles: (roles: USER_ROLE[]) => void;
        isAvailableFor: (...roles: USER_ROLE[]) => boolean;
        hasAuthority(...userAuthorities: string[]): boolean;
        hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean;
    }>;
    userOrganizations: import("@platform/tools/istore").Unit<IBankClient[], {
        toggleItem: (item: IBankClient) => void;
        set: (val: IBankClient[]) => void;
        setItem: (index: number, item: IBankClient) => void;
        add: (item: IBankClient | IBankClient[]) => void;
        remove: (item: IBankClient | IBankClient[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<IBankClient>) => void;
    }>;
    selectedOrganizationId: import("@platform/tools/istore").Unit<string, {
        set: (val: string) => void;
    }>;
    notifications: import("@platform/tools/istore").Domain<{
        visible: import("@platform/tools/istore").Unit<import("../../domains/common-domain").INotification[], {
            toggleItem: (item: import("../../domains/common-domain").INotification) => void;
            set: (val: import("../../domains/common-domain").INotification[]) => void;
            setItem: (index: number, item: import("../../domains/common-domain").INotification) => void;
            add: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            remove: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<import("../../domains/common-domain").INotification>) => void;
        }>;
        archive: import("@platform/tools/istore").Unit<import("../../domains/common-domain").INotification[], {
            toggleItem: (item: import("../../domains/common-domain").INotification) => void;
            set: (val: import("../../domains/common-domain").INotification[]) => void;
            setItem: (index: number, item: import("../../domains/common-domain").INotification) => void;
            add: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            remove: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<import("../../domains/common-domain").INotification>) => void;
        }>;
    }, {
        add: (data: Omit<import("../../domains/common-domain").INotification, "date">) => void;
        close: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
    }>;
    authConfig: import("@platform/tools/istore").Unit<import("../../interfaces").IAuthConfiguration | undefined, {
        set: (val: import("../../interfaces").IAuthConfiguration | undefined) => void;
    }>;
    hasFatalError: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
    notificationDomain: import("@platform/tools/istore").Unit<import("../../interfaces").INotificationDomain, {
        getCount: () => void;
        canPostpone: () => boolean;
        postponeNotifications: () => Promise<import("../../interfaces").IUserNotification[]>;
        postponeNotification: (id: string) => Promise<import("../../interfaces").IUserNotification>;
        getImportantNotification: (importantCount?: number) => Promise<void>;
        getPostponedNotifications: () => import("../../interfaces").IUserNotification[];
        merge: (updates: Partial<import("../../interfaces").INotificationDomain>) => void;
        setNotificationCount: (count: number) => void;
        setImportantNotificationMessage: (message: import("../../interfaces").IRenderNotification) => void;
        setImportantNotificationUrl: (url: string) => void;
        setShowImportantNotification: (isShow: boolean) => void;
        setShowImportantNotificationCount: (importantCount: number) => void;
        setImportantNotifications: (notifications: import("../../interfaces").IUserNotification[]) => void;
    }>;
}, import("@platform/tools/istore").IOnlyMethods<{
    user: import("@platform/tools/istore").Unit<import("../../domains/user").IUserDomainState, {
        setToken(token: string, authorities: string[]): void;
        /**
         * Пока делаем проверку на юзера dbo3 через этот метод, ждем аналитики.
         */
        isDbo3User(): boolean;
        getDefaultUserCertId(): string | undefined;
        load: ({ userData, roles, userOrganizations, }: {
            userData: Partial<IUserData>;
            roles: USER_ROLE[];
            userOrganizations: IBankClient[];
        }) => void;
        asyncProfile: () => Promise<IParsedToken>;
        showNotConfirmedDataDialog: (onClose?: () => void) => void;
        updateUserPhone: (onClose?: () => void, disableResultStep?: boolean | undefined) => void;
        updateUserEmail: (onClose?: () => void) => void;
        loadOrganizations: () => Promise<void>;
        checkNotConfirmedData: () => boolean;
        getAvailableActionsWithCategory: (actions: Record<string, import("../../interfaces").IActionWithAuth[]>) => import("../../interfaces").IButtonConfig<any, any>;
        logout: () => void;
        getAvailableActions: (actions: import("../../interfaces").IActionWithAuth[]) => import("../../interfaces").IActionWebInfo<any, any>[];
        setUserType: (userType: UserType) => void;
        updateData: (data: Partial<IUserData>) => void;
        setRoles: (roles: USER_ROLE[]) => void;
        isAvailableFor: (...roles: USER_ROLE[]) => boolean;
        hasAuthority(...userAuthorities: string[]): boolean;
        hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean;
    }>;
    userOrganizations: import("@platform/tools/istore").Unit<IBankClient[], {
        toggleItem: (item: IBankClient) => void;
        set: (val: IBankClient[]) => void;
        setItem: (index: number, item: IBankClient) => void;
        add: (item: IBankClient | IBankClient[]) => void;
        remove: (item: IBankClient | IBankClient[]) => void;
        removeByIndex: (index: number) => void;
        mergeToItem: (index: number, obj: Partial<IBankClient>) => void;
    }>;
    selectedOrganizationId: import("@platform/tools/istore").Unit<string, {
        set: (val: string) => void;
    }>;
    notifications: import("@platform/tools/istore").Domain<{
        visible: import("@platform/tools/istore").Unit<import("../../domains/common-domain").INotification[], {
            toggleItem: (item: import("../../domains/common-domain").INotification) => void;
            set: (val: import("../../domains/common-domain").INotification[]) => void;
            setItem: (index: number, item: import("../../domains/common-domain").INotification) => void;
            add: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            remove: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<import("../../domains/common-domain").INotification>) => void;
        }>;
        archive: import("@platform/tools/istore").Unit<import("../../domains/common-domain").INotification[], {
            toggleItem: (item: import("../../domains/common-domain").INotification) => void;
            set: (val: import("../../domains/common-domain").INotification[]) => void;
            setItem: (index: number, item: import("../../domains/common-domain").INotification) => void;
            add: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            remove: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
            removeByIndex: (index: number) => void;
            mergeToItem: (index: number, obj: Partial<import("../../domains/common-domain").INotification>) => void;
        }>;
    }, {
        add: (data: Omit<import("../../domains/common-domain").INotification, "date">) => void;
        close: (item: import("../../domains/common-domain").INotification | import("../../domains/common-domain").INotification[]) => void;
    }>;
    authConfig: import("@platform/tools/istore").Unit<import("../../interfaces").IAuthConfiguration | undefined, {
        set: (val: import("../../interfaces").IAuthConfiguration | undefined) => void;
    }>;
    hasFatalError: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
    notificationDomain: import("@platform/tools/istore").Unit<import("../../interfaces").INotificationDomain, {
        getCount: () => void;
        canPostpone: () => boolean;
        postponeNotifications: () => Promise<import("../../interfaces").IUserNotification[]>;
        postponeNotification: (id: string) => Promise<import("../../interfaces").IUserNotification>;
        getImportantNotification: (importantCount?: number) => Promise<void>;
        getPostponedNotifications: () => import("../../interfaces").IUserNotification[];
        merge: (updates: Partial<import("../../interfaces").INotificationDomain>) => void;
        setNotificationCount: (count: number) => void;
        setImportantNotificationMessage: (message: import("../../interfaces").IRenderNotification) => void;
        setImportantNotificationUrl: (url: string) => void;
        setShowImportantNotification: (isShow: boolean) => void;
        setShowImportantNotificationCount: (importantCount: number) => void;
        setImportantNotifications: (notifications: import("../../interfaces").IUserNotification[]) => void;
    }>;
}>>;
