import type { BrowserHistoryBuildOptions } from 'history';
declare type UserConfirmationFunc = BrowserHistoryBuildOptions['getUserConfirmation'];
export declare const getUserConfirmation: UserConfirmationFunc;
export declare const history: import("history").History<unknown>;
export declare const renderApp: () => void;
export {};
