import type { AxiosRequestConfig } from 'axios';
import type { ICertFilter, IUserOrgCertificate } from '../../interfaces';
import type { GenerateKeyParams, GenerateKeyResult, IError, IGPBCryptoModuleCertinfo, IGPBCryptoModuleCheck, IGPBCryptoModuleCheckResponse, IGPBCryptoModuleInitkey, IGPBCryptoModuleResultInitKey, IGPBCryptoModuleSign, IGPBCryptoModuleSignResult, IGPCryptoModuleBResultSystemInfo, IKeyContainerSFT } from './crypto-module-interface';
import { CRYPTO_PLUGIN_TYPE } from './crypto-module-interface';
export interface ICryptoModuleResponse {
    data?: any;
    error: IError;
}
/**
 * Выбор используемого плагина для работы с подписью.
 *
 * При работе с плагином, проверяется установленная у пользователя версия плагина.
 * Если версии плагина пользователя, меньше, чем минимальная версия, которая используется на ЭКО,
 * то пользователю выводится ошибка.
 *
 * @param plugin Плагин пользователя.
 */
export declare const setCryptoPlugin: (plugin?: CRYPTO_PLUGIN_TYPE) => void;
/**
 * Отдает тип криптоплагина.
 */
export declare const getCryptoPluginType: () => CRYPTO_PLUGIN_TYPE;
/**
 * Отдает URL для скачивания криптоплагина.
 */
export declare const getCryptoPluginApp: () => string;
export declare const request: (params: AxiosRequestConfig) => Promise<any>;
export declare const cryptoModuleService: {
    serviceVersion: () => Promise<string>;
    version: () => Promise<string>;
    getInitkey: (data: IGPBCryptoModuleInitkey) => Promise<IGPBCryptoModuleResultInitKey>;
    sign: (data: IGPBCryptoModuleSign) => Promise<IGPBCryptoModuleSignResult[]>;
    check: (data: IGPBCryptoModuleCheck) => Promise<IGPBCryptoModuleCheckResponse>;
    certinfo: (data: IGPBCryptoModuleCertinfo) => Promise<IGPCryptoModuleBResultSystemInfo>;
    keycontainersSft: () => Promise<IKeyContainerSFT>;
    sapiServiceVersion: () => Promise<string>;
    sapiVersion: () => Promise<string>;
    sapiStop: () => Promise<any>;
    getUserStorageCertificates: (accreditedCerts: string[], certificateFilter: ICertFilter) => Promise<IUserOrgCertificate[]>;
    /**
     * Получить все доступные пользователю криптоплагины.
     */
    getAvailableCryptoModulesVersion: () => Promise<{
        bss: any;
        sft: any;
    }>;
    /**
     * Получить плагин под которым авторизовался пользователь.
     */
    getAuthPlugin: () => {
        type: CRYPTO_PLUGIN_TYPE;
        version: string;
    } | undefined;
    /**
     * Генерация ключей и запроса на сертификат.
     */
    generateKey: (params: GenerateKeyParams) => Promise<GenerateKeyResult['data']>;
};
