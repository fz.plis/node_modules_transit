import type { ICryptoModule } from '../../interfaces';
export { setCryptoPlugin } from './crypto-module-service';
export interface IOlkInitOptions {
    certData: string;
    containerNameLocation: string;
}
export declare const olkCryptoModule: ICryptoModule;
