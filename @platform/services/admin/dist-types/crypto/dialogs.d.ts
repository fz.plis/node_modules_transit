import React from 'react';
import type { IUniversalSignData } from '..';
import type { IError } from './olk/crypto-module-interface';
export interface ISignErrorDialogProps {
    header: string;
    text: string;
    error?: IError;
}
export interface IErrorMessageProps {
    text: React.ReactNode;
}
export declare const ErrorMessage: React.FC<IErrorMessageProps>;
export interface IErrorDetailsProps {
    error: IError;
}
export declare const ErrorDetails: React.FC<IErrorDetailsProps>;
export declare const keyAccessDeined: () => void;
export declare const moduleNotFound: () => void;
export declare const certificatesNotFound: () => void;
export declare const certificatesNotRightSign: () => void;
export declare const signCertificatesNotFound: () => void;
export declare const universalSignError: (signData: IUniversalSignData[], error: any) => Promise<void>;
export declare const signCertificateInvalid: () => void;
export declare const signErrorDialog: {
    internalError: (error?: IError | undefined) => void;
    keyAccessDeined: () => void;
    moduleNotFound: () => void;
    certificatesNotFound: () => void;
    certificatesNotRightSign: () => void;
    signCertificatesNotFound: () => void;
    universalSignError: (signData: IUniversalSignData[], error: any) => Promise<void>;
    signCertificateInvalid: () => void;
};
