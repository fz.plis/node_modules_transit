export declare const t: (key: string, params?: any) => string;
declare const _default: {
    bss: {
        /**
         * @ru
         * Клиент-Банк
         */
        readonly label: string;
    };
    bsc: {
        /**
         * @ru
         * Банковское сопровождение контрактов
         */
        readonly label: string;
    };
    /**
     * @ru
     * Продолжить
     */
    readonly continue: string;
    /**
     * @ru
     * Готово
     */
    readonly done: string;
    client: {
        /**
         * @ru
         * Заявитель
         */
        readonly label: string;
    };
    /**
     * @ru
     * Счет {accountNum, account}
     */
    account: (p: {
        accountNum: string;
    }) => string;
    action: {
        /**
         * @ru
         * Закрыть окно
         */
        readonly closeWindow: string;
        /**
         * @ru
         * Зарезервировать счёт
         */
        readonly reserveAccount: string;
        /**
         * @ru
         * Свернуть
         */
        readonly collapse: string;
        /**
         * @ru
         * Развернуть
         */
        readonly expand: string;
        /**
         * @ru
         * Не удалось выполнить операцию
         */
        readonly error: string;
        /**
         * @ru
         * Недостаточно прав для подписи документа, поскольку Вы не являетесь ЕИО (единоличным исполнительным органом)
         */
        readonly errorSignAndSend: string;
        /**
         * @ru
         * Подписать
         */
        readonly sign: string;
        /**
         * @ru
         * Отменить
         */
        readonly cancel: string;
        /**
         * @ru
         * Применить
         */
        readonly accept: string;
        /**
         * @ru
         * Закрыть
         */
        readonly close: string;
        /**
         * @ru
         * Повторить
         */
        readonly repeat: string;
        /**
         * @ru
         * Посмотреть
         */
        readonly show: string;
        details: {
            /**
             * @ru
             * Показать подробности
             */
            readonly show: string;
            /**
             * @ru
             * Скрыть подробности
             */
            readonly hide: string;
        };
        /**
         * @ru
         * Да
         */
        readonly yes: string;
        /**
         * @ru
         * Нет
         */
        readonly no: string;
        /**
         * @ru
         * Установить
         */
        readonly install: string;
        /**
         * @ru
         * Повторить
         */
        readonly reTry: string;
        /**
         * @ru
         * Подписать другие документы
         */
        readonly signOthers: string;
        /**
         * @ru
         * Прервать
         */
        readonly cancelSign: string;
        /**
         * @ru
         * Открыть счёт
         */
        readonly openAccount: string;
        /**
         * @ru
         * Создать
         */
        readonly create: string;
        /**
         * @ru
         * Открыть дополнительный счёт (необходима ЭП)
         */
        readonly openAccountWithSign: string;
        /**
         * @ru
         * Открыть первый счёт
         */
        readonly regDoc: string;
        /**
         * @ru
         * ЛК ВЭД
         */
        readonly currencyControl: string;
        OrganizationNotDefined: {
            /**
             * @ru
             * Организация не определена
             */
            readonly label: string;
        };
        organizations: {
            /**
             * @ru
             * Все организации
             */
            readonly label: string;
        };
        /**
         * @ru
         * Выпустить новый сертификат
         */
        readonly generateCertificate: string;
        /**
         * @ru
         * Регистрация
         */
        readonly registration: string;
        /**
         * @ru
         * Больше не показывать
         */
        readonly notShowAgain: string;
        /**
         * @ru
         * Войти под учетной записью Клиент-Банк.WEB
         */
        readonly loginClientBank: string;
        /**
         * @ru
         * Заключить соглашение
         */
        readonly toConstitution: string;
        /**
         * @ru
         * Отмена
         */
        readonly сancel: string;
        /**
         * @ru
         * Удалить
         */
        readonly remove: string;
    };
    alert: {
        /**
         * @ru
         * Ваша сессия завершена.
         */
        readonly sessionEnded: string;
    };
    menuService: {
        /**
         * @ru
         * Сертификаты
         */
        readonly certificates: string;
    };
    errors: {
        passwordErrors: {
            /**
             * @ru
             * Длина пароля должна превышать 7 символов
             */
            readonly minLength: string;
            /**
             * @ru
             * Пароль слишком длинный
             */
            readonly maxLength: string;
            /**
             * @ru
             * Пароль должен содержать символы в нижнем регистре
             */
            readonly lowerCase: string;
            /**
             * @ru
             * Пароль должен содержать символы в верхнем регистре
             */
            readonly upperCase: string;
            /**
             * @ru
             * Пароль должен содержать как минимум одну цифру
             */
            readonly minNum: string;
            /**
             * @ru
             * Пароль должен содержать как минимум один специальный символ
             */
            readonly minSymb: string;
            /**
             * @ru
             * Пароль должен содержать минимум три из четырех групп символов (большие, маленькие цифры, спецсимволы)
             */
            readonly minGroup: string;
            /**
             * @ru
             * Пароль содержит недопустимые символы
             */
            readonly unacceptable: string;
            /**
             * @ru
             * Пароль совпадает с логином
             */
            readonly coincidence: string;
        };
        /**
         * @ru
         * Введены некорректные учетные данные. Измените данные и повторите попытку
         */
        readonly incorrectData: string;
        /**
         * @ru
         * Превышено количество попыток ввода кода подтверждения. Ваша учетная запись временно заблокирована. Обратитесь в службу поддержки Банка: 8 (800) 100-11-89
         */
        readonly numberAttemptsExceededBlock: string;
        /**
         * @ru
         * Превышено количество попыток ввода кода подтверждения.
         */
        readonly numberOfTriesExceededSendSMS: string;
        /**
         * @ru
         * Ваша учетная запись заблокирована. Обратитесь в службу поддержки Банка: 8 (800) 100-11-89
         */
        readonly accountBlocked: string;
        /**
         * @ru
         * Произошла ошибка сети, пожалуйста проверьте подключение к интернету.
         */
        readonly networkError: string;
        /**
         * @ru
         * При обращении к серверу произошла ошибка.
         * Попробуйте обновить страницу.
         *
         * Если ошибка повторится, свяжитесь с нами, пожалуйста.
         * Контакты для справок: 8 (800) 100-11-89
         */
        readonly serverError: string;
        /**
         * @ru
         * Проверьте адрес электронной почты
         */
        readonly email: string;
        /**
         * @ru
         * Превышено количество попыток ввода кода подтверждения. Повторите операцию позже или обратитесь в службу поддержки Банка: 8 (800) 100-11-89
         */
        readonly numberAttemptsExceeded: string;
        /**
         * @ru
         * Для пользователя не заданы настройки для входа по временному паролю. Обратитесь в службу поддержки Банка: 8 (800) 100-11-89
         */
        readonly mfaTotpUserNotFound: string;
        fatalHandler: {
            /**
             * @ru
             * Упс, что-то пошло не так
             */
            readonly header: string;
            /**
             * @ru
             * При обращении к серверу произошла ошибка, попробуйте обновить страницу
             */
            readonly firstBlock: string;
            /**
             * @ru
             * Если ошибка повторится, свяжитесь с нами, пожалуйста
             */
            readonly secondBlock: string;
            /**
             * @ru
             * 8 800 100 11 89
             */
            readonly phone: string;
        };
        /**
         * @ru
         * Код не может быть выслан на указанный телефон. Попробуйте запросить код еще раз или обратитесь в службу поддержки Банка. Контакты для справок: 8 (800) 100-11-89
         */
        readonly phoneNumber: string;
        /**
         * @ru
         * Произошла ошибка при загрузке модуля.
         */
        readonly loadingModule: string;
        /**
         * @ru
         * Произошла ошибка при получении дайджеста документа.
         */
        readonly fatalGetDigest: string;
    };
    error: {
        /**
         * @ru
         * Ошибка проверки кода подтверждения
         */
        readonly verificationCodesendFailed: string;
        /**
         * @ru
         * Ошибка
         */
        readonly text: string;
        /**
         * @ru
         * Ошибка выполнения операции
         */
        readonly callStreamError: string;
        /**
         * @ru
         * Запросить код не удалось, попробуйте позже.
         */
        readonly codeRequest: string;
        /**
         * @ru
         * Не удалось проверить код подтверждения. Повторите ввод или запросите код еще раз.
         */
        readonly codeCheck: string;
    };
    crypto: {
        error: {
            /**
             * @ru
             * Отсутствуют сертификаты
             */
            readonly certsNotFound: string;
            /**
             * @ru
             * Не установлено средство
             * электронной подписи
             */
            readonly cryptomodulNotInstalled: string;
            /**
             * @ru
             * Для создания электронной подписи
             * документов используется средство
             * электронной подписи «Криптоплагин».
             */
            readonly cryptomodulNotInstalledText1: string;
            /**
             * @ru
             * Установите средство подписи и попытайтесь
             * заново подписать документ.
             */
            readonly cryptomodulNotInstalledText2: string;
            access: {
                /**
                 * @ru
                 * Не установлено средство электронной подписи
                 */
                readonly header: string;
                /**
                 * @ru
                 * Вы отказали в доступе к Вашим ключам электронной подписи.
                 */
                readonly text1: string;
                /**
                 * @ru
                 * Повторите операцию, чтобы разрешить доступ и подписать документ.
                 */
                readonly text2: string;
            };
            noCerts: {
                /**
                 * @ru
                 * У Вас нет ни одного действующего сертификата.
                 */
                readonly text1: string;
                /**
                 * @ru
                 * Для уточнения причин, обратитесь в банк по телефону.
                 */
                readonly text2: string;
            };
            /**
             * @ru
             * Не найдена информация о подписанте или сертификате
             */
            readonly cantRetrieveInfoAboutSignerOrCertificate: string;
            /**
             * @ru
             * Возникла ошибка при проверке ключа ЭП выбранного абонента. Для уточнения информации обратитесь в службу технической поддержки по телефону 8 800 100 11 89 (по России бесплатно)
             */
            readonly invalidSign: string;
        };
    };
    statuses: {
        /**
         * @ru
         * Готово
         */
        readonly successful: string;
    };
    dialog: {
        error: {
            /**
             * @ru
             * Произошла ошибка
             */
            readonly title: string;
            /**
             * @ru
             * Произошла ошибка при удалении
             */
            readonly delete: string;
        };
        remove: {
            /**
             * @ru
             * Удаление заявки
             */
            readonly title: string;
        };
        delete: {
            /**
             * @ru
             * * "count > 1" - Документы успешно удалены
             * * "true" - Документ успешно удален
             */
            removed: (p: {
                count: number;
            }) => string;
            /**
             * @ru
             * * "count > 1" - Вы действительно хотите удалить документы?
             * * "true" - Вы действительно хотите удалить документ?
             */
            text: (p: {
                count: number;
            }) => string;
        };
        sign: {
            /**
             * @ru
             * Внутренняя ошибка сервера
             */
            readonly internalServerError: string;
            /**
             * @ru
             * Код ошибки
             */
            readonly errorCode: string;
            /**
             * @ru
             * Установка подписи
             */
            readonly header: string;
        };
        verifySign: {
            /**
             * @ru
             * Проверка подписи
             */
            readonly header: string;
        };
        updateContactInfo: {
            mainStep: {
                /**
                 * @ru
                 * Подтверждение контактной информации
                 */
                readonly header: string;
                /**
                 * @ru
                 * Если вы не подтвердите телефон и адрес электронной почты, то вы не сможете войти в систему
                 */
                readonly text: string;
                /**
                 * @ru
                 * Не показывать уведомление в ближайшее время
                 */
                readonly notShow: string;
                action: {
                    /**
                     * @ru
                     * Выйти
                     */
                    readonly logout: string;
                    /**
                     * @ru
                     * Подтвердить информацию
                     */
                    readonly update: string;
                };
            };
            updateEmail: {
                /**
                 * @ru
                 * Для подтверждения почтового адреса, мы направим Вам сообщение на почту с 6-значным кодом, который нужно будет ввести здесь в отдельном окне
                 */
                readonly text: string;
                /**
                 * @ru
                 * Изменение адреса электронной почты
                 */
                readonly header: string;
                field: {
                    /**
                     * @ru
                     * E-mail
                     */
                    readonly email: string;
                };
            };
            updatePhone: {
                /**
                 * @ru
                 * Для подтверждения номера телефона, мы направим Вам SMS с 6-значным кодом, который нужно будет ввести здесь в отдельном окне
                 */
                readonly text: string;
                /**
                 * @ru
                 * Изменение номера телефона
                 */
                readonly header: string;
                field: {
                    /**
                     * @ru
                     * Телефон
                     */
                    readonly phoneNumber: string;
                };
            };
            confirmPhone: {
                /**
                 * @ru
                 * Подтверждение номера телефона
                 */
                readonly header: string;
            };
            confirmEmail: {
                /**
                 * @ru
                 * Подтверждение адреса электронной почты
                 */
                readonly header: string;
            };
            confirm: {
                /**
                 * @ru
                 * На указанный Вами номер телефона
                 */
                readonly smsCodeMessageFirst: string;
                /**
                 * @ru
                 * На указанный Вами адрес электронной почты
                 */
                readonly emailCodeMessageFirst: string;
                /**
                 * @ru
                 * отправлено смс с кодом подтверждения.
                 */
                readonly smsCodeMessageSecond: string;
                /**
                 * @ru
                 * отправлено письмо с кодом подтверждения.
                 */
                readonly emailCodeMessageSecond: string;
                /**
                 * @ru
                 * Обычно сообщение приходит в течение двух минут.
                 */
                readonly smsCodeNote: string;
                /**
                 * @ru
                 * Обычно письмо приходит в течение пяти минут.
                 */
                readonly emailCodeNote: string;
                /**
                 * @ru
                 * Код подтверждения
                 */
                readonly smsCode: string;
                /**
                 * @ru
                 * Получить код еще раз
                 */
                readonly sendAgain: string;
                /**
                 * @ru
                 * Превышено количество попыток ввода кода подтверждения. Ваша сессия завершена
                 */
                readonly exceededCodeEntryAttempts: string;
                /**
                 * @ru
                 * Неверное значение кода
                 */
                readonly incorrectValueCode: string;
            };
            /**
             * @ru
             * Операция смены пароля завершилась успешно
             */
            readonly passwordChangeSuccessfully: string;
            /**
             * @ru
             * Неверное значение
             */
            readonly incorrectValue: string;
            /**
             * @ru
             * {text} успешно подтвержден
             */
            successfullyConfirmed: (p: {
                text: string;
            }) => string;
        };
        importantNotification: {
            /**
             * @ru
             * Есть уведомления, выполнение которых нельзя отложить. Показать эти уведомления?
             */
            readonly postponeInfo: string;
        };
        history: {
            /**
             * @ru
             * История изменений заявки
             */
            readonly header: string;
            /**
             * @ru
             * № {num} от {date,date}
             */
            numberAndDate: (p: {
                num: string;
                date: string;
            }) => string;
            /**
             * @ru
             * Дата изменений
             */
            readonly changedIn: string;
        };
        optimisticLockError: {
            /**
             * @ru
             * Другой пользователь изменил данный документ. Пожалуйста, перезагрузите страницу.
             */
            readonly text: string;
        };
    };
    statementType: {
        /**
         * @ru
         * Стандартная
         */
        readonly standart: string;
        /**
         * @ru
         * Расширенная
         */
        readonly extended: string;
    };
    openAccount: {
        /**
         * @ru
         * Заявка на открытие счета №{docNumber} от {docDate, date}
         */
        docName: (p: {
            docNumber: string;
            docDate: string;
        }) => string;
        /**
         * @ru
         * Основная информация
         */
        readonly main: string;
        /**
         * @ru
         * Резервированные ранее счета
         */
        readonly reserved: string;
        /**
         * @ru
         * Не резервированные ранее счета
         */
        readonly newAccounts: string;
        fields: {
            /**
             * @ru
             * Номер документа
             */
            readonly docNumber: string;
            /**
             * @ru
             * Дата документа
             */
            readonly docDate: string;
            client: {
                /**
                 * @ru
                 * Наименование организации
                 */
                readonly name: string;
                /**
                 * @ru
                 * Полное наименование организации
                 */
                readonly fullName: string;
                /**
                 * @ru
                 * Краткое наименование организации
                 */
                readonly shortName: string;
            };
            contract: {
                /**
                 * @ru
                 * Номер договора банковского счета
                 */
                readonly contractNumber: string;
                /**
                 * @ru
                 * Дата заключения договора банковского счета
                 */
                readonly date: string;
                /**
                 * @ru
                 * Карточка ОПОП, привязанная к счету
                 */
                readonly accountNumber: string;
            };
            /**
             * @ru
             * Подключить открываемые счета к интернет и мобильному банку
             */
            readonly onlineBanking: string;
            /**
             * @ru
             * Согласие с условиями сопровождения счетов ГК
             */
            readonly gozAgree: string;
            commission: {
                /**
                 * @ru
                 * Счет списания комиссии
                 */
                readonly accountNumber: string;
            };
            accountInfo: {
                /**
                 * @ru
                 * Номер счета
                 */
                readonly accountNumber: string;
                /**
                 * @ru
                 * Тип счета
                 */
                readonly accountType: string;
                /**
                 * @ru
                 * Подразделение Банка
                 */
                readonly branch: string;
                /**
                 * @ru
                 * Выписка
                 */
                readonly statementType: string;
                govContract: {
                    /**
                     * @ru
                     * Идентификатор государственного контракта
                     */
                    readonly identify: string;
                    /**
                     * @ru
                     * Номер государственного контракта
                     */
                    readonly number: string;
                    /**
                     * @ru
                     * Дата государственного контракта
                     */
                    readonly date: string;
                };
                /**
                 * @ru
                 * Сумма государственного контракта
                 */
                readonly amount: string;
                /**
                 * @ru
                 * ИНН контрагента по государственному контракту
                 */
                readonly agentInn: string;
                /**
                 * @ru
                 * Наименование контрагента по государственному контракту
                 */
                readonly agentName: string;
            };
            newAccountsInfo: {
                branch: {
                    /**
                     * @ru
                     * Подразделение филиала
                     */
                    readonly name: string;
                    /**
                     * @ru
                     * Номер подразделение филиала
                     */
                    readonly number: string;
                };
                info: {
                    currency: {
                        /**
                         * @ru
                         * Наименование валюты счета
                         */
                        readonly name: string;
                        /**
                         * @ru
                         * Код валюты счета
                         */
                        readonly code: string;
                    };
                    /**
                     * @ru
                     * Количество счетов
                     */
                    readonly count: string;
                };
            };
            /**
             * @ru
             * На дату отправки заявления изменения в документах и сведениях, предоставленных ранее при открытии предыдущих счетов отсутствуют
             */
            readonly confirmNoChanges: string;
            /**
             * @ru
             * Компания-резидент
             */
            readonly isResident: string;
            /**
             * @ru
             * ИНН/КИО
             */
            readonly innKio: string;
            /**
             * @ru
             * ОГРН/ОГРНИП
             */
            readonly ogrn: string;
            /**
             * @ru
             * КПП
             */
            readonly kpp: string;
            /**
             * @ru
             * БИК филиала
             */
            readonly branchBic: string;
            /**
             * @ru
             * Кор.счет филиала
             */
            readonly korAcc: string;
            /**
             * @ru
             * Номер счета заказчика по контракту
             */
            readonly custumerAccountNumber: string;
        };
    };
    signSend: {
        /**
         * @ru
         * * "count === 1" - Документ подписан и отправлен
         * * "count % 10 === 1 && count % 11 !== 0" - Подписано и отправлено {count} документ
         * * "count % 10 > 1 && count % 10 < 5" - Подписано и отправлено {count} документа
         * * "true" - Подписано и отправлено {count} документов
         */
        succeded: (p: {
            count: number;
        }) => string;
    };
    send: {
        /**
         * @ru
         * Не удалось отправить документы
         */
        readonly fatal: string;
        /**
         * @ru
         * * "count === 1" - Документ отправлен
         * * "count % 10 === 1 && count % 11 !== 0" - Отправлено {count} документ
         * * "count % 10 > 1 && count % 10 < 5" - Отправлено {count} документа
         * * "true" - Отправлено {count} документов
         */
        succeded: (p: {
            count: number;
        }) => string;
        /**
         * @ru
         * * "count === 1" - Не удалось отправить документ
         * * "count % 10 === 1 && count % 11 !== 0" - Не удалось отправить {count} документ
         * * "count % 10 > 1 && count % 10 < 5" - Не удалось отправить {count} документа
         * * "true" - Не удалось отправить {count} документов
         */
        failed: (p: {
            count: number;
        }) => string;
    };
    sign: {
        /**
         * @ru
         * Не удалось поставить подпись
         */
        readonly fatal: string;
        /**
         * @ru
         * Ошибка подписи
         */
        readonly signFailed: string;
        /**
         * @ru
         * Возникла внутренняя ошибка сервера
         */
        readonly fatalHeader: string;
        /**
         * @ru
         * Для уточнения информации обратитесь в службу технической поддержки по телефону 8 800 100 11 89 (по России бесплатно)
         */
        readonly fatalText: string;
        /**
         * @ru
         * Информация для службы технической поддержки
         */
        readonly fatalCollapsedHeader: string;
        /**
         * @ru
         * * "count === 1" - Документ подписан
         * * "count % 10 === 1 && count % 11 !== 0" - Подписан {count} документ
         * * "count % 10 > 1 && count % 10 < 5" - Подписано {count} документа
         * * "true" - Подписано {count} документов
         */
        succeded: (p: {
            count: number;
        }) => string;
        /**
         * @ru
         * Документ успешно подписан.
         */
        readonly successfullySignedDocument: string;
        /**
         * @ru
         * * "count === 1" - Не удалось подписать документ
         * * "count % 10 === 1 && count % 11 !== 0" - Не удалось подписать {count} документ
         * * "count % 10 > 1 && count % 10 < 5" - Не удалось подписать {count} документа
         * * "true" - Не удалось подписать {count} документов
         */
        failed: (p: {
            count: number;
        }) => string;
    };
    unload: {
        /**
         * @ru
         * * "count === 1" - Не удалось выгрузить документ
         * * "count % 10 === 1 && count % 11 !== 0" - Не удалось выгрузить {count} документ
         * * "count % 10 > 1 && count % 10 < 5" - Не удалось выгрузить {count} документа
         * * "true" - Не удалось выгрузить {count} документов
         */
        failed: (p: {
            count: number;
        }) => string;
        /**
         * @ru
         * Документ успешно выгружен
         */
        readonly successfullyUnloadDocument: string;
    };
    upload: {
        /**
         * @ru
         * * "count === 1" - Документ выгружен
         * * "count % 10 === 1 && count % 11 !== 0" - Выгружен {count} документ
         * * "count % 10 > 1 && count % 10 < 5" - Выгружено {count} документа
         * * "true" - Выгружено {count} документов
         */
        succeded: (p: {
            count: number;
        }) => string;
    };
    verifySign: {
        /**
         * @ru
         * Основная информация
         */
        readonly header: string;
        error: {
            /**
             * @ru
             * Код ошибки:
             */
            readonly code: string;
        };
        /**
         * @ru
         * Подпись верна
         */
        readonly valid: string;
        /**
         * @ru
         * Подпись неверна
         */
        readonly invalid: string;
        fields: {
            digest: {
                /**
                 * @ru
                 * Подписываемые данные
                 */
                readonly signData: string;
            };
            signature: {
                profile: {
                    /**
                     * @ru
                     * ФИО абонента
                     */
                    readonly name: string;
                    /**
                     * @ru
                     * Хеш сертификата
                     */
                    readonly thumbprint: string;
                };
                /**
                 * @ru
                 * Дата подписи
                 */
                readonly signDate: string;
                /**
                 * @ru
                 * Владелец сертификата
                 */
                readonly nameOfSignatory: string;
                /**
                 * @ru
                 * Сертификат
                 */
                readonly certificate: string;
                /**
                 * @ru
                 * Срок действия
                 */
                readonly validPeriod: string;
                /**
                 * @ru
                 * Издатель
                 */
                readonly issuerName: string;
                /**
                 * @ru
                 * Подпись
                 */
                readonly signature: string;
            };
        };
        /**
         * @ru
         * Проверено документов:
         */
        readonly verifiedDocs: string;
        /**
         * @ru
         * Не удалось выполнить проверку подписи
         */
        readonly fatal: string;
        /**
         * @ru
         * * "count === 1" - Не удалось выполнить проверку подписи документа
         * * "count % 10 === 1 && count % 11 !== 0" - Не удалось выполнить проверку подписи у {count} документа
         * * "true" - Не удалось выполнить проверку подписи {count} документов
         */
        failed: (p: {
            count: number;
        }) => string;
    };
    certificatesNotFound: {
        /**
         * @ru
         * Отсутствуют сертификаты входа в систему
         */
        readonly title: string;
        /**
         * @ru
         * У Вас нет ни одного действующего сертификата для входа в ситему.
         */
        readonly text1: string;
        /**
         * @ru
         * Для уточнения причин, обратитесь в банк по телефону.
         */
        readonly text2: string;
    };
    signCertificatesNotFound: {
        error: {
            /**
             * @ru
             * Отсутствуют сертификаты для подписи
             */
            readonly title: string;
            /**
             * @ru
             * У Вас нет ни одного сертификата для подписи.
             */
            readonly message: string;
        };
    };
    files: {
        error: {
            /**
             * @ru
             * Не пройдена проверка антивирусом, проверьте файл и загрузите его повторно
             */
            readonly isMalware: string;
            /**
             * @ru
             * Не удалось загрузить файл, проверьте файл и загрузите его повторно
             */
            readonly notLoaded: string;
            /**
             * @ru
             * Вы пытаетесь добавить один или несколько файлов формат которых не поддерживается. Поддерживаемые форматы:
             */
            readonly invalidExt: string;
            /**
             * @ru
             * Загрузка данного формата файла не поддерживается.
             */
            readonly invalidExtenstions: string;
            dataSizeExceeded: {
                /**
                 * @ru
                 * Превышен допустимый максимальный суммарный размер всех файлов в {size,size}
                 */
                multi: (p: {
                    size: number;
                }) => string;
                /**
                 * @ru
                 * Превышен допустимый максимальный размер файла в {size,size}
                 */
                single: (p: {
                    size: number;
                }) => string;
            };
            dataSizeNotEnough: {
                /**
                 * @ru
                 * Файлы не должны иметь нулевой размер
                 */
                readonly multi: string;
                /**
                 * @ru
                 * Файл не должен иметь нулевой размер
                 */
                readonly single: string;
            };
            /**
             * @ru
             * Допустимо вложение только одного файла
             */
            readonly singleOnly: string;
        };
    };
    validation: {
        login: {
            /**
             * @ru
             * Логин должен состоять из английских букв и цифр. Длина логина не менее 7 символов.
             */
            readonly pattern: string;
            /**
             * @ru
             * Логин не должен совпадать с номером телефона или паролем.
             */
            readonly phoneOrPassword: string;
        };
        /**
         * @ru
         * Длина атрибута должны быть меньше {length}
         */
        shorterThan: (p: {
            length: number;
        }) => string;
        /**
         * @ru
         * Длина атрибута должны быть больше {length}
         */
        longerThan: (p: {
            length: number;
        }) => string;
        /**
         * @ru
         * Длина атрибута должны быть равна {length}
         */
        exactLength: (p: {
            length: number;
        }) => string;
        /**
         * @ru
         * Значение атрибута должно содержать только цифры
         */
        readonly onlyNumbers: string;
        /**
         * @ru
         * Поле обязательно для заполнения
         */
        readonly necessarilyField: string;
        /**
         * @ru
         * Номер телефона должен состоять из 11 цифр
         */
        readonly invalidNumber: string;
        /**
         * @ru
         * Номер телефона должен начинаться с +79
         */
        readonly phoneMustBeBegin: string;
        /**
         * @ru
         * Новый номер телефона совпадает с текущим
         */
        readonly newNumberMatchesCurrent: string;
        /**
         * @ru
         * Указанный номер телефона содержится в логине
         */
        readonly newNumberMatchesLogin: string;
        /**
         * @ru
         * Новый адрес электронной почты совпадает с текущим
         */
        readonly newEmailMatchesCurrent: string;
    };
    fileExtensions: {
        /**
         * @ru
         * PDF
         */
        readonly pdf: string;
        /**
         * @ru
         * EXCEL
         */
        readonly xls: string;
    };
    print: {
        confirmCount: {
            /**
             * @ru
             * Внимание!
             */
            readonly header: string;
            /**
             * @ru
             * * "(count > 10 && count < 15) || (count % 10 != 1)" - Количество выбранных Вами документов превышает допустимое. Будет распечатано {count} {doc} из {total}.
             * * "true" - Количество выбранных Вами документов превышает допустимое. Будет распечатан {count} {doc} из {total}.
             */
            context: (p: {
                doc: string;
                count: number;
                total: number;
            }) => string;
        };
        documents: {
            /**
             * @ru
             * * "count > 10 && count < 15" - документов
             * * "count % 10 === 1" - документ
             * * "count % 10 > 1 && count % 10 < 5" - документа
             * * "true" - документов
             */
            count: (p: {
                count: number;
            }) => string;
        };
        /**
         * @ru
         * Распечатать
         */
        readonly text: string;
        confirmLastPrint: {
            /**
             * @ru
             * хотите продолжить печать оставшихся документов: {last}?
             */
            context: (p: {
                last: number;
            }) => string;
        };
        confirmPrint: {
            /**
             * @ru
             * * "(count > 10 && count < 15) || (count % 10 != 1)" - Распечатано {count} {doc} из {total}
             * * "true" - Распечатан {count} {doc} из {total}
             */
            context: (p: {
                doc: string;
                count: number;
                total: number;
            }) => string;
        };
    };
    page: {
        /**
         * @ru
         * Страница не найдена
         */
        readonly error404: string;
    };
    segments: {
        /**
         * @ru
         * Заявка на открытие дополнительного счета
         */
        readonly cas_acc_open_second_req: string;
        /**
         * @ru
         * Заявка на акцепт оферты к счету РКO
         */
        readonly cas_offer_rko_acceptance_req: string;
        /**
         * @ru
         * Заявка на присоединение к ГПБ Бизнес-Онлайн
         */
        readonly cas_offer_join_gpb_bo1_req: string;
        /**
         * @ru
         * Заявка на регистрацию / блокировку уполномоченного лица клиента
         */
        readonly cas_offer_join_gpb_bo2_req: string;
        /**
         * @ru
         * Документ свободного формата
         */
        readonly cas_free_format_doc: string;
        /**
         * @ru
         * Фрэйм ДБО БСС для главной страницы
         */
        readonly cas_dbo_bss_frame: string;
        /**
         * @ru
         * Бесшовный переход в ДБО БСС
         */
        readonly cas_switching_to_dbo_bss: string;
        /**
         * @ru
         * Заявка на присоединение к ЛК УВЭД
         */
        readonly cas_offer_join_po_uved: string;
        /**
         * @ru
         * Бесшовный переход в ЛК УВЭД (Обработка)
         */
        readonly cas_switching_to_uved_process: string;
        /**
         * @ru
         * Бесшовный переход в ЛК УВЭД (Просмотр)
         */
        readonly cas_switching_to_uved_view: string;
    };
    fallback: {
        dbo: {
            /**
             * @ru
             * Для работы с функционалом Вам необходимо пройти аутентификацию под учетной записью Клиент-Банк.WEB.
             */
            readonly text: string;
            /**
             * @ru
             * Войти под учетной записью ДБО
             */
            readonly signin: string;
        };
        /**
         * @ru
         * Функционал недоступен
         */
        readonly header: string;
        segment: {
            /**
             * @ru
             * Для данной организации отсутствует сертификат ЭП, обратитесь в Банк
             */
            readonly text: string;
        };
        notEnoughRight: {
            /**
             * @ru
             * Функционал недоступен
             */
            readonly header: string;
            /**
             * @ru
             * Недостаточно прав
             */
            readonly text: string;
        };
    };
    actions: {
        /**
         * @ru
         * Назад
         */
        readonly back: string;
    };
    forms: {
        certificateRequest: {
            label: {
                /**
                 * @ru
                 * Организация
                 */
                readonly organization: string;
                /**
                 * @ru
                 * ФИО
                 */
                readonly fio: string;
                /**
                 * @ru
                 * ИНН
                 */
                readonly innKio: string;
                /**
                 * @ru
                 * ОГРН
                 */
                readonly ogrn: string;
                /**
                 * @ru
                 * КПП
                 */
                readonly kpp: string;
                /**
                 * @ru
                 * Основная информация
                 */
                readonly mainInfo: string;
                /**
                 * @ru
                 * Информация о сертификате
                 */
                readonly certInfo: string;
                /**
                 * @ru
                 * Субъект сертификата
                 */
                readonly certificateSubject: string;
                /**
                 * @ru
                 * Серийный номер сертификата
                 */
                readonly serialNumber: string;
                /**
                 * @ru
                 * Сертификат действителен с
                 */
                readonly certificateValidFrom: string;
                /**
                 * @ru
                 * Сертификат действителен по
                 */
                readonly certificateValidTo: string;
                /**
                 * @ru
                 * Издатель сертификата
                 */
                readonly certificateIssuer: string;
                /**
                 * @ru
                 * Отпечаток сертификата
                 */
                readonly thumbprint: string;
                certificate: {
                    /**
                     * @ru
                     * Общепринятое имя (CN)
                     */
                    readonly commonName: string;
                    /**
                     * @ru
                     * Организация (O)
                     */
                    readonly organization: string;
                    /**
                     * @ru
                     * Подразделение (OU)
                     */
                    readonly organizationUnit: string;
                    /**
                     * @ru
                     * Населенный пункт (L)
                     */
                    readonly location: string;
                    /**
                     * @ru
                     * Страна (C)
                     */
                    readonly country: string;
                    /**
                     * @ru
                     * E-mail (E)
                     */
                    readonly email: string;
                    /**
                     * @ru
                     * Должность (T)
                     */
                    readonly title: string;
                    /**
                     * @ru
                     * Имя и отчество (G)
                     */
                    readonly name: string;
                    /**
                     * @ru
                     * Фамилия (SN)
                     */
                    readonly lastName: string;
                };
            };
        };
        signForm: {
            /**
             * @ru
             * Выберите подписанта
             */
            readonly selectSigner: string;
        };
    };
    requestType: {
        /**
         * @ru
         * Заявка на резервирование счета
         */
        readonly reserve: string;
        /**
         * @ru
         * Заявка на открытие дополнительного счета
         */
        readonly booking: string;
        /**
         * @ru
         * Заявка на открытие дополнительного счета и заключение ДБС
         */
        readonly newBooking: string;
        /**
         * @ru
         * Заявка на открытие первого счета
         */
        readonly regdoc: string;
        /**
         * @ru
         * Соглашение «ГПБ Бизнес-Онлайн»
         */
        readonly constitution: string;
        /**
         * @ru
         * Заявка на предоставление доступа и полномочий сотруднику в «ГПБ Бизнес-онлайн»
         */
        readonly constitution2new: string;
        /**
         * @ru
         * Конституция. Заявление 2
         */
        readonly constitution2: string;
        /**
         * @ru
         * Заявка на акцепт оферты к счету РКО
         */
        readonly offer: string;
        /**
         * @ru
         * Подключение КЭП
         */
        readonly ccert: string;
        /**
         * @ru
         * Документ свободного формата
         */
        readonly free: string;
        /**
         * @ru
         * Соглашение о подключении к ЛК УВЭД
         */
        readonly uved: string;
        /**
         * @ru
         * Предоставление/изменение доступа сотруднику к ЛК УВЭД
         */
        readonly userUved: string;
        /**
         * @ru
         * Соглашение о присоединении к онлайн-сервисам услуги Материальный пулинг
         */
        readonly lkmp: string;
        /**
         * @ru
         * Предоставление/изменение доступа к онлайн-сервисам услуги Материальный пулинг
         */
        readonly lkmpAccess: string;
        /**
         * @ru
         * Заявка на автолизинг
         */
        readonly carLease: string;
        /**
         * @ru
         * Заявка на корпоративный лизинг
         */
        readonly corpLease: string;
        /**
         * @ru
         * Запрос на проверку полномочий на заключение ДБС с открытием 2+ счетов
         */
        readonly checkPriviligeO2: string;
        /**
         * @ru
         * Запрос на проверку полномочий на открытие счетов по существующему ДБС
         */
        readonly checkPriviligeOpen: string;
        /**
         * @ru
         * Запрос справок для ЮЛ
         */
        readonly requestForAccountInfo: string;
        /**
         * @ru
         * Распоряжение на периодический перевод денежных средств
         */
        readonly rko: string;
        /**
         * @ru
         * Генерация НЭП
         */
        readonly offerGenCert: string;
    };
    userNotification: {
        dialog: {
            /**
             * @ru
             * Уведомления
             */
            readonly title: string;
            /**
             * @ru
             * Отложить на один день
             */
            readonly close: string;
        };
    };
    signDialog: {
        /**
         * @ru
         * Подробная информация о подписываемых данных
         */
        readonly detailedInfo: string;
        /**
         * @ru
         * Выбрано документов
         */
        readonly summary: string;
        /**
         * @ru
         * Пользователь отказался от выполнения операции
         */
        readonly userCancelErrorMessage: string;
        /**
         * @ru
         * При установке электронной подписи для документов {docs} произошла ошибка
         */
        signError: (p: {
            docs: string;
        }) => string;
    };
    labels: {
        /**
         * @ru
         * До
         */
        readonly before: string;
        /**
         * @ru
         * Да
         */
        readonly yes: string;
        /**
         * @ru
         * Нет
         */
        readonly no: string;
        /**
         * @ru
         * Произошла непредвиденная ошибка
         */
        readonly systemError: string;
        /**
         * @ru
         * Попробуйте еще раз позднее
         */
        readonly repeatOnMoreTimeLater: string;
    };
    modal: {
        generateCertificate: {
            /**
             * @ru
             * Выпуск нового сертификата
             */
            readonly header: string;
            /**
             * @ru
             * Дата окончания срока действия сертификата
             */
            readonly endDate: string;
            /**
             * @ru
             * Владелец сертификата
             */
            readonly owner: string;
            /**
             * @ru
             * Выберите тип нового сертификата
             */
            readonly selectCertificateType: string;
            options: {
                /**
                 * @ru
                 * Зарегистрировать сертификат КЭП
                 */
                readonly qes: string;
                qesInfo: {
                    /**
                     * @ru
                     * Вы можете получить сертификат Квалифицированной Электронной Подписи
                     */
                    readonly part1: string;
                    /**
                     * @ru
                     * и зарегистрировать его в системе «ГПБ Бизнес Онлайн». Сертификат КЭП позволит Вам работать со всеми дистанционными сервисами Банка ГПБ (АО) и других коммерческих и государственных организаций.
                     */
                    readonly part2: string;
                    /**
                     * @ru
                     * в стороннем аккредитованном удостоверяющем центре
                     */
                    readonly link: string;
                };
                /**
                 * @ru
                 * Выпустить сертификат НЭП
                 */
                readonly ues: string;
                /**
                 * @ru
                 * Удостоверяющий Центр Банка ГПБ (АО) выпустит для Вас сертификат Неквалифицированной Электронной Подписи. Сертификат НЭП позволит Вам работать со всеми дистанционными сервисами Банка ГПБ (АО). Выпуск сертификата НЭП будет происходить на странице системы Клиент-Банк.Web.
                 */
                readonly uesInfo: string;
            };
        };
    };
    bankClient: {
        labels: {
            /**
             * @ru
             * ИНН
             */
            readonly innKio: string;
            /**
             * @ru
             * ОГРН
             */
            readonly ogrn: string;
            /**
             * @ru
             * РНК
             */
            readonly abs: string;
            /**
             * @ru
             * Показывать потенциальных клиентов
             */
            readonly showPotential: string;
        };
    };
    components: {
        collapsedCheckInfo: {
            /**
             * @ru
             * Подпись верна
             */
            readonly signIsValid: string;
            /**
             * @ru
             * Подпись неверна
             */
            readonly signIsNotValid: string;
            certInfo: {
                /**
                 * @ru
                 * Сертификат ЭП
                 */
                readonly title: string;
                /**
                 * @ru
                 * Владелец ЭП:
                 */
                readonly owner: string;
                /**
                 * @ru
                 * Организация:
                 */
                readonly org: string;
                /**
                 * @ru
                 * Номер:
                 */
                readonly num: string;
                /**
                 * @ru
                 * Издатель:
                 */
                readonly creator: string;
                /**
                 * @ru
                 * Срок действия:
                 */
                readonly valid: string;
            };
            signExtInfo: {
                /**
                 * @ru
                 * Подписано:
                 */
                readonly signed: string;
                /**
                 * @ru
                 * Дата:
                 */
                readonly date: string;
                /**
                 * @ru
                 * Роль:
                 */
                readonly role: string;
                /**
                 * @ru
                 * Код ошибки:
                 */
                readonly errorCode: string;
            };
        };
        authority: {
            /**
             * @ru
             * Заявление должно быть подписано единоличным исполнительным органом организации либо лицом, доверенность на которого предоставлена в банк.
             */
            readonly noCertificateErrorMessage: string;
        };
    };
    modals: {
        signCheckDialog: {
            /**
             * @ru
             * Проверка подписи
             */
            readonly title: string;
            /**
             * @ru
             * Показать все
             */
            readonly showAll: string;
            /**
             * @ru
             * Скрыть все
             */
            readonly hideAll: string;
        };
    };
    userType: {
        /**
         * @ru
         * Клиент
         */
        readonly client: string;
        /**
         * @ru
         * Банк
         */
        readonly bank: string;
        /**
         * @ru
         * Технический пользователь
         */
        readonly technical: string;
    };
    /**
     * @ru
     * Фильтр
     */
    readonly filter: string;
    consent: {
        labels: {
            /**
             * @ru
             * Согласен на обработку и передачу персональных данных
             */
            readonly personalData: string;
            /**
             * @ru
             * Документ временно недоступен, попробуйте еще раз позднее
             */
            readonly documentNotAvailable: string;
            /**
             * @ru
             * Я соглашаюсь с условиями на передачу персональных данных
             */
            readonly iAgreeWithPersonalData: string;
            /**
             * @ru
             * Я соглашаюсь с
             */
            readonly iAgree: string;
            /**
             * @ru
             * условиями на передачу персональных данных
             */
            readonly withPersonalData: string;
            /**
             * @ru
             * Я принимаю
             */
            readonly iAccept: string;
            /**
             * @ru
             * условия соглашения на обработку и передачу данных
             */
            readonly acceptWithPersonalData: string;
        };
        approve: {
            /**
             * @ru
             * Подтверждение согласия на обработку и передачу персональных данных
             */
            readonly header: string;
            /**
             * @ru
             * Для перехода на сайт партнёра Банка ГПБ (АО) необходимо принять согласие на обработку и передачу персональных данных
             */
            readonly message: string;
        };
        toggle: {
            /**
             * @ru
             * Свернуть текст согласия
             */
            readonly collapse: string;
        };
    };
    /**
     * @ru
     * * "selected === 0" - Документы не выбраны
     * * "selected === 1" - Выбран 1 документ из {total}
     * * "selected > 1 && selected < 5" - Выбрано {selected} документа из {total}
     * * "selected >= 5" - Выбрано {selected} документов из {total}
     */
    selectedDocumentsInfo: (p: {
        selected: number;
        total: number;
    }) => string;
    serviceUnavailable: {
        /**
         * @ru
         * Сервис временно недоступен
         */
        readonly header: string;
        /**
         * @ru
         * Проводятся технические работы. Попробуйте обратиться к сервису немного позже.
         */
        readonly techWork: string;
        /**
         * @ru
         * Приносим извинения за доставленные неудобства.
         */
        readonly sorry: string;
    };
    confirmSpoForm: {
        /**
         * @ru
         * Комментарий для клиента
         */
        readonly commentForClient: string;
    };
    cryptoInstaller: {
        /**
         * @ru
         * Установка подписи
         */
        readonly title: string;
        /**
         * @ru
         * Проверка подписи
         */
        readonly checkSignTitle: string;
        /**
         * @ru
         * Для работы с ЭП не удалось обнаружить необходимое ПО или его версия устарела.
         */
        readonly header: string;
        /**
         * @ru
         * Нажмите кнопку «Установить ПО» для возможности работы с ЭП. Если ПО уже установлено, убедитесь, что оно запущено и нажмите кнопку «Продолжить».
         */
        readonly content: string;
        actions: {
            /**
             * @ru
             * Установить ПО
             */
            readonly installApp: string;
            /**
             * @ru
             * Отмена
             */
            readonly cancel: string;
        };
    };
    products: {
        /**
         * @ru
         * Соглашение о присоединении к ГПБ БО
         */
        readonly constitution: string;
        /**
         * @ru
         * Соглашение о подключении сервиса материального пулинга
         */
        readonly lkMp: string;
        /**
         * @ru
         * Соглашение о подключении сервиса внешнеэкономической деятельности
         */
        readonly lkUved: string;
    };
    conferenceModal: {
        /**
         * @ru
         * Уважаемые клиенты!
         */
        readonly clients: string;
        /**
         * @ru
         * Приглашаем принять участие в
         */
        readonly info1: string;
        /**
         * @ru
         * онлайн-конференции
         */
        readonly info2: string;
        /**
         * @ru
         * Банка ГПБ (АО) на тему: Актуальные вопросы и изменения законодательства Российской Федерации в области валютного регулирования и валютного контроля
         */
        readonly info3: string;
        /**
         * @ru
         * 28 мая 2021, 9:30 (МСК)
         */
        readonly date: string;
        /**
         * @ru
         * Программа конференции
         */
        readonly program: string;
        /**
         * @ru
         * 09:30–09:40
         */
        readonly greetingTime: string;
        /**
         * @ru
         * Приветственное слово
         */
        readonly greeting: string;
        /**
         * @ru
         * Владимир Бусько, Первый Вице-Президент Банка ГПБ (АО)
         */
        readonly greetingSpeaker: string;
        /**
         * @ru
         * 09:40–11:00
         */
        readonly legislationTime: string;
        /**
         * @ru
         * Обзор изменений законодательства Российской Федерации в области валютного регулирования и валютного контроля
         */
        readonly legislation: string;
        /**
         * @ru
         * Елена Шакина, заместитель Директора Департамента финансового мониторинга и валютного контроля Центрального банка Российской Федерации
         */
        readonly legislationSpeaker: string;
        /**
         * @ru
         * 11:00–11:30
         */
        readonly practiceTime: string;
        /**
         * @ru
         * Практические вопросы сопровождения валютных операций в Банке ГПБ (АО). Личный кабинет участника ВЭД – текущие возможности и планы развития
         */
        readonly practice: string;
        /**
         * @ru
         * Представители Департамента сопровождения валютных операций Банка ГПБ (АО)
         */
        readonly practiceSpeaker: string;
        /**
         * @ru
         * 11:30–12:00
         */
        readonly questionTime: string;
        /**
         * @ru
         * Сессия вопросов и ответов
         */
        readonly question: string;
        /**
         * @ru
         * Представители Департамента сопровождения валютных операций Банка ГПБ (АО)
         */
        readonly questionSpeaker: string;
        /**
         * @ru
         * Конференция Банка ГПБ (АО). Контактный телефон: +7 495 719-19-32
         */
        readonly conference: string;
    };
    changeStatusDialog: {
        /**
         * @ru
         * Изменить статус документа
         */
        readonly header: string;
        /**
         * @ru
         * Статус
         */
        readonly currentStatusLabel: string;
        /**
         * @ru
         * Новый статус
         */
        readonly newStatusLabel: string;
        /**
         * @ru
         * Выберите из списка
         */
        readonly selectPlaceholder: string;
        buttons: {
            /**
             * @ru
             * Установить
             */
            readonly set: string;
            /**
             * @ru
             * Отмена
             */
            readonly cancel: string;
        };
        /**
         * @ru
         * Комментарий для Клиента
         */
        readonly commentForClientLabel: string;
        /**
         * @ru
         * Комментарий для Банка
         */
        readonly commentForBankLabel: string;
        /**
         * @ru
         * Реквизит "{fieldName}" является обязательным для заполнения, не может быть пустым
         */
        emptyField: (p: {
            fieldName: string;
        }) => string;
    };
    generateCertificate: {
        /**
         * @ru
         * Не удалось загрузить данные сертификата
         */
        readonly certFileLoadError: string;
    };
    historyDialog: {
        /**
         * @ru
         * История изменений заявки № {number} от {date, date}
         */
        header: (p: {
            number: number;
            date: string;
        }) => string;
        /**
         * @ru
         * Дата и время установки статуса
         */
        readonly date: string;
        /**
         * @ru
         * Статус
         */
        readonly status: string;
        /**
         * @ru
         * Тип пользователя
         */
        readonly userType: string;
        /**
         * @ru
         * ФИО пользователя
         */
        readonly userName: string;
        /**
         * @ru
         * Комментарий для клиента
         */
        readonly infoForClient: string;
        /**
         * @ru
         * Комментарий для банка
         */
        readonly infoForBank: string;
        /**
         * @ru
         * Автомат
         */
        readonly technicalUserType: string;
        /**
         * @ru
         * Операционист
         */
        readonly bankUserType: string;
        hint: {
            userType: {
                /**
                 * @ru
                 * Пользователь клиента
                 */
                readonly client: string;
                /**
                 * @ru
                 * Сотрудник Банка
                 */
                readonly bank: string;
                /**
                 * @ru
                 * Автоматическая операция
                 */
                readonly technical: string;
            };
        };
    };
    outdatedCryptomoduleDialog: {
        /**
         * @ru
         * Криптографический модуль не установлен или его версия устарела
         */
        readonly header: string;
        /**
         * @ru
         * Для продолжения работы необходимо установить актуальную версию
         */
        readonly content: string;
        /**
         * @ru
         * Отмена
         */
        readonly cancel: string;
        /**
         * @ru
         * Перейти к установке
         */
        readonly install: string;
    };
    dboInformationDialog: {
        /**
         * @ru
         * Уважаемый клиент!
         */
        readonly header: string;
        /**
         * @ru
         * Пользовательский интерфейс Клиент-Банк переведён на новую технологическую платформу ГПБ Бизнес-Онлайн. Старый интерфейс системы Клиент-Банк технически устарел и не отвечает требованиям по добавлению новой функциональности, в том числе, по осуществлению платежей, удобству использования, повышению скорости работы и общей безопасности.
         */
        readonly content1: string;
        /**
         * @ru
         * Старая версия Клиент-Банка со временем будет отключена, сейчас производится поэтапное переключение всех клиентов на новую версию. Понимаем, что новое часто бывает непривычным, поэтому будем рады Вашим комментариям и пожеланиям, чтобы учесть их в разработке. Экосистема ГПБ Бизнес-онлайн вместе с новым интерфейсом Клиент-Банка будет развиваться и совершенствоваться.
         */
        readonly content2: string;
        /**
         * @ru
         * Посмотреть документы
         */
        readonly viewDocuments: string;
        mainPageFeatures: {
            /**
             * @ru
             * На главной странице уже доступны:
             */
            readonly header: string;
            /**
             * @ru
             * создание платежного поручения в один клик;
             */
            readonly feature1: string;
            /**
             * @ru
             * запрос выписки по счёту;
             */
            readonly feature2: string;
            /**
             * @ru
             * поиск платежа в истории и его повтор в одно нажатие.
             */
            readonly feature3: string;
        };
        gboFeatures: {
            /**
             * @ru
             * В ГПБ Бизнес-Онлайн Вам также доступны:
             */
            readonly header: string;
            /**
             * @ru
             * онлайн открытие второго и последующих расчётных счетов;
             */
            readonly feature1: string;
            /**
             * @ru
             * открытие специальных счетов участников закупок и счетов по гособоронзаказу;
             */
            readonly feature2: string;
            /**
             * @ru
             * просмотр и оплата задолженностей и штрафов от ФНС;
             */
            readonly feature3: string;
            /**
             * @ru
             * просмотр блокировок по счетам компании, поступившие от операторов электронных торговых площадок;
             */
            readonly feature4: string;
            /**
             * @ru
             * получение электронных банковских гарантий в режиме онлайн;
             */
            readonly feature5: string;
            /**
             * @ru
             * и многое другое.
             */
            readonly feature6: string;
        };
        feedback: {
            /**
             * @ru
             * Первые клиенты уже оценили удобство и скорость работы с новым интерфейсом ГПБ Бизнес-Онлайн
             */
            readonly header: string;
            /**
             * @ru
             * Теперь хотим узнать ваше мнение!
             */
            readonly part1Bold: string;
            /**
             * @ru
             * Напишите нам если возникли проблемы, на почту:
             */
            readonly part2: string;
            /**
             * @ru
             * или позвоните по телефону:
             */
            readonly part3: string;
            /**
             * @ru
             * Вы также можете перейти в прежний интерфейс по кнопке «Клиент Банк», расположенной в меню слева.
             */
            readonly info: string;
        };
        ending: {
            /**
             * @ru
             * Мы постоянно работаем над улучшением и будем держать Вас в курсе новинок!
             */
            readonly part1: string;
            /**
             * @ru
             * Ваш Газпромбанк
             */
            readonly part2: string;
        };
        /**
         * @ru
         * Больше не показывать
         */
        readonly dontShowAgain: string;
    };
    guardRouter: {
        errors: {
            mfa: {
                /**
                 * @ru
                 * Для доступа к функционалу войдите в систему с использованием сертификата электронной подписи
                 */
                readonly message: string;
            };
            authority: {
                /**
                 * @ru
                 * Недостаточно полномочий для использования сервиса
                 */
                readonly message: string;
            };
            allClientEco: {
                /**
                 * @ru
                 * Сервис недоступен.
                 */
                readonly title: string;
                /**
                 * @ru
                 * Если вы хотите стать клиентом банка, то вам необходимо открыть счет.
                 * Если вы уже являетесь действующим клиентом банка, то вам необходимо авторизоваться с логином и паролем, используемыми для входа в систему "Клиент-Банк.WEB", или зарегистрировать сертификат КЭП в профиле пользователя
                 */
                readonly message: string;
            };
            allClientDbo: {
                /**
                 * @ru
                 * Недостаточно полномочий для использования сервиса
                 */
                readonly message: string;
            };
            potentialClientEco: {
                /**
                 * @ru
                 * Сервис не доступен для выбранной организации.
                 */
                readonly title: string;
                /**
                 * @ru
                 * Если вы хотите стать клиентом банка, то вам необходимо открыть счет.
                 * Если вы уже являетесь действующим клиентом банка, то вам необходимо авторизоваться с логином и паролем, используемыми для входа в систему "Клиент-Банк.WEB", или зарегистрировать сертификат КЭП в профиле пользователя
                 */
                readonly message: string;
            };
            potentialClientDbo: {
                /**
                 * @ru
                 * Для работы с сервисом вам необходимо открыть счет для выбранной организации или выбрать другую организацию.
                 */
                readonly message: string;
            };
            activeClientEco: {
                /**
                 * @ru
                 * Для работы с сервисом под выбранной организацией вам необходимо заключить Соглашения об использовании системы электронного документооборота "ГПБ Бизнес-Онлайн", Соглашение на использование системы «ГПБ Бизнес-онлайн» может быть подписано уполномоченным лицом организации имеющим ЭП зарегистрированную в Банке, или авторизоваться с логином и паролем, используемыми для входа в систему "Клиент-Банк.WEB".
                 */
                readonly message: string;
            };
            activeClientDbo: {
                /**
                 * @ru
                 * Недостаточно полномочий для использования сервиса под выбранной организацией. Выберите другую организацию
                 */
                readonly message: string;
            };
            activeClientConstitutionEco: {
                /**
                 * @ru
                 * Недостаточно полномочий для использования сервиса под выбранной организацией. Выберите другую организацию или авторизуйтесь с логином и паролем, используемыми для входа в систему "Клиент-Банк.WEB"
                 */
                readonly message: string;
            };
            activeClientConstitutionDbo: {
                /**
                 * @ru
                 * Недостаточно полномочий для использования сервиса под выбранной организацией. Выберите другую организацию
                 */
                readonly message: string;
            };
            consultant: {
                /**
                 * @ru
                 * При необходимости получения консультации, обратитесь в Банк по номеру телефона 8(800)100-11-89 с кодом ошибки: {code}
                 */
                message: (p: {
                    code: string;
                }) => string;
            };
        };
    };
};
export default _default;
