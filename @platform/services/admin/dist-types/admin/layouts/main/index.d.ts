import React from 'react';
import type { IButtonAction } from '@platform/ui';
export interface ILayoutProps {
    actions?: IButtonAction[];
    disableReserveButton?: boolean;
    disableRegDocButton?: boolean;
    beforeLogout?(): Promise<boolean>;
    onClickExit?(): void;
}
export declare const MainLayout: React.FC<ILayoutProps>;
