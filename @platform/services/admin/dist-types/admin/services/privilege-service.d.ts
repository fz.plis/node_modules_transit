import type { IPrivileges, IServerResp } from '../../interfaces';
export declare const privilegeService: {
    update(data: IPrivileges): Promise<IServerResp<IPrivileges>>;
    delete(id: string): Promise<IServerResp<IPrivileges>>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IPrivileges>>;
    getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<IPrivileges>;
    create: (payload: IPrivileges) => Promise<any>;
    searchByName: <T extends keyof IPrivileges>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IPrivileges>>;
};
