import type { ITotp, IServerResp } from '../../interfaces';
export declare const totpService: {
    get(id: string): Promise<ITotp>;
    create(data: ITotp): Promise<IServerResp<ITotp>>;
    update(id: string, data: ITotp): Promise<IServerResp<ITotp>>;
    remove: (id: string) => Promise<IServerResp<boolean>>;
};
