import type { ACCEPT_MODE, ICertificateRequest, ICertificateRequestAccept, ICertificateRequestHistory, IEskOrganization } from '../../interfaces';
export declare const certificateRequestService: {
    /** Выгрузка реквизитов ЭП для проверки. */
    exportSign: (requestId: string) => Promise<{
        fileName: string;
        type: any;
        data: any;
    }>;
    /** Получить историю изменений заявления по его идентификатору. */
    getHistory: (id: string) => Promise<ICertificateRequestHistory[]>;
    /** Одобрить запрос на регистрацию сертификата ЭП. */
    accept: (acceptMode: ACCEPT_MODE, certRequestId: string, eskOrganization?: IEskOrganization | undefined, personEskId?: string | undefined, noteFromBank?: string | undefined) => Promise<ICertificateRequestAccept>;
    /** Отклонить запрос на регистрацию сертификата ЭП. */
    reject: (certRequestId: string, noteFromBank?: string | undefined) => Promise<any>;
    /** Повторить выгрузку. */
    repeatSend: (certRequestId: string) => Promise<ICertificateRequest>;
    getBySpoId: (spoId: string) => Promise<ICertificateRequest>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICertificateRequest>>;
    getCounter: (metaData: import("../../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<ICertificateRequest>;
    getByIds: (id: string, clientId: string) => Promise<ICertificateRequest>;
    search: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICertificateRequest>>;
    searchByName: <T extends keyof ICertificateRequest>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICertificateRequest>>;
};
