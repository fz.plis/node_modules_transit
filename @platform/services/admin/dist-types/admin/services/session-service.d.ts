import type { IServerResp, ISession } from '../../interfaces';
export declare const sessionsService: {
    remove: (id: string) => Promise<IServerResp<boolean>>;
    /**
     * Закрывает все активные сессии клиентских пользователей.
     *
     * @returns Массив закрытых сессиий клиентов.
     */
    closeActiveClientSessions(): Promise<ISession[]>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ISession>>;
    getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<ISession>;
    create: (payload: ISession) => Promise<any>;
    searchByName: <T extends keyof ISession>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ISession>>;
};
