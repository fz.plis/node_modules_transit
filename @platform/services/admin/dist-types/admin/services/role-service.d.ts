import type { IRole, IServerResp, IMetaData } from '../../interfaces';
export declare const ROLE_URL: string;
export declare const roleService: {
    update(data: IRole): Promise<IServerResp<IRole>>;
    delete(id: string): Promise<IServerResp<IRole>>;
    getClientRoles: (metaData: IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IRole>>;
    getBankRoles: (metaData: IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IRole>>;
    getList: (metaData: IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IRole>>;
    getCounter: ({ category, ...metaData }: IMetaData) => Promise<any>;
    get: (id: string) => Promise<IRole>;
    create: (payload: IRole) => Promise<any>;
    searchByName: <T extends keyof IRole>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IRole>>;
};
