import type { IBranchV2 } from '../../interfaces';
export declare const branchV2: {
    /** Получить все действующие филиалы. */
    getAllActiveFilials: () => Promise<string[]>;
    /** Получить действующий филиал по идентификатору филиала или подразделения. */
    findFilialById: (branchId: string) => Promise<IBranchV2>;
    /** Поиск действующего подразделения по коду подразделения. */
    findByAbsCode: (absCode: string) => Promise<IBranchV2>;
    /** Поиск действующих подразделений по списку кодов подразделений. */
    findByAbsCodes: (absCodes: string[]) => Promise<IBranchV2[]>;
    /** Поиск действующих подразделений по БИК. */
    findByBic: (bic: string) => Promise<IBranchV2[]>;
    /** Поиск действующего филиала или подразделения по наименованию. */
    findByName: (name: string) => Promise<IBranchV2[]>;
    /** Получить действующий филиал по его номеру в АБС. */
    findFilialByAbsNumber: (absNumber: string) => Promise<IBranchV2>;
    /** Поиск действующего филиала по БИК. */
    findFilialByBic: (bic: string) => Promise<IBranchV2>;
    /** Получить действующий головной офис. */
    getHeadUnit: () => Promise<IBranchV2>;
    /** Получить список действующих подразделений банка, находящихся в том же филиале, что и текущее подразделение. */
    getSameFilial: (branchId: string) => Promise<IBranchV2[]>;
    /** Синхронизировать филиалы со справочником КНОСИС. */
    sync: () => Promise<void>;
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranchV2>>;
    get: (id: string) => Promise<IBranchV2>;
    getCounter: (metaData: import("../../interfaces").IMetaData) => Promise<any>;
    searchByName: <T extends keyof IBranchV2>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranchV2>>;
    localityList(): Promise<string[]>;
};
