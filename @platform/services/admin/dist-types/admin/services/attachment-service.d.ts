import type { IAttachment, IFileUploadOptions, IDownloadedAttachment } from '../../interfaces';
export declare const attachmentService: {
    /**
     * Метод для скачивания файла в формате Base64.
     *
     * @param attachmentId Идентификатор файла.
     */
    downloadBase64: (attachmentId: string) => Promise<IDownloadedAttachment>;
    /**
     * Метод для загрузки вложения.
     *
     * @param file - Объекта файла.
     * @param file.onUploadProgress Обработчик для прогресса загрузки вложения.
     * @param file.onCancel Обработчик отмены.
     */
    upload: (file: File, { onUploadProgress, onCancel }?: IFileUploadOptions) => Promise<IAttachment>;
    getStatus: (attachmentId: string, onCancel?: ((cancel: import("axios").Canceler) => void) | undefined) => Promise<any>;
    checkUploadProgress: (attachmentId: string, options?: import("../../interfaces").ICheckUploadOptions) => Promise<IAttachment>;
    download: (attachmentId: string, accessToken: string) => Promise<IDownloadedAttachment>;
    downloadOneTimeData: (attachmentId: string, accessToken: string) => Promise<IDownloadedAttachment>;
};
