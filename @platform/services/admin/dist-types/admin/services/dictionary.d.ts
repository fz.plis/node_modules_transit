import type { IOkfs, IOkopf, ICurrency, IActivityType, IClientOfficial, ICountry, IAccountType, IBankClient, IGozContract, IServerResp, IDocumentType, ISearchF1DTO, IBranch } from '../../interfaces';
export declare const dictionaryService: {
    okopf: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkopf>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<IOkopf>;
        create: (payload: IOkopf) => Promise<any>;
        searchByName: <T extends keyof IOkopf>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkopf>>;
    };
    okfs: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkfs>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<IOkfs>;
        create: (payload: IOkfs) => Promise<any>;
        searchByName: <T_1 extends keyof IOkfs>(params: import("../../interfaces").ISearchByName, fieldName: T_1) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkfs>>;
    };
    currency: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICurrency>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<ICurrency>;
        create: (payload: ICurrency) => Promise<any>;
        searchByName: <T_2 extends keyof ICurrency>(params: import("../../interfaces").ISearchByName, fieldName: T_2) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICurrency>>;
    };
    activityType: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IActivityType>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<IActivityType>;
        create: (payload: IActivityType) => Promise<any>;
        searchByName: <T_3 extends keyof IActivityType>(params: import("../../interfaces").ISearchByName, fieldName: T_3) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IActivityType>>;
    };
    country: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICountry>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<ICountry>;
        create: (payload: ICountry) => Promise<any>;
        searchByName: <T_4 extends keyof ICountry>(params: import("../../interfaces").ISearchByName, fieldName: T_4) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICountry>>;
    };
    accountType: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccountType>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<IAccountType>;
        create: (payload: IAccountType) => Promise<any>;
        searchByName: <T_5 extends keyof IAccountType>(params: import("../../interfaces").ISearchByName, fieldName: T_5) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccountType>>;
    };
    gk: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IGozContract>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<IGozContract>;
        create: (payload: IGozContract) => Promise<any>;
        searchByName: <T_6 extends keyof IGozContract>(params: import("../../interfaces").ISearchByName, fieldName: T_6) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IGozContract>>;
    };
    clientOfficial: {
        getListClientOfficialForUser: (id: string) => Promise<IServerResp<IClientOfficial[]>>;
        delete: (id: string) => Promise<IServerResp<IClientOfficial>>;
        getUserOfficials: (userId: string) => Promise<IClientOfficial[]>;
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IClientOfficial>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<IClientOfficial>;
        create: (payload: IClientOfficial) => Promise<any>;
        searchByName: <T_7 extends keyof IClientOfficial>(params: import("../../interfaces").ISearchByName, fieldName: T_7) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IClientOfficial>>;
    };
    branch: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranch>>;
        get: (id: string) => Promise<IBranch>;
        searchByName: <T_8 extends keyof IBranch>(params: import("../../interfaces").ISearchByName, fieldName: T_8) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBranch>>;
        localityList(): Promise<string[]>;
        bicPage(metaData: import("../../interfaces").IMetaData): Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<string>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        create: (payload: IBranch) => Promise<any>;
    };
    branchV2: {
        getAllActiveFilials: () => Promise<string[]>;
        findFilialById: (branchId: string) => Promise<import("../../interfaces").IBranchV2>;
        findByAbsCode: (absCode: string) => Promise<import("../../interfaces").IBranchV2>;
        findByAbsCodes: (absCodes: string[]) => Promise<import("../../interfaces").IBranchV2[]>;
        findByBic: (bic: string) => Promise<import("../../interfaces").IBranchV2[]>;
        findByName: (name: string) => Promise<import("../../interfaces").IBranchV2[]>;
        findFilialByAbsNumber: (absNumber: string) => Promise<import("../../interfaces").IBranchV2>;
        findFilialByBic: (bic: string) => Promise<import("../../interfaces").IBranchV2>;
        getHeadUnit: () => Promise<import("../../interfaces").IBranchV2>;
        getSameFilial: (branchId: string) => Promise<import("../../interfaces").IBranchV2[]>;
        sync: () => Promise<void>;
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<import("../../interfaces").IBranchV2>>;
        get: (id: string) => Promise<import("../../interfaces").IBranchV2>;
        getCounter: (metaData: import("../../interfaces").IMetaData) => Promise<any>;
        searchByName: <T_9 extends keyof import("../../interfaces").IBranchV2>(params: import("../../interfaces").ISearchByName, fieldName: T_9) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<import("../../interfaces").IBranchV2>>;
        localityList(): Promise<string[]>;
    };
    bankClient: {
        getUserClients: (userId: string) => Promise<IBankClient[]>;
        block: (id: string) => Promise<IServerResp<IBankClient>>;
        unblock: (id: string) => Promise<IServerResp<IBankClient>>;
        update: (data: IBankClient) => Promise<IServerResp<IBankClient>>;
        searchF1: (data: ISearchF1DTO) => Promise<IServerResp<IBankClient[]>>;
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
        get: (id: string) => Promise<IBankClient>;
        searchByName: <T_10 extends keyof IBankClient>(params: import("../../interfaces").ISearchByName, fieldName: T_10) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
        searchByTextQuery: (params: import("../../interfaces").IClientSearch) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
        searchOgrn: (params: import("../../interfaces").ISearchByName) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
        searchRegNumber: (params: import("../../interfaces").ISearchByName) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IBankClient>>;
        getCounter: ({ category, ...metaData }: import("../../interfaces").IMetaData) => Promise<any>;
        create: (payload: IBankClient) => Promise<any>;
    };
    documentType: {
        getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IDocumentType>>;
        getCounter: (metaData: import("../../interfaces").IMetaData) => Promise<any>;
        get: (id: string) => Promise<IDocumentType>;
        getByIds: (id: string, clientId: string) => Promise<IDocumentType>;
        search: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IDocumentType>>;
        searchByName: <T_11 extends keyof IDocumentType>(params: import("../../interfaces").ISearchByName, fieldName: T_11) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IDocumentType>>;
    };
    fias: {
        search: (query: string, count?: number) => Promise<import("../../interfaces").IFiasResult[]>;
    };
};
