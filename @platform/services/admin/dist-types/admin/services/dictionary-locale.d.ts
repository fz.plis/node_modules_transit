export declare const dictionaryLocaleService: {
    get: () => Promise<Record<string, string>>;
};
