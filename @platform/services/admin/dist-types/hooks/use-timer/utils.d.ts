/**
 * Получить время работы таймера.
 *
 * @param startDateTime Начальная дата и время.
 * @param endDateTime Конечная дата и время.
 */
export declare const getTimerInterval: (startDateTime: number | string, endDateTime: number | string) => number;
