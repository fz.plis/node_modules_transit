import type { IOption } from '@platform/ui';
import type { CONSENT_TYPE } from '../constants';
import type { CRYPTO_PLUGIN_TYPE } from '../crypto';
import type { SCOPE } from '../utils';
import type { ISignatureDoc } from './actions';
import type { PRODUCT_TYPE } from './common';
import type { IRole } from './uaa';
export declare enum ACCOUNT_STATE {
    OPENED = "OPENED",
    ANNULLED = "ANNULLED",
    RESERVED = "RESERVED",
    CLOSED = "CLOSED"
}
/**
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=356368
 */
export declare enum DOCUMENT_TYPE_CODE {
    /**
     * Заявка на открытие первого счета.
     */
    ACC_OPEN_FIRST_REQ = "ACC_OPEN_FIRST_REQ",
    /**
     * Заявка на открытие дополнительного счета.
     */
    ACC_OPEN_SECOND_REQ = "ACC_OPEN_SECOND_REQ",
    /**
     * Заявка на резервирование счета.
     */
    ACC_RESERV_REQ = "ACC_RESERV_REQ",
    /**
     * Заявка на акцепт оферты к счету РКО.
     */
    OFFER_RKO_ACCEPTANCE_REQ = "OFFER_RKO_ACCEPTANCE_REQ",
    /**
     * Соглашение «ГПБ Бизнес-Онлайн».
     */
    OFFER_JOIN_GPB_BO1_REQ = "OFFER_JOIN_GPB_BO1_REQ",
    /**
     * Заявка на предоставление доступа и полномочий сотруднику в «ГПБ Бизнес-онлайн».
     */
    ADD_BLOCK_USER_GPBBO = "ADD_BLOCK_USER_GPBBO",
    /**
     * Конституция. Заявление 2.
     */
    OFFER_JOIN_GPB_BO2_REQ = "OFFER_JOIN_GPB_BO2_REQ",
    /**
     * Подключение КЭП.
     */
    OFFER_JOIN_CCERT = "OFFER_JOIN_CCERT",
    /**
     * Документ свободного формата.
     */
    FREE_FORMAT_DOC = "FREE_FORMAT_DOC",
    /**
     * Соглашение о подключении к ЛК УВЭД.
     */
    OFFER_JOIN_PO_UVED = "OFFER_JOIN_PO_UVED",
    /**
     * Предоставление/изменение доступа сотруднику к ЛК УВЭД.
     */
    ADD_BLOCK_USER_UVED = "ADD_BLOCK_USER_UVED",
    /**
     * Распоряжение на периодический перевод денежных средств.
     */
    ACC_RKO_CERVICE_PMT_ADD = "ACC_RKO_CERVICE_PMT_ADD",
    /**
     * Генерация НЭП.
     */
    OFFER_GEN_CERT = "OFFER_GEN_CERT",
    /**
     * Заявка на открытие дополнительного счета и заключение ДБС.
     */
    ACC_OPEN_SECOND_CONTRACT_REQ = "ACC_OPEN_SECOND_CONTRACT_REQ",
    /**
     * Соглашение о присоединении к онлайн-сервисам услуги Материальный пулинг.
     */
    OFFER_JOIN_PO_MP = "OFFER_JOIN_PO_MP",
    /**
     * Предоставление/изменение доступа к онлайн-сервисам услуги Материальный пулинг.
     */
    ADD_BLOCK_USER_MP = "ADD_BLOCK_USER_MP",
    /**
     * Заявка на автолизинг.
     */
    CAR_LEASING = "CAR_LEASING",
    /**
     * Заявка на корпоративный лизинг.
     */
    CORP_LEASING = "CORP_LEASING",
    /**
     * Запрос на проверку полномочий на заключение ДБС с открытием 2+ счетов.
     */
    GPB_DOG_ACC_OPEN_AUTH_CHECK = "GPB_DOG_ACC_OPEN_AUTH_CHECK",
    /**
     * Запрос на проверку полномочий на открытие счетов по существующему ДБС.
     */
    GPB_ACC_OPEN_AUTH_CHECK = "GPB_ACC_OPEN_AUTH_CHECK",
    /**
     * Запрос справок для ЮЛ.
     */
    REQUEST_FOR_ACCOUNT_INFO = "REQUEST_FOR_ACCOUNT_INFO",
    /**
     * Специальный код продукта для преобразования запроса в ЕСК-адаптере,
     * заменяется на `null`. Требуется для ситуаций, когда проверка ЕИО может
     * отличаться на стороне внешних сервисов из-за заполненности productType
     * (`[]` и `null | [null]` — это два разных кейса).
     */
    NULL_PRODUCT = "NULL_PRODUCT"
}
export declare enum OPEN_ACCOUNT_STATE {
    NONE = "NONE",
    DRAFT = "DRAFT",
    NEW = "NEW",
    DELETED = "DELETED",
    SIGNED = "SIGNED",
    DELIVERED = "DELIVERED",
    SIGN_VALID = "SIGN_VALID",
    SIGN_INVALID = "SIGN_INVALID",
    RECEIVED = "RECEIVED",
    DETAILS_ERROR = "DETAILS_ERROR",
    READY_FOR_PROCESSING = "READY_FOR_PROCESSING",
    EXPORTED_TO_ESK = "EXPORTED_TO_ESK",
    EXPORT_TO_ESK_ERROR = "EXPORT_TO_ESK_ERROR",
    EXECUTED_BY_ESK = "EXECUTED_BY_ESK",
    DENIED_BY_ESK = "DENIED_BY_ESK",
    EXPORTED_TO_RKO_F1 = "EXPORTED_TO_RKO_F1",
    EXPORT_TO_RKO_F1_ERROR = "EXPORT_TO_RKO_F1_ERROR",
    EXECUTED = "EXECUTED",
    DENIED = "DENIED",
    EXPORTED_TO_ELAR = "EXPORTED_TO_ELAR",
    EXPORT_TO_ELAR_ERROR = "EXPORT_TO_ELAR_ERROR",
    NOT_EXPORTED_TO_ELAR = "NOT_EXPORTED_TO_ELAR",
    DENIED_FOR_OPEN = "DENIED_FOR_OPEN",
    NOT_RECEIVED_BY_PROCESSING = "NOT_RECEIVED_BY_PROCESSING",
    RECEIVED_BY_PROCESSING = "RECEIVED_BY_PROCESSING",
    EXPORTED_ATTACHMENT = "EXPORTED_ATTACHMENT",
    EXPORT_ATTACHMENT_ERROR = "EXPORT_ATTACHMENT_ERROR",
    NEGATIVE_DECISION = "NEGATIVE_DECISION",
    EXPORTED_TO_ABS_FOR_OPEN = "EXPORTED_TO_ABS_FOR_OPEN",
    EXPORT_TO_ABS_FOR_OPEN_ERROR = "EXPORT_TO_ABS_FOR_OPEN_ERROR",
    PARTLY_EXECUTED = "PARTLY_EXECUTED",
    ACCEPTED = "ACCEPTED",
    RKO_F1_ACCOUNT_OPENED = "RKO_F1_ACCOUNT_OPENED",
    ECO_ACCOUNT_OPENING_ERROR = "ECO_ACCOUNT_OPENING_ERROR"
}
export declare enum CLAIMS_STATE {
    DRAFT = "DRAFT",
    NEW = "NEW",
    DELIVERED = "DELIVERED",
    DENIED = "DENIED",
    DETAILS_ERROR = "DETAILS_ERROR",
    EXECUTED = "EXECUTED",
    PARTLY_EXECUTED = "PARTLY_EXECUTED",
    RECEIVED = "RECEIVED",
    RECEIVED_BY_PROCESSING = "RECEIVED_BY_PROCESSING",
    EXPORTED_TO_ABS = "EXPORTED_TO_ABS",
    NOT_RECEIVED_BY_PROCESSING = "NOT_RECEIVED_BY_PROCESSING",
    EXPORT_TO_ABS_ERROR = "EXPORT_TO_ABS_ERROR",
    DELETED = "DELETED"
}
export declare enum PASSWORD_VALIDATION_ERRORS {
    PASSWORD_TOO_SHORT = "PASSWORD_TOO_SHORT",
    PASSWORD_TOO_LONG = "PASSWORD_TOO_LONG",
    PASSWORD_DOES_NOT_CONTAIN_LOWER_ALPHABET_SYMBOLS = "PASSWORD_DOES_NOT_CONTAIN_LOWER_ALPHABET_SYMBOLS",
    PASSWORD_DOES_NOT_CONTAIN_UPPER_ALPHABET_SYMBOLS = "PASSWORD_DOES_NOT_CONTAIN_UPPER_ALPHABET_SYMBOLS",
    PASSWORD_DOES_NOT_CONTAIN_NUMERIC_SYMBOLS = "PASSWORD_DOES_NOT_CONTAIN_NUMERIC_SYMBOLS",
    PASSWORD_DOES_NOT_CONTAIN_SPECIAL_SYMBOLS = "PASSWORD_DOES_NOT_CONTAIN_SPECIAL_SYMBOLS",
    PASSWORD_CONTAINS_TOO_FEW_DIFFERENT_SYMBOL_GROUPS = "PASSWORD_CONTAINS_TOO_FEW_DIFFERENT_SYMBOL_GROUPS",
    PASSWORD_CONTAINS_UNKNOWN_SYMBOLS = "PASSWORD_CONTAINS_UNKNOWN_SYMBOLS",
    PASSWORD_MATCHES_LOGIN = "PASSWORD_MATCHES_LOGIN"
}
export interface IBaseEntity {
    id: string;
}
export interface IBaseDocument extends IBaseEntity {
    number: string;
    docDate: string;
}
export interface ISimpleDictionary<T = number> {
    id: string;
    code: T;
    name: string;
}
export declare type IActivityType = ISimpleDictionary;
export declare type IOkopf = ISimpleDictionary<string>;
export interface IOkfs extends ISimpleDictionary<string> {
    parentCode: string;
}
/**
 * BLOCKED - Заблокирован;
 * UNBLOCKED - Незаблокирован.
 */
export declare enum BLOCK_STATE {
    UNBLOCKED = "UNBLOCKED",
    BLOCKED = "BLOCKED"
}
/**
 * ACTIVE - Активный
 * POTENTIAL - Потенциальный.
 */
export declare enum BANK_CLIENT_STATUS {
    ACTIVE = "ACTIVE",
    POTENTIAL = "POTENTIAL"
}
/**
 * UL - Юридическое лицо;
 * IP - Индивидуальный предприниматель;
 * FL - Физическое лицо, занимающееся частной практикой.
 */
export declare enum BANK_CLIENT_TYPE {
    UL = "UL",
    IP = "IP",
    FL = "FL"
}
/**
 * Вид деятельности.
 */
export declare enum ACTIVITY_TYPE {
    /**
     * Находящиеся в федеральной собственности.
     */
    FEDERAL = 1,
    /**
     * Государственной (кроме федеральной).
     */
    GOS = 2,
    /**
     * Негосударственной.
     */
    NON_GOS = 3,
    /**
     * Индивидуальный предприниматель-исполнитель по ГК.
     */
    IP = 4
}
export interface ICountry {
    code: string;
    id: string;
    name: string;
}
export declare enum SYSTEM_SOURCES {
    BSS_DBO_320 = "BSS_DBO_320",
    ECO = "ECO",
    RKO_F1 = "RKO_F1",
    ESK = "ESK",
    TESSA = "TESSA"
}
export interface BankClientCustomerId {
    customerId?: string;
    source?: SYSTEM_SOURCES;
}
/**
 * Соглашение Клиент Банка.
 */
export interface IBankClientAgreement {
    id: string;
    createdAt: string;
    bankClientId: string;
    offerDate: string;
    offerNumber: string;
    productType: PRODUCT_TYPE;
    remoteSource: SYSTEM_SOURCES;
    hasAuthorizedPerson: boolean;
    /** Дата расторжения соглашения. */
    breakDate?: string;
}
export interface IBankClient {
    /**
     * ID клиентской организации в банке (АБС).
     */
    absId: string;
    /**
     * Идентификаторs клиента в системах банка.
     */
    bankClientCustomerIds?: BankClientCustomerId[];
    /**
     * Дата и время создания записи.
     */
    createdAt?: string;
    /**
     * Система создавшая запись.
     */
    createdBySource?: SYSTEM_SOURCES;
    /**
     * Адрес электронной почты (может быть несколько).
     */
    emails?: string[];
    /**
     * Уникальный GUID клиента во внешней системе (Поле clientGUID в ответе F1).
     */
    externalClientGuid?: string;
    id: string;
    /**
     * Признак подключения организации к ДБО.
     */
    isConnectedToDbo?: boolean;
    /**
     * Информация о запрете дебетовых операций.
     */
    isDebitOperationsForbidden?: boolean;
    /**
     * Юридический адрес Клиента.
     */
    legalAddress?: string;
    /**
     * Контактный телефон.
     */
    phone?: string;
    /**
     * Почтовый адрес Клиента.
     */
    postAddress?: string;
    clientId: number;
    isResident: boolean;
    clientType: BANK_CLIENT_TYPE;
    fullName: string;
    shortName: string;
    innKio: string;
    /**
     * ACTIVE - Активный;
     * POTENTIAL - Потенциальный.
     */
    status: BANK_CLIENT_STATUS;
    additionalInfo: string;
    blockState: BLOCK_STATE;
    okopf: string;
    okfs: string;
    activityType: ACTIVITY_TYPE;
    /**
      Соглашения клиента.
     */
    clientAgreements?: IBankClientAgreement[];
    /**
     * Обязательно если isResident = true.
     */
    ogrnOgrip: string;
    /**
     * Обязательно если isResident = true и clientType = UL.
     */
    kpp: string;
    /**
     * Обязательно если isResident = false.
     */
    regNumber: string;
    /**
     * Обязательно если isResident = false.
     */
    internationalName: string;
    /**
     * Обязательно если isResident = false.
     */
    tin: string;
    country: ICountry;
    /**
     * Признак подписания конституции.
     */
    hasConstitution: boolean;
    /**
     * Сокращенное наименование.
     */
    abbreviationName?: string;
}
export interface ISystemInfo {
    acceptedAt: string;
    branchResponsibleName: string;
    branchResponsibleUserId: string;
    clientResponsibleName: string;
    clientResponsibleUserId: string;
    comment: string;
    createdAt: string;
    creatorId: string;
    executedAt: string;
}
export interface IAccount {
    id: string;
    accountNumber: string;
    transitAccountNumber?: string;
    state: ACCOUNT_STATE;
    startDate: string;
    endDate: string;
    annulledDate: string;
    openDate: string;
    openingRequestId?: string;
    documentPackageRequestId?: string;
    accountType: IAccountType;
    currency: ICurrency;
    branch: IBranch;
    bankClient: IBankClient;
    requestId: string;
    gozContract: IGozContract;
    /** Ссылка на заявку на открытие доп. Счета и заключение ДБС. */
    openingContractRequestId?: string;
}
export interface IAccountV2 {
    /** Идентификатор. */
    id: string;
    /** Номер. */
    accountNumber: string;
    /** Тип счета. */
    accountType: IAccountType;
    /** Признак учета на Балансе ГО. */
    accountingBalanceHeadUnit: boolean;
    /** Дата аннулирования. */
    annulledDate: string;
    /** Огранизация. */
    bankClient: IBankClient;
    /** Подразделение. */
    branch: IBranchV2;
    /** Филиал. */
    filial: IBranchV2;
    /** ГО. */
    headUnit: IBranchV2;
    /** Дата закрытия. */
    closeDate: string;
    /** Валюта. */
    currency: ICurrency;
    /** Ссылка на идентификатор ОНБ. */
    documentPackageRequestId?: string;
    /** Дата окончания резервирования. */
    endDate: string;
    /** */
    gozContract: IGozContract;
    /** Дата открытия. */
    openDate: string;
    /** Ссылка на заявку на открытие доп. Счета и заключение ДБС. */
    openingContractRequestId?: string;
    /** */
    openingRequestId?: string;
    /** Id заявки на резервирование (не точно). */
    requestId: string;
    /** Дата начала резервирования. */
    startDate: string;
    /** Статус. */
    state: ACCOUNT_STATE;
    /** Транзитный номер. */
    transitAccountNumber?: string;
    /** Идентификатор пользователя. */
    userId: string;
}
export interface IGozContract {
    date: string;
    id: string;
    identify: string;
    number: string;
    subDate: string;
    subNumber: string;
}
/**
 * Тип счета.
 */
export declare enum ACCOUNT_TYPE {
    /**
     * Рассчетный.
     */
    CHECKING = 1,
    /**
     * Счет исполнителя ГК.
     */
    GOZ = 2,
    /**
     * Счет головного исполнителя ГК.
     */
    MAIN_GOZ = 3,
    /**
     * Счёт участника закупок.
     */
    PARTICIPANT = 4
}
export interface IAccountType {
    id: string;
    code: ACCOUNT_TYPE;
    description: string;
    fullDescription: string;
}
export interface ICurrency {
    id: string;
    numericCode: string;
    code: string;
    name: string;
}
export interface IBranch {
    absCode: string;
    absNumber: string;
    id: string;
    name: string;
    fullName: string;
    /** Название филиала для платежных документов. */
    docsName: string;
    address: string;
    phone: string;
    schedule: string;
    latitude: string;
    longitude: string;
    locality: string;
    number: string;
    requisites: IBranchRequisites;
    canOpenAccounts: boolean;
    canOpenGozAccounts: boolean;
}
export interface IBranchRequisites {
    bic: string;
    correspondentAccount: string;
    inn: string;
    location: string;
    name: string;
    number: string;
    ogrn: string;
    okpo: string;
    oktmo: string;
}
export interface IOrganizationPdfData {
    shortName: string;
    inn: string;
}
export interface IAccountsInfo {
    balancePosition: string;
    balancePositionDescription: string;
    contractDate: string;
    contractNumber: string;
    count: number;
    currencyCode: string;
    currencyId: string;
    currencyNumCode: string;
    govContractDate: string;
    govContractId: string;
    govContractNumber: string;
    shortName: string;
    typeCode: ACCOUNT_TYPE;
}
export interface IBranchOfficial {
    id: string;
    blockState: BLOCK_STATE;
    position: string;
    branch: IBranch;
    userId: string;
}
export interface IClientOfficial {
    id: string;
    blockState: BLOCK_STATE;
    position: string;
    client: IBankClient;
    userId: string;
}
export interface IBranchInfo {
    bic: string;
    id: string;
    name: string;
    number: string;
}
export interface IClientInfo {
    activityType?: number;
    agreed: boolean;
    clientId: number;
    clientType: BANK_CLIENT_TYPE;
    clientUuid: string;
    countryId: string;
    fullName: string;
    inn: string;
    internationalName: string;
    isResident: boolean;
    kpp: string;
    ogrn: string;
    okfs: string;
    okopf: string;
    regNumber: string;
    shortName: string;
    tin: string;
}
export interface IReservedAccountInfo {
    accountNumber: string;
    anulledDate: string;
    closedDate: string;
    endDate: string;
    id: string;
    openDate: string;
    requestId: string;
    startDate: string;
    state: ACCOUNT_STATE;
    transitAccountNumber: string;
}
export interface IAccountClaims {
    docDate: string;
    id: string;
    number: number;
    status: string;
    system: ISystemInfo;
    userId: string;
    userLastName: string;
    client: IClientInfo;
    accountsInfo: IAccountsInfo[];
    branch: IBranchInfo;
    accounts: string[];
    uuid?: number;
    createdBy?: UserType;
    version: number;
}
export declare enum UserType {
    BANK = "BANK",
    CLIENT = "CLIENT",
    TECHNICAL = "TECHNICAL"
}
export declare enum USER_ROLE {
    CLIENT_USER = "CLIENT_USER",
    BANK_ADMIN = "BANK_ADMIN",
    BANK_SECURITY_ADMIN = "BANK_SECURITY_ADMIN",
    BANK_USER = "BANK_USER",
    BANK_BUSINESS_ADMIN = "BANK_BUSINESS_ADMIN",
    DDKS_EMPLOYEE = "DDKS_EMPLOYEE",
    CLIENT_CRT_SENDER = "CLIENT_CRT_SENDER",
    CLIENT_USER_RESTRICTED = "CLIENT_USER_RESTRICTED"
}
export declare enum VERIFICATION_STATE {
    VERIFIED = "VERIFIED",
    UNVERIFIED = "UNVERIFIED"
}
export declare enum MFA_CHAINS_STAGES {
    OTP = "OTP",
    CERTIFICATE = "CERTIFICATE",
    TOTP = "TOTP",
    PASSWORD = "PASSWORD"
}
export declare enum RESOURCE_TYPE {
    UAA = "UAA",
    SERVICE = "SERVICE"
}
export interface IUser {
    id: string;
    userId: number;
    firstName: string;
    secondName: string;
    patronymic: string;
    userType: UserType;
    login: string;
    password: string;
    verificationState: VERIFICATION_STATE;
    email: string;
    phoneNumber: string;
    isDataUsageApproved: boolean;
    lastPasswordChange: string;
    lastLogin: string;
    blockState: BLOCK_STATE;
    branchOfficials: IBranchOfficial[];
    clientOfficials: IClientOfficial[];
    roles: IRole[];
}
export interface IProfile {
    id: string;
    login: string;
    userType: UserType;
    roles: USER_ROLE[];
}
/**
 * Согласие пользователя.
 */
export interface IUserConsent extends IConsentRequest {
    /**
     * Дата создания.
     */
    createdAt: string;
}
export interface IClientProfile extends IProfile {
    firstName: string;
    phoneNumber: string;
    secondName: string;
    patronymic?: string;
    email?: string;
    showNotification: string;
    shouldNotification: boolean;
    phoneConfirmed: boolean;
    emailConfirmed: boolean;
    cryptoPlugin: CRYPTO_PLUGIN_TYPE;
    /**
     * Согласия пользователя.
     */
    userConsents: IUserConsent[];
}
export interface IUserInfo {
    email: string;
    familyName: string;
    firstName: string;
    login: string;
    middleName: string;
    password: string;
    phoneNumber: string;
}
/**
 * Интерфейс запроса согласия.
 */
export interface IConsentRequest {
    /**
     * Тип согласия.
     */
    consentType: CONSENT_TYPE;
    /**
     * Версия согласия.
     */
    consentVersion: number;
}
export interface ICreateUserDto {
    email: string;
    firstName: string;
    login: string;
    password: string;
    patronymic: string;
    phoneNumber: string;
    secondName: string;
}
export interface ISession {
    createdAt: string;
    expiredAt: string;
    id: string;
    login: string;
    userType: UserType;
}
export interface ITotp {
    externalId: string;
    userId: string;
}
export interface IEgrulInfo {
    address: string;
    email: string;
    fullName: string;
    inn: string;
    kpp: string;
    ogrn: string;
    okopf: string;
    okved: string;
    registerDate: string;
    shortName: string;
}
export declare enum EGRUL_SEARCH_TYPE {
    INN = "INN",
    OGRN = "OGRN"
}
export interface IBalancePosition {
    balancePosition: string;
    description: string;
}
export interface IAbsMeta {
    accountTypeCode: number;
    activityTypeCode?: number;
    branchId?: string;
    clientType: string;
    isResident: boolean;
    okfsCode?: string;
    okopfCode?: string;
}
export interface IBalancePositionResponse {
    responses: IBalancePosition[];
    isDone: boolean;
}
export interface ICheckClientMeta {
    inn: string;
    kpp: string;
    name: string;
    ogrn: string;
    accountType: ACCOUNT_TYPE;
}
export interface IOpeningAccountInfo {
    balancePosition: string;
    balancePositionDescription: string;
    count: number;
    currencyCode: string;
    currencyNumericCode: string;
}
export interface IOpeningReservedAccountInfo {
    number: string;
}
export interface IOpenedAccount {
    id: string;
}
export interface IDocumentAttachment {
    accessToken?: string;
    attachmentId?: string;
    fileHash?: string;
    fileName?: string;
    fileSize?: number;
}
export interface ISignDocumentAttachment {
    accessToken?: string;
    attachmentSignId?: string;
    signFileHash?: string;
    signFileName?: string;
    signFileSize?: number;
}
export interface IOpenAccountInfo {
    accountTypeCode: number;
    accountTypeDescription: string;
    document: {
        applicationNumber: string;
        attachment: IDocumentAttachment;
    };
    govContractCustomerInfo: {
        accountNumber?: string;
        inn?: string;
        name?: string;
    };
    govContractInfo: {
        amount?: string;
        contractId?: string;
        date?: string;
        number?: string;
    };
    statementType: STATEMENT_TYPE;
    id: string;
    needConnectMobileBank: boolean;
    openingAccountsInfos: IOpeningAccountInfo[];
    reservedAccounts: IOpeningReservedAccountInfo[];
}
export interface IOpenAccount {
    bankClient?: IBankClientInfo;
    branch: IBranchDetailsInfo;
    confirmNoChanges: boolean;
    commissionAccount: ICommissionInfo;
    commentForClient?: string;
    contract: IContractInfoDetails;
    date: string;
    id: string;
    errors: string;
    isRulesJoined: boolean;
    number: string;
    opopAccount: IOpopAccount;
    status: OPEN_ACCOUNT_STATE;
    userId: string;
    accountInfos: IOpenAccountInfo[];
    openedAccounts?: Array<{
        id: string;
    }>;
    version: number;
}
export interface IBankClientInfo {
    clientId: string;
    extId: string;
    innKio: string;
    kpp: string;
    name: string;
    ogrn: string;
    resident: boolean;
    loginCertificateId: string;
    absId: string;
}
export interface IBranchDetailsInfo {
    bic: string;
    id: string;
    name: string;
    correspondentAccount: string;
}
export interface ICommissionInfo {
    number: string;
}
export interface IContractInfoDetails {
    absId: string;
    date: string;
    number: string;
    isCommissionDebitingRequired: boolean;
}
export interface IOpopAccount {
    number: string;
}
export interface IContract {
    id: string;
    date: string;
    number: string;
    accounts: Array<{
        id: string;
        accountNumber: string;
    }>;
    subsidiaryNumber: string;
    subsidiaryName: string;
    subsidiaryId: string;
    subsidiaryBic: string;
}
export interface IFileInfo {
    name: string;
}
export interface IClientReservedAccount {
    accountTypeCode: number;
    accountTypeDescription: string;
    currencyCode: string;
    currencyNumericCode: string;
    branchAbsNumber: string;
    account: string;
}
export interface IClientKopops {
    approvers: string[];
    account: string;
}
export interface IClientcontract {
    number: string;
    date: string;
    branchId: string;
    accountTypeCode: number;
    branchAbsNumber: string;
    branchBic: string;
    branchName: string;
    branchCorAccount: string;
    commissionAccounts: string[];
    kopops: IClientKopops[];
    allowOpenAdditionalAccount: boolean;
    isCommissionDebitingRequired: boolean;
    rkoId: number;
}
export interface IClientContractsResponse {
    clientId: string;
    clientExtId: string;
    reservedAccounts: IClientReservedAccount[];
    contracts: IClientcontract[];
    responseSuccess: boolean;
}
export declare enum STATEMENT_TYPE {
    STANDARD = "STANDARD",
    EXTENDED = "EXTENDED"
}
/**
 * Дайджест.
 */
export interface IDigest {
    /**
     * Подписываемые поля
     * Не уверен что понадобится для фронта.
     */
    signFields: string[] | string;
    /**
     * Хеш подписываемых данных, который собственно и подписываем.
     */
    value: string;
    version?: number;
}
/**
 * Данные криптопрофиля.
 */
export interface ISignProfile {
    name: string;
    thumbprint: string;
}
/**
 * Данные подписи для отправки на бек.
 */
export interface ISign {
    /**
     * Id документа.
     */
    documentId: string;
    /**
     * Подпись.
     */
    signature: string;
    certificateId: string;
    digest: string;
    signTypeId: string;
}
/**
 * Данные о подписи.
 */
export interface ISignature {
    /**
     * Дата подписи.
     */
    signedAt?: string;
    /**
     * Сама подпись.
     */
    signature: string;
    /**
     * Криптопрофиль, которым сформирована подпись.
     */
    profile?: ISignProfile;
}
/**
 * Результат подписи.
 */
export interface ISignResult<T> {
    /**
     * Данные о сформированной подписи.
     */
    signature: ISignature;
    /**
     * Подписанный документ.
     */
    document: T;
}
/** Данные для проверки подписи. */
export interface ISignVerify {
    /**
     * Дайджест документа.
     */
    digest: IDigest;
    /**
     * Данные подписи.
     */
    signature: ISignature;
}
/**
 * Данные для экспорта.
 */
export interface IExportRequest {
    docIds: string[];
    format: string;
    id?: string;
    splitReport: boolean;
}
export declare enum SIGN_VERIFY_ERROR {
    DOCUMENT_MODIFIED = 0,
    INVALID_SIGNATURE = 1
}
export interface IPrintResponseDoc {
    content: string;
    fileName: string;
}
export interface IPrintResponse {
    list: IPrintResponseDoc[];
}
export interface IExportResponse {
    fileName: string;
    type: string;
    data: any;
}
/**
 * Сущность записи истории изменений.
 */
export interface IHistoryResponse {
    /**
     * Дата и время присвоения статуса документу.
     */
    changedIn: string;
    /**
     * Ссылка на сущность документа.
     */
    documentId: string;
    /**
     * Идентификатор записи истории.
     */
    id: string;
    /**
     * Статус документа.
     */
    status: string;
    /**
     * Ссылка на сущность “Пользователь”.
     */
    userId: string;
    /**
     * Имя пользователя.
     */
    userName: string;
    /**
     * Комментарий для Клиента.
     */
    commentForClient?: string;
    /**
     * Комментарий для Банка.
     */
    commentForBank?: string;
    /**
     * Комментарий для Клиента (в некоторых сервисах именование поле для комментария отличается).
     */
    infoForClient?: string;
    /**
     * Комментарий для Банка (в некоторых сервисах именование поле для комментария отличается).
     */
    infoForBank?: string;
    /**
     * Тип пользователя.
     */
    userType?: UserType;
}
export declare enum DOCUMENT_CLASS {
    ORIGINAL_DOCUMENT = "ORIGINAL_DOCUMENT",
    PRINTING_FORM_BANK = "PRINTING_FORM_BANK"
}
export declare enum DOCUMENT_CODE {
    IDENTITY_DOCUMENT = "IDENTITY_DOCUMENT",
    MIGRATION_CARD = "MIGRATION_CARD",
    GOZ = "GOZ",
    CONSTITUENT_DOCUMENT = "CONSTITUENT_DOCUMENT",
    LICENSE = "LICENSE",
    HEAD_APPOINTMENT = "HEAD_APPOINTMENT",
    RIGHT_SIGN_DOCUMENT = "RIGHT_SIGN_DOCUMENT",
    WARRANT_SIGNERS = "WARRANT_SIGNERS",
    WARRANT_SIGNER_PERSON = "WARRANT_SIGNER_PERSON",
    OTHER_DOCUMENTS = "OTHER_DOCUMENTS"
}
export interface IDocumentType {
    id: string;
    usedInOnboarding: boolean;
    documentClass: DOCUMENT_CLASS;
    name: string;
    shortName: string;
    mask: string;
    needUpload: true;
    paperworkRequirements: string;
    fkdoNames: string;
    maxAttachmentFileSize: number;
    priority: number;
    code: DOCUMENT_CODE;
}
export interface ISearchF1DTO {
    clientAddNumber?: string;
    clientGUID?: string;
    clientINN?: string;
    clientName?: string;
    clientOGRN?: string;
    clientRegNum?: string;
}
export interface IBindF1DTO {
    client: IBankClient;
    userId: string;
}
export interface IFiasEntity {
    fias_code?: string | null;
    fias_level?: number;
    postal_code?: string;
    region_with_type?: string;
    city_with_type?: string;
    settlement_with_type?: string;
    street_with_type?: string;
    house?: string;
    block?: string;
    flat?: string;
    area?: string;
    fias_id?: string;
    country?: string;
    country_iso_code?: string;
    region_kladr_id?: string;
    area_kladr_id?: string;
    settlement_kladr_id?: string;
    city_kladr_id?: string;
}
export interface IFiasResult {
    value: string;
    unrestricted_value: string;
    data: IFiasEntity;
}
export interface IFileData {
    content: string;
    fileName: string;
}
/**
 * Интерфейс ошибок авторизации.
 */
export interface IErrorAuthorization {
    /**
     * Код ошибки.
     */
    ErrorCode: string;
    /**
     * Текст ошибки.
     */
    ErrorText: string;
}
/**
 * Интерфейс удотстоверения личности.
 */
export interface IIdentityCard {
    /**
     * Код подразделения.
     */
    authorityCode: string;
    /**
     * Срок действия.
     */
    expiresDate: string;
    /**
     * Дата выдачи.
     */
    issueDate: string;
    /**
     * Место выдачи.
     */
    issuedBy: string;
    /**
     * Номер.
     */
    number: string;
    /**
     * Серия.
     */
    series: string;
    /**
     * Тип удостоверения.
     */
    type: string;
}
/**
 * Интерфейс представителя организации.
 */
export interface IOrganizationPerson {
    /**
     * Идентификатор в Классификаторе ЕСК.
     */
    classifierId: string;
    /**
     * Имя.
     */
    firstName: string;
    /**
     * Полное имя.
     */
    fullName: string;
    /**
     * Удостоверение личности.
     */
    identityCard: IIdentityCard;
    /**
     * Результат проверки паспорта представителя.
     */
    identityCardStatus: 'INVALID' | 'UNDEFINED' | 'VALID';
    /**
     * Должность.
     */
    jobTitle: string;
    /**
     * Фамилия.
     */
    lastName: string;
    /**
     * Отчество.
     */
    middleName: string;
    /**
     * Статус комплекта персональных данных представителя.
     */
    personalDataStatus: 'COMPLETE' | 'INCOMPLETE';
}
/**
 * Интерфейс организации из ЕСК.
 */
export interface IEskOrganization {
    /**
     * Local ID представителя в ЕСК.
     */
    clientPrivEskId: string;
    /**
     * Регистрационный номер компании (РНК).
     */
    clientRegNum: string;
    /**
     * GUID организации.
     */
    companyGUID: string;
    /**
     * Признак успеха авторизации.
     */
    isAuthorizationSuccess: boolean;
    /**
     * Признак успеха авторизации представителя.
     */
    isPersonSuccess: boolean;
    /**
     * Адрес регистрации.
     */
    legalAddress: string;
    /**
     * Наименование организации.
     */
    name: string;
    /**
     * Дата окончания действия полномочий.
     */
    validityTermDate: string;
    /**
     * Ошибки авторизации.
     */
    errors: IErrorAuthorization[];
    /**
     * Представители организации.
     */
    persons: IOrganizationPerson[];
}
/**
 * Блок отображаемых данных на форме подписи.
 */
export interface IUniversalSignDetails {
    /**
     * Наименование блока подписываемых данных.
     */
    label: string;
    /**
     * Подписываемые поля.
     */
    signFieldsViewList: IOption[];
}
/**
 * Тип подписываемого документа.
 */
export declare enum SIGN_DATA_TYPE {
    DOCUMENT = "DOCUMENT",
    ATTACHMENT = "ATTACHMENT"
}
/**
 * Подписываемая сущность для компонента универсальной подписи.
 */
export interface IUniversalSignEntity {
    /**
     * Локальный идентификатор документа для связи с исходящими параметрами.
     */
    documentId: string;
    /**
     * Набор подписываемых данных.
     */
    signDataList: IUniversalSignData[];
}
/**
 * Подписываемые данные для компонента универсальной подписи.
 */
export interface IUniversalSignData {
    /**
     * Идентификатор набора подписываемых данных для связи с исходящими параметрами функции. Например, для ДСФ:
        Документ ДСФ - заполняется идентфикатором документа;
        Вложение в ДСФ - заполняется идентфикатором вложения.
     */
    id: string;
    /**
     * Наименование подписываемых данных для отображения в диалоге подписи.
     */
    label: string;
    /**
     * Данные, которые будут подписаны ЭП.
     */
    digest: string;
    /**
     * Тип подписываемых данных: документ, вложение и т.п.
     */
    type?: SIGN_DATA_TYPE;
    /**
     * Блок отображаемых данных на форме подписи.
     */
    signDataViewList: IUniversalSignDetails[];
}
/**
 * Общий код завершения подписи документов.
 */
export declare enum SIGN_RESULT_CODE {
    /**
     * Операция подписи хотя бы одного документа завершилась корректно.
     */
    SUCCESS = "SUCCESS",
    /**
     * Пользователь отказался от установки подписи
     * или
     * возникла ошибка при подписи одного из наборов данных документа.
     */
    ERROR = "ERROR"
}
/**
 * Подписанная сущность.
 */
export interface IUniversalSignDataResult {
    /**
     * Локальный идентификатор набора подписываемых данных для связи с входящими параметрами.
     */
    id: string;
    /**
     * Подписанные данные.
     */
    digest: string;
    /**
     * Значение подписи.
     */
    signature: string;
    /**
     * Дата и время подписи.
     */
    signDate: string;
    /**
     * Тип сущности. Нужен на стороне прикладного стрима для понимания как сохранять результат подписи.
     */
    type?: SIGN_DATA_TYPE;
}
/**
 * Результат подписи документа.
 */
export interface IUniversalSignResultEntity {
    /**
     * Идентификатор документа для связи с входящими параметрами.
     */
    documentId: string;
    /**
     * Код завершения.
     */
    resultCode: SIGN_RESULT_CODE;
    /**
     * Если <Код завершения> не равен 0, то заполняется текстом ошибки, иначе не заполняется.
     */
    error?: string;
    /**
     * Набор подписанных данных.
     */
    signDataResult?: IUniversalSignDataResult[];
}
/**
 * Результат подписи для компонента универсальной подписи.
 */
export interface IUniversalSignResultSummary {
    /**
     * Общий код завершения.
     */
    resultCode: SIGN_RESULT_CODE;
    /**
     * Если <Общий код завершения> не равен 0, то заполняется текстом ошибки, иначе не заполняется.
     */
    error?: string;
    /**
     * Если <Общий код завершения> не равен 0, то заполняется локальным идентификатором сертификата из входящих параметров функции,
     * использованный для подписи, иначе не заполняется.
     */
    certificateId?: string;
    /**
     * Набор обработанных документов.
     */
    sign: IUniversalSignResultEntity[];
}
/**
 * Тип филиала.
 */
export declare enum BRANCH_TYPE {
    /**
     * Филиал.
     */
    BRANCH = "BRANCH",
    /**
     * Подразделение.
     */
    SUBDIVISION = "SUBDIVISION"
}
/**
 * Реквизиты подразделения банка.
 */
export interface IRequisitesV2 {
    /**
     * БИК филиала.
     */
    bic: string;
    /**
     * Корсчет филиала.
     */
    correspondentAccount: string;
    /**
     * ИНН филиала.
     */
    inn: string;
    /**
     * КПП филиала.
     */
    kpp: string;
    /**
     * Населенный пункт.
     */
    location: string;
    /**
     * ОГРН филиала.
     */
    ogrn: string;
    /**
     * ОКПО филиала.
     */
    okpo: string;
    /**
     * ОКТМО филиала.
     */
    oktmo: string;
}
/**
 * Подразделение банка v2.
 */
export interface IBranchV2 {
    /**
     * АБС Код подразделения.
     * */
    absCode: string;
    /**
     * Код филиала в РКО Ф1.
     */
    absNumber: string;
    /**
     * Адрес подразделения.
     */
    address: string;
    /**
     * Услуги подразделения.
     */
    branchServices: string;
    /**
     * Тип записи.
     */
    branchType: BRANCH_TYPE;
    /**
     * Дата и время создания записи.
     */
    createdAt: string;
    /**
     * Название филиала для платежных документов.
     */
    docsName: string;
    /**
     * Код филиала в ЕСК.
     */
    filialEskCode: string;
    /**
     * Название филиала.
     */
    filialName: string;
    /**
     * Идентификатор записи.
     */
    id: string;
    /**
     * КПП филиала (для указания в счет-фактурах).
     */
    invoiceKpp: string;
    /**
     * Признак удаления.
     */
    isDeleted: boolean;
    /**
     * Наименование судебной инстанции.
     */
    judicialInstance: string;
    /**
     * Идентификатор филиала в Мастер-Системе (КНОСИС).
     */
    knosisBranchId: number;
    /**
     * Идентификатор подразделения в Мастер-Системе (КНОСИС).
     */
    knosisSubdivisionId: number;
    /**
     * Геолокация. Координата по широте.
     */
    latitude: number;
    /**
     * Геолокация. Координата по долготе.
     */
    longitude: number;
    /**
     * Наименование подразделения.
     */
    name: string;
    /**
     * Номер подразделения.
     */
    number: string;
    /**
     * ОКАТО филиала.
     */
    okato: string;
    /**
     * Идентификатор филиала.
     */
    parentBranch: string;
    /**
     * Телефоны подразделения.
     */
    phone: string;
    /**
     * Реквизиты подразделения банка.
     */
    requisites: IRequisitesV2;
    /**
     * График работы подразделения.
     */
    schedule: string;
    /**
     * SWIFT филиала.
     */
    swift: string;
    /**
     * Переведен на баланс ГО.
     */
    transferredBalanceHeadUnit?: boolean;
    /**
     * Дата и время обновления записи.
     */
    updatedAt: string;
    /**
     * Разница во времени с UTC.
     */
    utcOffset: number;
    /**
     * Идентификатор ФИАС города или нас пункта.
     */
    settlementOrCityFiasId?: string;
}
/**
 * Справочник городов выездных менеджеров.
 */
export interface CityVisitingManagerDto {
    /**
     * Текст описания зоны доступности.
     */
    zoneDescription?: string;
    /**
     * Сервис доступен в ОНБ.
     */
    availableInDocPack: boolean;
}
/**
 * Тип вложения.
 */
export declare enum ATTACHMENT_TYPES {
    PDF = "PDF",
    DOC = "DOC",
    DOCX = "DOCX"
}
/**
 * Подписанное вложение.
 */
export interface ISignedAttachment extends IDocumentAttachment {
    /**
     * Идентификатор.
     */
    id: string;
    /**
     * Тип вложения.
     */
    contentType: string;
    /**
     * Расширение файла.
     */
    extension: ATTACHMENT_TYPES;
    /**
     * Ид владелеца.
     */
    owner: string;
    /**
     * Ид автора.
     */
    author: string;
    /**
     * ФИО автора.
     */
    authorFio: string;
    /**
     * Дата прикрепления.
     */
    attachedAt: string;
    /**
     * Список подписей.
     */
    signatures: ISignatureDoc[];
    /**
     * Место хранения файла.
     */
    side: SCOPE;
}
/**
 * Свойства подписанного документа.
 */
export interface IBaseSignedDocument extends IBaseEntity {
    /**
     * Список подписей.
     */
    signatures: ISignatureDoc[];
    /**
     * Список вложений.
     */
    attachments?: ISignedAttachment[];
}
/**
 * Тип настройки.
 */
export declare enum SETTING_TYPE {
    /**
     * Публичная настройка.
     */
    PUBLIC = "PUBLIC",
    /**
     * Клиентская настройка.
     */
    CLIENT = "CLIENT",
    /**
     * Банковская настройка.
     */
    BANK = "BANK"
}
/**
 * Окружение настройки.
 */
export declare enum SETTING_ENV {
    /**
     * Все окружения.
     */
    ALL = "ALL",
    /**
     * Окружение sandbox.
     */
    SANDBOX = "SANDBOX",
    /**
     * Окружение stage.
     */
    STAGE = "STAGE",
    /**
     * Окружение release.
     */
    RELEASE = "RELEASE",
    /**
     * Окружение ift.
     */
    IFT = "IFT",
    /**
     * Окружение prom.
     */
    PROM = "PROM"
}
/**
 * Код настройки.
 */
export declare enum SETTING_CODE {
    /**
     * Меню.
     */
    MENU = "MENU",
    /**
     * Настройки.
     */
    SETTINGS = "SETTINGS",
    /**
     * Состояние сервисов.
     */
    HEALTH_CHECK = "HEALTH_CHECK"
}
/**
 * Элемент настройки.
 */
export interface ISetting {
    /**
     * Код настройки.
     */
    code: SETTING_CODE;
    /**
     * ID настройки.
     */
    id: string;
    /**
     * Тип настройки.
     */
    settingType: SETTING_TYPE;
    /**
     * Окружение настройки.
     */
    settingEnv: SETTING_ENV;
    /**
     * Наименование настройки.
     */
    title: string;
    /**
     * ID пользователя.
     */
    userId?: string;
    /**
     * Логическое значение настройки.
     */
    valueBool?: boolean;
    /**
     * Денежное значение настройки.
     */
    valueCurrency?: string;
    /**
     * Целочисленное значение настройки.
     */
    valueLong?: number;
    /**
     * Строковое значение настройки.
     */
    valueStr?: string;
}
