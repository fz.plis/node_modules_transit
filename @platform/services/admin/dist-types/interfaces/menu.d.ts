import type { MenuIcons, Icons, MENU_ITEM_TYPES } from '@platform/ui';
/**
 * Базовый интерфейс для элемента меню.
 */
export interface IBaseMenuItem {
    /**
     * Подпись.
     */
    label: string;
    /**
     * Иконка.
     */
    icon?: keyof typeof Icons | keyof typeof MenuIcons;
    /**
     * Путь до страницы.
     */
    path?: string;
    /**
     * Тип элемента меню.
     */
    type?: MENU_ITEM_TYPES;
    /**
     * Признак раскрытия пункта.
     */
    collapsed?: boolean;
}
/**
 * Интерфейс элемента меню.
 */
export interface IMenuItem extends IBaseMenuItem {
    /**
     * Дочерние пункты.
     */
    options?: IBaseMenuItem[];
    /**
     * Ключевое поле меню.
     */
    key?: string;
}
