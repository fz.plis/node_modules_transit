import type { ICertResponse, IFileData, IOrgCert, IMetaData, ICollectionResponse } from '../../interfaces';
export interface IOrganizationCertificatesService<T = IOrgCert> {
    getList(metaData: IMetaData): Promise<ICollectionResponse<T>>;
    get(id: string): Promise<T>;
    download(certificateId: string): Promise<IFileData>;
}
/**
 * Утилита, обработки ответа запросов по сертификату.
 *
 * @throws Сообщение от бека.
 */
export declare const certificateResponse: <T = IOrgCert>({ data: response }: {
    data: ICertResponse<T>;
}) => T;
/**
 * Утилита, возвращающая стандартные методы справочников клиента и подписи.
 *
 * @param clientDictionaryURL Путь до справочника клиентов.
 * @param signatureURL Путь до сервиса подписи.
 * @example
 * const CLIENT_DICTIONARY_URL = '/api/client-dictionary-internal/internal/dictionary/client'
 * const SIGNATURE_URL = `/api/signature-internal/internal/sign`
 * const organizationCertificatesService = {
 *   ...getNewOrgCertsService(CLIENT_DICTIONARY_URL, SIGNATURE_URL)
 *   myNewMethod: async () => {...}
 * }
 */
export declare const getNewOrgCertsService: (clientDictionaryURL: string, signatureURL: string) => IOrganizationCertificatesService;
