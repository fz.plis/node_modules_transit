import type { Canceler } from 'axios';
import type { IDownloadedAttachment, IAttachment, IFileUploadOptions, ICheckUploadOptions } from '../../interfaces';
export declare const createAttachmentService: (baseUrl: string) => {
    getStatus: (attachmentId: string, onCancel?: ((cancel: Canceler) => void) | undefined) => Promise<any>;
    /**
     * Метод для загрузки вложения.
     *
     * @param file - Объекта файла.
     * @param file.onUploadProgress Обработчик для прогресса загрузки вложения.
     * @param file.onCancel Обработчик отмены.
     */
    upload: (file: File, { onUploadProgress, onCancel }?: IFileUploadOptions) => Promise<IAttachment>;
    checkUploadProgress: (attachmentId: string, options?: ICheckUploadOptions) => Promise<IAttachment>;
    /**
     * Метод для скачивания файла.
     *
     * @param attachmentId Идентификатор файла.
     * @param accessToken Токен доступа к файлу.
     */
    download: (attachmentId: string, accessToken: string) => Promise<IDownloadedAttachment>;
    /**
     * Метод для скачивания файла в формате Base64.
     *
     * @param attachmentId Идентификатор файла.
     * @param accessToken Токен доступа к файлу.
     */
    downloadBase64: (attachmentId: string, accessToken?: string | undefined) => Promise<IDownloadedAttachment>;
    /**
     * Метод для одноразового скачивания файла.
     *
     * @param attachmentId Идентификатор файла.
     * @param accessToken Токен доступа к файлу.
     */
    downloadOneTimeData: (attachmentId: string, accessToken: string) => Promise<IDownloadedAttachment>;
};
