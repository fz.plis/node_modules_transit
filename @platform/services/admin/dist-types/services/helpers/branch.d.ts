import type { IBranch, IBranchV2, ICollectionResponse, IMetaData } from '../../interfaces';
export declare const createClientBranchService: (baseUrl: string) => {
    getList: (metaData: IMetaData) => Promise<ICollectionResponse<IBranch>>;
    get: (id: string) => Promise<IBranch>;
    searchByName: <T extends keyof IBranch>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<ICollectionResponse<IBranch>>;
    localityList(): Promise<string[]>;
    bicPage(metaData: IMetaData): Promise<ICollectionResponse<string>>;
};
export declare const createBranchV2Service: (baseUrl: string) => {
    getList: (metaData: IMetaData) => Promise<ICollectionResponse<IBranchV2>>;
    get: (id: string) => Promise<IBranchV2>;
    getCounter: (metaData: IMetaData) => Promise<any>;
    searchByName: <T extends keyof IBranchV2>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<ICollectionResponse<IBranchV2>>;
    localityList(): Promise<string[]>;
};
