import type { ISetting } from '../interfaces';
export declare const appSettingsService: {
    getAll: () => Promise<ISetting[]>;
    getByCode: (code: string) => Promise<ISetting>;
};
