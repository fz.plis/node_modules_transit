import type { ICurrency } from '../../interfaces';
export declare const currency: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICurrency>>;
    get: (id: string) => Promise<ICurrency>;
    searchByName: <T extends keyof ICurrency>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<ICurrency>>;
};
