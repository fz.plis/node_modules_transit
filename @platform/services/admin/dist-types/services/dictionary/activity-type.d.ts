import type { IActivityType } from '../../interfaces';
export declare const activityType: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IActivityType>>;
    get: (id: string) => Promise<IActivityType>;
    searchByName: <T extends keyof IActivityType>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IActivityType>>;
};
