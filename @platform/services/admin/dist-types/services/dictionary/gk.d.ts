import type { IGozContract } from '../../interfaces';
export declare const gk: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IGozContract>>;
    get: (id: string) => Promise<IGozContract>;
    searchByName: <T extends keyof IGozContract>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IGozContract>>;
};
