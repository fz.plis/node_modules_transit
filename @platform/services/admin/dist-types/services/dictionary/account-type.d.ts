import type { IAccountType } from '../../interfaces';
export declare const accountType: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccountType>>;
    get: (id: string) => Promise<IAccountType>;
    searchByName: <T extends keyof IAccountType>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IAccountType>>;
};
