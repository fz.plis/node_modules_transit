export declare const dictionaryService: {
    okfs: {
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IOkfs>>;
        searchByName: <T extends keyof import("../..").IOkfs>(params: import("../..").ISearchByName, fieldName: T) => Promise<import("@platform/core").ICollectionResponse<import("../..").IOkfs>>;
    };
    okopf: {
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IOkopf>>;
        searchByName: <T_1 extends keyof import("../..").IOkopf>(params: import("../..").ISearchByName, fieldName: T_1) => Promise<import("@platform/core").ICollectionResponse<import("../..").IOkopf>>;
    };
    bankClient: {
        get: (id: string) => Promise<import("../..").IBankClient>;
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBankClient>>;
        searchByName: <T_2 extends keyof import("../..").IBankClient>(params: import("../..").ISearchByName, fieldName: T_2) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBankClient>>;
        searchOgrn: (params: import("../..").ISearchByName) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBankClient>>;
        searchRegNumber: (params: import("../..").ISearchByName) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBankClient>>;
    };
    currency: {
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").ICurrency>>;
        get: (id: string) => Promise<import("../..").ICurrency>;
        searchByName: <T_3 extends keyof import("../..").ICurrency>(params: import("../..").ISearchByName, fieldName: T_3) => Promise<import("@platform/core").ICollectionResponse<import("../..").ICurrency>>;
    };
    activityType: {
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IActivityType>>;
        get: (id: string) => Promise<import("../..").IActivityType>;
        searchByName: <T_4 extends keyof import("../..").IActivityType>(params: import("../..").ISearchByName, fieldName: T_4) => Promise<import("@platform/core").ICollectionResponse<import("../..").IActivityType>>;
    };
    clientOfficial: {
        getUserOfficials: (userId: string) => Promise<import("../..").IClientOfficial[]>;
    };
    country: {
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").ICountry>>;
        get: (id: string) => Promise<import("../..").ICountry>;
        searchByName: <T_5 extends keyof import("../..").ICountry>(params: import("../..").ISearchByName, fieldName: T_5) => Promise<import("@platform/core").ICollectionResponse<import("../..").ICountry>>;
    };
    accountType: {
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IAccountType>>;
        get: (id: string) => Promise<import("../..").IAccountType>;
        searchByName: <T_6 extends keyof import("../..").IAccountType>(params: import("../..").ISearchByName, fieldName: T_6) => Promise<import("@platform/core").ICollectionResponse<import("../..").IAccountType>>;
    };
    branch: {
        get: (id: string) => Promise<import("../..").IBranch>;
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBranch>>;
        searchByName: <T_7 extends keyof import("../..").IBranch>(params: import("../..").ISearchByName, fieldName: T_7) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBranch>>;
        localityList: () => Promise<string[]>;
        bicPage: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<string>>;
    };
    branchV2: {
        get: (id: string) => Promise<import("../..").IBranchV2>;
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBranchV2>>;
        getCounter: (metaData: import("../..").IMetaData) => any;
        searchByName: <T_8 extends keyof import("../..").IBranchV2>(params: import("../..").ISearchByName, fieldName: T_8) => Promise<import("@platform/core").ICollectionResponse<import("../..").IBranchV2>>;
        localityList: () => Promise<string[]>;
    };
    gk: {
        getList: (metaData: import("../..").IMetaData) => Promise<import("@platform/core").ICollectionResponse<import("../..").IGozContract>>;
        get: (id: string) => Promise<import("../..").IGozContract>;
        searchByName: <T_9 extends keyof import("../..").IGozContract>(params: import("../..").ISearchByName, fieldName: T_9) => Promise<import("@platform/core").ICollectionResponse<import("../..").IGozContract>>;
    };
    fias: {
        search: (query: string, count: number) => Promise<import("../..").IFiasResult[]>;
    };
};
