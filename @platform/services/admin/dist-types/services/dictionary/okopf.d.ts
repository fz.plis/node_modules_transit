import type { IOkopf } from '../../interfaces';
export declare const okopf: {
    getList: (metaData: import("../../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkopf>>;
    searchByName: <T extends keyof IOkopf>(params: import("../../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IOkopf>>;
};
