export declare const showFile: (data: ArrayBuffer | Blob | Uint8Array | string, name: string, mimeType: string) => void;
export declare const getFileName: (param: string) => string;
