export declare const mimeTypeToExt: (mimeType: string) => "doc" | "docx" | "xls" | "xlsx" | "html" | "pptx" | "pdf" | "zip";
