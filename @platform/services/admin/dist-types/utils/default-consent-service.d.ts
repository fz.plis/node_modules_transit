import type { CONSENT_TYPE } from '../constants';
export declare const getDefaultConsentService: (url: string) => {
    getLastVersion: (type: CONSENT_TYPE) => Promise<any>;
    getContent: (type: CONSENT_TYPE) => Promise<any>;
};
