export declare const getParamFromString: (str: string, paramName: string) => string | null;
export declare const getParamFromUrl: (url: string, paramName: string) => string | null;
export declare const createServiceUrl: (clientUrl: string, internalUrl: string) => (slug: string) => (isInternal?: boolean) => string;
export declare const parseUrlSearch: (search: string) => Record<string, string>;
export declare const queryString: (params: Record<string, any>) => string;
/**
 * Метод для выделения базового роута.
 *
 * @param path Роут из useRouteMatch.
 * @returns Урл с отброшенным суффиксом '/:id/:tab'.
 */
export declare const getBaseUrl: (path: string) => string;
