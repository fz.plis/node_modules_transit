import type { IAppSidebarOptionProps } from '@platform/ui';
import type { IMenuItem } from '../interfaces/menu';
export declare const getSidebarOptions: (menuItems: IMenuItem[]) => IAppSidebarOptionProps[];
