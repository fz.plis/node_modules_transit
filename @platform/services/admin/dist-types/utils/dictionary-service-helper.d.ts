import type { IMetaData } from '../interfaces';
export declare const EMPTY_FILTER = "";
export declare const metadataToRequestParams: (meta: IMetaData) => {
    params: {
        paging: {
            offset: number;
            limit: number;
        };
        sort: {
            field: string;
            direction: string;
        } | undefined;
        multiSort: {
            field: string;
            direction: string;
        }[] | undefined;
        str: string | undefined;
        filter: any;
    };
};
