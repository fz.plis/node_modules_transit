export declare type Handler = (data?: any, defaultErrorHandler?: Handler) => void;
export declare type IHandlerError = Record<string, (data?: any, defaultErrorHandler?: Handler) => void>;
export declare const CTYPTO_ERROR_HANDLERS: IHandlerError;
export declare const DEFAULT_ERROR_HANDLERS: IHandlerError;
export declare const errorHandler: (errorHandlers?: IHandlerError | undefined) => (resp?: any) => void;
export declare const showSignError: (err: any, handlers?: IHandlerError) => void;
