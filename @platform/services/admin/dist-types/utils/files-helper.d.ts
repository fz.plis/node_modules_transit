export declare const extToMimeType: (extInput: string) => string;
export declare const getExtenstion: (fileName: string) => string;
/** Функция проверки допустимости файлов, возвращает истину в случае недопустимости. */
export declare const checkIfForbiddenFiles: (files: File[], allowedMimes: string[], allowedExtentions: string[]) => boolean;
