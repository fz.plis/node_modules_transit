export declare enum CERTIFICATES_TYPES {
    QES = "qes",
    UES = "ues"
}
/**
 * [НЭП] Клиент: Функция создания заявки.
 *
 * @see https://confluence.gboteam.ru/pages/viewpage.action?pageId=6362539
 */
export declare const UNQ_CERT_CREATE_PRIVILEGE = "UNQ_CERT_REQUEST.CREATE";
