import React from 'react';
import type { FormRenderProps } from 'react-final-form';
import type { IConfirmSpoForm } from '../interface';
/** Содержимое формы "Изменение статуса сертификата". */
export declare const ConfirmSpo: React.FC<FormRenderProps<IConfirmSpoForm>>;
