/**
 * Поля формы.
 */
export declare enum FORM_FIELD {
    /**
     * Телефон.
     */
    PHONE = "phone",
    /**
     * Email.
     */
    EMAIL = "email",
    /**
     * Код.
     */
    CODE = "code"
}
export declare enum STEPS {
    MAIN = "MAIN",
    UPDATE_PHONE = "UPDATE_PHONE",
    UPDATE_EMAIL = "UPDATE_EMAIL",
    RESULT = "RESULT"
}
export declare enum UPDATE_CONTACT_INFO_TYPE {
    CONFIRM = "CONFIRM",
    PHONE = "PHONE",
    EMAIL = "EMAIL"
}
export declare const enum RESULT_TYPE {
    ERROR = "ERROR",
    SUCCESSFULLY = "SUCCESSFULLY"
}
export declare const INPUT_WIDTH = "200px";
export declare const paths: {
    email: import("@platform/core").IPathGenerator<Required<{
        email: string;
        captchaId: string;
        captchaText: string;
    }>>;
    phone: import("@platform/core").IPathGenerator<Required<{
        phoneNumber: string;
        captchaId: string;
        captchaText: string;
    }>>;
    confirm: import("@platform/core").IPathGenerator<Required<{
        code: string;
    }>>;
};
