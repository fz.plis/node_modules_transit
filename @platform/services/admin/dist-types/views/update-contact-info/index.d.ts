import React from 'react';
import type { IUpdateContactProviderProps } from './interfaces';
export declare const UpdateContactInfoContainer: React.FC;
export declare const UpdateContactInfo: React.FC<IUpdateContactProviderProps>;
