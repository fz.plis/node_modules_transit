import React from 'react';
import type { IOpenAccount, IOpenAccountInfo } from '../../interfaces';
export interface IOpenAccountDetailsProps {
    document: IOpenAccount;
}
export interface IDetailRowProps {
    label: string;
    value?: number | string;
}
export declare class OpenAccountDetails extends React.Component<IOpenAccountDetailsProps> {
    renderHeader(): JSX.Element;
    renderDocInfo(): JSX.Element;
    renderReserved(): JSX.Element | null;
    renderReserveItem(info: IOpenAccountInfo, branch: IOpenAccount['branch']): JSX.Element;
    renderNewAccounts(): JSX.Element | null;
    renderNewAccount(info: IOpenAccountInfo, index: number): JSX.Element;
    render(): JSX.Element;
}
