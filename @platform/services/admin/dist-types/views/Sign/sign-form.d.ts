import React from 'react';
import type { IUserCertificate } from '../../interfaces';
import type { IWithDetailProps } from './shared';
export interface ISignDocumentsProps<T> {
    documents: T[];
}
export interface ISignDocumentProps<T> {
    document: T;
}
export declare type RenderSummary<T> = (props: ISignDocumentsProps<T>) => React.ReactNode;
export declare type RenderDocument<T> = (props: ISignDocumentProps<T>) => React.ReactNode;
export interface ISignProps<T> extends ISignDocumentsProps<T>, IWithDetailProps {
    certs: IUserCertificate[];
    renderSummary?: RenderSummary<T>;
    renderDocument: RenderDocument<T>;
    onClose(): void;
    onSignClick(thumbprint: string, cert: IUserCertificate): void;
    selectedCertificate: IUserCertificate;
}
export interface ISignState {
    docIndex: number;
    currentCertificate: IUserCertificate | null;
    userCertificates: IUserCertificate[];
}
export declare const Summary: React.FC<ISignDocumentsProps<any>>;
export declare const Sign: any;
