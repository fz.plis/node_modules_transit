import React from 'react';
import type { ICertificateRequestProps } from './interface';
export declare const CertificateRequest: React.FC<ICertificateRequestProps>;
