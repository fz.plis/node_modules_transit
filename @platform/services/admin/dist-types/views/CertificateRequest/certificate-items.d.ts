import React from 'react';
interface ICertificateRows {
    rows: Record<string, any>;
    labelsPath: Record<string, any>;
}
export declare class CertificateRows extends React.Component<ICertificateRows> {
    render(): JSX.Element[];
}
export {};
