import type { IMetaData, ICollectionResponse, ISearchByName } from './interfaces';
export declare const fake: (..._: any[]) => any;
export declare const fakeDictionaryService: <TRow extends {
    id: string;
}, TCreate = TRow>() => {
    getList: (metaData: IMetaData) => Promise<ICollectionResponse<TRow>>;
    getCounter: (metaData: IMetaData) => any;
    get: (id: string) => Promise<TRow>;
    create: (data: TCreate) => any;
    searchByName: <T extends keyof TRow>(params: ISearchByName, fieldName: T) => Promise<ICollectionResponse<TRow>>;
};
