/**
 * Идентификаторы пунктов меню, имещющих саб-меню.
 * Необходимы для возможности раскрыть нужное саб-меню из прикладного стрима.
 * Должны совпадать с id в конфигах resources/app.config.*.json.
 */
export declare const SIDEBAR_ITEMS_CONFIG: {
    PAYMENTS: string;
    TREASURY: string;
    PRODUCTS: string;
    CLAIMS: string;
};
