export declare const PLATFORM_QUERY_NAMES: {
    OPEN_ID_CONFIGURATION: string;
    APP_CONFIG: string;
    USER_INFO: string;
    USER_AUTHORITIES: string;
};
