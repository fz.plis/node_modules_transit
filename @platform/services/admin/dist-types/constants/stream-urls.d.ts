export declare const COMMON_STREAM_URL: {
    MAINPAGE: string;
    ACCOUNTS: string;
    RESERVE_ACCOUNT: string;
    OPEN_ACCOUNTS: string;
    NEW_OPEN_ACCOUNT: string;
    REGDOC: string;
    PROFILE: string;
    OFFER_LIST: string;
    REDIRECT_BSS: string;
    CONSTITUTION: string;
    LETTERS: string;
    NOTIFICATIONS: string;
    RKO_SERVICE_MANAGEMENT: string;
    DEBTS: string;
    /**
     * ЛК Увед.
     */
    UVED: string;
    /**
     * Предоставление доступа сотдруднику к ЛК УВЕД.
     */
    UVED_ACCESS: string;
    /**
     * Материальный пулинг.
     */
    LK_MP: string;
    /**
     * Предоставление доступа сотдруднику к Материальному пулингу.
     */
    LK_MP_ACCESS: string;
    /**
     * Сервис генерации сертификатов НЭП.
     */
    ESIGN: string;
    /** Конституция 32, выдача УЛ и мегадоверенности. */
    CONSTITUTION_ACCESS: string;
    /** Конституция З2, ЭФ предоставление доступа. */
    CONSTITUTION_ACCESS_FORM: string;
    /** Конституция З2, ЭФ получение доверенности. */
    CONSTITUTION_ISSUE_ATTORNEY: string;
    /** Справки. */
    INQUIRY: string;
};
