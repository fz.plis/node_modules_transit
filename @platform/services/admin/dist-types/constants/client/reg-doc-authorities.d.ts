/**
 * Привилегии на открытие первого счёта.
 */
export declare const REG_DOC_AUTHORITIES: {
    /** Функция создания заявки. */
    CREATE: string;
};
