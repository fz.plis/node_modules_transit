/**
 * Привилегии на отрытие дополнительного счета.
 */
export declare const OPEN_ACCOUNT_AUTHORITIES: {
    /** Функция создания заявки. */
    CREATE: string;
};
