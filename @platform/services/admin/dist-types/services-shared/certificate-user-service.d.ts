import type { IUserCertificate, IUserCertificateUpdateResponce, IAuthorityResponse, IAuthorityRequest, IVerifySignByDateResponce, IVerifySignByDateRequest, IAdditionalVerifySignRequestData, IAdditionalVerifySignResponseData } from '../interfaces';
export declare const certificateUserService: {
    updateUserCertificates: (certificates: IUserCertificate[]) => Promise<IUserCertificateUpdateResponce>;
    checkAuthority: (data: IAuthorityRequest) => Promise<IAuthorityResponse[]>;
    getVerifyData: (data: IVerifySignByDateRequest) => Promise<IVerifySignByDateResponce>;
    getAdditionalVerifyData: (data: IAdditionalVerifySignRequestData) => Promise<IAdditionalVerifySignResponseData>;
    getList: (metaData: import("../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IUserCertificate>>;
    getCounter: (metaData: import("../interfaces").IMetaData) => Promise<any>;
    get: (id: string) => Promise<IUserCertificate>;
    getByIds: (id: string, clientId: string) => Promise<IUserCertificate>;
    search: (metaData: import("../interfaces").IMetaData) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IUserCertificate>>;
    searchByName: <T extends keyof IUserCertificate>(params: import("../interfaces").ISearchByName, fieldName: T) => Promise<import("@platform/core/dist-types/interfaces/scroller").ICollectionResponse<IUserCertificate>>;
};
