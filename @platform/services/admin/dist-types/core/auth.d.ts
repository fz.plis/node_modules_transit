import React from 'react';
import type { IActionWebInfo, IActionWithAuth, IAuthConfiguration, IButtonConfig, IParsedToken, IUserAuthoritiesResponse, USER_CONTEXT_TYPE, IAuthoritiesObject } from '../interfaces';
import { OAuthProvider } from '../utils';
/**
 * Интерфейс компонента auth.
 */
interface IAuthProps {
    /**
     * Метод загрузки данных аутентификации.
     */
    authLoader(): Promise<IAuthConfiguration>;
    /**
     * Метод загрузки привилегий.
     */
    authoritiesLoader(): Promise<IUserAuthoritiesResponse>;
}
/**
 * Контекст аутентификации.
 */
interface IAuthContext {
    /**
     * Привилегии пользователя.
     */
    authorities: string[];
    /**
     * Конфиг аутентификации.
     */
    config: IAuthConfiguration | null;
    /**
     * Фатальная ошибка загрузки.
     */
    fatalErr: boolean;
    /**
     * Провайдер аутентификации.
     */
    provider: OAuthProvider | null;
    /**
     * Список  ролей.
     */
    roles: string[];
    /**
     * Токен.
     */
    token: IParsedToken | null;
    /**
     * Строковое представление токена.
     */
    tokenString: string;
    /**
     * Получить список доступных текущему пользователю элементов.
     */
    getAvailableItems<T>(items: Array<IAuthoritiesObject & T>): T[];
    /**
     * Метод получения списка доступных действий.
     *
     * @param actions Список действий.
     */
    getAvailableActions(actions: IActionWithAuth[]): Array<IActionWebInfo<any, any>>;
    /**
     * Метод получения списка доступных действий по категории.
     *
     * @param actions Список действий.
     */
    getAvailableActionsWithCategory(actions: Record<string, IActionWithAuth[]>): IButtonConfig<any, any>;
    /**
     * Метод проверки контекста.
     *
     * @param allowedContexts Доступный контекст.
     */
    hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean;
    /**
     * Метод проверки привилегий.
     *
     * @param userAuthorities Список привилегий.
     */
    hasAuthority(...userAuthorities: string[]): boolean;
    /**
     * Метод выхода из системы.
     */
    logout(): void;
    /**
     * Метод редиректа на логин.
     */
    redirectToLogin(): void;
}
export declare const getAuthValue: () => IAuthContext;
export declare const getAuthItem: <K extends "redirectToLogin" | "token" | "tokenString" | "authorities" | "hasAuthority" | "hasAllowedContextType" | "roles" | "getAvailableActionsWithCategory" | "logout" | "getAvailableActions" | "getAvailableItems">(key: K) => IAuthContext[K];
export declare const AuthProvider: React.FC<IAuthProps>;
export declare const useAuth: () => IAuthContext;
export {};
