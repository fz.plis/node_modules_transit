import React from 'react';
import type { IAppConfig, ISetting } from '../interfaces';
/**
 * Интерфейс контекста загружаемой конфигурации приложения.
 */
export interface IAppConfigContext {
    /**
     * Содержимое конфигурации с сервера.
     */
    config: IAppConfig;
    /**
     * Признак загрузки конфига с сервера.
     */
    loading: boolean;
    /**
     * Метод, запускающий загрузку.
     */
    reload(): void;
}
/**
 * React Context конфига приложения.
 */
export declare const AppConfigContext: React.Context<IAppConfigContext>;
/**
 * Возвращает загруженный конфиг. Требуется для доступа вне React-приложения (экшены, крипта).
 */
export declare const getAppConfigValue: () => IAppConfig;
/**
 * Возвращает элемент конфига.
 *
 * @param key Ключ элемента.
 */
export declare const getAppConfigItem: <K extends keyof IAppConfig>(key: K) => IAppConfig[K];
export interface IAppConfigProps {
    /**
     * Метод получения конфига.
     */
    loader(): Promise<IAppConfig>;
    /**
     * Если `true` - компонент сам загружает конфиг по указанному url.
     * При этом на время загрузки можно отображать компонент, переданный в `fallback`.
     *
     * @default false
     */
    autoload?: boolean;
    /**
     * Если autoload - true, этот компонент отображается во время загрузки конфига.
     *
     * @default <LoaderOverlay doNotUsePortal opened />
     */
    fallback?: NonNullable<React.ReactNode> | null;
}
/**
 * Компонент предоставляет `AppConfigContext.Provider`.
 *
 * @example
 * <AppConfig url="/api/config-service/config.json">...</AppConfig>
 */
export declare const AppConfig: React.FC<IAppConfigProps>;
/**
 * Хук, возвращающий значение контекста из `AppConfigContext`.
 */
export declare const useAppConfig: () => IAppConfigContext;
export declare const getSettingsLoader: (menuLoader: () => Promise<ISetting>, settingsLoader: () => Promise<ISetting>, fallbackConfigLoader: () => Promise<IAppConfig>) => () => Promise<any>;
export declare const getFallbackConfigLoader: (url: string) => () => Promise<any>;
