import React from 'react';
import type { RouterProps } from 'react-router';
import type { IUserData } from '../interfaces';
/**
 * Свойства компонента приложения.
 */
export interface IAppParams {
    /**
     * Компонент отображения загрузчика стрима.
     */
    LoadingStream: React.ComponentType<any>;
    /**
     * Страница "не найдено".
     */
    NotFound: React.ComponentType<any>;
    /**
     * Страница "стрим недоступен".
     */
    Unavailable: React.ComponentType<any>;
    /**
     * История.
     */
    history: RouterProps['history'];
}
export declare const getAppMainComponent: ({ LoadingStream, NotFound, history, Unavailable }: IAppParams) => React.FC<{
    user?: IUserData | undefined;
}>;
