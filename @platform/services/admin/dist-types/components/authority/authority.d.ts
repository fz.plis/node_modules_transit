import React from 'react';
import type { IAuthorityInformerProps, IAuthorityProps } from './interfaces';
/**
 * Элемент информера.
 *
 * @param props Свойства.
 * @param props.idx Индекс элемента.
 * @param props.message Сообщение.
 * @param props.onClose Закрыть сообщение.
 * @class
 */
export declare const AuthorityInformer: React.FC<IAuthorityInformerProps>;
/**
 * Информер.
 *
 * @param props Свойства.
 * @param props.bankClient Организация пользователя.
 * @param props.documentType Тип документа.
 * @param props.productCodes Коды продуктов банка.
 * @param props.validators Опции проверки полномочий.
 * @param props.certificateErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для сертификата.
 * @param props.constitutionErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для ЕИО.
 * @param props.eioErrorDefaultMessage Сообщение по-умолчанию для проверки полномочий для конституцию.
 * @param props.noCertificateErrorMessage Сообщение по-умолчанию об отсутствии сертификата.
 * @param props.children Дочерние элементы.
 * @class
 */
export declare const Authority: React.FC<IAuthorityProps & {
    children(hasAuthorityError: boolean): React.ReactNode;
}>;
