import React from 'react';
import type { CONSENT_TYPE } from '../../constants';
/**
 * Свойства модального окна согласия.
 */
interface IConsentModalProps {
    /**
     * Лейбл принятия согласия.
     */
    acceptLabel?: string;
    /**
     * Тип согласия.
     */
    type: CONSENT_TYPE;
    /**
     * Закрыть модальное окно.
     */
    onClose(accept?: boolean): void;
    /**
     * Текущее значение принятия согласия.
     */
    value?: boolean;
    /**
     * Флаг только для чтения(дизейблим чекбокс).
     */
    readOnly?: boolean;
}
export declare const ConsentModal: React.FC<IConsentModalProps>;
export declare const openConsentModal: ({ onClose, ...rest }: IConsentModalProps) => void;
export {};
