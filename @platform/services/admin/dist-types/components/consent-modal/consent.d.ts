import React from 'react';
/**
 * Диалог согласия.
 */
interface IConsentDialogProps {
    /**
     * Текст согласия.
     */
    acceptLabel: string;
    /**
     * Метод закрытия диалога.
     *
     * @param value Значение принятия согласия.
     */
    onClose(value?: boolean): void;
    /**
     * Контент согалсия.
     */
    consentContent: string;
    /**
     * Заголовок согласия.
     */
    consentHeader: string;
    /**
     * Текущее значение принятия согласия.
     */
    value?: boolean;
    /**
     * Флаг только для чтения(дизейблим чекбокс).
     */
    readOnly?: boolean;
}
export declare const ConsentDialog: React.FC<IConsentDialogProps>;
export {};
