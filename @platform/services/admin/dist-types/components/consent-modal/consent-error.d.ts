import React from 'react';
/**
 * Свойства модального окна ошибки загрузки согласия.
 */
interface IConsentErrorProps {
    /**
     * Закрыть окно.
     */
    onClose(): void;
    /**
     * Перезагрузить согласие.
     */
    onReload(): void;
}
export declare const ConsentError: React.FC<IConsentErrorProps>;
export {};
