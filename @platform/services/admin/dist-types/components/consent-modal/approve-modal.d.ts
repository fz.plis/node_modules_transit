import React from 'react';
import type { CONSENT_TYPE } from '../../constants';
/**
 * Модальное окно согласия.
 */
interface IApproveModalProps {
    /**
     * Тип согласия.
     */
    type: CONSENT_TYPE;
    /**
     * Метод закрытия диалога.
     *
     * @param value Значение принятия согласия.
     */
    onClose(value?: boolean): void;
    /**
     * Текущее значение принятия согласия.
     */
    value?: boolean;
}
export declare const ApproveConsentModal: React.FC<IApproveModalProps>;
export declare const openApproveConsentModal: ({ onClose, ...rest }: IApproveModalProps) => void;
export {};
