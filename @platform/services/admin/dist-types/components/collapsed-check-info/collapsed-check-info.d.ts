import React from 'react';
/**
 * Свойства компонента информации о сертификате.
 */
interface ICertificateInfo {
    /**
     * Владелец.
     */
    owner: string;
    /**
     * Организация.
     */
    organization: string;
    /**
     * Номер.
     */
    number: string;
    /**
     * Издатель.
     */
    creator: string;
    /**
     * Начало срока действия.
     */
    validFrom: string;
    /**
     * Окончание срока действия.
     */
    validTo: string;
}
/**
 * Свойства компонента расширенной информации о документе, подписантах и информации о подписях и сертификатах.
 */
export interface ISignInfo {
    /**
     * ФИО.
     */
    fio: string;
    /**
     * Дата.
     */
    date: string;
    /**
     * Роль.
     */
    role: string;
    /**
     * Флаг валидности.
     */
    valid: boolean;
    /**
     * Информация о сертификате.
     */
    certificate: ICertificateInfo;
    /**
     * Код ошибки.
     */
    errorCode?: string;
    /**
     * Текст ошибки.
     */
    errorText?: string;
}
/**
 * Свойства компонента информации о документе, подписантах и информации о подписях и сертификатах.
 */
export interface ICollapsedCheckInfo {
    /**
     * Заголовок документа.
     */
    documentTitle: string;
    /**
     * Информация о подписантах, подписях и сертификатах.
     */
    signInfos: ISignInfo[];
    /**
     * Признак раскрытия.
     */
    expanded?: boolean;
}
/**
 * Компонент информации о документе, подписантах и информации о подписях и сертификатах.
 *
 * @param props Свойства компонента.
 * @param props.documentTitle Заголовок документа.
 * @param props.signInfos Информация о подписантах, подписях и сертификатах.
 * @param props.expanded Признак раскрытия.
 */
export declare const CollapsedCheckInfo: React.FC<ICollapsedCheckInfo>;
export {};
