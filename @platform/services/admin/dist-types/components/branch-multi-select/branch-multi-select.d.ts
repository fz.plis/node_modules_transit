import React from 'react';
import type { IMultiSelectProps, IOption } from '@platform/ui';
import type { IBranchV2, IFilters } from '../../interfaces';
/**
 * Расширенная опция для древовидного отображения.
 */
export interface ITreeOption extends IOption {
    /**
     * Id родителя.
     */
    parent?: string;
    /**
     * Массив дочерних опций.
     */
    children?: ITreeOption[];
    /**
     * Неопределенный значение опции для чекбокса.
     */
    indeterminate?: boolean;
}
export declare const getOptions: (result: IBranchV2[]) => ITreeOption[];
export declare const checkAllSelected: (options: ITreeOption[], values: string[], searchValue: string) => boolean;
export interface IBranchMultiSelect extends Omit<IMultiSelectProps, 'options'> {
    /**
     * Дополнительные фильтры для запроса списка бранчей.
     */
    filters?: IFilters;
    /** Признак клиентской стороны. */
    isClient?: boolean;
}
export declare const Component: React.FC<IBranchMultiSelect>;
export declare const BranchMultiSelect: React.ComponentType<import("@platform/ui").IInputWithValidation & IBranchMultiSelect>;
export declare const BranchMultiSelectField: import("@platform/ui/dist-types/form").FieldedComponentType<import("@platform/ui").IInputWithValidation & IBranchMultiSelect>;
