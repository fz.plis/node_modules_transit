import React from 'react';
export interface ISelectedDocumentsInfo {
    /** Функция для получения надписи о количестве выбранных элементов. */
    getSelectedElementsInfo?(p: {
        selected: number;
        total: number;
    }): string;
    /** Количество выбранных элементов. */
    selected: number;
    /** Всего элементов. */
    total: number;
}
/** Информация о выбранных элементах. */
export declare const SelectedDocumentsInfo: React.FC<ISelectedDocumentsInfo>;
