import React from 'react';
export interface ILabelWithHighlight {
    label: string;
    searchValue?: string;
    fieldName?: string;
    matchFromStartOnly?: boolean;
}
/**
 * Компонент лейбла элемента в результатах поиска с подсветкой совпадающих символов.
 *
 * @param props Свойства.
 * @param props.label Текущее значение поля.
 * @param props.searchValue Последовательность символов для поиска.
 * @param props.fieldName Название поля.
 * @param props.matchFromStartOnly Флаг проверки вхождения поисковой строки только с начала последовательности.
 */
export declare const LabelWithHighlight: React.FC<ILabelWithHighlight>;
