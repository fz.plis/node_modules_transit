import type { IStatusOption, IChangeStatusFormState } from './interfaces';
/**
 * Возвращает валидатор для формы смены статуса заявки.
 *
 * @param availableStatuses - Статусы на которые можно сменить текущий.
 */
export declare const getValidate: (availableStatuses: IStatusOption[]) => (values: IChangeStatusFormState) => import("@platform/ui/dist-types/form").IFieldValidationResult;
