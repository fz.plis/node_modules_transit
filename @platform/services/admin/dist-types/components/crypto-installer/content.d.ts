import React from 'react';
import type { ICryptoInstallerProps } from './interfaces';
export declare const CryptoInstallerContent: React.FC<ICryptoInstallerProps>;
