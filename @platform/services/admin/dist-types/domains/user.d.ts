import type { IActionWebInfo, IActionWithAuth, IButtonConfig, IUserData, USER_ROLE, UserType, USER_CONTEXT_TYPE } from '../interfaces';
export interface IUserDomainState {
    token: string;
    data?: IUserData;
    type?: UserType;
    roles: USER_ROLE[];
    /**
     * Список привилегий.
     */
    authorities: string[];
}
export declare const user: import("@platform/tools/istore").Unit<IUserDomainState, {
    getAvailableActionsWithCategory: (actions: Record<string, IActionWithAuth[]>) => IButtonConfig<any, any>;
    logout: () => void;
    setToken: (token: string, authorities: string[]) => void;
    getAvailableActions: (actions: IActionWithAuth[]) => IActionWebInfo<any, any>[];
    setUserType: (userType: UserType) => void;
    updateData: (data: Partial<IUserData>) => void;
    setRoles: (roles: USER_ROLE[]) => void;
    /**
     * Проверяет наличие хотя бы одной из указанных ролей у юзера.
     *
     * @deprecated Use `hasAuthority`.
     */
    isAvailableFor: (...roles: USER_ROLE[]) => boolean;
    hasAuthority(...userAuthorities: string[]): boolean;
    hasAllowedContextType(allowedContexts: USER_CONTEXT_TYPE[]): boolean;
}>;
export declare type UserDomain = typeof user;
