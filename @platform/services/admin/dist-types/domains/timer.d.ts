/**
 * Creates new count down timer domain.
 */
export declare const timer: (inititalValue: number) => import("@platform/tools/istore").Domain<{
    isTicking: import("@platform/tools/istore").Unit<boolean, {
        set: (val: boolean) => void;
    }>;
    value: import("@platform/tools/istore").Unit<number, {
        set: (val: number) => void;
    }>;
}, {
    /**
     * Starts new countdown from `intitalValue` with `interval` ms.
     */
    start: (interval?: number) => void;
    /**
     * Stops current countdown.
     */
    stop: () => void;
    /**
     * Stops current countdown and reset timing to `intitalValue`.
     */
    reset: () => void;
    /**
     * Continues current countdown from last stop.
     *
     * If there is no countdown starts new countdown from `intitalValue` with 1000 ms interval.
     */
    continue: () => void;
}>;
