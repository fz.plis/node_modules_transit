import type { IScrollerToolbarProps, ISortField } from '@platform/ui';
import { SORT_DIRECTION } from '@platform/ui';
declare type UseToolbarSort = (config: {
    sortFields: ISortField[];
    byDefault?: {
        fieldName: string;
        direction: SORT_DIRECTION;
    };
    onChangeSort?(fieldName: string, direction: SORT_DIRECTION): void;
    /**
     * Ключ, по которому будем доставать данные из хранилища. Если не передан, то
     * вместо хранилища (localstorage) будет использован обычный React.state.
     * */
    storageKey?: string;
}) => [Pick<IScrollerToolbarProps, 'onChangeSort' | 'sortBy' | 'sortDirection' | 'sortFields'>, Record<string, SORT_DIRECTION>];
/**
 * Хук, возвращающий props сортировки, а также параметры для запроса списка.
 *
 * Если не передавать в хук значение сортировки по-умолчанию, то применится
 * сортировка по первому элементу sortFields в нисходящем направлении.
 *
 * @example
 * const myComp = () => {
 *  const [toolbarSortProps, sortRequestParam] = useToolbarSort({ sortFields: [{ name: 'date', label: 'Дата' }], onChangeSort: () => {} })
 *  useEffect(() => {
 *    fetcher({ sort: sortRequestParam }).then(({ data }) => console.log(data))
 *  }, [sortRequestParam])
 *  return <ScrollerToolbar {...toolbarSortProps} />
 * }
 */
export declare const useToolbarSort: UseToolbarSort;
export interface IUseToolbarCheckbox<T> {
    rows: T[];
    onChangeCheckbox?(value: boolean): void;
}
/**
 * Хук, возвращающий props флажка панели инструментов.
 *
 * @example
 * const myComp = () => {
 *  const toolbarCheckboxProps = useToolbarCheckbox({ rows, selectedRows, onChangeCheckbox: () => {} })
 *  return <ScrollerToolbar {...toolbarCheckboxProps} />
 * }
 */
export declare const useToolbarCheckbox: <T>({ rows, onChangeCheckbox }: IUseToolbarCheckbox<T>) => {
    checkboxValue: boolean;
    selectedRows: T[];
    checkboxIndeterminate: boolean;
    onChangeCheckbox: (value: any) => void;
    onChangeSelectedRows: (val: any) => void;
};
export {};
