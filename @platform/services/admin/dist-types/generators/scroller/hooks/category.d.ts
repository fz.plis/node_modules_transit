import type { ICategory } from '@platform/ui';
import type { IMetaData } from '../../../interfaces';
/**
 * Хук категорий.
 */
interface IUserCategory {
    /**
     * Метод получения списка категорий скроллера.
     *
     * @param metaData Параметры запроса списка категорий.
     */
    fetcher(metaData: IMetaData): Promise<Array<{
        name: string;
        value: string;
        desc: string;
    }>>;
    /**
     * Категория по-умолчанию.
     */
    defaultCategory: string;
    /**
     * Параметры запроса списка категорий.
     */
    requestParams: IMetaData;
    /**
     * Ресурс локализации.
     *
     * @param key Ключ.
     */
    locale(key: string): string;
    /**
     * Ключ, по которому будем доставать данные из хранилища. Если не передан, то
     * вместо хранилища (localstorage) будет использован React.useState.
     * */
    storageKey?: string;
    /** Игнорировать содержимое хранилища при инициализации и форсировать инициализацию из дефолтных значений конфига. */
    forceFromDefault?: boolean;
    /**
     * Отключает обработку сетевой ошибки.
     * По умолчанию `false`. При ошибке будет показывать модалку с сообщением ошибки.
     *
     * @default false
     */
    disableErrorHandling?: boolean;
}
/**
 * Хук для работы с категориями скроллера.
 *
 * @example
 * const myComp = () => {
 *  const { categories, category, methods } = useCategory({ fetcher, defaultCategory, requestParams, locale })
 *  return <ScrollerManager
 *    categories={categories}
 *    category={category}
 *    onCategoryChange={methods.setCategory}
 *  />
 * }
 */
export declare const useCategory: ({ fetcher, defaultCategory, requestParams, locale, storageKey, forceFromDefault, disableErrorHandling, }: IUserCategory) => {
    categories: ICategory[];
    category: string;
    methods: {
        setCategory: (newCategory: string) => void;
        reload: () => Promise<void>;
    };
    state: {
        hasError: boolean;
        error: undefined;
    };
};
export {};
