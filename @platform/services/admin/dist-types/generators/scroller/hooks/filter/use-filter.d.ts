import type { IFilterField } from './interface';
interface IFilter {
    fields: Record<string, IFilterField>;
    labels: Record<string, string>;
    /**
     * Ключ, по которому будем доставать данные из хранилища. Если не передан, то
     * вместо хранилища (localstorage) будет использован обычный React.state.
     * */
    storageKey?: string;
    /** Игнорировать содержимое хранилища при инициализации и форсировать инициализацию из дефолтных значений конфига. */
    forceFromDefault?: boolean;
}
export declare const useFilter: ({ fields, labels, storageKey, forceFromDefault }: IFilter) => {
    filterValues: import("@platform/core").IFilters;
    tagsPanel: {
        tags: import("@platform/ui").IOption<any>[];
        onClick: () => void;
        onRemoveAllTags: () => void;
        onRemoveTag: (key: string) => void;
    };
    filterPanel: {
        values: Record<string, unknown>;
        opened: boolean;
        onClose: () => void;
        onOk: (formValues: any) => void;
        onClear: () => void;
    };
};
export {};
