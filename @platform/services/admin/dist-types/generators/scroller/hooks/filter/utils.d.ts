import type { AnyObject } from 'final-form';
import type { IFilters } from '@platform/core';
import type { IOption } from '@platform/ui';
import type { IFilterField } from './interface';
export declare const filters: {
    eq: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
    contains: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
    in: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
    ge: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
    le: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
    notEq: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
    and: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
    or: (value: unknown, fieldName?: string | undefined, formatter?: ((value: unknown) => unknown) | undefined) => {
        condition: "and" | "or" | "contains" | "eq" | "ge" | "in" | "le" | "notEq";
        value: unknown;
        fieldName: string | undefined;
        formatter: ((value: unknown) => unknown) | undefined;
    };
};
export declare const getFilterDefaultValues: (fields: Record<string, IFilterField>) => Record<string, unknown>;
export declare const mapDataToFilter: (values: AnyObject, filterFields: Record<string, IFilterField>) => IFilters;
export declare const getTags: (values: AnyObject, labels: Record<string, string>) => IOption[];
