import type { IFiltersStorageObject, IStorage } from '@platform/core';
/**
 * Получить результат выполнения хука.
 *
 * @param hook Хук.
 * @param params Параметры хука.
 */
export declare const getHookValue: <TVal, TResult>(hook: (...args: TVal[]) => TResult, params?: TVal | undefined) => TResult | null;
export declare const sessionStorageWrapper: IStorage;
declare enum SCROLLER_FILTER {
    CATEGORY = "CATEGORY",
    FILTER = "FILTER",
    SORT = "SORT"
}
export declare const FILTER_TYPE: Record<SCROLLER_FILTER, keyof IFiltersStorageObject>;
export interface IUseStorageSync<K extends keyof IFiltersStorageObject, T extends IFiltersStorageObject[K]> {
    /** Дефолтное значение фильтра, если хранилище пустое. */
    value: T;
    /** Тип фильтра `filters` | `category` | `sort`. */
    filterType: K;
    /** Ключ, по которому будет происходить доступ к хранилищу. */
    storageKey: string;
}
/** Хук для синхронизации настроек скроллера в `SessionStorage`. */
export declare const useStorageSync: <K extends keyof IFiltersStorageObject, T extends IFiltersStorageObject[K]>(config: IUseStorageSync<K, T>) => void;
export interface IStorageSettings<K extends keyof IFiltersStorageObject> {
    /** Тип фильтра `filters` | `category` | `sort`. */
    filterType: K;
    /** Ключ, по которому будет происходить доступ к хранилищу. */
    storageKey: string;
    /** Флаг, который определяет нужно ли инициализировать значение из переданного аргумента, игнорируя хранилище. */
    forceInitFromConfigValue?: boolean;
}
/** Утилита для получения значений для инициализации фильтра. */
export declare const getDefaultValue: <K extends keyof IFiltersStorageObject, T extends IFiltersStorageObject[K]>(configValue: T, storageSettings?: IStorageSettings<K> | undefined) => T;
export {};
