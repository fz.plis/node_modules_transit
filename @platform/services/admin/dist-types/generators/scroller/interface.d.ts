import type React from 'react';
import type { AnyObject } from 'final-form';
import type { ICollectionResponse } from '@platform/core';
import type { IActionListAction, IRowTemplateProps, ISortField, SIZE, SORT_DIRECTION } from '@platform/ui';
import type { IMetaData } from '../../interfaces';
import type { IFilterField } from './hooks/filter';
/**
 * Карточка скроллера.
 */
export interface IScrollerCardProps<T = any> extends IRowTemplateProps {
    /**
     * Метод обновления категорий и данных скроллера.
     */
    reload(): void;
    /**
     * Тип документа, с которым работает скроллер.
     */
    row: T;
}
/**
 * Категории.
 */
export interface IGeneratorCategory {
    /**
     * Категория по-умолчанию.
     */
    defaultCategory: string;
    /**
     * Ресурс локализации.
     *
     * @param localeItem Ключ.
     */
    locale(localeItem: string): string;
    /**
     * Метод получения списка категорий.
     *
     * @param metaData Параметры запроса.
     */
    fetcher(metaData: IMetaData): Promise<Array<{
        name: string;
        value: string;
        desc: string;
    }>>;
}
/**
 * Фильтр.
 */
export interface IGeneratorFilter {
    /**
     * Список полей фильтра.
     */
    fields: Record<string, IFilterField>;
    /**
     * Список переводов для полей фильтра.
     */
    labels: Record<string, string>;
    /**
     * Компонент фильтра.
     */
    component: React.ComponentType;
    /**
     * Функция проверки формы.
     */
    validate?(values: AnyObject): Promise<Record<string, unknown>> | Record<string, unknown>;
}
/**
 * Сортировка.
 */
export interface IGeneratorSorting {
    /**
     * Сортировка по-умолчанию.
     */
    byDefault?: {
        fieldName: string;
        direction: SORT_DIRECTION;
    };
    /**
     * Поля сортировки.
     */
    sortFields: ISortField[];
}
/**
 * Генератор скроллера.
 */
export interface IScrollerGeneratorProps<T> {
    /**
     * Метод получения данных скроллера.
     *
     * @param metaData
     */
    fetcher(metaData: IMetaData): Promise<ICollectionResponse<T>>;
    /**
     * Категории.
     */
    categories: IGeneratorCategory;
    /**
     * Фильтр.
     */
    filters?: IGeneratorFilter;
    /**
     * Сортировка.
     */
    sorting?: IGeneratorSorting;
    /**
     * Показать чекбокс.
     */
    showCheckbox?: boolean;
    /**
     * С раскрыващющимися карточками.
     */
    withExpandRows?: boolean;
}
/**
 * Заголовок колонки.
 */
export interface IColumnHeader {
    /**
     * Текст заголовка.
     */
    label: string;
    /**
     * Размер колонки в Patter.Span.
     */
    size: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
}
/**
 * Свойства скроллера.
 */
export interface IScrollerProps<T> {
    /**
     * Заголовок скроллера.
     */
    header: string;
    /**
     * Карточка скроллера.
     */
    card: React.ComponentType<IScrollerCardProps<T>>;
    /**
     * Метод получения действий для скроллера.
     */
    getActions?(category: string, selectedRows: T[], reload: () => void): IActionListAction[];
    /**
     * Количество отображаемых действий.
     */
    actionsVisibleCount?: number;
    /**
     * Домой.
     */
    onHomeClick?(): void;
    /**
     * Заголовки колонок.
     */
    columnHeadersProps?: {
        headers: IColumnHeader[];
        gap?: SIZE;
    };
}
