import React from 'react';
import type { ICollectionResponse, IExecuter, SORT_DIRECTION } from '@platform/core';
import type { IMetaData, IActionWithAuth } from '../../../interfaces';
import type { IGeneratorCategory, IGeneratorFilter, IGeneratorSorting } from '../interface';
import type { IActionWithChildren, IToolbarActionsGetterConfig } from './interface';
declare type IToolbarActionsGetter = <TRow>(config: IToolbarActionsGetterConfig<TRow>) => Array<IActionWithAuth | IActionWithChildren>;
/**
 * Генератор скроллера.
 */
export interface IScrollerFeatures<TRow, TExecuterContext = unknown> {
    /**
     * Метод получения данных скроллера.
     *
     * @param metaData
     */
    fetcher(metaData: IMetaData): Promise<ICollectionResponse<TRow>>;
    /** Параметры категорий. */
    categories?: IGeneratorCategory;
    /** Параметры фильтров. */
    filters?: IGeneratorFilter;
    /** Параметры сортировки. */
    sorting?: IGeneratorSorting;
    /** Возможность выделения строк. */
    selectable?: boolean;
    /** Экшн экзекутор, через который будут исполняться действия скроллера и строк. */
    executer: IExecuter<TExecuterContext>;
    /** Действия для скроллера. Можно кидать как массив, так и геттер функцию. */
    toolbarActions?: Array<IActionWithAuth | IActionWithChildren> | IToolbarActionsGetter;
    /** Количество отображаемых действий.
     *
     * @default 3.
     */
    toolbarActionsVisibleCount?: number;
    /**
     * Ключ для доступа в хранилище. Этот параметр включает функцию синхронизации текущих значений
     * фильтров скроллера с хранилищем (localstorage).
     * */
    storageKey?: string;
    /**
     * Дополнительные параметры для действий со строками скроллера.
     * Будут переданы в дополнение к row в action.
     *
     * @example action: ({ done, fatal, addSucceeded }, { showLoader, hideLoader }) => async ([document], actionsParams) => ...
     */
    actionsParams?: unknown;
}
/** Хук, который отвечает за всю логику компонента скроллера,
 * возвращает пропы практически для всех элементов страницы со скроллером.
 */
export declare const useScrollerFeatures: <TRow, TExecuterContext>({ fetcher, filters, sorting, categories, selectable, executer, toolbarActions, toolbarActionsVisibleCount, storageKey, actionsParams, }: IScrollerFeatures<TRow, TExecuterContext>) => {
    filtersPanelProps: {
        validate: ((values: import("final-form").AnyObject) => Record<string, unknown> | Promise<Record<string, unknown>>) | undefined;
        children: JSX.Element | undefined;
        values: Record<string, unknown>;
        opened: boolean;
        onClose: () => void;
        onOk: (formValues: any) => void;
        onClear: () => void;
    } | undefined;
    filterNodeProps: {
        onClick: () => void;
        value: number;
    } | undefined;
    sortSettingsProps: {
        direction: import("@platform/ui").SORT_DIRECTION | undefined;
        sortBy: string | undefined;
        options: {
            label: string;
            value: string;
        }[];
        onChange: (fieldName: string, order: SORT_DIRECTION) => void;
    } | undefined;
    categoryTabsProps: {
        categories: import("@platform/ui").ICategory[];
        category: string;
        onCategoryChange: (category: string) => void;
        hasError: boolean;
    } | undefined;
    scrollerComponentProps: {
        reload: () => Promise<void>;
        rows: any[];
        onIntersecting?: (() => void) | undefined;
        isLoading: boolean;
        hasError: boolean;
        error: any;
    };
    selectableRowsProps: {
        selectedRows: any[];
        onChangeSelectedRows: (val: any) => void;
    } | undefined;
    expandableRowsProps: {
        expandedRows: TRow[];
        onChangeExpandedRows: React.Dispatch<React.SetStateAction<TRow[]>>;
    };
    selectRowsCheckboxProps: {
        value: boolean;
        onChange: (value: any) => void;
        indeterminate: boolean;
    } | undefined;
    actionsBarProps: {
        actions: import("@platform/ui").IActionBarAction[];
        visibleCount: number;
    } | undefined;
    executer: IExecuter<TExecuterContext>;
    selectedDocumentsInfo: {
        selected: number;
        total: number;
    } | undefined;
};
export {};
