import React from 'react';
import type { History } from 'history';
import type { RouterProps } from 'react-router';
import type { IExecuter } from '@platform/core';
import type { IOmniTableColumn, IBreadcrumb } from '@platform/ui';
import type { IActionWithAuth } from '../../../interfaces';
import type { ICommonScrollerGeneratorProps, ScrollerPageProps } from './interface';
import type { IScrollerFeatures } from './scroller-features';
interface IOnRowDoubleClick<TRow, TExecuterContext> {
    /** Объект с данными текущей строчки. */
    row: TRow;
    /** Текущая категория. */
    category?: string;
    /** Экзекутор. */
    executer: IExecuter<TExecuterContext>;
}
/** Конфиг для функции для получения массива экшнов. */
interface IRowActionsGetter<TRow> {
    /** Объект с данными текущей строчки. */
    row: TRow;
    /** Текущая категория. */
    category?: string;
    /** Значения фильтра. */
    filters?: unknown;
}
declare type RowActionsGetter<TRow> = (config: IRowActionsGetter<TRow>) => IActionWithAuth[];
declare type RowActionsType<TRow> = IActionWithAuth[] | RowActionsGetter<TRow>;
/** Расширенный интерфейс хлебных крошек. */
export interface IBreadcrumbsExtended extends Omit<IBreadcrumb, 'onClick'> {
    /** Функция-обработчик нажатия на хлебную крошку. */
    onClick(router: History<History.LocationState>): void;
}
/** Конфиг для скроллера со строками. */
export interface IRowScrollerGeneratorProps<TRow, TExecuterContext> extends ICommonScrollerGeneratorProps, IScrollerFeatures<TRow, TExecuterContext> {
    /** Колонки для омни таблицы. */
    columns: Array<IOmniTableColumn<TRow>>;
    /**
     * Действия для строк. Можно кидать массив экшнов, или функцию, которая вернет массив экшнов.
     * Функция, в качестве аргументов получает текущую категорию и объект с данными текущей строчки.
     * */
    rowActions?: RowActionsType<TRow>;
    /** Компонент для рендера содержимого строки в раскрытом состоянии. */
    rowDetailsTemplate?: React.ComponentType<{
        /** Данные строки. */
        row: TRow;
    }>;
    /** По данным записи возвращает признак того, может ли она быть раскрыта. */
    canRowExpand?(row: TRow): boolean;
    /** Двойной клик по строке. Получает текущую категорию и данные строчки в качестве первого аргумента
     * и react-router history в качестве второго.
     *
     * @example
     *
     onRowDoubleClick: ({ row }, router) => {
        router.push(`${ADMIN_STREAM_URL.CONSTITUTION}/${row.id}`);
      }
     */
    onRowDoubleClick?(config: IOnRowDoubleClick<TRow, TExecuterContext>, router: RouterProps['history']): void;
    /** Хлебные крошки. */
    breadcrumbs?: IBreadcrumbsExtended[];
    /** Высота скроллера. Если false, то высота скроллера будет во весь экран, даже если записей меньше. */
    compactHeight?: boolean;
    /**
     * Дополнительные параметры для действий со строками скроллера.
     * Будут переданы в дополнение к row в action.
     *
     * @example action: ({ done, fatal, addSucceeded }, { showLoader, hideLoader }) => async ([document], actionsParams) => ...
     */
    actionsParams?: unknown;
    /**
     * Функция для получения надписи о количестве выбранных элементов.
     *
     * Как правило импортируется из локалей.
     *
     * @example "scroller.account.selectedElementsInfo": {
      "@selected": "num",
      "@total": "num",
      "ru": {
        "selected === 0": "Счета не выбраны",
        "selected === 1": "Выбран 1 счет из {total}",
        "selected > 1 && selected < 5": "Выбрано {selected} счетов из {total}",
        "selected >= 5": "Выбрано {selected} счетов из {total}"
      }
    },
     * */
    getSelectedElementsInfo?(p: {
        selected: number;
        total: number;
    }): string;
}
/**
 * Получить скроллер строк.
 *
 * @param props Свойства.
 * @param props.fetcher Метод получения данных скроллера.
 * @param props.categories Категории.
 * @param props.filters Фильтр.
 * @param props.sorting Сортировка.
 * @param props.selectable Можно выделять строки.
 * @param props.executer Экшн экзекутор, через который будут исполняться действия скроллера и строк.
 * @param props.header Заголовок скроллера.
 * @param props.toolbarActions Действия для скроллера в тулбаре (поддерживаются вложенные экшны с полем children).
 * @param props.actionsVisibleCount Количество отображаемых действий.
 * @param props.columns Конфигурация колонок.
 * @param props.rowActions Экшны для строк.
 * @param props.rowDetailsTemplate Компонент для рендера содержимого строки в раскрытом состоянии.
 * @param props.mainLayout Главный слой.
 * @param props.canRowExpand Функция. По данным записи возвращает признак того, может ли она быть раскрыта.
 * @param props.onRowDoubleClick Двойной клик по строке. Получает текущую категорию и данные строчки.
 * @param props.placeholder Элемент, который будет показываться, если в таблице нет записей.
 * @param props.breadcrumbs Хлебные крошки.
 * @param props.hideHeader Видимость заголовка скроллера.
 * @param props.removeDefaultPadding Флаг, при проставлении которого убирается дефолтный паддниг скроллера.
 * @param props.compactHeight Убирает высоту скроллера на весь экран.
 * @param props.actionsParams Дополнительные параметры для действий со строками скроллера.
 * @param props.handleCategoryChange Метод смены категории.
 * @param props.getSelectedElementsInfo Функция для получения надписи о количестве выбранных элементов..
 *
 * @example
  export const ScrollerPage = getRowScrollerPage({
    fetcher: periodicMoneyTransferService.getList,
    categories: {
      fetcher: periodicMoneyTransferService.getCounter,
      locale: translator,
      defaultCategory: CATEGORY.ALL,
    },
    filters: {
      fields: FILTER_FIELDS,
      labels: FILTER_FIELD_LABELS,
      validate: validateFilters,
      component: Filter,
    },
    sorting: {
      byDefault: {
        direction: SORT_DIRECTION.DESC,
        fieldName: SORT_FIELD.DATE,
      },
      sortFields: SORT_FIELD_LABELS,
    },
    selectable: true,
    toolbarActions: TOOLBAR_ACTIONS,
    header: locale.scroller.bank.title,
    executer: moneyTransferExecutor,
    columns: [
      {
        title: columnt1Title,
        width: 100,
        selector(row) {
          return row.column1;
        },
      },
      {
        title: columnt2Title,
        width: 200,
        selector(row) {
          return row.column2;
        },
      },
    ],
    rowActions: CARD_ACTIONS,
    rowDetailsTemplate: ({row}) => row.id,
    onRowDoubleClick: ({ row }, router) => {
      router.push(`${ADMIN_STREAM_URL.CONSTITUTION}/${row.id}`);
    },
    breadcrumbs: [
      {
        label: locale.crl.header,
        tooltip: locale.crl.header,
        onClick: router => {
          router.push(ADMIN_STREAM_URL.CRL)
        },
      },
    ],
    handleCategoryChange: (params) => params.filters.onClear()
  })
 */
export declare const getRowScrollerPage: <TRow, TExecuterContext>({ rowActions, columns, rowDetailsTemplate: RowDetailsTemplate, canRowExpand, mainLayout, header, categories, filters, onRowDoubleClick, hideHeader, compactHeight, placeholder, breadcrumbs, actionsParams, handleCategoryChange, getSelectedElementsInfo, ...scrollerFeaturesConfig }: IRowScrollerGeneratorProps<TRow, TExecuterContext>) => React.FC<ScrollerPageProps>;
export {};
