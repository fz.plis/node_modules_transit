import React from 'react';
import type { IExecuter } from '@platform/core';
export declare const ScrollerExecuterContext: React.Context<IExecuter<any>>;
/** Хук, который можно использовать в дочерних элементах скроллера, чтобы получить доступ к executer-у. */
export declare const useScrollerExecuter: () => IExecuter<any>;
