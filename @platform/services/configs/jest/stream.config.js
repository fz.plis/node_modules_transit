module.exports = {
  testURL: 'http://localhost/',
  roots: ['<rootDir>/src'],
  modulePaths: ['node_modules', '<rootDir>/src'],
  setupFiles: ['@platform/services/configs/jest/setup-files/index'],
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  testMatch: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)(spec|test).[jt]s?(x)'],
  moduleNameMapper: {
    '^.+\\.(css|less|scss)$': 'identity-obj-proxy',
    '.+\\.(svg|png|jpg)$': 'identity-obj-proxy',
    // обновление jest поломало маппер, с его отключением все работает
    // '^(chains|domains|interfaces|utils|components|constant|localization|pages|services|pages)(.*)$': '<rootDir>/src/$1$2',
    '^@platform/(.*)\\.(.*)$': '@platform/$1.$2',
    '^@platform/ui.*$': '@platform/ui',
    '^@platform/core.*$': '@platform/core',
    '^@platform/services.*$': '@platform/services',
  },
  coverageReporters: ['text', 'text-summary', 'html', 'lcov'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
};
