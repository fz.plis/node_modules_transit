# core

Пакет содержащий все необходимо для работы стрима поректа DBO-BOX. Содержит все необходимые зависимости и webpack конфииг который умеет билдить стрим для продакшена и билдить стрим для локальной разработки (т.е. собирает приложение полностью вместе с root, ui и core)

# Как этим пользоваться?

Для использования данного пакета нужно:
* добавить его как зависимость (можно как dependency так и devDependency) в стрим.
* Запустить npm i
* Добавить в проект файл webpack.config.js и написать туда следующее
```
module.exports = require('@platform/core/stream-webpack')(<название стрима>);
```
* Для запуска билда проекта для выкладки на стенд надо запустить 
```
cross-env NODE_ENV=production webpack --progress
```

* Для запуска локальной разработки
```
cross-env MODE=spa NODE_ENV=DEVELOPMENT PROXY_TARGET=<адрес стенда> webpack-dev-server --progress
```

# Требования к стриму
* все исходники должны быть в папке /src в корне проекта
* в папке src должен быть файл входная точка index.tsx
* если в проекте есть локализация то все справочники должны быть в папке /dictionaries в корне проекта
* название стрима (то что передается в фукнцию в файле webpack.config.js) должно совпадать с первым уровнем роутов (т.е. если например стрим называется 'rupayment' то все роуты должны начинаться на '/rupayment/')
* название стрима пишется в lowercase!!!

# Переменная окружения MODE

Для указания пути билда (как полностью рабочее приложение включающее root, ui и core) нужно назначать значение 'spa' переменной окружения MODE
```
 MODE=spa
```
Иначе приложение билдится как модуль для встраивания в существующее приложение (т.е. в dist кладется папкас названием стрима а в нее все файлы стрима такие как index.js styles.css и т.д) далее эту папку можно копировать в папку /streams сущесвующего приложения.

# Переменная окружения ENV
Данная переменная указывает для какого окружения собирается проект. Может иметь 2 значения:PRODUCTION и DEVELOPMENT.

Если приложение собирается как PRODUCTION и MODE=spa тогда будет собрано spa приложения на основе production билда проекта root (т.е. минифицированный и реакт не будет кидать ворнинги).
В случае если приложение собирается как DEVELOPMENT и MODE=spa тогда приложение соберется на оснвое dev билда проекта root (root не будет минифицирован и реакт будет кидать ворнги)

# Переменная окружения PROXY_TARGET
Данную переменную имеет смысл использовать только в тандеме с переменной MODE со значение spa и запуском webpack-dev-server. Данной переменной нужно присваивать адрес сервера на который нужно проксировать все запросы начинающиеся на '/api' в режиме локальной разработки