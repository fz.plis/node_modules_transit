module.exports = [
  {
    react: 'react',
    'react-dom': 'react-dom',
    axios: 'axios',
    'react-router-dom': 'react-router-dom',
    classnames: 'classnames',
    dayjs: 'dayjs',
    'file-saver': 'file-saver',
    'final-form': 'final-form',
    'final-form-arrays': 'final-form-arrays',
    'react-final-form': 'react-final-form',
    'react-final-form-arrays': 'react-final-form-arrays',
    'react-query': 'react-query',
    '@platform/root': '@platform/root',
    '@platform/ui': '@platform/ui',
    '@platform/core': '@platform/core',
    '@platform/services': '@platform/services',
    '@platform/tools/localization': '@platform/tools/localization',
    '@platform/tools/istore': '@platform/tools/istore',
    '@platform/tools/istore-react': '@platform/tools/istore-react',
    '@platform/tools/stream-loader': '@platform/tools/stream-loader',
    '@platform/tools/date-time': '@platform/tools/date-time',
    '@platform/tools/big-number': '@platform/tools/big-number',
  },
  function (_, request, callback) {
    if (/^@platform\/ui\/.*/.test(request)) {
      return callback(null, '@platform/ui');
    }
    if (/^@platform\/ui-base\/.*/.test(request)) {
      return callback(null, '@platform/ui');
    }
    if (/^@platform\/core\/.*/.test(request)) {
      return callback(null, '@platform/core');
    }
    if (/^@platform\/services\/.*/.test(request)) {
      return callback(null, '@platform/services');
    }
    callback();
  },
];
