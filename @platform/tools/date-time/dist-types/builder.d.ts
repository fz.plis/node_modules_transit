import type { Dayjs, ConfigType } from 'dayjs';
export declare type Formatter<V, T = never> = (input: ConfigType, options?: T) => V;
declare const builder: <T extends (input: Dayjs, options?: any) => any>(factory: T, defaultVal: ReturnType<T>) => Formatter<ReturnType<T>, Parameters<T>[1]>;
export default builder;
