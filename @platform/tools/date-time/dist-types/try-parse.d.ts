import type { ConfigType, Dayjs } from 'dayjs';
declare const tryParse: (input: ConfigType) => Dayjs | undefined;
export default tryParse;
