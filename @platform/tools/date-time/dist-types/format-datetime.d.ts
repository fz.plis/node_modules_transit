import type { Formatter } from './builder';
export interface FormatDateTimeOptions {
    keepLocalTime?: boolean;
    format?: string;
}
declare const formatDateTime: Formatter<string, FormatDateTimeOptions>;
export default formatDateTime;
