declare const formatMonthShort: (val: number | string, locale?: string) => string;
export default formatMonthShort;
