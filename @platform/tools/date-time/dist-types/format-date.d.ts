import type { ConfigType } from 'dayjs';
declare const formatDate: (val: ConfigType | string) => string;
export default formatDate;
