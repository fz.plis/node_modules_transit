import type { Formatter } from './builder';
declare const dateWithEndOfDay: Formatter<string>;
export default dateWithEndOfDay;
