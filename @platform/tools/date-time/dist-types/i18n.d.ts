export declare type SupportedLocale = 'en-us' | 'en' | 'ru';
export declare const getLocale: () => string;
export declare const setLocale: (locale: SupportedLocale | string) => void;
