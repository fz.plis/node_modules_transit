import type { Formatter } from './builder';
declare const dateWithStartOfDay: Formatter<string>;
export default dateWithStartOfDay;
