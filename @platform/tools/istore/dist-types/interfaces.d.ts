export declare type Path = Array<number | string>;
export interface IDomainConfig<T, TMethods> {
    domain: Record<string, any>;
    methodsGetter(methods: any, state: IOnlyStates<T>): TMethods;
}
export interface IStore {
    addState(path: Path, initialState: any): void;
    removeState(path: Path): void;
    setState(path: Path, state: any): void;
    subscribe(name: string, callback: (val: any) => void, highPriority?: boolean): () => void;
    isStateExists(path: Path): boolean;
    getState(path?: Path): any;
    startBulkUpdate(): boolean;
    finishBulkUpdate(): void;
    getReduxStore(): {
        getState(): Object;
        dispatch(action: any): void;
    } | undefined;
}
export declare type MethodsDecoratorFunc = (methods: any, index: number) => any;
export interface IListUnitMethods<TState, TMethods> {
    [key: number]: TMethods;
    ['**ListUnitMethods**']: '1';
    push(state: TState): void;
    pop(): void;
    remove(index: number): void;
    insert(state: TState, index: number): void;
    set(items: TState[]): void;
    registerDecorator(decorator: MethodsDecoratorFunc): void;
    getIterator(): number[];
}
export interface IListUnitConfig<T> {
    initialState: Array<IOnlyStates<T>>;
    blockProvider(t: IOnlyStates<T>): T;
}
export interface IBlock<TState, TMethods> {
    state: TState;
    methods: TMethods;
    isMounted(): boolean;
    mount(prefix: string): IBlock<TState, TMethods>;
    unmount(): IBlock<TState, TMethods>;
    next(config?: any): IBlock<TState, any>;
}
export declare type GetState<T, TAlt> = T extends IBlock<infer TState, any> ? TState : TAlt;
export declare type IOnlyStates<T extends Record<string, any>> = {
    [P in keyof T]: GetState<T[P], IOnlyStates<T[P]>>;
};
export declare type GetMethods<T, TAlt> = T extends IBlock<any, infer TMethods> ? TMethods : TAlt;
export declare type IOnlyMethods<T extends Record<string, any>> = {
    [P in keyof T]: GetMethods<T[P], IOnlyMethods<T[P]>>;
};
export declare type UnitSubscription<T> = (state: T) => void;
export interface IMethodsGetterContext<TState> {
    getState(): TState;
    setState(state: TState): void;
}
export interface IUnitConfig<TState, TMethods> {
    isPermanent?: boolean;
    name?: string;
    initialState: TState;
    methodsGetter(context: IMethodsGetterContext<TState>): TMethods;
    storage?: IStore;
}
export declare type MethodsDecorator<T extends Record<string, any>, TDecorator> = TDecorator & {
    [P in keyof T]: T[P] extends any ? T[P] extends IListUnitMethods<infer TState, infer TMethods> ? IListUnitMethods<TState, MethodsDecorator<TMethods, TDecorator>> : T[P] extends Record<string, object | ((...args: any[]) => any)> ? MethodsDecorator<T[P], TDecorator> : T[P] : never;
};
export declare type IState = Record<string, any>;
