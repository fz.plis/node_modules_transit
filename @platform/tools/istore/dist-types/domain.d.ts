import type { IBlock, IOnlyStates, IOnlyMethods, IDomainConfig } from './interfaces';
export declare class Domain<T, TMethods> implements IBlock<IOnlyStates<T>, TMethods> {
    get state(): IOnlyStates<T>;
    get methods(): TMethods;
    config: IDomainConfig<T, TMethods>;
    private currentState?;
    private currentMethods?;
    private itemsToUnmount?;
    constructor(config: IDomainConfig<T, TMethods>);
    isMounted: () => boolean;
    mount(prefix: string): this;
    unmount(): this;
    withMethods: <TNextMethods>(getter: (methods: TMethods, state: IOnlyStates<T>) => TNextMethods) => Domain<T, TNextMethods>;
    extend: <TExtended>(extender: TExtended & { [P in keyof T]?: undefined; }) => Domain<T & TExtended, IOnlyMethods<TExtended> & TMethods>;
    next: <TNextMethods = TMethods, NextT = T>(config: Partial<IDomainConfig<NextT, TNextMethods>>) => Domain<NextT, TNextMethods>;
}
export declare const domain: <T>(d: T) => Domain<T, IOnlyMethods<T>>;
