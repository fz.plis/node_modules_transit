export declare const request: <TResp, TParams extends any[]>(loader: (...p: TParams) => Promise<TResp>, initialResponse?: TResp | undefined) => import("..").Unit<{
    response: TResp | undefined;
    sending: boolean;
    error: undefined;
    count: number;
}, {
    send: (...loadDataParams: TParams) => Promise<TResp>;
}>;
