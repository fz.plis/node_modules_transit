export declare const value: <T>(initialState: T) => import("../unit").Unit<T, {
    set: (val: T) => void;
}>;
