export declare const LOCALE_CODE: {
    RU: string;
    EN_US: string;
};
export declare const SIZE_DESIGNATIONS: {
    [x: string]: {
        MB: string;
        KB: string;
        B: string;
    };
};
export declare const SIZE_DECIMAL = 2;
