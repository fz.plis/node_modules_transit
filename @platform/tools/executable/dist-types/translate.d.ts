export declare const format: (template: string, data: any, throwIfDataNotFound?: boolean) => string;
export declare const translate: (area: string, key: string, params?: {
    [key: string]: string | number;
} | undefined) => string;
