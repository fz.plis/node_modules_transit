export declare const truncated: (num: number, decimal?: number) => number;
export declare const getParamsStr: (params: {
    [key: string]: any;
}) => string;
export declare const runExpression: (paramsStr: string, body: string) => any;
export declare const findThrulyExpression: (params: {
    [key: string]: any;
}, expressions: string[]) => string | undefined;
