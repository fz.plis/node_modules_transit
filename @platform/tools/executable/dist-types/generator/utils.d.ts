export declare const getDataForLangFile: (locale: Record<string, Record<string, any>>, lang: string) => {};
export declare const getLocaleWithExtends: (locales: Record<string, Record<string, any> | string[] | string>) => {};
export declare const localesToObject: (locales: Record<string, Record<string, any> | string>) => any;
export declare const getType: (typeAlias: string) => string;
export declare const getDefaultValueForType: (typeAlias: string) => string | number;
export interface IParamInfo {
    name: string;
    type: string;
}
export declare const checkValue: (val: string, params: any) => void;
export declare type IObj<T> = Record<string, T>;
export declare const checkExpressions: (params: IObj<unknown>, expressions: IObj<string>, path: string) => void;
export declare const checkParams: (paramsConfig: IParamInfo[], values: any[], path: string) => void;
