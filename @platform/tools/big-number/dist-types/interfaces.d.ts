export interface IConfig {
    locale: string;
}
export interface ICurrency {
    thousandsSeparator: string;
    decimalSeparator: string;
    decimalDigits: number;
    format?: string;
}
