export declare const currencies: {
    [x: string]: {
        thousandsSeparator: string;
        decimalSeparator: string;
        decimalDigits: number;
        format: string;
    };
};
