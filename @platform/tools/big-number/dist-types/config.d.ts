export declare const getLocale: () => string;
export declare const setLocale: (locale: string) => void;
