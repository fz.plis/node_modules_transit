import { BigNumber } from 'bignumber.js';
export { BigNumber } from 'bignumber.js';
export * from './config';
export declare const bigNumber: (value: BigNumber.Value, base?: number | undefined) => BigNumber;
export declare const formatMoney: (numberIn: number | string, locale?: string) => string;
export declare enum CURRENCY_CODES {
    RUB = "RUB",
    USD = "USD",
    EUR = "EUR"
}
export declare const moneyInWords: (sum: BigNumber.Value, currCode?: CURRENCY_CODES) => string;
export declare const moneyInRub: (sum: BigNumber.Value) => string;
