import React from 'react';
import type { IRootWrappedComponentProps, IAsRootParams } from './interfaces';
export declare const asRootHoc: (config: IAsRootParams<any>) => <T>(Component: React.ComponentType<T>) => React.ForwardRefExoticComponent<React.PropsWithoutRef<IRootWrappedComponentProps & T> & React.RefAttributes<any>>;
