export declare const useDomain: <TRet>(domainId: string, selector: (state: any, methods: any) => TRet) => TRet;
