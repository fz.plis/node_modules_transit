import type { ComponentType } from 'react';
export declare type DomainGetter<T, P> = (props: P) => T;
export declare type ChangePropsHandler<T, P> = (domain: T, next: P, prev: P, isInit: boolean) => void;
export interface IDomain<TState, TMethods> {
    state: TState;
    methods: TMethods;
    mount?(name: string): void;
    unmount?(): void;
    isMounted?(): boolean;
}
export interface IExtraLockDomain<T, P> {
    onMount?(domain: T, props: P): void;
    onChangeProps?(domain: T, nextProps: P, prevProps: P): void;
}
export interface IRootWrappedComponentProps {
    recursiveRootComponent?: boolean;
}
export interface Selector<T> {
    selector(context: any): T;
}
export interface ICallFuncSelector<TMethods> {
    <T, TArgs extends any[]>(selector: (m: TMethods) => (...args: TArgs) => T): Selector<(...args: TArgs) => T>;
    <TArgs extends any[]>(selector: (m: TMethods) => (...args: TArgs) => undefined | void, ...rest: Array<(m: TMethods) => (...args: TArgs) => undefined | void>): Selector<(...args: TArgs) => void>;
}
export interface IAsRootParams<TDomain> {
    domainOrGetter?: DomainGetter<TDomain, any> | TDomain;
    nameOrGetter?: string | ((props: any) => string);
    domainId?: string;
    rootChangePropsCallback: ChangePropsHandler<TDomain, any>;
}
export interface ILifecicleProps<TProps> {
    onInitHook?: Selector<(props: TProps) => void>;
    onChangePropsHook?: Selector<(props: TProps, prev: TProps) => void>;
}
export declare type WithSelectors<T extends Record<string, any>> = {
    [P in keyof T]: Selector<T[P]> | T[P];
};
export declare type InjectableComponent<T> = T extends ComponentType<infer TProps> ? ComponentType<ILifecicleProps<TProps> & WithSelectors<TProps>> : T;
export declare type IMappedComponents<T extends Record<string, any>> = {
    [P in keyof T]: InjectableComponent<T[P]>;
};
export declare type Shape<T> = Partial<T> & Record<string, any>;
export interface IJoin {
    <T, T2 extends Shape<T>>(arg1: T, arg2: T2): T & T2;
    <T, T2 extends Shape<T>, T3 extends Shape<T & T2>>(arg1: T, arg2: T2, arg3: T3): T & T2 & T3;
    <T, T2 extends Shape<T>, T3 extends Shape<T & T2>, T4 extends Shape<T & T2 & T3>>(arg1: T, arg2: T2, arg3: T3, arg4: T4): T & T2 & T3 & T4;
}
