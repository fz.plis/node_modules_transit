/// <reference types="react" />
import type { IDomain, DomainGetter, IExtraLockDomain, ICallFuncSelector, IJoin } from './interfaces';
export declare const isSelector: (s: any) => any;
export declare const isFuncSelector: (s: any) => any;
export declare const getSelectors: <T extends IDomain<any, any>, P = {}>(nameOrGetter: string | ((props: P) => string), domainOrGetter: T | DomainGetter<T, P>, extra?: IExtraLockDomain<T, P>) => {
    asRoot: <T_1>(Component: import("react").ComponentType<T_1>) => import("react").ForwardRefExoticComponent<import("react").PropsWithoutRef<import("./interfaces").IRootWrappedComponentProps & T_1> & import("react").RefAttributes<any>>;
    call: ICallFuncSelector<T["methods"]>;
    join: IJoin;
    bind: <TRet>(getter: (s: T["state"], m: T["methods"]) => TRet) => {
        selector: (context: any) => TRet;
    };
    useDomain: <TRet_1>(selector: (s: T["state"], m: T["methods"]) => TRet_1) => TRet_1;
};
