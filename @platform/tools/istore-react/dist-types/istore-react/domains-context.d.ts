import * as React from 'react';
export declare type IDomainsContext = Record<string, any>;
export declare const DomainsContext: React.Context<IDomainsContext>;
