import React from 'react';
import type { IMappedComponents, ILifecicleProps } from './interfaces';
declare type Props = ILifecicleProps<any> & Record<string, any>;
export declare const withInjectableProps: (Comp: React.ElementType) => React.ForwardRefExoticComponent<Pick<Props, string> & React.RefAttributes<any>>;
export declare const injectable: <T extends Record<string, any>>(components: T) => IMappedComponents<T>;
export {};
