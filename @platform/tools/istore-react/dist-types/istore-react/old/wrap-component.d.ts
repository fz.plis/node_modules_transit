import * as React from 'react';
import type { IWrapChainConfig, IDomain, IExtraProps, WrappedComponentType } from './interfaces';
export declare type IWrappedComponentProps = IExtraProps;
export interface IRootWrappedComponentProps extends IWrappedComponentProps {
    recursiveRootComponent?: boolean;
}
export declare const wrapComponent: <TResultProps, TProvidedProps, TDomain extends IDomain<any, any>>(config: IWrapChainConfig<TDomain>) => (componentToWrap: React.ComponentType<any>) => WrappedComponentType<TResultProps, TProvidedProps>;
