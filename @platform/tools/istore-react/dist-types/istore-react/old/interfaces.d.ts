/// <reference types="react" />
import type { IDomain, ChangePropsHandler, DomainGetter } from '../interfaces';
export * from '../interfaces';
export interface IPropsGetter {
    getter: PropsPorivder<any, any, any>;
    domainId?: string;
}
export interface IPropsGetterContext<TProps, TScope> {
    getProps(): TProps;
    getScope(): TScope;
}
export interface IWrapChainConfig<TDomain extends IDomain<any, any>> {
    propsGetters: IPropsGetter[];
    changePropsCallback: ChangePropsHandler<TDomain, any>;
    hocs: Array<(c: React.ComponentType<any>) => React.ComponentType<any>>;
    domainOrGetter?: DomainGetter<TDomain, any> | TDomain;
    nameOrGetter?: string | ((props: any) => string);
    domainId?: string;
    asRoot?: boolean;
    rootChangePropsCallback: ChangePropsHandler<TDomain, any>;
}
export declare type PropsPorivder<TDomain extends IDomain<any, any>, T, TScope> = (state: TDomain['state'], methods: TDomain['methods'], context: IPropsGetterContext<any, TScope>) => T;
export interface IExtraProps {
    innerRef?(ref: any): void;
}
export declare type WrappedComponentType<TProps, TProvidedProps> = React.ComponentType<TProps> & {
    ProvidedProps: TProvidedProps;
};
