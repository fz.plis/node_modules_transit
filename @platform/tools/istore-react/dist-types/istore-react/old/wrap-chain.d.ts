import type { ComponentType } from 'react';
import type { IWrapChainConfig, PropsPorivder, ChangePropsHandler, IExtraLockDomain, IExtraProps, DomainGetter, IDomain } from './interfaces';
export declare abstract class BaseChain<TProvidedProps, TDomain extends IDomain<any, any>, TScope = {}> {
    config: IWrapChainConfig<TDomain>;
    constructor(config: IWrapChainConfig<TDomain>);
    onChangeProps: (nextOnChangeProps: ChangePropsHandler<TDomain, any>) => this;
    withHOC: (hoc: (c: ComponentType<any>) => ComponentType<any>) => this;
    component: <TProps extends Partial<TProvidedProps>>(component: ComponentType<TProps>) => import("./interfaces").WrappedComponentType<IExtraProps & Omit<TProps, keyof TProvidedProps>, TProvidedProps>;
    rootComponent: <TProps extends Partial<TProvidedProps>>(component: ComponentType<TProps>) => ComponentType<IExtraProps & Omit<TProps, keyof TProvidedProps>>;
    protected abstract createInstance(config: IWrapChainConfig<TDomain>): any;
    protected withScope<T>(_: ComponentType<T>): any;
    protected withProps<T>(propsGetter: PropsPorivder<TDomain, T, TScope>): any;
    protected join<T>(chain: BaseChain<T, any>): any;
    private next;
}
export declare class WrapChain<TProvidedProps, TDomain extends IDomain<any, any>, TScope> extends BaseChain<TProvidedProps, TDomain, TScope> {
    withProps<T>(propsGetter: PropsPorivder<TDomain, T, TScope>): WrapChain<T & TProvidedProps, TDomain, TScope>;
    join<T>(chain: WrapChain<T, any, any>): WrapChain<T & TProvidedProps, TDomain, TScope>;
    withScope<T>(s: ComponentType<T>): WrapChain<TProvidedProps, TDomain, T & TScope>;
    protected createInstance(config: IWrapChainConfig<TDomain>): WrapChain<unknown, TDomain, unknown>;
}
export declare const wrap: WrapChain<{}, {
    state: {};
    methods: {};
}, {}>;
export declare const lockDomain: <T extends IDomain<any, any>, P = {}>(nameOrGetter: string | ((props: P) => string), domainOrGetter: T | DomainGetter<T, P>, extra?: IExtraLockDomain<T, P>) => WrapChain<{}, T, {}>;
