import * as React from 'react';
export declare const getScope: <T extends Record<string, any>>() => React.SFC<T>;
