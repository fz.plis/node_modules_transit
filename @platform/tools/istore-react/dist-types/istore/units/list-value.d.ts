export declare const listValue: <T>(initialState?: T[]) => import("..").Unit<T[], {
    toggleItem: (item: T) => void;
    set: (val: T[]) => void;
    setItem: (index: number, item: T) => void;
    add: (item: T | T[]) => void;
    remove: (item: T | T[]) => void;
    removeByIndex: (index: number) => void;
    mergeToItem: (index: number, obj: Partial<T>) => void;
}>;
