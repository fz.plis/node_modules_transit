export declare const object: <T extends Record<string, any>>(initialState: T) => import("..").Unit<T, {
    merge: (updates: Partial<T>) => void;
    set: (val: T) => void;
}>;
