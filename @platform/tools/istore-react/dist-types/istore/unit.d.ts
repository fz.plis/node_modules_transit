import type { IBlock, IUnitConfig, IMethodsGetterContext, UnitSubscription, IStore, Path } from './interfaces';
export declare class Unit<TState, TMethods> implements IBlock<TState, TMethods> {
    get state(): TState;
    get methods(): Readonly<TMethods>;
    name?: string;
    path?: Path;
    config: IUnitConfig<TState, TMethods>;
    private currentState?;
    private currentMethods?;
    private unsubFromStorage?;
    private storage?;
    constructor(config: IUnitConfig<TState, TMethods>);
    isMounted: () => boolean;
    mount(externalName?: string): this;
    unmount(): this;
    subscribe: (subscription: UnitSubscription<TState>) => () => void;
    withMethods: <T>(m: (methods: TMethods, c: IMethodsGetterContext<TState>) => T) => Unit<TState, T>;
    asPermanent: () => Unit<TState, TMethods>;
    asNotPermanent: () => Unit<TState, TMethods>;
    withName: (name: string) => Unit<TState, TMethods>;
    withStore: (storage: IStore) => Unit<TState, TMethods>;
    next: <TNextMethods = TMethods>(config: Partial<IUnitConfig<TState, TNextMethods>>) => Unit<TState, TNextMethods>;
}
export declare const unit: <TState>(initialState: TState) => Unit<TState, {}>;
