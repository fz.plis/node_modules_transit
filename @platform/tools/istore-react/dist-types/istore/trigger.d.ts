export declare const trigger: (obj: any) => void;
export declare const start: (onTrigger?: ((obj: any) => void) | undefined) => void;
export declare const stop: () => void;
export declare const getTriggered: () => any[];
