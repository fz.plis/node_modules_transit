import type { IBlock, IListUnitMethods, UnitSubscription, IListUnitConfig } from './interfaces';
export declare class ListUnit<T extends IBlock<any, any>> implements IBlock<Array<T['state']>, IListUnitMethods<T['state'], T['methods']>> {
    get state(): Array<T['state']>;
    get methods(): IListUnitMethods<T['state'], T['methods']>;
    config: IListUnitConfig<T>;
    private currentState;
    private methodDecorators;
    private currentMethods;
    private name?;
    private items;
    private storage?;
    constructor(config: IListUnitConfig<T>);
    mount(prefix: string): this;
    unmount(): this;
    subscribe: (subscription: UnitSubscription<Array<T['state']>>) => () => void;
    isMounted: () => boolean;
    next: (config: Partial<IListUnitConfig<T>>) => ListUnit<T>;
    private remove;
    private insert;
    private push;
    private pop;
    private withBulkUpdate;
}
export declare const listUnit: <T extends IBlock<any, any>>(blockProvider: (t: T["state"]) => T, initialState?: T["state"][]) => ListUnit<T>;
