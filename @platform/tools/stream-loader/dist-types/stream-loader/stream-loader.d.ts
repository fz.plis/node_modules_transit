export declare const defaultFileNameGetter: (stream: string, file: string, hash?: string | undefined) => string;
export declare const setFileNameGetter: (getter: typeof defaultFileNameGetter) => void;
export declare const defaultLocaleGetter: (hash: string) => (locale: string, area: string) => Promise<{}>;
export declare const setLocaleLoader: (getter: typeof defaultLocaleGetter) => void;
export interface IGetStreamInfoResp {
    version: string;
    hash: string;
    locales: string[];
    hasStyles: boolean;
}
export declare const subscribeOnLoadStream: (sub: (...streams: any[]) => void) => () => void;
export declare const getLoadedStreamInfos: () => {
    name: string;
    version: string;
    hash: string;
    locales: string[];
    hasStyles: boolean;
}[];
export declare const getStreams: (...streamNames: string[]) => Promise<any[]>;
export declare const getStream: <T>(name: string) => Promise<T>;
export declare const getLoadedStreams: () => Record<string, any>;
