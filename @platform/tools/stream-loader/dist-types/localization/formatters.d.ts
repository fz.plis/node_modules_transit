export declare const formatAccountCode: (code: string) => string;
export declare const formatSizeMb: (byte: number | string, locale?: string) => string;
export declare const formatSizeKb: (byte: number | string, locale?: string) => string;
export declare const formatSize: (byte: number | string, locale?: string) => string;
declare type Formatter = (value: any, locale: string) => string;
export declare const registerFormatter: (formatterName: string, formater: Formatter) => void;
export declare const getFormatterByName: (formatterName: string) => Formatter;
export {};
