import { ITranslations, Loader } from './interfaces';
export declare const getLocale: () => string;
export declare const getTranslationsByArea: (area: string) => ITranslations;
export declare const setLocale: (locale: string) => Promise<ITranslations[]>;
export declare const registerArea: (area: string, loader: Loader) => Promise<ITranslations>;
export declare const unregisterArea: (area: string) => void;
export declare const localeChangeSubscribe: (func: (locale: string) => void) => () => boolean;
