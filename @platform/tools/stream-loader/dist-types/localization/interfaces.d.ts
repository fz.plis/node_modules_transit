export interface ITranslations {
    [key: string]: string | {
        [key: string]: string;
    };
}
export declare type Loader = (locale: string, area: string) => Promise<ITranslations> | ITranslations;
export interface IAreas {
    loader: Loader;
    localeValues: ITranslations;
}
export interface IConfig {
    areas: {
        [key: string]: IAreas;
    };
    locale: string;
}
