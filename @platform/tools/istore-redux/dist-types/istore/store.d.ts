import type { IStore, Path, IState } from './interfaces';
export declare class Store implements IStore {
    protected storage: IState;
    protected reduxStore: {
        getState(): object;
        dispatch(action: any): void;
    } | undefined;
    protected subs: Record<string, Array<(state: any) => void>>;
    protected highPrioritySubs: Record<string, Array<(state: any) => void>>;
    protected bulkUpdate: boolean;
    protected bulkUpdateItems: Path[];
    getReduxStore: () => {
        getState(): object;
        dispatch(action: any): void;
    } | undefined;
    addState: (path: Path, initialState: any) => void;
    removeState: (path: Path) => void;
    setState: (path: Path, state: object) => void;
    subscribe: (name: string, subscription: (val: any) => void, highPriority?: boolean) => () => void;
    isStateExists: (path: Path) => boolean;
    getState: (path?: Path | undefined) => any;
    startBulkUpdate(): boolean;
    finishBulkUpdate(): void;
    protected trigger(...paths: Path[]): void;
}
export declare const getDefaultStore: () => IStore;
export declare const setDefaultStore: (nextStore: IStore) => void;
