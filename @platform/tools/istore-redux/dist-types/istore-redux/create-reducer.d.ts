import type { Reducer, IAction } from './interfaces';
export declare const createReducer: <T>(initialState: T, handlers: Record<string, Reducer<T, any>>) => (state: T, action: IAction<any>) => T;
