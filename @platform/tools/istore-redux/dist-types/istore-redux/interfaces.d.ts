export interface IAction<T extends object> {
    readonly type: string;
    readonly payload: Readonly<T>;
}
export declare type ActionCreator<T extends object> = (...params: any[]) => IAction<T>;
export declare type Reducer<T, TPayload extends object> = (state: T, action: IAction<TPayload>) => T;
export interface IReduxStore {
    getState(): object;
    dispatch(action: any): void;
}
