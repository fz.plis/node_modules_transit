import type { Path } from '../istore';
import { Store } from '../istore';
import type { IReduxStore } from './interfaces';
export declare class ReduxStore extends Store {
    private dispatch;
    private getIStoreState;
    constructor(store: IReduxStore, name?: string);
    addState: (path: Path, intialState: any) => void;
    removeState: (path: Path) => void;
    setState: (path: Path, state: object) => void;
}
