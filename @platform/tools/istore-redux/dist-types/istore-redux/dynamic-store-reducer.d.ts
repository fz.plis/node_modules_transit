import type { Path } from '../istore';
export declare const ADD_STORE = "dynamic/addStore";
export declare const DELETE_STORE = "dynamic/deleteStore";
export declare const SET_DYNAMIC_STORE_STATE = "dynamic/setDynamicStoreState";
export declare const addStore: (path: Path, initialState: any) => {
    type: string;
    payload: {
        path: Path;
        initialState: any;
    };
};
export declare const deleteStore: (path: Path) => {
    type: string;
    payload: {
        path: Path;
    };
};
export declare const setDynamicStoreState: (path: Path, nextState: object) => {
    type: string;
    payload: {
        path: Path;
        nextState: object;
    };
};
export declare const dynamicStoreInitialState: {};
export declare const dynamicReducerActionsHandlers: {
    "dynamic/setDynamicStoreState": (state: any, { payload: { path, nextState } }: any) => object;
    "dynamic/addStore": (state: any, { payload: { path, initialState } }: any) => object;
    "dynamic/deleteStore": (state: any, { payload: { path } }: any) => object;
};
export declare const dynamicStoreReducer: (state: object, action: import("./interfaces").IAction<any>) => object;
