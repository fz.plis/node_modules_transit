var crypto = require('crypto')
var fs = require('fs')

module.exports = {
  BundleToFunction: function (options) {
    const moduleName = options.moduleName
    const idDev = options.isDev
    if (!moduleName) {
      throw new Error('moduleName is required!')
    }

    // looking for root["rupayment"] = factory(...)
    const devStrToReplace = new RegExp(`(root\\["${moduleName}"\\] = )(factory\\(.[^\\)]*\\))`)
    const prodStrsToReplace = new RegExp(`:(.\\.${moduleName}=)(.\\(.[^\\)]*\\))`) // lookin for :e.moduleMame=t(...)
    return {
      apply: compiler => {
        compiler.hooks.emit.tap('emitPlugin', compilation => {
          const src = compilation.assets['index.js'].source()
          compilation.assets['index.js'].source = () => {
            if (idDev) {
              return src.replace(devStrToReplace, (...args) => {
                return `${args[1]}function(){${args[1]}${args[2]}}`
              })
            } else {
              return src.replace(prodStrsToReplace, (...args) => {
                return `:${args[1]}function(){${args[1]}${args[2]}}`
              })
            }
          }
        })
      },
    }
  },
  StreamInfo: function (options) {
    const dictionariesPath = options.dictionariesPath
    return {
      apply: compiler => {
        compiler.hooks.emit.tap('emitPlugin', compilation => {
          let locales = []
          if (fs.existsSync(dictionariesPath)) {
            locales = fs.readdirSync(dictionariesPath).map(file => file.toString().split('.')[1])
          }

          const data = {
            version: process.env.npm_package_version,
            hash: crypto.createHash('md5').update(new Date().toString()).digest('hex'),
            locales,
            hasStyles: !!Array.from(compilation.fileDependencies).find(f => /(\.scss|\.css)$/.test(f)),
          }

          compilation.assets[`./info.json`] = {
            size: function () {
              return 0
            },
            source: function () {
              return JSON.stringify(data)
            },
          }
        })
      },
    }
  },
  IndexDtsGenerator: function (options) {
    return {
      apply: compiler => {
        compiler.hooks.emit.tap('emitPlugin', compilation => {
          compilation.assets[`./index.d.ts`] = {
            size: function () {
              return 0
            },
            source: function () {
              return `export * from './dist-types/${options.folder}'`
            },
          }
        })
      },
    }
  },
}
