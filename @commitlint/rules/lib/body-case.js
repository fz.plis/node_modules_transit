"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.bodyCase = void 0;
const ensure_1 = require("@commitlint/ensure");
const message_1 = __importDefault(require("@commitlint/message"));
const bodyCase = (parsed, when = 'always', value = undefined) => {
    const { body } = parsed;
    if (!body) {
        return [true];
    }
    const negated = when === 'never';
    const result = ensure_1.case(body, value);
    return [
        negated ? !result : result,
        message_1.default([`body must`, negated ? `not` : null, `be ${value}`]),
    ];
};
exports.bodyCase = bodyCase;
//# sourceMappingURL=body-case.js.map