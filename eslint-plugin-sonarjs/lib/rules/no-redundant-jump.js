"use strict";
/*
 * eslint-plugin-sonarjs
 * Copyright (C) 2018 SonarSource SA
 * mailto:info AT sonarsource DOT com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
// https://jira.sonarsource.com/browse/RSPEC-3626
const nodes_1 = require("../utils/nodes");
const message = "Remove this redundant jump.";
const loops = "WhileStatement, ForStatement, DoWhileStatement, ForInStatement, ForOfStatement";
const rule = {
    meta: {
        type: "suggestion",
    },
    create(context) {
        function reportIfLastStatement(node) {
            const withArgument = node.type === "ContinueStatement" ? !!node.label : !!node.argument;
            if (!withArgument) {
                const block = nodes_1.getParent(context);
                if (block.body[block.body.length - 1] === node && block.body.length > 1) {
                    context.report({
                        message,
                        node,
                    });
                }
            }
        }
        function reportIfLastStatementInsideIf(node) {
            const ancestors = context.getAncestors();
            const ifStatement = ancestors[ancestors.length - 2];
            const upperBlock = ancestors[ancestors.length - 3];
            if (upperBlock.body[upperBlock.body.length - 1] === ifStatement) {
                reportIfLastStatement(node);
            }
        }
        return {
            [`:matches(${loops}) > BlockStatement > ContinueStatement`]: (node) => {
                reportIfLastStatement(node);
            },
            [`:matches(${loops}) > BlockStatement > IfStatement > BlockStatement > ContinueStatement`]: (node) => {
                reportIfLastStatementInsideIf(node);
            },
            ":function > BlockStatement > ReturnStatement": (node) => {
                reportIfLastStatement(node);
            },
            ":function > BlockStatement > IfStatement > BlockStatement > ReturnStatement": (node) => {
                reportIfLastStatementInsideIf(node);
            },
        };
    },
};
module.exports = rule;
//# sourceMappingURL=no-redundant-jump.js.map