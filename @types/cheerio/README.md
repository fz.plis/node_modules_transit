# Installation
> `npm install --save @types/cheerio`

# Summary
This package contains type definitions for Cheerio (https://github.com/cheeriojs/cheerio).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/cheerio.

### Additional Details
 * Last updated: Tue, 06 Jul 2021 18:05:51 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Bret Little](https://github.com/blittle), [VILIC VANE](http://vilic.info), [Wayne Maurer](https://github.com/wmaurer), [Umar Nizamani](https://github.com/umarniz), [LiJinyao](https://github.com/LiJinyao), [Chennakrishna](https://github.com/chennakrishna8), [AzSiAz](https://github.com/AzSiAz), [Ryo Ota](https://github.com/nwtgck), [Hiroki Osame](https://github.com/privatenumber), and [Artishevskiy Alexey](https://github.com/dhvcc).
