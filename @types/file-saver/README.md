# Installation
> `npm install --save @types/file-saver`

# Summary
This package contains type definitions for FileSaver.js ( https://github.com/eligrey/FileSaver.js/ ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/file-saver

Additional Details
 * Last updated: Tue, 21 May 2019 19:56:14 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Cyril Schumacher <https://github.com/cyrilschumacher>, Daniel Roth <https://github.com/DaIgeb>, Chris Barr <https://github.com/chrismbarr>, HitkoDev <https://github.com/HitkoDev>.
