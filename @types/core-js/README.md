# Installation
> `npm install --save @types/core-js`

# Summary
This package contains type definitions for core-js (https://github.com/zloirock/core-js/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/core-js.

### Additional Details
 * Last updated: Tue, 08 Sep 2020 18:11:12 GMT
 * Dependencies: none
 * Global values: `$for`, `Dict`, `core`, `delay`

# Credits
These definitions were written by [Ron Buckton](https://github.com/rbuckton), and [Michel Felipe](https://github.com/mfdeveloper).
