# Installation
> `npm install --save @types/react-window-infinite-loader`

# Summary
This package contains type definitions for react-window-infinite-loader (https://github.com/bvaughn/react-window-infinite-loader/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-window-infinite-loader.

### Additional Details
 * Last updated: Mon, 13 Apr 2020 23:37:54 GMT
 * Dependencies: [@types/react](https://npmjs.com/package/@types/react), [@types/react-window](https://npmjs.com/package/@types/react-window)
 * Global values: none

# Credits
These definitions were written by [Nivesh Ravindran](https://github.com/Nibblesh), and [fnknzzz](https://github.com/fnknzzz).
