# Installation
> `npm install --save @types/enzyme`

# Summary
This package contains type definitions for Enzyme (https://github.com/airbnb/enzyme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/enzyme.

### Additional Details
 * Last updated: Wed, 28 Oct 2020 18:21:17 GMT
 * Dependencies: [@types/react](https://npmjs.com/package/@types/react), [@types/cheerio](https://npmjs.com/package/@types/cheerio)
 * Global values: none

# Credits
These definitions were written by [Marian Palkus](https://github.com/MarianPalkus), [Cap3](http://www.cap3.de), [Ivo Stratev](https://github.com/NoHomey), [jwbay](https://github.com/jwbay), [huhuanming](https://github.com/huhuanming), [MartynasZilinskas](https://github.com/MartynasZilinskas), [Torgeir Hovden](https://github.com/thovden), [Martin Hochel](https://github.com/hotell), [Christian Rackerseder](https://github.com/screendriver), [Mateusz Sokoła](https://github.com/mateuszsokola), [Braiden Cutforth](https://github.com/braidencutforth), [Erick Zhao](https://github.com/erickzhao), [Jack Tomaszewski](https://github.com/jtomaszewski), and [Jordan Harband](https://github.com/ljharb).
