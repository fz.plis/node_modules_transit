# Installation
> `npm install --save @types/react-custom-scrollbars`

# Summary
This package contains type definitions for react-custom-scrollbars (https://github.com/malte-wessel/react-custom-scrollbars).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-custom-scrollbars.

### Additional Details
 * Last updated: Wed, 12 Aug 2020 00:54:29 GMT
 * Dependencies: [@types/react](https://npmjs.com/package/@types/react)
 * Global values: `ReactCustomScrollbars`

# Credits
These definitions were written by [ David-LeBlanc-git](https://github.com/David-LeBlanc-git), and [kittimiyo](https://github.com/kittimiyo).
