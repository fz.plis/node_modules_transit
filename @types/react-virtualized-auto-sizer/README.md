# Installation
> `npm install --save @types/react-virtualized-auto-sizer`

# Summary
This package contains type definitions for react-virtualized-auto-sizer ( https://github.com/bvaughn/react-virtualized-auto-sizer/ ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-virtualized-auto-sizer

Additional Details
 * Last updated: Sat, 30 Mar 2019 02:51:36 GMT
 * Dependencies: @types/react
 * Global values: none

# Credits
These definitions were written by Hidemi Yukita <https://github.com/otofu-square>.
