interface LinkOptions {
    addDecorator?: boolean;
}
export declare function managerEntries(entry?: any[]): any[];
export declare function config(entry?: any[], { addDecorator }?: LinkOptions): any[];
export {};
