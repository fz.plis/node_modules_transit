import { NpmOptions } from '../../NpmOptions';
import { JsPackageManager } from '../../js-package-manager';
declare const _default: (packageManager: JsPackageManager, parser: string, npmOptions: NpmOptions) => Promise<void>;
export default _default;
