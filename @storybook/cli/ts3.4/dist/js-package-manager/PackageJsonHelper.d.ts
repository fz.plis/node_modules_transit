import { PackageJson } from './PackageJson';
export declare function readPackageJson(): PackageJson | false;
export declare function writePackageJson(packageJson: PackageJson): void;
