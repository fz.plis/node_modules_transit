interface Options {
    hasTSDependency: boolean;
}
export declare const warn: ({ hasTSDependency }: Options) => void;
export {};
