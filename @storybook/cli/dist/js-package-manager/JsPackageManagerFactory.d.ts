import { JsPackageManager } from './JsPackageManager';
export declare class JsPackageManagerFactory {
    static getPackageManager(forceNpmUsage?: boolean): JsPackageManager;
}
