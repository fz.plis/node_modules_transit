export declare function getAngularAppTsConfigPath(): any;
export declare function getAngularAppTsConfigJson(): any;
export declare function editStorybookTsConfig(tsconfigPath: string): void;
export declare function isDefaultProjectSet(): boolean;
export declare function getBaseTsConfigName(): Promise<"tsconfig.base.json" | "tsconfig.json">;
