import { SupportedFrameworks } from '../project_types';
export declare function configure(framework: SupportedFrameworks, addons: string[], custom?: any): void;
