"use strict";

require("core-js/modules/es.promise");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _baseGenerator = require("../baseGenerator");

const generator = async (packageManager, npmOptions, options) => {
  await (0, _baseGenerator.baseGenerator)(packageManager, npmOptions, options, 'marionette');
};

var _default = generator;
exports.default = _default;