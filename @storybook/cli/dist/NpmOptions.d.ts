export declare type NpmOptions = {
    skipInstall?: boolean;
    installAsDevDependencies?: boolean;
};
