import { MutationFilters } from '../core/utils';
export declare function useIsMutating(filters?: MutationFilters): number;
