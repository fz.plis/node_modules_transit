import { Persistor } from '../persistQueryClient-experimental';
interface CreateLocalStoragePersistorOptions {
    /** The key to use when storing the cache to localstorage */
    localStorageKey?: string;
    /** To avoid localstorage spamming,
     * pass a time in ms to throttle saving the cache to disk */
    throttleTime?: number;
}
export declare function createLocalStoragePersistor({ localStorageKey, throttleTime, }?: CreateLocalStoragePersistorOptions): Persistor;
export {};
