(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.ReactQueryPersistQueryClientExperimental = {}));
}(this, (function (exports) { 'use strict';

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function noop() {
    return undefined;
  }

  // FUNCTIONS
  var logger = console || {
    error: noop,
    warn: noop,
    log: noop
  };
  function getLogger() {
    return logger;
  }

  // TYPES
  // FUNCTIONS
  function dehydrateMutation(mutation) {
    return {
      mutationKey: mutation.options.mutationKey,
      state: mutation.state
    };
  } // Most config is not dehydrated but instead meant to configure again when
  // consuming the de/rehydrated data, typically with useQuery on the client.
  // Sometimes it might make sense to prefetch data on the server and include
  // in the html-payload, but not consume it on the initial render.


  function dehydrateQuery(query) {
    return {
      state: query.state,
      queryKey: query.queryKey,
      queryHash: query.queryHash
    };
  }

  function defaultShouldDehydrateMutation(mutation) {
    return mutation.state.isPaused;
  }

  function defaultShouldDehydrateQuery(query) {
    return query.state.status === 'success';
  }

  function dehydrate(client, options) {
    var _options, _options2;

    options = options || {};
    var mutations = [];
    var queries = [];

    if (((_options = options) == null ? void 0 : _options.dehydrateMutations) !== false) {
      var shouldDehydrateMutation = options.shouldDehydrateMutation || defaultShouldDehydrateMutation;
      client.getMutationCache().getAll().forEach(function (mutation) {
        if (shouldDehydrateMutation(mutation)) {
          mutations.push(dehydrateMutation(mutation));
        }
      });
    }

    if (((_options2 = options) == null ? void 0 : _options2.dehydrateQueries) !== false) {
      var shouldDehydrateQuery = options.shouldDehydrateQuery || defaultShouldDehydrateQuery;
      client.getQueryCache().getAll().forEach(function (query) {
        if (shouldDehydrateQuery(query)) {
          queries.push(dehydrateQuery(query));
        }
      });
    }

    return {
      mutations: mutations,
      queries: queries
    };
  }
  function hydrate(client, dehydratedState, options) {
    if (typeof dehydratedState !== 'object' || dehydratedState === null) {
      return;
    }

    var mutationCache = client.getMutationCache();
    var queryCache = client.getQueryCache();
    var mutations = dehydratedState.mutations || [];
    var queries = dehydratedState.queries || [];
    mutations.forEach(function (dehydratedMutation) {
      var _options$defaultOptio;

      mutationCache.build(client, _extends({}, options == null ? void 0 : (_options$defaultOptio = options.defaultOptions) == null ? void 0 : _options$defaultOptio.mutations, {
        mutationKey: dehydratedMutation.mutationKey
      }), dehydratedMutation.state);
    });
    queries.forEach(function (dehydratedQuery) {
      var _options$defaultOptio2;

      var query = queryCache.get(dehydratedQuery.queryHash); // Do not hydrate if an existing query exists with newer data

      if (query) {
        if (query.state.dataUpdatedAt < dehydratedQuery.state.dataUpdatedAt) {
          query.setState(dehydratedQuery.state);
        }

        return;
      } // Restore query


      queryCache.build(client, _extends({}, options == null ? void 0 : (_options$defaultOptio2 = options.defaultOptions) == null ? void 0 : _options$defaultOptio2.queries, {
        queryKey: dehydratedQuery.queryKey,
        queryHash: dehydratedQuery.queryHash
      }), dehydratedQuery.state);
    });
  }

  function _await(value, then, direct) {
    if (direct) {
      return then ? then(value) : value;
    }

    if (!value || !value.then) {
      value = Promise.resolve(value);
    }

    return then ? value.then(then) : value;
  }

  function _catch(body, recover) {
    try {
      var result = body();
    } catch (e) {
      return recover(e);
    }

    if (result && result.then) {
      return result.then(void 0, recover);
    }

    return result;
  }

  function _continue(value, then) {
    return value && value.then ? value.then(then) : then(value);
  }

  function _empty() {}

  function _invokeIgnored(body) {
    var result = body();

    if (result && result.then) {
      return result.then(_empty);
    }
  }

  function _async(f) {
    return function () {
      for (var args = [], i = 0; i < arguments.length; i++) {
        args[i] = arguments[i];
      }

      try {
        return Promise.resolve(f.apply(this, args));
      } catch (e) {
        return Promise.reject(e);
      }
    };
  }

  var persistQueryClient = _async(function (_ref) {
    var queryClient = _ref.queryClient,
        persistor = _ref.persistor,
        _ref$maxAge = _ref.maxAge,
        maxAge = _ref$maxAge === void 0 ? 1000 * 60 * 60 * 24 : _ref$maxAge,
        _ref$buster = _ref.buster,
        buster = _ref$buster === void 0 ? '' : _ref$buster,
        hydrateOptions = _ref.hydrateOptions,
        dehydrateOptions = _ref.dehydrateOptions;
    return _invokeIgnored(function () {
      if (typeof window !== 'undefined') {
        // Subscribe to changes
        var saveClient = function saveClient() {
          var persistClient = {
            buster: buster,
            timestamp: Date.now(),
            clientState: dehydrate(queryClient, dehydrateOptions)
          };
          persistor.persistClient(persistClient);
        }; // Attempt restore


        return _continue(_catch(function () {
          return _await(persistor.restoreClient(), function (persistedClient) {
            if (persistedClient) {
              if (persistedClient.timestamp) {
                var expired = Date.now() - persistedClient.timestamp > maxAge;
                var busted = persistedClient.buster !== buster;

                if (expired || busted) {
                  persistor.removeClient();
                } else {
                  hydrate(queryClient, persistedClient.clientState, hydrateOptions);
                }
              } else {
                persistor.removeClient();
              }
            }
          });
        }, function (err) {
          getLogger().error(err);
          getLogger().warn('Encountered an error attempting to restore client cache from persisted location. As a precaution, the persisted cache will be discarded.');
          persistor.removeClient();
        }), function () {
          // Subscribe to changes in the query cache to trigger the save
          queryClient.getQueryCache().subscribe(saveClient);
        });
      }
    });
  });

  exports.persistQueryClient = persistQueryClient;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=persistQueryClient-experimental.development.js.map
