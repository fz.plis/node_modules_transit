(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = global || self, factory(global.ReactQueryCreateLocalStoragePersistorExperimental = {}));
}(this, (function (exports) { 'use strict';

  function noop() {
    return undefined;
  }

  function createLocalStoragePersistor(_temp) {
    var _ref = _temp === void 0 ? {} : _temp,
        _ref$localStorageKey = _ref.localStorageKey,
        localStorageKey = _ref$localStorageKey === void 0 ? "REACT_QUERY_OFFLINE_CACHE" : _ref$localStorageKey,
        _ref$throttleTime = _ref.throttleTime,
        throttleTime = _ref$throttleTime === void 0 ? 1000 : _ref$throttleTime;

    if (typeof localStorage !== 'undefined') {
      return {
        persistClient: throttle(function (persistedClient) {
          localStorage.setItem(localStorageKey, JSON.stringify(persistedClient));
        }, throttleTime),
        restoreClient: function restoreClient() {
          var cacheString = localStorage.getItem(localStorageKey);

          if (!cacheString) {
            return;
          }

          return JSON.parse(cacheString);
        },
        removeClient: function removeClient() {
          localStorage.removeItem(localStorageKey);
        }
      };
    }

    return {
      persistClient: noop,
      restoreClient: noop,
      removeClient: noop
    };
  }

  function throttle(func, wait) {
    if (wait === void 0) {
      wait = 100;
    }

    var timer = null;
    return function () {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      if (timer === null) {
        timer = setTimeout(function () {
          func.apply(void 0, args);
          timer = null;
        }, wait);
      }
    };
  }

  exports.createLocalStoragePersistor = createLocalStoragePersistor;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=createLocalStoragePersistor-experimental.development.js.map
