(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react'), require('react-query')) :
  typeof define === 'function' && define.amd ? define(['exports', 'react', 'react-query'], factory) :
  (global = global || self, factory(global.ReactQueryHydration = {}, global.React, global.ReactQuery));
}(this, (function (exports, React, reactQuery) { 'use strict';

  React = React && Object.prototype.hasOwnProperty.call(React, 'default') ? React['default'] : React;

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  // TYPES
  // FUNCTIONS
  function dehydrateMutation(mutation) {
    return {
      mutationKey: mutation.options.mutationKey,
      state: mutation.state
    };
  } // Most config is not dehydrated but instead meant to configure again when
  // consuming the de/rehydrated data, typically with useQuery on the client.
  // Sometimes it might make sense to prefetch data on the server and include
  // in the html-payload, but not consume it on the initial render.


  function dehydrateQuery(query) {
    return {
      state: query.state,
      queryKey: query.queryKey,
      queryHash: query.queryHash
    };
  }

  function defaultShouldDehydrateMutation(mutation) {
    return mutation.state.isPaused;
  }

  function defaultShouldDehydrateQuery(query) {
    return query.state.status === 'success';
  }

  function dehydrate(client, options) {
    var _options, _options2;

    options = options || {};
    var mutations = [];
    var queries = [];

    if (((_options = options) == null ? void 0 : _options.dehydrateMutations) !== false) {
      var shouldDehydrateMutation = options.shouldDehydrateMutation || defaultShouldDehydrateMutation;
      client.getMutationCache().getAll().forEach(function (mutation) {
        if (shouldDehydrateMutation(mutation)) {
          mutations.push(dehydrateMutation(mutation));
        }
      });
    }

    if (((_options2 = options) == null ? void 0 : _options2.dehydrateQueries) !== false) {
      var shouldDehydrateQuery = options.shouldDehydrateQuery || defaultShouldDehydrateQuery;
      client.getQueryCache().getAll().forEach(function (query) {
        if (shouldDehydrateQuery(query)) {
          queries.push(dehydrateQuery(query));
        }
      });
    }

    return {
      mutations: mutations,
      queries: queries
    };
  }
  function hydrate(client, dehydratedState, options) {
    if (typeof dehydratedState !== 'object' || dehydratedState === null) {
      return;
    }

    var mutationCache = client.getMutationCache();
    var queryCache = client.getQueryCache();
    var mutations = dehydratedState.mutations || [];
    var queries = dehydratedState.queries || [];
    mutations.forEach(function (dehydratedMutation) {
      var _options$defaultOptio;

      mutationCache.build(client, _extends({}, options == null ? void 0 : (_options$defaultOptio = options.defaultOptions) == null ? void 0 : _options$defaultOptio.mutations, {
        mutationKey: dehydratedMutation.mutationKey
      }), dehydratedMutation.state);
    });
    queries.forEach(function (dehydratedQuery) {
      var _options$defaultOptio2;

      var query = queryCache.get(dehydratedQuery.queryHash); // Do not hydrate if an existing query exists with newer data

      if (query) {
        if (query.state.dataUpdatedAt < dehydratedQuery.state.dataUpdatedAt) {
          query.setState(dehydratedQuery.state);
        }

        return;
      } // Restore query


      queryCache.build(client, _extends({}, options == null ? void 0 : (_options$defaultOptio2 = options.defaultOptions) == null ? void 0 : _options$defaultOptio2.queries, {
        queryKey: dehydratedQuery.queryKey,
        queryHash: dehydratedQuery.queryHash
      }), dehydratedQuery.state);
    });
  }

  function useHydrate(state, options) {
    var queryClient = reactQuery.useQueryClient();
    var optionsRef = React.useRef(options);
    optionsRef.current = options; // Running hydrate again with the same queries is safe,
    // it wont overwrite or initialize existing queries,
    // relying on useMemo here is only a performance optimization.
    // hydrate can and should be run *during* render here for SSR to work properly

    React.useMemo(function () {
      if (state) {
        hydrate(queryClient, state, optionsRef.current);
      }
    }, [queryClient, state]);
  }
  var Hydrate = function Hydrate(_ref) {
    var children = _ref.children,
        options = _ref.options,
        state = _ref.state;
    useHydrate(state, options);
    return children;
  };

  exports.Hydrate = Hydrate;
  exports.dehydrate = dehydrate;
  exports.hydrate = hydrate;
  exports.useHydrate = useHydrate;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=react-query-hydration.development.js.map
