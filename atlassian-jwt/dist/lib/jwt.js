"use strict";
/*
 * Based off jwt-simple:
 * https://github.com/hokaccha/node-jwt-simple
 *
 * Add Atlassian query string hash verification:
 * https://developer.atlassian.com/cloud/jira/platform/understanding-jwt/
 *
 * JSON Web Token encode and decode module for node.js
 *
 * Copyright(c) 2011 Kazuhito Hokamura
 * MIT Licensed
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var crypto_1 = require("crypto");
var lodash_1 = __importDefault(require("lodash"));
var jsuri_1 = __importDefault(require("jsuri"));
var url = __importStar(require("url"));
// https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/
var Algorithm;
(function (Algorithm) {
    Algorithm["HS256"] = "HS256";
    Algorithm["HS384"] = "HS384";
    Algorithm["HS512"] = "HS512";
})(Algorithm = exports.Algorithm || (exports.Algorithm = {}));
function getAlgorithmFromString(rawAlgorithm) {
    switch (rawAlgorithm) {
        case 'HS256':
            return Algorithm.HS256;
        case 'HS384':
            return Algorithm.HS384;
        case 'HS512':
            return Algorithm.HS512;
        default:
            return undefined;
    }
}
/**
 * Supported algorithm mapping.
 */
var algorithmMap = {
    HS256: 'sha256',
    HS384: 'sha384',
    HS512: 'sha512'
};
function fromExpressRequest(eReq) {
    // req.originalUrl represents the full URL and req.path represents the URL from the last router
    // (https://expressjs.com/en/4x/api.html#req.originalUrl)
    // However, since some people depend on this lib without using real req object but rather mock them, we need this
    // fallback for it to not break.
    var pathname = eReq.originalUrl ? url.parse(eReq.originalUrl).pathname : eReq.path;
    return {
        method: eReq.method,
        pathname: pathname,
        query: eReq.query,
        body: eReq.body
    };
}
exports.fromExpressRequest = fromExpressRequest;
function fromMethodAndUrl(method, rawUrl) {
    var parsedUrl = url.parse(rawUrl, true);
    return {
        method: method,
        pathname: parsedUrl.pathname,
        query: parsedUrl.query
    };
}
exports.fromMethodAndUrl = fromMethodAndUrl;
function fromMethodAndPathAndBody(method, rawUrl, body) {
    var parsedUrl = url.parse(rawUrl, false);
    return {
        method: method,
        pathname: parsedUrl.pathname,
        body: body
    };
}
exports.fromMethodAndPathAndBody = fromMethodAndPathAndBody;
/**
 * The separator between sections of a canonical query.
 */
var CANONICAL_QUERY_SEPARATOR = '&';
exports.version = '1.0.3';
/**
 * Decodes JWT string to object.
 * The encoding algorithm must be HS256, HS384, or HS512.
 *
 * @param token JWT to decode
 * @param key Key used to decode
 * @param noVerify optional, set to true to skip the result verification
 *
 * @return Decoded JWT object
 *
 * @api public
 */
exports.decode = function jwt_decode(token, key, noVerify) {
    // Check seguments
    var segments = token.split('.');
    if (segments.length !== 3) {
        throw new Error('Not enough or too many JWT token segments; should be 3');
    }
    // All segment should be base64
    var headerSeg = segments[0];
    var payloadSeg = segments[1];
    var signatureSeg = segments[2];
    // Base64 decode and parse JSON
    var header = JSON.parse(base64urlDecode(headerSeg));
    var payload = JSON.parse(base64urlDecode(payloadSeg));
    // Normalize 'aud' claim, the spec allows both String and Array
    if (payload.aud && !lodash_1.default.isArray(payload.aud)) {
        payload.aud = [payload.aud];
    }
    if (!noVerify) {
        verifySignature(headerSeg, payloadSeg, signatureSeg, key, header.alg);
    }
    return payload;
};
/**
 * Encodes JWT object to string.
 *
 * @param payload Payload object to encode
 * @param key Key used to encode
 * @param algorithm Optional, must be HS256, HS384, or HS512; default is HS256
 *
 * @return Encoded JWT string
 *
 * @api public
 */
exports.encode = function jwt_encode(payload, key, algorithm) {
    var _a = validateAlgorithm(key, algorithm), signingAlgorithm = _a[0], signingMethod = _a[1];
    // typ is fixed value
    var header = { typ: 'JWT', alg: signingAlgorithm };
    // Create segments, all segment should be base64 string
    var segments = [];
    segments.push(base64urlEncode(JSON.stringify(header)));
    segments.push(base64urlEncode(JSON.stringify(payload)));
    segments.push(sign(segments.join('.'), key, signingMethod));
    return segments.join('.');
};
function createCanonicalRequest(req, checkBodyForParams, baseUrl) {
    return canonicalizeMethod(req) +
        CANONICAL_QUERY_SEPARATOR +
        canonicalizeUri(req, baseUrl) +
        CANONICAL_QUERY_SEPARATOR +
        canonicalizeQueryString(req, checkBodyForParams);
}
exports.createCanonicalRequest = createCanonicalRequest;
function createQueryStringHash(req, checkBodyForParams, baseUrl) {
    return crypto_1.createHash(algorithmMap.HS256)
        .update(createCanonicalRequest(req, checkBodyForParams, baseUrl))
        .digest('hex');
}
exports.createQueryStringHash = createQueryStringHash;
/**
 * Private util functions.
 */
function validateAlgorithm(key, algorithm) {
    // Check key
    if (!key) {
        throw new Error('Require key');
    }
    // Check algorithm, default is HS256
    var signingAlgorithm = algorithm || 'HS256';
    var alg = getAlgorithmFromString(signingAlgorithm);
    if (!alg) {
        throw new Error('Algorithm "' + algorithm + '" is not supported');
    }
    var signingMethod = algorithmMap[alg];
    if (!signingMethod) {
        throw new Error('Algorithm "' + algorithm + '" is not supported');
    }
    return [signingAlgorithm, signingMethod];
}
function verifySignature(headerSeg, payloadSeg, signatureSeg, key, algorithm) {
    var _a = validateAlgorithm(key, algorithm), signingMethod = _a[1];
    // Verify signature
    var signingInput = [headerSeg, payloadSeg].join('.');
    if (signatureSeg !== sign(signingInput, key, signingMethod)) {
        throw new Error('Signature verification failed for input: ' + signingInput + ' with method ' + signingMethod);
    }
}
function sign(input, key, method) {
    var base64str = crypto_1.createHmac(method, key).update(input).digest('base64');
    return base64urlEscape(base64str);
}
function base64urlDecode(str) {
    return Buffer.from(base64urlUnescape(str), 'base64').toString();
}
function base64urlUnescape(str) {
    str += Array(5 - str.length % 4).join('=');
    return str.replace(/\-/g, '+').replace(/_/g, '/');
}
function base64urlEncode(str) {
    return base64urlEscape(Buffer.from(str).toString('base64'));
}
function base64urlEscape(str) {
    return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
}
function canonicalizeMethod(req) {
    return req.method.toUpperCase();
}
function canonicalizeUri(req, baseUrlString) {
    var path = req.pathname;
    var baseUrl = new jsuri_1.default(baseUrlString);
    var baseUrlPath = baseUrl.path();
    if (path && path.indexOf(baseUrlPath) === 0) {
        path = path.slice(baseUrlPath.length);
    }
    if (!path || path.length === 0) {
        return '/';
    }
    // If the separator is not URL encoded then the following URLs have the same query-string-hash:
    //   https://djtest9.jira-dev.com/rest/api/2/project&a=b?x=y
    //   https://djtest9.jira-dev.com/rest/api/2/project?a=b&x=y
    path = path.replace(new RegExp(CANONICAL_QUERY_SEPARATOR, 'g'), encodeRfc3986(CANONICAL_QUERY_SEPARATOR));
    // Prefix with /
    if (path[0] !== '/') {
        path = '/' + path;
    }
    // Remove trailing /
    if (path.length > 1 && path[path.length - 1] === '/') {
        path = path.substring(0, path.length - 1);
    }
    return path;
}
function canonicalizeQueryString(req, checkBodyForParams) {
    var queryParams = req.query;
    var method = req.method.toUpperCase();
    // Apache HTTP client (or something) sometimes likes to take the query string and put it into the request body
    // if the method is PUT or POST
    if (checkBodyForParams && lodash_1.default.isEmpty(queryParams) && (method === 'POST' || method === 'PUT')) {
        queryParams = req.body;
    }
    var sortedQueryString = new Array(), query = lodash_1.default.extend({}, queryParams);
    if (!lodash_1.default.isEmpty(query)) {
        // Remove the 'jwt' query string param
        delete query.jwt;
        lodash_1.default.each(lodash_1.default.keys(query).sort(), function (key) {
            // The __proto__ field can sometimes sneak in depending on what node version is being used.
            // Get rid of it or the qsh calculation will be wrong.
            if (key === '__proto__') {
                return;
            }
            var param = query[key];
            var paramValue = '';
            if (Array.isArray(param)) {
                paramValue = lodash_1.default.map(param.sort(), encodeRfc3986).join(',');
            }
            else {
                paramValue = encodeRfc3986(param);
            }
            sortedQueryString.push(encodeRfc3986(key) + '=' + paramValue);
        });
    }
    return sortedQueryString.join('&');
}
/**
 * We follow the same rules as specified in OAuth1:
 * Percent-encode everything but the unreserved characters according to RFC-3986:
 * unreserved  = ALPHA / DIGIT / "-" / "." / "_" / "~"
 * See http://tools.ietf.org/html/rfc3986
 */
function encodeRfc3986(value) {
    return encodeURIComponent(value)
        .replace(/[!'()]/g, escape)
        .replace(/\*/g, '%2A');
}
