module.exports = {
  "(src|lib|tests|__tests__|)/**/*.{js,jsx,ts,tsx}": ["npm run eslint -- --fix"],
  "src/**/*.{scss,tsx}": ["npm run stylelint -- --fix"]
};
