# @eco/eslint-config

Пакет содержит предустановленный набор правил eslint, для линтинга платформенных пакетов и стримов.

В конфигурацию по умолчанию собраны правила:

1. для ts-кода;
1. для React;
1. для unit-тестов с использованием jest;
1. для линтинга jsdoc;
1. правил, предоставляемых плагином @eco/eslint-plugin.

## Установка

Для установки нужно установить сам пакет

```
npm i -D -E @eco/eslint-config
```

А также пакеты совместимой версии, указанные в peerDependencies.

```
npm i -D -E eslint @eco/{browserslist-config,eslint-plugin} @typescript-eslint/{eslint-plugin,parser} eslint-config-prettier eslint-plugin-{compat,jsdoc,import,jest,jest-formatting,prettier,react,react-hooks,sonarjs,unicorn}
```

## Конфигурирование

В пакете нужно создать конфиг, расширенный от `@eco`.

```js
// .eslintrc.js
module.exports = {
  extends: ["@eco"],
};
```

Можно использовать другие форматы конфигурации, поддерживаемые [Eslint](https://eslint.org/docs/user-guide/configuring)

## Состав пакета

В пакет входят конфигурация по умолчанию (максимальный набор правил) и несколько дополнительных, отвечающих за линтинг отдельных частей кода.

1. `index.js` - максимальная конфигурация (по умолчанию);
1. `defaults.js` - правила линтинга js- и ts- кода;
1. `jest.js` - правила линтинга unit-тестов с использованием jest;
1. `jsdoc.js` - правила линтинга jsdoc;
1. `react.js` - правила линтинга React-кода, в том числе хуков React;
1. `eco.js` - дополнительные правила линтинга, требуемые в Экосистеме (линтинг локализации, ascii-символов и другие). 

### Использование отдельных конфигураций

Каждая из конфигураций доступна в виде отдельного файла, поставляемого с пакетом. Если часть линтинга не требуется (например, не нужен линтинг React), можно в конфиге указать ссылки на отдельные конфиги.

```js
// .eslintrc.js
module.exports = {
  // берем правила ts-кода, unit-testing и jsdoc
  extends: [
    "@eco/eslint-config/defaults",
    "@eco/eslint-config/jest",
    "@eco/eslint-config/jsdoc",
  ],
};
```
