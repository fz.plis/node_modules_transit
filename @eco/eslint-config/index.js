module.exports = {
  extends: ["./defaults", "./eco", "./jsdoc", "./jest", "./react"],
  ignorePatterns: ["src/localization/*.ts"],
};
