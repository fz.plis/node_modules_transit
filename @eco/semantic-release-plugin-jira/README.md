# `@eco/semantic-release-plugin-jira`

Разработан на основе [semantic-release-jira-releases](https://github.com/UpHabit/semantic-release-jira-releases). Потребовалось доработка, чтобы встроить плагин в процесс.
Плагин не позволял обрабатывать тикеты в режиме dry-run и мог только создать релиз.

Плагин автоматически привязывает тикеты к fix-version Jira. Привязка происходит как в режиме dry-run, так и на CI.
Плагин автоматически обновляет номер fix-version, если обнаружена более старая версия.

Пример создания fix-version и привязки тикетов:

```
|
* feat: initial commit # => v1.0.0 
|
* feat: GBO-1234 имплементация фичи А # => Fix version v1.1.0, тикеты: GBO-1234  
|
* feat: GBO-5678 имплементация фичи B # => Fix version v1.1.0, тикеты: GBO-1234, GBO-5678  
| 
* chore(release): semantic-release [skip ci] # => Fix version v1.1.0, тикеты: GBO-1234, GBO-5678
| \
*  | feat: bump version [skip ci] # => Старт rc-ветки Fix version v1.2.0, нет тикетов. 
|  |
|  * fix: GBO-2345 фикс фичи А в рц-ветке # => Fix version v1.1.0 -> v1.1.1, тикеты: GBO-1234, GBO-5678, GBO-2345
|  |
*  | feat: GBO-6789 имплементация фичи C # => Fix version v1.2.0, тикеты: GBO-6789
|  |
| /
* merge branch rc-1.1.x in master # => Fix version v1.2.0, тикеты: GBO-6789
|
```

Плагин включает шаги:

- `verifyConditions`: проводит проверку конфига.
- `verifyRelease`: выделяет тикеты из commit-message, создает при необходимости Fix Version, привязывает тикеты.

## Установка

```
npm i -D @eco/semantic-release-plugin-jira
```

## Использование

Плагин требует обязательных настроек.

```js
// .releaserc.js
module.exports = {
 {
  "plugins": [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/git",
    ["@eco/semantic-release-plugin-jira", {
      "projectId": "TEST",
      "releaseNameTemplate": "package - v{version}",
      "jiraHost": "jira.example.com",
      "ticketPrefixes": [ "TEST", "GBO"],
    }]
  ]
}
};
```

А также требует наличия переменной окружения JIRA_AUTH. 

### Режимы

Плагин способен работать в нескольких режимах.

#### Стандартный режим

В стандартном режиме плагин анализирует коммиты и привязывает тикеты к fix version. При необходимости создает новую версию.

#### Режим выпуска

В этом режиме плагин запускается, чтобы выпустить текущую версию и переместить в архив более старые версии.
Используется после финиша RC или Hotfix.

## Настройка

Плагин подключается согласно руководству [Semantic Release](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/plugins.md#plugins)


### Опции

```ts
/**
 * Опции плагина, которые могут быть описаны в конфигурационном файле Semantic Release.
 */
export interface PluginOptions {
  /**
   * Адрес, на котором находится Jira. Например, `jira.example.com`.
   */
  jiraHost: string;

  /**
   * Id или ключ проекта, в котором будут создаваться тикеты.
   */
  projectId: string;

  /**
   * Список префиксов тикетов, по которым идет сопоставление коммитов с задачами.
   * Не может быть использован совместно с `ticketRegex`.
   *
   * Пример. ['TEST'] соответствует `TEST-123` and `TEST-456`.
   */
  ticketPrefixes?: string[];

  /**
   * Неэкранированный regex для поиска тикетов в коммитах (без символов `\`).
   * Не может быть использован совместно с `ticketPrefixes`.
   *
   * Пример. [a-zA-Z]{4}-\d{3,5} соответствует тикетам `TEST-456`, `PROJ-5643` и `TEST-56432`.
   */
  ticketRegex?: string;

  /**
   * Шаблон, по которому генерируется название версии Jira.
   * Шаблонизация происходит через утилиту [string-template][(https://github.com/Matt-Esch/string-template).
   * 
   * Доступны переменные:
   * - `{version}` - номер версии пакета, вычисленный Semantic Release.
   * - `${package}` - имя пакета, берется из `process.env.npm_package_name`.
   
   * Пример: `{package} - v{version}` -> `@eco/package_name v1.0.0`.
   *
   * @default `{package}@{version}`
   */
  releaseNameTemplate?: string;

  /**
   * Шаблон, по которому генерируется описание версии Jira.
   * Шаблонизация происходит через утилиту [string-template][(https://github.com/Matt-Esch/string-template).
   *
   * Доступны переменные:
   * - `{version}` - номер версии пакета, вычисленный Semantic Release.
   * - `${package}` - имя пакета, берется из `process.env.npm_package_name`.
   * - `${notes}` - Release Notes, агрегированный Semantic Release. Может быть очень большим.
   *
   * Пример: `{package}@{version}: {notes}` -> `@eco/package_name@1.0.0: Release notes by Semantic Release`.
   *
   * @default `Автоматический релиз [@eco/semantic-release-plugin-jira]`
   */
  releaseDescriptionTemplate?: string;

  /**
   * Число одновременных запросов на обновление тикетов.
   *
   * @default 10
   */
  networkConcurrency?: number;

  /**
   * Если `true` - ставит поле `released: true` в Jira Version.
   */
  released?: boolean;

  /**
   * Если `true` - ставит текущую дату в поле `release date` в Jira Version.
   */
  setReleaseDate?: boolean;

    /**
   * Имя релиз-кандидат ветки. Используется для выпуска и архивирования версий после выпуска релиза.
   *
   * @default
   * 'stage'
   */

  stageBranch?: string;
  /**
   * Имя релиз ветки. Используется для выпуска и архивирования версий после выпуска хотфикса.
   *
   * @default
   * 'release'
   */
  releaseBranch?: string;
}
```

### Переменные окружения

| **Переменная** | **Описание** |
|----------------|--------------|
| `JIRA_AUTH` | Должна содержать base-64 токен для Basic Auth при обращении к Jira. | - |
| `JIRA_OFF` | Полностью отключает плагин. Проставьте этот параметр на CI в случае проблем с Jira. | - |
| `JIRA_RELEASE` | Флаг, включающий выпуск и архивирование старых релизов. Используется после финиша RC или Hotfix. | - |
| `JIRA_FORCE_VERSION` | Флаг, включающий принудительное созданик версии. Используется при старте RC. | - |