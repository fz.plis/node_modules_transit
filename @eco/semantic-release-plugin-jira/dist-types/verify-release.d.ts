import type { PluginConfig, SemanticReleaseContext } from './types';
export default function verifyRelease(config: PluginConfig, context: SemanticReleaseContext): Promise<void>;
