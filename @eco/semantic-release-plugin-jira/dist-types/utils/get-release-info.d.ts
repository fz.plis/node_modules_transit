import { PluginConfig, SemanticReleaseContext } from '../types';
export interface ReleaseInfo {
    lastVersion: string;
    releaseVersion: string;
    description: string;
}
export default function getReleaseInfo(config: PluginConfig, context: SemanticReleaseContext): ReleaseInfo;
