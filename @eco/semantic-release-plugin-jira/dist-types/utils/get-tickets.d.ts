import type { PluginConfig, SemanticReleaseContext } from '../types';
export default function getTickets(config: PluginConfig, context: SemanticReleaseContext): string[];
