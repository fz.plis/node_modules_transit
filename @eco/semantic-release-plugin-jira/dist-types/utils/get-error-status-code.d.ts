import { SemanticReleaseContext } from '../types';
export default function getErrorStatusCode(err: any, context: SemanticReleaseContext): 'unknown' | number;
