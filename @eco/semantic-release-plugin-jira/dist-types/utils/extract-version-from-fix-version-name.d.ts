export default function extractVersionNumberFromFixVersion(fixVersion: string, format: string): string;
