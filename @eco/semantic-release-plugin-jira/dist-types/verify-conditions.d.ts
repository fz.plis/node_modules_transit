import type { PluginConfig, SemanticReleaseContext } from './types';
export default function verifyConditions(config: PluginConfig, context: SemanticReleaseContext): Promise<void>;
