import type JiraClient from 'jira-connector';
import type { Version } from 'jira-connector';
import { PluginConfig, SemanticReleaseContext } from '../types';
export declare const updateIssue: (context: SemanticReleaseContext, params: {
    releaseVersionId: string;
    issueKey: string;
    jira: JiraClient;
}) => Promise<void>;
export declare const selectNewIssues: (config: PluginConfig, context: SemanticReleaseContext, params: {
    tickets: string[];
    jira: JiraClient;
    release: Version;
}) => Promise<string[]>;
export default function setFixVersion(config: PluginConfig, context: SemanticReleaseContext, params: {
    tickets: string[];
    jira: JiraClient;
    release: Version;
}): Promise<void>;
