import type JiraClient from 'jira-connector';
import type { Version } from 'jira-connector';
import type { PluginConfig, SemanticReleaseContext } from '../types';
export declare const NOT_FOUND_ERR_TEXT = "\u041D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u0430 \u0432\u0435\u0440\u0441\u0438\u044F \u043F\u043E\u0434\u0445\u043E\u0434\u044F\u0449\u0430\u044F \u0432\u0435\u0440\u0441\u0438\u044F.";
export declare const getPackageVersions: (allVersions: Version[], config: PluginConfig, context: SemanticReleaseContext) => Version[];
export default function releaseVersions(config: PluginConfig, context: SemanticReleaseContext, jira: JiraClient): Promise<void[]>;
