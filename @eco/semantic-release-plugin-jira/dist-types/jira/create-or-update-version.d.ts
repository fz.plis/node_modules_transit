import type JiraClient from 'jira-connector';
import type { Version } from 'jira-connector';
import type { PluginConfig, SemanticReleaseContext } from '../types';
export default function createOrUpdateVersion(config: PluginConfig, context: SemanticReleaseContext, params: {
    jira: JiraClient;
    projectIdOrKey: string;
    lastVersion: string;
    releaseVersion: string;
    description: string;
}): Promise<Version>;
