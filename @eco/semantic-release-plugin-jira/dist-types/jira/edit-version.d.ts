import type JiraClient from 'jira-connector';
import type { Version } from 'jira-connector';
import { SemanticReleaseContext } from '../types';
export default function editVersion(context: SemanticReleaseContext, jira: JiraClient, data: {
    versionId: string;
    version: Partial<Version>;
}): Promise<any>;
