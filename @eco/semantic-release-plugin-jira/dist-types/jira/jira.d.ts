import JiraClient from 'jira-connector';
import type { Context } from 'semantic-release';
import type { PluginConfig } from '../types';
export default function jira(config: PluginConfig, context: Context): JiraClient;
