import type { Commit, Context, LastRelease, NextRelease, Options } from 'semantic-release';
export declare const DEFAULT_VERSION_TEMPLATE: "{package}@{version}";
export declare const DEFAULT_RELEASE_DESCRIPTION_TEMPLATE: "Автоматический релиз [@eco/semantic-release-plugin-jira]";
export declare const DEFAULT_NETWORK_CONCURENCY: 10;
export declare const STAGE_BRANCH: "stage";
export declare const RELEASE_BRANCH: "release";
export interface BaseConfig {
    branch: string;
    debug: boolean;
    dryRun: boolean;
}
export interface PluginOptions {
    jiraHost: string;
    projectId: string;
    ticketPrefixes?: string[];
    ticketRegex?: string;
    releaseNameTemplate?: string;
    releaseDescriptionTemplate?: string;
    networkConcurrency?: number;
    released?: boolean;
    setReleaseDate?: boolean;
    stageBranch?: string;
    releaseBranch?: string;
}
export declare type PluginConfig = BaseConfig & PluginOptions;
export interface SemanticReleaseContext extends Context, Options {
    logger: Context['logger'] & {
        info(message: string): void;
    };
    commits: Commit[];
    lastRelease: LastRelease;
    nextRelease: NextRelease;
    branch: {
        name: string;
    };
}
