"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var error_1 = __importDefault(require("@semantic-release/error"));
var jira_1 = require("./jira");
var types_1 = require("./types");
function verifyConditions(config, context) {
    return __awaiter(this, void 0, void 0, function () {
        var networkConcurrency, ticketError, ticketPrefixError, _a, _b, prefix, _c, stageBranch, _d, releaseBranch, releaseBranches, client, error_2;
        var e_1, _e;
        return __generator(this, function (_f) {
            switch (_f.label) {
                case 0:
                    if (context.env.JIRA_OFF) {
                        context.logger.log('Передан флаг JIRA_OFF. Верификация плагина пропущена.');
                        return [2];
                    }
                    networkConcurrency = config.networkConcurrency;
                    if (typeof config.jiraHost !== 'string') {
                        throw new error_1.default('config.jiraHost должен быть строкой');
                    }
                    if (typeof config.projectId !== 'string') {
                        throw new error_1.default('config.projectId должен быть строкой');
                    }
                    ticketError = 'Требуется указать либо config.ticketPrefixes, либо config.ticketRegex';
                    if (!config.ticketPrefixes && !config.ticketRegex) {
                        throw new error_1.default(ticketError);
                    }
                    if (config.ticketPrefixes && config.ticketRegex) {
                        throw new error_1.default(ticketError);
                    }
                    if (config.ticketPrefixes) {
                        ticketPrefixError = 'config.ticketPrefixes должен быть массивом строк';
                        if (!Array.isArray(config.ticketPrefixes)) {
                            throw new error_1.default(ticketPrefixError);
                        }
                        try {
                            for (_a = __values(config.ticketPrefixes), _b = _a.next(); !_b.done; _b = _a.next()) {
                                prefix = _b.value;
                                if (typeof prefix !== 'string') {
                                    throw new error_1.default(ticketPrefixError);
                                }
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (_b && !_b.done && (_e = _a.return)) _e.call(_a);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                    }
                    if (config.ticketRegex && typeof config.ticketRegex !== 'string') {
                        throw new error_1.default('config.ticketRegex должен быть строкой');
                    }
                    if (config.releaseNameTemplate &&
                        (typeof config.releaseNameTemplate !== 'string' ||
                            !config.releaseNameTemplate.includes('{version}') ||
                            !config.releaseNameTemplate.includes('{package}'))) {
                        throw new error_1.default('Шаблон config.releaseNameTemplate должен обязательно содержать {version} и {package}');
                    }
                    if (config.releaseDescriptionTemplate !== null &&
                        config.releaseDescriptionTemplate !== undefined &&
                        typeof config.releaseDescriptionTemplate !== 'string') {
                        throw new error_1.default('config.releaseDescriptionTemplate должен быть строкой');
                    }
                    if (networkConcurrency && (typeof networkConcurrency !== 'number' || networkConcurrency < 1)) {
                        throw new error_1.default('config.networkConcurrency должен быть больше 0');
                    }
                    if (config.stageBranch !== undefined && typeof config.stageBranch !== 'string') {
                        throw new error_1.default('Параметр `stageBranch` должен быть строкой');
                    }
                    if (config.releaseBranch !== undefined && typeof config.releaseBranch !== 'string') {
                        throw new error_1.default('Параметр `releaseBranch` должен быть строкой');
                    }
                    _c = config.stageBranch, stageBranch = _c === void 0 ? types_1.STAGE_BRANCH : _c, _d = config.releaseBranch, releaseBranch = _d === void 0 ? types_1.RELEASE_BRANCH : _d;
                    releaseBranches = [stageBranch, releaseBranch];
                    if (context.env.JIRA_RELEASE && !releaseBranches.includes(context.branch.name)) {
                        throw new error_1.default("\u0414\u043B\u044F \u0440\u0435\u0436\u0438\u043C\u0430 \u0432\u044B\u043F\u0443\u0441\u043A\u0430 \u0432\u0435\u0440\u0441\u0438\u0439 \u0442\u0440\u0435\u0431\u0443\u044E\u0442\u0441\u044F \u0432\u0435\u0442\u043A\u0438 " + releaseBranches.join(', ') + ". \u0422\u0435\u043A\u0443\u0449\u0430\u044F \u0432\u0435\u0442\u043A\u0430 - " + context.branch.name);
                    }
                    if (!context.env.JIRA_AUTH) {
                        throw new error_1.default('Переменная окружения JIRA_AUTH должна быть строкой');
                    }
                    client = jira_1.jira(config, context);
                    _f.label = 1;
                case 1:
                    _f.trys.push([1, 3, , 4]);
                    return [4, client.project.getProject({ projectIdOrKey: config.projectId })];
                case 2:
                    _f.sent();
                    context.logger.log('Подключение к Jira прошло успешно');
                    return [3, 4];
                case 3:
                    error_2 = _f.sent();
                    throw new error_1.default('Не удалось подключиться к Jira');
                case 4:
                    if (!context.env.JIRA_RELEASE) return [3, 6];
                    return [4, jira_1.releaseVersions(config, context, client)];
                case 5:
                    _f.sent();
                    _f.label = 6;
                case 6: return [2];
            }
        });
    });
}
exports.default = verifyConditions;
