"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RELEASE_BRANCH = exports.STAGE_BRANCH = exports.DEFAULT_NETWORK_CONCURENCY = exports.DEFAULT_RELEASE_DESCRIPTION_TEMPLATE = exports.DEFAULT_VERSION_TEMPLATE = void 0;
exports.DEFAULT_VERSION_TEMPLATE = '{package}@{version}';
exports.DEFAULT_RELEASE_DESCRIPTION_TEMPLATE = 'Автоматический релиз [@eco/semantic-release-plugin-jira]';
exports.DEFAULT_NETWORK_CONCURENCY = 10;
exports.STAGE_BRANCH = 'stage';
exports.RELEASE_BRANCH = 'release';
