"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var string_template_1 = __importDefault(require("string-template"));
var types_1 = require("../types");
function getReleaseInfo(config, context) {
    var getPackageVersion = function (version) {
        return string_template_1.default(config.releaseNameTemplate || types_1.DEFAULT_VERSION_TEMPLATE, {
            version: version,
            package: context.env.npm_package_name,
        });
    };
    return {
        releaseVersion: getPackageVersion(context.nextRelease.version),
        lastVersion: getPackageVersion(context.lastRelease.version),
        description: string_template_1.default(config.releaseDescriptionTemplate || types_1.DEFAULT_RELEASE_DESCRIPTION_TEMPLATE, {
            package: context.env.npm_package_name,
            version: context.nextRelease.version,
            notes: context.nextRelease.notes,
        }),
    };
}
exports.default = getReleaseInfo;
