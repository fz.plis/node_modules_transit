"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var error_1 = __importDefault(require("@semantic-release/error"));
function extractVersionNumberFromFixVersion(fixVersion, format) {
    var regexpStr = format.replace('{package}', '(?<package>.+)').replace('{version}', '(?<version>.+)');
    var matches = fixVersion.match(regexpStr);
    var groups = (matches === null || matches === void 0 ? void 0 : matches.groups) || {};
    var version = groups.version;
    if (!version) {
        throw new error_1.default("\u041D\u0435 \u0443\u0434\u0430\u043B\u043E\u0441\u044C \u0432\u044B\u0434\u0435\u043B\u0438\u0442\u044C \u0434\u0430\u043D\u043D\u044B\u0435 \u0440\u0435\u043B\u0438\u0437\u0430 '" + format + "' \u0438\u0437 '" + fixVersion + "'");
    }
    return version;
}
exports.default = extractVersionNumberFromFixVersion;
