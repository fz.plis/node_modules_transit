"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getErrorStatusCode(err, context) {
    var allowedStatusCodes = [400, 404];
    var statusCode = err.statusCode;
    if (typeof err === 'string') {
        try {
            var parsedErr = JSON.parse(err);
            statusCode |= parsedErr.statusCode;
        }
        catch (jsonParseErr) {
            context.logger.log('Ошибка парсинга ответа от Jira');
        }
    }
    if (!allowedStatusCodes.includes(statusCode)) {
        return 'unknown';
    }
    return statusCode;
}
exports.default = getErrorStatusCode;
