"use strict";
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var escape_reg_exp_1 = __importDefault(require("./escape-reg-exp"));
function getTickets(config, context) {
    var e_1, _a, e_2, _b;
    var patterns = typeof config.ticketRegex === 'undefined'
        ? config.ticketPrefixes.map(function (prefix) { return new RegExp("\\b" + escape_reg_exp_1.default(prefix) + "-(\\d+)\\b", 'giu'); })
        : [new RegExp(config.ticketRegex, 'giu')];
    var tickets = new Set();
    try {
        for (var _c = __values(context.commits), _d = _c.next(); !_d.done; _d = _c.next()) {
            var commit = _d.value;
            try {
                for (var patterns_1 = (e_2 = void 0, __values(patterns)), patterns_1_1 = patterns_1.next(); !patterns_1_1.done; patterns_1_1 = patterns_1.next()) {
                    var pattern = patterns_1_1.value;
                    var matches = commit.message.match(pattern);
                    if (matches) {
                        matches.forEach(function (match) {
                            tickets.add(match);
                        });
                        context.logger.info("\u041D\u0430\u0439\u0434\u0435\u043D\u044B \u0442\u0438\u043A\u0435\u0442\u044B (" + matches.length + "): " + matches.join(', ') + ". \u041A\u043E\u043C\u043C\u0438\u0442: " + commit.commit.short);
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (patterns_1_1 && !patterns_1_1.done && (_b = patterns_1.return)) _b.call(patterns_1);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return __spreadArray([], __read(tickets));
}
exports.default = getTickets;
