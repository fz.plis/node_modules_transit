"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jira_connector_1 = __importDefault(require("jira-connector"));
function jira(config, context) {
    return new jira_connector_1.default({
        host: config.jiraHost,
        basic_auth: {
            base64: context.env.JIRA_AUTH,
        },
    });
}
exports.default = jira;
