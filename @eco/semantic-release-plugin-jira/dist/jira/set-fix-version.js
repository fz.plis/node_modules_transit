"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.selectNewIssues = exports.updateIssue = void 0;
var p_limit_1 = __importDefault(require("p-limit"));
var types_1 = require("../types");
var get_error_status_code_1 = __importDefault(require("../utils/get-error-status-code"));
var updateIssue = function updateIssue(context, params) {
    return __awaiter(this, void 0, void 0, function () {
        var releaseVersionId, issueKey, jira, err_1, statusCode;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    releaseVersionId = params.releaseVersionId, issueKey = params.issueKey, jira = params.jira;
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4, jira.issue.editIssue({
                            issueKey: issueKey,
                            issue: {
                                update: {
                                    fixVersions: [
                                        {
                                            add: { id: releaseVersionId },
                                        },
                                    ],
                                },
                                properties: undefined,
                            },
                        })];
                case 2:
                    _a.sent();
                    return [3, 4];
                case 3:
                    err_1 = _a.sent();
                    statusCode = get_error_status_code_1.default(err_1, context);
                    context.logger.error("\u041D\u0435 \u0443\u0434\u0430\u043B\u043E\u0441\u044C \u043E\u0431\u043D\u043E\u0432\u0438\u0442\u044C \u0437\u0430\u0434\u0430\u0447\u0443 " + issueKey + ". \u041A\u043E\u0434 \u043E\u0442\u0432\u0435\u0442\u0430: " + statusCode);
                    return [3, 4];
                case 4: return [2];
            }
        });
    });
};
exports.updateIssue = updateIssue;
var selectNewIssues = function selectNewIssues(config, context, params) {
    return __awaiter(this, void 0, void 0, function () {
        var jql, result, filterIssues_1, newIssues, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    jql = "project = \"" + config.projectId + "\" AND fixVersion = \"" + params.release.name + "\"";
                    context.logger.log("\u041F\u043E\u0438\u0441\u043A \u0437\u0430\u0434\u0430\u0447 \u043F\u043E \u0444\u0438\u043B\u044C\u0442\u0440\u0443 `" + jql + "`");
                    return [4, params.jira.search.search({ jql: jql, method: 'post', maxResults: 200 })];
                case 1:
                    result = _a.sent();
                    context.logger.log("\u041D\u0430\u0439\u0434\u0435\u043D\u043E \u0442\u0438\u043A\u0435\u0442\u043E\u0432 \u043F\u043E \u0444\u0438\u043B\u044C\u0442\u0440\u0443: " + result.total);
                    filterIssues_1 = new Set(result.issues.map(function (x) { return x.key; }));
                    newIssues = params.tickets.filter(function (x) { return !filterIssues_1.has(x); });
                    context.logger.info("\u0412\u044B\u0431\u0440\u0430\u043D\u043E \u043D\u043E\u0432\u044B\u0445 \u0442\u0438\u043A\u0435\u0442\u043E\u0432 - " + newIssues.length + ". \u0417\u0430\u0434\u0430\u0447\u0438: " + newIssues.join(', '));
                    return [2, newIssues];
                case 2:
                    error_1 = _a.sent();
                    context.logger.error('Не удалось получить тикеты из фильтра. Обновляем все задачи.');
                    return [3, 3];
                case 3: return [2, params.tickets];
            }
        });
    });
};
exports.selectNewIssues = selectNewIssues;
function setFixVersion(config, context, params) {
    return __awaiter(this, void 0, void 0, function () {
        var issuesForUpdate, release, jira, concurrentLimit, edits;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4, exports.selectNewIssues(config, context, params)];
                case 1:
                    issuesForUpdate = _a.sent();
                    if (issuesForUpdate.length === 0) {
                        context.logger.info('Новых тикетов не найдено.');
                        return [2];
                    }
                    release = params.release, jira = params.jira;
                    concurrentLimit = p_limit_1.default(config.networkConcurrency || types_1.DEFAULT_NETWORK_CONCURENCY);
                    edits = issuesForUpdate.map(function (issueKey) {
                        return concurrentLimit(function () {
                            context.logger.info("\u041F\u0440\u043E\u0441\u0442\u0430\u0432\u043B\u044F\u0435\u043C fix-version '" + release.name + "' \u0432 \u0437\u0430\u0434\u0430\u0447\u0443 " + issueKey);
                            void exports.updateIssue(context, { jira: jira, releaseVersionId: release.id, issueKey: issueKey });
                        });
                    });
                    return [4, Promise.all(edits)];
                case 2:
                    _a.sent();
                    return [2];
            }
        });
    });
}
exports.default = setFixVersion;
