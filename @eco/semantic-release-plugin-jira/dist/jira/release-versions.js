"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPackageVersions = exports.NOT_FOUND_ERR_TEXT = void 0;
var error_1 = __importDefault(require("@semantic-release/error"));
var p_limit_1 = __importDefault(require("p-limit"));
var semver_1 = require("semver");
var types_1 = require("../types");
var extract_version_from_fix_version_name_1 = __importDefault(require("../utils/extract-version-from-fix-version-name"));
var edit_version_1 = __importDefault(require("./edit-version"));
exports.NOT_FOUND_ERR_TEXT = 'Не найдена версия подходящая версия.';
var getPackageVersions = function getPackageVersions(allVersions, config, context) {
    var template = config.releaseNameTemplate || types_1.DEFAULT_VERSION_TEMPLATE;
    return allVersions
        .filter(function (x) { return !x.archived && x.name.includes(context.env.npm_package_name); })
        .sort(function (a, b) {
        var v1 = extract_version_from_fix_version_name_1.default(a.name, template);
        var v2 = extract_version_from_fix_version_name_1.default(b.name, template);
        return semver_1.rcompare(v1, v2);
    });
};
exports.getPackageVersions = getPackageVersions;
function releaseVersions(config, context, jira) {
    return __awaiter(this, void 0, void 0, function () {
        var remoteVersions, versions, activeVersions, _a, releaseBranch, currentVersionIndex, currentVersion, archived, concurrentLimit, edits;
        var _this = this;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    context.logger.info('Передан флаг JIRA_RELEASE. Запущен процесс выпуска версии Jira');
                    return [4, jira.project.getVersions({ projectIdOrKey: config.projectId })];
                case 1:
                    remoteVersions = _b.sent();
                    versions = exports.getPackageVersions(remoteVersions, config, context);
                    activeVersions = versions.filter(function (x) { return !x.released; });
                    _a = config.releaseBranch, releaseBranch = _a === void 0 ? types_1.RELEASE_BRANCH : _a;
                    currentVersionIndex = releaseBranch === context.branch.name ? activeVersions.length - 1 : 1;
                    currentVersion = activeVersions[currentVersionIndex];
                    if (!currentVersion) {
                        throw new error_1.default(exports.NOT_FOUND_ERR_TEXT);
                    }
                    context.logger.info("\u041D\u0430\u0439\u0434\u0435\u043D\u0430 \u0432\u0435\u0440\u0441\u0438\u044F '" + currentVersion.id + "'.");
                    return [4, edit_version_1.default(context, jira, { versionId: currentVersion.id, version: { released: true, releaseDate: new Date().toISOString() } })];
                case 2:
                    _b.sent();
                    context.logger.info("\u0412\u0435\u0440\u0441\u0438\u044F { Id:'" + currentVersion.id + "', " + currentVersion.name + " } \u0432\u044B\u043F\u0443\u0449\u0435\u043D\u0430");
                    archived = versions.filter(function (x) { return x.released; });
                    concurrentLimit = p_limit_1.default(config.networkConcurrency || types_1.DEFAULT_NETWORK_CONCURENCY);
                    edits = archived.map(function (v) {
                        return concurrentLimit(function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4, edit_version_1.default(context, jira, { versionId: v.id, version: { archived: true } })];
                                    case 1:
                                        _a.sent();
                                        context.logger.info("\u0412\u0435\u0440\u0441\u0438\u044F { Id: '" + v.id + "', " + v.name + " } \u043F\u0435\u0440\u0435\u043C\u0435\u0449\u0430\u043D\u0430 \u0432 \u0430\u0440\u0445\u0438\u0432.");
                                        return [2];
                                }
                            });
                        }); });
                    });
                    return [2, Promise.all(edits)];
            }
        });
    });
}
exports.default = releaseVersions;
