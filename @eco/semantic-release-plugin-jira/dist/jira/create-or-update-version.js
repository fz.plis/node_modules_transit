"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var edit_version_1 = __importDefault(require("./edit-version"));
function createOrUpdateVersion(config, context, params) {
    return __awaiter(this, void 0, void 0, function () {
        var jira, projectIdOrKey, releaseVersion, description, lastVersion, remoteVersions, existing, last, newVersion;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    jira = params.jira, projectIdOrKey = params.projectIdOrKey, releaseVersion = params.releaseVersion, description = params.description, lastVersion = params.lastVersion;
                    return [4, jira.project.getVersions({ projectIdOrKey: projectIdOrKey })];
                case 1:
                    remoteVersions = _a.sent();
                    if (context.env.JIRA_FORCE_VERSION) {
                        context.logger.info('Передан флаг JIRA_FORCE_VERSION. Проверка существующих релизов в Jira пропущена.');
                    }
                    else {
                        context.logger.info("\u041F\u043E\u0438\u0441\u043A \u0440\u0435\u043B\u0438\u0437\u0430 '" + releaseVersion + "'");
                        existing = remoteVersions.find(function (x) { return x.name === releaseVersion; });
                        if (existing) {
                            context.logger.info("\u041D\u0430\u0439\u0434\u0435\u043D \u0440\u0435\u043B\u0438\u0437 '" + existing.id + "'");
                            return [2, existing];
                        }
                        context.logger.info("\u0420\u0435\u043B\u0438\u0437\u0430 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D\u043E '" + releaseVersion + "'. \u041F\u0440\u043E\u0431\u0443\u0435\u043C \u043D\u0430\u0439\u0442\u0438 \u0440\u0435\u043B\u0438\u0437 '" + lastVersion + "'");
                        last = remoteVersions.find(function (x) { return x.name === lastVersion && !x.released; });
                        if (last) {
                            context.logger.info("\u041D\u0430\u0439\u0434\u0435\u043D\u0430 \u043D\u0435\u0432\u044B\u043F\u0443\u0449\u0435\u043D\u043D\u0430\u044F \u0432\u0435\u0440\u0441\u0438\u044F '" + last.id + "'. \u041E\u043D\u0430 \u0431\u0443\u0434\u0435\u0442 \u043E\u0431\u043D\u043E\u0432\u043B\u0435\u043D\u0430 \u0434\u043E '" + releaseVersion + "'");
                            return [2, edit_version_1.default(context, jira, { versionId: last.id, version: { name: releaseVersion } })];
                        }
                        context.logger.info('Релизов не найдено, создаем новый');
                    }
                    return [4, jira.version.createVersion({
                            name: releaseVersion,
                            projectId: projectIdOrKey,
                            description: description || '',
                            released: !config.dryRun && !!config.released,
                            startDate: new Date().toISOString(),
                            releaseDate: config.setReleaseDate ? new Date().toISOString() : undefined,
                        })];
                case 2:
                    newVersion = _a.sent();
                    context.logger.info("\u0421\u043E\u0437\u0434\u0430\u043D \u0440\u0435\u043B\u0438\u0437 '" + newVersion.id + "'");
                    return [2, newVersion];
            }
        });
    });
}
exports.default = createOrUpdateVersion;
