"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jira_1 = require("./jira");
var get_release_info_1 = __importDefault(require("./utils/get-release-info"));
var get_tickets_1 = __importDefault(require("./utils/get-tickets"));
function verifyRelease(config, context) {
    return __awaiter(this, void 0, void 0, function () {
        var releaseInfo, jiraApi, project, release, tickets;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (context.env.JIRA_OFF) {
                        context.logger.log('Передан флаг JIRA_OFF. Привязка к релизам Jira была пропущена.');
                        return [2];
                    }
                    releaseInfo = get_release_info_1.default(config, context);
                    context.logger.info("\u041E\u043F\u0440\u0435\u0434\u0435\u043B\u0435\u043D \u0440\u0435\u043B\u0438\u0437 '" + releaseInfo.releaseVersion + "'");
                    jiraApi = jira_1.jira(config, context);
                    return [4, jiraApi.project.getProject({ projectIdOrKey: config.projectId })];
                case 1:
                    project = _a.sent();
                    return [4, jira_1.createOrUpdateVersion(config, context, __assign(__assign({}, releaseInfo), { jira: jiraApi, projectIdOrKey: project.id }))];
                case 2:
                    release = _a.sent();
                    tickets = get_tickets_1.default(config, context);
                    if (tickets.length === 0) {
                        context.logger.info('Тикетов не найдено');
                        return [2];
                    }
                    context.logger.info("\u041D\u0430\u0439\u0434\u0435\u043D\u043E \u0442\u0438\u043A\u0435\u0442\u043E\u0432: (" + tickets.length + ")");
                    return [2, jira_1.setFixVersion(config, context, { tickets: tickets, release: release, jira: jiraApi })];
            }
        });
    });
}
exports.default = verifyRelease;
