module.exports = {
  mode: "file",
  out: "docs-api",
  excludeNotExported: true,
  excludePrivate: true,
  excludeNotDocumented: true,
  includes: "docs",
  includeVersion: true,
  stripInternal: true,
  disableSources: true,
};
