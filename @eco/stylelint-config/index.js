module.exports = {
  extends: ['stylelint-config-standard', 'stylelint-prettier/recommended'],
  plugins: [
    'stylelint-scss',
    'stylelint-order',
    'stylelint-config-rational-order/plugin',
    'stylelint-no-unsupported-browser-features',
    './unsupported-selector-plugin.js',
    'stylelint-prettier',
    './z-index-plugin.js',
  ],
  rules: {
    'prettier/prettier': true,
    'declaration-block-no-redundant-longhand-properties': true,
    'max-nesting-depth': 3,
    'at-rule-no-unknown': null,
    'at-rule-empty-line-before': null,
    'scss/at-rule-no-unknown': true,
    'order/order': ['custom-properties', 'at-rules', 'declarations'],
    indentation: 2,
    'plugin/unsupported-abstract-selectors': true,
    'plugin/z-index': true,
    'plugin/no-unsupported-browser-features': [
      true,
      {
        browsers: ['IE >= 11', 'last 1 Chrome versions', 'last 1 Safari versions', 'last 1 Firefox versions'],
        ignore: ['outline', 'viewport-units', 'flexbox', 'css-gradients', 'css-initial-value', 'calc', 'will-change'],
        severity: 'error',
      },
    ],
    'declaration-no-important': [
      true,
      {
        severity: 'warning',
      },
    ],
    'unit-disallowed-list': ['em', 'rem'],
    'declaration-block-no-duplicate-properties': true,
    'selector-nested-pattern': null,
    'function-name-case': null,
    'value-keyword-case': null,
    'order/properties-order': null,
    'plugin/rational-order': [
      true,
      {
        'border-in-box-model': false,
      },
    ],
  },
};
