# @eco/stylelint-config

Пакет содержит предустановленный набор правил stylelint, для линтинга платформенных пакетов и стримов.


## Установка

Для установки нужно установить сам пакет

```
npm i -D -E @eco/stylelint-config
```

А также пакеты совместимой версии, указанные в devDependencies.

```
npm i -D -E stylelint stylelint-{config-prettier,no-unsupported-browser-features,config-standard,scss,prettier,order}
```

## Конфигурирование

В пакете нужно создать конфиг, расширенный от `@eco`.

```js
// .stylelintrc.js
module.exports = {
  extends: ['@eco/stylelint-config'],
}
```

Можно использовать другие форматы конфигурации, поддерживаемые [Stylelint](https://stylelint.io/user-guide/rules/list)

## Конфигурирование prettier

В конфигурационный файл prettier, если установлен parser по умолчанию, отличный от scss, нужно добавить следующую настрйоку:
```js
// .prettierrc.js
module.exports = {
  ...
  parser: 'typescript',
  jsxSingleQuote: false,
  arrowParens: 'avoid',
  overrides: [
    {
      files: 'src/**/*.scss',
      options: {
      parser: 'scss',
     },
    },
  ],
}
```
