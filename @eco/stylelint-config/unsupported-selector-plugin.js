const stylelint = require('stylelint')

const ruleName = 'plugin/unsupported-abstract-selectors'
const messages = stylelint.utils.ruleMessages(ruleName, {
  rejected: selector => `Unsupported selector ${selector}`,
})

const RULE_TYPE = 'rule'
const abstractSelectorRegexp = />[\w\*]/

const checkSelector = nodes => {
  return nodes.reduce((acc, node) => {
    const { type, nodes: children, selector } = node
    if (type === RULE_TYPE) {
      const s = selector.replace(/\s/g, '')
      const res = abstractSelectorRegexp.test(s)
      if (res) {
        acc.push({
          node: node,
          line: node.source.start.line,
          column: node.source.start.column,
        })
      }

      if (children) {
        return [...acc, ...checkSelector(children)]
      }

      return acc
    }

    return acc
  }, [])
}

const ruleFunction = () => (root, result) => {
  const validOptions = stylelint.utils.validateOptions(result, ruleName)
  if (!validOptions) {
    return
  }

  const reportResults = checkSelector(root.nodes)
  reportResults.forEach(item => {
    stylelint.utils.report({
      ruleName,
      result,
      message: messages.rejected(item.node.selector),
      node: item.node,
      line: item.line,
      column: item.column,
    })
  })
}

module.exports = stylelint.createPlugin(ruleName, ruleFunction)
module.exports.ruleName = ruleName
module.exports.messages = messages
