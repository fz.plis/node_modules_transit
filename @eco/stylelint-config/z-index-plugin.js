const stylelint = require('stylelint');

const ruleName = 'plugin/z-index';
const messages = stylelint.utils.ruleMessages(ruleName, {
  rejected: () => `Use of numbers in z-index is forbidden. Use getIndex function instead.
  Example
  @import '../../common';

  .z-index-class {
    z-index: getIndex(element_type);
  }`,
});

const checkSelector = nodes => {
  return nodes.reduce((acc, node) => {
    const { nodes: children = [] } = node;
    children.forEach(n => {
      if (n.prop === 'z-index' && !isNaN(n.value)) {
        acc.push({
          node: n,
          line: n.source.start.line,
          column: n.source.start.column,
        });
      }
    });

    return acc;
  }, []);
};

const ruleFunction = () => (root, result) => {
  const validOptions = stylelint.utils.validateOptions(result, ruleName);
  if (!validOptions) {
    return;
  }

  const reportResults = checkSelector(root.nodes);
  reportResults.forEach(item => {
    stylelint.utils.report({
      ruleName,
      result,
      message: messages.rejected(item.node.selector),
      node: item.node,
      line: item.line,
      column: item.column,
    });
  });
};

module.exports = stylelint.createPlugin(ruleName, ruleFunction);
module.exports.ruleName = ruleName;
module.exports.messages = messages;
