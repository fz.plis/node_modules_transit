# eco/cz-config

Пакет содержит конфиг для интерактивного ввода коммитов через [commitizen](https://github.com/commitizen/cz-cli) и [cz-customizable](https://github.com/leoforfree/cz-customizable).

## Установка

Для установки нужно установить сам пакет

```
npm i -DE @eco/cz-config
```

А также пакеты совместимой версии, указанные в peerDependencies.

```
npm i -D -E commitizen cz-customizable
```

## Конфигурирование

### Дефолтный конфиг

Нужно выполнить действия указанные в [руководстве cz-customizable](https://github.com/leoforfree/cz-customizable#configuration-shared-between-options-1-and-2)

### Переопределение доступных scopes

В пакете нужно создать файл `cz-config`. Скоупы можно прописать через `spread`-оператор.

```js
// .cz-config.js
const baseConfig = require("@eco/cz-config");

module.exports = {
  ...baseConfig,
  scopes: [
    {
      name: "один из скоупов стрима",
    },
    {
      name: "другой скоуп",
    },
  ],
};
```

Также можно переопределить скоупы для отдельных типов коммита

```js
// .cz-config.js
const baseConfig = require("@eco/cz-config");

module.exports = {
  ...baseConfig,
  scopeOverrides: {
    chore: [
      { name: "ci" },
      { name: "build" },
      { name: "demo" },
      { name: "devtools" },
    ],
    docs: [{ name: "readme" }, { name: "jsdoc" }, { name: "документация" }],
  },
};
```

### Дополнения для встраивающихся в Экосистему

По умолчанию в конфиг зашит префикс проекта Экосистемы - **GBO**

Если команда использует другое пространство, то можно переопределить стандартный конфиг или взять за основу дефолтный, поставляемый с пакетом

```js
// .cz-config.js
// берем дефолтный конфиг
const baseConfig = require("@eco/cz-config/defaults");

module.exports = require("@eco/cz-config/defaults");
```

В этом случае утилита `commitizen` будет спрашивать номер задачи с указанием префикса.
