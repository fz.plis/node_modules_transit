const defaultConfig = require("./defaults");

module.exports = {
  ...defaultConfig,
  messages: {
    ...defaultConfig.messages,
    ticketNumber: 'Введите номер задачи (только цифры, максимум 6 цифр)',
  },
  ticketNumberPrefix: "GBO-",
  ticketNumberRegExp: "\\d{1,6}",
};
