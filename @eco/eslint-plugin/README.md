# `@eco/eslint-localization-plugin`

Плагин, содержащий дополнительные правила линтинга исходного кода для проектов ЭКО.

## Установка

```
npm i -D -E @eco/eslint-config
```

## Настройка

Плагин подключается согласно руководству [Eslint](https://eslint.org/docs/user-guide/configuring#configuring-pluginss)

```js
// eslintrc.js
module.exports = {
  plugins: ["@eco"],
  rules: {
    "@eco/no-missing-localization": "error",
  },
};
```

## Правила

| Имя                                                                       | Описание                                                   | Рекомендовано | Автофикс |
| ------------------------------------------------------------------------- | ---------------------------------------------------------- | :-----------: | :------: |
| [`@eco/no-missing-localization`](docs/rules/no-missing-localization.md) | Требуется вынести локализуемые ресурсы в файлы локализации |       +       |          |
| [`@eco/no-non-ascii-symbols`](docs/rules/no-non-ascii-symbols.md)       | Запрещает использование не-ASCII символов в определениях   |       +       |          |
