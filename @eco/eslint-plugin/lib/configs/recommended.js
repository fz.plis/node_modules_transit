module.exports = {
  plugins: ["@eco"],
  rules: {
    "@eco/no-missing-localization": [
      "error",
      { ignoreFilePatterns: ["**/*.stories.tsx", "**/*.(test|spec).(ts|tsx)"] },
    ],
    "@eco/no-non-ascii-symbols": "error",
  },
};
