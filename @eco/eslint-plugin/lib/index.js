const rules = require("./rules");
const configs = require("./configs");

module.exports = {
  rules,
  configs,
};
