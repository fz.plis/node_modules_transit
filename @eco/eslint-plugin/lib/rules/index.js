const noMissingLocalizationRule = require("./no-missing-localization");
const noNonAsciiSymbols = require("./no-non-ascii-symbols");

module.exports = {
  "no-non-ascii-symbols": noNonAsciiSymbols,
  "no-missing-localization": noMissingLocalizationRule,
};
