const micromatch = require('micromatch');

module.exports = {
  meta: {
    type: 'suggestion',
    docs: {
      description: 'Локализуемые ресурсы должны быть размещены в файлах локализации',
      category: 'Possible Errors',
      recommended: true,
    },
    schema: [
      {
        type: 'object',
        properties: {
          ignoreFilePatterns: {
            type: 'array',
            items: {
              type: 'string',
            },
          },
        },
        additionalProperties: false,
      },
    ],
  },
  create(context) {
    const { options = [{}] } = context;
    const [{ ignoreFilePatterns }] = options;
    const report = (node, resource) =>
      context.report({
        node,
        message: 'Ресурс "{{ resource }}" должен быть вынесен в файл локализации',
        data: {
          resource,
        },
      });

    const tester = val => /[А-яЁё]/.test(val);
    const applyRule = (node, val) => {
      if (ignoreFilePatterns && micromatch.isMatch(context.getFilename(), ignoreFilePatterns)) {
        return;
      }

      if (tester(val)) {
        report(node, val);
      }
    };

    return {
      TemplateElement(node) {
        applyRule(node, node.value.raw);
      },
      Literal(node) {
        if (node.regex) {
          return;
        }

        applyRule(node, node.value);
      },
    };
  },
};
