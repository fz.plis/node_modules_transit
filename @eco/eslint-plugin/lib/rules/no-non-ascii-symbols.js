module.exports = (context) => {
  return {
    Identifier(node) {
      const matches = node.name.match(/([^\x00-\x7F]+)/g);

      if (matches) {
        context.report({
          node,
          message: `Найдены не-ascii символы в определении '${node.name}': '${matches}'`,
        });
      }
    },
  };
};
