const plugins = require('./lib/plugins');
const branches = require('./lib/branches');
const tagFormat = require('./lib/tag-format');

/**
 * Дефолтный конфиг без плагина '@eco/semantic-release-plugin-jira'.
 */
module.exports = {
  branches,
  tagFormat,
  plugins,
};
