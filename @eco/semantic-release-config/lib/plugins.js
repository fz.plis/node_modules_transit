/**
 * Список дефолтных плагинов с настройками по умолчанию.
 */
 module.exports = [
  '@semantic-release/changelog',
  [
    '@eco/commit-analyzer',
    {
      overrideBranches: {
        'hotfix-+([0-9])?(.{+([0-9]),x}).x': 'patch',
        'rc-+([0-9])?(.{+([0-9]),x}).x': 'patch',
        stage: 'patch',
        release: 'patch',
      },
      releaseRules: [
        { type: 'docs', scope: 'readme', release: 'patch' },
        { type: 'docs', scope: 'jsdoc', release: 'patch' },
        { type: 'docs', scope: 'документация', release: 'patch' },
        { type: 'chore', name: 'demo', release: 'patch' },
        { type: 'chore', scope: 'build', release: 'patch' },
        { type: 'chore', scope: 'devtools', release: 'patch' },
        { type: 'refactor', release: 'patch' },
        { type: 'style', release: 'patch' },
        { type: 'test', release: 'patch' },
        { type: 'perf', release: 'patch' },
        { scope: 'no-release', release: false },
      ],
    },
  ],
  '@semantic-release/release-notes-generator',
  '@semantic-release/npm',
  [
    '@semantic-release/git',
    {
      assets: ['CHANGELOG.md', 'package.json'],
      message: 'chore(release): ${nextRelease.version} [skip ci] \n\n${nextRelease.notes}',
    },
  ],
];
