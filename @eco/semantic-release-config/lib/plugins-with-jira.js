const plugins = require('./plugins');

/**
 * Дополненный список с включенным плагином @eco/semantic-release-plugin-jira.
 */
module.exports = [
  ...plugins,
  [
    '@eco/semantic-release-plugin-jira',
    {
      projectId: 'GBO',
      jiraHost: 'jira.gboteam.ru',
      ticketPrefixes: ['GBO'],
      stageBranch: 'stage',
      releaseBranch: 'release',
    },
  ],
];
