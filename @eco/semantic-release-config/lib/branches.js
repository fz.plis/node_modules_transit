/**
 * Список веток, в которых срабатывает semantic-release по умолчанию.
 */
module.exports =  [
  'master',
  'next',
  'next-major',
  { name: '+([0-9])?(.{+([0-9]),x}).x', channel: 'hotfix' },
  { name: '+([0-9])?(.{+([0-9]),x}).x-stage', channel: 'rc' },
  { name: 'hotfix-+([0-9])?(.{+([0-9]),x}).x', channel: 'hotfix' },
  { name: 'rc-+([0-9])?(.{+([0-9]),x}).x', channel: 'rc' },
  { name: 'beta', prerelease: true },
  { name: 'alpha', prerelease: true },
  { name: 'develop', channel: 'dev', prerelease: true },
]