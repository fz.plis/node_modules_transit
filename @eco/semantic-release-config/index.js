const plugins = require('./lib/plugins-with-jira');
const branches = require('./lib/branches');
const tagFormat = require('./lib/tag-format');

/**
 * Конфиг semantic-release со всеми требуемыми для стримов плагинами.
 */
module.exports = {
  branches,
  tagFormat,
  plugins,
};
