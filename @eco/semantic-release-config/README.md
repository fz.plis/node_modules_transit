# eco/semantic-release-config

Пакет содержит конфиг для утилиты [Semantic-Release](https://github.com/semantic-release/semantic-release)

## Установка

Для установки нужно установить сам пакет

```
npm i -DE @eco/semantic-release-config
```

А также пакеты совместимой версии, указанные в peerDependencies.

```
npm i -DE @eco/{commit-analyzer,semantic-release-plugin-jira}
```

## Использование

Для выпуска версии достаточно воспользоваться флагом `--extends` semantic-release-cli.

**Этот способ является предпочтительным вместо использования файла `.releaserc.js`.**

При использовании файла могут возникнуть проблемы со сборками из-за существования трех стабильных веток.

```sh
npx run semantic-release -e @eco/semantic-release-config
```

Или:

```sh
npx run semantic-release -e @eco/semantic-release-config/patch
```

Или:

```sh
npx run semantic-release -e @eco/semantic-release-config/hotfix
```

### Как использовать в стримах?

Чтобы утилита semantic-release корректно работала в трех постоянных ветках следует выполнить следующее.

Удалить конфигурационный файл semantic-release - обычно это `.releaserc.js`.

Модифицировать команду semantic-release в package.json

```json
{
  "scripts": {
    "semantic-release": "semantic-release -e @eco/semantic-release-config"
  }
}
```

Добавить команды `release:patch` и `release:hotfix` в package.json

```json
{
  "scripts": {
    "semantic-release": "semantic-release -e @eco/semantic-release-config",
    "release:patch": "semantic-release -e @eco/semantic-release-config/patch",
    "release:hotfix": "semantic-release -e @eco/semantic-release-config/hotfix"
  }
}
```

## Конфигурирование

В пакете нужно создать файл `.releaserc.js`

```js
// .releaserc.js
module.exports = {
  extends: '@eco/semantic-release-config',
}
```

Дефолтный импорт содержит максимальный конфиг. Также доступен упрощенный (без плагина `@eco/semantic-release-plugin-jira`).

```js
// .releaserc.js
module.exports = {
  extends: '@eco/semantic-release-config/defaults',
}
```

Для корректного выпуска в ветках stage и release следует использовать конфиги `@eco/semantic-release-config/patch` и `@eco/semantic-release-config/hotfix` соответственно.

```js
// .releaserc.js
module.exports = {
  extends: '@eco/semantic-release-config/patch',
}
```

```js
// .releaserc.js
module.exports = {
  extends: '@eco/semantic-release-config/hotfix',
}
```
