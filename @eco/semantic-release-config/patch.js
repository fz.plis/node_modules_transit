const plugins = require('./lib/plugins-with-jira');
const tagFormat = require('./lib/tag-format');

/**
 * Конфиг semantic-release со всеми требуемыми для стримов плагинами
 * для выпуска patch-версий в ветке stage.
 */
module.exports = {
  branches: [
    { name: 'stage', channel: 'rc', },
  ],
  tagFormat,
  plugins,
};
