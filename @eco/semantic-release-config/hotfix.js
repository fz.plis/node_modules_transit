const plugins = require('./lib/plugins-with-jira');
const tagFormat = require('./lib/tag-format');

/**
 * Конфиг semantic-release со всеми требуемыми для стримов плагинами
 * для выпуска patch-версий в ветке release.
 */
module.exports = {
  branches: [
    { name: 'release', channel: 'hotfix', },
  ],
  tagFormat,
  plugins,
};
