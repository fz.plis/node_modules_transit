# eco/testing

Пакет содержит пресеты для конфигурационных файлов Jest.

В папке **lib/platforms** содержатся пресеты для пакетов платформы.

В папке **lib/streams** содержатся пресеты для стримов.

# Migration guide
- Обновить платформу, в стриме.
- Создать файл jest.config.ts:

```ts
// jest.config.ts
import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  preset: './node_modules/@eco/testing/lib/streams/cov-min.stream.config.js',
  setupFiles: ['<rootDir>/test-setup.ts'], // При необходимости, см. ниже.
};

export default config;
```
- В поле **preset** указать необходимый пресет. Пресеты различаются требованием к покрытию тестами.
Доступные пресеты описаны в [Доступные конфигурации для стримов](#доступные-конфигурации-для-стримов). Если ни один пресет не подходит по покрытию, то надо либо увеличить покрытие в стриме, либо использовать base.stream.config.js (в нём не указано покрытие)
- Если стрим содержит файл **test-setup.ts** и если в этом файле происходит только настройка локализации и установка enzyme адаптера, то этот файл нужно удалить. Если локализация не будет работать, то сделать как описано в [Проблемы с локализацией в стримах](#проблемы-с-локализацией-в-стримах)

- Добавить в .gitignore

```
jest-sonar
```

# Доступные конфигурации для стримов:

| Конфиг | Комментарий | Statements, % | Branches, % | Functions, % | Lines, % |
|--------|-------------|---------------|-------------|--------------|----------|
| default | К чему стремимся, alias для cov-standart | 70 | 70 | 70 |70|
| cov-min | Медианный, периодически будем пересматривать | 20 | 5 | 10 |25|
| cov-avg | По среднему значению, периодически будем пересматривать | 30 | 10 | 15 | 30 |
| cov-30 | Для выхода за медиану и среднее значение | 30 | 30 | 30 | 30 |
| cov-40 | Конфиг для поэтапного повышения | 40 | 40 | 40 | 40 |
| cov-50 | Конфиг для поэтапного повышения | 50 | 50 | 50 | 50 |
| cov-60 | Конфиг для поэтапного повышения | 60 | 60 | 60 | 60 |
| cov-standard | Общепринятый стандарт | 70 | 70 | 70 | 70 |
| cov-high | Для повышенного требования к покрытию для важных пакетов | 80 | 80 | 80 | 80 |
| cov-crit | Для повышенного требования к покрытию для критически важных пакетов | 90 | 90 | 90 | 90 |
| cov-perfect | Для идеалистов | 100 | 100 | 100 | 100 |

# Возможные проблемы

## Проблемы с локализацией в стримах

Jest сконфигурирован так, что по умолчанию пытается получить имя локали из package.json.
Если в packaje.json будет **{ "name": "@eco/stream-name" }**, то имя локали будет **"stream-name"**.
Если такое поведение не подходит, то можно указать имя локали через имя переменно среды при вызове jest,
**{ "scripts": { "test": "STREAM_LOCALE_NAME=stream-name jest" } }**

