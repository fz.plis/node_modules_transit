import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
  testURL: 'http://localhost/',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  moduleNameMapper: {
    '^.+\\.(css|less|scss|png|jpg)$': 'identity-obj-proxy',
    '.+\\.svg$': 'identity-obj-proxy', // Выделено в отдельное правило, для удобства переопределения в @platform/ui
    '^@platform/ui$': '<rootDir>/../ui/src',
    '^@platform/core$': '<rootDir>/../core/src',
    '^@platform/tools/(.*)$': '<rootDir>/../tools/src/$1',
  },
  transform: {
    '^.+\\.(ts|tsx|js)$': 'ts-jest',
  },
  testMatch: [
    '<rootDir>/src/**/__tests__/*.(ts|tsx)',
    '<rootDir>/lib/**/__tests__/*.(ts|tsx)',
    '<rootDir>/__tests__/**.(ts|tsx)',
    '<rootDir>/tests/**.(ts|tsx)',
  ],
  collectCoverage: true,
  collectCoverageFrom: ['./src/**/*.{ts,tsx}'],
  reporters: [
    'default',
    [
      'jest-sonar',
      {
        outputDirectory: 'jest-sonar',
        outputName: 'jest-sonar-report.xml',
        reportedFilePath: 'absolute',
      },
    ],
  ],
  coverageReporters: ['text', 'text-summary', 'html', 'lcov'],
  testPathIgnorePatterns: [],
};

export default config;
