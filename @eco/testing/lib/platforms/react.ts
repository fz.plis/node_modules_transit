import type { Config } from '@jest/types';
import defaultConfig from './defaults';

const config: Config.InitialOptions = {
  ...defaultConfig,
  snapshotSerializers: ['enzyme-to-json/serializer'],
  setupFiles: ['@eco/testing/lib/platforms/setup-files/enzyme.ts', '<rootDir>/test-setup.ts'],
};

export default config;
