const baseConfig = require('./base.stream.config');

const config = {
  ...baseConfig,
  coverageThreshold: {
    global: {
      statements: 50,
      branches: 50,
      functions: 50,
      lines: 50,
    },
  },
};

module.exports = config;
