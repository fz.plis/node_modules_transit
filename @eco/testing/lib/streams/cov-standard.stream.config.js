const baseConfig = require('./base.stream.config');

const config = {
  ...baseConfig,
  coverageThreshold: {
    global: {
      statements: 70,
      branches: 70,
      functions: 70,
      lines: 70,
    },
  },
};

module.exports = config;
