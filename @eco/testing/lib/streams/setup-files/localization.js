const localeServices = require('@platform/services/client/_services.ru.json');
const translation = require('@platform/tools/localization');
const fs = require('fs');

const cwd = process.cwd();

let localeName;
let localeRU;

// Пытается получить имя файла локализации.
// Если в package.json стрима есть { "scripts": { "test": "STREAM_LOCALE_NAME=stream-name jest" } },
// то значение localeName будет "stream-name",
// иначе, если в package.json есть { "name": "@eco/stream-name" } то localeName будет "stream-name".
localeName = process.env.STREAM_LOCALE_NAME
  ? process.env.STREAM_LOCALE_NAME
  : JSON.parse(fs.readFileSync(`${cwd}/package.json`)).name.split('/')[1];

localeRU = require(`${cwd}/dictionaries/${localeName}.ru.json`);

translation.registerArea(localeName, () => localeRU);

translation.registerArea('_services', () => localeServices);
