const baseConfig = require('./base.stream.config');

const config = {
  ...baseConfig,
  coverageThreshold: {
    global: {
      statements: 30,
      branches: 10,
      functions: 15,
      lines: 30,
    },
  },
};

module.exports = config;
