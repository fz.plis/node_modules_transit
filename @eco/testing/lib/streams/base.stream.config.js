const config = {
  testURL: 'http://localhost/',
  roots: ['<rootDir>/src'],
  modulePaths: ['node_modules', '<rootDir>/src'],
  setupFiles: ['@eco/testing/lib/streams/setup-files/index'],
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  testMatch: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)(spec|test).[jt]s?(x)'],
  moduleNameMapper: {
    '^.+\\.(css|less|scss|svg|png|jpg)$': 'identity-obj-proxy',
    '^@platform/(.*)\\.(.*)$': '@platform/$1.$2',
    '^@platform/ui.*$': '@platform/ui',
    '^@platform/core.*$': '@platform/core',
    '^@platform/services.*$': '@platform/services',
  },
  reporters: [
    'default',
    [
      'jest-sonar',
      {
        outputDirectory: 'jest-sonar',
        outputName: 'jest-sonar-report.xml',
        reportedFilePath: 'absolute',
      },
    ],
  ],
  collectCoverage: true,
  collectCoverageFrom: ['./src/**/*.{ts,tsx}', '!./src/localization/*.ts'],
  coverageReporters: ['text', 'text-summary', 'html', 'lcov'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
};

module.exports = config;
