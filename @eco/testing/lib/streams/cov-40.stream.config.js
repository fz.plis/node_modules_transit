const baseConfig = require('./base.stream.config');

const config = {
  ...baseConfig,
  coverageThreshold: {
    global: {
      statements: 40,
      branches: 40,
      functions: 40,
      lines: 40,
    },
  },
};

module.exports = config;
