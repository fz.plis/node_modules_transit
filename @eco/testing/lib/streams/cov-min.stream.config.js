const baseConfig = require('./base.stream.config');

const config = {
  ...baseConfig,
  coverageThreshold: {
    global: {
      statements: 20,
      branches: 5,
      functions: 10,
      lines: 25,
    },
  },
};

module.exports = config;
