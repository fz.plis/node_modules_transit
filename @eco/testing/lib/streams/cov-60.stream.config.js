const baseConfig = require('./base.stream.config');

const config = {
  ...baseConfig,
  coverageThreshold: {
    global: {
      statements: 60,
      branches: 60,
      functions: 60,
      lines: 60,
    },
  },
};

module.exports = config;
