# `@eco/commit-analyzer`

Расширение плагина `@semantic-release/commit-analyzer`.
Позволяет в указанных в конфиге плагина ветках переопределить тип коммита, выбранный `@semantic-release/commit-analyzer`.

Плагин включает шаги:

- `verifyConditions`: проводит проверку конфига.
- `analyzeCommits`: проводит анализ коммитов.

Плагин `@eco/commit-analyzer` запускает `@semantic-release/commit-analyzer`.
Если последний выдал тип релиза, то `@eco/commit-analyzer` пытается проверить переопределение коммита в текущей ветке.

Если тип нового релиза, выданный `@semantic-release/commit-analyzer` выше релиза `@eco/commit-analyzer`, то выпускается последний.

## Установка

```
npm i -D -E @eco/commit-analyzer
```

## Использование

Например, в проекте используется конфиг.

```js
// .releaserc.js
module.exports = {
 plugins: [
    "@semantic-release/changelog",
    [
      "@eco/commit-analyzer",
      {
        overrideBranches: {
          'hotfix-+([0-9])?(.{+([0-9]),x}).x': 'patch',
          'rc-+([0-9])?(.{+([0-9]),x}).x': 'patch',
        }
    ],
    "@semantic-release/release-notes-generator",
    "@semantic-release/npm",
    "@semantic-release/git",
 ]
};
```

Если `@semantic-release/commit-analyzer` выпускает minor-релиз в ветке rc-1.2.x, то `@eco/commit-analyzer` понизит его до patch-релиза. Если релиз от `@semantic-release/commit-analyzer` будет ниже согласно semver чем patch (например, prepatch), то он и будет использован.

## Настройка

Плагин подключается согласно руководству [Semantic Release](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/plugins.md#plugins)

```js
// .releaserc.js
module.exports = {
  plugins: [
    "@semantic-release/changelog",
    [
      // подключаем плагин
      "@eco/commit-analyzer",
      {
        // переопредялем тип релиза в ветках hotfix-* и rc-* на `patch`
        overrideBranches: {
          "hotfix-+([0-9])?(.{+([0-9]),x}).x": "patch",
          "rc-+([0-9])?(.{+([0-9]),x}).x": "patch",
        },
        // опции для `@semantic-release/commit-analyzer`
        releaseRules: [
          { type: "docs", scope: "readme", release: "patch" },
          { type: "docs", scope: "jsdoc", release: "patch" },
          { type: "docs", scope: "документация", release: "patch" },
          { type: "chore", name: "demo", release: "patch" },
          { type: "chore", scope: "build", release: "patch" },
          { type: "chore", scope: "devtools", release: "patch" },
          { type: "refactor", release: "patch" },
          { type: "style", release: "patch" },
          { type: "test", release: "patch" },
          { type: "perf", release: "patch" },
          { scope: "no-release", release: false },
        ],
      },
    ],
    "@semantic-release/release-notes-generator",
    "@semantic-release/npm",
    [
      "@semantic-release/git",
      {
        assets: ["CHANGELOG.md", "package.json"],
        message:
          "chore(release): ${nextRelease.version} \n\n${nextRelease.notes}",
      },
    ],
  ],
};
```

### Опции

| **Опция** | **Описание** | **По умолчанию** |
|-----------|--------------|------------------|
| `overrideBranches` | Объект вида ключ-значение, в котором ключом является паттерн, а значением - тип релиза. Паттерн сравнивается через [`micromatch`](https://github.com/micromatch/micromatch) | - |

В настройках плагина можно указать опции для [`@semantic-release/commit-analyzer`](https://github.com/semantic-release/commit-analyzer)
