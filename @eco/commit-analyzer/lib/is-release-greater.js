const RELEASES = require("./releases");

/**
 * Функция возвращает `true`, если `releaseType` выше `currentReleaseType` согласно semver.
 * @param {string} currentReleaseType Тип релиза, сравниваемый с новым
 * @param {string} releaseType Тип релиза, который был вычислен
 */
module.exports = (currentReleaseType, releaseType) =>
  !currentReleaseType ||
  RELEASES.indexOf(releaseType) < RELEASES.indexOf(currentReleaseType);
