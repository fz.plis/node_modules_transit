/**
 * Шаг верификации перед запуском анализа коммитов.
 * @param {*} pluginOptions Настройки плагина из конфига.
 *
 * @see {@link https://github.com/semantic-release/semantic-release/blob/master/docs/usage/plugins.md#plugins|Руководство Semantic-release}
 */
module.exports = (pluginConfig) => {
  const { overrideBranches = {} } = pluginConfig;

  // Проверяем, что оверрайд - это объект.
  // Позже можно добавить дополнительные проверки - верификацию glob, допустимые релизы и т.д.
  if (typeof overrideBranches !== "object") {
    throw new Error("Config must be an object");
  }
};
