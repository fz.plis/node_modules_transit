/**
 * Список доступных релизов, начиная от старшего до младшего.
 */
module.exports = [
  "major",
  "premajor",
  "minor",
  "preminor",
  "patch",
  "prepatch",
  "prerelease",
];
