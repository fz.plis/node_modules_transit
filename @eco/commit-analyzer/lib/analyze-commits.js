const commitAnalyzer = require('@semantic-release/commit-analyzer');
const micromatch = require('micromatch');
const isReleaseGreater = require('./is-release-greater');

/**
 * Шаг анализа коммитов в semantic-release.
 * @param {*} pluginOptions Настройки плагина из конфига.
 * @param {*} context Контекст анализа плагином.
 *
 * @see {@link https://github.com/semantic-release/semantic-release/blob/master/docs/usage/plugins.md#plugins|Руководство Semantic-release}
 */
module.exports = async (pluginOptions, context) => {
  const { logger, branch } = context;
  const { overrideBranches } = pluginOptions;

  // Сперва проводим анализ стандартным плагином
  const commitRelease = await commitAnalyzer.analyzeCommits(pluginOptions, context);

  // Смотрим есть ли оверрайд релиза для текущей ветки в настройках плагина
  const releaseType = Object.entries(overrideBranches).find(([key, val]) => {
    if (micromatch.isMatch(branch.name, key)) {
      return val;
    }
  });

  // Если оверрайд есть и он ниже релиза от `@semantic-release/commit-analyzer`,
  // то возвращаем релиз из настроек
  if (commitRelease && releaseType && isReleaseGreater(releaseType[1], commitRelease)) {
    const [, release] = releaseType;
    logger.log('Release type in branch %s downgraded to "%s". Default: %s', branch.name, release, commitRelease);
    return release;
  }

  // Иначе возвращаем релиз от `@semantic-release/commit-analyzer`

  logger.log("There's no override for current branch. Fallback to release from @semantic-release/commit-analyzer");

  return commitRelease;
};
