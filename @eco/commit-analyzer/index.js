const verifyConditions = require("./lib/verify-conditions");
const analyzeCommits = require("./lib/analyze-commits");

module.exports = {
  verifyConditions,
  analyzeCommits,
};
