export declare class UnreachableCaseError extends Error {
    constructor(value: never);
}
export declare function assert(condition: any, msg?: string): asserts condition;
