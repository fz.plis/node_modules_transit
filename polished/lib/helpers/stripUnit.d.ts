declare function stripUnit(value: string | number, unitReturn?: boolean): any;

export default stripUnit;
