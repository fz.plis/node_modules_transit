declare function cssVar(
  cssVariable: string,
  passThrough?: boolean,
): string | number;

export default cssVar;
