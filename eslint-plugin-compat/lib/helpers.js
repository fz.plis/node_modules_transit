"use strict";

require("core-js/modules/es.object.from-entries");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lintCallExpression = lintCallExpression;
exports.lintNewExpression = lintNewExpression;
exports.lintExpressionStatement = lintExpressionStatement;
exports.lintMemberExpression = lintMemberExpression;
exports.reverseTargetMappings = reverseTargetMappings;
exports.determineTargetsFromConfig = determineTargetsFromConfig;
exports.parseBrowsersListVersion = parseBrowsersListVersion;

var _browserslist = _interopRequireDefault(require("browserslist"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint no-nested-ternary: off */

/*
3) Figures out which browsers user is targeting

- Uses browserslist config and/or targets defined eslint config to discover this
- For every API ecnountered during traversal, gets compat record for that
- Protochain (e.g. 'document.querySelector')
  - All of the rules have compatibility info attached to them
- Each API is given to versioning.ts with compatibility info
*/
function isInsideIfStatement(context) {
  return context.getAncestors().some(ancestor => {
    return ancestor.type === "IfStatement";
  });
}

function checkNotInsideIfStatementAndReport(context, handleFailingRule, failingRule, node) {
  if (!isInsideIfStatement(context)) {
    handleFailingRule(failingRule, node);
  }
}

function lintCallExpression(context, handleFailingRule, rules, node) {
  if (!node.callee) return;
  const calleeName = node.callee.name;
  const failingRule = rules.find(rule => rule.object === calleeName);
  if (failingRule) checkNotInsideIfStatementAndReport(context, handleFailingRule, failingRule, node);
}

function lintNewExpression(context, handleFailingRule, rules, node) {
  if (!node.callee) return;
  const calleeName = node.callee.name;
  const failingRule = rules.find(rule => rule.object === calleeName);
  if (failingRule) checkNotInsideIfStatementAndReport(context, handleFailingRule, failingRule, node);
}

function lintExpressionStatement(context, handleFailingRule, rules, node) {
  var _node$expression;

  if (!(node !== null && node !== void 0 && (_node$expression = node.expression) !== null && _node$expression !== void 0 && _node$expression.name)) return;
  const failingRule = rules.find(rule => {
    var _node$expression2;

    return rule.object === (node === null || node === void 0 ? void 0 : (_node$expression2 = node.expression) === null || _node$expression2 === void 0 ? void 0 : _node$expression2.name);
  });
  if (failingRule) checkNotInsideIfStatementAndReport(context, handleFailingRule, failingRule, node);
}

function protoChainFromMemberExpression(node) {
  if (!node.object) return [node.name];

  const protoChain = (() => {
    switch (node.object.type) {
      case "NewExpression":
      case "CallExpression":
        return protoChainFromMemberExpression(node.object.callee);

      default:
        return protoChainFromMemberExpression(node.object);
    }
  })();

  return [...protoChain, node.property.name];
}

function lintMemberExpression(context, handleFailingRule, rules, node) {
  if (!node.object || !node.property) return;

  if (!node.object.name || node.object.name === "window" || node.object.name === "globalThis") {
    const rawProtoChain = protoChainFromMemberExpression(node);
    const [firstObj] = rawProtoChain;
    const protoChain = firstObj === "window" || firstObj === "globalThis" ? rawProtoChain.slice(1) : rawProtoChain;
    const protoChainId = protoChain.join(".");
    const failingRule = rules.find(rule => rule.protoChainId === protoChainId);

    if (failingRule) {
      checkNotInsideIfStatementAndReport(context, handleFailingRule, failingRule, node);
    }
  } else {
    const objectName = node.object.name;
    const propertyName = node.property.name;
    const failingRule = rules.find(rule => rule.object === objectName && (rule.property == null || rule.property === propertyName));
    if (failingRule) checkNotInsideIfStatementAndReport(context, handleFailingRule, failingRule, node);
  }
}

function reverseTargetMappings(targetMappings) {
  const reversedEntries = Object.entries(targetMappings).map(entry => entry.reverse());
  return Object.fromEntries(reversedEntries);
}
/**
 * Determine the targets based on the browserslist config object
 * Get the targets from the eslint config and merge them with targets in browserslist config
 * Eslint target config will be deprecated in 4.0.0
 *
 * @param configPath - The file or a directory path to look for the browserslist config file
 */


function determineTargetsFromConfig(configPath, config) {
  const browserslistOpts = {
    path: configPath
  };

  const eslintTargets = (() => {
    // Get targets from eslint settings
    if (Array.isArray(config) || typeof config === "string") {
      return (0, _browserslist.default)(config, browserslistOpts);
    }

    if (config && typeof config === "object") {
      return (0, _browserslist.default)([...(config.production || []), ...(config.development || [])], browserslistOpts);
    }

    return [];
  })();

  if (_browserslist.default.findConfig(configPath)) {
    // If targets are defined in ESLint and browerslist configs, merge the targets together
    if (eslintTargets.length) {
      const browserslistTargets = (0, _browserslist.default)(undefined, browserslistOpts);
      return Array.from(new Set(eslintTargets.concat(browserslistTargets)));
    }
  } else if (eslintTargets.length) {
    return eslintTargets;
  } // Get targets fron browserslist configs


  return (0, _browserslist.default)(undefined, browserslistOpts);
}
/**
 * Parses the versions that are given by browserslist. They're
 *
 * ```ts
 * parseBrowsersListVersion(['chrome 50'])
 *
 * {
 *   target: 'chrome',
 *   parsedVersion: 50,
 *   version: '50'
 * }
 * ```
 * @param targetslist - List of targest from browserslist api
 * @returns - The lowest version version of each target
 */


function parseBrowsersListVersion(targetslist) {
  return (// Sort the targets by target name and then version number in ascending order
    targetslist.map(e => {
      const [target, version] = e.split(" ");

      const parsedVersion = (() => {
        if (typeof version === "number") return version;
        if (version === "all") return 0;
        return version.includes("-") ? parseFloat(version.split("-")[0]) : parseFloat(version);
      })();

      return {
        target,
        version,
        parsedVersion
      };
    }) // Sort the targets by target name and then version number in descending order
    // ex. [a@3, b@3, a@1] => [a@3, a@1, b@3]
    .sort((a, b) => {
      if (b.target === a.target) {
        // If any version === 'all', return 0. The only version of op_mini is 'all'
        // Otherwise, compare the versions
        return typeof b.parsedVersion === "string" || typeof a.parsedVersion === "string" ? 0 : b.parsedVersion - a.parsedVersion;
      }

      return b.target > a.target ? 1 : -1;
    }) // First last target always has the latest version
    .filter((e, i, items) => // Check if the current target is the last of its kind.
    // If it is, then it's the most recent version.
    i + 1 === items.length || e.target !== items[i + 1].target)
  );
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9oZWxwZXJzLnRzIl0sIm5hbWVzIjpbImlzSW5zaWRlSWZTdGF0ZW1lbnQiLCJjb250ZXh0IiwiZ2V0QW5jZXN0b3JzIiwic29tZSIsImFuY2VzdG9yIiwidHlwZSIsImNoZWNrTm90SW5zaWRlSWZTdGF0ZW1lbnRBbmRSZXBvcnQiLCJoYW5kbGVGYWlsaW5nUnVsZSIsImZhaWxpbmdSdWxlIiwibm9kZSIsImxpbnRDYWxsRXhwcmVzc2lvbiIsInJ1bGVzIiwiY2FsbGVlIiwiY2FsbGVlTmFtZSIsIm5hbWUiLCJmaW5kIiwicnVsZSIsIm9iamVjdCIsImxpbnROZXdFeHByZXNzaW9uIiwibGludEV4cHJlc3Npb25TdGF0ZW1lbnQiLCJleHByZXNzaW9uIiwicHJvdG9DaGFpbkZyb21NZW1iZXJFeHByZXNzaW9uIiwicHJvdG9DaGFpbiIsInByb3BlcnR5IiwibGludE1lbWJlckV4cHJlc3Npb24iLCJyYXdQcm90b0NoYWluIiwiZmlyc3RPYmoiLCJzbGljZSIsInByb3RvQ2hhaW5JZCIsImpvaW4iLCJvYmplY3ROYW1lIiwicHJvcGVydHlOYW1lIiwicmV2ZXJzZVRhcmdldE1hcHBpbmdzIiwidGFyZ2V0TWFwcGluZ3MiLCJyZXZlcnNlZEVudHJpZXMiLCJPYmplY3QiLCJlbnRyaWVzIiwibWFwIiwiZW50cnkiLCJyZXZlcnNlIiwiZnJvbUVudHJpZXMiLCJkZXRlcm1pbmVUYXJnZXRzRnJvbUNvbmZpZyIsImNvbmZpZ1BhdGgiLCJjb25maWciLCJicm93c2Vyc2xpc3RPcHRzIiwicGF0aCIsImVzbGludFRhcmdldHMiLCJBcnJheSIsImlzQXJyYXkiLCJwcm9kdWN0aW9uIiwiZGV2ZWxvcG1lbnQiLCJicm93c2Vyc2xpc3QiLCJmaW5kQ29uZmlnIiwibGVuZ3RoIiwiYnJvd3NlcnNsaXN0VGFyZ2V0cyIsInVuZGVmaW5lZCIsImZyb20iLCJTZXQiLCJjb25jYXQiLCJwYXJzZUJyb3dzZXJzTGlzdFZlcnNpb24iLCJ0YXJnZXRzbGlzdCIsImUiLCJ0YXJnZXQiLCJ2ZXJzaW9uIiwic3BsaXQiLCJwYXJzZWRWZXJzaW9uIiwiaW5jbHVkZXMiLCJwYXJzZUZsb2F0Iiwic29ydCIsImEiLCJiIiwiZmlsdGVyIiwiaSIsIml0ZW1zIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFDQTs7OztBQURBOztBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVNBLG1CQUFULENBQTZCQyxPQUE3QixFQUErQztBQUM3QyxTQUFPQSxPQUFPLENBQUNDLFlBQVIsR0FBdUJDLElBQXZCLENBQTZCQyxRQUFELElBQWM7QUFDL0MsV0FBT0EsUUFBUSxDQUFDQyxJQUFULEtBQWtCLGFBQXpCO0FBQ0QsR0FGTSxDQUFQO0FBR0Q7O0FBRUQsU0FBU0Msa0NBQVQsQ0FDRUwsT0FERixFQUVFTSxpQkFGRixFQUdFQyxXQUhGLEVBSUVDLElBSkYsRUFLRTtBQUNBLE1BQUksQ0FBQ1QsbUJBQW1CLENBQUNDLE9BQUQsQ0FBeEIsRUFBbUM7QUFDakNNLElBQUFBLGlCQUFpQixDQUFDQyxXQUFELEVBQWNDLElBQWQsQ0FBakI7QUFDRDtBQUNGOztBQUVNLFNBQVNDLGtCQUFULENBQ0xULE9BREssRUFFTE0saUJBRkssRUFHTEksS0FISyxFQUlMRixJQUpLLEVBS0w7QUFDQSxNQUFJLENBQUNBLElBQUksQ0FBQ0csTUFBVixFQUFrQjtBQUNsQixRQUFNQyxVQUFVLEdBQUdKLElBQUksQ0FBQ0csTUFBTCxDQUFZRSxJQUEvQjtBQUNBLFFBQU1OLFdBQVcsR0FBR0csS0FBSyxDQUFDSSxJQUFOLENBQVlDLElBQUQsSUFBVUEsSUFBSSxDQUFDQyxNQUFMLEtBQWdCSixVQUFyQyxDQUFwQjtBQUNBLE1BQUlMLFdBQUosRUFDRUYsa0NBQWtDLENBQ2hDTCxPQURnQyxFQUVoQ00saUJBRmdDLEVBR2hDQyxXQUhnQyxFQUloQ0MsSUFKZ0MsQ0FBbEM7QUFNSDs7QUFFTSxTQUFTUyxpQkFBVCxDQUNMakIsT0FESyxFQUVMTSxpQkFGSyxFQUdMSSxLQUhLLEVBSUxGLElBSkssRUFLTDtBQUNBLE1BQUksQ0FBQ0EsSUFBSSxDQUFDRyxNQUFWLEVBQWtCO0FBQ2xCLFFBQU1DLFVBQVUsR0FBR0osSUFBSSxDQUFDRyxNQUFMLENBQVlFLElBQS9CO0FBQ0EsUUFBTU4sV0FBVyxHQUFHRyxLQUFLLENBQUNJLElBQU4sQ0FBWUMsSUFBRCxJQUFVQSxJQUFJLENBQUNDLE1BQUwsS0FBZ0JKLFVBQXJDLENBQXBCO0FBQ0EsTUFBSUwsV0FBSixFQUNFRixrQ0FBa0MsQ0FDaENMLE9BRGdDLEVBRWhDTSxpQkFGZ0MsRUFHaENDLFdBSGdDLEVBSWhDQyxJQUpnQyxDQUFsQztBQU1IOztBQUVNLFNBQVNVLHVCQUFULENBQ0xsQixPQURLLEVBRUxNLGlCQUZLLEVBR0xJLEtBSEssRUFJTEYsSUFKSyxFQUtMO0FBQUE7O0FBQ0EsTUFBSSxFQUFDQSxJQUFELGFBQUNBLElBQUQsbUNBQUNBLElBQUksQ0FBRVcsVUFBUCw2Q0FBQyxpQkFBa0JOLElBQW5CLENBQUosRUFBNkI7QUFDN0IsUUFBTU4sV0FBVyxHQUFHRyxLQUFLLENBQUNJLElBQU4sQ0FDakJDLElBQUQ7QUFBQTs7QUFBQSxXQUFVQSxJQUFJLENBQUNDLE1BQUwsTUFBZ0JSLElBQWhCLGFBQWdCQSxJQUFoQiw0Q0FBZ0JBLElBQUksQ0FBRVcsVUFBdEIsc0RBQWdCLGtCQUFrQk4sSUFBbEMsQ0FBVjtBQUFBLEdBRGtCLENBQXBCO0FBR0EsTUFBSU4sV0FBSixFQUNFRixrQ0FBa0MsQ0FDaENMLE9BRGdDLEVBRWhDTSxpQkFGZ0MsRUFHaENDLFdBSGdDLEVBSWhDQyxJQUpnQyxDQUFsQztBQU1IOztBQUVELFNBQVNZLDhCQUFULENBQXdDWixJQUF4QyxFQUFvRTtBQUNsRSxNQUFJLENBQUNBLElBQUksQ0FBQ1EsTUFBVixFQUFrQixPQUFPLENBQUNSLElBQUksQ0FBQ0ssSUFBTixDQUFQOztBQUNsQixRQUFNUSxVQUFVLEdBQUcsQ0FBQyxNQUFNO0FBQ3hCLFlBQVFiLElBQUksQ0FBQ1EsTUFBTCxDQUFZWixJQUFwQjtBQUNFLFdBQUssZUFBTDtBQUNBLFdBQUssZ0JBQUw7QUFDRSxlQUFPZ0IsOEJBQThCLENBQUNaLElBQUksQ0FBQ1EsTUFBTCxDQUFZTCxNQUFiLENBQXJDOztBQUNGO0FBQ0UsZUFBT1MsOEJBQThCLENBQUNaLElBQUksQ0FBQ1EsTUFBTixDQUFyQztBQUxKO0FBT0QsR0FSa0IsR0FBbkI7O0FBU0EsU0FBTyxDQUFDLEdBQUdLLFVBQUosRUFBZ0JiLElBQUksQ0FBQ2MsUUFBTCxDQUFjVCxJQUE5QixDQUFQO0FBQ0Q7O0FBRU0sU0FBU1Usb0JBQVQsQ0FDTHZCLE9BREssRUFFTE0saUJBRkssRUFHTEksS0FISyxFQUlMRixJQUpLLEVBS0w7QUFDQSxNQUFJLENBQUNBLElBQUksQ0FBQ1EsTUFBTixJQUFnQixDQUFDUixJQUFJLENBQUNjLFFBQTFCLEVBQW9DOztBQUNwQyxNQUNFLENBQUNkLElBQUksQ0FBQ1EsTUFBTCxDQUFZSCxJQUFiLElBQ0FMLElBQUksQ0FBQ1EsTUFBTCxDQUFZSCxJQUFaLEtBQXFCLFFBRHJCLElBRUFMLElBQUksQ0FBQ1EsTUFBTCxDQUFZSCxJQUFaLEtBQXFCLFlBSHZCLEVBSUU7QUFDQSxVQUFNVyxhQUFhLEdBQUdKLDhCQUE4QixDQUFDWixJQUFELENBQXBEO0FBQ0EsVUFBTSxDQUFDaUIsUUFBRCxJQUFhRCxhQUFuQjtBQUNBLFVBQU1ILFVBQVUsR0FDZEksUUFBUSxLQUFLLFFBQWIsSUFBeUJBLFFBQVEsS0FBSyxZQUF0QyxHQUNJRCxhQUFhLENBQUNFLEtBQWQsQ0FBb0IsQ0FBcEIsQ0FESixHQUVJRixhQUhOO0FBSUEsVUFBTUcsWUFBWSxHQUFHTixVQUFVLENBQUNPLElBQVgsQ0FBZ0IsR0FBaEIsQ0FBckI7QUFDQSxVQUFNckIsV0FBVyxHQUFHRyxLQUFLLENBQUNJLElBQU4sQ0FDakJDLElBQUQsSUFBVUEsSUFBSSxDQUFDWSxZQUFMLEtBQXNCQSxZQURkLENBQXBCOztBQUdBLFFBQUlwQixXQUFKLEVBQWlCO0FBQ2ZGLE1BQUFBLGtDQUFrQyxDQUNoQ0wsT0FEZ0MsRUFFaENNLGlCQUZnQyxFQUdoQ0MsV0FIZ0MsRUFJaENDLElBSmdDLENBQWxDO0FBTUQ7QUFDRixHQXZCRCxNQXVCTztBQUNMLFVBQU1xQixVQUFVLEdBQUdyQixJQUFJLENBQUNRLE1BQUwsQ0FBWUgsSUFBL0I7QUFDQSxVQUFNaUIsWUFBWSxHQUFHdEIsSUFBSSxDQUFDYyxRQUFMLENBQWNULElBQW5DO0FBQ0EsVUFBTU4sV0FBVyxHQUFHRyxLQUFLLENBQUNJLElBQU4sQ0FDakJDLElBQUQsSUFDRUEsSUFBSSxDQUFDQyxNQUFMLEtBQWdCYSxVQUFoQixLQUNDZCxJQUFJLENBQUNPLFFBQUwsSUFBaUIsSUFBakIsSUFBeUJQLElBQUksQ0FBQ08sUUFBTCxLQUFrQlEsWUFENUMsQ0FGZ0IsQ0FBcEI7QUFLQSxRQUFJdkIsV0FBSixFQUNFRixrQ0FBa0MsQ0FDaENMLE9BRGdDLEVBRWhDTSxpQkFGZ0MsRUFHaENDLFdBSGdDLEVBSWhDQyxJQUpnQyxDQUFsQztBQU1IO0FBQ0Y7O0FBRU0sU0FBU3VCLHFCQUFULENBQStCQyxjQUEvQixFQUF1RTtBQUM1RSxRQUFNQyxlQUFlLEdBQUdDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlSCxjQUFmLEVBQStCSSxHQUEvQixDQUFvQ0MsS0FBRCxJQUN6REEsS0FBSyxDQUFDQyxPQUFOLEVBRHNCLENBQXhCO0FBR0EsU0FBT0osTUFBTSxDQUFDSyxXQUFQLENBQW1CTixlQUFuQixDQUFQO0FBQ0Q7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ08sU0FBU08sMEJBQVQsQ0FDTEMsVUFESyxFQUVMQyxNQUZLLEVBR1U7QUFDZixRQUFNQyxnQkFBZ0IsR0FBRztBQUFFQyxJQUFBQSxJQUFJLEVBQUVIO0FBQVIsR0FBekI7O0FBRUEsUUFBTUksYUFBYSxHQUFHLENBQUMsTUFBTTtBQUMzQjtBQUNBLFFBQUlDLEtBQUssQ0FBQ0MsT0FBTixDQUFjTCxNQUFkLEtBQXlCLE9BQU9BLE1BQVAsS0FBa0IsUUFBL0MsRUFBeUQ7QUFDdkQsYUFBTywyQkFBYUEsTUFBYixFQUFxQkMsZ0JBQXJCLENBQVA7QUFDRDs7QUFDRCxRQUFJRCxNQUFNLElBQUksT0FBT0EsTUFBUCxLQUFrQixRQUFoQyxFQUEwQztBQUN4QyxhQUFPLDJCQUNMLENBQUMsSUFBSUEsTUFBTSxDQUFDTSxVQUFQLElBQXFCLEVBQXpCLENBQUQsRUFBK0IsSUFBSU4sTUFBTSxDQUFDTyxXQUFQLElBQXNCLEVBQTFCLENBQS9CLENBREssRUFFTE4sZ0JBRkssQ0FBUDtBQUlEOztBQUNELFdBQU8sRUFBUDtBQUNELEdBWnFCLEdBQXRCOztBQWNBLE1BQUlPLHNCQUFhQyxVQUFiLENBQXdCVixVQUF4QixDQUFKLEVBQXlDO0FBQ3ZDO0FBQ0EsUUFBSUksYUFBYSxDQUFDTyxNQUFsQixFQUEwQjtBQUN4QixZQUFNQyxtQkFBbUIsR0FBRywyQkFBYUMsU0FBYixFQUF3QlgsZ0JBQXhCLENBQTVCO0FBQ0EsYUFBT0csS0FBSyxDQUFDUyxJQUFOLENBQVcsSUFBSUMsR0FBSixDQUFRWCxhQUFhLENBQUNZLE1BQWQsQ0FBcUJKLG1CQUFyQixDQUFSLENBQVgsQ0FBUDtBQUNEO0FBQ0YsR0FORCxNQU1PLElBQUlSLGFBQWEsQ0FBQ08sTUFBbEIsRUFBMEI7QUFDL0IsV0FBT1AsYUFBUDtBQUNELEdBekJjLENBMkJmOzs7QUFDQSxTQUFPLDJCQUFhUyxTQUFiLEVBQXdCWCxnQkFBeEIsQ0FBUDtBQUNEO0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDTyxTQUFTZSx3QkFBVCxDQUNMQyxXQURLLEVBRVU7QUFDZixTQUNFO0FBQ0FBLElBQUFBLFdBQVcsQ0FDUnZCLEdBREgsQ0FFS3dCLENBQUQsSUFBdUI7QUFDckIsWUFBTSxDQUFDQyxNQUFELEVBQVNDLE9BQVQsSUFBb0JGLENBQUMsQ0FBQ0csS0FBRixDQUFRLEdBQVIsQ0FBMUI7O0FBS0EsWUFBTUMsYUFBcUIsR0FBRyxDQUFDLE1BQU07QUFDbkMsWUFBSSxPQUFPRixPQUFQLEtBQW1CLFFBQXZCLEVBQWlDLE9BQU9BLE9BQVA7QUFDakMsWUFBSUEsT0FBTyxLQUFLLEtBQWhCLEVBQXVCLE9BQU8sQ0FBUDtBQUN2QixlQUFPQSxPQUFPLENBQUNHLFFBQVIsQ0FBaUIsR0FBakIsSUFDSEMsVUFBVSxDQUFDSixPQUFPLENBQUNDLEtBQVIsQ0FBYyxHQUFkLEVBQW1CLENBQW5CLENBQUQsQ0FEUCxHQUVIRyxVQUFVLENBQUNKLE9BQUQsQ0FGZDtBQUdELE9BTjZCLEdBQTlCOztBQVFBLGFBQU87QUFDTEQsUUFBQUEsTUFESztBQUVMQyxRQUFBQSxPQUZLO0FBR0xFLFFBQUFBO0FBSEssT0FBUDtBQUtELEtBckJMLEVBc0JJO0FBQ0Y7QUF2QkYsS0F3QkdHLElBeEJILENBd0JRLENBQUNDLENBQUQsRUFBWUMsQ0FBWixLQUFrQztBQUN0QyxVQUFJQSxDQUFDLENBQUNSLE1BQUYsS0FBYU8sQ0FBQyxDQUFDUCxNQUFuQixFQUEyQjtBQUN6QjtBQUNBO0FBQ0EsZUFBTyxPQUFPUSxDQUFDLENBQUNMLGFBQVQsS0FBMkIsUUFBM0IsSUFDTCxPQUFPSSxDQUFDLENBQUNKLGFBQVQsS0FBMkIsUUFEdEIsR0FFSCxDQUZHLEdBR0hLLENBQUMsQ0FBQ0wsYUFBRixHQUFrQkksQ0FBQyxDQUFDSixhQUh4QjtBQUlEOztBQUNELGFBQU9LLENBQUMsQ0FBQ1IsTUFBRixHQUFXTyxDQUFDLENBQUNQLE1BQWIsR0FBc0IsQ0FBdEIsR0FBMEIsQ0FBQyxDQUFsQztBQUNELEtBbENILEVBa0NLO0FBbENMLEtBbUNHUyxNQW5DSCxDQW9DSSxDQUFDVixDQUFELEVBQVlXLENBQVosRUFBdUJDLEtBQXZCLEtBQ0U7QUFDQTtBQUNBRCxJQUFBQSxDQUFDLEdBQUcsQ0FBSixLQUFVQyxLQUFLLENBQUNwQixNQUFoQixJQUEwQlEsQ0FBQyxDQUFDQyxNQUFGLEtBQWFXLEtBQUssQ0FBQ0QsQ0FBQyxHQUFHLENBQUwsQ0FBTCxDQUFhVixNQXZDMUQ7QUFGRjtBQTRDRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludCBuby1uZXN0ZWQtdGVybmFyeTogb2ZmICovXG5pbXBvcnQgYnJvd3NlcnNsaXN0IGZyb20gXCJicm93c2Vyc2xpc3RcIjtcbmltcG9ydCB7XG4gIEFzdE1ldGFkYXRhQXBpV2l0aFRhcmdldHNSZXNvbHZlcixcbiAgRVNMaW50Tm9kZSxcbiAgQnJvd3Nlckxpc3RDb25maWcsXG4gIFRhcmdldCxcbiAgSGFuZGxlRmFpbGluZ1J1bGUsXG4gIENvbnRleHQsXG59IGZyb20gXCIuL3R5cGVzXCI7XG5pbXBvcnQgeyBUYXJnZXROYW1lTWFwcGluZ3MgfSBmcm9tIFwiLi9jb25zdGFudHNcIjtcblxuLypcbjMpIEZpZ3VyZXMgb3V0IHdoaWNoIGJyb3dzZXJzIHVzZXIgaXMgdGFyZ2V0aW5nXG5cbi0gVXNlcyBicm93c2Vyc2xpc3QgY29uZmlnIGFuZC9vciB0YXJnZXRzIGRlZmluZWQgZXNsaW50IGNvbmZpZyB0byBkaXNjb3ZlciB0aGlzXG4tIEZvciBldmVyeSBBUEkgZWNub3VudGVyZWQgZHVyaW5nIHRyYXZlcnNhbCwgZ2V0cyBjb21wYXQgcmVjb3JkIGZvciB0aGF0XG4tIFByb3RvY2hhaW4gKGUuZy4gJ2RvY3VtZW50LnF1ZXJ5U2VsZWN0b3InKVxuICAtIEFsbCBvZiB0aGUgcnVsZXMgaGF2ZSBjb21wYXRpYmlsaXR5IGluZm8gYXR0YWNoZWQgdG8gdGhlbVxuLSBFYWNoIEFQSSBpcyBnaXZlbiB0byB2ZXJzaW9uaW5nLnRzIHdpdGggY29tcGF0aWJpbGl0eSBpbmZvXG4qL1xuZnVuY3Rpb24gaXNJbnNpZGVJZlN0YXRlbWVudChjb250ZXh0OiBDb250ZXh0KSB7XG4gIHJldHVybiBjb250ZXh0LmdldEFuY2VzdG9ycygpLnNvbWUoKGFuY2VzdG9yKSA9PiB7XG4gICAgcmV0dXJuIGFuY2VzdG9yLnR5cGUgPT09IFwiSWZTdGF0ZW1lbnRcIjtcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGNoZWNrTm90SW5zaWRlSWZTdGF0ZW1lbnRBbmRSZXBvcnQoXG4gIGNvbnRleHQ6IENvbnRleHQsXG4gIGhhbmRsZUZhaWxpbmdSdWxlOiBIYW5kbGVGYWlsaW5nUnVsZSxcbiAgZmFpbGluZ1J1bGU6IEFzdE1ldGFkYXRhQXBpV2l0aFRhcmdldHNSZXNvbHZlcixcbiAgbm9kZTogRVNMaW50Tm9kZVxuKSB7XG4gIGlmICghaXNJbnNpZGVJZlN0YXRlbWVudChjb250ZXh0KSkge1xuICAgIGhhbmRsZUZhaWxpbmdSdWxlKGZhaWxpbmdSdWxlLCBub2RlKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gbGludENhbGxFeHByZXNzaW9uKFxuICBjb250ZXh0OiBDb250ZXh0LFxuICBoYW5kbGVGYWlsaW5nUnVsZTogSGFuZGxlRmFpbGluZ1J1bGUsXG4gIHJ1bGVzOiBBc3RNZXRhZGF0YUFwaVdpdGhUYXJnZXRzUmVzb2x2ZXJbXSxcbiAgbm9kZTogRVNMaW50Tm9kZVxuKSB7XG4gIGlmICghbm9kZS5jYWxsZWUpIHJldHVybjtcbiAgY29uc3QgY2FsbGVlTmFtZSA9IG5vZGUuY2FsbGVlLm5hbWU7XG4gIGNvbnN0IGZhaWxpbmdSdWxlID0gcnVsZXMuZmluZCgocnVsZSkgPT4gcnVsZS5vYmplY3QgPT09IGNhbGxlZU5hbWUpO1xuICBpZiAoZmFpbGluZ1J1bGUpXG4gICAgY2hlY2tOb3RJbnNpZGVJZlN0YXRlbWVudEFuZFJlcG9ydChcbiAgICAgIGNvbnRleHQsXG4gICAgICBoYW5kbGVGYWlsaW5nUnVsZSxcbiAgICAgIGZhaWxpbmdSdWxlLFxuICAgICAgbm9kZVxuICAgICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsaW50TmV3RXhwcmVzc2lvbihcbiAgY29udGV4dDogQ29udGV4dCxcbiAgaGFuZGxlRmFpbGluZ1J1bGU6IEhhbmRsZUZhaWxpbmdSdWxlLFxuICBydWxlczogQXJyYXk8QXN0TWV0YWRhdGFBcGlXaXRoVGFyZ2V0c1Jlc29sdmVyPixcbiAgbm9kZTogRVNMaW50Tm9kZVxuKSB7XG4gIGlmICghbm9kZS5jYWxsZWUpIHJldHVybjtcbiAgY29uc3QgY2FsbGVlTmFtZSA9IG5vZGUuY2FsbGVlLm5hbWU7XG4gIGNvbnN0IGZhaWxpbmdSdWxlID0gcnVsZXMuZmluZCgocnVsZSkgPT4gcnVsZS5vYmplY3QgPT09IGNhbGxlZU5hbWUpO1xuICBpZiAoZmFpbGluZ1J1bGUpXG4gICAgY2hlY2tOb3RJbnNpZGVJZlN0YXRlbWVudEFuZFJlcG9ydChcbiAgICAgIGNvbnRleHQsXG4gICAgICBoYW5kbGVGYWlsaW5nUnVsZSxcbiAgICAgIGZhaWxpbmdSdWxlLFxuICAgICAgbm9kZVxuICAgICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBsaW50RXhwcmVzc2lvblN0YXRlbWVudChcbiAgY29udGV4dDogQ29udGV4dCxcbiAgaGFuZGxlRmFpbGluZ1J1bGU6IEhhbmRsZUZhaWxpbmdSdWxlLFxuICBydWxlczogQXN0TWV0YWRhdGFBcGlXaXRoVGFyZ2V0c1Jlc29sdmVyW10sXG4gIG5vZGU6IEVTTGludE5vZGVcbikge1xuICBpZiAoIW5vZGU/LmV4cHJlc3Npb24/Lm5hbWUpIHJldHVybjtcbiAgY29uc3QgZmFpbGluZ1J1bGUgPSBydWxlcy5maW5kKFxuICAgIChydWxlKSA9PiBydWxlLm9iamVjdCA9PT0gbm9kZT8uZXhwcmVzc2lvbj8ubmFtZVxuICApO1xuICBpZiAoZmFpbGluZ1J1bGUpXG4gICAgY2hlY2tOb3RJbnNpZGVJZlN0YXRlbWVudEFuZFJlcG9ydChcbiAgICAgIGNvbnRleHQsXG4gICAgICBoYW5kbGVGYWlsaW5nUnVsZSxcbiAgICAgIGZhaWxpbmdSdWxlLFxuICAgICAgbm9kZVxuICAgICk7XG59XG5cbmZ1bmN0aW9uIHByb3RvQ2hhaW5Gcm9tTWVtYmVyRXhwcmVzc2lvbihub2RlOiBFU0xpbnROb2RlKTogc3RyaW5nW10ge1xuICBpZiAoIW5vZGUub2JqZWN0KSByZXR1cm4gW25vZGUubmFtZV07XG4gIGNvbnN0IHByb3RvQ2hhaW4gPSAoKCkgPT4ge1xuICAgIHN3aXRjaCAobm9kZS5vYmplY3QudHlwZSkge1xuICAgICAgY2FzZSBcIk5ld0V4cHJlc3Npb25cIjpcbiAgICAgIGNhc2UgXCJDYWxsRXhwcmVzc2lvblwiOlxuICAgICAgICByZXR1cm4gcHJvdG9DaGFpbkZyb21NZW1iZXJFeHByZXNzaW9uKG5vZGUub2JqZWN0LmNhbGxlZSk7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gcHJvdG9DaGFpbkZyb21NZW1iZXJFeHByZXNzaW9uKG5vZGUub2JqZWN0KTtcbiAgICB9XG4gIH0pKCk7XG4gIHJldHVybiBbLi4ucHJvdG9DaGFpbiwgbm9kZS5wcm9wZXJ0eS5uYW1lXTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxpbnRNZW1iZXJFeHByZXNzaW9uKFxuICBjb250ZXh0OiBDb250ZXh0LFxuICBoYW5kbGVGYWlsaW5nUnVsZTogSGFuZGxlRmFpbGluZ1J1bGUsXG4gIHJ1bGVzOiBBcnJheTxBc3RNZXRhZGF0YUFwaVdpdGhUYXJnZXRzUmVzb2x2ZXI+LFxuICBub2RlOiBFU0xpbnROb2RlXG4pIHtcbiAgaWYgKCFub2RlLm9iamVjdCB8fCAhbm9kZS5wcm9wZXJ0eSkgcmV0dXJuO1xuICBpZiAoXG4gICAgIW5vZGUub2JqZWN0Lm5hbWUgfHxcbiAgICBub2RlLm9iamVjdC5uYW1lID09PSBcIndpbmRvd1wiIHx8XG4gICAgbm9kZS5vYmplY3QubmFtZSA9PT0gXCJnbG9iYWxUaGlzXCJcbiAgKSB7XG4gICAgY29uc3QgcmF3UHJvdG9DaGFpbiA9IHByb3RvQ2hhaW5Gcm9tTWVtYmVyRXhwcmVzc2lvbihub2RlKTtcbiAgICBjb25zdCBbZmlyc3RPYmpdID0gcmF3UHJvdG9DaGFpbjtcbiAgICBjb25zdCBwcm90b0NoYWluID1cbiAgICAgIGZpcnN0T2JqID09PSBcIndpbmRvd1wiIHx8IGZpcnN0T2JqID09PSBcImdsb2JhbFRoaXNcIlxuICAgICAgICA/IHJhd1Byb3RvQ2hhaW4uc2xpY2UoMSlcbiAgICAgICAgOiByYXdQcm90b0NoYWluO1xuICAgIGNvbnN0IHByb3RvQ2hhaW5JZCA9IHByb3RvQ2hhaW4uam9pbihcIi5cIik7XG4gICAgY29uc3QgZmFpbGluZ1J1bGUgPSBydWxlcy5maW5kKFxuICAgICAgKHJ1bGUpID0+IHJ1bGUucHJvdG9DaGFpbklkID09PSBwcm90b0NoYWluSWRcbiAgICApO1xuICAgIGlmIChmYWlsaW5nUnVsZSkge1xuICAgICAgY2hlY2tOb3RJbnNpZGVJZlN0YXRlbWVudEFuZFJlcG9ydChcbiAgICAgICAgY29udGV4dCxcbiAgICAgICAgaGFuZGxlRmFpbGluZ1J1bGUsXG4gICAgICAgIGZhaWxpbmdSdWxlLFxuICAgICAgICBub2RlXG4gICAgICApO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBjb25zdCBvYmplY3ROYW1lID0gbm9kZS5vYmplY3QubmFtZTtcbiAgICBjb25zdCBwcm9wZXJ0eU5hbWUgPSBub2RlLnByb3BlcnR5Lm5hbWU7XG4gICAgY29uc3QgZmFpbGluZ1J1bGUgPSBydWxlcy5maW5kKFxuICAgICAgKHJ1bGUpID0+XG4gICAgICAgIHJ1bGUub2JqZWN0ID09PSBvYmplY3ROYW1lICYmXG4gICAgICAgIChydWxlLnByb3BlcnR5ID09IG51bGwgfHwgcnVsZS5wcm9wZXJ0eSA9PT0gcHJvcGVydHlOYW1lKVxuICAgICk7XG4gICAgaWYgKGZhaWxpbmdSdWxlKVxuICAgICAgY2hlY2tOb3RJbnNpZGVJZlN0YXRlbWVudEFuZFJlcG9ydChcbiAgICAgICAgY29udGV4dCxcbiAgICAgICAgaGFuZGxlRmFpbGluZ1J1bGUsXG4gICAgICAgIGZhaWxpbmdSdWxlLFxuICAgICAgICBub2RlXG4gICAgICApO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiByZXZlcnNlVGFyZ2V0TWFwcGluZ3ModGFyZ2V0TWFwcGluZ3M6IFJlY29yZDxzdHJpbmcsIHN0cmluZz4pIHtcbiAgY29uc3QgcmV2ZXJzZWRFbnRyaWVzID0gT2JqZWN0LmVudHJpZXModGFyZ2V0TWFwcGluZ3MpLm1hcCgoZW50cnkpID0+XG4gICAgZW50cnkucmV2ZXJzZSgpXG4gICk7XG4gIHJldHVybiBPYmplY3QuZnJvbUVudHJpZXMocmV2ZXJzZWRFbnRyaWVzKTtcbn1cblxuLyoqXG4gKiBEZXRlcm1pbmUgdGhlIHRhcmdldHMgYmFzZWQgb24gdGhlIGJyb3dzZXJzbGlzdCBjb25maWcgb2JqZWN0XG4gKiBHZXQgdGhlIHRhcmdldHMgZnJvbSB0aGUgZXNsaW50IGNvbmZpZyBhbmQgbWVyZ2UgdGhlbSB3aXRoIHRhcmdldHMgaW4gYnJvd3NlcnNsaXN0IGNvbmZpZ1xuICogRXNsaW50IHRhcmdldCBjb25maWcgd2lsbCBiZSBkZXByZWNhdGVkIGluIDQuMC4wXG4gKlxuICogQHBhcmFtIGNvbmZpZ1BhdGggLSBUaGUgZmlsZSBvciBhIGRpcmVjdG9yeSBwYXRoIHRvIGxvb2sgZm9yIHRoZSBicm93c2Vyc2xpc3QgY29uZmlnIGZpbGVcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGRldGVybWluZVRhcmdldHNGcm9tQ29uZmlnKFxuICBjb25maWdQYXRoOiBzdHJpbmcsXG4gIGNvbmZpZz86IEJyb3dzZXJMaXN0Q29uZmlnXG4pOiBBcnJheTxzdHJpbmc+IHtcbiAgY29uc3QgYnJvd3NlcnNsaXN0T3B0cyA9IHsgcGF0aDogY29uZmlnUGF0aCB9O1xuXG4gIGNvbnN0IGVzbGludFRhcmdldHMgPSAoKCkgPT4ge1xuICAgIC8vIEdldCB0YXJnZXRzIGZyb20gZXNsaW50IHNldHRpbmdzXG4gICAgaWYgKEFycmF5LmlzQXJyYXkoY29uZmlnKSB8fCB0eXBlb2YgY29uZmlnID09PSBcInN0cmluZ1wiKSB7XG4gICAgICByZXR1cm4gYnJvd3NlcnNsaXN0KGNvbmZpZywgYnJvd3NlcnNsaXN0T3B0cyk7XG4gICAgfVxuICAgIGlmIChjb25maWcgJiYgdHlwZW9mIGNvbmZpZyA9PT0gXCJvYmplY3RcIikge1xuICAgICAgcmV0dXJuIGJyb3dzZXJzbGlzdChcbiAgICAgICAgWy4uLihjb25maWcucHJvZHVjdGlvbiB8fCBbXSksIC4uLihjb25maWcuZGV2ZWxvcG1lbnQgfHwgW10pXSxcbiAgICAgICAgYnJvd3NlcnNsaXN0T3B0c1xuICAgICAgKTtcbiAgICB9XG4gICAgcmV0dXJuIFtdO1xuICB9KSgpO1xuXG4gIGlmIChicm93c2Vyc2xpc3QuZmluZENvbmZpZyhjb25maWdQYXRoKSkge1xuICAgIC8vIElmIHRhcmdldHMgYXJlIGRlZmluZWQgaW4gRVNMaW50IGFuZCBicm93ZXJzbGlzdCBjb25maWdzLCBtZXJnZSB0aGUgdGFyZ2V0cyB0b2dldGhlclxuICAgIGlmIChlc2xpbnRUYXJnZXRzLmxlbmd0aCkge1xuICAgICAgY29uc3QgYnJvd3NlcnNsaXN0VGFyZ2V0cyA9IGJyb3dzZXJzbGlzdCh1bmRlZmluZWQsIGJyb3dzZXJzbGlzdE9wdHMpO1xuICAgICAgcmV0dXJuIEFycmF5LmZyb20obmV3IFNldChlc2xpbnRUYXJnZXRzLmNvbmNhdChicm93c2Vyc2xpc3RUYXJnZXRzKSkpO1xuICAgIH1cbiAgfSBlbHNlIGlmIChlc2xpbnRUYXJnZXRzLmxlbmd0aCkge1xuICAgIHJldHVybiBlc2xpbnRUYXJnZXRzO1xuICB9XG5cbiAgLy8gR2V0IHRhcmdldHMgZnJvbiBicm93c2Vyc2xpc3QgY29uZmlnc1xuICByZXR1cm4gYnJvd3NlcnNsaXN0KHVuZGVmaW5lZCwgYnJvd3NlcnNsaXN0T3B0cyk7XG59XG5cbi8qKlxuICogUGFyc2VzIHRoZSB2ZXJzaW9ucyB0aGF0IGFyZSBnaXZlbiBieSBicm93c2Vyc2xpc3QuIFRoZXkncmVcbiAqXG4gKiBgYGB0c1xuICogcGFyc2VCcm93c2Vyc0xpc3RWZXJzaW9uKFsnY2hyb21lIDUwJ10pXG4gKlxuICoge1xuICogICB0YXJnZXQ6ICdjaHJvbWUnLFxuICogICBwYXJzZWRWZXJzaW9uOiA1MCxcbiAqICAgdmVyc2lvbjogJzUwJ1xuICogfVxuICogYGBgXG4gKiBAcGFyYW0gdGFyZ2V0c2xpc3QgLSBMaXN0IG9mIHRhcmdlc3QgZnJvbSBicm93c2Vyc2xpc3QgYXBpXG4gKiBAcmV0dXJucyAtIFRoZSBsb3dlc3QgdmVyc2lvbiB2ZXJzaW9uIG9mIGVhY2ggdGFyZ2V0XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBwYXJzZUJyb3dzZXJzTGlzdFZlcnNpb24oXG4gIHRhcmdldHNsaXN0OiBBcnJheTxzdHJpbmc+XG4pOiBBcnJheTxUYXJnZXQ+IHtcbiAgcmV0dXJuIChcbiAgICAvLyBTb3J0IHRoZSB0YXJnZXRzIGJ5IHRhcmdldCBuYW1lIGFuZCB0aGVuIHZlcnNpb24gbnVtYmVyIGluIGFzY2VuZGluZyBvcmRlclxuICAgIHRhcmdldHNsaXN0XG4gICAgICAubWFwKFxuICAgICAgICAoZTogc3RyaW5nKTogVGFyZ2V0ID0+IHtcbiAgICAgICAgICBjb25zdCBbdGFyZ2V0LCB2ZXJzaW9uXSA9IGUuc3BsaXQoXCIgXCIpIGFzIFtcbiAgICAgICAgICAgIGtleW9mIFRhcmdldE5hbWVNYXBwaW5ncyxcbiAgICAgICAgICAgIG51bWJlciB8IHN0cmluZ1xuICAgICAgICAgIF07XG5cbiAgICAgICAgICBjb25zdCBwYXJzZWRWZXJzaW9uOiBudW1iZXIgPSAoKCkgPT4ge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiB2ZXJzaW9uID09PSBcIm51bWJlclwiKSByZXR1cm4gdmVyc2lvbjtcbiAgICAgICAgICAgIGlmICh2ZXJzaW9uID09PSBcImFsbFwiKSByZXR1cm4gMDtcbiAgICAgICAgICAgIHJldHVybiB2ZXJzaW9uLmluY2x1ZGVzKFwiLVwiKVxuICAgICAgICAgICAgICA/IHBhcnNlRmxvYXQodmVyc2lvbi5zcGxpdChcIi1cIilbMF0pXG4gICAgICAgICAgICAgIDogcGFyc2VGbG9hdCh2ZXJzaW9uKTtcbiAgICAgICAgICB9KSgpO1xuXG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHRhcmdldCxcbiAgICAgICAgICAgIHZlcnNpb24sXG4gICAgICAgICAgICBwYXJzZWRWZXJzaW9uLFxuICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICkgLy8gU29ydCB0aGUgdGFyZ2V0cyBieSB0YXJnZXQgbmFtZSBhbmQgdGhlbiB2ZXJzaW9uIG51bWJlciBpbiBkZXNjZW5kaW5nIG9yZGVyXG4gICAgICAvLyBleC4gW2FAMywgYkAzLCBhQDFdID0+IFthQDMsIGFAMSwgYkAzXVxuICAgICAgLnNvcnQoKGE6IFRhcmdldCwgYjogVGFyZ2V0KTogbnVtYmVyID0+IHtcbiAgICAgICAgaWYgKGIudGFyZ2V0ID09PSBhLnRhcmdldCkge1xuICAgICAgICAgIC8vIElmIGFueSB2ZXJzaW9uID09PSAnYWxsJywgcmV0dXJuIDAuIFRoZSBvbmx5IHZlcnNpb24gb2Ygb3BfbWluaSBpcyAnYWxsJ1xuICAgICAgICAgIC8vIE90aGVyd2lzZSwgY29tcGFyZSB0aGUgdmVyc2lvbnNcbiAgICAgICAgICByZXR1cm4gdHlwZW9mIGIucGFyc2VkVmVyc2lvbiA9PT0gXCJzdHJpbmdcIiB8fFxuICAgICAgICAgICAgdHlwZW9mIGEucGFyc2VkVmVyc2lvbiA9PT0gXCJzdHJpbmdcIlxuICAgICAgICAgICAgPyAwXG4gICAgICAgICAgICA6IGIucGFyc2VkVmVyc2lvbiAtIGEucGFyc2VkVmVyc2lvbjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gYi50YXJnZXQgPiBhLnRhcmdldCA/IDEgOiAtMTtcbiAgICAgIH0pIC8vIEZpcnN0IGxhc3QgdGFyZ2V0IGFsd2F5cyBoYXMgdGhlIGxhdGVzdCB2ZXJzaW9uXG4gICAgICAuZmlsdGVyKFxuICAgICAgICAoZTogVGFyZ2V0LCBpOiBudW1iZXIsIGl0ZW1zOiBBcnJheTxUYXJnZXQ+KTogYm9vbGVhbiA9PlxuICAgICAgICAgIC8vIENoZWNrIGlmIHRoZSBjdXJyZW50IHRhcmdldCBpcyB0aGUgbGFzdCBvZiBpdHMga2luZC5cbiAgICAgICAgICAvLyBJZiBpdCBpcywgdGhlbiBpdCdzIHRoZSBtb3N0IHJlY2VudCB2ZXJzaW9uLlxuICAgICAgICAgIGkgKyAxID09PSBpdGVtcy5sZW5ndGggfHwgZS50YXJnZXQgIT09IGl0ZW1zW2kgKyAxXS50YXJnZXRcbiAgICAgIClcbiAgKTtcbn1cbiJdfQ==