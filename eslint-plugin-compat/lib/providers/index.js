"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nodes = void 0;

var _caniuseProvider = _interopRequireDefault(require("./caniuse-provider"));

var _mdnProvider = _interopRequireDefault(require("./mdn-provider"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Step 3) Compat use CanIUse and MDN providers to check if a target browser supports a particular API
 */
// eslint-disable-next-line import/prefer-default-export
const nodes = [..._caniuseProvider.default, ..._mdnProvider.default];
exports.nodes = nodes;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9wcm92aWRlcnMvaW5kZXgudHMiXSwibmFtZXMiOlsibm9kZXMiLCJjYW5JVXNlTm9kZXMiLCJtZG5Ob2RlcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUdBOztBQUNBOzs7O0FBSkE7QUFDQTtBQUNBO0FBS0E7QUFDTyxNQUFNQSxLQUErQyxHQUFHLENBQzdELEdBQUdDLHdCQUQwRCxFQUU3RCxHQUFHQyxvQkFGMEQsQ0FBeEQiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogU3RlcCAzKSBDb21wYXQgdXNlIENhbklVc2UgYW5kIE1ETiBwcm92aWRlcnMgdG8gY2hlY2sgaWYgYSB0YXJnZXQgYnJvd3NlciBzdXBwb3J0cyBhIHBhcnRpY3VsYXIgQVBJXG4gKi9cbmltcG9ydCBjYW5JVXNlTm9kZXMgZnJvbSBcIi4vY2FuaXVzZS1wcm92aWRlclwiO1xuaW1wb3J0IG1kbk5vZGVzIGZyb20gXCIuL21kbi1wcm92aWRlclwiO1xuaW1wb3J0IHR5cGUgeyBBc3RNZXRhZGF0YUFwaVdpdGhUYXJnZXRzUmVzb2x2ZXIgfSBmcm9tIFwiLi4vdHlwZXNcIjtcblxuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGltcG9ydC9wcmVmZXItZGVmYXVsdC1leHBvcnRcbmV4cG9ydCBjb25zdCBub2RlczogQXJyYXk8QXN0TWV0YWRhdGFBcGlXaXRoVGFyZ2V0c1Jlc29sdmVyPiA9IFtcbiAgLi4uY2FuSVVzZU5vZGVzLFxuICAuLi5tZG5Ob2Rlcyxcbl07XG4iXX0=