"use strict";

var _object = _interopRequireDefault(require("object.assign"));

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _server = _interopRequireDefault(require("react-dom/server"));

var _shallow = _interopRequireDefault(require("react-test-renderer/shallow"));

var _testUtils = _interopRequireDefault(require("react-dom/test-utils"));

var _checkPropTypes2 = _interopRequireDefault(require("prop-types/checkPropTypes"));

var _has = _interopRequireDefault(require("has"));

var _reactIs = require("react-is");

var _enzyme = require("enzyme");

var _Utils = require("enzyme/build/Utils");

var _enzymeShallowEqual = _interopRequireDefault(require("enzyme-shallow-equal"));

var _enzymeAdapterUtils = require("@wojtekmaj/enzyme-adapter-utils");

var _findCurrentFiberUsingSlowPath = _interopRequireDefault(require("./findCurrentFiberUsingSlowPath"));

var _detectFiberTags = _interopRequireDefault(require("./detectFiberTags"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Lazily populated if DOM is available.
var FiberTags = null;

function nodeAndSiblingsArray(nodeWithSibling) {
  var array = [];
  var node = nodeWithSibling;

  while (node != null) {
    array.push(node);
    node = node.sibling;
  }

  return array;
}

function flatten(arr) {
  var result = [];
  var stack = [{
    i: 0,
    array: arr
  }];

  while (stack.length) {
    var n = stack.pop();

    while (n.i < n.array.length) {
      var el = n.array[n.i];
      n.i += 1;

      if (Array.isArray(el)) {
        stack.push(n);
        stack.push({
          i: 0,
          array: el
        });
        break;
      }

      result.push(el);
    }
  }

  return result;
}

function nodeTypeFromType(type) {
  if (type === _reactIs.Portal) {
    return 'portal';
  }

  return (0, _enzymeAdapterUtils.nodeTypeFromType)(type);
}

function isMemo(type) {
  return (0, _enzymeAdapterUtils.compareNodeTypeOf)(type, _reactIs.Memo);
}

function isLazy(type) {
  return (0, _enzymeAdapterUtils.compareNodeTypeOf)(type, _reactIs.Lazy);
}

function unmemoType(type) {
  return isMemo(type) ? type.type : type;
}

function checkIsSuspenseAndCloneElement(el, _ref) {
  var suspenseFallback = _ref.suspenseFallback;

  if (!(0, _reactIs.isSuspense)(el)) {
    return el;
  }

  var children = el.props.children;

  if (suspenseFallback) {
    var fallback = el.props.fallback;
    children = replaceLazyWithFallback(children, fallback);
  }

  var FakeSuspenseWrapper = function FakeSuspenseWrapper(props) {
    return /*#__PURE__*/_react["default"].createElement(el.type, _objectSpread(_objectSpread({}, el.props), props), children);
  };

  return /*#__PURE__*/_react["default"].createElement(FakeSuspenseWrapper, null, children);
}

function elementToTree(el) {
  if (!(0, _reactIs.isPortal)(el)) {
    return (0, _enzymeAdapterUtils.elementToTree)(el, elementToTree);
  }

  var children = el.children,
      containerInfo = el.containerInfo;
  var props = {
    children: children,
    containerInfo: containerInfo
  };
  return {
    nodeType: 'portal',
    type: _reactIs.Portal,
    props: props,
    key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(el.key),
    ref: el.ref || null,
    instance: null,
    rendered: elementToTree(el.children)
  };
}

function _toTree(vnode) {
  if (vnode == null) {
    return null;
  } // TODO(lmr): I'm not really sure I understand whether or not this is what
  // i should be doing, or if this is a hack for something i'm doing wrong
  // somewhere else. Should talk to sebastian about this perhaps


  var node = (0, _findCurrentFiberUsingSlowPath["default"])(vnode);

  switch (node.tag) {
    case FiberTags.HostRoot:
      return childrenToTree(node.child);

    case FiberTags.HostPortal:
      {
        var containerInfo = node.stateNode.containerInfo,
            children = node.memoizedProps;
        var props = {
          containerInfo: containerInfo,
          children: children
        };
        return {
          nodeType: 'portal',
          type: _reactIs.Portal,
          props: props,
          key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
          ref: node.ref,
          instance: null,
          rendered: childrenToTree(node.child)
        };
      }

    case FiberTags.ClassComponent:
      return {
        nodeType: 'class',
        type: node.type,
        props: _objectSpread({}, node.memoizedProps),
        key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
        ref: node.ref,
        instance: node.stateNode,
        rendered: childrenToTree(node.child)
      };

    case FiberTags.FunctionalComponent:
      return {
        nodeType: 'function',
        type: node.type,
        props: _objectSpread({}, node.memoizedProps),
        key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
        ref: node.ref,
        instance: null,
        rendered: childrenToTree(node.child)
      };

    case FiberTags.MemoClass:
      return {
        nodeType: 'class',
        type: node.elementType.type,
        props: _objectSpread({}, node.memoizedProps),
        key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
        ref: node.ref,
        instance: node.stateNode,
        rendered: childrenToTree(node.child.child)
      };

    case FiberTags.MemoSFC:
      {
        var renderedNodes = flatten(nodeAndSiblingsArray(node.child).map(_toTree));

        if (renderedNodes.length === 0) {
          renderedNodes = [node.memoizedProps.children];
        }

        return {
          nodeType: 'function',
          type: node.elementType,
          props: _objectSpread({}, node.memoizedProps),
          key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
          ref: node.ref,
          instance: null,
          rendered: renderedNodes
        };
      }

    case FiberTags.HostComponent:
      {
        var _renderedNodes = flatten(nodeAndSiblingsArray(node.child).map(_toTree));

        if (_renderedNodes.length === 0) {
          _renderedNodes = [node.memoizedProps.children];
        }

        return {
          nodeType: 'host',
          type: node.type,
          props: _objectSpread({}, node.memoizedProps),
          key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
          ref: node.ref,
          instance: node.stateNode,
          rendered: _renderedNodes
        };
      }

    case FiberTags.HostText:
      return node.memoizedProps;

    case FiberTags.Fragment:
    case FiberTags.Mode:
    case FiberTags.ContextProvider:
    case FiberTags.ContextConsumer:
      return childrenToTree(node.child);

    case FiberTags.Profiler:
    case FiberTags.ForwardRef:
      {
        return {
          nodeType: 'function',
          type: node.type,
          props: _objectSpread({}, node.pendingProps),
          key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
          ref: node.ref,
          instance: null,
          rendered: childrenToTree(node.child)
        };
      }

    case FiberTags.Suspense:
      {
        return {
          nodeType: 'function',
          type: _reactIs.Suspense,
          props: _objectSpread({}, node.memoizedProps),
          key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(node.key),
          ref: node.ref,
          instance: null,
          rendered: childrenToTree(node.child)
        };
      }

    case FiberTags.Lazy:
      return childrenToTree(node.child);

    case FiberTags.OffscreenComponent:
      return _toTree(node.child);

    default:
      throw new Error("Enzyme Internal Error: unknown node with tag ".concat(node.tag));
  }
}

function childrenToTree(node) {
  if (!node) {
    return null;
  }

  var children = nodeAndSiblingsArray(node);

  if (children.length === 0) {
    return null;
  }

  if (children.length === 1) {
    return _toTree(children[0]);
  }

  return flatten(children.map(_toTree));
}

function _nodeToHostNode(_node) {
  // NOTE(lmr): node could be a function component
  // which wont have an instance prop, but we can get the
  // host node associated with its return value at that point.
  // Although this breaks down if the return value is an array,
  // as is possible with React 16.
  var node = _node;

  while (node && !Array.isArray(node) && node.instance === null) {
    node = node.rendered;
  } // if the SFC returned null effectively, there is no host node.


  if (!node) {
    return null;
  }

  var mapper = function mapper(item) {
    if (item && item.instance) return _reactDom["default"].findDOMNode(item.instance);
    return null;
  };

  if (Array.isArray(node)) {
    return node.map(mapper);
  }

  if (Array.isArray(node.rendered) && node.nodeType === 'class') {
    return node.rendered.map(mapper);
  }

  return mapper(node);
}

function replaceLazyWithFallback(node, fallback) {
  if (!node) {
    return null;
  }

  if (Array.isArray(node)) {
    return node.map(function (el) {
      return replaceLazyWithFallback(el, fallback);
    });
  }

  if (isLazy(node.type)) {
    return fallback;
  }

  return _objectSpread(_objectSpread({}, node), {}, {
    props: _objectSpread(_objectSpread({}, node.props), {}, {
      children: replaceLazyWithFallback(node.props.children, fallback)
    })
  });
}

var eventOptions = {
  animation: true,
  pointerEvents: true,
  auxClick: true
};

function getEmptyStateValue() {
  // this handles a bug in React 16.0 - 16.2
  // see https://github.com/facebook/react/commit/39be83565c65f9c522150e52375167568a2a1459
  // also see https://github.com/facebook/react/pull/11965
  var EmptyState = /*#__PURE__*/function (_React$Component) {
    _inherits(EmptyState, _React$Component);

    var _super = _createSuper(EmptyState);

    function EmptyState() {
      _classCallCheck(this, EmptyState);

      return _super.apply(this, arguments);
    }

    _createClass(EmptyState, [{
      key: "render",
      value: function render() {
        return null;
      }
    }]);

    return EmptyState;
  }(_react["default"].Component);

  var testRenderer = new _shallow["default"]();
  testRenderer.render( /*#__PURE__*/_react["default"].createElement(EmptyState));
  return testRenderer._instance.state;
}

function wrapAct(fn) {
  var returnVal;

  _testUtils["default"].act(function () {
    returnVal = fn();
  });

  return returnVal;
}

function getProviderDefaultValue(Provider) {
  // React stores references to the Provider's defaultValue differently across versions.
  if ('_defaultValue' in Provider._context) {
    return Provider._context._defaultValue;
  }

  if ('_currentValue' in Provider._context) {
    return Provider._context._currentValue;
  }

  throw new Error('Enzyme Internal Error: can’t figure out how to get Provider’s default value');
}

function makeFakeElement(type) {
  return {
    $$typeof: _reactIs.Element,
    type: type
  };
}

function isStateful(Component) {
  return Component.prototype && (Component.prototype.isReactComponent || Array.isArray(Component.__reactAutoBindPairs) // fallback for createClass components
  );
}

var ReactSeventeenAdapter = /*#__PURE__*/function (_EnzymeAdapter) {
  _inherits(ReactSeventeenAdapter, _EnzymeAdapter);

  var _super2 = _createSuper(ReactSeventeenAdapter);

  function ReactSeventeenAdapter() {
    var _this;

    _classCallCheck(this, ReactSeventeenAdapter);

    _this = _super2.call(this);
    var lifecycles = _this.options.lifecycles;
    _this.options = _objectSpread(_objectSpread({}, _this.options), {}, {
      enableComponentDidUpdateOnSetState: true,
      // TODO: remove, semver-major
      legacyContextMode: 'parent',
      lifecycles: _objectSpread(_objectSpread({}, lifecycles), {}, {
        componentDidUpdate: {
          onSetState: true
        },
        getDerivedStateFromProps: {
          hasShouldComponentUpdateBug: false
        },
        getSnapshotBeforeUpdate: true,
        setState: {
          skipsComponentDidUpdateOnNullish: true
        },
        getChildContext: {
          calledByRenderer: false
        },
        getDerivedStateFromError: true
      })
    });
    return _this;
  }

  _createClass(ReactSeventeenAdapter, [{
    key: "createMountRenderer",
    value: function createMountRenderer(options) {
      (0, _enzymeAdapterUtils.assertDomAvailable)('mount');

      if ((0, _has["default"])(options, 'suspenseFallback')) {
        throw new TypeError('`suspenseFallback` is not supported by the `mount` renderer');
      }

      if (FiberTags === null) {
        // Requires DOM.
        FiberTags = (0, _detectFiberTags["default"])();
      }

      var attachTo = options.attachTo,
          hydrateIn = options.hydrateIn,
          wrappingComponentProps = options.wrappingComponentProps;
      var domNode = hydrateIn || attachTo || global.document.createElement('div');
      var instance = null;
      var adapter = this;
      return {
        render: function render(el, context, callback) {
          return wrapAct(function () {
            if (instance === null) {
              var type = el.type,
                  props = el.props,
                  ref = el.ref;

              var wrapperProps = _objectSpread({
                Component: type,
                props: props,
                wrappingComponentProps: wrappingComponentProps,
                context: context
              }, ref && {
                refProp: ref
              });

              var ReactWrapperComponent = (0, _enzymeAdapterUtils.createMountWrapper)(el, _objectSpread(_objectSpread({}, options), {}, {
                adapter: adapter
              }));

              var wrappedEl = /*#__PURE__*/_react["default"].createElement(ReactWrapperComponent, wrapperProps);

              instance = hydrateIn ? _reactDom["default"].hydrate(wrappedEl, domNode) : _reactDom["default"].render(wrappedEl, domNode);

              if (typeof callback === 'function') {
                callback();
              }
            } else {
              instance.setChildProps(el.props, context, callback);
            }
          });
        },
        unmount: function unmount() {
          _reactDom["default"].unmountComponentAtNode(domNode);

          instance = null;
        },
        getNode: function getNode() {
          if (!instance) {
            return null;
          }

          return (0, _enzymeAdapterUtils.getNodeFromRootFinder)(adapter.isCustomComponent, _toTree(instance._reactInternals), options);
        },
        simulateError: function simulateError(nodeHierarchy, rootNode, error) {
          var isErrorBoundary = function isErrorBoundary(_ref2) {
            var elInstance = _ref2.instance,
                type = _ref2.type;

            if (type && type.getDerivedStateFromError) {
              return true;
            }

            return elInstance && elInstance.componentDidCatch;
          };

          var _ref3 = nodeHierarchy.find(isErrorBoundary) || {},
              catchingInstance = _ref3.instance,
              catchingType = _ref3.type;

          (0, _enzymeAdapterUtils.simulateError)(error, catchingInstance, rootNode, nodeHierarchy, nodeTypeFromType, adapter.displayNameOfNode, catchingType);
        },
        simulateEvent: function simulateEvent(node, event, mock) {
          var mappedEvent = (0, _enzymeAdapterUtils.mapNativeEventNames)(event, eventOptions);
          var eventFn = _testUtils["default"].Simulate[mappedEvent];

          if (!eventFn) {
            throw new TypeError("ReactWrapper::simulate() event '".concat(event, "' does not exist"));
          }

          wrapAct(function () {
            eventFn(adapter.nodeToHostNode(node), mock);
          });
        },
        batchedUpdates: function batchedUpdates(fn) {
          return fn(); // return ReactDOM.unstable_batchedUpdates(fn);
        },
        getWrappingComponentRenderer: function getWrappingComponentRenderer() {
          return _objectSpread(_objectSpread({}, this), (0, _enzymeAdapterUtils.getWrappingComponentMountRenderer)({
            toTree: function toTree(inst) {
              return _toTree(inst._reactInternals);
            },
            getMountWrapperInstance: function getMountWrapperInstance() {
              return instance;
            }
          }));
        },
        wrapInvoke: wrapAct
      };
    }
  }, {
    key: "createShallowRenderer",
    value: function createShallowRenderer() {
      var _this2 = this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var adapter = this;
      var renderer = new _shallow["default"]();
      var suspenseFallback = options.suspenseFallback;

      if (typeof suspenseFallback !== 'undefined' && typeof suspenseFallback !== 'boolean') {
        throw TypeError('`options.suspenseFallback` should be boolean or undefined');
      }

      var isDOM = false;
      var cachedNode = null;
      var lastComponent = null;
      var wrappedComponent = null;
      var sentinel = {}; // wrap memo components with a PureComponent, or a class component with sCU

      var wrapPureComponent = function wrapPureComponent(Component, compare) {
        if (lastComponent !== Component) {
          if (isStateful(Component)) {
            wrappedComponent = /*#__PURE__*/function (_Component) {
              _inherits(wrappedComponent, _Component);

              var _super3 = _createSuper(wrappedComponent);

              function wrappedComponent() {
                _classCallCheck(this, wrappedComponent);

                return _super3.apply(this, arguments);
              }

              return wrappedComponent;
            }(Component);

            if (compare) {
              wrappedComponent.prototype.shouldComponentUpdate = function (nextProps) {
                return !compare(_this2.props, nextProps);
              };
            } else {
              wrappedComponent.prototype.isPureReactComponent = true;
            }
          } else {
            var memoized = sentinel;
            var prevProps;

            wrappedComponent = function wrappedComponentFn(props) {
              var shouldUpdate = memoized === sentinel || (compare ? !compare(prevProps, props) : !(0, _enzymeShallowEqual["default"])(prevProps, props));

              if (shouldUpdate) {
                for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                  args[_key - 1] = arguments[_key];
                }

                memoized = Component.apply(void 0, [_objectSpread(_objectSpread({}, Component.defaultProps), props)].concat(args));
                prevProps = props;
              }

              return memoized;
            };
          }

          (0, _object["default"])(wrappedComponent, Component, {
            displayName: adapter.displayNameOfNode({
              type: Component
            })
          });
          lastComponent = Component;
        }

        return wrappedComponent;
      }; // Wrap functional components on versions prior to 16.5,
      // to avoid inadvertently pass a `this` instance to it.


      var wrapFunctionalComponent = function wrapFunctionalComponent(Component) {
        if ((0, _has["default"])(Component, 'defaultProps')) {
          if (lastComponent !== Component) {
            wrappedComponent = (0, _object["default"])( // eslint-disable-next-line new-cap
            function (props) {
              for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
                args[_key2 - 1] = arguments[_key2];
              }

              return Component.apply(void 0, [_objectSpread(_objectSpread({}, Component.defaultProps), props)].concat(args));
            }, Component, {
              displayName: adapter.displayNameOfNode({
                type: Component
              })
            });
            lastComponent = Component;
          }

          return wrappedComponent;
        }

        return Component;
      };

      var renderElement = function renderElement(elConfig) {
        for (var _len3 = arguments.length, rest = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
          rest[_key3 - 1] = arguments[_key3];
        }

        var renderedEl = renderer.render.apply(renderer, [elConfig].concat(rest));
        var typeIsExisted = !!(renderedEl && renderedEl.type);

        if (typeIsExisted) {
          var clonedEl = checkIsSuspenseAndCloneElement(renderedEl, {
            suspenseFallback: suspenseFallback
          });
          var elementIsChanged = clonedEl.type !== renderedEl.type;

          if (elementIsChanged) {
            return renderer.render.apply(renderer, [_objectSpread(_objectSpread({}, elConfig), {}, {
              type: clonedEl.type
            })].concat(rest));
          }
        }

        return renderedEl;
      };

      return {
        // eslint-disable-next-line consistent-return
        render: function render(el, unmaskedContext) {
          var _ref4 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
              _ref4$providerValues = _ref4.providerValues,
              providerValues = _ref4$providerValues === void 0 ? new Map() : _ref4$providerValues;

          cachedNode = el;

          if (typeof el.type === 'string') {
            isDOM = true;
          } else if ((0, _reactIs.isContextProvider)(el)) {
            providerValues.set(el.type, el.props.value);
            var MockProvider = (0, _object["default"])(function (props) {
              return props.children;
            }, el.type);
            return (0, _enzymeAdapterUtils.withSetStateAllowed)(function () {
              return renderElement(_objectSpread(_objectSpread({}, el), {}, {
                type: MockProvider
              }));
            });
          } else if ((0, _reactIs.isContextConsumer)(el)) {
            var Provider = adapter.getProviderFromConsumer(el.type);
            var value = providerValues.has(Provider) ? providerValues.get(Provider) : getProviderDefaultValue(Provider);
            var MockConsumer = (0, _object["default"])(function (props) {
              return props.children(value);
            }, el.type);
            return (0, _enzymeAdapterUtils.withSetStateAllowed)(function () {
              return renderElement(_objectSpread(_objectSpread({}, el), {}, {
                type: MockConsumer
              }));
            });
          } else {
            isDOM = false;
            var renderedEl = el;

            if (isLazy(renderedEl)) {
              throw TypeError('`React.lazy` is not supported by shallow rendering.');
            }

            renderedEl = checkIsSuspenseAndCloneElement(renderedEl, {
              suspenseFallback: suspenseFallback
            });
            var _renderedEl = renderedEl,
                Component = _renderedEl.type;
            var context = (0, _enzymeAdapterUtils.getMaskedContext)(Component.contextTypes, unmaskedContext);

            if (isMemo(el.type)) {
              var _el$type = el.type,
                  InnerComp = _el$type.type,
                  compare = _el$type.compare;
              return (0, _enzymeAdapterUtils.withSetStateAllowed)(function () {
                return renderElement(_objectSpread(_objectSpread({}, el), {}, {
                  type: wrapPureComponent(InnerComp, compare)
                }), context);
              });
            }

            var isComponentStateful = isStateful(Component);

            if (!isComponentStateful && typeof Component === 'function') {
              return (0, _enzymeAdapterUtils.withSetStateAllowed)(function () {
                return renderElement(_objectSpread(_objectSpread({}, renderedEl), {}, {
                  type: wrapFunctionalComponent(Component)
                }), context);
              });
            }

            if (isComponentStateful) {
              if (renderer._instance && el.props === renderer._instance.props && !(0, _enzymeShallowEqual["default"])(context, renderer._instance.context)) {
                var _spyMethod = (0, _enzymeAdapterUtils.spyMethod)(renderer, '_updateClassComponent', function (originalMethod) {
                  return function _updateClassComponent() {
                    var props = renderer._instance.props;

                    var clonedProps = _objectSpread({}, props);

                    renderer._instance.props = clonedProps;

                    for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
                      args[_key4] = arguments[_key4];
                    }

                    var result = originalMethod.apply(renderer, args);
                    renderer._instance.props = props;
                    restore();
                    return result;
                  };
                }),
                    restore = _spyMethod.restore;
              } // fix react bug; see implementation of `getEmptyStateValue`


              var emptyStateValue = getEmptyStateValue();

              if (emptyStateValue) {
                Object.defineProperty(Component.prototype, 'state', {
                  configurable: true,
                  enumerable: true,
                  get: function get() {
                    return null;
                  },
                  set: function set(value) {
                    if (value !== emptyStateValue) {
                      Object.defineProperty(this, 'state', {
                        configurable: true,
                        enumerable: true,
                        value: value,
                        writable: true
                      });
                    }

                    return true;
                  }
                });
              }
            }

            return (0, _enzymeAdapterUtils.withSetStateAllowed)(function () {
              return renderElement(renderedEl, context);
            });
          }
        },
        unmount: function unmount() {
          renderer.unmount();
        },
        getNode: function getNode() {
          if (isDOM) {
            return elementToTree(cachedNode);
          }

          var output = renderer.getRenderOutput();
          return {
            nodeType: nodeTypeFromType(cachedNode.type),
            type: cachedNode.type,
            props: cachedNode.props,
            key: (0, _enzymeAdapterUtils.ensureKeyOrUndefined)(cachedNode.key),
            ref: cachedNode.ref,
            instance: renderer._instance,
            rendered: Array.isArray(output) ? flatten(output).map(function (el) {
              return elementToTree(el);
            }) : elementToTree(output)
          };
        },
        simulateError: function simulateError(nodeHierarchy, rootNode, error) {
          (0, _enzymeAdapterUtils.simulateError)(error, renderer._instance, cachedNode, nodeHierarchy.concat(cachedNode), nodeTypeFromType, adapter.displayNameOfNode, cachedNode.type);
        },
        simulateEvent: function simulateEvent(node, event) {
          for (var _len5 = arguments.length, args = new Array(_len5 > 2 ? _len5 - 2 : 0), _key5 = 2; _key5 < _len5; _key5++) {
            args[_key5 - 2] = arguments[_key5];
          }

          var handler = node.props[(0, _enzymeAdapterUtils.propFromEvent)(event, eventOptions)];

          if (handler) {
            (0, _enzymeAdapterUtils.withSetStateAllowed)(function () {
              // TODO(lmr): create/use synthetic events
              // TODO(lmr): emulate React's event propagation
              // ReactDOM.unstable_batchedUpdates(() => {
              handler.apply(void 0, args); // });
            });
          }
        },
        batchedUpdates: function batchedUpdates(fn) {
          return fn(); // return ReactDOM.unstable_batchedUpdates(fn);
        },
        checkPropTypes: function checkPropTypes(typeSpecs, values, location, hierarchy) {
          return (0, _checkPropTypes2["default"])(typeSpecs, values, location, (0, _enzymeAdapterUtils.displayNameOfNode)(cachedNode), function () {
            return (0, _enzymeAdapterUtils.getComponentStack)(hierarchy.concat([cachedNode]));
          });
        }
      };
    }
  }, {
    key: "createStringRenderer",
    value: function createStringRenderer(options) {
      if ((0, _has["default"])(options, 'suspenseFallback')) {
        throw new TypeError('`suspenseFallback` should not be specified in options of string renderer');
      }

      return {
        render: function render(el, context) {
          if (options.context && (el.type.contextTypes || options.childContextTypes)) {
            var childContextTypes = _objectSpread(_objectSpread({}, el.type.contextTypes || {}), options.childContextTypes);

            var ContextWrapper = (0, _enzymeAdapterUtils.createRenderWrapper)(el, context, childContextTypes);
            return _server["default"].renderToStaticMarkup( /*#__PURE__*/_react["default"].createElement(ContextWrapper));
          }

          return _server["default"].renderToStaticMarkup(el);
        }
      };
    } // Provided a bag of options, return an `EnzymeRenderer`. Some options can be implementation
    // specific, like `attach` etc. for React, but not part of this interface explicitly.
    // eslint-disable-next-line class-methods-use-this

  }, {
    key: "createRenderer",
    value: function createRenderer(options) {
      switch (options.mode) {
        case _enzyme.EnzymeAdapter.MODES.MOUNT:
          return this.createMountRenderer(options);

        case _enzyme.EnzymeAdapter.MODES.SHALLOW:
          return this.createShallowRenderer(options);

        case _enzyme.EnzymeAdapter.MODES.STRING:
          return this.createStringRenderer(options);

        default:
          throw new Error("Enzyme Internal Error: Unrecognized mode: ".concat(options.mode));
      }
    }
  }, {
    key: "wrap",
    value: function wrap(element) {
      return (0, _enzymeAdapterUtils.wrap)(element);
    } // converts an RSTNode to the corresponding JSX Pragma Element. This will be needed
    // in order to implement the `Wrapper.mount()` and `Wrapper.shallow()` methods, but should
    // be pretty straightforward for people to implement.
    // eslint-disable-next-line class-methods-use-this

  }, {
    key: "nodeToElement",
    value: function nodeToElement(node) {
      if (!node || _typeof(node) !== 'object') return null;
      var type = node.type;
      return /*#__PURE__*/_react["default"].createElement(unmemoType(type), (0, _enzymeAdapterUtils.propsWithKeysAndRef)(node));
    } // eslint-disable-next-line class-methods-use-this

  }, {
    key: "matchesElementType",
    value: function matchesElementType(node, matchingType) {
      if (!node) {
        return node;
      }

      var type = node.type;
      return unmemoType(type) === unmemoType(matchingType);
    }
  }, {
    key: "elementToNode",
    value: function elementToNode(element) {
      return elementToTree(element);
    }
  }, {
    key: "nodeToHostNode",
    value: function nodeToHostNode(node) {
      var supportsArray = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      var nodes = _nodeToHostNode(node);

      if (Array.isArray(nodes) && !supportsArray) {
        return nodes[0];
      }

      return nodes;
    }
  }, {
    key: "displayNameOfNode",
    value: function displayNameOfNode(node) {
      if (!node) return null;
      var type = node.type,
          $$typeof = node.$$typeof;
      var adapter = this;
      var nodeType = type || $$typeof; // newer node types may be undefined, so only test if the nodeType exists

      if (nodeType) {
        switch (nodeType) {
          case _reactIs.ConcurrentMode || NaN:
            return 'ConcurrentMode';

          case _reactIs.Fragment || NaN:
            return 'Fragment';

          case _reactIs.StrictMode || NaN:
            return 'StrictMode';

          case _reactIs.Profiler || NaN:
            return 'Profiler';

          case _reactIs.Portal || NaN:
            return 'Portal';

          case _reactIs.Suspense || NaN:
            return 'Suspense';

          default:
        }
      }

      var $$typeofType = type && type.$$typeof;

      switch ($$typeofType) {
        case _reactIs.ContextConsumer || NaN:
          return 'ContextConsumer';

        case _reactIs.ContextProvider || NaN:
          return 'ContextProvider';

        case _reactIs.Memo || NaN:
          {
            var nodeName = (0, _enzymeAdapterUtils.displayNameOfNode)(node);
            return typeof nodeName === 'string' ? nodeName : "Memo(".concat(adapter.displayNameOfNode(type), ")");
          }

        case _reactIs.ForwardRef || NaN:
          {
            if (type.displayName) {
              return type.displayName;
            }

            var name = adapter.displayNameOfNode({
              type: type.render
            });
            return name ? "ForwardRef(".concat(name, ")") : 'ForwardRef';
          }

        case _reactIs.Lazy || NaN:
          {
            return 'lazy';
          }

        default:
          return (0, _enzymeAdapterUtils.displayNameOfNode)(node);
      }
    }
  }, {
    key: "isValidElement",
    value: function isValidElement(element) {
      return (0, _reactIs.isElement)(element);
    }
  }, {
    key: "isValidElementType",
    value: function isValidElementType(object) {
      return !!object && (0, _reactIs.isValidElementType)(object);
    }
  }, {
    key: "isFragment",
    value: function isFragment(fragment) {
      return (0, _Utils.typeOfNode)(fragment) === _reactIs.Fragment;
    }
  }, {
    key: "isCustomComponent",
    value: function isCustomComponent(type) {
      var fakeElement = makeFakeElement(type);
      return !!type && (typeof type === 'function' || (0, _reactIs.isForwardRef)(fakeElement) || (0, _reactIs.isContextProvider)(fakeElement) || (0, _reactIs.isContextConsumer)(fakeElement) || (0, _reactIs.isSuspense)(fakeElement));
    }
  }, {
    key: "isContextConsumer",
    value: function isContextConsumer(type) {
      return !!type && (0, _reactIs.isContextConsumer)(makeFakeElement(type));
    }
  }, {
    key: "isCustomComponentElement",
    value: function isCustomComponentElement(inst) {
      if (!inst || !this.isValidElement(inst)) {
        return false;
      }

      return this.isCustomComponent(inst.type);
    }
  }, {
    key: "getProviderFromConsumer",
    value: function getProviderFromConsumer(Consumer) {
      // React stores references to the Provider on a Consumer differently across versions.
      if (Consumer) {
        var Provider;

        if (Consumer._context) {
          // check this first, to avoid a deprecation warning
          Provider = Consumer._context.Provider;
        } else if (Consumer.Provider) {
          Provider = Consumer.Provider;
        }

        if (Provider) {
          return Provider;
        }
      }

      throw new Error('Enzyme Internal Error: can’t figure out how to get Provider from Consumer');
    }
  }, {
    key: "createElement",
    value: function createElement() {
      return /*#__PURE__*/_react["default"].createElement.apply(_react["default"], arguments);
    }
  }, {
    key: "wrapWithWrappingComponent",
    value: function wrapWithWrappingComponent(node, options) {
      return {
        RootFinder: _enzymeAdapterUtils.RootFinder,
        node: (0, _enzymeAdapterUtils.wrapWithWrappingComponent)(_react["default"].createElement, node, options)
      };
    }
  }]);

  return ReactSeventeenAdapter;
}(_enzyme.EnzymeAdapter);

module.exports = ReactSeventeenAdapter;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9SZWFjdFNldmVudGVlbkFkYXB0ZXIuanMiXSwibmFtZXMiOlsiRmliZXJUYWdzIiwibm9kZUFuZFNpYmxpbmdzQXJyYXkiLCJub2RlV2l0aFNpYmxpbmciLCJhcnJheSIsIm5vZGUiLCJwdXNoIiwic2libGluZyIsImZsYXR0ZW4iLCJhcnIiLCJyZXN1bHQiLCJzdGFjayIsImkiLCJsZW5ndGgiLCJuIiwicG9wIiwiZWwiLCJBcnJheSIsImlzQXJyYXkiLCJub2RlVHlwZUZyb21UeXBlIiwidHlwZSIsIlBvcnRhbCIsImlzTWVtbyIsIk1lbW8iLCJpc0xhenkiLCJMYXp5IiwidW5tZW1vVHlwZSIsImNoZWNrSXNTdXNwZW5zZUFuZENsb25lRWxlbWVudCIsInN1c3BlbnNlRmFsbGJhY2siLCJjaGlsZHJlbiIsInByb3BzIiwiZmFsbGJhY2siLCJyZXBsYWNlTGF6eVdpdGhGYWxsYmFjayIsIkZha2VTdXNwZW5zZVdyYXBwZXIiLCJSZWFjdCIsImNyZWF0ZUVsZW1lbnQiLCJlbGVtZW50VG9UcmVlIiwiY29udGFpbmVySW5mbyIsIm5vZGVUeXBlIiwia2V5IiwicmVmIiwiaW5zdGFuY2UiLCJyZW5kZXJlZCIsInRvVHJlZSIsInZub2RlIiwidGFnIiwiSG9zdFJvb3QiLCJjaGlsZHJlblRvVHJlZSIsImNoaWxkIiwiSG9zdFBvcnRhbCIsInN0YXRlTm9kZSIsIm1lbW9pemVkUHJvcHMiLCJDbGFzc0NvbXBvbmVudCIsIkZ1bmN0aW9uYWxDb21wb25lbnQiLCJNZW1vQ2xhc3MiLCJlbGVtZW50VHlwZSIsIk1lbW9TRkMiLCJyZW5kZXJlZE5vZGVzIiwibWFwIiwiSG9zdENvbXBvbmVudCIsIkhvc3RUZXh0IiwiRnJhZ21lbnQiLCJNb2RlIiwiQ29udGV4dFByb3ZpZGVyIiwiQ29udGV4dENvbnN1bWVyIiwiUHJvZmlsZXIiLCJGb3J3YXJkUmVmIiwicGVuZGluZ1Byb3BzIiwiU3VzcGVuc2UiLCJPZmZzY3JlZW5Db21wb25lbnQiLCJFcnJvciIsIm5vZGVUb0hvc3ROb2RlIiwiX25vZGUiLCJtYXBwZXIiLCJpdGVtIiwiUmVhY3RET00iLCJmaW5kRE9NTm9kZSIsImV2ZW50T3B0aW9ucyIsImFuaW1hdGlvbiIsInBvaW50ZXJFdmVudHMiLCJhdXhDbGljayIsImdldEVtcHR5U3RhdGVWYWx1ZSIsIkVtcHR5U3RhdGUiLCJDb21wb25lbnQiLCJ0ZXN0UmVuZGVyZXIiLCJTaGFsbG93UmVuZGVyZXIiLCJyZW5kZXIiLCJfaW5zdGFuY2UiLCJzdGF0ZSIsIndyYXBBY3QiLCJmbiIsInJldHVyblZhbCIsIlRlc3RVdGlscyIsImFjdCIsImdldFByb3ZpZGVyRGVmYXVsdFZhbHVlIiwiUHJvdmlkZXIiLCJfY29udGV4dCIsIl9kZWZhdWx0VmFsdWUiLCJfY3VycmVudFZhbHVlIiwibWFrZUZha2VFbGVtZW50IiwiJCR0eXBlb2YiLCJFbGVtZW50IiwiaXNTdGF0ZWZ1bCIsInByb3RvdHlwZSIsImlzUmVhY3RDb21wb25lbnQiLCJfX3JlYWN0QXV0b0JpbmRQYWlycyIsIlJlYWN0U2V2ZW50ZWVuQWRhcHRlciIsImxpZmVjeWNsZXMiLCJvcHRpb25zIiwiZW5hYmxlQ29tcG9uZW50RGlkVXBkYXRlT25TZXRTdGF0ZSIsImxlZ2FjeUNvbnRleHRNb2RlIiwiY29tcG9uZW50RGlkVXBkYXRlIiwib25TZXRTdGF0ZSIsImdldERlcml2ZWRTdGF0ZUZyb21Qcm9wcyIsImhhc1Nob3VsZENvbXBvbmVudFVwZGF0ZUJ1ZyIsImdldFNuYXBzaG90QmVmb3JlVXBkYXRlIiwic2V0U3RhdGUiLCJza2lwc0NvbXBvbmVudERpZFVwZGF0ZU9uTnVsbGlzaCIsImdldENoaWxkQ29udGV4dCIsImNhbGxlZEJ5UmVuZGVyZXIiLCJnZXREZXJpdmVkU3RhdGVGcm9tRXJyb3IiLCJUeXBlRXJyb3IiLCJhdHRhY2hUbyIsImh5ZHJhdGVJbiIsIndyYXBwaW5nQ29tcG9uZW50UHJvcHMiLCJkb21Ob2RlIiwiZ2xvYmFsIiwiZG9jdW1lbnQiLCJhZGFwdGVyIiwiY29udGV4dCIsImNhbGxiYWNrIiwid3JhcHBlclByb3BzIiwicmVmUHJvcCIsIlJlYWN0V3JhcHBlckNvbXBvbmVudCIsIndyYXBwZWRFbCIsImh5ZHJhdGUiLCJzZXRDaGlsZFByb3BzIiwidW5tb3VudCIsInVubW91bnRDb21wb25lbnRBdE5vZGUiLCJnZXROb2RlIiwiaXNDdXN0b21Db21wb25lbnQiLCJfcmVhY3RJbnRlcm5hbHMiLCJzaW11bGF0ZUVycm9yIiwibm9kZUhpZXJhcmNoeSIsInJvb3ROb2RlIiwiZXJyb3IiLCJpc0Vycm9yQm91bmRhcnkiLCJlbEluc3RhbmNlIiwiY29tcG9uZW50RGlkQ2F0Y2giLCJmaW5kIiwiY2F0Y2hpbmdJbnN0YW5jZSIsImNhdGNoaW5nVHlwZSIsImRpc3BsYXlOYW1lT2ZOb2RlIiwic2ltdWxhdGVFdmVudCIsImV2ZW50IiwibW9jayIsIm1hcHBlZEV2ZW50IiwiZXZlbnRGbiIsIlNpbXVsYXRlIiwiYmF0Y2hlZFVwZGF0ZXMiLCJnZXRXcmFwcGluZ0NvbXBvbmVudFJlbmRlcmVyIiwiaW5zdCIsImdldE1vdW50V3JhcHBlckluc3RhbmNlIiwid3JhcEludm9rZSIsInJlbmRlcmVyIiwiaXNET00iLCJjYWNoZWROb2RlIiwibGFzdENvbXBvbmVudCIsIndyYXBwZWRDb21wb25lbnQiLCJzZW50aW5lbCIsIndyYXBQdXJlQ29tcG9uZW50IiwiY29tcGFyZSIsInNob3VsZENvbXBvbmVudFVwZGF0ZSIsIm5leHRQcm9wcyIsImlzUHVyZVJlYWN0Q29tcG9uZW50IiwibWVtb2l6ZWQiLCJwcmV2UHJvcHMiLCJ3cmFwcGVkQ29tcG9uZW50Rm4iLCJzaG91bGRVcGRhdGUiLCJhcmdzIiwiZGVmYXVsdFByb3BzIiwiZGlzcGxheU5hbWUiLCJ3cmFwRnVuY3Rpb25hbENvbXBvbmVudCIsInJlbmRlckVsZW1lbnQiLCJlbENvbmZpZyIsInJlc3QiLCJyZW5kZXJlZEVsIiwidHlwZUlzRXhpc3RlZCIsImNsb25lZEVsIiwiZWxlbWVudElzQ2hhbmdlZCIsInVubWFza2VkQ29udGV4dCIsInByb3ZpZGVyVmFsdWVzIiwiTWFwIiwic2V0IiwidmFsdWUiLCJNb2NrUHJvdmlkZXIiLCJnZXRQcm92aWRlckZyb21Db25zdW1lciIsImhhcyIsImdldCIsIk1vY2tDb25zdW1lciIsImNvbnRleHRUeXBlcyIsIklubmVyQ29tcCIsImlzQ29tcG9uZW50U3RhdGVmdWwiLCJvcmlnaW5hbE1ldGhvZCIsIl91cGRhdGVDbGFzc0NvbXBvbmVudCIsImNsb25lZFByb3BzIiwiYXBwbHkiLCJyZXN0b3JlIiwiZW1wdHlTdGF0ZVZhbHVlIiwiT2JqZWN0IiwiZGVmaW5lUHJvcGVydHkiLCJjb25maWd1cmFibGUiLCJlbnVtZXJhYmxlIiwid3JpdGFibGUiLCJvdXRwdXQiLCJnZXRSZW5kZXJPdXRwdXQiLCJjb25jYXQiLCJoYW5kbGVyIiwiY2hlY2tQcm9wVHlwZXMiLCJ0eXBlU3BlY3MiLCJ2YWx1ZXMiLCJsb2NhdGlvbiIsImhpZXJhcmNoeSIsImNoaWxkQ29udGV4dFR5cGVzIiwiQ29udGV4dFdyYXBwZXIiLCJSZWFjdERPTVNlcnZlciIsInJlbmRlclRvU3RhdGljTWFya3VwIiwibW9kZSIsIkVuenltZUFkYXB0ZXIiLCJNT0RFUyIsIk1PVU5UIiwiY3JlYXRlTW91bnRSZW5kZXJlciIsIlNIQUxMT1ciLCJjcmVhdGVTaGFsbG93UmVuZGVyZXIiLCJTVFJJTkciLCJjcmVhdGVTdHJpbmdSZW5kZXJlciIsImVsZW1lbnQiLCJtYXRjaGluZ1R5cGUiLCJzdXBwb3J0c0FycmF5Iiwibm9kZXMiLCJDb25jdXJyZW50TW9kZSIsIk5hTiIsIlN0cmljdE1vZGUiLCIkJHR5cGVvZlR5cGUiLCJub2RlTmFtZSIsIm5hbWUiLCJvYmplY3QiLCJmcmFnbWVudCIsImZha2VFbGVtZW50IiwiaXNWYWxpZEVsZW1lbnQiLCJDb25zdW1lciIsIlJvb3RGaW5kZXIiLCJtb2R1bGUiLCJleHBvcnRzIl0sIm1hcHBpbmdzIjoiOzs7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBQ0E7O0FBcUJBOztBQUNBOztBQUNBOztBQUNBOztBQXVCQTs7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTtBQUNBLElBQUlBLFNBQVMsR0FBRyxJQUFoQjs7QUFFQSxTQUFTQyxvQkFBVCxDQUE4QkMsZUFBOUIsRUFBK0M7QUFDN0MsTUFBTUMsS0FBSyxHQUFHLEVBQWQ7QUFDQSxNQUFJQyxJQUFJLEdBQUdGLGVBQVg7O0FBQ0EsU0FBT0UsSUFBSSxJQUFJLElBQWYsRUFBcUI7QUFDbkJELElBQUFBLEtBQUssQ0FBQ0UsSUFBTixDQUFXRCxJQUFYO0FBQ0FBLElBQUFBLElBQUksR0FBR0EsSUFBSSxDQUFDRSxPQUFaO0FBQ0Q7O0FBQ0QsU0FBT0gsS0FBUDtBQUNEOztBQUVELFNBQVNJLE9BQVQsQ0FBaUJDLEdBQWpCLEVBQXNCO0FBQ3BCLE1BQU1DLE1BQU0sR0FBRyxFQUFmO0FBQ0EsTUFBTUMsS0FBSyxHQUFHLENBQUM7QUFBRUMsSUFBQUEsQ0FBQyxFQUFFLENBQUw7QUFBUVIsSUFBQUEsS0FBSyxFQUFFSztBQUFmLEdBQUQsQ0FBZDs7QUFDQSxTQUFPRSxLQUFLLENBQUNFLE1BQWIsRUFBcUI7QUFDbkIsUUFBTUMsQ0FBQyxHQUFHSCxLQUFLLENBQUNJLEdBQU4sRUFBVjs7QUFDQSxXQUFPRCxDQUFDLENBQUNGLENBQUYsR0FBTUUsQ0FBQyxDQUFDVixLQUFGLENBQVFTLE1BQXJCLEVBQTZCO0FBQzNCLFVBQU1HLEVBQUUsR0FBR0YsQ0FBQyxDQUFDVixLQUFGLENBQVFVLENBQUMsQ0FBQ0YsQ0FBVixDQUFYO0FBQ0FFLE1BQUFBLENBQUMsQ0FBQ0YsQ0FBRixJQUFPLENBQVA7O0FBQ0EsVUFBSUssS0FBSyxDQUFDQyxPQUFOLENBQWNGLEVBQWQsQ0FBSixFQUF1QjtBQUNyQkwsUUFBQUEsS0FBSyxDQUFDTCxJQUFOLENBQVdRLENBQVg7QUFDQUgsUUFBQUEsS0FBSyxDQUFDTCxJQUFOLENBQVc7QUFBRU0sVUFBQUEsQ0FBQyxFQUFFLENBQUw7QUFBUVIsVUFBQUEsS0FBSyxFQUFFWTtBQUFmLFNBQVg7QUFDQTtBQUNEOztBQUNETixNQUFBQSxNQUFNLENBQUNKLElBQVAsQ0FBWVUsRUFBWjtBQUNEO0FBQ0Y7O0FBQ0QsU0FBT04sTUFBUDtBQUNEOztBQUVELFNBQVNTLGdCQUFULENBQTBCQyxJQUExQixFQUFnQztBQUM5QixNQUFJQSxJQUFJLEtBQUtDLGVBQWIsRUFBcUI7QUFDbkIsV0FBTyxRQUFQO0FBQ0Q7O0FBRUQsU0FBTywwQ0FBcUJELElBQXJCLENBQVA7QUFDRDs7QUFFRCxTQUFTRSxNQUFULENBQWdCRixJQUFoQixFQUFzQjtBQUNwQixTQUFPLDJDQUFrQkEsSUFBbEIsRUFBd0JHLGFBQXhCLENBQVA7QUFDRDs7QUFFRCxTQUFTQyxNQUFULENBQWdCSixJQUFoQixFQUFzQjtBQUNwQixTQUFPLDJDQUFrQkEsSUFBbEIsRUFBd0JLLGFBQXhCLENBQVA7QUFDRDs7QUFFRCxTQUFTQyxVQUFULENBQW9CTixJQUFwQixFQUEwQjtBQUN4QixTQUFPRSxNQUFNLENBQUNGLElBQUQsQ0FBTixHQUFlQSxJQUFJLENBQUNBLElBQXBCLEdBQTJCQSxJQUFsQztBQUNEOztBQUVELFNBQVNPLDhCQUFULENBQXdDWCxFQUF4QyxRQUFrRTtBQUFBLE1BQXBCWSxnQkFBb0IsUUFBcEJBLGdCQUFvQjs7QUFDaEUsTUFBSSxDQUFDLHlCQUFXWixFQUFYLENBQUwsRUFBcUI7QUFDbkIsV0FBT0EsRUFBUDtBQUNEOztBQUgrRCxNQUsxRGEsUUFMMEQsR0FLN0NiLEVBQUUsQ0FBQ2MsS0FMMEMsQ0FLMURELFFBTDBEOztBQU9oRSxNQUFJRCxnQkFBSixFQUFzQjtBQUFBLFFBQ1pHLFFBRFksR0FDQ2YsRUFBRSxDQUFDYyxLQURKLENBQ1pDLFFBRFk7QUFFcEJGLElBQUFBLFFBQVEsR0FBR0csdUJBQXVCLENBQUNILFFBQUQsRUFBV0UsUUFBWCxDQUFsQztBQUNEOztBQUVELE1BQU1FLG1CQUFtQixHQUFHLFNBQXRCQSxtQkFBc0IsQ0FBQ0gsS0FBRDtBQUFBLHdCQUFXSSxrQkFBTUMsYUFBTixDQUNyQ25CLEVBQUUsQ0FBQ0ksSUFEa0Msa0NBRWhDSixFQUFFLENBQUNjLEtBRjZCLEdBRW5CQSxLQUZtQixHQUdyQ0QsUUFIcUMsQ0FBWDtBQUFBLEdBQTVCOztBQUtBLHNCQUFPSyxrQkFBTUMsYUFBTixDQUFvQkYsbUJBQXBCLEVBQXlDLElBQXpDLEVBQStDSixRQUEvQyxDQUFQO0FBQ0Q7O0FBRUQsU0FBU08sYUFBVCxDQUF1QnBCLEVBQXZCLEVBQTJCO0FBQ3pCLE1BQUksQ0FBQyx1QkFBU0EsRUFBVCxDQUFMLEVBQW1CO0FBQ2pCLFdBQU8sdUNBQWtCQSxFQUFsQixFQUFzQm9CLGFBQXRCLENBQVA7QUFDRDs7QUFId0IsTUFLakJQLFFBTGlCLEdBS1diLEVBTFgsQ0FLakJhLFFBTGlCO0FBQUEsTUFLUFEsYUFMTyxHQUtXckIsRUFMWCxDQUtQcUIsYUFMTztBQU16QixNQUFNUCxLQUFLLEdBQUc7QUFBRUQsSUFBQUEsUUFBUSxFQUFSQSxRQUFGO0FBQVlRLElBQUFBLGFBQWEsRUFBYkE7QUFBWixHQUFkO0FBRUEsU0FBTztBQUNMQyxJQUFBQSxRQUFRLEVBQUUsUUFETDtBQUVMbEIsSUFBQUEsSUFBSSxFQUFFQyxlQUZEO0FBR0xTLElBQUFBLEtBQUssRUFBTEEsS0FISztBQUlMUyxJQUFBQSxHQUFHLEVBQUUsOENBQXFCdkIsRUFBRSxDQUFDdUIsR0FBeEIsQ0FKQTtBQUtMQyxJQUFBQSxHQUFHLEVBQUV4QixFQUFFLENBQUN3QixHQUFILElBQVUsSUFMVjtBQU1MQyxJQUFBQSxRQUFRLEVBQUUsSUFOTDtBQU9MQyxJQUFBQSxRQUFRLEVBQUVOLGFBQWEsQ0FBQ3BCLEVBQUUsQ0FBQ2EsUUFBSjtBQVBsQixHQUFQO0FBU0Q7O0FBRUQsU0FBU2MsT0FBVCxDQUFnQkMsS0FBaEIsRUFBdUI7QUFDckIsTUFBSUEsS0FBSyxJQUFJLElBQWIsRUFBbUI7QUFDakIsV0FBTyxJQUFQO0FBQ0QsR0FIb0IsQ0FJckI7QUFDQTtBQUNBOzs7QUFDQSxNQUFNdkMsSUFBSSxHQUFHLCtDQUE4QnVDLEtBQTlCLENBQWI7O0FBQ0EsVUFBUXZDLElBQUksQ0FBQ3dDLEdBQWI7QUFDRSxTQUFLNUMsU0FBUyxDQUFDNkMsUUFBZjtBQUNFLGFBQU9DLGNBQWMsQ0FBQzFDLElBQUksQ0FBQzJDLEtBQU4sQ0FBckI7O0FBQ0YsU0FBSy9DLFNBQVMsQ0FBQ2dELFVBQWY7QUFBMkI7QUFBQSxZQUVWWixhQUZVLEdBSXJCaEMsSUFKcUIsQ0FFdkI2QyxTQUZ1QixDQUVWYixhQUZVO0FBQUEsWUFHUlIsUUFIUSxHQUlyQnhCLElBSnFCLENBR3ZCOEMsYUFIdUI7QUFLekIsWUFBTXJCLEtBQUssR0FBRztBQUFFTyxVQUFBQSxhQUFhLEVBQWJBLGFBQUY7QUFBaUJSLFVBQUFBLFFBQVEsRUFBUkE7QUFBakIsU0FBZDtBQUNBLGVBQU87QUFDTFMsVUFBQUEsUUFBUSxFQUFFLFFBREw7QUFFTGxCLFVBQUFBLElBQUksRUFBRUMsZUFGRDtBQUdMUyxVQUFBQSxLQUFLLEVBQUxBLEtBSEs7QUFJTFMsVUFBQUEsR0FBRyxFQUFFLDhDQUFxQmxDLElBQUksQ0FBQ2tDLEdBQTFCLENBSkE7QUFLTEMsVUFBQUEsR0FBRyxFQUFFbkMsSUFBSSxDQUFDbUMsR0FMTDtBQU1MQyxVQUFBQSxRQUFRLEVBQUUsSUFOTDtBQU9MQyxVQUFBQSxRQUFRLEVBQUVLLGNBQWMsQ0FBQzFDLElBQUksQ0FBQzJDLEtBQU47QUFQbkIsU0FBUDtBQVNEOztBQUNELFNBQUsvQyxTQUFTLENBQUNtRCxjQUFmO0FBQ0UsYUFBTztBQUNMZCxRQUFBQSxRQUFRLEVBQUUsT0FETDtBQUVMbEIsUUFBQUEsSUFBSSxFQUFFZixJQUFJLENBQUNlLElBRk47QUFHTFUsUUFBQUEsS0FBSyxvQkFBT3pCLElBQUksQ0FBQzhDLGFBQVosQ0FIQTtBQUlMWixRQUFBQSxHQUFHLEVBQUUsOENBQXFCbEMsSUFBSSxDQUFDa0MsR0FBMUIsQ0FKQTtBQUtMQyxRQUFBQSxHQUFHLEVBQUVuQyxJQUFJLENBQUNtQyxHQUxMO0FBTUxDLFFBQUFBLFFBQVEsRUFBRXBDLElBQUksQ0FBQzZDLFNBTlY7QUFPTFIsUUFBQUEsUUFBUSxFQUFFSyxjQUFjLENBQUMxQyxJQUFJLENBQUMyQyxLQUFOO0FBUG5CLE9BQVA7O0FBU0YsU0FBSy9DLFNBQVMsQ0FBQ29ELG1CQUFmO0FBQ0UsYUFBTztBQUNMZixRQUFBQSxRQUFRLEVBQUUsVUFETDtBQUVMbEIsUUFBQUEsSUFBSSxFQUFFZixJQUFJLENBQUNlLElBRk47QUFHTFUsUUFBQUEsS0FBSyxvQkFBT3pCLElBQUksQ0FBQzhDLGFBQVosQ0FIQTtBQUlMWixRQUFBQSxHQUFHLEVBQUUsOENBQXFCbEMsSUFBSSxDQUFDa0MsR0FBMUIsQ0FKQTtBQUtMQyxRQUFBQSxHQUFHLEVBQUVuQyxJQUFJLENBQUNtQyxHQUxMO0FBTUxDLFFBQUFBLFFBQVEsRUFBRSxJQU5MO0FBT0xDLFFBQUFBLFFBQVEsRUFBRUssY0FBYyxDQUFDMUMsSUFBSSxDQUFDMkMsS0FBTjtBQVBuQixPQUFQOztBQVNGLFNBQUsvQyxTQUFTLENBQUNxRCxTQUFmO0FBQ0UsYUFBTztBQUNMaEIsUUFBQUEsUUFBUSxFQUFFLE9BREw7QUFFTGxCLFFBQUFBLElBQUksRUFBRWYsSUFBSSxDQUFDa0QsV0FBTCxDQUFpQm5DLElBRmxCO0FBR0xVLFFBQUFBLEtBQUssb0JBQU96QixJQUFJLENBQUM4QyxhQUFaLENBSEE7QUFJTFosUUFBQUEsR0FBRyxFQUFFLDhDQUFxQmxDLElBQUksQ0FBQ2tDLEdBQTFCLENBSkE7QUFLTEMsUUFBQUEsR0FBRyxFQUFFbkMsSUFBSSxDQUFDbUMsR0FMTDtBQU1MQyxRQUFBQSxRQUFRLEVBQUVwQyxJQUFJLENBQUM2QyxTQU5WO0FBT0xSLFFBQUFBLFFBQVEsRUFBRUssY0FBYyxDQUFDMUMsSUFBSSxDQUFDMkMsS0FBTCxDQUFXQSxLQUFaO0FBUG5CLE9BQVA7O0FBU0YsU0FBSy9DLFNBQVMsQ0FBQ3VELE9BQWY7QUFBd0I7QUFDdEIsWUFBSUMsYUFBYSxHQUFHakQsT0FBTyxDQUFDTixvQkFBb0IsQ0FBQ0csSUFBSSxDQUFDMkMsS0FBTixDQUFwQixDQUFpQ1UsR0FBakMsQ0FBcUNmLE9BQXJDLENBQUQsQ0FBM0I7O0FBQ0EsWUFBSWMsYUFBYSxDQUFDNUMsTUFBZCxLQUF5QixDQUE3QixFQUFnQztBQUM5QjRDLFVBQUFBLGFBQWEsR0FBRyxDQUFDcEQsSUFBSSxDQUFDOEMsYUFBTCxDQUFtQnRCLFFBQXBCLENBQWhCO0FBQ0Q7O0FBQ0QsZUFBTztBQUNMUyxVQUFBQSxRQUFRLEVBQUUsVUFETDtBQUVMbEIsVUFBQUEsSUFBSSxFQUFFZixJQUFJLENBQUNrRCxXQUZOO0FBR0x6QixVQUFBQSxLQUFLLG9CQUFPekIsSUFBSSxDQUFDOEMsYUFBWixDQUhBO0FBSUxaLFVBQUFBLEdBQUcsRUFBRSw4Q0FBcUJsQyxJQUFJLENBQUNrQyxHQUExQixDQUpBO0FBS0xDLFVBQUFBLEdBQUcsRUFBRW5DLElBQUksQ0FBQ21DLEdBTEw7QUFNTEMsVUFBQUEsUUFBUSxFQUFFLElBTkw7QUFPTEMsVUFBQUEsUUFBUSxFQUFFZTtBQVBMLFNBQVA7QUFTRDs7QUFDRCxTQUFLeEQsU0FBUyxDQUFDMEQsYUFBZjtBQUE4QjtBQUM1QixZQUFJRixjQUFhLEdBQUdqRCxPQUFPLENBQUNOLG9CQUFvQixDQUFDRyxJQUFJLENBQUMyQyxLQUFOLENBQXBCLENBQWlDVSxHQUFqQyxDQUFxQ2YsT0FBckMsQ0FBRCxDQUEzQjs7QUFDQSxZQUFJYyxjQUFhLENBQUM1QyxNQUFkLEtBQXlCLENBQTdCLEVBQWdDO0FBQzlCNEMsVUFBQUEsY0FBYSxHQUFHLENBQUNwRCxJQUFJLENBQUM4QyxhQUFMLENBQW1CdEIsUUFBcEIsQ0FBaEI7QUFDRDs7QUFDRCxlQUFPO0FBQ0xTLFVBQUFBLFFBQVEsRUFBRSxNQURMO0FBRUxsQixVQUFBQSxJQUFJLEVBQUVmLElBQUksQ0FBQ2UsSUFGTjtBQUdMVSxVQUFBQSxLQUFLLG9CQUFPekIsSUFBSSxDQUFDOEMsYUFBWixDQUhBO0FBSUxaLFVBQUFBLEdBQUcsRUFBRSw4Q0FBcUJsQyxJQUFJLENBQUNrQyxHQUExQixDQUpBO0FBS0xDLFVBQUFBLEdBQUcsRUFBRW5DLElBQUksQ0FBQ21DLEdBTEw7QUFNTEMsVUFBQUEsUUFBUSxFQUFFcEMsSUFBSSxDQUFDNkMsU0FOVjtBQU9MUixVQUFBQSxRQUFRLEVBQUVlO0FBUEwsU0FBUDtBQVNEOztBQUNELFNBQUt4RCxTQUFTLENBQUMyRCxRQUFmO0FBQ0UsYUFBT3ZELElBQUksQ0FBQzhDLGFBQVo7O0FBQ0YsU0FBS2xELFNBQVMsQ0FBQzRELFFBQWY7QUFDQSxTQUFLNUQsU0FBUyxDQUFDNkQsSUFBZjtBQUNBLFNBQUs3RCxTQUFTLENBQUM4RCxlQUFmO0FBQ0EsU0FBSzlELFNBQVMsQ0FBQytELGVBQWY7QUFDRSxhQUFPakIsY0FBYyxDQUFDMUMsSUFBSSxDQUFDMkMsS0FBTixDQUFyQjs7QUFDRixTQUFLL0MsU0FBUyxDQUFDZ0UsUUFBZjtBQUNBLFNBQUtoRSxTQUFTLENBQUNpRSxVQUFmO0FBQTJCO0FBQ3pCLGVBQU87QUFDTDVCLFVBQUFBLFFBQVEsRUFBRSxVQURMO0FBRUxsQixVQUFBQSxJQUFJLEVBQUVmLElBQUksQ0FBQ2UsSUFGTjtBQUdMVSxVQUFBQSxLQUFLLG9CQUFPekIsSUFBSSxDQUFDOEQsWUFBWixDQUhBO0FBSUw1QixVQUFBQSxHQUFHLEVBQUUsOENBQXFCbEMsSUFBSSxDQUFDa0MsR0FBMUIsQ0FKQTtBQUtMQyxVQUFBQSxHQUFHLEVBQUVuQyxJQUFJLENBQUNtQyxHQUxMO0FBTUxDLFVBQUFBLFFBQVEsRUFBRSxJQU5MO0FBT0xDLFVBQUFBLFFBQVEsRUFBRUssY0FBYyxDQUFDMUMsSUFBSSxDQUFDMkMsS0FBTjtBQVBuQixTQUFQO0FBU0Q7O0FBQ0QsU0FBSy9DLFNBQVMsQ0FBQ21FLFFBQWY7QUFBeUI7QUFDdkIsZUFBTztBQUNMOUIsVUFBQUEsUUFBUSxFQUFFLFVBREw7QUFFTGxCLFVBQUFBLElBQUksRUFBRWdELGlCQUZEO0FBR0x0QyxVQUFBQSxLQUFLLG9CQUFPekIsSUFBSSxDQUFDOEMsYUFBWixDQUhBO0FBSUxaLFVBQUFBLEdBQUcsRUFBRSw4Q0FBcUJsQyxJQUFJLENBQUNrQyxHQUExQixDQUpBO0FBS0xDLFVBQUFBLEdBQUcsRUFBRW5DLElBQUksQ0FBQ21DLEdBTEw7QUFNTEMsVUFBQUEsUUFBUSxFQUFFLElBTkw7QUFPTEMsVUFBQUEsUUFBUSxFQUFFSyxjQUFjLENBQUMxQyxJQUFJLENBQUMyQyxLQUFOO0FBUG5CLFNBQVA7QUFTRDs7QUFDRCxTQUFLL0MsU0FBUyxDQUFDd0IsSUFBZjtBQUNFLGFBQU9zQixjQUFjLENBQUMxQyxJQUFJLENBQUMyQyxLQUFOLENBQXJCOztBQUNGLFNBQUsvQyxTQUFTLENBQUNvRSxrQkFBZjtBQUNFLGFBQU8xQixPQUFNLENBQUN0QyxJQUFJLENBQUMyQyxLQUFOLENBQWI7O0FBQ0Y7QUFDRSxZQUFNLElBQUlzQixLQUFKLHdEQUEwRGpFLElBQUksQ0FBQ3dDLEdBQS9ELEVBQU47QUFsSEo7QUFvSEQ7O0FBRUQsU0FBU0UsY0FBVCxDQUF3QjFDLElBQXhCLEVBQThCO0FBQzVCLE1BQUksQ0FBQ0EsSUFBTCxFQUFXO0FBQ1QsV0FBTyxJQUFQO0FBQ0Q7O0FBQ0QsTUFBTXdCLFFBQVEsR0FBRzNCLG9CQUFvQixDQUFDRyxJQUFELENBQXJDOztBQUNBLE1BQUl3QixRQUFRLENBQUNoQixNQUFULEtBQW9CLENBQXhCLEVBQTJCO0FBQ3pCLFdBQU8sSUFBUDtBQUNEOztBQUNELE1BQUlnQixRQUFRLENBQUNoQixNQUFULEtBQW9CLENBQXhCLEVBQTJCO0FBQ3pCLFdBQU84QixPQUFNLENBQUNkLFFBQVEsQ0FBQyxDQUFELENBQVQsQ0FBYjtBQUNEOztBQUNELFNBQU9yQixPQUFPLENBQUNxQixRQUFRLENBQUM2QixHQUFULENBQWFmLE9BQWIsQ0FBRCxDQUFkO0FBQ0Q7O0FBRUQsU0FBUzRCLGVBQVQsQ0FBd0JDLEtBQXhCLEVBQStCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFJbkUsSUFBSSxHQUFHbUUsS0FBWDs7QUFDQSxTQUFPbkUsSUFBSSxJQUFJLENBQUNZLEtBQUssQ0FBQ0MsT0FBTixDQUFjYixJQUFkLENBQVQsSUFBZ0NBLElBQUksQ0FBQ29DLFFBQUwsS0FBa0IsSUFBekQsRUFBK0Q7QUFDN0RwQyxJQUFBQSxJQUFJLEdBQUdBLElBQUksQ0FBQ3FDLFFBQVo7QUFDRCxHQVQ0QixDQVU3Qjs7O0FBQ0EsTUFBSSxDQUFDckMsSUFBTCxFQUFXO0FBQ1QsV0FBTyxJQUFQO0FBQ0Q7O0FBRUQsTUFBTW9FLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUNDLElBQUQsRUFBVTtBQUN2QixRQUFJQSxJQUFJLElBQUlBLElBQUksQ0FBQ2pDLFFBQWpCLEVBQTJCLE9BQU9rQyxxQkFBU0MsV0FBVCxDQUFxQkYsSUFBSSxDQUFDakMsUUFBMUIsQ0FBUDtBQUMzQixXQUFPLElBQVA7QUFDRCxHQUhEOztBQUlBLE1BQUl4QixLQUFLLENBQUNDLE9BQU4sQ0FBY2IsSUFBZCxDQUFKLEVBQXlCO0FBQ3ZCLFdBQU9BLElBQUksQ0FBQ3FELEdBQUwsQ0FBU2UsTUFBVCxDQUFQO0FBQ0Q7O0FBQ0QsTUFBSXhELEtBQUssQ0FBQ0MsT0FBTixDQUFjYixJQUFJLENBQUNxQyxRQUFuQixLQUFnQ3JDLElBQUksQ0FBQ2lDLFFBQUwsS0FBa0IsT0FBdEQsRUFBK0Q7QUFDN0QsV0FBT2pDLElBQUksQ0FBQ3FDLFFBQUwsQ0FBY2dCLEdBQWQsQ0FBa0JlLE1BQWxCLENBQVA7QUFDRDs7QUFDRCxTQUFPQSxNQUFNLENBQUNwRSxJQUFELENBQWI7QUFDRDs7QUFFRCxTQUFTMkIsdUJBQVQsQ0FBaUMzQixJQUFqQyxFQUF1QzBCLFFBQXZDLEVBQWlEO0FBQy9DLE1BQUksQ0FBQzFCLElBQUwsRUFBVztBQUNULFdBQU8sSUFBUDtBQUNEOztBQUNELE1BQUlZLEtBQUssQ0FBQ0MsT0FBTixDQUFjYixJQUFkLENBQUosRUFBeUI7QUFDdkIsV0FBT0EsSUFBSSxDQUFDcUQsR0FBTCxDQUFTLFVBQUMxQyxFQUFEO0FBQUEsYUFBUWdCLHVCQUF1QixDQUFDaEIsRUFBRCxFQUFLZSxRQUFMLENBQS9CO0FBQUEsS0FBVCxDQUFQO0FBQ0Q7O0FBQ0QsTUFBSVAsTUFBTSxDQUFDbkIsSUFBSSxDQUFDZSxJQUFOLENBQVYsRUFBdUI7QUFDckIsV0FBT1csUUFBUDtBQUNEOztBQUNELHlDQUNLMUIsSUFETDtBQUVFeUIsSUFBQUEsS0FBSyxrQ0FDQXpCLElBQUksQ0FBQ3lCLEtBREw7QUFFSEQsTUFBQUEsUUFBUSxFQUFFRyx1QkFBdUIsQ0FBQzNCLElBQUksQ0FBQ3lCLEtBQUwsQ0FBV0QsUUFBWixFQUFzQkUsUUFBdEI7QUFGOUI7QUFGUDtBQU9EOztBQUVELElBQU04QyxZQUFZLEdBQUc7QUFDbkJDLEVBQUFBLFNBQVMsRUFBRSxJQURRO0FBRW5CQyxFQUFBQSxhQUFhLEVBQUUsSUFGSTtBQUduQkMsRUFBQUEsUUFBUSxFQUFFO0FBSFMsQ0FBckI7O0FBTUEsU0FBU0Msa0JBQVQsR0FBOEI7QUFDNUI7QUFDQTtBQUNBO0FBSDRCLE1BS3RCQyxVQUxzQjtBQUFBOztBQUFBOztBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsK0JBTWpCO0FBQ1AsZUFBTyxJQUFQO0FBQ0Q7QUFSeUI7O0FBQUE7QUFBQSxJQUtIaEQsa0JBQU1pRCxTQUxIOztBQVU1QixNQUFNQyxZQUFZLEdBQUcsSUFBSUMsbUJBQUosRUFBckI7QUFDQUQsRUFBQUEsWUFBWSxDQUFDRSxNQUFiLGVBQW9CcEQsa0JBQU1DLGFBQU4sQ0FBb0IrQyxVQUFwQixDQUFwQjtBQUNBLFNBQU9FLFlBQVksQ0FBQ0csU0FBYixDQUF1QkMsS0FBOUI7QUFDRDs7QUFFRCxTQUFTQyxPQUFULENBQWlCQyxFQUFqQixFQUFxQjtBQUNuQixNQUFJQyxTQUFKOztBQUNBQyx3QkFBVUMsR0FBVixDQUFjLFlBQU07QUFBRUYsSUFBQUEsU0FBUyxHQUFHRCxFQUFFLEVBQWQ7QUFBbUIsR0FBekM7O0FBQ0EsU0FBT0MsU0FBUDtBQUNEOztBQUVELFNBQVNHLHVCQUFULENBQWlDQyxRQUFqQyxFQUEyQztBQUN6QztBQUNBLE1BQUksbUJBQW1CQSxRQUFRLENBQUNDLFFBQWhDLEVBQTBDO0FBQ3hDLFdBQU9ELFFBQVEsQ0FBQ0MsUUFBVCxDQUFrQkMsYUFBekI7QUFDRDs7QUFDRCxNQUFJLG1CQUFtQkYsUUFBUSxDQUFDQyxRQUFoQyxFQUEwQztBQUN4QyxXQUFPRCxRQUFRLENBQUNDLFFBQVQsQ0FBa0JFLGFBQXpCO0FBQ0Q7O0FBQ0QsUUFBTSxJQUFJNUIsS0FBSixDQUFVLDZFQUFWLENBQU47QUFDRDs7QUFFRCxTQUFTNkIsZUFBVCxDQUF5Qi9FLElBQXpCLEVBQStCO0FBQzdCLFNBQU87QUFBRWdGLElBQUFBLFFBQVEsRUFBRUMsZ0JBQVo7QUFBcUJqRixJQUFBQSxJQUFJLEVBQUpBO0FBQXJCLEdBQVA7QUFDRDs7QUFFRCxTQUFTa0YsVUFBVCxDQUFvQm5CLFNBQXBCLEVBQStCO0FBQzdCLFNBQU9BLFNBQVMsQ0FBQ29CLFNBQVYsS0FDTHBCLFNBQVMsQ0FBQ29CLFNBQVYsQ0FBb0JDLGdCQUFwQixJQUNHdkYsS0FBSyxDQUFDQyxPQUFOLENBQWNpRSxTQUFTLENBQUNzQixvQkFBeEIsQ0FGRSxDQUU0QztBQUY1QyxHQUFQO0FBSUQ7O0lBRUtDLHFCOzs7OztBQUNKLG1DQUFjO0FBQUE7O0FBQUE7O0FBQ1o7QUFEWSxRQUVKQyxVQUZJLEdBRVcsTUFBS0MsT0FGaEIsQ0FFSkQsVUFGSTtBQUdaLFVBQUtDLE9BQUwsbUNBQ0ssTUFBS0EsT0FEVjtBQUVFQyxNQUFBQSxrQ0FBa0MsRUFBRSxJQUZ0QztBQUU0QztBQUMxQ0MsTUFBQUEsaUJBQWlCLEVBQUUsUUFIckI7QUFJRUgsTUFBQUEsVUFBVSxrQ0FDTEEsVUFESztBQUVSSSxRQUFBQSxrQkFBa0IsRUFBRTtBQUNsQkMsVUFBQUEsVUFBVSxFQUFFO0FBRE0sU0FGWjtBQUtSQyxRQUFBQSx3QkFBd0IsRUFBRTtBQUN4QkMsVUFBQUEsMkJBQTJCLEVBQUU7QUFETCxTQUxsQjtBQVFSQyxRQUFBQSx1QkFBdUIsRUFBRSxJQVJqQjtBQVNSQyxRQUFBQSxRQUFRLEVBQUU7QUFDUkMsVUFBQUEsZ0NBQWdDLEVBQUU7QUFEMUIsU0FURjtBQVlSQyxRQUFBQSxlQUFlLEVBQUU7QUFDZkMsVUFBQUEsZ0JBQWdCLEVBQUU7QUFESCxTQVpUO0FBZVJDLFFBQUFBLHdCQUF3QixFQUFFO0FBZmxCO0FBSlo7QUFIWTtBQXlCYjs7Ozt3Q0FFbUJaLE8sRUFBUztBQUMzQixrREFBbUIsT0FBbkI7O0FBQ0EsVUFBSSxxQkFBSUEsT0FBSixFQUFhLGtCQUFiLENBQUosRUFBc0M7QUFDcEMsY0FBTSxJQUFJYSxTQUFKLENBQWMsNkRBQWQsQ0FBTjtBQUNEOztBQUNELFVBQUl4SCxTQUFTLEtBQUssSUFBbEIsRUFBd0I7QUFDdEI7QUFDQUEsUUFBQUEsU0FBUyxHQUFHLGtDQUFaO0FBQ0Q7O0FBUjBCLFVBU25CeUgsUUFUbUIsR0FTNkJkLE9BVDdCLENBU25CYyxRQVRtQjtBQUFBLFVBU1RDLFNBVFMsR0FTNkJmLE9BVDdCLENBU1RlLFNBVFM7QUFBQSxVQVNFQyxzQkFURixHQVM2QmhCLE9BVDdCLENBU0VnQixzQkFURjtBQVUzQixVQUFNQyxPQUFPLEdBQUdGLFNBQVMsSUFBSUQsUUFBYixJQUF5QkksTUFBTSxDQUFDQyxRQUFQLENBQWdCNUYsYUFBaEIsQ0FBOEIsS0FBOUIsQ0FBekM7QUFDQSxVQUFJTSxRQUFRLEdBQUcsSUFBZjtBQUNBLFVBQU11RixPQUFPLEdBQUcsSUFBaEI7QUFDQSxhQUFPO0FBQ0wxQyxRQUFBQSxNQURLLGtCQUNFdEUsRUFERixFQUNNaUgsT0FETixFQUNlQyxRQURmLEVBQ3lCO0FBQzVCLGlCQUFPekMsT0FBTyxDQUFDLFlBQU07QUFDbkIsZ0JBQUloRCxRQUFRLEtBQUssSUFBakIsRUFBdUI7QUFBQSxrQkFDYnJCLElBRGEsR0FDUUosRUFEUixDQUNiSSxJQURhO0FBQUEsa0JBQ1BVLEtBRE8sR0FDUWQsRUFEUixDQUNQYyxLQURPO0FBQUEsa0JBQ0FVLEdBREEsR0FDUXhCLEVBRFIsQ0FDQXdCLEdBREE7O0FBRXJCLGtCQUFNMkYsWUFBWTtBQUNoQmhELGdCQUFBQSxTQUFTLEVBQUUvRCxJQURLO0FBRWhCVSxnQkFBQUEsS0FBSyxFQUFMQSxLQUZnQjtBQUdoQjhGLGdCQUFBQSxzQkFBc0IsRUFBdEJBLHNCQUhnQjtBQUloQkssZ0JBQUFBLE9BQU8sRUFBUEE7QUFKZ0IsaUJBS1p6RixHQUFHLElBQUk7QUFBRTRGLGdCQUFBQSxPQUFPLEVBQUU1RjtBQUFYLGVBTEssQ0FBbEI7O0FBT0Esa0JBQU02RixxQkFBcUIsR0FBRyw0Q0FBbUJySCxFQUFuQixrQ0FBNEI0RixPQUE1QjtBQUFxQ29CLGdCQUFBQSxPQUFPLEVBQVBBO0FBQXJDLGlCQUE5Qjs7QUFDQSxrQkFBTU0sU0FBUyxnQkFBR3BHLGtCQUFNQyxhQUFOLENBQW9Ca0cscUJBQXBCLEVBQTJDRixZQUEzQyxDQUFsQjs7QUFDQTFGLGNBQUFBLFFBQVEsR0FBR2tGLFNBQVMsR0FDaEJoRCxxQkFBUzRELE9BQVQsQ0FBaUJELFNBQWpCLEVBQTRCVCxPQUE1QixDQURnQixHQUVoQmxELHFCQUFTVyxNQUFULENBQWdCZ0QsU0FBaEIsRUFBMkJULE9BQTNCLENBRko7O0FBR0Esa0JBQUksT0FBT0ssUUFBUCxLQUFvQixVQUF4QixFQUFvQztBQUNsQ0EsZ0JBQUFBLFFBQVE7QUFDVDtBQUNGLGFBakJELE1BaUJPO0FBQ0x6RixjQUFBQSxRQUFRLENBQUMrRixhQUFULENBQXVCeEgsRUFBRSxDQUFDYyxLQUExQixFQUFpQ21HLE9BQWpDLEVBQTBDQyxRQUExQztBQUNEO0FBQ0YsV0FyQmEsQ0FBZDtBQXNCRCxTQXhCSTtBQXlCTE8sUUFBQUEsT0F6QksscUJBeUJLO0FBQ1I5RCwrQkFBUytELHNCQUFULENBQWdDYixPQUFoQzs7QUFDQXBGLFVBQUFBLFFBQVEsR0FBRyxJQUFYO0FBQ0QsU0E1Qkk7QUE2QkxrRyxRQUFBQSxPQTdCSyxxQkE2Qks7QUFDUixjQUFJLENBQUNsRyxRQUFMLEVBQWU7QUFDYixtQkFBTyxJQUFQO0FBQ0Q7O0FBQ0QsaUJBQU8sK0NBQ0x1RixPQUFPLENBQUNZLGlCQURILEVBRUxqRyxPQUFNLENBQUNGLFFBQVEsQ0FBQ29HLGVBQVYsQ0FGRCxFQUdMakMsT0FISyxDQUFQO0FBS0QsU0F0Q0k7QUF1Q0xrQyxRQUFBQSxhQXZDSyx5QkF1Q1NDLGFBdkNULEVBdUN3QkMsUUF2Q3hCLEVBdUNrQ0MsS0F2Q2xDLEVBdUN5QztBQUM1QyxjQUFNQyxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLFFBQW9DO0FBQUEsZ0JBQXZCQyxVQUF1QixTQUFqQzFHLFFBQWlDO0FBQUEsZ0JBQVhyQixJQUFXLFNBQVhBLElBQVc7O0FBQzFELGdCQUFJQSxJQUFJLElBQUlBLElBQUksQ0FBQ29HLHdCQUFqQixFQUEyQztBQUN6QyxxQkFBTyxJQUFQO0FBQ0Q7O0FBQ0QsbUJBQU8yQixVQUFVLElBQUlBLFVBQVUsQ0FBQ0MsaUJBQWhDO0FBQ0QsV0FMRDs7QUFENEMsc0JBV3hDTCxhQUFhLENBQUNNLElBQWQsQ0FBbUJILGVBQW5CLEtBQXVDLEVBWEM7QUFBQSxjQVNoQ0ksZ0JBVGdDLFNBUzFDN0csUUFUMEM7QUFBQSxjQVVwQzhHLFlBVm9DLFNBVTFDbkksSUFWMEM7O0FBYTVDLGlEQUNFNkgsS0FERixFQUVFSyxnQkFGRixFQUdFTixRQUhGLEVBSUVELGFBSkYsRUFLRTVILGdCQUxGLEVBTUU2RyxPQUFPLENBQUN3QixpQkFOVixFQU9FRCxZQVBGO0FBU0QsU0E3REk7QUE4RExFLFFBQUFBLGFBOURLLHlCQThEU3BKLElBOURULEVBOERlcUosS0E5RGYsRUE4RHNCQyxJQTlEdEIsRUE4RDRCO0FBQy9CLGNBQU1DLFdBQVcsR0FBRyw2Q0FBb0JGLEtBQXBCLEVBQTJCN0UsWUFBM0IsQ0FBcEI7QUFDQSxjQUFNZ0YsT0FBTyxHQUFHakUsc0JBQVVrRSxRQUFWLENBQW1CRixXQUFuQixDQUFoQjs7QUFDQSxjQUFJLENBQUNDLE9BQUwsRUFBYztBQUNaLGtCQUFNLElBQUlwQyxTQUFKLDJDQUFpRGlDLEtBQWpELHNCQUFOO0FBQ0Q7O0FBQ0RqRSxVQUFBQSxPQUFPLENBQUMsWUFBTTtBQUNab0UsWUFBQUEsT0FBTyxDQUFDN0IsT0FBTyxDQUFDekQsY0FBUixDQUF1QmxFLElBQXZCLENBQUQsRUFBK0JzSixJQUEvQixDQUFQO0FBQ0QsV0FGTSxDQUFQO0FBR0QsU0F2RUk7QUF3RUxJLFFBQUFBLGNBeEVLLDBCQXdFVXJFLEVBeEVWLEVBd0VjO0FBQ2pCLGlCQUFPQSxFQUFFLEVBQVQsQ0FEaUIsQ0FFakI7QUFDRCxTQTNFSTtBQTRFTHNFLFFBQUFBLDRCQTVFSywwQ0E0RTBCO0FBQzdCLGlEQUNLLElBREwsR0FFSywyREFBa0M7QUFDbkNySCxZQUFBQSxNQUFNLEVBQUUsZ0JBQUNzSCxJQUFEO0FBQUEscUJBQVV0SCxPQUFNLENBQUNzSCxJQUFJLENBQUNwQixlQUFOLENBQWhCO0FBQUEsYUFEMkI7QUFFbkNxQixZQUFBQSx1QkFBdUIsRUFBRTtBQUFBLHFCQUFNekgsUUFBTjtBQUFBO0FBRlUsV0FBbEMsQ0FGTDtBQU9ELFNBcEZJO0FBcUZMMEgsUUFBQUEsVUFBVSxFQUFFMUU7QUFyRlAsT0FBUDtBQXVGRDs7OzRDQUVtQztBQUFBOztBQUFBLFVBQWRtQixPQUFjLHVFQUFKLEVBQUk7QUFDbEMsVUFBTW9CLE9BQU8sR0FBRyxJQUFoQjtBQUNBLFVBQU1vQyxRQUFRLEdBQUcsSUFBSS9FLG1CQUFKLEVBQWpCO0FBRmtDLFVBRzFCekQsZ0JBSDBCLEdBR0xnRixPQUhLLENBRzFCaEYsZ0JBSDBCOztBQUlsQyxVQUFJLE9BQU9BLGdCQUFQLEtBQTRCLFdBQTVCLElBQTJDLE9BQU9BLGdCQUFQLEtBQTRCLFNBQTNFLEVBQXNGO0FBQ3BGLGNBQU02RixTQUFTLENBQUMsMkRBQUQsQ0FBZjtBQUNEOztBQUNELFVBQUk0QyxLQUFLLEdBQUcsS0FBWjtBQUNBLFVBQUlDLFVBQVUsR0FBRyxJQUFqQjtBQUVBLFVBQUlDLGFBQWEsR0FBRyxJQUFwQjtBQUNBLFVBQUlDLGdCQUFnQixHQUFHLElBQXZCO0FBQ0EsVUFBTUMsUUFBUSxHQUFHLEVBQWpCLENBWmtDLENBY2xDOztBQUNBLFVBQU1DLGlCQUFpQixHQUFHLFNBQXBCQSxpQkFBb0IsQ0FBQ3ZGLFNBQUQsRUFBWXdGLE9BQVosRUFBd0I7QUFDaEQsWUFBSUosYUFBYSxLQUFLcEYsU0FBdEIsRUFBaUM7QUFDL0IsY0FBSW1CLFVBQVUsQ0FBQ25CLFNBQUQsQ0FBZCxFQUEyQjtBQUN6QnFGLFlBQUFBLGdCQUFnQjtBQUFBOztBQUFBOztBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUFBQTtBQUFBLGNBQWlCckYsU0FBakIsQ0FBaEI7O0FBQ0EsZ0JBQUl3RixPQUFKLEVBQWE7QUFDWEgsY0FBQUEsZ0JBQWdCLENBQUNqRSxTQUFqQixDQUEyQnFFLHFCQUEzQixHQUFtRCxVQUFDQyxTQUFEO0FBQUEsdUJBQ2pELENBQUNGLE9BQU8sQ0FBQyxNQUFJLENBQUM3SSxLQUFOLEVBQWErSSxTQUFiLENBRHlDO0FBQUEsZUFBbkQ7QUFHRCxhQUpELE1BSU87QUFDTEwsY0FBQUEsZ0JBQWdCLENBQUNqRSxTQUFqQixDQUEyQnVFLG9CQUEzQixHQUFrRCxJQUFsRDtBQUNEO0FBQ0YsV0FURCxNQVNPO0FBQ0wsZ0JBQUlDLFFBQVEsR0FBR04sUUFBZjtBQUNBLGdCQUFJTyxTQUFKOztBQUNBUixZQUFBQSxnQkFBZ0IsR0FBRyxTQUFTUyxrQkFBVCxDQUE0Qm5KLEtBQTVCLEVBQTRDO0FBQzdELGtCQUFNb0osWUFBWSxHQUFHSCxRQUFRLEtBQUtOLFFBQWIsS0FBMEJFLE9BQU8sR0FDbEQsQ0FBQ0EsT0FBTyxDQUFDSyxTQUFELEVBQVlsSixLQUFaLENBRDBDLEdBRWxELENBQUMsb0NBQWFrSixTQUFiLEVBQXdCbEosS0FBeEIsQ0FGZ0IsQ0FBckI7O0FBSUEsa0JBQUlvSixZQUFKLEVBQWtCO0FBQUEsa0RBTHFDQyxJQUtyQztBQUxxQ0Esa0JBQUFBLElBS3JDO0FBQUE7O0FBQ2hCSixnQkFBQUEsUUFBUSxHQUFHNUYsU0FBUyxNQUFULDBDQUFlQSxTQUFTLENBQUNpRyxZQUF6QixHQUEwQ3RKLEtBQTFDLFVBQXNEcUosSUFBdEQsRUFBWDtBQUNBSCxnQkFBQUEsU0FBUyxHQUFHbEosS0FBWjtBQUNEOztBQUNELHFCQUFPaUosUUFBUDtBQUNELGFBVkQ7QUFXRDs7QUFDRCxrQ0FDRVAsZ0JBREYsRUFFRXJGLFNBRkYsRUFHRTtBQUFFa0csWUFBQUEsV0FBVyxFQUFFckQsT0FBTyxDQUFDd0IsaUJBQVIsQ0FBMEI7QUFBRXBJLGNBQUFBLElBQUksRUFBRStEO0FBQVIsYUFBMUI7QUFBZixXQUhGO0FBS0FvRixVQUFBQSxhQUFhLEdBQUdwRixTQUFoQjtBQUNEOztBQUNELGVBQU9xRixnQkFBUDtBQUNELE9BbENELENBZmtDLENBbURsQztBQUNBOzs7QUFDQSxVQUFNYyx1QkFBdUIsR0FBRyxTQUExQkEsdUJBQTBCLENBQUNuRyxTQUFELEVBQWU7QUFDN0MsWUFBSSxxQkFBSUEsU0FBSixFQUFlLGNBQWYsQ0FBSixFQUFvQztBQUNsQyxjQUFJb0YsYUFBYSxLQUFLcEYsU0FBdEIsRUFBaUM7QUFDL0JxRixZQUFBQSxnQkFBZ0IsR0FBRyx5QkFDakI7QUFDQSxzQkFBQzFJLEtBQUQ7QUFBQSxpREFBV3FKLElBQVg7QUFBV0EsZ0JBQUFBLElBQVg7QUFBQTs7QUFBQSxxQkFBb0JoRyxTQUFTLE1BQVQsMENBQWVBLFNBQVMsQ0FBQ2lHLFlBQXpCLEdBQTBDdEosS0FBMUMsVUFBc0RxSixJQUF0RCxFQUFwQjtBQUFBLGFBRmlCLEVBR2pCaEcsU0FIaUIsRUFJakI7QUFBRWtHLGNBQUFBLFdBQVcsRUFBRXJELE9BQU8sQ0FBQ3dCLGlCQUFSLENBQTBCO0FBQUVwSSxnQkFBQUEsSUFBSSxFQUFFK0Q7QUFBUixlQUExQjtBQUFmLGFBSmlCLENBQW5CO0FBTUFvRixZQUFBQSxhQUFhLEdBQUdwRixTQUFoQjtBQUNEOztBQUNELGlCQUFPcUYsZ0JBQVA7QUFDRDs7QUFFRCxlQUFPckYsU0FBUDtBQUNELE9BZkQ7O0FBaUJBLFVBQU1vRyxhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLENBQUNDLFFBQUQsRUFBdUI7QUFBQSwyQ0FBVEMsSUFBUztBQUFUQSxVQUFBQSxJQUFTO0FBQUE7O0FBQzNDLFlBQU1DLFVBQVUsR0FBR3RCLFFBQVEsQ0FBQzlFLE1BQVQsT0FBQThFLFFBQVEsR0FBUW9CLFFBQVIsU0FBcUJDLElBQXJCLEVBQTNCO0FBRUEsWUFBTUUsYUFBYSxHQUFHLENBQUMsRUFBRUQsVUFBVSxJQUFJQSxVQUFVLENBQUN0SyxJQUEzQixDQUF2Qjs7QUFDQSxZQUFJdUssYUFBSixFQUFtQjtBQUNqQixjQUFNQyxRQUFRLEdBQUdqSyw4QkFBOEIsQ0FBQytKLFVBQUQsRUFBYTtBQUFFOUosWUFBQUEsZ0JBQWdCLEVBQWhCQTtBQUFGLFdBQWIsQ0FBL0M7QUFFQSxjQUFNaUssZ0JBQWdCLEdBQUdELFFBQVEsQ0FBQ3hLLElBQVQsS0FBa0JzSyxVQUFVLENBQUN0SyxJQUF0RDs7QUFDQSxjQUFJeUssZ0JBQUosRUFBc0I7QUFDcEIsbUJBQU96QixRQUFRLENBQUM5RSxNQUFULE9BQUE4RSxRQUFRLG1DQUFhb0IsUUFBYjtBQUF1QnBLLGNBQUFBLElBQUksRUFBRXdLLFFBQVEsQ0FBQ3hLO0FBQXRDLHVCQUFpRHFLLElBQWpELEVBQWY7QUFDRDtBQUNGOztBQUVELGVBQU9DLFVBQVA7QUFDRCxPQWREOztBQWdCQSxhQUFPO0FBQ0w7QUFDQXBHLFFBQUFBLE1BRkssa0JBRUV0RSxFQUZGLEVBRU04SyxlQUZOLEVBSUc7QUFBQSwwRkFBSixFQUFJO0FBQUEsMkNBRE5DLGNBQ007QUFBQSxjQUROQSxjQUNNLHFDQURXLElBQUlDLEdBQUosRUFDWDs7QUFDTjFCLFVBQUFBLFVBQVUsR0FBR3RKLEVBQWI7O0FBQ0EsY0FBSSxPQUFPQSxFQUFFLENBQUNJLElBQVYsS0FBbUIsUUFBdkIsRUFBaUM7QUFDL0JpSixZQUFBQSxLQUFLLEdBQUcsSUFBUjtBQUNELFdBRkQsTUFFTyxJQUFJLGdDQUFrQnJKLEVBQWxCLENBQUosRUFBMkI7QUFDaEMrSyxZQUFBQSxjQUFjLENBQUNFLEdBQWYsQ0FBbUJqTCxFQUFFLENBQUNJLElBQXRCLEVBQTRCSixFQUFFLENBQUNjLEtBQUgsQ0FBU29LLEtBQXJDO0FBQ0EsZ0JBQU1DLFlBQVksR0FBRyx3QkFDbkIsVUFBQ3JLLEtBQUQ7QUFBQSxxQkFBV0EsS0FBSyxDQUFDRCxRQUFqQjtBQUFBLGFBRG1CLEVBRW5CYixFQUFFLENBQUNJLElBRmdCLENBQXJCO0FBSUEsbUJBQU8sNkNBQW9CO0FBQUEscUJBQU1tSyxhQUFhLGlDQUFNdkssRUFBTjtBQUFVSSxnQkFBQUEsSUFBSSxFQUFFK0s7QUFBaEIsaUJBQW5CO0FBQUEsYUFBcEIsQ0FBUDtBQUNELFdBUE0sTUFPQSxJQUFJLGdDQUFrQm5MLEVBQWxCLENBQUosRUFBMkI7QUFDaEMsZ0JBQU0rRSxRQUFRLEdBQUdpQyxPQUFPLENBQUNvRSx1QkFBUixDQUFnQ3BMLEVBQUUsQ0FBQ0ksSUFBbkMsQ0FBakI7QUFDQSxnQkFBTThLLEtBQUssR0FBR0gsY0FBYyxDQUFDTSxHQUFmLENBQW1CdEcsUUFBbkIsSUFDVmdHLGNBQWMsQ0FBQ08sR0FBZixDQUFtQnZHLFFBQW5CLENBRFUsR0FFVkQsdUJBQXVCLENBQUNDLFFBQUQsQ0FGM0I7QUFHQSxnQkFBTXdHLFlBQVksR0FBRyx3QkFDbkIsVUFBQ3pLLEtBQUQ7QUFBQSxxQkFBV0EsS0FBSyxDQUFDRCxRQUFOLENBQWVxSyxLQUFmLENBQVg7QUFBQSxhQURtQixFQUVuQmxMLEVBQUUsQ0FBQ0ksSUFGZ0IsQ0FBckI7QUFJQSxtQkFBTyw2Q0FBb0I7QUFBQSxxQkFBTW1LLGFBQWEsaUNBQU12SyxFQUFOO0FBQVVJLGdCQUFBQSxJQUFJLEVBQUVtTDtBQUFoQixpQkFBbkI7QUFBQSxhQUFwQixDQUFQO0FBQ0QsV0FWTSxNQVVBO0FBQ0xsQyxZQUFBQSxLQUFLLEdBQUcsS0FBUjtBQUNBLGdCQUFJcUIsVUFBVSxHQUFHMUssRUFBakI7O0FBQ0EsZ0JBQUlRLE1BQU0sQ0FBQ2tLLFVBQUQsQ0FBVixFQUF3QjtBQUN0QixvQkFBTWpFLFNBQVMsQ0FBQyxxREFBRCxDQUFmO0FBQ0Q7O0FBRURpRSxZQUFBQSxVQUFVLEdBQUcvSiw4QkFBOEIsQ0FBQytKLFVBQUQsRUFBYTtBQUFFOUosY0FBQUEsZ0JBQWdCLEVBQWhCQTtBQUFGLGFBQWIsQ0FBM0M7QUFQSyw4QkFRdUI4SixVQVJ2QjtBQUFBLGdCQVFTdkcsU0FSVCxlQVFHL0QsSUFSSDtBQVVMLGdCQUFNNkcsT0FBTyxHQUFHLDBDQUFpQjlDLFNBQVMsQ0FBQ3FILFlBQTNCLEVBQXlDVixlQUF6QyxDQUFoQjs7QUFFQSxnQkFBSXhLLE1BQU0sQ0FBQ04sRUFBRSxDQUFDSSxJQUFKLENBQVYsRUFBcUI7QUFBQSw2QkFDa0JKLEVBQUUsQ0FBQ0ksSUFEckI7QUFBQSxrQkFDTHFMLFNBREssWUFDWHJMLElBRFc7QUFBQSxrQkFDTXVKLE9BRE4sWUFDTUEsT0FETjtBQUduQixxQkFBTyw2Q0FBb0I7QUFBQSx1QkFBTVksYUFBYSxpQ0FDdkN2SyxFQUR1QztBQUNuQ0ksa0JBQUFBLElBQUksRUFBRXNKLGlCQUFpQixDQUFDK0IsU0FBRCxFQUFZOUIsT0FBWjtBQURZLG9CQUU1QzFDLE9BRjRDLENBQW5CO0FBQUEsZUFBcEIsQ0FBUDtBQUlEOztBQUVELGdCQUFNeUUsbUJBQW1CLEdBQUdwRyxVQUFVLENBQUNuQixTQUFELENBQXRDOztBQUVBLGdCQUFJLENBQUN1SCxtQkFBRCxJQUF3QixPQUFPdkgsU0FBUCxLQUFxQixVQUFqRCxFQUE2RDtBQUMzRCxxQkFBTyw2Q0FBb0I7QUFBQSx1QkFBTW9HLGFBQWEsaUNBQ3ZDRyxVQUR1QztBQUMzQnRLLGtCQUFBQSxJQUFJLEVBQUVrSyx1QkFBdUIsQ0FBQ25HLFNBQUQ7QUFERixvQkFFNUM4QyxPQUY0QyxDQUFuQjtBQUFBLGVBQXBCLENBQVA7QUFJRDs7QUFFRCxnQkFBSXlFLG1CQUFKLEVBQXlCO0FBQ3ZCLGtCQUNFdEMsUUFBUSxDQUFDN0UsU0FBVCxJQUNHdkUsRUFBRSxDQUFDYyxLQUFILEtBQWFzSSxRQUFRLENBQUM3RSxTQUFULENBQW1CekQsS0FEbkMsSUFFRyxDQUFDLG9DQUFhbUcsT0FBYixFQUFzQm1DLFFBQVEsQ0FBQzdFLFNBQVQsQ0FBbUIwQyxPQUF6QyxDQUhOLEVBSUU7QUFBQSxpQ0FDb0IsbUNBQ2xCbUMsUUFEa0IsRUFFbEIsdUJBRmtCLEVBR2xCLFVBQUN1QyxjQUFEO0FBQUEseUJBQW9CLFNBQVNDLHFCQUFULEdBQXdDO0FBQUEsd0JBQ2xEOUssS0FEa0QsR0FDeENzSSxRQUFRLENBQUM3RSxTQUQrQixDQUNsRHpELEtBRGtEOztBQUUxRCx3QkFBTStLLFdBQVcscUJBQVEvSyxLQUFSLENBQWpCOztBQUNBc0ksb0JBQUFBLFFBQVEsQ0FBQzdFLFNBQVQsQ0FBbUJ6RCxLQUFuQixHQUEyQitLLFdBQTNCOztBQUgwRCx1REFBTjFCLElBQU07QUFBTkEsc0JBQUFBLElBQU07QUFBQTs7QUFLMUQsd0JBQU16SyxNQUFNLEdBQUdpTSxjQUFjLENBQUNHLEtBQWYsQ0FBcUIxQyxRQUFyQixFQUErQmUsSUFBL0IsQ0FBZjtBQUVBZixvQkFBQUEsUUFBUSxDQUFDN0UsU0FBVCxDQUFtQnpELEtBQW5CLEdBQTJCQSxLQUEzQjtBQUNBaUwsb0JBQUFBLE9BQU87QUFFUCwyQkFBT3JNLE1BQVA7QUFDRCxtQkFYRDtBQUFBLGlCQUhrQixDQURwQjtBQUFBLG9CQUNRcU0sT0FEUixjQUNRQSxPQURSO0FBaUJELGVBdEJzQixDQXdCdkI7OztBQUNBLGtCQUFNQyxlQUFlLEdBQUcvSCxrQkFBa0IsRUFBMUM7O0FBQ0Esa0JBQUkrSCxlQUFKLEVBQXFCO0FBQ25CQyxnQkFBQUEsTUFBTSxDQUFDQyxjQUFQLENBQXNCL0gsU0FBUyxDQUFDb0IsU0FBaEMsRUFBMkMsT0FBM0MsRUFBb0Q7QUFDbEQ0RyxrQkFBQUEsWUFBWSxFQUFFLElBRG9DO0FBRWxEQyxrQkFBQUEsVUFBVSxFQUFFLElBRnNDO0FBR2xEZCxrQkFBQUEsR0FIa0QsaUJBRzVDO0FBQ0osMkJBQU8sSUFBUDtBQUNELG1CQUxpRDtBQU1sREwsa0JBQUFBLEdBTmtELGVBTTlDQyxLQU44QyxFQU12QztBQUNULHdCQUFJQSxLQUFLLEtBQUtjLGVBQWQsRUFBK0I7QUFDN0JDLHNCQUFBQSxNQUFNLENBQUNDLGNBQVAsQ0FBc0IsSUFBdEIsRUFBNEIsT0FBNUIsRUFBcUM7QUFDbkNDLHdCQUFBQSxZQUFZLEVBQUUsSUFEcUI7QUFFbkNDLHdCQUFBQSxVQUFVLEVBQUUsSUFGdUI7QUFHbkNsQix3QkFBQUEsS0FBSyxFQUFMQSxLQUhtQztBQUluQ21CLHdCQUFBQSxRQUFRLEVBQUU7QUFKeUIsdUJBQXJDO0FBTUQ7O0FBQ0QsMkJBQU8sSUFBUDtBQUNEO0FBaEJpRCxpQkFBcEQ7QUFrQkQ7QUFDRjs7QUFDRCxtQkFBTyw2Q0FBb0I7QUFBQSxxQkFBTTlCLGFBQWEsQ0FBQ0csVUFBRCxFQUFhekQsT0FBYixDQUFuQjtBQUFBLGFBQXBCLENBQVA7QUFDRDtBQUNGLFNBeEdJO0FBeUdMUSxRQUFBQSxPQXpHSyxxQkF5R0s7QUFDUjJCLFVBQUFBLFFBQVEsQ0FBQzNCLE9BQVQ7QUFDRCxTQTNHSTtBQTRHTEUsUUFBQUEsT0E1R0sscUJBNEdLO0FBQ1IsY0FBSTBCLEtBQUosRUFBVztBQUNULG1CQUFPakksYUFBYSxDQUFDa0ksVUFBRCxDQUFwQjtBQUNEOztBQUNELGNBQU1nRCxNQUFNLEdBQUdsRCxRQUFRLENBQUNtRCxlQUFULEVBQWY7QUFDQSxpQkFBTztBQUNMakwsWUFBQUEsUUFBUSxFQUFFbkIsZ0JBQWdCLENBQUNtSixVQUFVLENBQUNsSixJQUFaLENBRHJCO0FBRUxBLFlBQUFBLElBQUksRUFBRWtKLFVBQVUsQ0FBQ2xKLElBRlo7QUFHTFUsWUFBQUEsS0FBSyxFQUFFd0ksVUFBVSxDQUFDeEksS0FIYjtBQUlMUyxZQUFBQSxHQUFHLEVBQUUsOENBQXFCK0gsVUFBVSxDQUFDL0gsR0FBaEMsQ0FKQTtBQUtMQyxZQUFBQSxHQUFHLEVBQUU4SCxVQUFVLENBQUM5SCxHQUxYO0FBTUxDLFlBQUFBLFFBQVEsRUFBRTJILFFBQVEsQ0FBQzdFLFNBTmQ7QUFPTDdDLFlBQUFBLFFBQVEsRUFBRXpCLEtBQUssQ0FBQ0MsT0FBTixDQUFjb00sTUFBZCxJQUNOOU0sT0FBTyxDQUFDOE0sTUFBRCxDQUFQLENBQWdCNUosR0FBaEIsQ0FBb0IsVUFBQzFDLEVBQUQ7QUFBQSxxQkFBUW9CLGFBQWEsQ0FBQ3BCLEVBQUQsQ0FBckI7QUFBQSxhQUFwQixDQURNLEdBRU5vQixhQUFhLENBQUNrTCxNQUFEO0FBVFosV0FBUDtBQVdELFNBNUhJO0FBNkhMeEUsUUFBQUEsYUE3SEsseUJBNkhTQyxhQTdIVCxFQTZId0JDLFFBN0h4QixFQTZIa0NDLEtBN0hsQyxFQTZIeUM7QUFDNUMsaURBQ0VBLEtBREYsRUFFRW1CLFFBQVEsQ0FBQzdFLFNBRlgsRUFHRStFLFVBSEYsRUFJRXZCLGFBQWEsQ0FBQ3lFLE1BQWQsQ0FBcUJsRCxVQUFyQixDQUpGLEVBS0VuSixnQkFMRixFQU1FNkcsT0FBTyxDQUFDd0IsaUJBTlYsRUFPRWMsVUFBVSxDQUFDbEosSUFQYjtBQVNELFNBdklJO0FBd0lMcUksUUFBQUEsYUF4SUsseUJBd0lTcEosSUF4SVQsRUF3SWVxSixLQXhJZixFQXdJK0I7QUFBQSw2Q0FBTnlCLElBQU07QUFBTkEsWUFBQUEsSUFBTTtBQUFBOztBQUNsQyxjQUFNc0MsT0FBTyxHQUFHcE4sSUFBSSxDQUFDeUIsS0FBTCxDQUFXLHVDQUFjNEgsS0FBZCxFQUFxQjdFLFlBQXJCLENBQVgsQ0FBaEI7O0FBQ0EsY0FBSTRJLE9BQUosRUFBYTtBQUNYLHlEQUFvQixZQUFNO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBQSxjQUFBQSxPQUFPLE1BQVAsU0FBV3RDLElBQVgsRUFKd0IsQ0FLeEI7QUFDRCxhQU5EO0FBT0Q7QUFDRixTQW5KSTtBQW9KTHBCLFFBQUFBLGNBcEpLLDBCQW9KVXJFLEVBcEpWLEVBb0pjO0FBQ2pCLGlCQUFPQSxFQUFFLEVBQVQsQ0FEaUIsQ0FFakI7QUFDRCxTQXZKSTtBQXdKTGdJLFFBQUFBLGNBeEpLLDBCQXdKVUMsU0F4SlYsRUF3SnFCQyxNQXhKckIsRUF3SjZCQyxRQXhKN0IsRUF3SnVDQyxTQXhKdkMsRUF3SmtEO0FBQ3JELGlCQUFPLGlDQUNMSCxTQURLLEVBRUxDLE1BRkssRUFHTEMsUUFISyxFQUlMLDJDQUFrQnZELFVBQWxCLENBSkssRUFLTDtBQUFBLG1CQUFNLDJDQUFrQndELFNBQVMsQ0FBQ04sTUFBVixDQUFpQixDQUFDbEQsVUFBRCxDQUFqQixDQUFsQixDQUFOO0FBQUEsV0FMSyxDQUFQO0FBT0Q7QUFoS0ksT0FBUDtBQWtLRDs7O3lDQUVvQjFELE8sRUFBUztBQUM1QixVQUFJLHFCQUFJQSxPQUFKLEVBQWEsa0JBQWIsQ0FBSixFQUFzQztBQUNwQyxjQUFNLElBQUlhLFNBQUosQ0FBYywwRUFBZCxDQUFOO0FBQ0Q7O0FBQ0QsYUFBTztBQUNMbkMsUUFBQUEsTUFESyxrQkFDRXRFLEVBREYsRUFDTWlILE9BRE4sRUFDZTtBQUNsQixjQUFJckIsT0FBTyxDQUFDcUIsT0FBUixLQUFvQmpILEVBQUUsQ0FBQ0ksSUFBSCxDQUFRb0wsWUFBUixJQUF3QjVGLE9BQU8sQ0FBQ21ILGlCQUFwRCxDQUFKLEVBQTRFO0FBQzFFLGdCQUFNQSxpQkFBaUIsbUNBQ2pCL00sRUFBRSxDQUFDSSxJQUFILENBQVFvTCxZQUFSLElBQXdCLEVBRFAsR0FFbEI1RixPQUFPLENBQUNtSCxpQkFGVSxDQUF2Qjs7QUFJQSxnQkFBTUMsY0FBYyxHQUFHLDZDQUFvQmhOLEVBQXBCLEVBQXdCaUgsT0FBeEIsRUFBaUM4RixpQkFBakMsQ0FBdkI7QUFDQSxtQkFBT0UsbUJBQWVDLG9CQUFmLGVBQW9DaE0sa0JBQU1DLGFBQU4sQ0FBb0I2TCxjQUFwQixDQUFwQyxDQUFQO0FBQ0Q7O0FBQ0QsaUJBQU9DLG1CQUFlQyxvQkFBZixDQUFvQ2xOLEVBQXBDLENBQVA7QUFDRDtBQVhJLE9BQVA7QUFhRCxLLENBRUQ7QUFDQTtBQUNBOzs7O21DQUNlNEYsTyxFQUFTO0FBQ3RCLGNBQVFBLE9BQU8sQ0FBQ3VILElBQWhCO0FBQ0UsYUFBS0Msc0JBQWNDLEtBQWQsQ0FBb0JDLEtBQXpCO0FBQWdDLGlCQUFPLEtBQUtDLG1CQUFMLENBQXlCM0gsT0FBekIsQ0FBUDs7QUFDaEMsYUFBS3dILHNCQUFjQyxLQUFkLENBQW9CRyxPQUF6QjtBQUFrQyxpQkFBTyxLQUFLQyxxQkFBTCxDQUEyQjdILE9BQTNCLENBQVA7O0FBQ2xDLGFBQUt3SCxzQkFBY0MsS0FBZCxDQUFvQkssTUFBekI7QUFBaUMsaUJBQU8sS0FBS0Msb0JBQUwsQ0FBMEIvSCxPQUExQixDQUFQOztBQUNqQztBQUNFLGdCQUFNLElBQUl0QyxLQUFKLHFEQUF1RHNDLE9BQU8sQ0FBQ3VILElBQS9ELEVBQU47QUFMSjtBQU9EOzs7eUJBRUlTLE8sRUFBUztBQUNaLGFBQU8sOEJBQUtBLE9BQUwsQ0FBUDtBQUNELEssQ0FFRDtBQUNBO0FBQ0E7QUFDQTs7OztrQ0FDY3ZPLEksRUFBTTtBQUNsQixVQUFJLENBQUNBLElBQUQsSUFBUyxRQUFPQSxJQUFQLE1BQWdCLFFBQTdCLEVBQXVDLE9BQU8sSUFBUDtBQURyQixVQUVWZSxJQUZVLEdBRURmLElBRkMsQ0FFVmUsSUFGVTtBQUdsQiwwQkFBT2Msa0JBQU1DLGFBQU4sQ0FBb0JULFVBQVUsQ0FBQ04sSUFBRCxDQUE5QixFQUFzQyw2Q0FBb0JmLElBQXBCLENBQXRDLENBQVA7QUFDRCxLLENBRUQ7Ozs7dUNBQ21CQSxJLEVBQU13TyxZLEVBQWM7QUFDckMsVUFBSSxDQUFDeE8sSUFBTCxFQUFXO0FBQ1QsZUFBT0EsSUFBUDtBQUNEOztBQUhvQyxVQUk3QmUsSUFKNkIsR0FJcEJmLElBSm9CLENBSTdCZSxJQUo2QjtBQUtyQyxhQUFPTSxVQUFVLENBQUNOLElBQUQsQ0FBVixLQUFxQk0sVUFBVSxDQUFDbU4sWUFBRCxDQUF0QztBQUNEOzs7a0NBRWFELE8sRUFBUztBQUNyQixhQUFPeE0sYUFBYSxDQUFDd00sT0FBRCxDQUFwQjtBQUNEOzs7bUNBRWN2TyxJLEVBQTZCO0FBQUEsVUFBdkJ5TyxhQUF1Qix1RUFBUCxLQUFPOztBQUMxQyxVQUFNQyxLQUFLLEdBQUd4SyxlQUFjLENBQUNsRSxJQUFELENBQTVCOztBQUNBLFVBQUlZLEtBQUssQ0FBQ0MsT0FBTixDQUFjNk4sS0FBZCxLQUF3QixDQUFDRCxhQUE3QixFQUE0QztBQUMxQyxlQUFPQyxLQUFLLENBQUMsQ0FBRCxDQUFaO0FBQ0Q7O0FBQ0QsYUFBT0EsS0FBUDtBQUNEOzs7c0NBRWlCMU8sSSxFQUFNO0FBQ3RCLFVBQUksQ0FBQ0EsSUFBTCxFQUFXLE9BQU8sSUFBUDtBQURXLFVBRWRlLElBRmMsR0FFS2YsSUFGTCxDQUVkZSxJQUZjO0FBQUEsVUFFUmdGLFFBRlEsR0FFSy9GLElBRkwsQ0FFUitGLFFBRlE7QUFHdEIsVUFBTTRCLE9BQU8sR0FBRyxJQUFoQjtBQUVBLFVBQU0xRixRQUFRLEdBQUdsQixJQUFJLElBQUlnRixRQUF6QixDQUxzQixDQU90Qjs7QUFDQSxVQUFJOUQsUUFBSixFQUFjO0FBQ1osZ0JBQVFBLFFBQVI7QUFDRSxlQUFLME0sMkJBQWtCQyxHQUF2QjtBQUE0QixtQkFBTyxnQkFBUDs7QUFDNUIsZUFBS3BMLHFCQUFZb0wsR0FBakI7QUFBc0IsbUJBQU8sVUFBUDs7QUFDdEIsZUFBS0MsdUJBQWNELEdBQW5CO0FBQXdCLG1CQUFPLFlBQVA7O0FBQ3hCLGVBQUtoTCxxQkFBWWdMLEdBQWpCO0FBQXNCLG1CQUFPLFVBQVA7O0FBQ3RCLGVBQUs1TixtQkFBVTROLEdBQWY7QUFBb0IsbUJBQU8sUUFBUDs7QUFDcEIsZUFBSzdLLHFCQUFZNkssR0FBakI7QUFBc0IsbUJBQU8sVUFBUDs7QUFDdEI7QUFQRjtBQVNEOztBQUVELFVBQU1FLFlBQVksR0FBRy9OLElBQUksSUFBSUEsSUFBSSxDQUFDZ0YsUUFBbEM7O0FBRUEsY0FBUStJLFlBQVI7QUFDRSxhQUFLbkwsNEJBQW1CaUwsR0FBeEI7QUFBNkIsaUJBQU8saUJBQVA7O0FBQzdCLGFBQUtsTCw0QkFBbUJrTCxHQUF4QjtBQUE2QixpQkFBTyxpQkFBUDs7QUFDN0IsYUFBSzFOLGlCQUFRME4sR0FBYjtBQUFrQjtBQUNoQixnQkFBTUcsUUFBUSxHQUFHLDJDQUFrQi9PLElBQWxCLENBQWpCO0FBQ0EsbUJBQU8sT0FBTytPLFFBQVAsS0FBb0IsUUFBcEIsR0FBK0JBLFFBQS9CLGtCQUFrRHBILE9BQU8sQ0FBQ3dCLGlCQUFSLENBQTBCcEksSUFBMUIsQ0FBbEQsTUFBUDtBQUNEOztBQUNELGFBQUs4Qyx1QkFBYytLLEdBQW5CO0FBQXdCO0FBQ3RCLGdCQUFJN04sSUFBSSxDQUFDaUssV0FBVCxFQUFzQjtBQUNwQixxQkFBT2pLLElBQUksQ0FBQ2lLLFdBQVo7QUFDRDs7QUFDRCxnQkFBTWdFLElBQUksR0FBR3JILE9BQU8sQ0FBQ3dCLGlCQUFSLENBQTBCO0FBQUVwSSxjQUFBQSxJQUFJLEVBQUVBLElBQUksQ0FBQ2tFO0FBQWIsYUFBMUIsQ0FBYjtBQUNBLG1CQUFPK0osSUFBSSx3QkFBaUJBLElBQWpCLFNBQTJCLFlBQXRDO0FBQ0Q7O0FBQ0QsYUFBSzVOLGlCQUFRd04sR0FBYjtBQUFrQjtBQUNoQixtQkFBTyxNQUFQO0FBQ0Q7O0FBQ0Q7QUFBUyxpQkFBTywyQ0FBa0I1TyxJQUFsQixDQUFQO0FBakJYO0FBbUJEOzs7bUNBRWN1TyxPLEVBQVM7QUFDdEIsYUFBTyx3QkFBVUEsT0FBVixDQUFQO0FBQ0Q7Ozt1Q0FFa0JVLE0sRUFBUTtBQUN6QixhQUFPLENBQUMsQ0FBQ0EsTUFBRixJQUFZLGlDQUFtQkEsTUFBbkIsQ0FBbkI7QUFDRDs7OytCQUVVQyxRLEVBQVU7QUFDbkIsYUFBTyx1QkFBV0EsUUFBWCxNQUF5QjFMLGlCQUFoQztBQUNEOzs7c0NBRWlCekMsSSxFQUFNO0FBQ3RCLFVBQU1vTyxXQUFXLEdBQUdySixlQUFlLENBQUMvRSxJQUFELENBQW5DO0FBQ0EsYUFBTyxDQUFDLENBQUNBLElBQUYsS0FDTCxPQUFPQSxJQUFQLEtBQWdCLFVBQWhCLElBQ0csMkJBQWFvTyxXQUFiLENBREgsSUFFRyxnQ0FBa0JBLFdBQWxCLENBRkgsSUFHRyxnQ0FBa0JBLFdBQWxCLENBSEgsSUFJRyx5QkFBV0EsV0FBWCxDQUxFLENBQVA7QUFPRDs7O3NDQUVpQnBPLEksRUFBTTtBQUN0QixhQUFPLENBQUMsQ0FBQ0EsSUFBRixJQUFVLGdDQUFrQitFLGVBQWUsQ0FBQy9FLElBQUQsQ0FBakMsQ0FBakI7QUFDRDs7OzZDQUV3QjZJLEksRUFBTTtBQUM3QixVQUFJLENBQUNBLElBQUQsSUFBUyxDQUFDLEtBQUt3RixjQUFMLENBQW9CeEYsSUFBcEIsQ0FBZCxFQUF5QztBQUN2QyxlQUFPLEtBQVA7QUFDRDs7QUFDRCxhQUFPLEtBQUtyQixpQkFBTCxDQUF1QnFCLElBQUksQ0FBQzdJLElBQTVCLENBQVA7QUFDRDs7OzRDQUV1QnNPLFEsRUFBVTtBQUNoQztBQUNBLFVBQUlBLFFBQUosRUFBYztBQUNaLFlBQUkzSixRQUFKOztBQUNBLFlBQUkySixRQUFRLENBQUMxSixRQUFiLEVBQXVCO0FBQUU7QUFDcEJELFVBQUFBLFFBRGtCLEdBQ0wySixRQUFRLENBQUMxSixRQURKLENBQ2xCRCxRQURrQjtBQUV0QixTQUZELE1BRU8sSUFBSTJKLFFBQVEsQ0FBQzNKLFFBQWIsRUFBdUI7QUFDekJBLFVBQUFBLFFBRHlCLEdBQ1oySixRQURZLENBQ3pCM0osUUFEeUI7QUFFN0I7O0FBQ0QsWUFBSUEsUUFBSixFQUFjO0FBQ1osaUJBQU9BLFFBQVA7QUFDRDtBQUNGOztBQUNELFlBQU0sSUFBSXpCLEtBQUosQ0FBVSwyRUFBVixDQUFOO0FBQ0Q7OztvQ0FFc0I7QUFDckIsMEJBQU9wQyxrQkFBTUMsYUFBTixvQ0FBUDtBQUNEOzs7OENBRXlCOUIsSSxFQUFNdUcsTyxFQUFTO0FBQ3ZDLGFBQU87QUFDTCtJLFFBQUFBLFVBQVUsRUFBVkEsOEJBREs7QUFFTHRQLFFBQUFBLElBQUksRUFBRSxtREFBMEI2QixrQkFBTUMsYUFBaEMsRUFBK0M5QixJQUEvQyxFQUFxRHVHLE9BQXJEO0FBRkQsT0FBUDtBQUlEOzs7O0VBcmlCaUN3SCxxQjs7QUF3aUJwQ3dCLE1BQU0sQ0FBQ0MsT0FBUCxHQUFpQm5KLHFCQUFqQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIG5vLXVzZS1iZWZvcmUtZGVmaW5lICovXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFJlYWN0RE9NIGZyb20gJ3JlYWN0LWRvbSc7XG5pbXBvcnQgUmVhY3RET01TZXJ2ZXIgZnJvbSAncmVhY3QtZG9tL3NlcnZlcic7XG5pbXBvcnQgU2hhbGxvd1JlbmRlcmVyIGZyb20gJ3JlYWN0LXRlc3QtcmVuZGVyZXIvc2hhbGxvdyc7XG5pbXBvcnQgVGVzdFV0aWxzIGZyb20gJ3JlYWN0LWRvbS90ZXN0LXV0aWxzJztcbmltcG9ydCBjaGVja1Byb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzL2NoZWNrUHJvcFR5cGVzJztcbmltcG9ydCBoYXMgZnJvbSAnaGFzJztcbmltcG9ydCB7XG4gIENvbmN1cnJlbnRNb2RlLFxuICBDb250ZXh0Q29uc3VtZXIsXG4gIENvbnRleHRQcm92aWRlcixcbiAgRWxlbWVudCxcbiAgRm9yd2FyZFJlZixcbiAgRnJhZ21lbnQsXG4gIGlzQ29udGV4dENvbnN1bWVyLFxuICBpc0NvbnRleHRQcm92aWRlcixcbiAgaXNFbGVtZW50LFxuICBpc0ZvcndhcmRSZWYsXG4gIGlzUG9ydGFsLFxuICBpc1N1c3BlbnNlLFxuICBpc1ZhbGlkRWxlbWVudFR5cGUsXG4gIExhenksXG4gIE1lbW8sXG4gIFBvcnRhbCxcbiAgUHJvZmlsZXIsXG4gIFN0cmljdE1vZGUsXG4gIFN1c3BlbnNlLFxufSBmcm9tICdyZWFjdC1pcyc7XG5pbXBvcnQgeyBFbnp5bWVBZGFwdGVyIH0gZnJvbSAnZW56eW1lJztcbmltcG9ydCB7IHR5cGVPZk5vZGUgfSBmcm9tICdlbnp5bWUvYnVpbGQvVXRpbHMnO1xuaW1wb3J0IHNoYWxsb3dFcXVhbCBmcm9tICdlbnp5bWUtc2hhbGxvdy1lcXVhbCc7XG5pbXBvcnQge1xuICBkaXNwbGF5TmFtZU9mTm9kZSxcbiAgZWxlbWVudFRvVHJlZSBhcyB1dGlsRWxlbWVudFRvVHJlZSxcbiAgbm9kZVR5cGVGcm9tVHlwZSBhcyB1dGlsTm9kZVR5cGVGcm9tVHlwZSxcbiAgbWFwTmF0aXZlRXZlbnROYW1lcyxcbiAgcHJvcEZyb21FdmVudCxcbiAgYXNzZXJ0RG9tQXZhaWxhYmxlLFxuICB3aXRoU2V0U3RhdGVBbGxvd2VkLFxuICBjcmVhdGVSZW5kZXJXcmFwcGVyLFxuICBjcmVhdGVNb3VudFdyYXBwZXIsXG4gIHByb3BzV2l0aEtleXNBbmRSZWYsXG4gIGVuc3VyZUtleU9yVW5kZWZpbmVkLFxuICBzaW11bGF0ZUVycm9yLFxuICB3cmFwLFxuICBnZXRNYXNrZWRDb250ZXh0LFxuICBnZXRDb21wb25lbnRTdGFjayxcbiAgUm9vdEZpbmRlcixcbiAgZ2V0Tm9kZUZyb21Sb290RmluZGVyLFxuICB3cmFwV2l0aFdyYXBwaW5nQ29tcG9uZW50LFxuICBnZXRXcmFwcGluZ0NvbXBvbmVudE1vdW50UmVuZGVyZXIsXG4gIGNvbXBhcmVOb2RlVHlwZU9mLFxuICBzcHlNZXRob2QsXG59IGZyb20gJ0B3b2p0ZWttYWovZW56eW1lLWFkYXB0ZXItdXRpbHMnO1xuaW1wb3J0IGZpbmRDdXJyZW50RmliZXJVc2luZ1Nsb3dQYXRoIGZyb20gJy4vZmluZEN1cnJlbnRGaWJlclVzaW5nU2xvd1BhdGgnO1xuaW1wb3J0IGRldGVjdEZpYmVyVGFncyBmcm9tICcuL2RldGVjdEZpYmVyVGFncyc7XG5cbi8vIExhemlseSBwb3B1bGF0ZWQgaWYgRE9NIGlzIGF2YWlsYWJsZS5cbmxldCBGaWJlclRhZ3MgPSBudWxsO1xuXG5mdW5jdGlvbiBub2RlQW5kU2libGluZ3NBcnJheShub2RlV2l0aFNpYmxpbmcpIHtcbiAgY29uc3QgYXJyYXkgPSBbXTtcbiAgbGV0IG5vZGUgPSBub2RlV2l0aFNpYmxpbmc7XG4gIHdoaWxlIChub2RlICE9IG51bGwpIHtcbiAgICBhcnJheS5wdXNoKG5vZGUpO1xuICAgIG5vZGUgPSBub2RlLnNpYmxpbmc7XG4gIH1cbiAgcmV0dXJuIGFycmF5O1xufVxuXG5mdW5jdGlvbiBmbGF0dGVuKGFycikge1xuICBjb25zdCByZXN1bHQgPSBbXTtcbiAgY29uc3Qgc3RhY2sgPSBbeyBpOiAwLCBhcnJheTogYXJyIH1dO1xuICB3aGlsZSAoc3RhY2subGVuZ3RoKSB7XG4gICAgY29uc3QgbiA9IHN0YWNrLnBvcCgpO1xuICAgIHdoaWxlIChuLmkgPCBuLmFycmF5Lmxlbmd0aCkge1xuICAgICAgY29uc3QgZWwgPSBuLmFycmF5W24uaV07XG4gICAgICBuLmkgKz0gMTtcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KGVsKSkge1xuICAgICAgICBzdGFjay5wdXNoKG4pO1xuICAgICAgICBzdGFjay5wdXNoKHsgaTogMCwgYXJyYXk6IGVsIH0pO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICAgIHJlc3VsdC5wdXNoKGVsKTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuZnVuY3Rpb24gbm9kZVR5cGVGcm9tVHlwZSh0eXBlKSB7XG4gIGlmICh0eXBlID09PSBQb3J0YWwpIHtcbiAgICByZXR1cm4gJ3BvcnRhbCc7XG4gIH1cblxuICByZXR1cm4gdXRpbE5vZGVUeXBlRnJvbVR5cGUodHlwZSk7XG59XG5cbmZ1bmN0aW9uIGlzTWVtbyh0eXBlKSB7XG4gIHJldHVybiBjb21wYXJlTm9kZVR5cGVPZih0eXBlLCBNZW1vKTtcbn1cblxuZnVuY3Rpb24gaXNMYXp5KHR5cGUpIHtcbiAgcmV0dXJuIGNvbXBhcmVOb2RlVHlwZU9mKHR5cGUsIExhenkpO1xufVxuXG5mdW5jdGlvbiB1bm1lbW9UeXBlKHR5cGUpIHtcbiAgcmV0dXJuIGlzTWVtbyh0eXBlKSA/IHR5cGUudHlwZSA6IHR5cGU7XG59XG5cbmZ1bmN0aW9uIGNoZWNrSXNTdXNwZW5zZUFuZENsb25lRWxlbWVudChlbCwgeyBzdXNwZW5zZUZhbGxiYWNrIH0pIHtcbiAgaWYgKCFpc1N1c3BlbnNlKGVsKSkge1xuICAgIHJldHVybiBlbDtcbiAgfVxuXG4gIGxldCB7IGNoaWxkcmVuIH0gPSBlbC5wcm9wcztcblxuICBpZiAoc3VzcGVuc2VGYWxsYmFjaykge1xuICAgIGNvbnN0IHsgZmFsbGJhY2sgfSA9IGVsLnByb3BzO1xuICAgIGNoaWxkcmVuID0gcmVwbGFjZUxhenlXaXRoRmFsbGJhY2soY2hpbGRyZW4sIGZhbGxiYWNrKTtcbiAgfVxuXG4gIGNvbnN0IEZha2VTdXNwZW5zZVdyYXBwZXIgPSAocHJvcHMpID0+IFJlYWN0LmNyZWF0ZUVsZW1lbnQoXG4gICAgZWwudHlwZSxcbiAgICB7IC4uLmVsLnByb3BzLCAuLi5wcm9wcyB9LFxuICAgIGNoaWxkcmVuLFxuICApO1xuICByZXR1cm4gUmVhY3QuY3JlYXRlRWxlbWVudChGYWtlU3VzcGVuc2VXcmFwcGVyLCBudWxsLCBjaGlsZHJlbik7XG59XG5cbmZ1bmN0aW9uIGVsZW1lbnRUb1RyZWUoZWwpIHtcbiAgaWYgKCFpc1BvcnRhbChlbCkpIHtcbiAgICByZXR1cm4gdXRpbEVsZW1lbnRUb1RyZWUoZWwsIGVsZW1lbnRUb1RyZWUpO1xuICB9XG5cbiAgY29uc3QgeyBjaGlsZHJlbiwgY29udGFpbmVySW5mbyB9ID0gZWw7XG4gIGNvbnN0IHByb3BzID0geyBjaGlsZHJlbiwgY29udGFpbmVySW5mbyB9O1xuXG4gIHJldHVybiB7XG4gICAgbm9kZVR5cGU6ICdwb3J0YWwnLFxuICAgIHR5cGU6IFBvcnRhbCxcbiAgICBwcm9wcyxcbiAgICBrZXk6IGVuc3VyZUtleU9yVW5kZWZpbmVkKGVsLmtleSksXG4gICAgcmVmOiBlbC5yZWYgfHwgbnVsbCxcbiAgICBpbnN0YW5jZTogbnVsbCxcbiAgICByZW5kZXJlZDogZWxlbWVudFRvVHJlZShlbC5jaGlsZHJlbiksXG4gIH07XG59XG5cbmZ1bmN0aW9uIHRvVHJlZSh2bm9kZSkge1xuICBpZiAodm5vZGUgPT0gbnVsbCkge1xuICAgIHJldHVybiBudWxsO1xuICB9XG4gIC8vIFRPRE8obG1yKTogSSdtIG5vdCByZWFsbHkgc3VyZSBJIHVuZGVyc3RhbmQgd2hldGhlciBvciBub3QgdGhpcyBpcyB3aGF0XG4gIC8vIGkgc2hvdWxkIGJlIGRvaW5nLCBvciBpZiB0aGlzIGlzIGEgaGFjayBmb3Igc29tZXRoaW5nIGknbSBkb2luZyB3cm9uZ1xuICAvLyBzb21ld2hlcmUgZWxzZS4gU2hvdWxkIHRhbGsgdG8gc2ViYXN0aWFuIGFib3V0IHRoaXMgcGVyaGFwc1xuICBjb25zdCBub2RlID0gZmluZEN1cnJlbnRGaWJlclVzaW5nU2xvd1BhdGgodm5vZGUpO1xuICBzd2l0Y2ggKG5vZGUudGFnKSB7XG4gICAgY2FzZSBGaWJlclRhZ3MuSG9zdFJvb3Q6XG4gICAgICByZXR1cm4gY2hpbGRyZW5Ub1RyZWUobm9kZS5jaGlsZCk7XG4gICAgY2FzZSBGaWJlclRhZ3MuSG9zdFBvcnRhbDoge1xuICAgICAgY29uc3Qge1xuICAgICAgICBzdGF0ZU5vZGU6IHsgY29udGFpbmVySW5mbyB9LFxuICAgICAgICBtZW1vaXplZFByb3BzOiBjaGlsZHJlbixcbiAgICAgIH0gPSBub2RlO1xuICAgICAgY29uc3QgcHJvcHMgPSB7IGNvbnRhaW5lckluZm8sIGNoaWxkcmVuIH07XG4gICAgICByZXR1cm4ge1xuICAgICAgICBub2RlVHlwZTogJ3BvcnRhbCcsXG4gICAgICAgIHR5cGU6IFBvcnRhbCxcbiAgICAgICAgcHJvcHMsXG4gICAgICAgIGtleTogZW5zdXJlS2V5T3JVbmRlZmluZWQobm9kZS5rZXkpLFxuICAgICAgICByZWY6IG5vZGUucmVmLFxuICAgICAgICBpbnN0YW5jZTogbnVsbCxcbiAgICAgICAgcmVuZGVyZWQ6IGNoaWxkcmVuVG9UcmVlKG5vZGUuY2hpbGQpLFxuICAgICAgfTtcbiAgICB9XG4gICAgY2FzZSBGaWJlclRhZ3MuQ2xhc3NDb21wb25lbnQ6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBub2RlVHlwZTogJ2NsYXNzJyxcbiAgICAgICAgdHlwZTogbm9kZS50eXBlLFxuICAgICAgICBwcm9wczogeyAuLi5ub2RlLm1lbW9pemVkUHJvcHMgfSxcbiAgICAgICAga2V5OiBlbnN1cmVLZXlPclVuZGVmaW5lZChub2RlLmtleSksXG4gICAgICAgIHJlZjogbm9kZS5yZWYsXG4gICAgICAgIGluc3RhbmNlOiBub2RlLnN0YXRlTm9kZSxcbiAgICAgICAgcmVuZGVyZWQ6IGNoaWxkcmVuVG9UcmVlKG5vZGUuY2hpbGQpLFxuICAgICAgfTtcbiAgICBjYXNlIEZpYmVyVGFncy5GdW5jdGlvbmFsQ29tcG9uZW50OlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbm9kZVR5cGU6ICdmdW5jdGlvbicsXG4gICAgICAgIHR5cGU6IG5vZGUudHlwZSxcbiAgICAgICAgcHJvcHM6IHsgLi4ubm9kZS5tZW1vaXplZFByb3BzIH0sXG4gICAgICAgIGtleTogZW5zdXJlS2V5T3JVbmRlZmluZWQobm9kZS5rZXkpLFxuICAgICAgICByZWY6IG5vZGUucmVmLFxuICAgICAgICBpbnN0YW5jZTogbnVsbCxcbiAgICAgICAgcmVuZGVyZWQ6IGNoaWxkcmVuVG9UcmVlKG5vZGUuY2hpbGQpLFxuICAgICAgfTtcbiAgICBjYXNlIEZpYmVyVGFncy5NZW1vQ2xhc3M6XG4gICAgICByZXR1cm4ge1xuICAgICAgICBub2RlVHlwZTogJ2NsYXNzJyxcbiAgICAgICAgdHlwZTogbm9kZS5lbGVtZW50VHlwZS50eXBlLFxuICAgICAgICBwcm9wczogeyAuLi5ub2RlLm1lbW9pemVkUHJvcHMgfSxcbiAgICAgICAga2V5OiBlbnN1cmVLZXlPclVuZGVmaW5lZChub2RlLmtleSksXG4gICAgICAgIHJlZjogbm9kZS5yZWYsXG4gICAgICAgIGluc3RhbmNlOiBub2RlLnN0YXRlTm9kZSxcbiAgICAgICAgcmVuZGVyZWQ6IGNoaWxkcmVuVG9UcmVlKG5vZGUuY2hpbGQuY2hpbGQpLFxuICAgICAgfTtcbiAgICBjYXNlIEZpYmVyVGFncy5NZW1vU0ZDOiB7XG4gICAgICBsZXQgcmVuZGVyZWROb2RlcyA9IGZsYXR0ZW4obm9kZUFuZFNpYmxpbmdzQXJyYXkobm9kZS5jaGlsZCkubWFwKHRvVHJlZSkpO1xuICAgICAgaWYgKHJlbmRlcmVkTm9kZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIHJlbmRlcmVkTm9kZXMgPSBbbm9kZS5tZW1vaXplZFByb3BzLmNoaWxkcmVuXTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5vZGVUeXBlOiAnZnVuY3Rpb24nLFxuICAgICAgICB0eXBlOiBub2RlLmVsZW1lbnRUeXBlLFxuICAgICAgICBwcm9wczogeyAuLi5ub2RlLm1lbW9pemVkUHJvcHMgfSxcbiAgICAgICAga2V5OiBlbnN1cmVLZXlPclVuZGVmaW5lZChub2RlLmtleSksXG4gICAgICAgIHJlZjogbm9kZS5yZWYsXG4gICAgICAgIGluc3RhbmNlOiBudWxsLFxuICAgICAgICByZW5kZXJlZDogcmVuZGVyZWROb2RlcyxcbiAgICAgIH07XG4gICAgfVxuICAgIGNhc2UgRmliZXJUYWdzLkhvc3RDb21wb25lbnQ6IHtcbiAgICAgIGxldCByZW5kZXJlZE5vZGVzID0gZmxhdHRlbihub2RlQW5kU2libGluZ3NBcnJheShub2RlLmNoaWxkKS5tYXAodG9UcmVlKSk7XG4gICAgICBpZiAocmVuZGVyZWROb2Rlcy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgcmVuZGVyZWROb2RlcyA9IFtub2RlLm1lbW9pemVkUHJvcHMuY2hpbGRyZW5dO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbm9kZVR5cGU6ICdob3N0JyxcbiAgICAgICAgdHlwZTogbm9kZS50eXBlLFxuICAgICAgICBwcm9wczogeyAuLi5ub2RlLm1lbW9pemVkUHJvcHMgfSxcbiAgICAgICAga2V5OiBlbnN1cmVLZXlPclVuZGVmaW5lZChub2RlLmtleSksXG4gICAgICAgIHJlZjogbm9kZS5yZWYsXG4gICAgICAgIGluc3RhbmNlOiBub2RlLnN0YXRlTm9kZSxcbiAgICAgICAgcmVuZGVyZWQ6IHJlbmRlcmVkTm9kZXMsXG4gICAgICB9O1xuICAgIH1cbiAgICBjYXNlIEZpYmVyVGFncy5Ib3N0VGV4dDpcbiAgICAgIHJldHVybiBub2RlLm1lbW9pemVkUHJvcHM7XG4gICAgY2FzZSBGaWJlclRhZ3MuRnJhZ21lbnQ6XG4gICAgY2FzZSBGaWJlclRhZ3MuTW9kZTpcbiAgICBjYXNlIEZpYmVyVGFncy5Db250ZXh0UHJvdmlkZXI6XG4gICAgY2FzZSBGaWJlclRhZ3MuQ29udGV4dENvbnN1bWVyOlxuICAgICAgcmV0dXJuIGNoaWxkcmVuVG9UcmVlKG5vZGUuY2hpbGQpO1xuICAgIGNhc2UgRmliZXJUYWdzLlByb2ZpbGVyOlxuICAgIGNhc2UgRmliZXJUYWdzLkZvcndhcmRSZWY6IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5vZGVUeXBlOiAnZnVuY3Rpb24nLFxuICAgICAgICB0eXBlOiBub2RlLnR5cGUsXG4gICAgICAgIHByb3BzOiB7IC4uLm5vZGUucGVuZGluZ1Byb3BzIH0sXG4gICAgICAgIGtleTogZW5zdXJlS2V5T3JVbmRlZmluZWQobm9kZS5rZXkpLFxuICAgICAgICByZWY6IG5vZGUucmVmLFxuICAgICAgICBpbnN0YW5jZTogbnVsbCxcbiAgICAgICAgcmVuZGVyZWQ6IGNoaWxkcmVuVG9UcmVlKG5vZGUuY2hpbGQpLFxuICAgICAgfTtcbiAgICB9XG4gICAgY2FzZSBGaWJlclRhZ3MuU3VzcGVuc2U6IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5vZGVUeXBlOiAnZnVuY3Rpb24nLFxuICAgICAgICB0eXBlOiBTdXNwZW5zZSxcbiAgICAgICAgcHJvcHM6IHsgLi4ubm9kZS5tZW1vaXplZFByb3BzIH0sXG4gICAgICAgIGtleTogZW5zdXJlS2V5T3JVbmRlZmluZWQobm9kZS5rZXkpLFxuICAgICAgICByZWY6IG5vZGUucmVmLFxuICAgICAgICBpbnN0YW5jZTogbnVsbCxcbiAgICAgICAgcmVuZGVyZWQ6IGNoaWxkcmVuVG9UcmVlKG5vZGUuY2hpbGQpLFxuICAgICAgfTtcbiAgICB9XG4gICAgY2FzZSBGaWJlclRhZ3MuTGF6eTpcbiAgICAgIHJldHVybiBjaGlsZHJlblRvVHJlZShub2RlLmNoaWxkKTtcbiAgICBjYXNlIEZpYmVyVGFncy5PZmZzY3JlZW5Db21wb25lbnQ6XG4gICAgICByZXR1cm4gdG9UcmVlKG5vZGUuY2hpbGQpO1xuICAgIGRlZmF1bHQ6XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoYEVuenltZSBJbnRlcm5hbCBFcnJvcjogdW5rbm93biBub2RlIHdpdGggdGFnICR7bm9kZS50YWd9YCk7XG4gIH1cbn1cblxuZnVuY3Rpb24gY2hpbGRyZW5Ub1RyZWUobm9kZSkge1xuICBpZiAoIW5vZGUpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuICBjb25zdCBjaGlsZHJlbiA9IG5vZGVBbmRTaWJsaW5nc0FycmF5KG5vZGUpO1xuICBpZiAoY2hpbGRyZW4ubGVuZ3RoID09PSAwKSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cbiAgaWYgKGNoaWxkcmVuLmxlbmd0aCA9PT0gMSkge1xuICAgIHJldHVybiB0b1RyZWUoY2hpbGRyZW5bMF0pO1xuICB9XG4gIHJldHVybiBmbGF0dGVuKGNoaWxkcmVuLm1hcCh0b1RyZWUpKTtcbn1cblxuZnVuY3Rpb24gbm9kZVRvSG9zdE5vZGUoX25vZGUpIHtcbiAgLy8gTk9URShsbXIpOiBub2RlIGNvdWxkIGJlIGEgZnVuY3Rpb24gY29tcG9uZW50XG4gIC8vIHdoaWNoIHdvbnQgaGF2ZSBhbiBpbnN0YW5jZSBwcm9wLCBidXQgd2UgY2FuIGdldCB0aGVcbiAgLy8gaG9zdCBub2RlIGFzc29jaWF0ZWQgd2l0aCBpdHMgcmV0dXJuIHZhbHVlIGF0IHRoYXQgcG9pbnQuXG4gIC8vIEFsdGhvdWdoIHRoaXMgYnJlYWtzIGRvd24gaWYgdGhlIHJldHVybiB2YWx1ZSBpcyBhbiBhcnJheSxcbiAgLy8gYXMgaXMgcG9zc2libGUgd2l0aCBSZWFjdCAxNi5cbiAgbGV0IG5vZGUgPSBfbm9kZTtcbiAgd2hpbGUgKG5vZGUgJiYgIUFycmF5LmlzQXJyYXkobm9kZSkgJiYgbm9kZS5pbnN0YW5jZSA9PT0gbnVsbCkge1xuICAgIG5vZGUgPSBub2RlLnJlbmRlcmVkO1xuICB9XG4gIC8vIGlmIHRoZSBTRkMgcmV0dXJuZWQgbnVsbCBlZmZlY3RpdmVseSwgdGhlcmUgaXMgbm8gaG9zdCBub2RlLlxuICBpZiAoIW5vZGUpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGNvbnN0IG1hcHBlciA9IChpdGVtKSA9PiB7XG4gICAgaWYgKGl0ZW0gJiYgaXRlbS5pbnN0YW5jZSkgcmV0dXJuIFJlYWN0RE9NLmZpbmRET01Ob2RlKGl0ZW0uaW5zdGFuY2UpO1xuICAgIHJldHVybiBudWxsO1xuICB9O1xuICBpZiAoQXJyYXkuaXNBcnJheShub2RlKSkge1xuICAgIHJldHVybiBub2RlLm1hcChtYXBwZXIpO1xuICB9XG4gIGlmIChBcnJheS5pc0FycmF5KG5vZGUucmVuZGVyZWQpICYmIG5vZGUubm9kZVR5cGUgPT09ICdjbGFzcycpIHtcbiAgICByZXR1cm4gbm9kZS5yZW5kZXJlZC5tYXAobWFwcGVyKTtcbiAgfVxuICByZXR1cm4gbWFwcGVyKG5vZGUpO1xufVxuXG5mdW5jdGlvbiByZXBsYWNlTGF6eVdpdGhGYWxsYmFjayhub2RlLCBmYWxsYmFjaykge1xuICBpZiAoIW5vZGUpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuICBpZiAoQXJyYXkuaXNBcnJheShub2RlKSkge1xuICAgIHJldHVybiBub2RlLm1hcCgoZWwpID0+IHJlcGxhY2VMYXp5V2l0aEZhbGxiYWNrKGVsLCBmYWxsYmFjaykpO1xuICB9XG4gIGlmIChpc0xhenkobm9kZS50eXBlKSkge1xuICAgIHJldHVybiBmYWxsYmFjaztcbiAgfVxuICByZXR1cm4ge1xuICAgIC4uLm5vZGUsXG4gICAgcHJvcHM6IHtcbiAgICAgIC4uLm5vZGUucHJvcHMsXG4gICAgICBjaGlsZHJlbjogcmVwbGFjZUxhenlXaXRoRmFsbGJhY2sobm9kZS5wcm9wcy5jaGlsZHJlbiwgZmFsbGJhY2spLFxuICAgIH0sXG4gIH07XG59XG5cbmNvbnN0IGV2ZW50T3B0aW9ucyA9IHtcbiAgYW5pbWF0aW9uOiB0cnVlLFxuICBwb2ludGVyRXZlbnRzOiB0cnVlLFxuICBhdXhDbGljazogdHJ1ZSxcbn07XG5cbmZ1bmN0aW9uIGdldEVtcHR5U3RhdGVWYWx1ZSgpIHtcbiAgLy8gdGhpcyBoYW5kbGVzIGEgYnVnIGluIFJlYWN0IDE2LjAgLSAxNi4yXG4gIC8vIHNlZSBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svcmVhY3QvY29tbWl0LzM5YmU4MzU2NWM2NWY5YzUyMjE1MGU1MjM3NTE2NzU2OGEyYTE0NTlcbiAgLy8gYWxzbyBzZWUgaHR0cHM6Ly9naXRodWIuY29tL2ZhY2Vib29rL3JlYWN0L3B1bGwvMTE5NjVcblxuICBjbGFzcyBFbXB0eVN0YXRlIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgICByZW5kZXIoKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cbiAgY29uc3QgdGVzdFJlbmRlcmVyID0gbmV3IFNoYWxsb3dSZW5kZXJlcigpO1xuICB0ZXN0UmVuZGVyZXIucmVuZGVyKFJlYWN0LmNyZWF0ZUVsZW1lbnQoRW1wdHlTdGF0ZSkpO1xuICByZXR1cm4gdGVzdFJlbmRlcmVyLl9pbnN0YW5jZS5zdGF0ZTtcbn1cblxuZnVuY3Rpb24gd3JhcEFjdChmbikge1xuICBsZXQgcmV0dXJuVmFsO1xuICBUZXN0VXRpbHMuYWN0KCgpID0+IHsgcmV0dXJuVmFsID0gZm4oKTsgfSk7XG4gIHJldHVybiByZXR1cm5WYWw7XG59XG5cbmZ1bmN0aW9uIGdldFByb3ZpZGVyRGVmYXVsdFZhbHVlKFByb3ZpZGVyKSB7XG4gIC8vIFJlYWN0IHN0b3JlcyByZWZlcmVuY2VzIHRvIHRoZSBQcm92aWRlcidzIGRlZmF1bHRWYWx1ZSBkaWZmZXJlbnRseSBhY3Jvc3MgdmVyc2lvbnMuXG4gIGlmICgnX2RlZmF1bHRWYWx1ZScgaW4gUHJvdmlkZXIuX2NvbnRleHQpIHtcbiAgICByZXR1cm4gUHJvdmlkZXIuX2NvbnRleHQuX2RlZmF1bHRWYWx1ZTtcbiAgfVxuICBpZiAoJ19jdXJyZW50VmFsdWUnIGluIFByb3ZpZGVyLl9jb250ZXh0KSB7XG4gICAgcmV0dXJuIFByb3ZpZGVyLl9jb250ZXh0Ll9jdXJyZW50VmFsdWU7XG4gIH1cbiAgdGhyb3cgbmV3IEVycm9yKCdFbnp5bWUgSW50ZXJuYWwgRXJyb3I6IGNhbuKAmXQgZmlndXJlIG91dCBob3cgdG8gZ2V0IFByb3ZpZGVy4oCZcyBkZWZhdWx0IHZhbHVlJyk7XG59XG5cbmZ1bmN0aW9uIG1ha2VGYWtlRWxlbWVudCh0eXBlKSB7XG4gIHJldHVybiB7ICQkdHlwZW9mOiBFbGVtZW50LCB0eXBlIH07XG59XG5cbmZ1bmN0aW9uIGlzU3RhdGVmdWwoQ29tcG9uZW50KSB7XG4gIHJldHVybiBDb21wb25lbnQucHJvdG90eXBlICYmIChcbiAgICBDb21wb25lbnQucHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnRcbiAgICB8fCBBcnJheS5pc0FycmF5KENvbXBvbmVudC5fX3JlYWN0QXV0b0JpbmRQYWlycykgLy8gZmFsbGJhY2sgZm9yIGNyZWF0ZUNsYXNzIGNvbXBvbmVudHNcbiAgKTtcbn1cblxuY2xhc3MgUmVhY3RTZXZlbnRlZW5BZGFwdGVyIGV4dGVuZHMgRW56eW1lQWRhcHRlciB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gICAgY29uc3QgeyBsaWZlY3ljbGVzIH0gPSB0aGlzLm9wdGlvbnM7XG4gICAgdGhpcy5vcHRpb25zID0ge1xuICAgICAgLi4udGhpcy5vcHRpb25zLFxuICAgICAgZW5hYmxlQ29tcG9uZW50RGlkVXBkYXRlT25TZXRTdGF0ZTogdHJ1ZSwgLy8gVE9ETzogcmVtb3ZlLCBzZW12ZXItbWFqb3JcbiAgICAgIGxlZ2FjeUNvbnRleHRNb2RlOiAncGFyZW50JyxcbiAgICAgIGxpZmVjeWNsZXM6IHtcbiAgICAgICAgLi4ubGlmZWN5Y2xlcyxcbiAgICAgICAgY29tcG9uZW50RGlkVXBkYXRlOiB7XG4gICAgICAgICAgb25TZXRTdGF0ZTogdHJ1ZSxcbiAgICAgICAgfSxcbiAgICAgICAgZ2V0RGVyaXZlZFN0YXRlRnJvbVByb3BzOiB7XG4gICAgICAgICAgaGFzU2hvdWxkQ29tcG9uZW50VXBkYXRlQnVnOiBmYWxzZSxcbiAgICAgICAgfSxcbiAgICAgICAgZ2V0U25hcHNob3RCZWZvcmVVcGRhdGU6IHRydWUsXG4gICAgICAgIHNldFN0YXRlOiB7XG4gICAgICAgICAgc2tpcHNDb21wb25lbnREaWRVcGRhdGVPbk51bGxpc2g6IHRydWUsXG4gICAgICAgIH0sXG4gICAgICAgIGdldENoaWxkQ29udGV4dDoge1xuICAgICAgICAgIGNhbGxlZEJ5UmVuZGVyZXI6IGZhbHNlLFxuICAgICAgICB9LFxuICAgICAgICBnZXREZXJpdmVkU3RhdGVGcm9tRXJyb3I6IHRydWUsXG4gICAgICB9LFxuICAgIH07XG4gIH1cblxuICBjcmVhdGVNb3VudFJlbmRlcmVyKG9wdGlvbnMpIHtcbiAgICBhc3NlcnREb21BdmFpbGFibGUoJ21vdW50Jyk7XG4gICAgaWYgKGhhcyhvcHRpb25zLCAnc3VzcGVuc2VGYWxsYmFjaycpKSB7XG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdgc3VzcGVuc2VGYWxsYmFja2AgaXMgbm90IHN1cHBvcnRlZCBieSB0aGUgYG1vdW50YCByZW5kZXJlcicpO1xuICAgIH1cbiAgICBpZiAoRmliZXJUYWdzID09PSBudWxsKSB7XG4gICAgICAvLyBSZXF1aXJlcyBET00uXG4gICAgICBGaWJlclRhZ3MgPSBkZXRlY3RGaWJlclRhZ3MoKTtcbiAgICB9XG4gICAgY29uc3QgeyBhdHRhY2hUbywgaHlkcmF0ZUluLCB3cmFwcGluZ0NvbXBvbmVudFByb3BzIH0gPSBvcHRpb25zO1xuICAgIGNvbnN0IGRvbU5vZGUgPSBoeWRyYXRlSW4gfHwgYXR0YWNoVG8gfHwgZ2xvYmFsLmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgIGxldCBpbnN0YW5jZSA9IG51bGw7XG4gICAgY29uc3QgYWRhcHRlciA9IHRoaXM7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJlbmRlcihlbCwgY29udGV4dCwgY2FsbGJhY2spIHtcbiAgICAgICAgcmV0dXJuIHdyYXBBY3QoKCkgPT4ge1xuICAgICAgICAgIGlmIChpbnN0YW5jZSA9PT0gbnVsbCkge1xuICAgICAgICAgICAgY29uc3QgeyB0eXBlLCBwcm9wcywgcmVmIH0gPSBlbDtcbiAgICAgICAgICAgIGNvbnN0IHdyYXBwZXJQcm9wcyA9IHtcbiAgICAgICAgICAgICAgQ29tcG9uZW50OiB0eXBlLFxuICAgICAgICAgICAgICBwcm9wcyxcbiAgICAgICAgICAgICAgd3JhcHBpbmdDb21wb25lbnRQcm9wcyxcbiAgICAgICAgICAgICAgY29udGV4dCxcbiAgICAgICAgICAgICAgLi4uKHJlZiAmJiB7IHJlZlByb3A6IHJlZiB9KSxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBjb25zdCBSZWFjdFdyYXBwZXJDb21wb25lbnQgPSBjcmVhdGVNb3VudFdyYXBwZXIoZWwsIHsgLi4ub3B0aW9ucywgYWRhcHRlciB9KTtcbiAgICAgICAgICAgIGNvbnN0IHdyYXBwZWRFbCA9IFJlYWN0LmNyZWF0ZUVsZW1lbnQoUmVhY3RXcmFwcGVyQ29tcG9uZW50LCB3cmFwcGVyUHJvcHMpO1xuICAgICAgICAgICAgaW5zdGFuY2UgPSBoeWRyYXRlSW5cbiAgICAgICAgICAgICAgPyBSZWFjdERPTS5oeWRyYXRlKHdyYXBwZWRFbCwgZG9tTm9kZSlcbiAgICAgICAgICAgICAgOiBSZWFjdERPTS5yZW5kZXIod3JhcHBlZEVsLCBkb21Ob2RlKTtcbiAgICAgICAgICAgIGlmICh0eXBlb2YgY2FsbGJhY2sgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaW5zdGFuY2Uuc2V0Q2hpbGRQcm9wcyhlbC5wcm9wcywgY29udGV4dCwgY2FsbGJhY2spO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9LFxuICAgICAgdW5tb3VudCgpIHtcbiAgICAgICAgUmVhY3RET00udW5tb3VudENvbXBvbmVudEF0Tm9kZShkb21Ob2RlKTtcbiAgICAgICAgaW5zdGFuY2UgPSBudWxsO1xuICAgICAgfSxcbiAgICAgIGdldE5vZGUoKSB7XG4gICAgICAgIGlmICghaW5zdGFuY2UpIHtcbiAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZ2V0Tm9kZUZyb21Sb290RmluZGVyKFxuICAgICAgICAgIGFkYXB0ZXIuaXNDdXN0b21Db21wb25lbnQsXG4gICAgICAgICAgdG9UcmVlKGluc3RhbmNlLl9yZWFjdEludGVybmFscyksXG4gICAgICAgICAgb3B0aW9ucyxcbiAgICAgICAgKTtcbiAgICAgIH0sXG4gICAgICBzaW11bGF0ZUVycm9yKG5vZGVIaWVyYXJjaHksIHJvb3ROb2RlLCBlcnJvcikge1xuICAgICAgICBjb25zdCBpc0Vycm9yQm91bmRhcnkgPSAoeyBpbnN0YW5jZTogZWxJbnN0YW5jZSwgdHlwZSB9KSA9PiB7XG4gICAgICAgICAgaWYgKHR5cGUgJiYgdHlwZS5nZXREZXJpdmVkU3RhdGVGcm9tRXJyb3IpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZWxJbnN0YW5jZSAmJiBlbEluc3RhbmNlLmNvbXBvbmVudERpZENhdGNoO1xuICAgICAgICB9O1xuXG4gICAgICAgIGNvbnN0IHtcbiAgICAgICAgICBpbnN0YW5jZTogY2F0Y2hpbmdJbnN0YW5jZSxcbiAgICAgICAgICB0eXBlOiBjYXRjaGluZ1R5cGUsXG4gICAgICAgIH0gPSBub2RlSGllcmFyY2h5LmZpbmQoaXNFcnJvckJvdW5kYXJ5KSB8fCB7fTtcblxuICAgICAgICBzaW11bGF0ZUVycm9yKFxuICAgICAgICAgIGVycm9yLFxuICAgICAgICAgIGNhdGNoaW5nSW5zdGFuY2UsXG4gICAgICAgICAgcm9vdE5vZGUsXG4gICAgICAgICAgbm9kZUhpZXJhcmNoeSxcbiAgICAgICAgICBub2RlVHlwZUZyb21UeXBlLFxuICAgICAgICAgIGFkYXB0ZXIuZGlzcGxheU5hbWVPZk5vZGUsXG4gICAgICAgICAgY2F0Y2hpbmdUeXBlLFxuICAgICAgICApO1xuICAgICAgfSxcbiAgICAgIHNpbXVsYXRlRXZlbnQobm9kZSwgZXZlbnQsIG1vY2spIHtcbiAgICAgICAgY29uc3QgbWFwcGVkRXZlbnQgPSBtYXBOYXRpdmVFdmVudE5hbWVzKGV2ZW50LCBldmVudE9wdGlvbnMpO1xuICAgICAgICBjb25zdCBldmVudEZuID0gVGVzdFV0aWxzLlNpbXVsYXRlW21hcHBlZEV2ZW50XTtcbiAgICAgICAgaWYgKCFldmVudEZuKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihgUmVhY3RXcmFwcGVyOjpzaW11bGF0ZSgpIGV2ZW50ICcke2V2ZW50fScgZG9lcyBub3QgZXhpc3RgKTtcbiAgICAgICAgfVxuICAgICAgICB3cmFwQWN0KCgpID0+IHtcbiAgICAgICAgICBldmVudEZuKGFkYXB0ZXIubm9kZVRvSG9zdE5vZGUobm9kZSksIG1vY2spO1xuICAgICAgICB9KTtcbiAgICAgIH0sXG4gICAgICBiYXRjaGVkVXBkYXRlcyhmbikge1xuICAgICAgICByZXR1cm4gZm4oKTtcbiAgICAgICAgLy8gcmV0dXJuIFJlYWN0RE9NLnVuc3RhYmxlX2JhdGNoZWRVcGRhdGVzKGZuKTtcbiAgICAgIH0sXG4gICAgICBnZXRXcmFwcGluZ0NvbXBvbmVudFJlbmRlcmVyKCkge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIC4uLnRoaXMsXG4gICAgICAgICAgLi4uZ2V0V3JhcHBpbmdDb21wb25lbnRNb3VudFJlbmRlcmVyKHtcbiAgICAgICAgICAgIHRvVHJlZTogKGluc3QpID0+IHRvVHJlZShpbnN0Ll9yZWFjdEludGVybmFscyksXG4gICAgICAgICAgICBnZXRNb3VudFdyYXBwZXJJbnN0YW5jZTogKCkgPT4gaW5zdGFuY2UsXG4gICAgICAgICAgfSksXG4gICAgICAgIH07XG4gICAgICB9LFxuICAgICAgd3JhcEludm9rZTogd3JhcEFjdCxcbiAgICB9O1xuICB9XG5cbiAgY3JlYXRlU2hhbGxvd1JlbmRlcmVyKG9wdGlvbnMgPSB7fSkge1xuICAgIGNvbnN0IGFkYXB0ZXIgPSB0aGlzO1xuICAgIGNvbnN0IHJlbmRlcmVyID0gbmV3IFNoYWxsb3dSZW5kZXJlcigpO1xuICAgIGNvbnN0IHsgc3VzcGVuc2VGYWxsYmFjayB9ID0gb3B0aW9ucztcbiAgICBpZiAodHlwZW9mIHN1c3BlbnNlRmFsbGJhY2sgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBzdXNwZW5zZUZhbGxiYWNrICE9PSAnYm9vbGVhbicpIHtcbiAgICAgIHRocm93IFR5cGVFcnJvcignYG9wdGlvbnMuc3VzcGVuc2VGYWxsYmFja2Agc2hvdWxkIGJlIGJvb2xlYW4gb3IgdW5kZWZpbmVkJyk7XG4gICAgfVxuICAgIGxldCBpc0RPTSA9IGZhbHNlO1xuICAgIGxldCBjYWNoZWROb2RlID0gbnVsbDtcblxuICAgIGxldCBsYXN0Q29tcG9uZW50ID0gbnVsbDtcbiAgICBsZXQgd3JhcHBlZENvbXBvbmVudCA9IG51bGw7XG4gICAgY29uc3Qgc2VudGluZWwgPSB7fTtcblxuICAgIC8vIHdyYXAgbWVtbyBjb21wb25lbnRzIHdpdGggYSBQdXJlQ29tcG9uZW50LCBvciBhIGNsYXNzIGNvbXBvbmVudCB3aXRoIHNDVVxuICAgIGNvbnN0IHdyYXBQdXJlQ29tcG9uZW50ID0gKENvbXBvbmVudCwgY29tcGFyZSkgPT4ge1xuICAgICAgaWYgKGxhc3RDb21wb25lbnQgIT09IENvbXBvbmVudCkge1xuICAgICAgICBpZiAoaXNTdGF0ZWZ1bChDb21wb25lbnQpKSB7XG4gICAgICAgICAgd3JhcHBlZENvbXBvbmVudCA9IGNsYXNzIGV4dGVuZHMgQ29tcG9uZW50IHt9O1xuICAgICAgICAgIGlmIChjb21wYXJlKSB7XG4gICAgICAgICAgICB3cmFwcGVkQ29tcG9uZW50LnByb3RvdHlwZS5zaG91bGRDb21wb25lbnRVcGRhdGUgPSAobmV4dFByb3BzKSA9PiAoXG4gICAgICAgICAgICAgICFjb21wYXJlKHRoaXMucHJvcHMsIG5leHRQcm9wcylcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHdyYXBwZWRDb21wb25lbnQucHJvdG90eXBlLmlzUHVyZVJlYWN0Q29tcG9uZW50ID0gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbGV0IG1lbW9pemVkID0gc2VudGluZWw7XG4gICAgICAgICAgbGV0IHByZXZQcm9wcztcbiAgICAgICAgICB3cmFwcGVkQ29tcG9uZW50ID0gZnVuY3Rpb24gd3JhcHBlZENvbXBvbmVudEZuKHByb3BzLCAuLi5hcmdzKSB7XG4gICAgICAgICAgICBjb25zdCBzaG91bGRVcGRhdGUgPSBtZW1vaXplZCA9PT0gc2VudGluZWwgfHwgKGNvbXBhcmVcbiAgICAgICAgICAgICAgPyAhY29tcGFyZShwcmV2UHJvcHMsIHByb3BzKVxuICAgICAgICAgICAgICA6ICFzaGFsbG93RXF1YWwocHJldlByb3BzLCBwcm9wcylcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBpZiAoc2hvdWxkVXBkYXRlKSB7XG4gICAgICAgICAgICAgIG1lbW9pemVkID0gQ29tcG9uZW50KHsgLi4uQ29tcG9uZW50LmRlZmF1bHRQcm9wcywgLi4ucHJvcHMgfSwgLi4uYXJncyk7XG4gICAgICAgICAgICAgIHByZXZQcm9wcyA9IHByb3BzO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG1lbW9pemVkO1xuICAgICAgICAgIH07XG4gICAgICAgIH1cbiAgICAgICAgT2JqZWN0LmFzc2lnbihcbiAgICAgICAgICB3cmFwcGVkQ29tcG9uZW50LFxuICAgICAgICAgIENvbXBvbmVudCxcbiAgICAgICAgICB7IGRpc3BsYXlOYW1lOiBhZGFwdGVyLmRpc3BsYXlOYW1lT2ZOb2RlKHsgdHlwZTogQ29tcG9uZW50IH0pIH0sXG4gICAgICAgICk7XG4gICAgICAgIGxhc3RDb21wb25lbnQgPSBDb21wb25lbnQ7XG4gICAgICB9XG4gICAgICByZXR1cm4gd3JhcHBlZENvbXBvbmVudDtcbiAgICB9O1xuXG4gICAgLy8gV3JhcCBmdW5jdGlvbmFsIGNvbXBvbmVudHMgb24gdmVyc2lvbnMgcHJpb3IgdG8gMTYuNSxcbiAgICAvLyB0byBhdm9pZCBpbmFkdmVydGVudGx5IHBhc3MgYSBgdGhpc2AgaW5zdGFuY2UgdG8gaXQuXG4gICAgY29uc3Qgd3JhcEZ1bmN0aW9uYWxDb21wb25lbnQgPSAoQ29tcG9uZW50KSA9PiB7XG4gICAgICBpZiAoaGFzKENvbXBvbmVudCwgJ2RlZmF1bHRQcm9wcycpKSB7XG4gICAgICAgIGlmIChsYXN0Q29tcG9uZW50ICE9PSBDb21wb25lbnQpIHtcbiAgICAgICAgICB3cmFwcGVkQ29tcG9uZW50ID0gT2JqZWN0LmFzc2lnbihcbiAgICAgICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuZXctY2FwXG4gICAgICAgICAgICAocHJvcHMsIC4uLmFyZ3MpID0+IENvbXBvbmVudCh7IC4uLkNvbXBvbmVudC5kZWZhdWx0UHJvcHMsIC4uLnByb3BzIH0sIC4uLmFyZ3MpLFxuICAgICAgICAgICAgQ29tcG9uZW50LFxuICAgICAgICAgICAgeyBkaXNwbGF5TmFtZTogYWRhcHRlci5kaXNwbGF5TmFtZU9mTm9kZSh7IHR5cGU6IENvbXBvbmVudCB9KSB9LFxuICAgICAgICAgICk7XG4gICAgICAgICAgbGFzdENvbXBvbmVudCA9IENvbXBvbmVudDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gd3JhcHBlZENvbXBvbmVudDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIENvbXBvbmVudDtcbiAgICB9O1xuXG4gICAgY29uc3QgcmVuZGVyRWxlbWVudCA9IChlbENvbmZpZywgLi4ucmVzdCkgPT4ge1xuICAgICAgY29uc3QgcmVuZGVyZWRFbCA9IHJlbmRlcmVyLnJlbmRlcihlbENvbmZpZywgLi4ucmVzdCk7XG5cbiAgICAgIGNvbnN0IHR5cGVJc0V4aXN0ZWQgPSAhIShyZW5kZXJlZEVsICYmIHJlbmRlcmVkRWwudHlwZSk7XG4gICAgICBpZiAodHlwZUlzRXhpc3RlZCkge1xuICAgICAgICBjb25zdCBjbG9uZWRFbCA9IGNoZWNrSXNTdXNwZW5zZUFuZENsb25lRWxlbWVudChyZW5kZXJlZEVsLCB7IHN1c3BlbnNlRmFsbGJhY2sgfSk7XG5cbiAgICAgICAgY29uc3QgZWxlbWVudElzQ2hhbmdlZCA9IGNsb25lZEVsLnR5cGUgIT09IHJlbmRlcmVkRWwudHlwZTtcbiAgICAgICAgaWYgKGVsZW1lbnRJc0NoYW5nZWQpIHtcbiAgICAgICAgICByZXR1cm4gcmVuZGVyZXIucmVuZGVyKHsgLi4uZWxDb25maWcsIHR5cGU6IGNsb25lZEVsLnR5cGUgfSwgLi4ucmVzdCk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHJlbmRlcmVkRWw7XG4gICAgfTtcblxuICAgIHJldHVybiB7XG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgY29uc2lzdGVudC1yZXR1cm5cbiAgICAgIHJlbmRlcihlbCwgdW5tYXNrZWRDb250ZXh0LCB7XG4gICAgICAgIHByb3ZpZGVyVmFsdWVzID0gbmV3IE1hcCgpLFxuICAgICAgfSA9IHt9KSB7XG4gICAgICAgIGNhY2hlZE5vZGUgPSBlbDtcbiAgICAgICAgaWYgKHR5cGVvZiBlbC50eXBlID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIGlzRE9NID0gdHJ1ZTtcbiAgICAgICAgfSBlbHNlIGlmIChpc0NvbnRleHRQcm92aWRlcihlbCkpIHtcbiAgICAgICAgICBwcm92aWRlclZhbHVlcy5zZXQoZWwudHlwZSwgZWwucHJvcHMudmFsdWUpO1xuICAgICAgICAgIGNvbnN0IE1vY2tQcm92aWRlciA9IE9iamVjdC5hc3NpZ24oXG4gICAgICAgICAgICAocHJvcHMpID0+IHByb3BzLmNoaWxkcmVuLFxuICAgICAgICAgICAgZWwudHlwZSxcbiAgICAgICAgICApO1xuICAgICAgICAgIHJldHVybiB3aXRoU2V0U3RhdGVBbGxvd2VkKCgpID0+IHJlbmRlckVsZW1lbnQoeyAuLi5lbCwgdHlwZTogTW9ja1Byb3ZpZGVyIH0pKTtcbiAgICAgICAgfSBlbHNlIGlmIChpc0NvbnRleHRDb25zdW1lcihlbCkpIHtcbiAgICAgICAgICBjb25zdCBQcm92aWRlciA9IGFkYXB0ZXIuZ2V0UHJvdmlkZXJGcm9tQ29uc3VtZXIoZWwudHlwZSk7XG4gICAgICAgICAgY29uc3QgdmFsdWUgPSBwcm92aWRlclZhbHVlcy5oYXMoUHJvdmlkZXIpXG4gICAgICAgICAgICA/IHByb3ZpZGVyVmFsdWVzLmdldChQcm92aWRlcilcbiAgICAgICAgICAgIDogZ2V0UHJvdmlkZXJEZWZhdWx0VmFsdWUoUHJvdmlkZXIpO1xuICAgICAgICAgIGNvbnN0IE1vY2tDb25zdW1lciA9IE9iamVjdC5hc3NpZ24oXG4gICAgICAgICAgICAocHJvcHMpID0+IHByb3BzLmNoaWxkcmVuKHZhbHVlKSxcbiAgICAgICAgICAgIGVsLnR5cGUsXG4gICAgICAgICAgKTtcbiAgICAgICAgICByZXR1cm4gd2l0aFNldFN0YXRlQWxsb3dlZCgoKSA9PiByZW5kZXJFbGVtZW50KHsgLi4uZWwsIHR5cGU6IE1vY2tDb25zdW1lciB9KSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaXNET00gPSBmYWxzZTtcbiAgICAgICAgICBsZXQgcmVuZGVyZWRFbCA9IGVsO1xuICAgICAgICAgIGlmIChpc0xhenkocmVuZGVyZWRFbCkpIHtcbiAgICAgICAgICAgIHRocm93IFR5cGVFcnJvcignYFJlYWN0LmxhenlgIGlzIG5vdCBzdXBwb3J0ZWQgYnkgc2hhbGxvdyByZW5kZXJpbmcuJyk7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmVuZGVyZWRFbCA9IGNoZWNrSXNTdXNwZW5zZUFuZENsb25lRWxlbWVudChyZW5kZXJlZEVsLCB7IHN1c3BlbnNlRmFsbGJhY2sgfSk7XG4gICAgICAgICAgY29uc3QgeyB0eXBlOiBDb21wb25lbnQgfSA9IHJlbmRlcmVkRWw7XG5cbiAgICAgICAgICBjb25zdCBjb250ZXh0ID0gZ2V0TWFza2VkQ29udGV4dChDb21wb25lbnQuY29udGV4dFR5cGVzLCB1bm1hc2tlZENvbnRleHQpO1xuXG4gICAgICAgICAgaWYgKGlzTWVtbyhlbC50eXBlKSkge1xuICAgICAgICAgICAgY29uc3QgeyB0eXBlOiBJbm5lckNvbXAsIGNvbXBhcmUgfSA9IGVsLnR5cGU7XG5cbiAgICAgICAgICAgIHJldHVybiB3aXRoU2V0U3RhdGVBbGxvd2VkKCgpID0+IHJlbmRlckVsZW1lbnQoXG4gICAgICAgICAgICAgIHsgLi4uZWwsIHR5cGU6IHdyYXBQdXJlQ29tcG9uZW50KElubmVyQ29tcCwgY29tcGFyZSkgfSxcbiAgICAgICAgICAgICAgY29udGV4dCxcbiAgICAgICAgICAgICkpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGNvbnN0IGlzQ29tcG9uZW50U3RhdGVmdWwgPSBpc1N0YXRlZnVsKENvbXBvbmVudCk7XG5cbiAgICAgICAgICBpZiAoIWlzQ29tcG9uZW50U3RhdGVmdWwgJiYgdHlwZW9mIENvbXBvbmVudCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgcmV0dXJuIHdpdGhTZXRTdGF0ZUFsbG93ZWQoKCkgPT4gcmVuZGVyRWxlbWVudChcbiAgICAgICAgICAgICAgeyAuLi5yZW5kZXJlZEVsLCB0eXBlOiB3cmFwRnVuY3Rpb25hbENvbXBvbmVudChDb21wb25lbnQpIH0sXG4gICAgICAgICAgICAgIGNvbnRleHQsXG4gICAgICAgICAgICApKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBpZiAoaXNDb21wb25lbnRTdGF0ZWZ1bCkge1xuICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICByZW5kZXJlci5faW5zdGFuY2VcbiAgICAgICAgICAgICAgJiYgZWwucHJvcHMgPT09IHJlbmRlcmVyLl9pbnN0YW5jZS5wcm9wc1xuICAgICAgICAgICAgICAmJiAhc2hhbGxvd0VxdWFsKGNvbnRleHQsIHJlbmRlcmVyLl9pbnN0YW5jZS5jb250ZXh0KVxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgIGNvbnN0IHsgcmVzdG9yZSB9ID0gc3B5TWV0aG9kKFxuICAgICAgICAgICAgICAgIHJlbmRlcmVyLFxuICAgICAgICAgICAgICAgICdfdXBkYXRlQ2xhc3NDb21wb25lbnQnLFxuICAgICAgICAgICAgICAgIChvcmlnaW5hbE1ldGhvZCkgPT4gZnVuY3Rpb24gX3VwZGF0ZUNsYXNzQ29tcG9uZW50KC4uLmFyZ3MpIHtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHsgcHJvcHMgfSA9IHJlbmRlcmVyLl9pbnN0YW5jZTtcbiAgICAgICAgICAgICAgICAgIGNvbnN0IGNsb25lZFByb3BzID0geyAuLi5wcm9wcyB9O1xuICAgICAgICAgICAgICAgICAgcmVuZGVyZXIuX2luc3RhbmNlLnByb3BzID0gY2xvbmVkUHJvcHM7XG5cbiAgICAgICAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IG9yaWdpbmFsTWV0aG9kLmFwcGx5KHJlbmRlcmVyLCBhcmdzKTtcblxuICAgICAgICAgICAgICAgICAgcmVuZGVyZXIuX2luc3RhbmNlLnByb3BzID0gcHJvcHM7XG4gICAgICAgICAgICAgICAgICByZXN0b3JlKCk7XG5cbiAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gZml4IHJlYWN0IGJ1Zzsgc2VlIGltcGxlbWVudGF0aW9uIG9mIGBnZXRFbXB0eVN0YXRlVmFsdWVgXG4gICAgICAgICAgICBjb25zdCBlbXB0eVN0YXRlVmFsdWUgPSBnZXRFbXB0eVN0YXRlVmFsdWUoKTtcbiAgICAgICAgICAgIGlmIChlbXB0eVN0YXRlVmFsdWUpIHtcbiAgICAgICAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KENvbXBvbmVudC5wcm90b3R5cGUsICdzdGF0ZScsIHtcbiAgICAgICAgICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXG4gICAgICAgICAgICAgICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICBnZXQoKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHNldCh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlICE9PSBlbXB0eVN0YXRlVmFsdWUpIHtcbiAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdzdGF0ZScsIHtcbiAgICAgICAgICAgICAgICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSxcbiAgICAgICAgICAgICAgICAgICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHdpdGhTZXRTdGF0ZUFsbG93ZWQoKCkgPT4gcmVuZGVyRWxlbWVudChyZW5kZXJlZEVsLCBjb250ZXh0KSk7XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB1bm1vdW50KCkge1xuICAgICAgICByZW5kZXJlci51bm1vdW50KCk7XG4gICAgICB9LFxuICAgICAgZ2V0Tm9kZSgpIHtcbiAgICAgICAgaWYgKGlzRE9NKSB7XG4gICAgICAgICAgcmV0dXJuIGVsZW1lbnRUb1RyZWUoY2FjaGVkTm9kZSk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3Qgb3V0cHV0ID0gcmVuZGVyZXIuZ2V0UmVuZGVyT3V0cHV0KCk7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgbm9kZVR5cGU6IG5vZGVUeXBlRnJvbVR5cGUoY2FjaGVkTm9kZS50eXBlKSxcbiAgICAgICAgICB0eXBlOiBjYWNoZWROb2RlLnR5cGUsXG4gICAgICAgICAgcHJvcHM6IGNhY2hlZE5vZGUucHJvcHMsXG4gICAgICAgICAga2V5OiBlbnN1cmVLZXlPclVuZGVmaW5lZChjYWNoZWROb2RlLmtleSksXG4gICAgICAgICAgcmVmOiBjYWNoZWROb2RlLnJlZixcbiAgICAgICAgICBpbnN0YW5jZTogcmVuZGVyZXIuX2luc3RhbmNlLFxuICAgICAgICAgIHJlbmRlcmVkOiBBcnJheS5pc0FycmF5KG91dHB1dClcbiAgICAgICAgICAgID8gZmxhdHRlbihvdXRwdXQpLm1hcCgoZWwpID0+IGVsZW1lbnRUb1RyZWUoZWwpKVxuICAgICAgICAgICAgOiBlbGVtZW50VG9UcmVlKG91dHB1dCksXG4gICAgICAgIH07XG4gICAgICB9LFxuICAgICAgc2ltdWxhdGVFcnJvcihub2RlSGllcmFyY2h5LCByb290Tm9kZSwgZXJyb3IpIHtcbiAgICAgICAgc2ltdWxhdGVFcnJvcihcbiAgICAgICAgICBlcnJvcixcbiAgICAgICAgICByZW5kZXJlci5faW5zdGFuY2UsXG4gICAgICAgICAgY2FjaGVkTm9kZSxcbiAgICAgICAgICBub2RlSGllcmFyY2h5LmNvbmNhdChjYWNoZWROb2RlKSxcbiAgICAgICAgICBub2RlVHlwZUZyb21UeXBlLFxuICAgICAgICAgIGFkYXB0ZXIuZGlzcGxheU5hbWVPZk5vZGUsXG4gICAgICAgICAgY2FjaGVkTm9kZS50eXBlLFxuICAgICAgICApO1xuICAgICAgfSxcbiAgICAgIHNpbXVsYXRlRXZlbnQobm9kZSwgZXZlbnQsIC4uLmFyZ3MpIHtcbiAgICAgICAgY29uc3QgaGFuZGxlciA9IG5vZGUucHJvcHNbcHJvcEZyb21FdmVudChldmVudCwgZXZlbnRPcHRpb25zKV07XG4gICAgICAgIGlmIChoYW5kbGVyKSB7XG4gICAgICAgICAgd2l0aFNldFN0YXRlQWxsb3dlZCgoKSA9PiB7XG4gICAgICAgICAgICAvLyBUT0RPKGxtcik6IGNyZWF0ZS91c2Ugc3ludGhldGljIGV2ZW50c1xuICAgICAgICAgICAgLy8gVE9ETyhsbXIpOiBlbXVsYXRlIFJlYWN0J3MgZXZlbnQgcHJvcGFnYXRpb25cbiAgICAgICAgICAgIC8vIFJlYWN0RE9NLnVuc3RhYmxlX2JhdGNoZWRVcGRhdGVzKCgpID0+IHtcbiAgICAgICAgICAgIGhhbmRsZXIoLi4uYXJncyk7XG4gICAgICAgICAgICAvLyB9KTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIGJhdGNoZWRVcGRhdGVzKGZuKSB7XG4gICAgICAgIHJldHVybiBmbigpO1xuICAgICAgICAvLyByZXR1cm4gUmVhY3RET00udW5zdGFibGVfYmF0Y2hlZFVwZGF0ZXMoZm4pO1xuICAgICAgfSxcbiAgICAgIGNoZWNrUHJvcFR5cGVzKHR5cGVTcGVjcywgdmFsdWVzLCBsb2NhdGlvbiwgaGllcmFyY2h5KSB7XG4gICAgICAgIHJldHVybiBjaGVja1Byb3BUeXBlcyhcbiAgICAgICAgICB0eXBlU3BlY3MsXG4gICAgICAgICAgdmFsdWVzLFxuICAgICAgICAgIGxvY2F0aW9uLFxuICAgICAgICAgIGRpc3BsYXlOYW1lT2ZOb2RlKGNhY2hlZE5vZGUpLFxuICAgICAgICAgICgpID0+IGdldENvbXBvbmVudFN0YWNrKGhpZXJhcmNoeS5jb25jYXQoW2NhY2hlZE5vZGVdKSksXG4gICAgICAgICk7XG4gICAgICB9LFxuICAgIH07XG4gIH1cblxuICBjcmVhdGVTdHJpbmdSZW5kZXJlcihvcHRpb25zKSB7XG4gICAgaWYgKGhhcyhvcHRpb25zLCAnc3VzcGVuc2VGYWxsYmFjaycpKSB7XG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdgc3VzcGVuc2VGYWxsYmFja2Agc2hvdWxkIG5vdCBiZSBzcGVjaWZpZWQgaW4gb3B0aW9ucyBvZiBzdHJpbmcgcmVuZGVyZXInKTtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgIHJlbmRlcihlbCwgY29udGV4dCkge1xuICAgICAgICBpZiAob3B0aW9ucy5jb250ZXh0ICYmIChlbC50eXBlLmNvbnRleHRUeXBlcyB8fCBvcHRpb25zLmNoaWxkQ29udGV4dFR5cGVzKSkge1xuICAgICAgICAgIGNvbnN0IGNoaWxkQ29udGV4dFR5cGVzID0ge1xuICAgICAgICAgICAgLi4uKGVsLnR5cGUuY29udGV4dFR5cGVzIHx8IHt9KSxcbiAgICAgICAgICAgIC4uLm9wdGlvbnMuY2hpbGRDb250ZXh0VHlwZXMsXG4gICAgICAgICAgfTtcbiAgICAgICAgICBjb25zdCBDb250ZXh0V3JhcHBlciA9IGNyZWF0ZVJlbmRlcldyYXBwZXIoZWwsIGNvbnRleHQsIGNoaWxkQ29udGV4dFR5cGVzKTtcbiAgICAgICAgICByZXR1cm4gUmVhY3RET01TZXJ2ZXIucmVuZGVyVG9TdGF0aWNNYXJrdXAoUmVhY3QuY3JlYXRlRWxlbWVudChDb250ZXh0V3JhcHBlcikpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBSZWFjdERPTVNlcnZlci5yZW5kZXJUb1N0YXRpY01hcmt1cChlbCk7XG4gICAgICB9LFxuICAgIH07XG4gIH1cblxuICAvLyBQcm92aWRlZCBhIGJhZyBvZiBvcHRpb25zLCByZXR1cm4gYW4gYEVuenltZVJlbmRlcmVyYC4gU29tZSBvcHRpb25zIGNhbiBiZSBpbXBsZW1lbnRhdGlvblxuICAvLyBzcGVjaWZpYywgbGlrZSBgYXR0YWNoYCBldGMuIGZvciBSZWFjdCwgYnV0IG5vdCBwYXJ0IG9mIHRoaXMgaW50ZXJmYWNlIGV4cGxpY2l0bHkuXG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzXG4gIGNyZWF0ZVJlbmRlcmVyKG9wdGlvbnMpIHtcbiAgICBzd2l0Y2ggKG9wdGlvbnMubW9kZSkge1xuICAgICAgY2FzZSBFbnp5bWVBZGFwdGVyLk1PREVTLk1PVU5UOiByZXR1cm4gdGhpcy5jcmVhdGVNb3VudFJlbmRlcmVyKG9wdGlvbnMpO1xuICAgICAgY2FzZSBFbnp5bWVBZGFwdGVyLk1PREVTLlNIQUxMT1c6IHJldHVybiB0aGlzLmNyZWF0ZVNoYWxsb3dSZW5kZXJlcihvcHRpb25zKTtcbiAgICAgIGNhc2UgRW56eW1lQWRhcHRlci5NT0RFUy5TVFJJTkc6IHJldHVybiB0aGlzLmNyZWF0ZVN0cmluZ1JlbmRlcmVyKG9wdGlvbnMpO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBFbnp5bWUgSW50ZXJuYWwgRXJyb3I6IFVucmVjb2duaXplZCBtb2RlOiAke29wdGlvbnMubW9kZX1gKTtcbiAgICB9XG4gIH1cblxuICB3cmFwKGVsZW1lbnQpIHtcbiAgICByZXR1cm4gd3JhcChlbGVtZW50KTtcbiAgfVxuXG4gIC8vIGNvbnZlcnRzIGFuIFJTVE5vZGUgdG8gdGhlIGNvcnJlc3BvbmRpbmcgSlNYIFByYWdtYSBFbGVtZW50LiBUaGlzIHdpbGwgYmUgbmVlZGVkXG4gIC8vIGluIG9yZGVyIHRvIGltcGxlbWVudCB0aGUgYFdyYXBwZXIubW91bnQoKWAgYW5kIGBXcmFwcGVyLnNoYWxsb3coKWAgbWV0aG9kcywgYnV0IHNob3VsZFxuICAvLyBiZSBwcmV0dHkgc3RyYWlnaHRmb3J3YXJkIGZvciBwZW9wbGUgdG8gaW1wbGVtZW50LlxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgY2xhc3MtbWV0aG9kcy11c2UtdGhpc1xuICBub2RlVG9FbGVtZW50KG5vZGUpIHtcbiAgICBpZiAoIW5vZGUgfHwgdHlwZW9mIG5vZGUgIT09ICdvYmplY3QnKSByZXR1cm4gbnVsbDtcbiAgICBjb25zdCB7IHR5cGUgfSA9IG5vZGU7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQodW5tZW1vVHlwZSh0eXBlKSwgcHJvcHNXaXRoS2V5c0FuZFJlZihub2RlKSk7XG4gIH1cblxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgY2xhc3MtbWV0aG9kcy11c2UtdGhpc1xuICBtYXRjaGVzRWxlbWVudFR5cGUobm9kZSwgbWF0Y2hpbmdUeXBlKSB7XG4gICAgaWYgKCFub2RlKSB7XG4gICAgICByZXR1cm4gbm9kZTtcbiAgICB9XG4gICAgY29uc3QgeyB0eXBlIH0gPSBub2RlO1xuICAgIHJldHVybiB1bm1lbW9UeXBlKHR5cGUpID09PSB1bm1lbW9UeXBlKG1hdGNoaW5nVHlwZSk7XG4gIH1cblxuICBlbGVtZW50VG9Ob2RlKGVsZW1lbnQpIHtcbiAgICByZXR1cm4gZWxlbWVudFRvVHJlZShlbGVtZW50KTtcbiAgfVxuXG4gIG5vZGVUb0hvc3ROb2RlKG5vZGUsIHN1cHBvcnRzQXJyYXkgPSBmYWxzZSkge1xuICAgIGNvbnN0IG5vZGVzID0gbm9kZVRvSG9zdE5vZGUobm9kZSk7XG4gICAgaWYgKEFycmF5LmlzQXJyYXkobm9kZXMpICYmICFzdXBwb3J0c0FycmF5KSB7XG4gICAgICByZXR1cm4gbm9kZXNbMF07XG4gICAgfVxuICAgIHJldHVybiBub2RlcztcbiAgfVxuXG4gIGRpc3BsYXlOYW1lT2ZOb2RlKG5vZGUpIHtcbiAgICBpZiAoIW5vZGUpIHJldHVybiBudWxsO1xuICAgIGNvbnN0IHsgdHlwZSwgJCR0eXBlb2YgfSA9IG5vZGU7XG4gICAgY29uc3QgYWRhcHRlciA9IHRoaXM7XG5cbiAgICBjb25zdCBub2RlVHlwZSA9IHR5cGUgfHwgJCR0eXBlb2Y7XG5cbiAgICAvLyBuZXdlciBub2RlIHR5cGVzIG1heSBiZSB1bmRlZmluZWQsIHNvIG9ubHkgdGVzdCBpZiB0aGUgbm9kZVR5cGUgZXhpc3RzXG4gICAgaWYgKG5vZGVUeXBlKSB7XG4gICAgICBzd2l0Y2ggKG5vZGVUeXBlKSB7XG4gICAgICAgIGNhc2UgQ29uY3VycmVudE1vZGUgfHwgTmFOOiByZXR1cm4gJ0NvbmN1cnJlbnRNb2RlJztcbiAgICAgICAgY2FzZSBGcmFnbWVudCB8fCBOYU46IHJldHVybiAnRnJhZ21lbnQnO1xuICAgICAgICBjYXNlIFN0cmljdE1vZGUgfHwgTmFOOiByZXR1cm4gJ1N0cmljdE1vZGUnO1xuICAgICAgICBjYXNlIFByb2ZpbGVyIHx8IE5hTjogcmV0dXJuICdQcm9maWxlcic7XG4gICAgICAgIGNhc2UgUG9ydGFsIHx8IE5hTjogcmV0dXJuICdQb3J0YWwnO1xuICAgICAgICBjYXNlIFN1c3BlbnNlIHx8IE5hTjogcmV0dXJuICdTdXNwZW5zZSc7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICB9XG4gICAgfVxuXG4gICAgY29uc3QgJCR0eXBlb2ZUeXBlID0gdHlwZSAmJiB0eXBlLiQkdHlwZW9mO1xuXG4gICAgc3dpdGNoICgkJHR5cGVvZlR5cGUpIHtcbiAgICAgIGNhc2UgQ29udGV4dENvbnN1bWVyIHx8IE5hTjogcmV0dXJuICdDb250ZXh0Q29uc3VtZXInO1xuICAgICAgY2FzZSBDb250ZXh0UHJvdmlkZXIgfHwgTmFOOiByZXR1cm4gJ0NvbnRleHRQcm92aWRlcic7XG4gICAgICBjYXNlIE1lbW8gfHwgTmFOOiB7XG4gICAgICAgIGNvbnN0IG5vZGVOYW1lID0gZGlzcGxheU5hbWVPZk5vZGUobm9kZSk7XG4gICAgICAgIHJldHVybiB0eXBlb2Ygbm9kZU5hbWUgPT09ICdzdHJpbmcnID8gbm9kZU5hbWUgOiBgTWVtbygke2FkYXB0ZXIuZGlzcGxheU5hbWVPZk5vZGUodHlwZSl9KWA7XG4gICAgICB9XG4gICAgICBjYXNlIEZvcndhcmRSZWYgfHwgTmFOOiB7XG4gICAgICAgIGlmICh0eXBlLmRpc3BsYXlOYW1lKSB7XG4gICAgICAgICAgcmV0dXJuIHR5cGUuZGlzcGxheU5hbWU7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgbmFtZSA9IGFkYXB0ZXIuZGlzcGxheU5hbWVPZk5vZGUoeyB0eXBlOiB0eXBlLnJlbmRlciB9KTtcbiAgICAgICAgcmV0dXJuIG5hbWUgPyBgRm9yd2FyZFJlZigke25hbWV9KWAgOiAnRm9yd2FyZFJlZic7XG4gICAgICB9XG4gICAgICBjYXNlIExhenkgfHwgTmFOOiB7XG4gICAgICAgIHJldHVybiAnbGF6eSc7XG4gICAgICB9XG4gICAgICBkZWZhdWx0OiByZXR1cm4gZGlzcGxheU5hbWVPZk5vZGUobm9kZSk7XG4gICAgfVxuICB9XG5cbiAgaXNWYWxpZEVsZW1lbnQoZWxlbWVudCkge1xuICAgIHJldHVybiBpc0VsZW1lbnQoZWxlbWVudCk7XG4gIH1cblxuICBpc1ZhbGlkRWxlbWVudFR5cGUob2JqZWN0KSB7XG4gICAgcmV0dXJuICEhb2JqZWN0ICYmIGlzVmFsaWRFbGVtZW50VHlwZShvYmplY3QpO1xuICB9XG5cbiAgaXNGcmFnbWVudChmcmFnbWVudCkge1xuICAgIHJldHVybiB0eXBlT2ZOb2RlKGZyYWdtZW50KSA9PT0gRnJhZ21lbnQ7XG4gIH1cblxuICBpc0N1c3RvbUNvbXBvbmVudCh0eXBlKSB7XG4gICAgY29uc3QgZmFrZUVsZW1lbnQgPSBtYWtlRmFrZUVsZW1lbnQodHlwZSk7XG4gICAgcmV0dXJuICEhdHlwZSAmJiAoXG4gICAgICB0eXBlb2YgdHlwZSA9PT0gJ2Z1bmN0aW9uJ1xuICAgICAgfHwgaXNGb3J3YXJkUmVmKGZha2VFbGVtZW50KVxuICAgICAgfHwgaXNDb250ZXh0UHJvdmlkZXIoZmFrZUVsZW1lbnQpXG4gICAgICB8fCBpc0NvbnRleHRDb25zdW1lcihmYWtlRWxlbWVudClcbiAgICAgIHx8IGlzU3VzcGVuc2UoZmFrZUVsZW1lbnQpXG4gICAgKTtcbiAgfVxuXG4gIGlzQ29udGV4dENvbnN1bWVyKHR5cGUpIHtcbiAgICByZXR1cm4gISF0eXBlICYmIGlzQ29udGV4dENvbnN1bWVyKG1ha2VGYWtlRWxlbWVudCh0eXBlKSk7XG4gIH1cblxuICBpc0N1c3RvbUNvbXBvbmVudEVsZW1lbnQoaW5zdCkge1xuICAgIGlmICghaW5zdCB8fCAhdGhpcy5pc1ZhbGlkRWxlbWVudChpbnN0KSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5pc0N1c3RvbUNvbXBvbmVudChpbnN0LnR5cGUpO1xuICB9XG5cbiAgZ2V0UHJvdmlkZXJGcm9tQ29uc3VtZXIoQ29uc3VtZXIpIHtcbiAgICAvLyBSZWFjdCBzdG9yZXMgcmVmZXJlbmNlcyB0byB0aGUgUHJvdmlkZXIgb24gYSBDb25zdW1lciBkaWZmZXJlbnRseSBhY3Jvc3MgdmVyc2lvbnMuXG4gICAgaWYgKENvbnN1bWVyKSB7XG4gICAgICBsZXQgUHJvdmlkZXI7XG4gICAgICBpZiAoQ29uc3VtZXIuX2NvbnRleHQpIHsgLy8gY2hlY2sgdGhpcyBmaXJzdCwgdG8gYXZvaWQgYSBkZXByZWNhdGlvbiB3YXJuaW5nXG4gICAgICAgICh7IFByb3ZpZGVyIH0gPSBDb25zdW1lci5fY29udGV4dCk7XG4gICAgICB9IGVsc2UgaWYgKENvbnN1bWVyLlByb3ZpZGVyKSB7XG4gICAgICAgICh7IFByb3ZpZGVyIH0gPSBDb25zdW1lcik7XG4gICAgICB9XG4gICAgICBpZiAoUHJvdmlkZXIpIHtcbiAgICAgICAgcmV0dXJuIFByb3ZpZGVyO1xuICAgICAgfVxuICAgIH1cbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0VuenltZSBJbnRlcm5hbCBFcnJvcjogY2Fu4oCZdCBmaWd1cmUgb3V0IGhvdyB0byBnZXQgUHJvdmlkZXIgZnJvbSBDb25zdW1lcicpO1xuICB9XG5cbiAgY3JlYXRlRWxlbWVudCguLi5hcmdzKSB7XG4gICAgcmV0dXJuIFJlYWN0LmNyZWF0ZUVsZW1lbnQoLi4uYXJncyk7XG4gIH1cblxuICB3cmFwV2l0aFdyYXBwaW5nQ29tcG9uZW50KG5vZGUsIG9wdGlvbnMpIHtcbiAgICByZXR1cm4ge1xuICAgICAgUm9vdEZpbmRlcixcbiAgICAgIG5vZGU6IHdyYXBXaXRoV3JhcHBpbmdDb21wb25lbnQoUmVhY3QuY3JlYXRlRWxlbWVudCwgbm9kZSwgb3B0aW9ucyksXG4gICAgfTtcbiAgfVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IFJlYWN0U2V2ZW50ZWVuQWRhcHRlcjtcbiJdfQ==
//# sourceMappingURL=ReactSeventeenAdapter.js.map