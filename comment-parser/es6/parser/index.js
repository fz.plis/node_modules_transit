import { splitLines } from '../util';
import blockParser from './block-parser';
import sourceParser from './source-parser';
import specParser from './spec-parser';
import tokenizeTag from './tokenizers/tag';
import tokenizeType from './tokenizers/type';
import tokenizeName from './tokenizers/name';
import tokenizeDescription, { getJoiner as getDescriptionJoiner, } from './tokenizers/description';
export default function getParser({ startLine = 0, fence = '```', spacing = 'compact', tokenizers = [
    tokenizeTag(),
    tokenizeType(spacing),
    tokenizeName(),
    tokenizeDescription(spacing),
], } = {}) {
    if (startLine < 0 || startLine % 1 > 0)
        throw new Error('Invalid startLine');
    const parseSource = sourceParser({ startLine });
    const parseBlock = blockParser({ fence });
    const parseSpec = specParser({ tokenizers });
    const joinDescription = getDescriptionJoiner(spacing);
    const notEmpty = (line) => line.tokens.description.trim() != '';
    return function (source) {
        const blocks = [];
        for (const line of splitLines(source)) {
            const lines = parseSource(line);
            if (lines === null)
                continue;
            if (lines.find(notEmpty) === undefined)
                continue;
            const sections = parseBlock(lines);
            const specs = sections.slice(1).map(parseSpec);
            blocks.push({
                description: joinDescription(sections[0]),
                tags: specs,
                source: lines,
                problems: specs.reduce((acc, spec) => acc.concat(spec.problems), []),
            });
        }
        return blocks;
    };
}
