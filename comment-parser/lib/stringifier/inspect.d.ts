import { Block } from '../primitives';
export default function inspect({ source }: Block): string;
