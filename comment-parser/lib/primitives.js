"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Markers = void 0;
var Markers;
(function (Markers) {
    Markers["start"] = "/**";
    Markers["nostart"] = "/***";
    Markers["delim"] = "*";
    Markers["end"] = "*/";
})(Markers = exports.Markers || (exports.Markers = {}));
