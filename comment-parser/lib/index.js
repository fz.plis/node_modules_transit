"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.tokenizers = exports.transforms = exports.inspect = exports.stringify = exports.parse = void 0;
const index_1 = require("./parser/index");
const description_1 = require("./parser/tokenizers/description");
const name_1 = require("./parser/tokenizers/name");
const tag_1 = require("./parser/tokenizers/tag");
const type_1 = require("./parser/tokenizers/type");
const index_2 = require("./stringifier/index");
const align_1 = require("./transforms/align");
const indent_1 = require("./transforms/indent");
const index_3 = require("./transforms/index");
function parse(source, options = {}) {
    return index_1.default(options)(source);
}
exports.parse = parse;
exports.stringify = index_2.default();
var inspect_1 = require("./stringifier/inspect");
Object.defineProperty(exports, "inspect", { enumerable: true, get: function () { return inspect_1.default; } });
exports.transforms = {
    flow: index_3.flow,
    align: align_1.default,
    indent: indent_1.default,
};
exports.tokenizers = {
    tag: tag_1.default,
    type: type_1.default,
    name: name_1.default,
    description: description_1.default,
};
