import { Transform } from './index';
export default function indent(pos: number): Transform;
