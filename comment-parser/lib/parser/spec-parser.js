"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../util");
function getParser({ tokenizers }) {
    return function parseSpec(source) {
        var _a;
        let spec = util_1.seedSpec({ source });
        for (const tokenize of tokenizers) {
            spec = tokenize(spec);
            if ((_a = spec.problems[spec.problems.length - 1]) === null || _a === void 0 ? void 0 : _a.critical)
                break;
        }
        return spec;
    };
}
exports.default = getParser;
