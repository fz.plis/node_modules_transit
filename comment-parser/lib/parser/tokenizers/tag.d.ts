import { Tokenizer } from './index';
/**
 * Splits the `@prefix` from remaining `Spec.lines[].token.descrioption` into the `tag` token,
 * and populates `spec.tag`
 */
export default function tagTokenizer(): Tokenizer;
