import type { TsJestPresets } from '../dist/types'

declare const _default: {
  defaults: TsJestPresets;
  jsWithTs: TsJestPresets;
  jsWithBabel: TsJestPresets;
};
export = _default;
