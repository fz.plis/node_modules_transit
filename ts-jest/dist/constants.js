"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DEFAULT_JEST_TEST_MATCH = exports.DECLARATION_TYPE_EXT = exports.JS_JSX_REGEX = exports.TS_TSX_REGEX = exports.LINE_FEED = void 0;
exports.LINE_FEED = '\n';
exports.TS_TSX_REGEX = /\.tsx?$/;
exports.JS_JSX_REGEX = /\.jsx?$/;
exports.DECLARATION_TYPE_EXT = '.d.ts';
exports.DEFAULT_JEST_TEST_MATCH = ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[jt]s?(x)'];
