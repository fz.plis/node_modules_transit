import type { Config } from '@jest/types';
import type * as _babel from 'babel__core';
import type * as _ts from 'typescript';
import type { RawCompilerOptions } from './tsconfig-raw';
export declare type TTypeScript = typeof _ts;
export declare type BabelConfig = _babel.TransformOptions;
export interface AstTransformer<T = Record<string, unknown>> {
    path: string;
    options?: T;
}
export interface ConfigCustomTransformer {
    before?: (string | AstTransformer)[];
    after?: (string | AstTransformer)[];
    afterDeclarations?: (string | AstTransformer)[];
}
export interface TsJestGlobalOptions {
    tsConfig?: boolean | string | RawCompilerOptions;
    tsconfig?: boolean | string | RawCompilerOptions;
    packageJson?: boolean | string | Record<string, unknown>;
    isolatedModules?: boolean;
    compiler?: string;
    astTransformers?: string[] | ConfigCustomTransformer;
    diagnostics?: boolean | {
        pretty?: boolean;
        ignoreCodes?: number | string | (number | string)[];
        pathRegex?: RegExp | string;
        exclude?: Config.Glob[];
        warnOnly?: boolean;
    };
    babelConfig?: boolean | string | BabelConfig;
    stringifyContentPathRegex?: string | RegExp;
}
export interface GlobalConfigTsJest extends Config.ConfigGlobals {
    'ts-jest': TsJestGlobalOptions;
}
export interface InitialOptionsTsJest extends Config.InitialOptions {
    globals?: GlobalConfigTsJest;
}
export declare type TsJestPresets = Pick<Config.InitialOptions, 'moduleFileExtensions' | 'transform' | 'testMatch'>;
export interface TsJestDiagnosticsCfg {
    pretty: boolean;
    ignoreCodes: number[];
    pathRegex?: string | undefined;
    exclude: Config.Glob[];
    throws: boolean;
    warnOnly?: boolean;
}
export interface TsCompiler {
    program: _ts.Program | undefined;
}
