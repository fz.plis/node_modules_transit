"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initializeLanguageServiceInstance = void 0;
var bs_logger_1 = require("bs-logger");
var fs_1 = require("fs");
var path_1 = require("path");
var memoize = require("lodash/memoize");
var mkdirp = require("mkdirp");
var constants_1 = require("../constants");
var messages_1 = require("../utils/messages");
var json_1 = require("../utils/json");
var sha1_1 = require("../utils/sha1");
function doTypeChecking(configs, diagnosedFiles, fileName, service, logger) {
    if (configs.shouldReportDiagnostics(fileName)) {
        var diagnostics = service.getSemanticDiagnostics(fileName).concat(service.getSyntacticDiagnostics(fileName));
        diagnosedFiles.push(fileName);
        configs.raiseDiagnostics(diagnostics, fileName, logger);
    }
}
exports.initializeLanguageServiceInstance = function (configs, logger) {
    var _a;
    logger.debug('initializeLanguageServiceInstance(): create typescript compiler');
    var ts = configs.compilerModule;
    var cwd = configs.cwd;
    var cacheDir = configs.tsCacheDir;
    var _b = configs.parsedTsConfig, options = _b.options, fileNames = _b.fileNames;
    var diagnosedFiles = [];
    var serviceHostTraceCtx = (_a = {
            namespace: 'ts:serviceHost',
            call: null
        },
        _a[bs_logger_1.LogContexts.logLevel] = bs_logger_1.LogLevels.trace,
        _a);
    var memoryCache = {
        files: new Map(),
        resolvedModules: new Map(),
    };
    var tsResolvedModulesCachePath;
    if (cacheDir) {
        mkdirp.sync(cacheDir);
        tsResolvedModulesCachePath = path_1.join(cacheDir, sha1_1.sha1('ts-jest-resolved-modules', '\x00'));
        try {
            var cachedTSResolvedModules = fs_1.readFileSync(tsResolvedModulesCachePath, 'utf-8');
            memoryCache.resolvedModules = new Map(json_1.parse(cachedTSResolvedModules));
        }
        catch (e) { }
    }
    configs.parsedTsConfig.fileNames
        .filter(function (fileName) { return constants_1.TS_TSX_REGEX.test(path_1.extname(fileName)) && !configs.isTestFile(fileName); })
        .forEach(function (fileName) {
        memoryCache.files.set(fileName, {
            version: 0,
        });
    });
    function isFileInCache(fileName) {
        return memoryCache.files.has(fileName) && memoryCache.files.get(fileName).version !== 0;
    }
    var cacheReadFile = logger.wrap(serviceHostTraceCtx, 'readFile', memoize(ts.sys.readFile));
    var moduleResolutionHost = {
        fileExists: memoize(ts.sys.fileExists),
        readFile: cacheReadFile,
        directoryExists: memoize(ts.sys.directoryExists),
        getCurrentDirectory: function () { return cwd; },
        realpath: ts.sys.realpath && memoize(ts.sys.realpath),
        getDirectories: memoize(ts.sys.getDirectories),
    };
    var moduleResolutionCache = ts.createModuleResolutionCache(cwd, function (x) { return x; }, options);
    function resolveModuleNames(moduleNames, containingFile) {
        var _a;
        var normalizedContainingFile = path_1.normalize(containingFile);
        var currentResolvedModules = (_a = memoryCache.resolvedModules.get(normalizedContainingFile)) !== null && _a !== void 0 ? _a : [];
        return moduleNames.map(function (moduleName) {
            var resolveModuleName = ts.resolveModuleName(moduleName, containingFile, options, moduleResolutionHost, moduleResolutionCache);
            var resolvedModule = resolveModuleName.resolvedModule;
            if (configs.isTestFile(normalizedContainingFile) && resolvedModule) {
                var normalizedResolvedFileName = path_1.normalize(resolvedModule.resolvedFileName);
                if (!currentResolvedModules.includes(normalizedResolvedFileName)) {
                    currentResolvedModules.push(normalizedResolvedFileName);
                    memoryCache.resolvedModules.set(normalizedContainingFile, currentResolvedModules);
                }
            }
            return resolvedModule;
        });
    }
    var projectVersion = 1;
    var updateMemoryCache = function (contents, fileName) {
        logger.debug({ fileName: fileName }, 'updateMemoryCache(): update memory cache for language service');
        var shouldIncrementProjectVersion = false;
        var hit = isFileInCache(fileName);
        if (!hit) {
            memoryCache.files.set(fileName, {
                text: contents,
                version: 1,
            });
            shouldIncrementProjectVersion = true;
        }
        else {
            var previousContents = memoryCache.files.get(fileName).text;
            if (previousContents !== contents) {
                memoryCache.files.set(fileName, {
                    text: contents,
                    version: memoryCache.files.get(fileName).version + 1,
                });
                if (hit)
                    shouldIncrementProjectVersion = true;
            }
            if (!fileNames.includes(fileName)) {
                shouldIncrementProjectVersion = true;
            }
        }
        if (shouldIncrementProjectVersion)
            projectVersion++;
    };
    var serviceHost = {
        getProjectVersion: function () { return String(projectVersion); },
        getScriptFileNames: function () { return __spread(memoryCache.files.keys()); },
        getScriptVersion: function (fileName) {
            var _a;
            var normalizedFileName = path_1.normalize(fileName);
            var version = (_a = memoryCache.files.get(normalizedFileName)) === null || _a === void 0 ? void 0 : _a.version;
            return version === undefined ? undefined : String(version);
        },
        getScriptSnapshot: function (fileName) {
            var _a;
            var normalizedFileName = path_1.normalize(fileName);
            var hit = isFileInCache(normalizedFileName);
            logger.trace({ normalizedFileName: normalizedFileName, cacheHit: hit }, 'getScriptSnapshot():', 'cache', hit ? 'hit' : 'miss');
            if (!hit) {
                memoryCache.files.set(normalizedFileName, {
                    text: cacheReadFile(normalizedFileName),
                    version: 1,
                });
            }
            var contents = (_a = memoryCache.files.get(normalizedFileName)) === null || _a === void 0 ? void 0 : _a.text;
            if (contents === undefined)
                return;
            return ts.ScriptSnapshot.fromString(contents);
        },
        fileExists: memoize(ts.sys.fileExists),
        readFile: cacheReadFile,
        readDirectory: memoize(ts.sys.readDirectory),
        getDirectories: memoize(ts.sys.getDirectories),
        directoryExists: memoize(ts.sys.directoryExists),
        realpath: ts.sys.realpath && memoize(ts.sys.realpath),
        getNewLine: function () { return constants_1.LINE_FEED; },
        getCurrentDirectory: function () { return cwd; },
        getCompilationSettings: function () { return options; },
        getDefaultLibFileName: function () { return ts.getDefaultLibFilePath(options); },
        getCustomTransformers: function () { return configs.customTransformers; },
        resolveModuleNames: resolveModuleNames,
    };
    logger.debug('initializeLanguageServiceInstance(): creating language service');
    var service = ts.createLanguageService(serviceHost, ts.createDocumentRegistry());
    return {
        compileFn: function (code, fileName) {
            var e_1, _a;
            var _b;
            logger.debug({ fileName: fileName }, 'compileFn(): compiling using language service');
            updateMemoryCache(code, fileName);
            var output = service.getEmitOutput(fileName);
            if (tsResolvedModulesCachePath) {
                void (function () { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4, fs_1.writeFile(tsResolvedModulesCachePath, json_1.stringify(__spread(memoryCache.resolvedModules)), function () { })];
                            case 1:
                                _a.sent();
                                return [2];
                        }
                    });
                }); })();
            }
            if (!diagnosedFiles.includes(fileName)) {
                logger.debug({ fileName: fileName }, 'compileFn(): computing diagnostics using language service');
                doTypeChecking(configs, diagnosedFiles, fileName, service, logger);
            }
            if (!configs.isTestFile(fileName)) {
                try {
                    for (var _c = __values(memoryCache.resolvedModules.entries()), _d = _c.next(); !_d.done; _d = _c.next()) {
                        var _e = __read(_d.value, 2), testFileName = _e[0], resolvedModules = _e[1];
                        if (resolvedModules.includes(fileName) &&
                            !diagnosedFiles.includes(testFileName) &&
                            fs_1.existsSync(testFileName)) {
                            var testFileContent = (_b = memoryCache.files.get(testFileName)) === null || _b === void 0 ? void 0 : _b.text;
                            if (!testFileContent) {
                                updateMemoryCache(cacheReadFile(testFileName), testFileName);
                            }
                            logger.debug({ testFileName: testFileName }, 'compileFn(): computing diagnostics using language service for test file which uses the module');
                            doTypeChecking(configs, diagnosedFiles, testFileName, service, logger);
                        }
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }
            if (output.emitSkipped) {
                logger.warn(messages_1.interpolate("Unable to process '{{file}}', falling back to original file content. Please make sure that `outDir` in your tsconfig is neither `''` or `'.'`. You can also configure Jest config option `transformIgnorePatterns` to ignore {{file}} from transformation", { file: fileName }));
                return [code, '{}'];
            }
            if (!output.outputFiles.length) {
                throw new TypeError(messages_1.interpolate("Unable to require `.d.ts` file for file: {{file}}.\nThis is usually the result of a faulty configuration or import. Make sure there is a `.js`, `.json` or another executable extension available alongside `{{file}}`.", {
                    file: path_1.basename(fileName),
                }));
            }
            return [output.outputFiles[1].text, output.outputFiles[0].text];
        },
        program: service.getProgram(),
    };
};
