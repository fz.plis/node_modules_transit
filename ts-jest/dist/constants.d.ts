export declare const LINE_FEED = "\n";
export declare const TS_TSX_REGEX: RegExp;
export declare const JS_JSX_REGEX: RegExp;
export declare const DECLARATION_TYPE_EXT = ".d.ts";
