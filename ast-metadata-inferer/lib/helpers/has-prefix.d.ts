/**
 * Determine if a css or javascript attribute is vendor-prefixed
 */
export declare const vendorPrefixMappings: {
    chrome: string;
    safari: string;
    firefox: string;
    edge: string;
    ie: string;
};
export declare const prefixes: string[];
/**
 * Determine if a css or js value is prefixed
 * ex. HasPrefix('document.mozOffscreenWidth()') => true
 * ex. HasPrefix('document.offscreenWidth()') => false
 */
export default function HasPrefix(property: string): boolean;
//# sourceMappingURL=has-prefix.d.ts.map