"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = HasPrefix;
exports.prefixes = exports.vendorPrefixMappings = void 0;

/**
 * Determine if a css or javascript attribute is vendor-prefixed
 */
const vendorPrefixMappings = {
  chrome: "webkit",
  safari: "webkit",
  firefox: "moz",
  edge: "ms",
  ie: "ms"
};
exports.vendorPrefixMappings = vendorPrefixMappings;
const prefixes = Object.values(vendorPrefixMappings);
/**
 * Determine if a css or js value is prefixed
 * ex. HasPrefix('document.mozOffscreenWidth()') => true
 * ex. HasPrefix('document.offscreenWidth()') => false
 */

exports.prefixes = prefixes;

function HasPrefix(property) {
  const lowerCaseProperty = property.toLowerCase(); // $FlowFixMe: Waiting on github.com/facebook/flow/issues/2174

  return prefixes.some(prefix => lowerCaseProperty.includes(prefix));
}