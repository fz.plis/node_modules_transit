/**
 * Map webidl definition names to prototype chain parent
 * ex. Console -> console
 *
 * This helps generate protoChain's and protoChainId's
 * ex. Console.log -> console.log
 */
export default function interceptAndNormalize(parentObjectId: string): string;
//# sourceMappingURL=normalize-protochain.d.ts.map