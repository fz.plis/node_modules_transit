import { ApiMetadata } from "../../types";
/**
 * Comvert camelcase phrases to hypen-separated words
 * ex. camelCase => camel-case
 * This is used to map CSS DOM API names to css properties and attributes
 */
export declare function camelCaseToHyphen(string: string): string;
/**
 * @TODO: Allow overriding database records
 */
export default function MicrosoftAPICatalogProvider(): Array<ApiMetadata>;
//# sourceMappingURL=index.d.ts.map