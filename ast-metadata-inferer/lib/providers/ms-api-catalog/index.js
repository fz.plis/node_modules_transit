"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.camelCaseToHyphen = camelCaseToHyphen;
exports.default = MicrosoftAPICatalogProvider;

var _microsoftApiCatalogData = _interopRequireDefault(require("./microsoft-api-catalog-data.json"));

var _hasPrefix = _interopRequireDefault(require("../../helpers/has-prefix"));

var _normalizeProtochain = _interopRequireDefault(require("../../helpers/normalize-protochain"));

var _types = require("../../types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-nocheck

/**
 * Comvert camelcase phrases to hypen-separated words
 * ex. camelCase => camel-case
 * This is used to map CSS DOM API names to css properties and attributes
 */
function camelCaseToHyphen(string) {
  return Array // Covert string to array
  .from(string) // If char `X` is uppercase, map it to `-x`
  .map(curr => curr === curr.toUpperCase() ? `-${curr.toLowerCase()}` : curr, []).join("");
}
/**
 * @TODO: Allow overriding database records
 */


function MicrosoftAPICatalogProvider() {
  const formattedRecords = [];
  const ignoredAPIs = ["arguments", "caller", "constructor", "length", "name", "prototype"]; // Convert two dimentional records to single dimentional array

  _microsoftApiCatalogData.default.forEach(record => {
    formattedRecords.push({ ...record,
      parentName: record.name,
      protoChain: [(0, _normalizeProtochain.default)(record.name)],
      protoChainId: (0, _normalizeProtochain.default)(record.name),
      spec: record.spec || false,
      webidlId: record.name
    });
    record.apis.forEach(api => {
      // @TODO: Properly strip vendor prefixes and check if non-prefixed API
      //        exists. If not, create the record for it
      formattedRecords.push({ ...api,
        spec: record.spec || false,
        parentName: record.name
      });
    });
  });

  const JSAPIs = formattedRecords // Filter all CSS records. For some reason reason, MicrosoftAPICatalog does not report
  // the correctly. Validate that the record's name is a string. Some record
  // names are numbers from some odd reason
  .filter(formattedRecord => !formattedRecord.name.includes("-") && formattedRecord.parentName !== "CSS2Properties" && Number.isNaN(parseInt(formattedRecord.name, 10)) && typeof formattedRecord.spec !== "undefined").map(formattedRecord => {
    const protoChain = (formattedRecord.protoChain || [(0, _normalizeProtochain.default)(formattedRecord.parentName), formattedRecord.name]). // Remove 'window' from the protochain
    filter(e => e !== "window");
    return {
      id: formattedRecord.name,
      name: formattedRecord.name,
      specNames: formattedRecord.specNames,
      language: _types.Language.JS,
      specIsFinished: formattedRecord.spec,
      protoChain,
      protoChainId: protoChain.join("."),
      compat: {}
    };
  }).filter(record => record.name !== "defaultStatus" && record.protoChain.length !== 0 && !ignoredAPIs.includes(record.name) && !(0, _hasPrefix.default)(record.name) && !(0, _hasPrefix.default)(record.protoChainId) && !(0, _hasPrefix.default)(record.id)); // Find the CSS DOM API's and use them create the css style records
  // const CSSAPIs = JSAPIs
  //   .filter(record => record.protoChain.includes('CSSStyleDeclaration'))
  //   .map(record => ({
  //     ...record,
  //     id: camelCaseToHyphen(record.name),
  //     name: camelCaseToHyphen(record.name),
  //   }));
  // return [...CSSAPIs, ...JSAPIs];

  return JSAPIs;
}