"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Providers;

var _mdn = _interopRequireDefault(require("./mdn"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

async function Providers() {
  const [mdnRecords] = await Promise.all([(0, _mdn.default)()]);
  const map = new Map(mdnRecords.map(record => [record.protoChainId, record]));
  return Array.from(map.values()).filter(record => !record.protoChain.includes("RegExp") && !record.protoChainId.includes("@@"));
}