"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = mdnComaptDataProvider;

var _mdnBrowserCompatData = _interopRequireDefault(require("mdn-browser-compat-data"));

var _normalizeProtochain = _interopRequireDefault(require("../../helpers/normalize-protochain"));

var _types = require("../../types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint @typescript-eslint/ban-ts-ignore: off, no-underscore-dangle: off */
// `version_added: true` or `version_added: "some browser version number"`
// means that the feature has been implemented in the browser. When `true`,
// a specific version is unknown. `version_added: false` means that the browser
// does not support the feature, and never has. `version_added: null` means that
// we have no idea if the browser has support for the feature. (A major goal is to
// get rid of as many of the `null` values we can and replace them with real data
// from the browsers.)
//
// See https://github.com/mdn/browser-compat-data/issues/3425#issuecomment-462176276
function mdnComaptDataProvider() {
  const apiMetadata = [];
  const normalizedBrowserCompatApis = [...Object.entries(_mdnBrowserCompatData.default.api).map(([name, api]) => ({ ...api,
    name,
    kind: _types.APIKind.Web
  })), ...Object.entries(_mdnBrowserCompatData.default.javascript.builtins).map(([name, api]) => ({ ...api,
    name,
    kind: _types.APIKind.ES
  }))];
  normalizedBrowserCompatApis.forEach(api => {
    // ex. 'Window'
    // ex. Window {... }
    const {
      name
    } = api;
    const normalizedApi = (0, _normalizeProtochain.default)(name);
    apiMetadata.push({
      id: normalizedApi,
      name,
      language: _types.Language.JS,
      protoChain: [normalizedApi],
      protoChainId: normalizedApi,
      kind: api.kind,
      // @ts-ignore
      compat: api.__compat || api
    }); // ex. ['alert', 'document', ...]

    Object.entries(api).forEach(([childName, childApi]) => {
      const protoChainId = [normalizedApi, childName].join(".");
      apiMetadata.push({
        id: protoChainId,
        name: childName,
        language: _types.Language.JS,
        kind: api.kind,
        protoChain: [normalizedApi, childName],
        protoChainId,
        // eslint-disable-next-line no-underscore-dangle
        // @ts-ignore
        compat: (childApi === null || childApi === void 0 ? void 0 : childApi.__compat) || childApi || api
      });
    });
  });
  return apiMetadata;
}