import { ProviderApiMetadata } from "./types";
/**
 * Write compat.json file which contains API metadata and compat data
 */
export default function Compat(): Promise<ProviderApiMetadata[]>;
//# sourceMappingURL=compat.d.ts.map