"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = astMetadataInferer;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _providers = _interopRequireDefault(require("./providers"));

var _astNodeTypesTester = _interopRequireDefault(require("./helpers/ast-node-types-tester"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const API_BLACKLIST = ["close", "confirm", "print"];

async function astMetadataInferer() {
  // @HACK: Temporarily ignoring the last 1K records because they
  //        cause issues for some unknown reason. They prevent
  //        AstMetadataInferer from returning
  const providerResults = await (0, _providers.default)();
  const records = providerResults.filter(metadata => !API_BLACKLIST.includes(metadata.name));

  const file = _path.default.join(__dirname, "..", "metadata.json");

  if (_fs.default.existsSync(file)) {
    await _fs.default.promises.unlink(file);
  }

  const promises = [];
  const parallelisim = 4;
  const eachRecordsSize = Math.floor(records.length / parallelisim);

  for (let i = 0; i < parallelisim; i += 1) {
    const recordsSliceEnd = i === parallelisim ? records.length + 1 : (i + 1) * eachRecordsSize;
    const recordsSlice = records.slice(i * eachRecordsSize, recordsSliceEnd);
    promises.push((0, _astNodeTypesTester.default)(recordsSlice));
  }

  const recordsWithMetadata = await Promise.all(promises).then(res => res.flat());
  await _fs.default.promises.writeFile(file, JSON.stringify(recordsWithMetadata));
  return recordsWithMetadata;
}