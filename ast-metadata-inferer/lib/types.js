"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.APIKind = exports.Language = exports.AstNodeTypes = void 0;
let AstNodeTypes;
exports.AstNodeTypes = AstNodeTypes;

(function (AstNodeTypes) {
  AstNodeTypes["MemberExpression"] = "MemberExpression";
  AstNodeTypes["CallExpression"] = "CallExpression";
  AstNodeTypes["NewExpression"] = "NewExpression";
})(AstNodeTypes || (exports.AstNodeTypes = AstNodeTypes = {}));

let Language;
exports.Language = Language;

(function (Language) {
  Language["JS"] = "js-api";
  Language["CSS"] = "css-api";
})(Language || (exports.Language = Language = {}));

let APIKind;
exports.APIKind = APIKind;

(function (APIKind) {
  APIKind["Web"] = "web";
  APIKind["ES"] = "es";
})(APIKind || (exports.APIKind = APIKind = {}));