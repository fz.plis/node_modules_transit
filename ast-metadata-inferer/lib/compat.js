"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Compat;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _metadata = _interopRequireDefault(require("./metadata"));

var _mdn = _interopRequireDefault(require("./providers/mdn"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Write compat.json file which contains API metadata and compat data
 */
async function Compat() {
  const astMetadata = await (0, _metadata.default)(); // Add all the corresponding compat data for each inferred ast node

  const compatDataMap = new Map((0, _mdn.default)().map(e => [e.protoChainId, e]));
  const apisWithCompatRecords = astMetadata.filter(api => compatDataMap.has(api.protoChainId));

  const compatRecordsFile = _path.default.join(__dirname, "..", "compat.json");

  await _fs.default.promises.writeFile(compatRecordsFile, JSON.stringify(apisWithCompatRecords));
  return apisWithCompatRecords;
}

if (require.main === module) {
  Compat().catch(e => {
    throw new Error(e);
  });
}