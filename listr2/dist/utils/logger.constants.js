"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logLevels = void 0;
var logLevels;
(function (logLevels) {
    logLevels["silent"] = "silent";
    logLevels["fail"] = "fail";
    logLevels["skip"] = "skip";
    logLevels["success"] = "success";
    logLevels["data"] = "data";
    logLevels["start"] = "start";
    logLevels["title"] = "title";
})(logLevels = exports.logLevels || (exports.logLevels = {}));
//# sourceMappingURL=logger.constants.js.map