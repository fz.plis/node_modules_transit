export declare enum stateConstants {
    'PENDING' = "PENDING",
    'COMPLETED' = "COMPLETED",
    'FAILED' = "FAILED",
    'SKIPPED' = "SKIPPED"
}
