"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stateConstants = void 0;
var stateConstants;
(function (stateConstants) {
    stateConstants["PENDING"] = "PENDING";
    stateConstants["COMPLETED"] = "COMPLETED";
    stateConstants["FAILED"] = "FAILED";
    stateConstants["SKIPPED"] = "SKIPPED";
})(stateConstants = exports.stateConstants || (exports.stateConstants = {}));
//# sourceMappingURL=state.constants.js.map